@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Account Create</div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{url('accounts/')}}" class="btn theme-color text-white">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alart alart-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="needs-validation" method="POST" action="{{route('accounts.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">Branch</label>
                            <select type="select" id="branch_id" name="branch_id" class="form-control">
                                <option value="">Choose Branch</option>
                                @foreach ($branches as $branch)
                                    <option value="{{$branch->id}}">{{$branch->branch_name}}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('branch_id')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">COA Type</label>
                            <select type="select" name="type" class="form-control">
                                <option value="">Choose COA type</option>
                                <option value="assets">Assets</option>
                                <option value="liabilities">Liabilities</option>
                                <option value="equity">Equity</option>
                                <option value="income">Income</option>
                                <option value="expenses">Expenses</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('type')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">DR/CR</label>
                            <select type="select" name="dr_cr" class="form-control">
                                <option value="">Choose DR/CR</option>
                                <option value="dr">DR</option>
                                <option value="cr">CR</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('dr_cr')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">COA Detail</label>
                            <select type="select" id="acc_id" name="acc_main_id" class="form-control">
                                <option value="">Choose account code</option>
                                @foreach ($acc_code_name as $code)
                                    <option value="{{$code->id}}" {{(old("acc_id") == $code->acc_code ? "selected":"")}}>{{$code->acc_code}} - {{$code->acc_name}}</option>
                                @endforeach
                                {{-- @foreach ($acc_code_name as $code)
                                    <option value="{{$code->id}}">{{$code->acc_code}} - {{$code->acc_name}}</option>
                                @endforeach --}}
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('acc_main_id')}}
                            </div>
                        </div>
                        {{-- <div class="col-md-3 mb-3">
                            <label for="validationCustom03">Name</label>
                            <select type="select" name="acc_name_id" class="form-control">
                                <option value="">Choose account name</option>
                                @foreach ($acc_code_name as $name)
                                    <option value="{{$name->id}}">{{$name->acc_name}}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('acc_name_id')}}
                            </div>
                        </div> --}}
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">Sub Account Code</label>
                            <input type="text" class="form-control" id="" name="sub_acc_code" value="" placeholder="" required>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('sub_acc_code')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">Sub Account Name</label>
                            <input type="text" class="form-control" id="" name="sub_acc_name" value="" placeholder="" required>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('sub_acc_name')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">FRD COA</label>
                            <select type="select" id="frd_coa_code_id" name="frd_coa_code_id" class="form-control">
                                <option value=""></option>
                                {{-- @foreach ($frd_code as $frd)
                                    <option value="{{$frd->id}}">{{$frd->frd_coa}}</option>
                                @endforeach --}}
                            </select>
                            {{-- <input type="text" class="form-control" id="" name="frd_coa_code" value="" placeholder=""> --}}
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('frd_coa_code')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">Status </label>
                            <select type="select" id="" name="status" class="form-control">
                                <option value="test">Choose Status</option>
                                <option value="none">None</option>
                                <option value="active">Active</option>
                                <option value="unactive">Unactive</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('status')}}
                            </div>
                        </div>
                        
                    </div>
                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                    <a href="{{url('accounts/')}}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
                </div>
            </div>
        </form>
    </div>
    </div>
@endsection

