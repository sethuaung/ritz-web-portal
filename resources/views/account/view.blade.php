@extends('layouts.app')
@section('content')

    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Account Details
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{url('accounts/')}}" class="btn theme-color text-white">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>    </div>
        </div>            <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Branch</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        : {{$acc_code_name->branch_name}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">COA Type</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        @if ($account->type == 'assets')
                            : Assets
                        @elseif($account->type == 'liabilities')
                            Liabilities
                        @elseif($account->type == 'equity')
                            : Equity
                        @elseif($account->type == 'income')
                            : Income
                        @else
                            : Expenses
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">DR/CR</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        @if ($account->dr_cr == 'dr')
                            : DR
                        @else
                            : CR
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">COA Detail</label>
                    </div>
                    <div class="col-md-9 mb-9">
                        : {{$acc_code_name->acc_code}} - {{$acc_code_name->acc_name}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Sub Code</label>
                    </div>
                    <div class="col-md-9 mb-9">
                        : {{$account->sub_acc_code}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Sub Account Name</label>
                    </div>
                    <div class="col-md-9 mb-9">
                        : {{$account->sub_acc_name}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">FRA COA</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        : {{$acc_code_name->frd_coa}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Status</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        @if ($account->status == 'none')
                            : None
                        @elseif($account->status == 'active')
                            : Active 
                        @else
                            : Unactive
                        @endif
                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection
