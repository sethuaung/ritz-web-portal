@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Account Edit</div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{url('accounts/')}}" class="btn theme-color text-white">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alart alart-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="needs-validation" method="POST" action="{{route('accounts.update',$account->id)}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">Branch</label>
                            <select type="select" id="branch_id" name="branch_id" class="form-control">
                                <option value="">Choose Branch</option>
                                @foreach ($branches as $branch)
                                    <option value="{{$branch->id}}" @if ($account->branch_id == $branch->id) selected="selected" @endif>{{$branch->branch_name}}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('branch_id')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">COA Type</label>
                            <select type="select" name="type"  class="form-control">
                                <option value="">Choose COA type</option>
                                <option value="assets" {{($account->type=="assets")?"selected":""}}>Assets</option>
                                <option value="liabilities" {{($account->type=="liabilities")?"selected":""}}>Liabilities</option>
                                <option value="equity" {{($account->type=="equity")?"selected":""}}>Equity</option>
                                <option value="income" {{($account->type=="income")?"selected":""}}>Income</option>
                                <option value="expenses" {{($account->type=="expenses")?"selected":""}}>Expenses</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('type')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">DR/CR</label>
                            <select type="select" name="dr_cr" class="form-control">
                                <option value="">Choose DR/CR</option>
                                <option value="dr" {{($account->dr_cr=="dr")?"selected":""}}>DR</option>
                                <option value="cr" {{($account->dr_cr=="cr")?"selected":""}}>CR</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('dr_cr')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">COA Detail</label>
                            <select type="select" id="acc_id" name="acc_main_id" class="form-control">
                                <option value="">Choose account code</option>
                                @foreach ($acc_code_name as $code)
                                    <option value="{{$code->id}}" @if ($account->acc_main_id == $code->id) selected="selected" @endif {{(old("acc_id") == $code->acc_code ? "selected":"")}}>{{$code->acc_code}} - {{$code->acc_name}}</option>
                                @endforeach
                                {{-- @foreach ($account_code_name as $code)
                                    <option value="{{$code->id}}" @if ($account->acc_main_id == $code->id) selected="selected" @endif>{{$code->acc_code}} - {{$code->acc_name}}</option>
                                @endforeach --}}
                                
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('acc_main_id')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">Sub Code</label>
                            <input type="text" class="form-control" id="" name="sub_acc_code" value="{{$account->sub_acc_code}}" placeholder="">
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('sub_acc_code')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">Sub Account Name</label>
                            <input type="text" class="form-control" id="" name="sub_acc_name" value="{{$account->sub_acc_name}}" placeholder="">
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('sub_acc_name')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">FRD COA</label>
                            <select type="select" id="frd_coa_code_id" name="frd_coa_code_id" class="form-control">
                                <option value="">Choose FRD COA Code</option>
                                {{-- @foreach ($frd_code as $frd) --}}
                                    {{-- <option value="{{$frd->id}}" @if ($account->frd_coa_code_id == $frd->id) selected="selected" @endif>{{$frd->frd_coa}}</option> --}}
                                    <option data-value="{{$account->frd_coa_code_id}}" selected >{{$account->frd_coa_code_id}}</option>
    
                                {{-- @endforeach --}}
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('frd_coa_code_id')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="validationCustom03">Status </label>
                            <select type="select" id="" name="status" class="form-control">
                                <option value="test">Choose Status</option>
                                <option value="none" {{($account->status=="none")?"selected":""}}>None</option>
                                <option value="active" {{($account->status=="active")?"selected":""}}>Active</option>
                                <option value="unactive" {{($account->status=="unactive")?"selected":""}}>Unactive</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('status')}}
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                    <a href="{{url('accounts/')}}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
                </div>
            </div>
        </form>
    </div>
    </div>
@endsection
