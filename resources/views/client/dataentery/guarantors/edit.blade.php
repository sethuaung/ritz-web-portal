@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Guarantor Edit
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{ url('guarantors/') }}" class="btn theme-color text-white">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alart alart-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="needs-validation" method="POST"
                    action="{{ route('guarantors.update', $guarantor->guarantor_uniquekey) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Guarantor Unique Code</label>
                            <input type="text" disabled class="form-control" id="guarantor_uniquekey_edit"
                                name="guarantor_uniquekey_edit" value="{{ $guarantor->guarantor_uniquekey }}">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('guarantor_uniquekey_edit') }}
                            </div>
                        </div>

                    </div>
                    <div class="position-relative form-group form-row">

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Guarantor Name<span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="name" name="name"
                                value="{{ $guarantor->name }}" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('name') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="exampleEmail" class="">Guarantor Name MM </label>
                            <input name="name_mm" id="name_mm" type="text" class="form-control"
                                placeholder="Enter Name MM" value="{{ $guarantor->name_mm }}">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('name_mm') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Gender <span
                                    style="color: red">*</span></label><br>
                            <select type="select" id="gender" name="gender" class="form-control">
                                <option value="">Choose Gender</option>
                                <option value="male"{{ $guarantor->gender == 'male' ? 'selected' : '' }}>Male</option>
                                <option value="female"{{ $guarantor->gender == 'female' ? 'selected' : '' }}>Female</option>
                                <option value="none" {{ $guarantor->gender == 'none' ? 'selected' : '' }}>Others</option>

                            </select>

                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('gender') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Date of Birth <span
                                    style="color: red">*</span></label>
                            <input type="date" class="form-control" id="dob" name="dob"
                                value="{{ $guarantor->dob }}" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('dob') }}
                            </div>
                        </div>
                    </div>

                    @if (!empty($guarantor->nrc))
                        <div class="position-relative form-group form-row">
                            <div class="col-md-6 ">
                                <label style="font-weight: 600" for="validationCustom01">New NRC </label>
                                <div class="position-relative form-group form-row">
                                    <div class="col-md-3">
                                        <select name="nrc_1" id="nrc_1" class="form-control">
                                            <option value=""></option>
                                            @for ($i = 1; $i <= 14; $i++)
                                                <option value="{{ $i }}"
                                                    @if ($exploded_nrc[0] == $i) selected="selected" @endif>
                                                    {{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select name="nrc_2" id="nrc_2" class="form-control">
                                            <option value="{{ $exploded_nrc[1] }}">{{ $exploded_nrc[1] }}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 ">
                                        <select type="select" id="nrc_3" name="nrc_3" class="form-control">
                                            <option value="(N)"
                                                @if ($exploded_nrc[2] == 'N') selected="selected" @endif>(N)</option>
                                            <option value="(C)"
                                                @if ($exploded_nrc[2] == 'C') selected="selected" @endif>(C)</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3 ">
                                        <input type="number" class="form-control" id="nrc_4" name="nrc_4"
                                            value="{{ $exploded_nrc[3] }}" placeholder=""
                                            oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                            maxlength="6">
                                    </div>
                                </div>

                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('nrc_4') }}
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <label style="font-weight: 600" for="exampleEmail" class="">NRC Card ID </label>
                                <input name="nrc_card_id" id="nrc_card_id" type="text" class="form-control"
                                    value="{{ $guarantor->nrc_card_id }}">
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('nrc_card_id') }}
                                </div>
                            </div>
                        </div>
                    @elseif(!empty($guarantor->old_nrc))
                        <div class="position-relative form-group form-row">
                            <div class="col-md-6 ">
                                <label style="font-weight: 600" for="validationCustom01">Old NRC <span
                                        style="color: red">*</span></label>
                                <div class="position-relative form-group form-row">
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" id="old_nrc" name="old_nrc"
                                            value="{{ $guarantor->old_nrc }}" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <label style="font-weight: 600" for="exampleEmail" class="">NRC Card ID <span
                                        style="color: red">*</span></label>
                                <input name="nrc_card_id" id="nrc_card_id" type="text" class="form-control"
                                    value="{{ $guarantor->nrc_card_id }}" required>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('nrc_card_id') }}
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="position-relative form-group form-row">
                            <div class="col-md-12 ">
                                <div class="position-relative form-group form-row">
                                    <div class="col-md-12 ">
                                        <input type="text" disabled class="form-control" placeholder="Empty NRC"
                                            placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="position-relative form-group form-row">
                        @if ($guarantor->guarantor_nrc_front_photo)
                            <div class="col-md-3 mb-3 ">
                                <label style="font-weight: 600" for="" class="">NRC Front</label>
                                <input name="guarantor_nrc_front_photo" id=""
                                    value="{{ $guarantor->guarantor_nrc_front_photo }}" type="file"
                                    class="form-control"><br>
                                <img src="{{ asset('public/storage/guarantor_photos/' . $guarantor->guarantor_nrc_front_photo) }}"
                                    width="150px" height="120px" alt="" title="">
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('guarantor_nrc_front_photo') }}
                                </div>
                            </div>
                        @endif
                        @if ($guarantor->guarantor_nrc_back_photo)
                            <div class="col-md-3 mb-3">
                                <label style="font-weight: 600" for="" class="">NRC Back</label>
                                <input name="guarantor_nrc_back_photo" id=""
                                    value="{{ $guarantor->guarantor_nrc_back_photo }}" type="file"
                                    class="form-control"><br>
                                <img src="{{ asset('public/storage/guarantor_photos/' . $guarantor->guarantor_nrc_back_photo) }}"
                                    width="150px" height="120px" alt="" title="">
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('guarantor_nrc_back_photo') }}
                                </div>
                            </div>
                        @endif
                        @if ($guarantor->nrc_recommendation)
                            <div class="col-md-3 mb-3">
                                <label style="font-weight: 600" for="" class="">NRC Recommendation</label>
                                <input name="nrc_recommendation" id=""
                                    value="{{ $guarantor->nrc_recommendation }}" type="file"
                                    class="form-control"><br>
                                <img src="{{ asset('public/storage/guarantor_photos/' . $guarantor->nrc_recommendation) }}"
                                    width="150px" height="120px" alt="" title="">
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('nrc_recommendation') }}
                                </div>
                            </div>
                        @endif
                        <div class="col-md-3 mb-3 ">
                            <label style="font-weight: 600" for="" class="">Guarantor's Photo</label>
                            <input name="guarantor_photo" id="" value="{{ $guarantor->guarantor_photo }}"
                                type="file" class="form-control"><br>
                            <img src="{{ asset('public/storage/guarantor_photos/' . $guarantor->guarantor_photo) }}"
                                width="150px" height="120px" alt="" title="">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('guarantor_photo') }}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3 ">
                            <label style="font-weight: 600" for="" class="">KYC Photo</label>
                            <input name="kyc_photo" id="" value="{{ $guarantor->kyc_photo }}" type="file"
                                class="form-control"><br>
                            <img src="{{ asset('public/storage/guarantor_photos/' . $guarantor->kyc_photo) }}"
                                width="150px" height="120px" alt="" title="">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('kyc_photo') }}
                            </div>
                        </div>

                    </div>

                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Primary Phone </label>
                            <input type="number" class="form-control" id="phone_primary" name="phone_primary"
                                value="{{ $guarantor->phone_primary }}" minlength="01000000"
                                oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                maxlength="11">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('phone_primary') }}
                            </div>
                        </div>


                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Secondary Phone</label>
                            <input type="number" class="form-control" id="phone_secondary" name="phone_secondary"
                                value="{{ $guarantor->phone_secondary }}" minlength="01000000"
                                oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                maxlength="11">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('phone_secondary') }}
                            </div>
                        </div>


                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Email </label>
                            <input type="text" class="form-control" id="email" name="email"
                                value="{{ $guarantor->email }}">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('email') }}
                            </div>
                        </div>


                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Blood Type </label><br>
                            <select type="select" id="blood_type" name="blood_type" class="form-control">
                                <option value="">Choose Blood Type</option>
                                <option value="A"{{ $guarantor->blood_type == 'A' ? 'selected' : '' }}>A</option>
                                <option value="B"{{ $guarantor->blood_type == 'B' ? 'selected' : '' }}>B</option>
                                <option value="AB" {{ $guarantor->blood_type == 'AB' ? 'selected' : '' }}>AB</option>
                                <option value="O"{{ $guarantor->blood_type == 'O' ? 'selected' : '' }}>O</option>
                                <option value="none" {{ $guarantor->blood_type == 'none' ? 'selected' : '' }}>Others
                                </option>

                            </select>


                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('blood_type') }}
                            </div>
                        </div>
                    </div>

                    <div class="position-relative form-group form-row">
                        {{-- <div class="col-md-3 mb-3">
                        <label style="font-weight: 600" for="validationCustom03">Religion <span style="color: red">*</span></label>
                        <input type="text" class="form-control" id="religion" name="religion" value="{{$guarantor->religion}}" required>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('religion')}}
                        </div>
                    </div>


                    <div class="col-md-3 mb-3">
                        <label style="font-weight: 600" for="validationCustom03">Nationality <span style="color: red">*</span></label>
                   <input type="text" class="form-control" id="nationality" name="nationality" value="{{$guarantor->nationality}}" required>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('nationality')}}
                        </div>
                    </div> --}}
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Religion </label>
                            <select type="select" id="religion" name="religion" class="form-control">
                                <option value="">Choose Religion</option>
                                <option {{ $guarantor->religion == 'Kachin' ? 'selected' : '' }}>Buddhist</option>
                                <option {{ $guarantor->religion == 'Christian' ? 'selected' : '' }}>Christian</option>
                                <option {{ $guarantor->religion == 'Hindu' ? 'selected' : '' }}>Hindu</option>
                                <option {{ $guarantor->religion == 'Islamic' ? 'selected' : '' }}>Islamic</option>
                                <option {{ $guarantor->religion == 'Jewish' ? 'selected' : '' }}>Jewish</option>
                                <option {{ $guarantor->religion == 'Other Religions' ? 'selected' : '' }}>Other Religions
                                </option>
                                <option {{ $guarantor->religion == 'Not Religious' ? 'selected' : '' }}>Not Religious
                                </option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('religion') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Nationality</label>
                            <select type="select" id="nationality" name="nationality" class="form-control">
                                <option value="">Choose Religion</option>
                                <option {{ $guarantor->nationality == 'Kachin' ? 'selected' : '' }}>Kachin</option>
                                <option {{ $guarantor->nationality == 'Kayin' ? 'selected' : '' }}>Kayin</option>
                                <option {{ $guarantor->nationality == 'Kayah' ? 'selected' : '' }}>Kayah</option>
                                <option {{ $guarantor->nationality == 'Chin' ? 'selected' : '' }}>Chin</option>
                                <option {{ $guarantor->nationality == 'Mon' ? 'selected' : '' }}>Mon</option>
                                <option {{ $guarantor->nationality == 'Bamar' ? 'selected' : '' }}>Bamar</option>
                                <option {{ $guarantor->nationality == 'Rakhine' ? 'selected' : '' }}>Rakhine</option>
                                <option {{ $guarantor->nationality == 'Shan' ? 'selected' : '' }}>Shan</option>
                                <option {{ $guarantor->nationality == 'Unknown' ? 'selected' : '' }}>Unknown</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('nationality') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Education</label>
                            <select name="education_id" id="education_id" class="form-control">
                                <option value="">Choose Education</option>
                                @foreach ($educations as $education)
                                    <option value="{{ $education->id }}"
                                        {{ $guarantor->education_id == $education->id ? 'selected' : '' }}>
                                        {{ $education->education_name }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('education_id') }}
                            </div>
                        </div>


                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Other Education </label>
                            <input type="text" class="form-control" id="other_education" name="other_education"
                                value="{{ $guarantor->other_education }}">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('other_education') }}
                            </div>
                        </div>


                    </div>

                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Division/State <span
                                    style="color: red">*</span></label>
                            <select type="select" id="division_id" name="division_id" class="form-control">
                                @foreach ($divisions as $division)
                                    <option value="{{ $division->code }}"
                                        {{ $guarantor->division_id == $division->code ? 'selected' : '' }}>
                                        {{ $division->name }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('division_id') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">District <span
                                    style="color: red">*</span></label>
                            <select type="select" id="district_id" name="district_id" class="form-control">
                                @foreach ($districts as $district)
                                    <option value="{{ $district->code }}"
                                        {{ $guarantor->district_id == $district->code ? 'selected' : '' }}>
                                        {{ $district->name }} </option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('district_id') }}
                            </div>
                        </div>

                        {{-- <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">City <span
                                    style="color: red">*</span></label>
                            <select type="select" id="city_id" name="city_id" class="form-control">
                                @foreach ($cities as $city)
                                    <option value="{{ $city->id }}"
                                        {{ $guarantor->city_id == $city->id ? 'selected' : '' }}>{{ $city->city_name }}
                                    </option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('city_id') }}
                            </div>
                        </div> --}}

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Township/Province <span
                                    style="color: red">*</span></label>
                            <select type="select" id="township_id" name="township_id" class="form-control">
                                @foreach ($townships as $township)
                                    <option value="{{ $township->code }}"
                                        {{ $guarantor->township_id == $township->code ? 'selected' : '' }}>
                                        {{ $township->name }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('township_id') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Village <span
                                    style="color: red">*</span></label>
                            <select type="select" id="village_id" name="village_id" class="form-control">
                                @foreach ($villages as $village)
                                    <option value="{{ $village->code }}"
                                        {{ $guarantor->village_id == $village->code ? 'selected' : '' }}>
                                        {{ $village->name }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('village_id') }}
                            </div>
                        </div>
                    </div>

                    <div class="position-relative form-group form-row">
                        {{-- <div class="col-md-3 mb-3">
                        <label style="font-weight: 600" for="validationCustom03">Province <span style="color: red">*</span></label>
                        <select type="select" id="province_id" name="province_id" class="form-control">
                                    @foreach ($provinces as $province)
                                    <option value="{{$province->id}}" {{ ($guarantor->province_id == $province->id) ? "selected" : "" }}>{{$province->province_name}} </option>
                                    @endforeach
                                    </select>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('province_id')}}
                        </div>
                    </div> --}}


                        


                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Quarter</label>
                            <select type="select" id="ward_id" name="ward_id" class="form-control">
                                <option selected disabled>Select Quarter</option>
                                @foreach ($wards as $ward)
                                    <option value="{{ $ward->code }}"
                                        {{ $guarantor->quarter_id == $ward->code ? 'selected' : '' }}>
                                        {{ $ward->name }} </option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('quarter_id') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">status <span
                                    style="color: red">*</span></label> <br>
                            <select type="select" id="status" name="status" class="form-control">
                                <option value="">Choose Status</option>
                                <option value="active"{{ $guarantor->status == 'active' ? 'selected' : '' }}>Active
                                </option>
                                <option value="inactive"{{ $guarantor->status == 'inactive' ? 'selected' : '' }}>Inactive
                                </option>
                                <option value="none" {{ $guarantor->status == 'none' ? 'selected' : '' }}>None</option>

                            </select>

                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('status') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Guarantor's Current Job</label>
                            <input type="text" class="form-control" id="current_job" name="current_job"
                                value="{{ $guarantor->current_job }}" placeholder="Example : Office Staff">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('current_job') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Guarantor's Income Per Month In
                                Kyats</label>
                            <input type="number" class="form-control" id="income" name="income"
                                value="{{ $guarantor->income }}" placeholder="Example : 300000">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('income') }}
                            </div>
                        </div>
                    </div>
                    <div class="position-relative form-group form-row">
                        <div class="col-md-12 mb-12">
                            <label style="font-weight: 600" for="exampleEmail" class="">Primary Address</label>
                            <textarea name="address_primary" id="address_primary" value="" class="form-control">{{ $guarantor->address_primary }}</textarea>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('address') }}
                            </div>
                        </div>
                    </div>
                    <div class="position-relative form-group form-row">
                        <div class="col-md-12 mb-12">
                            <label style="font-weight: 600" for="exampleEmail" class="">Secondary Address</label>
                            <textarea name="address_secondary" id="address_secondary" value="" class="form-control">{{ $guarantor->address_secondary }}</textarea>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('address2') }}
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save &
                            Back </i></button>
                    <a href="{{ url('guarantors/') }}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
                </form>
            </div>
        </div>

    </div>
@endsection
