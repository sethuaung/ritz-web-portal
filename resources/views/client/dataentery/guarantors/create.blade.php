@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Guarantor Create
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{ url('guarantors/') }}" class="btn theme-color text-light">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alart alart-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="needs-validation" method="POST" action="{{ route('guarantors.store') }}"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="position-relative form-group form-row" hidden>
                        <div class="col-md-4 mb-4">
                            <label style="font-weight: 600" for="validationCustom01">Guarantor Unique Code</label>
                            <input type="text" readonly class="form-control" id="guarantor_uniquekey"
                                name="guarantor_uniquekey" value="" placeholder="">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('guarantor_uniquekey') }}
                            </div>
                        </div>

                    </div>
                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Guarantor Name<span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="name" name="name"
                                value="{{ old('name') }}" autofocus="autofocus" placeholder="Example : Mg Mg" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('name') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="exampleEmail" class="">Guarantor Name MM </label>
                            <input name="name_mm" id="name_mm" type="text" class="form-control"
                                placeholder="Enter Name MM" value="{{ old('name_mm') }}">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('name_mm') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Gender <span
                                    style="color: red">*</span></label><br>
                            <select type="select" id="gender" name="gender" class="form-control" required>
                                <option value="">Choose Gender</option>
                                <option {{ old('gender') == 'male' ? 'selected' : '' }} value="male">Male</option>
                                <option {{ old('gender') == 'female' ? 'selected' : '' }} value="female">Female</option>
                                <option {{ old('gender') == 'none' ? 'selected' : '' }} value="none">Others</option>
                            </select>

                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('gender') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Date of Birth <span
                                    style="color: red">*</span></label>
                            <input type="date" class="form-control" id="dob" name="dob"
                                value="{{ old('dob') }}" placeholder="" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('dob') }}
                            </div>
                        </div>


                    </div>

                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 ">
                            <div class="position-relative form-group form-row">
                                <div class="col-md-6 ">
                                    <label style="font-weight: 600" for="exampleEmail" class="">Has NRC <span
                                            style="color: red">*</span></label>
                                    <select type="select" id="has_nrc" name="has_nrc" class="form-control">
                                        <option value="">Has NRC??</option>
                                        <option {{ old('nrc_type') == 'Yes' ? 'selected' : '' }}>Yes</option>
                                        <option {{ old('nrc_type') == 'No' ? 'selected' : '' }}>No</option>
                                    </select>
                                </div>
                                <div class="col-md-6 " id="nrc_type1">
                                    <label style="font-weight: 600" for="exampleEmail" class="">NRC Type <span
                                            style="color: red">*</span></label>
                                    <select type="select" id="nrc_type_select1" name="nrc_type" class="form-control">
                                        <option value="">Choose NRC Type</option>
                                        <option {{ old('nrc_type') == 'Old NRC' ? 'selected' : '' }}>Old NRC</option>
                                        <option {{ old('nrc_type') == 'New NRC' ? 'selected' : '' }}>New NRC</option>
                                    </select>
                                </div>
                                <div class="col-md-6 " id="nrc_type2">
                                    <label style="font-weight: 600" for="exampleEmail" class="">NRC Type <span
                                            style="color: red">*</span></label>
                                    <select type="select" id="nrc_type_select2" name="nrc_type" class="form-control">
                                        <option value="">Not Has NRC</option>
                                        <option {{ old('nrc_type') == 'Still Applying' ? 'selected' : '' }}
                                            value="Still Applying">Still Applying(လျှောက်ထားဆဲ)</option>
                                        <option {{ old('nrc_type') == 'Loss NRC' ? 'selected' : '' }} value="Loss NRC">
                                            Loss NRC(ပျောက်ဆုံး)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="oldNRC">
                            <label style="font-weight: 600" for="validationCustom01">Old NRC <span
                                    style="color: red">*</span></label>
                            <input name="old_nrc" id="nrc" type="text" class="form-control"
                                value="{{ old('old_nrc') }}">

                        </div>
                        <div class="col-md-6" id="GuarantorstillNRC">
                            <label style="font-weight: 600" for="validationCustom01">Still Applying <span
                                    style="color: red">*</span></label>
                            <input name="nrc_recommendation" id="stillappnrc" type="file" class="form-control"
                                placeholder="Please select recommendation file" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('nrc') }}
                            </div>
                        </div>

                        <div class="col-md-6" id="GuarantorlossNRC">
                            <label style="font-weight: 600" for="validationCustom01"> Loss NRC <span
                                    style="color: red">*</span></label>
                            <input name="nrc_recommendation" id="lossnrc" type="file" class="form-control"
                                placeholder="Please select recommendation file" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('nrc') }}
                            </div>
                        </div>
                        <div class="col-md-6 " id="newNRC">
                            <label style="font-weight: 600" for="validationCustom01">New NRC <span
                                    style="color: red">*</span></label>
                            <div class="position-relative form-group form-row">
                                <div class="col-md-3">
                                    <select name="nrc_1" id="nrc_1" class="form-control">
                                        <option value=""></option>
                                        @for ($i = 1; $i <= 14; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select name="nrc_2" id="nrc_2" class="form-control">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="col-md-3 ">
                                    <select type="select" id="nrc_3" name="nrc_3" class="form-control">
                                        <option value=""></option>
                                        <option value="(N)">(N)</option>
                                        <option value="(C)">(C)</option>
                                    </select>
                                </div>
                                <div class="col-md-3 ">
                                    <input type="number" class="form-control" id="nrc_4" name="nrc_4"
                                        value="{{ old('nrc_4') }}" placeholder=""
                                        oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                        maxlength="6">
                                </div>
                            </div>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('nrc_4') }}
                            </div>
                        </div>
                        <div class="col-md-3 ">
                            <label style="font-weight: 600" for="exampleEmail" class="">NRC Card ID <span
                                    style="color: red">*</span></label>
                            <input name="nrc_card_id" id="nrc_card_id" type="text" class="form-control"
                                placeholder="Example : AA 123456" value="{{ old('nrc_card_id') }}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                        maxlength="9" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('nrc_card_id') }}
                            </div>
                        </div>
                    </div>

                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 mb-3 " id="guarantor_nrc_front_photo">
                            <label style="font-weight: 600" for="validationCustom01" class="">NRC Front <span
                                    style="color: red">*</span></label>
                            <input name="guarantor_nrc_front_photo" id="gnfp" type="file" class="form-control"
                                required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('guarantor_nrc_front_photo') }}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3" id="guarantor_nrc_back_photo">
                            <label style="font-weight: 600" for="validationCustom01" class="">NRC Back <span
                                    style="color: red">*</span></label>
                            <input name="guarantor_nrc_back_photo" id="gnbp" type="file" class="form-control"
                                required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('guarantor_nrc_back_photo') }}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01" class="">Guarantor's Photo
                                <span style="color: red">*</span></label>
                            <input name="guarantor_photo" id="guarantor_photo" type="file" class="form-control"
                                required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('guarantor_photo') }}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01" class="">KYC Photo <span
                                    style="color: red">*</span></label>
                            <input name="kyc_photo" id="kyc_photo" type="file" class="form-control" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('kyc_photo') }}
                            </div>
                        </div>

                    </div>

                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Primary Phone <span
                                    style="color: red">*</span></label>
                            <input type="number" class="form-control" id="phone_primary" name="phone_primary"
                                value="{{ old('phone_primary') }}" placeholder="Example : 091234567"
                                minlength="01000000"
                                oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                maxlength="11" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('phone_primary') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Secondary Phone</label>
                            <input type="number" class="form-control" id="phone_secondary" name="phone_secondary"
                                value="{{ old('phone_secondary') }}" placeholder="Example : 091234567"
                                minlength="01000000"
                                oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                maxlength="11">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('phone_secondary') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Email <span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="email" name="email"
                                value="{{ old('email') }}" placeholder="example@gmail.com" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('email') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Blood Type <span
                                    style="color: red">*</span></label><br>
                            <select type="select" id="blood_type" name="blood_type" class="form-control" required>
                                <option value="">Choose Blood Type</option>
                                <option {{ old('blood_type') == 'A' ? 'selected' : '' }} value="A">A</option>
                                <option {{ old('blood_type') == 'B' ? 'selected' : '' }} value="B">B</option>
                                <option {{ old('blood_type') == 'AB' ? 'selected' : '' }} value="AB">AB</option>
                                <option {{ old('blood_type') == 'O' ? 'selected' : '' }} value="O">O</option>
                                <option {{ old('blood_type') == 'none' ? 'selected' : '' }} value="none">Others</option>
                            </select>

                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('blood_type') }}
                            </div>
                        </div>
                    </div>

                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Religion <span
                                    style="color: red">*</span></label>
                            <select type="select" id="religion" name="religion" class="form-control">
                                <option value="">Choose Religion</option>
                                <option {{ old('religion') == 'Buddhist' ? 'selected' : '' }}>Buddhist</option>
                                <option {{ old('religion') == 'Christian' ? 'selected' : '' }}>Christian</option>
                                <option {{ old('religion') == 'Hindu' ? 'selected' : '' }}>Hindu</option>
                                <option {{ old('religion') == 'Islamic' ? 'selected' : '' }}>Islamic</option>
                                <option {{ old('religion') == 'Jewish' ? 'selected' : '' }}>Jewish</option>
                                <option {{ old('religion') == 'Other Religions' ? 'selected' : '' }}>Other Religions
                                </option>
                                <option {{ old('religion') == 'Not Religious' ? 'selected' : '' }}>Not Religious</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('religion') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Nationality <span
                                    style="color: red">*</span></label>
                            <select type="select" id="nationality" name="nationality" class="form-control">
                                <option value="">Choose Religion</option>
                                <option {{ old('nationality') == 'Kachin' ? 'selected' : '' }}>Kachin</option>
                                <option {{ old('nationality') == 'Kayin' ? 'selected' : '' }}>Kayin</option>
                                <option {{ old('nationality') == 'Kayah' ? 'selected' : '' }}>Kayah</option>
                                <option {{ old('nationality') == 'Chin' ? 'selected' : '' }}>Chin</option>
                                <option {{ old('nationality') == 'Mon' ? 'selected' : '' }}>Mon</option>
                                <option {{ old('nationality') == 'Bamar' ? 'selected' : '' }}>Bamar</option>
                                <option {{ old('nationality') == 'Rakhine' ? 'selected' : '' }}>Rakhine</option>
                                <option {{ old('nationality') == 'Shan' ? 'selected' : '' }}>Shan</option>
                                <option {{ old('nationality') == 'Unknown' ? 'selected' : '' }}>Unknown</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('nationality') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Education <span
                                    style="color: red">*</span></label>
                            <select name="education_id" id="education_id" class="form-control" required>
                                <option value="">Choose Education</option>
                                @foreach ($educations as $education)
                                    <option value="{{ $education->id }}">{{ $education->education_name }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('education_id') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Other Education</label>
                            <input type="text" class="form-control" id="other_education" name="other_education"
                                value="{{ old('other_education') }}" placeholder="Example : Diploma in Business">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('other_education') }}
                            </div>
                        </div>
                    </div>

                    <div class="position-relative form-group form-row">

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">State / Region <span
                                    style="color: red">*</span></label>
                            <select type="select" id="division_id" name="division_id" class="form-control">
                                <option value="">Select State</option>
                                @foreach ($divisions as $division)
                                    <option value="{{ $division->code }}" {{(old("division_id")== $division->name ? "selected":"")}} >{{ $division->name }} / {{$division->description}}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('state_id') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">District / Division <span
                                    style="color: red">*</span></label>
                            <select type="select" id="district_id" name="district_id" class="form-control">
                                <option></option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('district_id') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Township <span
                                    style="color: red">*</span></label>
                            <select type="select" id="township_id" name="township_id" class="form-control">
                                <option></option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('township_id') }}
                            </div>
                        </div>


                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Town / Village / Group Village<span
                                    style="color: red">*</span></label>
                            <select type="select" id="village_id" name="village_id" class="form-control">
                                <option></option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('village_id') }}
                            </div>
                        </div>
                    </div>

                    <div class="position-relative form-group form-row">

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Ward / Small Village <span
                                    ></label>
                            <select type="select" id="ward_id" name="ward_id" class="form-control">
                                <option></option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('ward_id') }}
                            </div>
                        </div>

                        {{-- <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Quarter</label>
                            <select type="select" id="quarter_id" name="quarter_id" class="form-control">
                                <option></option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('quarter_id') }}
                            </div>
                        </div> --}}

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="exampleEmail" class="">Home No <span
                                    style="color: red">*</span></label>
                            <input type="text" name="home_no" id="home_no" class="form-control"
                                placeholder="No(1) " required value="{{ old('home_no') }}">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('home_no') }}
                            </div>
                        </div>

                        {{-- <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="exampleEmail" class="">Street Name <span
                                    style="color: red">*</span></label>
                            <input type="text" name="street_name" id="street_name" class="form-control"
                                placeholder="Thumingalar Street " required value="{{ old('street_name') }}">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('street_name') }}
                            </div>
                        </div> --}}

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Status<span
                                    style="color: red">*</span></label><br>
                            <select type="select" id="status" name="status" class="form-control">
                                <option value="">Choose Status</option>
                                <option value="active">Active</option>
                                <option value="inactive">Inactive</option>
                                <option value="none">None</option>
                            </select>

                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('status') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Guarantor's Current Job</label>
                            <input type="text" class="form-control" id="current_job" name="current_job"
                                value="{{ old('current_job') }}" placeholder="Example : Office Staff">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('current_job') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Guarantor's Income Per Month</label>
                            <input type="number" class="form-control" id="income" name="income"
                                value="{{ old('income') }}" placeholder="Example : 300000" pattern="[0-9]" onkeyup="if(this.value<0){this.value= 0}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "8">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('income') }}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                        	<label style="font-weight: 600" for="">Income Per Year</label>
                            <input type="number" class="form-control" disabled id="g_income_yearly"
                                name="g_income_yearly" value="{{ old('g_income_yearly') }}">
                        </div>
                    </div>
                    <div class="position-relative form-group form-row">
                        <div class="col-md-12 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Primary Address <span
                                    style="color: red">*</span></label>
                            <textarea class="form-control" id="address_primary" name="address_primary" value=""
                                placeholder="Example : No(1), 18th Street, Latha Township, Yangon" required>{{ old('address_primary') }}</textarea>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('address_primary') }}
                            </div>
                        </div>
                    </div>
                    <div class="position-relative form-group form-row">
                        <div class="col-md-12 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Secondary Address</label>
                            <textarea class="form-control" id="address_secondary" name="address_secondary" value="" placeholder="">{{ old('address_secondary') }}</textarea>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('address_secondary') }}
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save &
                            Back </i></button>
                    <a href="{{ url('guarantors/') }}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
                </form>
            </div>
        </div>

    </div>
@endsection
