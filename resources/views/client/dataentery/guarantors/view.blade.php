@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Guarantor Details View
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{ url('guarantors/') }}" class="btn theme-color">
                            <i class="pe-7s-back btn-icon-wrapper text-light">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Guarantor Info</h5>
                                <div class="row">
                                    <div class="col-md-2">
                                        <img src="{{ asset('public/storage/guarantor_photos/' . $guarantor->guarantor_photo) }}"
                                            width='110px' alt="" title=""
                                            class="rounded-circle border-secondary ml-4" style="border: 3px solid;">
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-row">
                                                    <div class="col-md-5 mb-3">
                                                        <label for="validationCustom01" class="font-weight-bold">Guarantor
                                                            ID</label>
                                                    </div>
                                                    <div class="col-md-7 mb-9 ">
                                                        : {{ $guarantor->guarantor_uniquekey }}
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-5 mb-3">
                                                        <label for="validationCustom01"
                                                            class="font-weight-bold">Name</label>
                                                    </div>
                                                    <div class="col-md-7 mb-9 ">
                                                        : {{ $guarantor->name }}
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-5 mb-3">
                                                        <label for="validationCustom01" class="font-weight-bold">Name in
                                                            MM</label>
                                                    </div>
                                                    <div class="col-md-7 mb-9 ">
                                                        : @if ($guarantor->name_mm)
                                                            {{ $guarantor->name_mm }}
                                                        @else
                                                            N/A
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-row">
                                                    <div class="col-md-5 mb-3">
                                                        <label for="validationCustom01" class="font-weight-bold">Date of
                                                            Birth</label>
                                                    </div>
                                                    <div class="col-md-7 mb-9 ">
                                                        : @if ($guarantor->dob)
                                                            {{ $guarantor->dob }}
                                                        @else
                                                            N/A
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-5 mb-3">
                                                        <label for="validationCustom01" class="font-weight-bold">Nationality
                                                            ID</label>
                                                    </div>
                                                    <div class="col-md-7 mb-9 ">
                                                        : @if ($guarantor->nrc)
                                                            {{ $guarantor->nrc }}
                                                        @elseif($guarantor->old_nrc)
                                                        	{{$guarantor->old_nrc}}
                                                        @else
                                                            N/A
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-5 mb-3">
                                                        <label for="validationCustom01" class="font-weight-bold">NRC Card
                                                            ID</label>
                                                    </div>
                                                    <div class="col-md-7 mb-9 ">
                                                        : @if ($guarantor->nrc_card_id)
                                                            {{ $guarantor->nrc_card_id }}
                                                        @else
                                                            N/A
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <label for="validationCustom01" class="font-weight-bold">Primary
                                                            Phone</label>
                                                    </div>
                                                    <div class="col-md-6 mb-9 ">
                                                        : @if ($guarantor->phone_primary)
                                                            {{ $guarantor->phone_primary }}
                                                        @else
                                                            N/A
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <label for="validationCustom01" class="font-weight-bold">Secondary
                                                            Phone</label>
                                                    </div>
                                                    <div class="col-md-6 mb-9 ">
                                                        : @if ($guarantor->phone_secondary)
                                                            {{ $guarantor->phone_secondary }}
                                                        @else
                                                            N/A
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <label for="validationCustom01"
                                                            class="font-weight-bold">Email</label>
                                                    </div>
                                                    <div class="col-md-6 mb-9 ">
                                                        : @if ($guarantor->email)
                                                            {{ $guarantor->email }}
                                                        @else
                                                            N/A
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row mb-4">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header theme-color">
                                <h5 class="text-light">
                                    <i class="fas fa-info-circle"></i>
                                    <span class="ml-2">DETAILS</span>
                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">Gender</label>
                                    </div>
                                    <div class="col-md-6 mb-9 ">
                                        : {{ $guarantor->gender }}
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">Blood Type</label>
                                    </div>
                                    <div class="col-md-6 mb-9 ">
                                        : @if ($guarantor->blood_type)
                                            {{ $guarantor->blood_type }}
                                        @else
                                            N/A
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">Religion</label>
                                    </div>
                                    <div class="col-md-6 mb-9 ">
                                        : @if ($guarantor->religion)
                                            {{ $guarantor->religion }}
                                        @else
                                            N/A
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">Nationality</label>
                                    </div>
                                    <div class="col-md-6 mb-9 ">
                                        : @if ($guarantor->nationality)
                                            {{ $guarantor->nationality }}
                                        @else
                                            N/A
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">Education</label>
                                    </div>
                                    <div class="col-md-6 mb-9 ">
                                        : @if ($guarantor->education_name)
                                            {{ $guarantor->education_name }}
                                        @else
                                            N/A
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">Other Education</label>
                                    </div>
                                    <div class="col-md-6 mb-9 ">
                                        : @if ($guarantor->other_education)
                                            {{ $guarantor->other_education }}
                                        @else
                                            N/A
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">Current Job</label>
                                    </div>
                                    <div class="col-md-6 mb-9 ">
                                        : @if ($guarantor->current_job)
                                            {{ $guarantor->current_job }}
                                        @else
                                            N/A
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">Income</label>
                                    </div>
                                    <div class="col-md-6 mb-9 ">
                                        : {{ $guarantor->income }} Ks ({{ $guarantor->income * 12 }} Ks per year)
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">Status</label>
                                    </div>
                                    <div class="col-md-6 mb-9 text-capitalize">
                                        : {{ $guarantor->status }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header theme-color">
                                <h5 class="text-light text-uppercase">
                                    <i class="fas fa-address-book"></i>
                                    <span class="ml-2">Address</span>
                                </h5>
                            </div>
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">Division</label>
                                    </div>
                                    <div class="col-md-6 mb-9 ">
                                        : @if ($division)
                                            {{ $division->name }}
                                        @else
                                            N/A
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">District</label>
                                    </div>
                                    <div class="col-md-6 mb-9 ">
                                        : @if ($district)
                                            {{ $district->name }}
                                        @else
                                            N/A
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">Township</label>
                                    </div>
                                    <div class="col-md-6 mb-9 ">
                                        : @if ($township)
                                            {{ $township->name }}
                                        @else
                                            N/A
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">Village</label>
                                    </div>
                                    <div class="col-md-6 mb-9 ">
                                        : @if ($village)
                                            {{ $village->name }}
                                        @else
                                            N/A
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">Quarter</label>
                                    </div>
                                    <div class="col-md-6 mb-9 ">
                                        : @if ($quarter)
                                            {{ $quarter->name }}
                                        @else
                                            N/A
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">Primary Address</label>
                                    </div>
                                    <div class="col-md-6 mb-9 ">
                                        : @if ($guarantor->address_primary)
                                            {{$guarantor->address_primary }}
                                        @else
                                            N/A
                                        @endif
                                    </div>
                                </div>
                                <div class="form-row mb-4">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01" class="font-weight-bold">Secondary Address</label>
                                    </div>
                                    <div class="col-md-6 mb-9 ">
                                        : @if ($guarantor->address_secondary)
                                            {{ $guarantor->address_secondary }}
                                        @else
                                            N/A
                                        @endif
                                    </div>
                                </div><br>
                            </div>
                        </div>
                    </div>
                </div>
                <h5 class="menu-header-title text-uppercase mb-3 fsize-2 text-secondary font-weight-bold">Guarantor
                    Documents</h5>
                <div class="row">
                    @if ($guarantor->guarantor_nrc_front_photo)
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="font-weight-bold">NRC Front Photo</label>
                                <br>
                                <img src="{{ asset('public/storage/guarantor_photos/' . $guarantor->guarantor_nrc_front_photo) }}"
                                    width='300px' height="180px" alt="" title="" class="rounded">
                            </div>
                        </div>
                    @endif
                    @if ($guarantor->guarantor_nrc_back_photo)
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="font-weight-bold">NRC Back Photo</label>
                                <br>
                                <img src="{{ asset('public/storage/guarantor_photos/' . $guarantor->guarantor_nrc_back_photo) }}"
                                    width='300px' height="180px" alt="" title="" class="rounded">
                            </div>
                        </div>
                    @endif
                    @if ($guarantor->nrc_recommendation)
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="font-weight-bold">NRC Recommendation</label>
                                <br>
                                <img src="{{ asset('public/storage/guarantor_photos/' . $guarantor->nrc_recommendation) }}"
                                    width='300px' height="180px" alt="" title="" class="rounded">
                            </div>
                        </div>
                    @endif
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label for="" class="font-weight-bold">KYC Photo</label>
                            <br>
                            <img src="{{ asset('public/storage/guarantor_photos/' . $guarantor->kyc_photo) }}"
                                width='300px' height="180px" alt="" title="" class="rounded">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
