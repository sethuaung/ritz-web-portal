@extends('layouts.app')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                
                <div>Business Category Edit          
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('business_categories/')}}" class="btn theme-color text-white">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>                    
                </div>
            </div>    
        </div>
    </div>            
    <div class="main-card mb-3 card">
        <div class="card-body">
            @if ($errors->any())
				<div class="alart alart-danger">
					<ul>
						@foreach($errors->all() as $error)
						<li>{{$error}}</li>
						@endforeach
					</ul>
				</div>
			@endif    
            <form class="needs-validation" method="post" action="{{route('business_categories.update',$business_categories->id)}}" enctype="multipart/form-data" novalidate>
            @csrf
            @method('PUT')
            <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Choose Business Type (Eng) <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                    <select name="business_type_id" id="business_type_id" class="form-control" required>
                        @foreach ($business_types as $business_type) 
                        <option value="{{ $business_type->id }}" @if($business_categories->business_type_id == $business_type->id) selected="selected" @endif >{{ $business_type->business_type_name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div><br>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Business Category Name (Eng)<span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="name" name="business_category_name"  value="{{$business_categories->business_category_name}}" required>                   
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('business_category_name')}}                                        
                        </div>
                    </div>
                </div><br>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom03">Business Category Name (MM) <span style="color: red">*</span></label>                        
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="name_mm" name="business_category_name_mm" value="{{$business_categories->business_category_name_mm}}" required> 
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('business_category_name_mm')}}                                        
                        </div>
                    </div>
                </div>
                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                    <a href="{{url('business_categories/')}}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
            </form>
        </div>
    </div>
    
</div>
@endsection