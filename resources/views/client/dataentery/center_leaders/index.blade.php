@extends('layouts.app')
@section('content')

    <div class="col-md-12">
        <div class="mb-3"></div>
        <div class="main-card mb-3 card">
            <div class="card-header">Centers List

                <div class="btn-actions-pane-right d-flex align-items-center">
                    <form method="GET" >
                        <div class="input-group">
                            <input type="text" name="search_leader_list" id="search_leader_list"
                                value="{{ request()->get('search_leader_list') }}"
                                class="form-control mr-1"
                                placeholder="Search..."
                                aria-label="Search"
                                aria-describedby="button-addon2"
                                >

                            <button class="btn theme-color text-light mr-1" type="submit" id="button-addon2">
                                <i class="fa fa-search"></i>
                            </button>

                            <a href="{{url('center_leaders')}}" class="btn theme-color text-light">
                                <i class="fa fa-refresh"></i>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th>Portal Client ID</th>
							<th>Main Client ID</th>
                            <th>Client Name</th>
                            <th>Branch</th>
                            <th>Portal Center ID</th>
							<th>Main Center ID</th>
                        </tr>
                    </thead>

                    <tbody>
                    @php $i=1; @endphp
                    @foreach($data as $d)
                    	<tr>
                        	<td class="text-center text-muted">{{ $i++ }}</td>
                    		<td>{{$d->client_uniquekey}}</td>
                    		<td>{{$d->main_client_code}}</td>
                    		<td>{{$d->name}}</td>
                    		<td>{{DB::table('tbl_branches')->where('id',$d->branch_id)->first()->branch_name}}</td>
                    		<td>{{$d->center_id}}</td>
                    		<td>{{$d->main_center_code}}</td>
                    	</tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-block text-center card-footer">
                <div class="">
                    {{ $data->appends($_GET)->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
                    
@if(session('successMsg')!=NULL)
@section('script')
    <script>
        statusAlert("{{session('successMsg')}}");
    </script>
@endsection
@endif
