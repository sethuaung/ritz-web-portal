@extends('layouts.app')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Staff Details View         
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('staffs/')}}" class="btn theme-color">
                        <i class="pe-7s-back btn-icon-wrapper text-light">Back To List</i>
                    </a>                    
                </div>
            </div>    
        </div>
    </div>            
    <div class="main-card mb-3 card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header theme-color">
                            <h5 class="text-light">
                                <i class="fas fa-info-circle"></i>
                                <span class="ml-2">Staff Info</span>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-row">
                                        <div class="col-md-12 mb-1">
                                            {{-- <label for="validationCustom01">Photo</label><br> --}}
                                            @if($staff->photo)
												<img src="{{ asset('/storage/app/public/clientphotos/'.$staff->photo) }}" width="200px" height="200px" alt="" title="" class="rounded-circle border-secondary ml-3 mt-2" style="border: 3px solid; object-fit:cover">
                                        	@else
    											<img src="{{ asset('storage/app/public/clientphotos/default_user_icon.jpg') }}" width="200px" height="200px" alt="" title="" class="rounded-circle border-secondary ml-3 mt-2" style="border: 3px solid; object-fit:cover">

											@endif
                                            
                                        </div>
                                        {{-- <div class="col-md-12 mb-9">
                                            <label for="validationCustom01">Finger Biometric</label><br>
                                            <img src="{{ asset('/storage/app/public/clientphotos/'.$staff->finger_biometric) }}" width="200px" height="130px" alt="" title="" class="rounded">
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-row">
                                        <div class="col-md-5 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Branch</label>
                                        </div>
                                        <div  class="col-md-7 mb-9 ">
                                        : {{$staff->branch_name }}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-5 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Staff ID</label>
                                        </div>
                                        <div  class="col-md-7 mb-9 ">
                                            : {{$staff->staff_code}}
                                        </div>
                                    </div> 
                                    <div class="form-row">
                                        <div class="col-md-5 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Name</label>
                                        </div>
                                        <div  class="col-md-7 mb-9 ">
                                        : {{$staff->name}}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-5 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Role</label>
                                        </div>
                                        <div  class="col-md-7 mb-9 ">
                                        : {{$staff->role}}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-5 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Father Name</label>
                                        </div>
                                        <div  class="col-md-7 mb-9 ">
                                        : {{$staff->father_name }}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-5 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Nationality ID</label>
                                        </div>
                                        <div  class="col-md-7 mb-9 ">
                                        : {{$staff->nrc }}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-5 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Phone</label>
                                        </div>
                                        <div  class="col-md-7 mb-9 ">
                                        : {{$staff->phone}}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-5 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Email</label>
                                        </div>
                                        <div  class="col-md-7 mb-9 ">
                                        : {{$staff->email }}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-5 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Email Verified Time</label>
                                        </div>
                                        <div  class="col-md-7 mb-9 ">
                                        : {{$staff->email_verified_at }}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-5 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Full Address</label>
                                        </div>
                                        <div  class="col-md-7 mb-9 ">
                                        : {{$staff->address }}
                                        </div>
                                    </div>
        
                                    <div class="form-row">
                                        <div class="col-md-5 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">User Account</label>
                                        </div>
                                        <div  class="col-md-7 mb-9 ">
                                        : {{$staff->user_acc }}
                                        </div>
                                    </div>
                                    {{-- <div class="form-row" hidden>
                                        <div class="col-md-5 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">User Password</label>
                                        </div>
                                        <div  class="col-md-7 mb-9 ">
                                        {{$staff->user_password }}
                                        </div>
                                    </div>
                               		<div class="form-row" hidden>
                                        <div class="col-md-5 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">User Token</label>
                                        </div>
                                        <div  class="col-md-7 mb-9 ">
                                        {{$staff->user_token}}
                                        </div>
                                    </div> --}}
									
									@if($staff->center_leader_id)
                                    	<div class="form-row">
                                        	<div class="col-md-5 mb-3">
                                            	<label for="validationCustom01" class="font-weight-bold">Center Leader</label>
                                        	</div>
                                        	<div  class="col-md-7 mb-9 ">
                                        	: {{$staff->center_leader_id }}
                                        	</div>
                                    	</div>
                                    @endif
                                </div>
                                
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <h5 class="menu-header-title text-uppercase mb-4 fsize-2 text-secondary font-weight-bold">Finger Biometric</h5>
                    {{-- <label for="validationCustom01">Finger Biometric</label><br> --}}
                @if($staff->finger_biometric)
					<img src="{{ asset('/storage/app/public/clientphotos/'.$staff->finger_biometric) }}" width="200px" alt="" title="" class="rounded">
                @else
    				<img src="{{ asset('storage/app/public/clientphotos/No_Image_Available.jpg') }}" width="200px" height="200px" alt="" title="" class="rounded-circle border-secondary ml-3 mt-2" style="border: 3px solid; object-fit:cover">
				@endif
                    </div>
                {{-- <div class="col-md-4">
                    <div class="form-row">
                        <div class="col-md-12 mb-9">
                            <label for="validationCustom01">Photo</label><br>
                            <img src="{{ asset('/storage/app/public/clientphotos/'.$staff->photo) }}" width="300px" height="180px" alt="" title="">
                        </div>
                        <div class="col-md-12 mb-9">
                            <label for="validationCustom01">Finger Biometric</label><br>
                            <img src="{{ asset('/storage/app/public/clientphotos/'.$staff->finger_biometric) }}" width="300px" height="180px" alt="" title="">
                        </div>
                    </div>
                        
                </div> --}}
            </div>
            {{-- <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Staff Code</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                    {{$staff->staff_code}}
                </div>
            </div>

             <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Branch</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$staff->branch_name }}
                </div>
            </div> 
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Finger Biometric</label>
                </div>
                <div class="col-md-9 mb-9">
                    <img src="{{ asset('/storage/app/public/clientphotos/'.$staff->finger_biometric) }}" width="100px" height="100px" alt="" title="">
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Name</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$staff->name}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Phone</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$staff->phone}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Father Name</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$staff->father_name }}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Nationality ID</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$staff->nrc }}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Full Address</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$staff->full_address }}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Email</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$staff->email }}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Email Verified Time</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$staff->email_verified_at }}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">User Account</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$staff->user_acc }}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">User Password</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$staff->user_password }}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Photo</label>
                </div>
                <div class="col-md-9 mb-9">
                    <img src="{{ asset('/storage/app/public/clientphotos/'.$staff->photo) }}" width="100px" height="100px" alt="" title="">
                    </div>
            </div>
           
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Center Leader</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$staff->center_leader_id }}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">User Token</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$staff->user_token}}
                </div>
            </div> --}}
        </div>
    </div>
    
</div>
@endsection