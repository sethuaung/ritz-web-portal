@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Staff Create
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{ url('staffs') }}" class="btn theme-color text-white">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alart alart-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="needs-validation" method="POST" action="{{ route('staffs.update', $staff->staff_code) }}"
                    enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="position-relative form-group form-row">

                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Branch</label>
                            <select name="branch_id[]" class="form-control" multiple>
                                <option value="" data-address="">Choose Branch</option>
                                @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}" data-address="{{ $branch->branch_code }}"
                                        @if (in_array($branch->id, $user_branches)) selected @endif>
                                        {{ $branch->branch_name }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('branch_id') }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Staff Code</label>
                            <input type="text" class="form-control" id="staff_code" name="staff_code"
                                value="{{ $staff->staff_code }}" placeholder="Example : KTD-0004" readonly>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('staff_code') }}
                            </div>
                        </div>

                    </div>

                    <div class="position-relative form-group form-row">

                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Name</label>
                            <input type="text" autofocus class="form-control" id="name" name="name"
                                value="{{ $staff->name }}" placeholder="Mg Mg">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('name') }}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Phone Number</label>
                            <input type="number" class="form-control" id="name" pattern="[0-9.]+" name="phone"
                                value="{{ $staff->phone }}" placeholder="091234567"
                                oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                maxlength="11">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('phone') }}
                            </div>
                        </div>
                    </div>

                    <div class="position-relative form-group form-row">
                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Father Name</label>
                            <input type="text" class="form-control" id="name" name="father_name"
                                value="{{ $staff->father_name }}" placeholder="U Hla">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('father_name') }}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">NRC Number
                                <span class="text-black-50"></span></label>
                            <input type="text" value="{{ $staff->nrc }}" class="form-control" id="nrc">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('nrc_4') }}
                            </div>
                        </div>
                    </div>

                    <div class="position-relative form-group form-row">

                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Email</label>
                            <input type="text" class="form-control" id="name" name="email"
                                value="{{ $staff->email }}" placeholder="example@gmail.com" readonly>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('email') }}
                            </div>
                        </div>

                        <div class="col-md-6" style="display: none">
                            <label style="font-weight: 600" for="validationCustom01">Email Verified Time</label>
                            <input type="text" disabled class="form-control" id="name" name="email_verified_at"
                                value="{{ old('email_verified_at') }}" placeholder="">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('email_verified_at') }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom03">Finger Biometric </label>
                            <input type="file" class="form-control" id="finger_biometric" name="finger_biometric">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('finger_biometric') }}
                            </div>
                            @if ($staff->finger_biometric)
                                <img src="{{ asset('public/clientphotos/' . $staff->finger_biometric) }}" class="rounded"
                                    alt="">
                            @endif
                        </div>

                        {{-- <div class="col-md-3">
                            <label style="font-weight: 600" for="validationCustom01">User Account <span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="name" name="user_acc"
                                value="{{ old('user_acc') }}" placeholder="TestUser1" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('user_acc') }}
                            </div>
                        </div> --}}

                    </div>

                    <div class="position-relative form-group form-row">

                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">User Password</label>
                            <input type="text" class="form-control" id="name" name="user_password"
                                value="{{ old('user_password') }}" placeholder="asd123!@#">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('user_password') }}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Password Confirmation</label>
                            <input type="text" class="form-control" id="name" name="user_password_confirmation"
                                value="{{ old('user_password_confirmation') }}" placeholder="Rewrite your password">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('user_password_confirmation') }}
                            </div>
                        </div>

                    </div>

                    <div class="position-relative form-group form-row">
                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Photo</label>
                            <input type="file" class="form-control" id="photo" name="photo">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('photo') }}
                            </div>
                            @if ($staff->photo)
                                <img src="{{ asset('public/clientphotos/' . $staff->photo) }}" class="rounded"
                                    alt="">
                            @endif
                        </div>

                        <div class="col-md-6" hidden>
                            <label style="font-weight: 600" for="validationCustom01">Center Leader</label>
                            <select name="center_leader_id" id="center_leader_id" class="form-control"
                                value="{{ old('center_leader_id') }}">
                                <option value="">Choose Center</option>
                                @foreach ($centers as $center)
                                    <option value="{{ $center->center_uniquekey }}">{{ $center->center_uniquekey }}
                                    </option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('center_leader_id') }}
                            </div>
                        </div>

                        {{-- <div class="col-md-3">
                            <label style="font-weight: 600" for="validationCustom01">User Token <span style="color: red">*</span></label>
                                <input type="text" class="form-control" id="name" name="user_token" value="{{ old('user_token') }}" placeholder="">
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('user_token')}}                                        
                            </div>
                        </div> --}}
                    </div>
                    {{-- <div class="position-relative form-group form-row">
                        <div class="col-md-12">
                            <label style="font-weight: 600" for="validationCustom01">Full Address <span
                                    style="color: red">*</span></label>
                            <textarea class="form-control" id="name" name="full_address"
                                placeholder="No(1), 18th Street, Latha Township, Yangon" required>{{ old('full_address') }}</textarea>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('full_address') }}
                            </div>
                        </div>
                    </div> --}}
                    <div>
                        <h6 class="mb-3 font-weight-bold">Roles</h6>
                        <input type="checkbox" id="uncheckRoles"><label class="text-danger ml-1 font-weight-bold" for="uncheckRoles" style="user-select: none;">Remove role *</label>
                        <div class="row mt-2">
                            @foreach ($roles as $role)
                                <div class="col-4 mb-3">
                                    <input type="radio" class="role-check" value="{{ $role->id }}"
                                        id="{{ $role->name }}" name="role"
                                        @if ($staff->role_id == $role->id) checked @endif>
                                    <label for="{{ $role->name }}"
                                        style="user-select: none">{{ $role->name }}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <!-- permissions -->
                    @php
                        $role_permissions = DB::table('role_has_permissions')
                            ->leftJoin('permissions', 'permissions.id', '=', 'role_has_permissions.permission_id')
                            ->where('role_has_permissions.role_id', $staff->role_id)
                            ->select('permissions.id')
                            ->pluck('permissions.id')
                            ->toArray();
                        
                        // array_push($permissions, $extra_permissions);
                        
                    @endphp

                    <div>
                        <h6 class="mb-3 font-weight-bold">Dashboard</h6>
                        <!-- manage client -->
                        <p class="mb-3  text-danger" style="font-size: 12px">DASHBOARD</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'DASHBOARD')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check"
                                            value="{{ $permission->id }}" id="{{ $permission->name }}"
                                            @if (in_array($permission->id, $role_permissions)) checked disabled @endif @if(in_array($permission->id, $extra_permissions)) checked @endif>
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <h6 class="mb-3 font-weight-bold">Permissions</h6>
                        <!-- manage client -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE CLIENT</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE CLIENT')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check"
                                            value="{{ $permission->id }}" id="{{ $permission->name }}"
                                            @if (in_array($permission->id, $role_permissions)) checked disabled @endif @if(in_array($permission->id, $extra_permissions)) checked @endif>
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage loan -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE LOAN</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE LOAN')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check"
                                            value="{{ $permission->id }}" id="{{ $permission->name }}"
                                            @if (in_array($permission->id, $role_permissions)) checked disabled @endif @if(in_array($permission->id, $extra_permissions)) checked @endif>
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage payment -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE PAYMENT</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE PAYMENT')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check"
                                            value="{{ $permission->id }}" id="{{ $permission->name }}"
                                            @if (in_array($permission->id, $role_permissions)) checked disabled @endif @if(in_array($permission->id, $extra_permissions)) checked @endif>
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- compulsory saving -->
                        <p class="mb-3  text-danger" style="font-size: 12px">COMPULSORY SAVING</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'COMPULSORY SAVING')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check"
                                            value="{{ $permission->id }}" id="{{ $permission->name }}"
                                            @if (in_array($permission->id, $role_permissions)) checked disabled @endif @if(in_array($permission->id, $extra_permissions)) checked @endif>
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage saving -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE SAVING</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE SAVING')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check"
                                            value="{{ $permission->id }}" id="{{ $permission->name }}"
                                            @if (in_array($permission->id, $role_permissions)) checked disabled @endif @if(in_array($permission->id, $extra_permissions)) checked @endif>
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage transfer -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE TRANSFER</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE TRANSFER')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check"
                                            value="{{ $permission->id }}" id="{{ $permission->name }}"
                                            @if (in_array($permission->id, $role_permissions)) checked disabled @endif @if(in_array($permission->id, $extra_permissions)) checked @endif>
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage product -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE PRODUCT</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE PRODUCT')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check"
                                            value="{{ $permission->id }}" id="{{ $permission->name }}"
                                            @if (in_array($permission->id, $role_permissions)) checked disabled @endif @if(in_array($permission->id, $extra_permissions)) checked @endif>
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage accounting -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE ACCOUNTING</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE ACCOUNTING')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check"
                                            value="{{ $permission->id }}" id="{{ $permission->name }}"
                                            @if (in_array($permission->id, $role_permissions)) checked disabled @endif @if(in_array($permission->id, $extra_permissions)) checked @endif>
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage capital -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE CAPITAL</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE CAPITAL')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check"
                                            value="{{ $permission->id }}" id="{{ $permission->name }}"
                                            @if (in_array($permission->id, $role_permissions)) checked disabled @endif @if(in_array($permission->id, $extra_permissions)) checked @endif>
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- reports -->
                        <p class="mb-3  text-danger" style="font-size: 12px">REPORTS</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'REPORTS')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check"
                                            value="{{ $permission->id }}" id="{{ $permission->name }}"
                                            @if (in_array($permission->id, $role_permissions)) checked disabled @endif @if(in_array($permission->id, $extra_permissions)) checked @endif>
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage user -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE USER</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE USER')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check"
                                            value="{{ $permission->id }}" id="{{ $permission->name }}"
                                            @if (in_array($permission->id, $role_permissions)) checked disabled @endif @if(in_array($permission->id, $extra_permissions)) checked @endif>
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- general setting -->
                        <p class="mb-3  text-danger" style="font-size: 12px">GENERAL SETTING</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'GENERAL SETTING')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check"
                                            value="{{ $permission->id }}" id="{{ $permission->name }}"
                                            @if (in_array($permission->id, $role_permissions)) checked disabled @endif @if(in_array($permission->id, $extra_permissions)) checked @endif>
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- other -->
                        <p class="mb-3  text-danger" style="font-size: 12px">OTHER</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'OTHER')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check"
                                            value="{{ $permission->id }}" id="{{ $permission->name }}"
                                            @if (in_array($permission->id, $role_permissions)) checked disabled @endif @if(in_array($permission->id, $extra_permissions)) checked @endif>
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- reprot -->
                        <p class="mb-3  text-danger" style="font-size: 12px">REPROT</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'REPROT')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check"
                                            value="{{ $permission->id }}" id="{{ $permission->name }}"
                                            @if (in_array($permission->id, $role_permissions)) checked disabled @endif @if(in_array($permission->id, $extra_permissions)) checked @endif>
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                    </div>

                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save &
                            Back </i></button>
                    <a href="{{ url('staffs/') }}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
                    <!-- <input type="reset" value="reset"> -->
                </form>
            </div>
        </div>

    </div>
@endsection
