@extends('layouts.app')
@section('content')
    <div class="col-md-12">
        <div class="mb-3"></div>
        <div class="main-card mb-3 card">
            <div class="card-header">Staff List

                <div class="btn-actions-pane-right d-flex align-items-center">
                    <a href="{{ url('staffs/create') }}">
                        <button type="button" title="" data-placement="bottom"
                            class="btn-shadow mr-1 btn theme-color text-white">
                            <i class="fa fa-plus"></i>
                            Add
                        </button>
                    </a>
                    <form method="GET">
                        <div class="input-group">
                            <input type="text" name="search_value" id="search_value"
                                value="{{ request()->get('search_value') }}" class="form-control mr-1"
                                placeholder="Search..." aria-label="Search" aria-describedby="button-addon2">

                            <button class="btn theme-color text-light mr-1" type="submit" id="button-addon2">
                                <i class="fa fa-search"></i>
                            </button>

                            <a href="{{ url('staffs') }}" class="btn theme-color text-light">
                                <i class="fa fa-refresh"></i>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
            {{-- @if (session('successMsg') != null)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('successMsg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif --}}
            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Portal Staff ID</th>
                            <th>Main Staff ID</th>
                            <th>Name</th>
                            <th>Phone Number</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Branch</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>

                    @php $i=1; @endphp
                    @foreach ($staffs as $staff)
                        <tr>
                            <td class="text-center text-muted">{{ $i++ }}</td>
                            <td>{{ $staff->staff_code }}</td>
                            <td>
                                @if (DB::table('tbl_main_join_staff')->where('portal_staff_id', $staff->staff_code)->first())
                                    {{ DB::table('tbl_main_join_staff')->where('portal_staff_id', $staff->staff_code)->first()->main_staff_code }}
                                    {{--
                    {{ DB::connection('main')->table('users')->where('id', DB::table('tbl_main_join_staff')->where('portal_staff_id', $staff->staff_code)->first()->main_staff_id)->first()->user_code }}
                    --}}
                                @endif
                            </td>
                            <td>{{ $staff->name }}</td>
                            <td>{{ $staff->phone }}</td>
                            <td>{{ $staff->email }}</td>
                            <td>{{ $staff->role }}</td>
                            <td class="text-nowrap">
                                {{ DB::table('tbl_branches')->where('id', $staff->branch_id)->first() == null ? '' : DB::table('tbl_branches')->where('id', $staff->branch_id)->first()->branch_name }}</td>
                            <td class="text-center text-nowrap">
                                <a href="staffs/view/{{ $staff->staff_code }}">
                                    <button type="button" id="PopoverCustomT-1"
                                        class="border-0 btn-transition btn  btn-outline-info btn-sm"><i
                                            class="fa fa-eye"></i></button>
                                </a>

                                <a href="staffs/{{ $staff->staff_code }}/edit">
                                    <button type="button" id="PopoverCustomT-1"
                                        class="border-0 btn-transition btn btn-outline-warning"><i
                                            class="fa fa-pencil"></i></button>
                                </a>
                                <form action="{{ url('staffs/destroy', ['id' => $staff->staff_code]) }}" method="POST"
                                    class="d-inline-block" onsubmit="return confirm('Are you sure want to delete?')">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" id="PopoverCustomT-1"
                                        class="border-0 btn-transition btn btn-outline-danger"><i class="fa fa-trash">
                                        </i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-block text-center card-footer">
                <div class="">
                    {{ $staffs->appends($_GET)->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@if (session('successMsg') != null)
    @section('script')
        <script>
            statusAlert("{{ session('successMsg') }}");
        </script>
    @endsection
@endif
