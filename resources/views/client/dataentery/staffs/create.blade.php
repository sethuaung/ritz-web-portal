@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Staff Create
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{ url('staffs') }}" class="btn theme-color text-white">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alart alart-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="needs-validation" method="POST" action="{{ route('staffs.store') }}"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="position-relative form-group form-row">

                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Branch</label>
                            <select name="branch_id[]" id="branch_id" class="form-control" 
                                multiple>
                                <option value="" data-address="">Choose Branch</option>
                                @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}" data-address="{{ $branch->branch_code }}">
                                        {{ $branch->branch_name }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('branch_id') }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Staff Code <span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="staff_code" name="staff_code"
                                value="{{ old('staff_code') }}" placeholder="Example : KTD-0004" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('staff_code') }}
                            </div>
                        </div>

                    </div>

                    <div class="position-relative form-group form-row">

                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Name <span
                                    style="color: red">*</span></label>
                            <input type="text" autofocus class="form-control" id="name" name="name"
                                value="{{ old('name') }}" placeholder="Mg Mg" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('name') }}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Phone Number <span
                                    style="color: red">*</span></label>
                            <input type="number" class="form-control" id="name" pattern="[0-9.]+" name="phone"
                                value="{{ old('phone') }}" placeholder="091234567"
                                oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                maxlength="11" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('phone') }}
                            </div>
                        </div>
                    </div>

                    <div class="position-relative form-group form-row">
                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Father Name <span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="name" name="father_name"
                                value="{{ old('father_name') }}" placeholder="U Hla" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('father_name') }}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">NRC Number <span
                                    style="color: red">*</span></label>
                            <div class="position-relative form-group form-row">
                                <div class="col-md-2">
                                    <select name="nrc_1" id="nrc_1" class="form-control">
                                        <option value=""></option>
                                        @for ($i = 1; $i <= 14; $i++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select name="nrc_2" id="nrc_2" class="form-control">
                                        <option value=""></option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select type="select" id="nrc_3" name="nrc_3" class="form-control">
                                        <option value="(N)">(N)</option>
                                        <option value="(C)">(C)</option>

                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <input type="number" class="form-control" id="nrc_4" name="nrc_4"
                                        value="{{ old('nrc_4') }}" placeholder=""
                                        oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                        maxlength="6" required>
                                </div>
                            </div>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('nrc_4') }}
                            </div>
                        </div>
                    </div>

                    <div class="position-relative form-group form-row">

                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Email <span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="name" name="email"
                                value="{{ old('email') }}" placeholder="example@gmail.com" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('email') }}
                            </div>
                        </div>

                        <div class="col-md-6" style="display: none">
                            <label style="font-weight: 600" for="validationCustom01">Email Verified Time</label>
                            <input type="text" disabled class="form-control" id="name" name="email_verified_at"
                                value="{{ old('email_verified_at') }}" placeholder="">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('email_verified_at') }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom03">Finger Biometric </label>
                            <input type="file" class="form-control" id="finger_biometric" name="finger_biometric">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('finger_biometric') }}
                            </div>
                        </div>

                        {{-- <div class="col-md-3">
                            <label style="font-weight: 600" for="validationCustom01">User Account <span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="name" name="user_acc"
                                value="{{ old('user_acc') }}" placeholder="TestUser1" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('user_acc') }}
                            </div>
                        </div> --}}

                    </div>

                    <div class="position-relative form-group form-row">

                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">User Password <span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="name" name="user_password"
                                value="{{ old('user_password') }}" placeholder="asd123!@#" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('user_password') }}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Password Confirmation <span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="name" name="user_password_confirmation"
                                value="{{ old('user_password_confirmation') }}" placeholder="Rewrite your password"
                                required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('user_password_confirmation') }}
                            </div>
                        </div>

                    </div>

                    <div class="position-relative form-group form-row">
                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">Photo <span
                                    style="color: red">*</span></label>
                            <input type="file" class="form-control" id="photo" name="photo" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('photo') }}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label style="font-weight: 600" for="validationCustom01">User Code <span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="user_code" name="user_code" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('user_code') }}
                            </div>
                        </div>

                        <div class="col-md-6" hidden>
                            <label style="font-weight: 600" for="validationCustom01">Center Leader</label>
                            <select name="center_leader_id" id="center_leader_id" class="form-control"
                                value="{{ old('center_leader_id') }}">
                                <option value="">Choose Center</option>
                                @foreach ($centers as $center)
                                    <option value="{{ $center->center_uniquekey }}">{{ $center->center_uniquekey }}
                                    </option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('center_leader_id') }}
                            </div>
                        </div>

                        {{-- <div class="col-md-3">
                            <label style="font-weight: 600" for="validationCustom01">User Token <span style="color: red">*</span></label>
                                <input type="text" class="form-control" id="name" name="user_token" value="{{ old('user_token') }}" placeholder="">
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('user_token')}}                                        
                            </div>
                        </div> --}}
                    </div>
                    {{-- <div class="position-relative form-group form-row">
                        <div class="col-md-12">
                            <label style="font-weight: 600" for="validationCustom01">Full Address <span
                                    style="color: red">*</span></label>
                            <textarea class="form-control" id="name" name="full_address"
                                placeholder="No(1), 18th Street, Latha Township, Yangon" required>{{ old('full_address') }}</textarea>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('full_address') }}
                            </div>
                        </div>
                    </div> --}}
                    <div>
                        <h6 class="mb-3 font-weight-bold">Roles</h6>
                        <div class="row">
                            @foreach ($roles as $role)
                                <div class="col-4 mb-3">
                                    <input type="radio" class="role-check" value="{{ $role->id }}"
                                        id="{{ $role->name }}" name="role">
                                    <label for="{{ $role->name }}"
                                        style="user-select: none">{{ $role->name }}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <div>
                        <h6 class="mb-3 font-weight-bold">Dashboard</h6>
                        <!-- manage client -->
                        <p class="mb-3  text-danger" style="font-size: 12px">DASHBOARD</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'DASHBOARD')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check" value="{{ $permission->id }}"
                                            id="{{ $permission->name }}">
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <h6 class="mb-3 font-weight-bold">Permissions</h6>
                        <!-- manage client -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE CLIENT</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE CLIENT')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check" value="{{ $permission->id }}"
                                            id="{{ $permission->name }}">
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage loan -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE LOAN</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE LOAN')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check" value="{{ $permission->id }}"
                                            id="{{ $permission->name }}">
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage payment -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE PAYMENT</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE PAYMENT')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check" value="{{ $permission->id }}"
                                            id="{{ $permission->name }}">
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- compulsory saving -->
                        <p class="mb-3  text-danger" style="font-size: 12px">COMPULSORY SAVING</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'COMPULSORY SAVING')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check" value="{{ $permission->id }}"
                                            id="{{ $permission->name }}">
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage saving -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE SAVING</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE SAVING')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check" value="{{ $permission->id }}"
                                            id="{{ $permission->name }}">
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage transfer -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE TRANSFER</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE TRANSFER')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check" value="{{ $permission->id }}"
                                            id="{{ $permission->name }}">
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage product -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE PRODUCT</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE PRODUCT')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check" value="{{ $permission->id }}"
                                            id="{{ $permission->name }}">
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage accounting -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE ACCOUNTING</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE ACCOUNTING')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check" value="{{ $permission->id }}"
                                            id="{{ $permission->name }}">
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage capital -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE CAPITAL</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE CAPITAL')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check" value="{{ $permission->id }}"
                                            id="{{ $permission->name }}">
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- reports -->
                        <p class="mb-3  text-danger" style="font-size: 12px">REPORTS</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'REPORTS')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check" value="{{ $permission->id }}"
                                            id="{{ $permission->name }}">
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- manage user -->
                        <p class="mb-3  text-danger" style="font-size: 12px">MANAGE USER</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'MANAGE USER')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check" value="{{ $permission->id }}"
                                            id="{{ $permission->name }}">
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- general setting -->
                        <p class="mb-3  text-danger" style="font-size: 12px">GENERAL SETTING</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'GENERAL SETTING')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check" value="{{ $permission->id }}"
                                            id="{{ $permission->name }}">
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- other -->
                        <p class="mb-3  text-danger" style="font-size: 12px">OTHER</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'OTHER')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check" value="{{ $permission->id }}"
                                            id="{{ $permission->name }}">
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                        <!-- reprot -->
                        <p class="mb-3  text-danger" style="font-size: 12px">REPROT</p>
                        <div class="row">
                            @foreach ($permissions as $permission)
                                @if ($permission->group_name == 'REPROT')
                                    <div class="col-4 mb-3">
                                        <input type="checkbox" name="permissions[]" class="permission-check" value="{{ $permission->id }}"
                                            id="{{ $permission->name }}">
                                        <label for="{{ $permission->name }}"
                                            style="user-select: none">{{ $permission->name }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                    </div>

                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save &
                            Back </i></button>
                    <a href="{{ url('staffs/') }}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
                    <!-- <input type="reset" value="reset"> -->
                </form>
            </div>
        </div>

    </div>
@endsection
