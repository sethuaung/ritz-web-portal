@extends('layouts.app')
@section('content')

    <div class="col-md-12">
        <div class="mb-3"></div> 
        <div class="main-card mb-3 card">
            <div class="card-header">Township List
            
                <div class="btn-actions-pane-right">
                    <a href="{{url('townships/create')}}">
                        <button type="button" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-white">
                            <i class="fa fa-plus"></i> 
                            Add
                        </button> 
                    </a>
                </div>
            </div>
            @if(session('successMsg')!=NULL)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('successMsg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>District Name</th>
                            <th>Township Name (Eng)</th>
                            <th>Township Name (MM)</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    
                    @php $i=1; @endphp
                    @foreach($townships as $township)
                        <tr>
                            <td class="text-center text-muted">{{$i++}}</td>
                            <td >{{$township["district_id"]}}</td>
                            <td>{{$township["township_name"]}}</td>
                            <td>{{$township["township_name_mm"]}}</td>
                            
                            <td class="text-center">                                
                                <a href="townships/view/{{$township['id']}}">
                                    <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn  btn-outline-info btn-sm"><i class="fa fa-eye"></i></button>
                                </a>
                                                                   
                                <a href="townships/{{$township['id']}}/edit">
                                    <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-warning"><i class="fa fa-pencil" ></i></button>
                                </a>
                                <form action="{{ url('townships/destroy',['id'=>$township['id']]) }}" method="POST" class="d-inline-block" onsubmit="return confirm('Are you sure want to delete?')">
                                    @csrf
                                    @method('DELETE')                                       
                                    <button type="submit" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-danger"><i class="fa fa-trash"> </i></button>                                        
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-block text-center card-footer">
                <div class="">
                    {{$results->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection