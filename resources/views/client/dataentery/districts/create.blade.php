@extends('layouts.app')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                
                <div>District Create          
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('districts/')}}" class="btn theme-color text-light">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>                    
                </div>
            </div>    </div>
    </div>            <div class="main-card mb-3 card">
        <div class="card-body">
            @if ($errors->any())
				<div class="alart alart-danger">
					<ul>
						@foreach($errors->all() as $error)
						<li>{{$error}}</li>
						@endforeach
					</ul>
				</div>
			@endif    
            <form class="needs-validation" method="post" action="{{route('districts.store')}}" enctype="multipart/form-data">
            @csrf
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Division Name  <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                    <select class=" form-control" name="division_id" id="division_id" required>
                        <option value="">Choose Division</option>
                        @foreach ($divisions as $division)
                        <option value="{{$division->id}}" >
                            {{ $division->division_name }} 
                        </option>               
                        @endforeach
                    </select> 
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">District Name(Eng)  <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="name" name="district_name" placeholder="Example= Yangon" value="{{ old('district_name') }}"  required>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('district_name')}}                                        
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom03">District Name(MM)  <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="name_mm" name="district_name_mm" placeholder=" ဥပမာ= ရန်ကုန်" value="{{ old('district_name_mm') }}"  required>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('district_name_mm')}}                                        
                        </div>
                    </div>
                </div>
                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                    <a href="{{url('districts/')}}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
            </form>
        </div>
    </div>
    
</div>
@endsection