@extends('layouts.app')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Village Create          
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('villages/')}}" class="btn theme-color text-white">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>                    
                </div>
            </div>    
        </div>
    </div>            
<div class="main-card mb-3 card">
    <div class="card-body">
        @if ($errors->any())
            <div class="alart alart-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif    
        
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-link active" id="nav-home-tab" data-toggle="tab" href="#township" role="tab" aria-controls="nav-home" aria-selected="true">Township</a>
                <a class="nav-link" id="nav-profile-tab" data-toggle="tab" href="#province" role="tab" aria-controls="nav-profile" aria-selected="false">Province</a>

            </div>
        </nav>
            <div class="form-row">
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="township" role="tabpanel" aria-labelledby="nav-home-tab">
                        <form class="needs-validation" method="post" action="{{route('villages.store')}}" enctype="multipart/form-data" novalidate>
                        @csrf
                            <div class="form-group row">
                                <div class="col-md-3 mb-3">
                                    <label for="validationCustom01">Township Name</label>
                                </div>
                                <div class="col-md-9">
                                    <select class=" form-control" name="township_province_id" id="township_id">
                                        <option selected>Choose Township</option>
                                        @foreach ($townships as $township)
                                        <option value="{{$township->id}}" >
                                            {{ $township->township_name }} 
                                        </option>               
                                        @endforeach
                                    </select> 
                                </div>
                                <div class="col-md-3 mb-3">
                                <label for="validationCustom01">Type Status</label>
                                </div>
                                <div class="col-md-9 mb-9">
                                    <select class=" form-control" name="sub_status" id="">
                                        <option selected value="township">Township</option>
                                        
                                    </select>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('village_name')}}                                        
                                    </div>
                                </div>
                                
                                <div class="col-md-3 mb-3">
                                    <label for="validationCustom01">Village Name(Eng)</label>
                                </div>
                                <div class="col-md-9 mb-9">
                                    <input type="text" class="form-control" id="name" name="village_name"  value="" required>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('village_name')}}                                        
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="validationCustom03">Village Name(MM)</label>
                                </div>
                                <div class="col-md-9 mb-9">
                                    <input type="text" class="form-control" id="name_mm" name="village_name_mm"  required>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('village_name_mm')}}                                        
                                    </div>
                                </div>
                                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                                    <a href="{{url('villages/')}}" class="btn btn-secondary m-1">
                                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                                    </a>
                            </div>
                        </form>
                    </div>
                
                    <div class="tab-pane fade" id="province" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <form class="needs-validation" method="post" action="{{route('villages.store')}}" enctype="multipart/form-data" novalidate>
                        @csrf
                            <div class="form-group row">
                                <div class="col-md-3 mb-3">
                                    <label for="validationCustom01">Province Name</label>
                                </div>
                                <div class="col-md-9">
                                    <select class=" form-control" name="township_province_id" id="province_id">
                                        <option selected>Choose Province</option>
                                        @foreach ($provinces as $province)
                                        <option value="{{$province->id}}" >
                                            {{ $province->province_name }} 
                                        </option>               
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3 mb-3">
                                <label for="validationCustom01">Type Status</label>
                                </div>
                                <div class="col-md-9 mb-9">
                                    <select class=" form-control" name="sub_status" id="province_id" selected>
                                        <option selected value="province" >Province</option>
                                    </select>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('village_name')}}                                        
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                <label for="validationCustom01">Village Name(Eng)</label>
                                </div>
                                <div class="col-md-9 mb-9">
                                    <input type="text" class="form-control" id="name" name="village_name"  value="" required>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('village_name')}}                                        
                                    </div>
                                </div>
                            
                            
                                <div class="col-md-3 mb-3">
                                    <label for="validationCustom03">Village Name(MM)</label>
                                </div>
                                <div class="col-md-9 mb-9">
                                    <input type="text" class="form-control" id="name_mm" name="village_name_mm"  required>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('village_name_mm')}}                                        
                                    </div>
                                </div>
                                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                                    <a href="{{url('villages/')}}" class="btn btn-secondary m-1">
                                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                                    </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection