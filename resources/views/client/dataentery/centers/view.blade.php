@extends('layouts.app')
@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Center Details View          
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('centers/')}}" class="btn theme-color text-white">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>                    
                </div>
            </div>    </div>
    </div>            <div class="main-card mb-3 card">
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Center Code</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                    : {{$center->center_uniquekey}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Staff</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                : {{ $staffName }}
              
                </div>
            </div>
            <div class="form-row d-none">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Center Leader</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                : {{ $centerleadername }}
              
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Branch</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                : {{$branchname}}
                </div>
            </div>
            @if($division)
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Division</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                : {{$division->name}}
                </div>
            </div>
            @endif
            @if($district)
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">District</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                : {{$district->name}}
                </div>
            </div>
            @endif
            @if($township)
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Township</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                : {{$township->name}}
                </div>
            </div>
            @endif
            @if($village)
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Village</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                : {{$village->name}}
                </div>
            </div>
            @endif
            @if($quarter)
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Quarter</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                : {{$quarter->name}}
                </div>
            </div>
            @endif
            @if($center->address)
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Address</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                : {{$center->address}}
                </div>
            </div>
            @endif
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Phone Number</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                : {{$center->phone_number}}
                </div>
            </div>
            <div class="form-row d-none">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Center Leader Type</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                : {{$center->type_status }}
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection