@extends('layouts.app')
@section('content')

    <div class="col-md-12">
        <div class="mb-3"></div>
        <div class="main-card mb-3 card">
            <div class="card-header">Centers List

                <div class="btn-actions-pane-right d-flex align-items-center">
                    <a href="{{url('centers/create')}}">
                        <button type="button" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-light">
                            <i class="fa fa-plus"></i>
                            Add
                        </button>
                    </a>
                    <form method="GET" >
                        <div class="input-group">
                            <input type="text" name="search_centerlist" id="search_centerlist"
                                value="{{ request()->get('search_centerlist') }}"
                                class="form-control mr-1"
                                placeholder="Search..."
                                aria-label="Search"
                                aria-describedby="button-addon2"
                                >

                            <button class="btn theme-color text-light mr-1" type="submit" id="button-addon2">
                                <i class="fa fa-search"></i>
                            </button>

                            <a href="{{url('centers')}}" class="btn theme-color text-light">
                                <i class="fa fa-refresh"></i>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
           {{--@if(session('successMsg')!=NULL)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('successMsg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif --}}
            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th>Portal Center ID</th>
            				<th>Main Center ID</th>
            				<th>Loan Officer</th>
            {{--<th>Center Leader</th>--}}
                            <th>Branch ID</th>
                            {{--<th>Center Leader Type</th>--}}
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                    @php $i=1; @endphp
                    @foreach($centers as $center)
                        <tr>
                            <td class="text-center text-muted">{{$i++}}</td>
                            @if($center["center_uniquekey"])
                    			<td>{{ $center["center_uniquekey"] }}</td>
                    		@else <td>Not Available</td>
                    		@endif
                    		<td>
                    			@if(DB::table('tbl_main_join_center')->where('portal_center_id', $center["center_uniquekey"])->first())
                    				{{ DB::table('tbl_main_join_center')->where('portal_center_id', $center["center_uniquekey"])->first()->main_center_code }}
                    			@endif
                    		</td>
                            <td>{{ DB::table('tbl_staff')->where('staff_code', $center["staff_id"])->first()->name }}</td>
                                {{--@if($center["staff_client_name"])
                                <td>{{ $center["staff_client_name"] }}</td>
                            @else
                                <td>Not Available</td> 
                            @endif--}}
                            <td>{{ $center["branch_id"] }}</td>
                            {{--<td>{{ $center["type_status"] }}</td>--}}

                            <td class="text-center">
                                <a href="centers/view/{{$center['center_uniquekey']}}">
                                    <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn  btn-outline-info btn-sm"><i class="fa fa-eye"></i></button>
                                </a>

                                <a href="centers/{{$center['center_uniquekey']}}/edit">
                                    <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-warning"><i class="fa fa-pencil" ></i></button>
                                </a>

                                <form action="{{ url('centers/destroy',['id'=>$center['center_uniquekey']]) }}" method="POST" class="d-inline-block" onsubmit="return confirm('Are you sure want to delete?')">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-danger"><i class="fa fa-trash"> </i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-block text-center card-footer">
                <div class="">
                    {{ $results->appends($_GET)->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
                    
@if(session('successMsg')!=NULL)
@section('script')
    <script>
        statusAlert("{{session('successMsg')}}");
    </script>
@endsection
@endif
