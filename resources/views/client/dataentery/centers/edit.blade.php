@extends('layouts.app')
@section('content')

    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Center Edit
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{ url('centers/') }}" class="btn theme-color text-white">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alart alart-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="needs-validation" method="post"
                    action="{{ route('centers.update', $center->center_uniquekey) }}" enctype="multipart/form-data"
                    novalidate>
                    @csrf
                    @method('PUT')
                    <div class="position-relative form-group form-row">

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Branch <span
                                    style="color: red">*</span></label>
                            <select name="branch_id" id="branch_id_center" class="form-control" required>

                                @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}" data-address="{{ $branch->branch_code }}"
                                        @if ($center->branch_id == $branch->id) selected="selected" @endif>
                                        {{ $branch->branch_name }}</option>
                                @endforeach
                            </select>

                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('branch_id') }}
                            </div>
                        </div>

                        {{-- staff start --}}
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Staff <span
                                    style="color: red">*</span></label>
                            <select name="staff_id" class="form-control" required>
                                @foreach ($staff as $s)
                                    <option value="{{ $s->staff_code }}"
                                        @if ($center->staff_id == $s->staff_code) selected="selected" @endif>{{ $s->name }}
                                    </option>
                                @endforeach
                            </select>

                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('staff_id') }}
                            </div>
                        </div>
                        {{-- staff end --}}

                        <div class="col-md-3 mb-3 d-none">
                            <label style="font-weight: 600" for="validationCustom01">Center Code <span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="center_uniquekey_edit" name="center_uniquekey"
                                value="{{ $center->center_uniquekey }}" readonly required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('center_uniquekey') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Division/State <span
                                    style="color: red">*</span></label>
                            <select type="select" id="division_id" name="division_id" class="form-control">
                                @foreach ($divisions as $division)
                                    <option value="{{ $division->code }}"
                                        {{ $center->division_id == $division->code ? 'selected' : '' }}>
                                        {{ $division->name }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('division_id') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">District <span
                                    style="color: red">*</span></label>
                            <select type="select" id="district_id" name="district_id" class="form-control">
                                @foreach ($districts as $district)
                                    <option value="{{ $district->code }}"
                                        {{ $center->district_id == $district->code ? 'selected' : '' }}>
                                        {{ $district->name }} </option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('district_id') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Township/Province <span
                                    style="color: red">*</span></label>
                            <select type="select" id="township_id" name="township_id" class="form-control">
                                @foreach ($townships as $township)
                                    <option value="{{ $township->code }}"
                                        {{ $center->township_id == $township->code ? 'selected' : '' }}>
                                        {{ $township->name }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('township_id') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-4">
                            <label style="font-weight: 600" for="validationCustom03">Village <span
                                    style="color: red">*</span></label>
                            <select type="select" id="village_id" name="village_id" class="form-control">
                                @foreach ($villages as $village)
                                    <option value="{{ $village->code }}"
                                        {{ $center->village_id == $village->code ? 'selected' : '' }}>
                                        {{ $village->name }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('village_id') }}
                            </div>
                        </div>


                        <div class="col-md-3 mb-4">
                            <label style="font-weight: 600" for="validationCustom03">Quarter</label>
                            <select type="select" id="ward_id" name="ward_id" class="form-control">
                                <option selected disabled>Select Quarter</option>
                                @foreach ($quarters as $quarter)
                                    <option value="{{ $quarter->code }}"
                                        {{ $center->quarter_id == $quarter->code ? 'selected' : '' }}>
                                        {{ $quarter->name }} </option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('quarter_id') }}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="exampleEmail" style="font-weight: 600">Phone No <span
                                    style="color: red">*</span></label>
                            <input type="text" name="phoneno" id="phoneno" class="form-control" placeholder=""
                                value="{{ $center->phone_number }}" minlength="01000000"
                                oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                maxlength="11" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('phoneno') }}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="exampleEmail" style="font-weight: 600">Core Center ID <span
                                    style="color: red">*</span></label>
                            <input type="text" name="main_center_id" id="main_center_id" class="form-control" placeholder=""
                                value="{{ $core_center_id }}" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('main_center_id') }}
                            </div>
                        </div>
                        <div class="col-md-9 mb-9">
                            <label for="exampleEmail" style="font-weight: 600">Address</label>
                            <textarea name="address_primary" id="address_primary" readonly class="form-control"
                                placeholder="No(1), 18th Street, Latha Township, Yangon" required>{{ $center->address }}</textarea>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('address_primary') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3 d-none">
                            <label style="font-weight: 600" for="validationCustom03">Center Leader Type</label>
                            <select type="select" id="type_status" name="type_status" class="form-control">
                                <option value="test">Choose Center Leader Type</option>
                                <option value="client"{{ $center->type_status == 'client' ? 'selected' : '' }}>Client
                                </option>
                                <option value="staff"{{ $center->type_status == 'staff' ? 'selected' : '' }}
                                    style="display:none;">Staff</option>
                                <option value="none" {{ $center->type_status == 'none' ? 'selected' : '' }}
                                    style="display:none;">None</option>

                            </select>

                            <!-- <input type="radio" id="inlineradio1" name="type_status" value="client" {{ $center->type_status == 'client' ? 'checked' : '' }}> Client
                                        <input type="radio" id="inlineradio2" name="type_status" value="staff" {{ $center->type_status == 'staff' ? 'checked' : '' }}> Staff
                                        <input type="radio" id="inlineradio3" name="type_status" value="none" {{ $center->type_status == 'active' ? 'none' : '' }}> None -->
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('type_status') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3 d-none">
                            <label style="font-weight: 600" for="validationCustom03">Center Leader</label>
                            <select type="select" id="staff_client_id" name="staff_client_id" class="form-control">
                                <option value="{{ $centerleaderid }}">{{ $centerleadername }}</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('staff_client_id') }}
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="mt-1 mb-2 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper">
                            Save & Back </i></button>
                    <a href="{{ url('centers/') }}" class="btn btn-secondary mb-2 mt-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
                </form>
            </div>
        </div>

    </div>
@endsection
