@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Center Create
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{ url('centers/') }}" class="btn theme-color text-light">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alart alart-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="needs-validation" method="POST" action="{{ route('centers.store') }}"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="position-relative form-group form-row">

                        <div class="col-md-3 mb-3 d-none">
                            <label style="font-weight: 600" for="validationCustom03">Branch <span
                                    style="color: red">*</span></label>
                            <select name="branch_id" id="branch_id_center" class="form-control" required>
                                <option value="" disabled>Choose Branch</option>
                                @php
                                    $sessionBranchid = DB::table('tbl_staff')
                                        ->where('staff_code', session()->get('staff_code'))
                                        ->first()->branch_id;
                                @endphp
                                @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}" data-address="{{ $branch->branch_code }}"
                                        @if ($branch->id == $sessionBranchid) selected @endif>{{ $branch->branch_name }}
                                    </option>
                                @endforeach
                            </select>

                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('branch_id') }}
                            </div>
                        </div>
                        <div class="position-relative form-group form-row">
                            {{-- staff start --}}
                            <div class="col-md-3 mb-3">
                                <label style="font-weight: 600" for="validationCustom03">Staff <span
                                        style="color: red">*</span></label>
                                <select name="staff_id" id="staff_id_center" class="form-control" required>
                                    <option value="" disabled selected>Choose Staff</option>
                                </select>

                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('staff_id') }}
                                </div>
                            </div>
                            {{-- staff end --}}

                            <div class="col-md-3 mb-3">
                                <label style="font-weight: 600" for="validationCustom01">Center Code <span
                                        style="color: red">*</span></label>
                                <input type="text" class="form-control" id="center_uniquekey" name="center_uniquekey"
                                    value="{{ old('center_uniquekey') }}" placeholder="Example : EM-05-0001" required readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('center_uniquekey') }}
                                </div>
                            </div>

                            <div class="col-md-3 mb-3">
                                <label for="exampleEmail" style="font-weight: 600">State / Region </label>
                                <select type="select" id="division_id" name="division_id" class="form-control">
                                    <option value="">Select State</option>
                                    @foreach ($divisions as $division)
                                        <option value="{{ $division->code }}"
                                            {{ old('division_id') == $division->name ? 'selected' : '' }}>
                                            {{ $division->name }}
                                            / {{ $division->description }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('division_id') }}
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="exampleEmail" style="font-weight: 600">District / Division </label>
                                <select type="select" id="district_id" name="district_id" class="form-control">
                                    <option></option>
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('district_id') }}
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="exampleEmail" style="font-weight: 600">Township</label>
                                <select type="select" id="township_id" name="township_id" class="form-control">
                                    <option></option>
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('township_id') }}
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="exampleEmail" style="font-weight: 600">Town / Village / Group Village</label>
                                <select type="select" id="village_id" name="village_id" class="form-control">
                                    <option></option>
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('village_id') }}
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="exampleEmail" style="font-weight: 600">Ward / Small Village</label>
                                <select type="select" id="ward_id" name="ward_id" class="form-control">
                                    <option></option>
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('ward_id') }}
                                </div>
                            </div>

                            <div class="col-md-3 mb-3">
                                <label for="exampleEmail" style="font-weight: 600">Home No</label>
                                <input type="text" name="home_no" id="home_no" 
                                    class="form-control" placeholder="No(1) " value="{{ old('home_no') }}">
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('home_no') }}
                                </div>
                            </div>
                            <div class="col-md-4 mb-4">
                                <label for="exampleEmail" style="font-weight: 600">Phone No <span
                                        style="color: red">*</span></label>
                                <input type="number" name="phoneno" id="phoneno"
                                    class="form-control" placeholder="" value="{{ old('phoneno') }}"
                                    minlength="01000000"
                                    oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                    maxlength="11" required>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('phoneno') }}
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="exampleEmail" style="font-weight: 600">Main Center ID<span style="color:red"> *</span></label>
                                <input type="text" name="main_center_id" id="main_center_id" 
                                    class="form-control" value="{{ old('main_center_id') }}">
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('main_center_id') }}
                                </div>
                            </div>
                            <div class="col-md-12 mb-12">
                                <label for="exampleEmail" style="font-weight: 600">Address</label>
                                <textarea name="address_primary" id="address_primary" readonly class="form-control" style="height: 100px" required>{{ old('address_primary') }}</textarea>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('address_primary') }}
                                </div>
                            </div>
                        </div>
                        
                        {{-- <div class="col-md-12">
                            <label for="exampleEmail" style="font-weight: 600">Address</label>
                            <textarea name="address_primary" id="address_primary" readonly class="form-control"
                                placeholder="No(1), 18th Street, Latha Township, Yangon" required>{{ old('address_primary') }}</textarea>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('address_primary') }}
                            </div>
                        </div> --}}
                        

                        @php
                            $clientCount = DB::table('tbl_client_join')
                                ->where(
                                    'branch_id',
                                    DB::table('tbl_staff')
                                        ->where('staff_code', session()->get('staff_code'))
                                        ->first()->branch_id,
                                )
                                ->count();
                        @endphp
                        <div class="col-md-3 mb-3 @if ($clientCount == 0) d-none @endif d-none">
                            <label style="font-weight: 600" for="validationCustom01">Center Leader Type <span
                                    style="color: red">*</span></label>
                            <select type="select" id="type_status" name="type_status" class="form-control">
                                <option value="test">Choose Center Leader Type</option>
                                <option value="client">Client</option>
                                <option value="staff" style="display:none;">Staff</option>
                                <option value="none" style="display:none;">None</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('type_status') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3 d-none">
                            <label style="font-weight: 600" for="validationCustom03">Center Leader</label>
                            <select type="select" id="staff_client_id" name="staff_client_id" class="form-control">
                                <option></option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('staff_client_id') }}
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="m-1 btn btn-success"><i
                                    class="pe-7s-diskette btn-icon-wrapper">
                                    Save & Back </i></button>
                            <a href="{{ url('centers/') }}" class="btn btn-secondary m-1">
                                <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                            </a>
                        </div>
                </form>
            </div>
        </div>

    </div>
@endsection
