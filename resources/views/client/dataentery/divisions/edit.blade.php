@extends('layouts.app')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Division/State Edit          
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('divisions/')}}" class="btn btn-info">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>                    
                </div>
            </div>    </div>
    </div>            <div class="main-card mb-3 card">
        <div class="card-body">
            @if ($errors->any())
				<div class="alart alart-danger">
					<ul>
						@foreach($errors->all() as $error)
						<li>{{$error}}</li>
						@endforeach
					</ul>
				</div>
			@endif    
            <form class="needs-validation" method="post" action="{{route('divisions.update',$division->id)}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Division/State Name(Eng) <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="name" name="division_name"  value="{{$division->division_name}}" required>                   
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('division_name')}}                                        
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom03">Division/State Name(MM)  <span style="color: red">*</span></label>                        
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="name_mm" name="division_name_mm" value="{{$division->division_name_mm}}" required> 
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('division_name_mm')}}                                        
                        </div>
                    </div>
                </div>
                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                    <a href="{{url('divisions/')}}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
            </form>
        </div>
    </div>
    
</div>
@endsection