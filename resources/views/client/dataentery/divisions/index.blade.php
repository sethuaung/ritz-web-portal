@extends('layouts.app')
@section('content')

    <div class="col-md-12">
        <div class="mb-3"></div> 
        <div class="main-card mb-3 card">
            <div class="card-header">Division List
            <div class="btn-actions-pane-right">
                <div role="group" class="btn-group-sm btn-group">
                    <a href="{{url('divisions/create')}}">
                        <button type="button" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-light">
                                <i class="fa fa-plus"></i> 
                                Add
                        </button>
                    </a>
                </div>
            </div>
            </div>
            @if(session('successMsg')!=NULL)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('successMsg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th>Division Name (Eng)</th>
                            <th>Division Name (MM)</th>
                            <th class="text-center">Action</th>
                         </tr>
                    </thead>
                    <tbody>
                    @php $i=1; @endphp                        
                    @foreach($divisions as $division)
                        <tr>
                            <td class="text-center text-muted">{{$i++}}</td>
                            <td>{{$division->division_name}}</td>
                            <td>{{$division->division_name_mm}}</td>
                            <td class="text-center">
                                <a href="divisions/view/{{$division->id}}">
                                    <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn  btn-outline-info btn-sm"><i class="fa fa-eye"></i></button>
                                </a>
                                                                   
                                <a href="divisions/{{$division->id}}/edit">
                                    <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-warning"><i class="fa fa-pencil" ></i></button>
                                </a>
                                <form action="{{ url('divisions/destroy',['id'=>$division->id]) }}" method="POST" class="d-inline-block" onsubmit="return confirm('Are you sure want to delete?')">
                                    @csrf
                                    @method('DELETE')                                       
                                    <button type="submit" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-danger"><i class="fa fa-trash"> </i></button>                                        
                                </form> 
                            </td>
                        </tr>
                    @endforeach    
                    </tbody>
                </table>
            </div>
            <div class="d-block text-center card-footer">
                <div class="">
                    {{$divisions->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection