@extends('layouts.app')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Quarter Create          
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('quarters/')}}" class="btn btn-info">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>                    
                </div>
            </div>    </div>
    </div>            <div class="main-card mb-3 card">
        <div class="card-body">
            @if ($errors->any())
				<div class="alart alart-danger">
					<ul>
						@foreach($errors->all() as $error)
						<li>{{$error}}</li>
						@endforeach
					</ul>
				</div>
			@endif    
            <form class="needs-validation" method="post" action="{{route('quarters.store')}}" enctype="multipart/form-data" novalidate>
            @csrf
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Township Name <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                    <select class=" form-control" name="township_id" id="township_id">
                        <option selected>Choose Township</option>
                        @foreach ($townships as $township)
                        <option value="{{$township->id}}" >
                            {{ $township->township_name }} 
                        </option>               
                        @endforeach
                    </select> 
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Quarter Name(Eng) <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="name" name="quarter_name"  value="{{ old('quarter_name') }}" required>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('quarter_name')}}                                        
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom03">Quarter Name(MM) <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="name_mm" name="quarter_name_mm" value="{{ old('quarter_name_mm') }}" required>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('quarter_name_mm')}}                                        
                        </div>
                    </div>
                </div>
                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                    <a href="{{url('quarters/')}}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
            </form>
        </div>
    </div>
    
</div>
@endsection