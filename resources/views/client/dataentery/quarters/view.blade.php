@extends('layouts.app')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Quarter Details          
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('quarters/')}}" class="btn btn-info">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>                    
                </div>
            </div>    </div>
    </div>            <div class="main-card mb-3 card">
        <div class="card-body">
        <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Town Name</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                    {{$quarter->village_name}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Quarter Name(Eng)</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                    {{$quarter->quarter_name}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Quarter Name(MM)</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                    {{$quarter->quarter_name_mm}}
                </div>
            </div>
            <br>
        </div>
    </div>
    
</div>
@endsection