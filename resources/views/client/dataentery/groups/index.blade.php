@extends('layouts.app')
@section('content')

    <div class="col-md-12">
        <div class="mb-3"></div>
        <div class="main-card mb-3 card">
            <div class="card-header">Groups List
                <div class="btn-actions-pane-right d-flex align-items-center">
                    <a href="{{url('groups/create')}}">
                        <button type="button" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-light">
                            <i class="fa fa-plus"></i>
                            Add
                        </button>
                    </a>
                    <form method="GET" >
                        <div class="input-group">
                            <input type="text" name="search_grouplist" id="search_grouplist"
                                value="{{ request()->get('search_grouplist') }}"
                                class="form-control mr-1"
                                placeholder="Search..."
                                aria-label="Search"
                                aria-describedby="button-addon2"
                                >

                            <button class="btn theme-color text-light mr-1" type="submit" id="button-addon2">
                                <i class="fa fa-search"></i>
                            </button>

                            <a href="{{url('groups')}}" class="btn theme-color text-light">
                                <i class="fa fa-refresh"></i>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
            {{--@if(session('successMsg')!=NULL)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('successMsg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif --}}
            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Portal Group ID</th>
            				<th>Main Group ID</th>
            				{{--<th>Leader</th>--}}
            				{{--<th>Member</th>--}}
                            <th>Branch</th>
                            {{--<th>Level Status</th>--}}
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>

                    @php $i=1; @endphp
                    @foreach($groups as $group)
                        <tr>
                            <td class="text-center text-muted">{{$i++}}</td>
                            <td>{{ $group["group_uniquekey"] }}</td>
                    		<td>
                    			@if(DB::table('tbl_main_join_group')->where('portal_group_id', $group["group_uniquekey"])->first())
                    				{{ DB::table('tbl_main_join_group')->where('portal_group_id', $group["group_uniquekey"])->first()->main_group_code }}
                    			@endif
                    		</td>
                                {{--@if(isset($group["staff_id"]))
                            <td>{{ $group["staff_id"] }}</td>
                            @else <td></td>
                            @endif--}}
                    {{--@if(isset($group["client_id"]))
                            <td>{{ $group["client_id"] }}</td>
                            @else <td></td>
                            @endif--}}
                            <td>{{ $group["branch_id"] }}</td>
                            {{--<td>{{ $group["level_status"] == null ? 'N/A' : $group["level_status"] }}</td>--}}

                            <td class="text-center">
                                <a href="groups/view/{{$group['group_uniquekey']}}">
                                    <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn  btn-outline-info btn-sm"><i class="fa fa-eye"></i></button>
                                </a>

                                <a href="groups/{{$group['group_uniquekey']}}/edit">
                                    <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-warning"><i class="fa fa-pencil" ></i></button>
                                </a>
                                <form action="{{ url('groups/destroy',['id'=>$group['group_uniquekey']]) }}" method="POST" class="d-inline-block" onsubmit="return confirm('Are you sure want to delete?')">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-danger"><i class="fa fa-trash"> </i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-block text-center card-footer">
                <div class="">
                    {{ $results->appends($_GET)->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
                    
@if(session('successMsg')!=NULL)
@section('script')
    <script>
        statusAlert("{{session('successMsg')}}");
    </script>
@endsection
@endif
