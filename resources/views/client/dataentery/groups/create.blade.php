@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Group Create
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{url('groups/')}}" class="btn theme-color text-light">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alart alart-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="needs-validation" method="POST" action="{{route('groups.store')}}"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="position-relative form-group form-row">

                        <div class="col-md-3 mb-3 d-none">
                            <label style="font-weight: 600" for="validationCustom01">Branch <span style="color: red">*</span></label>
                            <select name="branch_id" id="branch_id_group" class="form-control"
                                    value="{{ old('branch_id') }}" required>
                            	@php
                                    $sessionBranchid = DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id;
                                @endphp
                                <option value="">Choose Branch</option>
                                @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}"
                                            data-address="{{ $branch->branch_code }}" @if($branch->id == $sessionBranchid) selected @endif>{{ $branch->branch_name }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('branch_id')}}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Center <span style="color: red">*</span></label>
                            <select name="center_id" id="center_id_group" class="form-control"
                                    value="{{ old('center_id') }}" required>
                                <option value="" selected disabled>Choose Center</option>
                                @foreach ($centers as $center)
                                    <option value="{{ $center->center_uniquekey }}"
                                            data-address="{{ $center->branch_code }}">{{ $center->main_center_code }}
                                    </option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('center_id')}}
                            </div>
                        </div>

                            <input type="hidden" id="staff_id_group" name="staff_id" class="form-control" readonly>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Loan Officer</label>
                            <input type="text" id="staff_name_group" class="form-control" disabled>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('staff_id')}}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Group Code </label>
                            <input type="text" class="form-control" id="group_uniquekey" name="group_uniquekey"
                                   value="{{ old('group_uniquekey') }}" placeholder="" readonly>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('group_uniquekey')}}
                            </div>
                        </div>

                        @php
                           $clientCount = DB::table('tbl_client_join')->where('branch_id', DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id)->count();
                        @endphp
                        <div class="col-md-3 mb-3 d-none">
                            <label style="font-weight: 600" for="validationCustom01">Level Status</label>
                            <select type="select" id="level_status" name="level_status"
                                    placeholder="Choose Level Status" class="form-control">
                            <option value="" selected disabled>Choose level</option>
							<option value="leader" @if($clientCount == 0) selected @endif>Leader</option>
                            {{--<option value="leader" >Leader</option>--}}
                            {{--<option value="member">Member</option>--}}
							{{--<option value="none">None</option>--}}

                            </select>
                            <!-- <input type="radio" id="inlineradio1" name="level_status" value="leader"> Leader
                             <input type="radio" id="inlineradio2" name="level_status" value="member"> Member
                             <input type="radio" id="inlineradio3" name="level_status" value="none"> None  -->
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('level_status')}}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3 d-none">
                            <label style="font-weight: 600" for="validationCustom01">Group Leader</label>
                            <select name="client_id" id="client_id" class="form-control" value="{{ old('client_id') }}">
                                <option value="">Choose Leader</option>
                                @foreach ($clients as $client)
                                    <option value="{{ $client->client_uniquekey }}">{{ $client->name }}({{ $client->client_uniquekey }})</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('client_id')}}
                            </div>
                        </div>

                    </div>

                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save &
                            Back </i></button>
                    <a href="{{url('groups/')}}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
                </form>
            </div>
        </div>

    </div>
@endsection
