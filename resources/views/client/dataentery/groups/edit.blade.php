@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Group Edit
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{url('groups/')}}" class="btn theme-color text-white">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alart alart-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="needs-validation" method="post" action="{{route('groups.update',$group->group_uniquekey)}}"
                      enctype="multipart/form-data" novalidate>
                    @csrf
                    @method('PUT')
                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Branch <span style="color: red">*</span></label>
                            <select name="branch_id" id="branch_id_group" class="form-control">
                                @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}" data-address="{{ $branch->branch_code }}"
                                            @if($group->branch_id == $branch->id) selected="selected" @endif >{{ $branch->branch_name }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('branch_id')}}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Center <span style="color: red">*</span></label>
                            <select name="center_id" id="center_id_group" class="form-control">
                                @foreach ($centers as $center)
                                    <option value="{{ $center->center_uniquekey }}"
                                            @if($group->center_id == $center->center_uniquekey) selected="selected" @endif >{{ $center->center_uniquekey }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('center_id')}}
                            </div>
                        </div>

                        <input type="hidden" id="staff_id_group" name="staff_id" value="{{ $staff->staff_code }}" class="form-control" readonly="" autocomplete="off">

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Loan Officer</label>
                            <input type="text" class="form-control" id="staff_name_group" disabled
                                   value="{{ $staff->name }}">
                            {{--                            <select name="staff_id" id="staff_id" class="form-control">--}}
                            {{--                                <option value="">Choose Staff</option>--}}
                            {{--                                @foreach ($staffs as $staff)--}}
                            {{--                                    <option--}}
                            {{--                                        value="{{ $staff->id }}" {{ ($group->staff_id== $staff->id )? "selected" : "" }}> {{ $staff->name }}</option>--}}
                            {{--                                @endforeach--}}
                            {{--                            </select>--}}
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('staff_id')}}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Group Code <span style="color: red">*</span></label>
                            <input type="text" class="form-control" id="group_uniquekey_edit" name="group_uniquekey"
                                   value="{{$group->group_uniquekey}}" readonly required>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('group_uniquekey')}}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3 d-none">
                            <label style="font-weight: 600" for="validationCustom03">Level Status <span style="color: red">*</span></label>
                            <select type="select" id="level_status" name="level_status" class="form-control">
                                <option value="test">Choose Level Status</option>
                                <option value="leader"{{ ($group->level_status=="leader")? "selected" : "" }} >Leader
                                </option>
                            {{--<option value="member"{{ ($group->level_status=="member")? "selected" : "" }} >Member
                                </option>
                                <option value="none" {{ ($group->level_status=="none")? "selected" : "" }}>None</option>--}}

                            </select>
                        <!-- <input type="radio" id="inlineradio1" name="level_status" value="leader" {{ ($group->level_status=="leader")? "checked" : "" }}> Leader
                    <input type="radio" id="inlineradio2" name="level_status" value="member" {{ ($group->level_status=="member")? "checked" : "" }}> Member
                    <input type="radio" id="inlineradio3" name="level_status" value="none" {{ ($group->level_status=="none")? "checked" : "" }}> None  -->
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('level_status')}}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3 d-none">
                            <label style="font-weight: 600" for="validationCustom03">Client</label>
                            <select name="client_id" id="client_id" class="form-control">
                                <option value="">Choose Client</option>
                                @foreach ($clients as $client)
                                    <option
                                        value="{{ $client->client_uniquekey }}" {{ ($group->client_id== $client->client_uniquekey )? "selected" : "" }}> {{ $client->name }}({{$client->client_uniquekey}})</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('client_id')}}
                            </div>
                        </div>

                    </div>
                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save &
                            Back </i></button>
                    <a href="{{url('groups/')}}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
                </form>
            </div>
        </div>

    </div>
@endsection
