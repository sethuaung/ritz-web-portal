@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Group Details View
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{url('groups/')}}" class="btn theme-color text-white">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
				
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Group Code</label>
                    </div>
                    <div class="col-md-9 mb-9 ">
                        : {{$group->group_uniquekey}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Branch</label>
                    </div>
                    <div class="col-md-9 mb-9 ">
                        : {{$group->branch_name }}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Center</label>
                    </div>
                    <div class="col-md-9 mb-9 ">
                        : {{$group->center_uniquekey }}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Group Leader</label>
                    </div>
                    <div class="col-md-9 mb-9 ">
                        @if(isset($group->client_name))
                            : {{$group->client_name}}
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Group Leader ID</label>
                    </div>
                    <div class="col-md-9 mb-9 ">
                        @if(isset($group->c_id))
                            : {{$group->c_id}}
                        @endif
                    </div>
                </div>
                
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Level Status</label>
                    </div>
                    <div class="col-md-9 mb-9 ">
                        : {{$group->level_status }}
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
