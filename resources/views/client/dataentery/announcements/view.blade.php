@extends('layouts.app')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Announcements View          
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('announcements/')}}" class="btn theme-color text-white">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>                    
                </div>
            </div>    
        </div>
    </div>            
    <div class="main-card mb-3 card">
        <div class="card-body">
            <div class="form-row pt-3">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Title</label>
                </div>
                <div  class="col-md-9 mb-9">
                    : {{$announcements->title}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Author</label>
                </div>
                <div  class="col-md-9 mb-9">
                    : {{$announcements->author}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Company</label>
                </div>
                <div  class="col-md-9 mb-9">
                    : {{$announcements->company}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Branch</label>
                </div>
                <div  class="col-md-9 mb-9">
                    : {{$announcements->branch}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Department</label>
                </div>
                <div  class="col-md-9 mb-9">
                    : {{$announcements->department}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01" class="font-weight-bold">Description</label>
                </div>
                <div  class="col-md-9 mb-9">
                    : {{$announcements->description}}
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection