@extends('layouts.app')
@section('content')
<div class="col-md-12">
    <div class="mb-3"></div> 
    <div class="main-card mb-3 card">
        <div class="card-header">
        Announcements List
            <div class="btn-actions-pane-right">
                <div role="group" class="btn-group-sm btn-group">
                    <a href="{{url('announcements/create')}}">
                        <button type="button" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-light">
                            <i class="fa fa-plus"></i> Add
                        </button>
                    </a>
                </div>
            </div>
        </div>

        <div class="widget-content p-2">
            <div class="widget-content-wrapper">
                <div class="widget-content-left mr-3 ml-3">
                    <label class="m-0"> Show </label>
                </div>
                <div class="widget-content-left mr-1">
                    <div class="widget-heading">
                        <select name="example_length" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select> 
                    </div>
                </div>
                <div class="widget-content-right">
                    <div class="btn-group">
                           <!-- <label class="m-2"> Search </label> -->
                        <form method="GET" >
                            <div class="input-group">
                                <input type="text" name="search_announcementslist" id="search_announcementslist"
                                       value="{{ request()->get('search_announcementslist') }}" 
                                       class="form-control mr-1" 
                                       placeholder="Search..." 
                                       aria-label="Search" 
                                       aria-describedby="button-addon2"
                                >

                                <button class="btn theme-color text-light mr-1" type="submit" id="button-addon2">
                                    <i class="fa fa-search"></i> 
                                </button>

                                <a href="{{url('announcements')}}" class="btn theme-color text-light">
                                       <i class="fa fa-refresh"></i> 
                                </a>
                            </div>
                        </form>
                    </div> 
                </div>
            </div>
        </div>

        @if(session('successMsg')!=NULL)
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Success!</strong> {{session('successMsg')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <div class="table-responsive pl-3 pr-3">
            <table class="align-middle mb-0 table table-bordered">
                    <tr>
                        <th class="text-center">No</th>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Description</th>
                        <th class="text-center">Action</th>
                     </tr>
                    </thead>
                <tbody>    
                @php $i=1; @endphp                  
                @foreach ($announcements as $announcement)
                    <tr>
                        <td class="text-center text-muted">{{$i++}}</td>
                        <td>{{ $announcement->title }}</td>
                        <td>
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left mr-3">
                                       
                                    </div>
                                    <div class="widget-content-left flex2">
                                        <div class="widget-heading">{{ $announcement->author }}</div>
                                        <div class="widget-subheading pt-3">{{ $announcement->company }}</div>
                                        <div class="widget-subheading pt-3">{{ $announcement->department }}</div>
                                        <div class="widget-subheading pt-3">{{ $announcement->branch }}</div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        
                        <td>{{ $announcement->description }}</td>
                        <td class="text-center text-nowrap">
                            <a href="announcements/view/{{$announcement->id}}">
                                <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn  btn-outline-info btn-sm"><i class="fa fa-eye"></i></button>
                            </a>
                                                                   
                            <a href="announcements/{{$announcement->id}}/edit">
                                <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-warning"><i class="fa fa-pencil" ></i></button>
                            </a>
                            <form action="{{ url('announcements/destroy',['id'=>$announcement->id]) }}" method="POST" class="d-inline-block" onsubmit="return confirm('Are you sure want to delete?')">
                                @csrf
                                @method('DELETE')                                       
                                <button type="submit" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-danger"><i class="fa fa-trash"> </i></button>                                        
                            </form> 
                        </td>
                    </tr>
                @endforeach       
                </tbody>
            </table>
        </div>
        <div class="d-block text-center card-footer">
            <div class="">
                @if ($announcements->lastPage() > 1)
                    <ul class="pagination d-flex flex-wrap justify-content-center ">
                    <!-- <i class="pe-7s-prev btn-icon-wrapper"></i> -->
                        <li class="page-item {{ ($announcements->currentPage() == 1) ? ' disabled' : '' }}">
                            
                            <a class="page-link" href="{{ $announcements->url(1) }}">Previous</a>
                        </li>
                        @for ($i = 1; $i <= $announcements->lastPage(); $i++)
                            <li class="page-item {{ ($announcements->currentPage() == $i) ? ' active' : '' }}">
                                <a class="page-link" href="{{ $announcements->url($i) }}">{{ $i }}</a>
                            </li>
                        @endfor
                        <li class="page-item {{ ($announcements->currentPage() == $announcements->lastPage()) ? ' disabled' : '' }}">
                            <a class="page-link" href="{{ $announcements->url($announcements->currentPage()+1) }}" >Next</a>
                        </li>
                    </ul>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection