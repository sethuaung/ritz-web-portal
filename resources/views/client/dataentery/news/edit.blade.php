@extends('layouts.app')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>News Edit          
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('news/')}}" class="btn theme-color text-white">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>                    
                </div>
            </div>    </div>
    </div>            <div class="main-card mb-3 card">
        <div class="card-body">
            @if ($errors->any())
				<div class="alart alart-danger">
					<ul>
						@foreach($errors->all() as $error)
						<li>{{$error}}</li>
						@endforeach
					</ul>
				</div>
			@endif    
            <form class="needs-validation" method="POST" action="{{route('news.update',$news->id)}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
                <div class="form-row pt-3">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Title <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="name" name="title"  value="{{$news->title}}" required>                   
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('title')}}                                        
                        </div>
                    </div>
                </div>
                <div class="form-row pt-3">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom03">Author <span style="color: red">*</span></label>                        
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="name_mm" name="author" value="{{$news->author}}" required> 
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('author')}}                                        
                        </div>
                    </div>
                </div>
                <div class="form-row pt-3">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom03">Department <span style="color: red">*</span></label>                        
                    </div>
                    <div class="col-md-9 mb-9">
                        <select type="select" id="id" name="department" class="form-control" required>
                            <option value="">Select Department</option>
                            @foreach ($departments as $department) 
                                <option value="{{ $department->department_name }}" @if($news->department == $department->department_name) selected @endif>{{ $department->department_name }}</option>
                                {{-- {{(old("id")== $department->department_name ? "selected":"")}} --}}
                            @endforeach
                        </select>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('department')}}                                        
                        </div>
                    </div>
                </div>
                <div class="form-row pt-3">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom03">Company <span style="color: red">*</span></label>                        
                    </div>
                    <div class="col-md-9 mb-9">
                        <select type="select" id="id" name="company" class="form-control" required>
                            <option value="">Select Company</option>
                            @foreach ($companies as $company) 
                                <option value="{{ $company->company_name }}" @if($news->company == $company->company_name) selected @endif >{{ $company->company_name }}</option>
                            @endforeach
                        </select>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('company')}}                                        
                        </div>
                    </div>
                </div>
                <div class="form-row pt-3">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom03">Description <span style="color: red">*</span></label>                        
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="name_mm" name="description" value="{{$news->description}}" required> 
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('description')}}                                        
                        </div>
                    </div>
                </div>
                <div class="form-row pt-3 pb-3">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Photo <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9 p-3">
                        <input type="file" class="form-control" id="" name="photo[]" value="photo" required multiple>
                        @foreach (json_decode($news->photo, true) as $photo)
                            <img src="{{ asset('/storage/app/public/clientphotos/'.$photo) }}" width="100px" height="100px" alt="image" title="">
                        @endforeach
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('photo')}}                                        
                        </div>
                    </div>
                </div>
                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                    <a href="{{url('news/')}}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
            </form>
        </div>
    </div>
    
</div>
@endsection