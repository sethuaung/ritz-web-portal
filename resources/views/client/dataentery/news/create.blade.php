@extends('layouts.app')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>News Create          
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('news/')}}" class="btn theme-color text-light">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>                    
                </div>
            </div>    
        </div>
    </div>            
    <div class="main-card mb-3 card">
        <div class="card-body">
            @if ($errors->any())
				<div class="alart alart-danger">
					<ul>
						@foreach($errors->all() as $error)
						<li>{{$error}}</li>
						@endforeach
					</ul>
				</div>
			@endif    
            <form class="needs-validation" method="POST" action="{{route('news.store')}}" enctype="multipart/form-data" novalidate>
            @csrf
                
                <div class="form-row pt-3">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Title <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="" name="title" value="" required>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('education_name')}}                                        
                        </div>
                    </div>
                </div>
                <div class="form-row pt-3">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom03">Author <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="" name="author" value="" required>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('author')}}                                        
                        </div>
                    </div>
                </div>
                <div class="form-row pt-3">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Department <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <select type="select" id="id" name="department" class="form-control" required>
                            <option value="">Select Department</option>
                            @foreach ($departments as $department) 
                                <option value="{{ $department->department_name }}" {{(old("id")== $department->department_name ? "selected":"")}} >{{ $department->department_name }}</option>
                            @endforeach
                        </select>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('department')}}                                        
                        </div>
                    </div>
                </div>
                <div class="form-row pt-3">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Company <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <select type="select" id="id" name="company" class="form-control" required>
                            <option value="">Select Company</option>
                            @foreach ($companies as $company) 
                                <option value="{{ $company->company_name }}" {{(old("id")== $company->company_name ? "selected":"")}} >{{ $company->company_name }}</option>
                            @endforeach
                        </select>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('company')}}                                        
                        </div>
                    </div>
                </div>
                <div class="form-row pt-3">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Description <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="" name="description" value="" required>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('description')}}                                        
                        </div>
                    </div>
                </div>
                <div class="form-row pt-3 pb-3">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Photo <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="file" class="form-control"  name="photo[]" value="" required multiple>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('photo')}}                                        
                        </div>
                    </div>
                </div>

                <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                <a href="{{url('news/')}}" class="btn btn-secondary m-1">
                    <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                </a>
            </form>
        </div>
    </div>
    
</div>
@endsection