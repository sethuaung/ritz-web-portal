@extends('layouts.app')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Province Details          
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('provinces/')}}" class="btn btn-info">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>                    
                </div>
            </div>    </div>
    </div>            <div class="main-card mb-3 card">
        <div class="card-body">
        <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">District Name</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                    {{$province->district_name}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Province Name(Eng)</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                    {{$province->province_name}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Province Name(MM)</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                    {{$province->province_name_mm}}
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection