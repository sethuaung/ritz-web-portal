@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title  mb-0">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Branch Create</div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{ url('branches/') }}" class="btn theme-color text-white">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alart alart-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="needs-validation" method="POST" action="{{ route('branches.store') }}"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="position-relative form-group form-row">

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Branch Name <span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="name" name="branch_name"
                                value="{{ old('branch_name') }}" placeholder="Example: Aung Ban" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('branch_name') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Branch Code <span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="name" name="branch_code"
                                value="{{ old('branch_code') }}" placeholder="Example: AB" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('branch_code') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Primary Phone <span
                                    style="color: red">*</span></label>

                            <input type="number" class="form-control" id="name" name="phone_primary"
                                value="{{ old('phone_primary') }}" placeholder="Example: 091234567" minlength="01000000"
                                oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                maxlength="11" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('phone_primary') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Secondary Phone</label>
                            <input type="number" class="form-control" id="name" name="phone_secondary"
                                value="{{ old('phone_secondary') }}" placeholder="" minlength="01000000"
                                oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                maxlength="11">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('phone_secondary') }}
                            </div>
                        </div>



                    </div>
                    <div class="position-relative form-group form-row">



                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Tertiary Phone</label>
                            <input type="number" class="form-control" id="name" name="phone_tertiary"
                                value="{{ old('phone_tertiary') }}" placeholder="">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('phone_tertiary') }}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Branch Photo <span
                                    style="color: red">*</span> </label>
                            <input type="file" class="form-control" id="branch_photo" name="branch_photo" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('branch_photo') }}
                            </div>
                        </div>

                        <div class="col-md-6 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Description</label>
                            <textarea class="form-control" id="name" name="description" value="" placeholder="">{{ old('center_leader_id') }}</textarea>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('description') }}
                            </div>
                        </div>

                    </div>

                    <div class="position-relative form-group form-row">

                        <div class="col-md-3 mb-3">
                            <label for="exampleEmail" style="font-weight: 600">State / Region <span
                                    style="color: red">*</span></label>
                            <select type="select" id="division_id" name="division_id" class="form-control" required>
                                <option value="">Select State</option>
                                @foreach ($divisions as $division)
                                    <option value="{{ $division->code }}"
                                        {{ old('division_id') == $division->name ? 'selected' : '' }}>
                                        {{ $division->name }}
                                        / {{ $division->description }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('division_id') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label for="exampleEmail" style="font-weight: 600">District / Division <span
                                    style="color: red">*</span></label>
                            <select type="select" id="district_id" name="district_id" class="form-control" required>
                                <option></option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('district_id') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label for="exampleEmail" style="font-weight: 600">Township<span
                                    style="color: red">*</span></label>
                            <select type="select" id="township_id" name="township_id" class="form-control" required>
                                <option></option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('township_id') }}
                            </div>
                        </div>



                        <div class="col-md-3 mb-3">
                            <label for="exampleEmail" style="font-weight: 600">Town / Village / Group Village<span
                                    style="color: red">*</span></label>
                            <select type="select" id="village_id" name="village_id" class="form-control" required>
                                <option></option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('village_id') }}
                            </div>
                        </div>

                    </div>

                    <div class="position-relative form-group form-row">

                        <!--<div class="col-md-3 mb-3">
                                        <label style="font-weight: 600" for="validationCustom01">Province <span style="color: red">*</span></label>
                                        <select type="select" id="province_id" name="province_id" class="form-control" required>
                                                        <option></option>
                                                    </select>
                                                    <div class="text-danger form-control-feedback">
                                                        {{-- {{ $errors->first('province_id') }} --}}
                                                    </div>
                                    </div> -->

                        <div class="col-md-3 mb-3">
                            <label for="exampleEmail" style="font-weight: 600">Ward / Small Village</label>
                            <select type="select" id="ward_id" name="ward_id" class="form-control">
                                <option></option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('ward_id') }}
                            </div>
                        </div>

                        {{-- <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Quarter</label>
                            <select type="select" id="quarter_id" name="quarter_id" class="form-control">
                                <option></option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('quarter_id') }}
                            </div>
                        </div> --}}

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Location</label>
                            <textarea class="form-control" id="name" name="location" value="{{ old('location') }}"
                                placeholder=" Example: 16.771812, 96.154443"></textarea>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('location') }}
                            </div>
                        </div>

                    </div>

                    <div class="position-relative form-group form-row">

                        <div class="col-md-12">
                            <label for="exampleEmail" style="font-weight: 600">Full Address<span
                                    style="color: red">*</span></label>
                            <textarea name="address_primary" id="address" class="form-control"
                                placeholder="No(1), 18th Street, Latha Township, Yangon" required>{{ old('address_primary') }}</textarea>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('address_primary') }}
                            </div>
                        </div>


                    </div>
                    <button type="submit" class="mt-1 mb-2 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper">
                            Save & Back </i></button>
                    <a href="{{ url('branches/') }}" class="btn btn-secondary mb-2 mt-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
                </form>
            </div>
        </div>

    </div>
@endsection
