 @extends('layouts.app')
@section('content')

    <div class="col-md-12">
        <div class="mb-3"></div>  
        <div class="main-card mb-3 card mb-0">
            <div class="card-header">
                Branches List
                <div class="btn-actions-pane-right">
                    <div role="group" class="btn-group-sm btn-group">
                        <a href="{{url('branches/create')}}">
                            <button type="button" title="" data-placement="bottom" class="btn-shadow mr-3 btn theme-color text-white">
                                <i class="fa fa-plus"></i> Add
                            </button>
                        </a>
                        <!-- <button class="btn-shadow btn btn-primary mr-1">This Week</button>
                        <button class="btn-shadow btn btn-primary mr-1">This Month</button>
                        <button class="btn-shadow btn btn-primary mr-1">This Year</button>
                        <button class="btn-shadow btn btn-primary">All Month</button> -->
                    </div>
                </div>
            </div>
            <div class="widget-content p-2">
                <div class="widget-content-wrapper">
                    <div class="widget-content-left mr-3 ml-3">
                        <label class="m-0"> Show </label>
                    </div>
                    <div class="widget-content-left">
                        <div class="widget-heading">
                            <select name="example_length" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> 
                        </div>
                    </div>
					{{--<div class="widget-content-left mr-3 ml-3">
                        <div class="btn-group">
                            <label class="m-0"> Show </label>
                            <select name="example_length" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> 
                        </div> 
                    </div>   
                    <div class="widget-content-right">
                        <div class="btn-group">
                            <label class="m-2"> Branch ID </label>
                            <select name="example_length" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select> 

                            <label class="m-1 ml-3">  </label>
                            <input id="" type="search" class="form-control form-control-sm" placeholder="" aria-controls="example">
                            
                        </div> 
                       
                    </div> --}}
                </div>
            </div>

            {{-- @if(session('successMsg')!=NULL)
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('successMsg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            @endif --}}
            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table table-bordered">
                <tr>
                            <th class="text-center">No</th>
                            <th>Branch Code</th>
                            <th>Branch Name</th>
                            <th>Primary Phone</th>
                            <th>Full Address</th>
                            <th class="text-center">Action</th>
                         </tr>
                        </thead>
                        
                    <tbody>
                    @php $i=1; @endphp                        
                    @foreach($branches as $branch)
                        <tr style="height:80px">
                            <td class="text-center text-muted">{{$i++}}</td>
                            <td>{{$branch->branch_code}}</td>
                            <td>{{$branch->branch_name}}</td>
                            <td>{{$branch->phone_primary}}</td>
                            <td style="font:message-box">{{$branch->full_address}}</td>
                            {{-- <td class="text-center">
                                <a href="branches/view/{{$branch->id}}">
                                    <button type="button" id="PopoverCustomT-1" class="btn btn-icon btn-icon-only btn-info btn-sm"><i class="pe-7s-look btn-icon-wrapper"></i></button>
                                </a>
                                                                   
                                <a href="branches/{{$branch->id}}/edit">
                                    <button type="button" id="PopoverCustomT-1" class="btn btn-warning btn-sm"><i class="pe-7s-edit btn-icon-wrapper" ></i></button>
                                </a>
                                <form action="{{ url('branches/destroy',['id'=>$branch->id]) }}" method="POST" class="d-inline-block" onsubmit="return confirm('Are you sure want to delete?')">
                                    @csrf
                                    @method('DELETE')                                       
                                    <button type="submit" id="PopoverCustomT-1" class=" btn-sm btn-icon-only btn btn-danger"><i class="pe-7s-trash btn-icon-wrapper"> </i></button>                                        
                                </form> 

                            </td> --}}

                            <td class="text-center" style="width:150px">                                
                                <a href="branches/view/{{$branch->id}}">
                                    <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn  btn-outline-info btn-sm"><i class="fa fa-eye"></i></button>
                                </a>
                                                                   
                                <a href="branches/{{$branch->id}}/edit">
                                    <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-warning"><i class="fa fa-pencil" ></i></button>
                                </a>
                                <form action="{{ url('branches/destroy',['id'=>$branch->id]) }}" method="POST" class="d-inline-block" onsubmit="return confirm('Are you sure want to delete?')">
                                    @csrf
                                    @method('DELETE')                                       
                                    <button type="submit" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-danger"><i class="fa fa-trash"> </i></button>                                        
                                </form>
                            </td>
                        </tr>
                    @endforeach    
                    </tbody>
                </table>
            </div>
            <div class="d-block text-center card-footer">
                <div class="">
                     {{ $branches->links() }}

                </div>
            </div>
        </div>
    </div>
@endsection
                
@if(session('successMsg')!=NULL)
@section('script')
    <script>
        statusAlert("{{session('successMsg')}}");
    </script>
@endsection
@endif