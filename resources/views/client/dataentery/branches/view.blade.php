@extends('layouts.app')
@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="lnr-picture text-danger">
                    </i>
                </div>
                <div>Branch Details View          
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('branches/')}}" class="btn theme-color">
                        <i class="pe-7s-back btn-icon-wrapper text-light">Back To List</i>
                    </a>                    
                </div>
            </div>    
        </div>
    </div>            
    <div class="main-card mb-3 card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header theme-color">
                            <h5 class="text-light">
                                <i class="fas fa-info-circle"></i>
                                <span class="ml-2">Details</span>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <h5 class="menu-header-title text-uppercase mb-3 fsize-2 text-secondary font-weight-bold">Branch Photo</h5>
                                            <img src="{{ asset('/storage/app/public/clientphotos/'.$branch->branch_photo) }}" width="250px" alt="" title="" class="rounded">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Branch Name</label>
                                        </div>
                                        <div  class="col-md-6 mb-9 ">
                                            : {{$branch->branch_name}}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Branch Code</label>
                                        </div>
                                        <div  class="col-md-6 mb-9 ">
                                            : {{$branch->branch_code}}
                                        </div>
                                    </div>
                                    <div class="form-row" hidden>
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom01">Client</label>
                                        </div>
                                        <div  class="col-md-6 mb-9 ">
                                        : {{$branch->client_prefix}}
                                        </div>
                                    </div>
                                    
                                    <div class="form-row" hidden>
                                        <div class="col-md-3 mb-3">
                                            <label for="validationCustom01">Title</label>
                                        </div>
                                        <div  class="col-md-9 mb-9 ">
                                        : {{$branch->title}}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Primary Phone</label>
                                        </div>
                                        <div  class="col-md-6 mb-9 ">
                                        : {{$branch->phone_primary }}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Secondary Phone</label>
                                        </div>
                                        <div  class="col-md-6 mb-9 ">
                                        : {{$branch->phone_secondary }}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Tertiary Phone</label>
                                        </div>
                                        <div  class="col-md-6 mb-9 ">
                                        : {{$branch->phone_tertiary }}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Description</label>
                                        </div>
                                        <div  class="col-md-6 mb-9 ">
                                        : {{$branch->description }}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Full Address</label>
                                        </div>
                                        <div  class="col-md-6 mb-9 ">
                                        : {{$branch->full_address }}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationCustom01" class="font-weight-bold">Location</label>
                                        </div>
                                        <div  class="col-md-6 mb-9 ">
                                        : {{$branch->location}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header theme-color">
                            <h5 class="text-light">
                                <i class="pe-7s-map-marker"></i>
                                <span class="ml-2">Location</span>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Division/State</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                : {{$division->name }}
                                </div>
                            </div>
                            {{-- <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">City</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                : {{$branch->city_name }}
                                </div>
                            </div> --}}
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">District</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                : {{$district->name }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Township</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                : {{$township->name }}
                                </div>
                            </div>
                            {{-- <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Province</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                : {{$branch->province_name }}
                                </div>
                            </div> --}}
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Village</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                : {{$village->name }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Quarter</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                : {{$ward->name }}
                                </div>
                            </div><br><br><br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection