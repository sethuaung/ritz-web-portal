@extends('layouts.app')
@section('content')

    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">

                    <div>Branch Edit
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{ url('branches/') }}" class="btn theme-color text-white">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alart alart-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form class="needs-validation" method="POST" action="{{ route('branches.update', $branch->id) }}"
                    enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Branch Name <span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="branch_name" name="branch_name"
                                value="{{ $branch->branch_name }}" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('branch_name') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Branch Code <span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="branch_code" name="branch_code"
                                value="{{ $branch->branch_code }}" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('branch_code') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Primary Phone <span
                                    style="color: red">*</span></label>
                            <input type="number" class="form-control" id="phone_primary" name="phone_primary"
                                value="{{ $branch->phone_primary }}" minlength="01000000"
                                oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                maxlength="11" required>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('phone_primary') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Secondary Phone</label>
                            <input type="number" class="form-control" id="phone_secondary" name="phone_secondary"
                                value="{{ $branch->phone_secondary }}" minlength="01000000"
                                oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                maxlength="11">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('phone_secondary') }}
                            </div>
                        </div>


                    </div>

                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Tertiary Phone</label>
                            <input type="number" class="form-control" id="phone_tertiary" name="phone_tertiary"
                                value="{{ $branch->phone_tertiary }}">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('phone_tertiary') }}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Branch Photo <span
                                    style="color: red">*</span></label>
                            <input type="file" class="form-control" id="branch_photo" name="branch_photo" placeholder="">
                            <img src="{{ asset('/storage/app/public/clientphotos/' . $branch->branch_photo) }}"
                                width="100px" height="100px" alt="" title="">
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('branch_photo') }}
                            </div>
                        </div>

                        <div class="col-md-6 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Description</label>
                            <textarea class="form-control" id="description" name="description" value="">{{ $branch->description }}</textarea>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('description') }}
                            </div>
                        </div>

                    </div>

                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 mb-3">
                            <label for="exampleEmail" style="font-weight: 600">State / Region <span
                                    style="color: red">*</span></label>
                            <select type="select" id="division_id" name="division_id" class="form-control" required>
                                <option value="">Select State</option>
                                @foreach ($divisions as $division)
                                    <option value="{{ $division->code }}"
                                        {{ $branch->division_id == $division->code ? 'selected' : '' }}>
                                        {{ $division->name }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('division_id') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label for="exampleEmail" style="font-weight: 600">District / Division <span
                                    style="color: red">*</span></label>
                            <select type="select" id="district_id" name="district_id" class="form-control" required>
                                @foreach ($districts as $district)
                                    <option value="{{ $district->code }}"
                                        {{ $branch->district_id == $district->code ? 'selected' : '' }}>
                                        {{ $district->name }} </option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('district_id') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label for="exampleEmail" style="font-weight: 600">Township<span
                                    style="color: red">*</span></label>
                            <select type="select" id="township_id" name="township_id" class="form-control" required>
                                @foreach ($townships as $township)
                                    <option value="{{ $township->code }}"
                                        {{ $branch->township_id == $township->code ? 'selected' : '' }}>
                                        {{ $township->name }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('township_id') }}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label for="exampleEmail" style="font-weight: 600">Town / Village / Group Village<span
                                    style="color: red">*</span></label>
                            <select type="select" id="village_id" name="village_id" class="form-control" required>
                                @foreach ($villages as $village)
                                    <option value="{{ $village->code }}"
                                        {{ $branch->village_id == $village->code ? 'selected' : '' }}>
                                        {{ $village->name }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('village_id') }}
                            </div>
                        </div>
                    </div>

                    <div class="position-relative form-group form-row">


                        <!--<div class="col-md-3 mb-3">
                                    <label style="font-weight: 600" for="validationCustom03">Province <span style="color: red">*</span></label>
                                    <select type="select" id="province_id" name="province_id" class="form-control">
                                                {{-- @foreach ($provinces as $province) --}}
            {{-- <option value="{{ $province->id }}" @if ($branch->province_id == $province->id) selected @endif >{{ $province->province_name }} </option> --}}
            {{-- @endforeach --}}
                                                </select>
                                    <div class="text-danger form-control-feedback">
                                        {{-- {{ $errors->first('province_id') }} --}}
                                    </div>
                                </div> -->

                        <div class="col-md-3 mb-3">
                            <label for="exampleEmail" style="font-weight: 600">Ward / Small Village</label>
                            <select type="select" id="ward_id" name="ward_id" class="form-control">
                                @foreach ($wards as $ward)
                                    <option value="{{ $ward->code }}"
                                        {{ $branch->quarter_id == $ward->code ? 'selected' : '' }}>
                                        {{ $ward->name }} </option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('ward_id') }}
                            </div>
                        </div>
                        {{-- <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Quarter</label>
                            <select type="select" id="quarter_id" name="quarter_id" class="form-control">
                                <option selected disabled>Choose Quarter</option>
                                @foreach ($quarters as $quarter)
                                    <option value="{{ $quarter->id }}" @if ($branch->quarter_id == $quarter->id) selected @endif>
                                        {{ $quarter->quarter_name }} </option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('quarter_id') }}
                            </div>
                        </div> --}}

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Location</label>
                            <textarea class="form-control" id="location" name="location" value="">{{ $branch->location }}</textarea>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('location') }}
                            </div>
                        </div>

                    </div>

                    <div class="position-relative form-group form-row">
                        <div class="col-md-12 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Full Address <span
                                    style="color: red">*</span></label>
                            <textarea class="form-control" id="full_address" name="full_address" value="" required>{{ $branch->full_address }}</textarea>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('full_address') }}
                            </div>
                        </div>


                    </div>
                    <button type="submit" class="mt-1 mb-2 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper">
                            Save & Back </i></button>
                    <a href="{{ url('branches/') }}" class="btn btn-secondary mb-2 mt-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
                </form>
            </div>
        </div>

    </div>

@endsection
