@extends('layouts.app')
@section('content')

    <div class="col-md-12">
        <div class="mb-3"></div> 
        <div class="main-card mb-3 card">
            <div class="card-header">Business Types List
            
                <div class="btn-actions-pane-right">
                    <a href="{{url('business_types/create')}}">
                        <button type="button" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-white">
                            <i class="fa fa-plus"></i> 
                            Add
                        </button>
                    </a>
                </div>
            </div>
            {{-- @if(session('successMsg')!=NULL)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('successMsg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif --}}
            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Business Type Name</th>
                            <th>Business Type Name (MM)</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    
                    @php $i=1; @endphp
                    @foreach($businesstypes as $businesstype)
                        <tr>
                            <td class="text-center text-muted">{{$i++}}</td>
                            <td>{{$businesstype["business_type_name"]}}</td>
                            <td>{{$businesstype["business_type_name_mm"]}}</td>

                            
                           
                            <td class="text-center">                                
                                <a href="business_types/view/{{$businesstype['id']}}">
                                    <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-info btn-sm"><i class="fa fa-eye"></i></button>
                                </a>
                                                                   
                                <a href="business_types/{{$businesstype['id']}}/edit">
                                    <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-warning"><i class="fa fa-pencil"></i></button>
                                </a>
                                <form action="{{ url('business_types/destroy',['id'=>$businesstype['id']]) }}" method="POST" class="d-inline-block" onsubmit="return confirm('Are you sure want to delete?')">
                                    @csrf
                                    @method('DELETE')                                       
                                    <button type="submit" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-danger"><i class="fa fa-trash"> </i></button>                                        
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-block text-center card-footer">
                <div class="">
                    @if ($businesstypes->lastPage() > 1)
                        <ul class="pagination d-flex flex-wrap justify-content-center ">
                            <li class="page-item {{ ($businesstypes->currentPage() == 1) ? ' disabled' : '' }}">
                                <a class="page-link" href="{{ $businesstypes->url(1) }}">Previous</a>
                            </li>
                            @for ($i = 1; $i <= $businesstypes->lastPage(); $i++)
                                <li class="page-item {{ ($businesstypes->currentPage() == $i) ? ' active' : '' }}">
                                    <a class="page-link" href="{{ $businesstypes->url($i) }}">{{ $i }}</a>
                                </li>
                            @endfor
                            <li class="page-item {{ ($businesstypes->currentPage() == $businesstypes->lastPage()) ? ' disabled' : '' }}">
                                <a class="page-link" href="{{ $businesstypes->url($businesstypes->currentPage()+1) }}" >Next</a>
                            </li>
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
                    
@if(session('successMsg')!=NULL)
@section('script')
    <script>
        statusAlert("{{session('successMsg')}}");
    </script>
@endsection
@endif