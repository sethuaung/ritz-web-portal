@extends('layouts.app')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Department Create          
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('departments/')}}" class="btn theme-color text-white">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>                    
                </div>
            </div>    </div>
    </div>            <div class="main-card mb-3 card">
        <div class="card-body">
            @if ($errors->any())
				<div class="alart alart-danger">
					<ul>
						@foreach($errors->all() as $error)
						<li>{{$error}}</li>
						@endforeach
					</ul>
				</div>
			@endif    
            <form class="needs-validation" method="post" action="{{route('departments.store')}}" enctype="multipart/form-data" novalidate>
            @csrf
            <div class="form-row pt-3">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Industry Name <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                    <select name="industry_id" id="industry_id" class="form-control" required>
                        <option value="" selected >Choose Industry</option>
                        @foreach ($industries as $industry) 
                        <option value="{{ $industry->id }}">{{ $industry->industry_name }}</option>
                        @endforeach
                        </select>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('industry_id')}}                                        
                        </div>
                    </div>
                </div>
                <div class="form-row pt-3">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Department Name(Eng) <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="name" name="department_name" placeholder="Example = Accounting" value="{{ old('department_name') }}" required>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('department_name')}}                                        
                        </div>
                    </div>
                </div>
                <div class="form-row pt-3">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom03">Department Name(MM) <span style="color: red">*</span></label>
                    </div>
                    <div class="col-md-9 mb-9">
                        <input type="text" class="form-control" id="name_mm" name="department_name_mm" placeholder=" ဥပမာ = စာရင်းဌာန" value="{{ old('department_name_mm') }}" required>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('department_name_mm')}}                                        
                        </div>
                    </div>
                </div>
                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                    <a href="{{url('departments/')}}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
            </form>
        </div>
    </div>
    
</div>
@endsection