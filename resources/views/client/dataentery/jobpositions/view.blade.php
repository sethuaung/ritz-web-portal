@extends('layouts.app')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Job Position Details          
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('jobpositions/')}}" class="btn btn-info">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>                    
                </div>
            </div>    </div>
    </div>            <div class="main-card mb-3 card">
        <div class="card-body">
             <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Industry Name</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{ $departments->industry_name }}
                </div>
            </div>
        
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Department Name</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{ $jobposition->department_name }}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Job Postion Name(Eng)</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                    {{$jobposition->job_position_name}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Job Postion Name(MM)</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$jobposition->job_position_name_mm}}
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection