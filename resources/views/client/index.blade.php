@extends('layouts.app')
@section('content')

    <div class="col-md-12">
        <div class="mb-3"></div>
        <div class="main-card mb-3 card">
            <div class="card-header">

                {{ $data_clienttitle}}
                <div class="btn-actions-pane-right">
                    <div role="group" class="btn-group-sm btn-group">
                        <a href="{{url('client/create')}}">
                            <button type="button" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-white">
                                <i class="fa fa-plus"></i>
                                Add
                            </button>
                        </a>
                        <a href="{{url('client?date=week')}}">
                            <button type="button" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-white">
                                <i class="fa fa-calendar"></i>
                                This Week
                            </button>
                        </a>
                        <a href="{{url('client?date=month')}}">
                            <button type="button" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-white">
                            <i class="fa fa-calendar"></i>
                            This Month
                            </button>
                        </a>
                        <a href="{{url('client?date=year')}}">
                            <button type="button" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-white">
                            <i class="fa fa-calendar"></i>
                            This Year
                            </button>
                        </a>
                        <a href="{{url('client?date=date')}}">
                            <button type="button" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-white">
                            <i class="fa fa-calendar"></i>
                            Today
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            <div class="widget-content p-2">
                <div class="widget-content-wrapper">
                    <div class="widget-content-left mr-3 ml-3">
                        <label class="m-0"> Show </label>
                    </div>
                    <div class="widget-content-left mr-1">
                        <div class="widget-heading">
                            <form method="GET" id="clientLengthForm">
                                <select name="client_length" id="clientLength" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm">
                                    <option value="10" @if(request()->client_length == 10) selected @endif>10</option>
                                    <option value="25" @if(request()->client_length == 25) selected @endif>25</option>
                                    <option value="50" @if(request()->client_length == 50) selected @endif>50</option>
                                    <option value="100" @if(request()->client_length == 100) selected @endif>100</option>
                                </select>
                            </form>
                        </div>
                    </div>
                <!--
                    <div class="widget-content-left ">
                        <label class="m-p">  </label>
                        <div class="btn-group">
                            <select class="custom-select custom-select-sm form-control form-control-sm text-center text-black" name="branchlist">
                                @foreach($data_branch as $branchlist)
                                  <option class="text-left text-black" value="{{ $branchlist->id }}">{{ $branchlist->branch_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                -->
                    <div class="widget-content-right">
                        <div class="btn-group">
                            <!-- <label class="m-2"> Search </label> -->
                            <form method="GET" >
                                <div class="input-group">
                                    <input type="text" name="search_clientlist" id="search_clientlist"
                                        value="{{ request()->get('search_clientlist') }}"
                                        class="form-control mr-1"
                                        placeholder="Search..."
                                        aria-label="Search"
                                        aria-describedby="button-addon2"
                                        >

                                    <button class="btn theme-color text-light mr-1" type="submit" id="button-addon2">
                                        <i class="fa fa-search"></i>
                                    </button>
                
                					@if(request()->has('clienttype'))
               							<input type="hidden" name="clienttype" value="{{ request()->get('clienttype') }}">
               						@endif

                                    <a href="{{url('client')}}" class="btn theme-color text-light">
                                        <i class="fa fa-refresh"></i>
                                    </a>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            {{-- @if(session('successMsg')!=NULL)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('successMsg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif --}}
            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center "> No </th>
                            <th class="sorting"> Portal Client ID</th>
            				<th> Main Client ID </th>
                            <th> Name </th>
                            <th> DOB </th>
                            <th> Gender </th>
                            <th> Phone </th>
                            <th> Status </th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=1; @endphp
                    @foreach($data_clients as $client)
                        <tr>
                            <td class="text-center text-muted">{{$i++}}</td>
                            <td>{{$client->client_uniquekey}}</td>
                    		<td>
                    			@if(DB::table('tbl_main_join_client')->where('portal_client_id', $client->client_uniquekey)->first())
                    				{{ DB::table('tbl_main_join_client')->where('portal_client_id', $client->client_uniquekey)->first()->main_client_code }}
                    			@endif
                    		</td>
                            <td>
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left mr-3">
                                            <div class="widget-content-left">
                                                @if($client->client_photo)
                                					<img width="40" height="40" class="rounded-circle" style="object-fit: cover;" src="{{asset('storage/app/public/clientphotos/'.$client->client_photo)}}" alt="">
												@else
                                					<img width="40" height="40" class="rounded-circle" style="object-fit: cover;" src="{{asset('storage/app/public/clientphotos/default_user_icon.jpg')}}" alt="">
												@endif
                                            </div>
                                        </div>
                                        <div class="widget-content-left flex2">
                                            <div class="widget-heading">{{$client->name}}</div>
                    						@php
                    							$nrc = "";
                    							if($client->nrc == null && $client->old_nrc == null) {
                                                	$nrc = "N/A";
                                                } else if($client->nrc == null) {
                                                	$nrc = $client->old_nrc;
                                                } else {
                                                	$nrc = $client->nrc;
                                                }
                    						@endphp
                                            <div class="widget-subheading pt-3">{{$nrc}}</div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>{{date('Y-m-d', strtotime($client->dob))}}</td>
                            <td>{{$client->gender}}</td>
                            <td>{{$client->phone_primary}}</td>

                            <td>
                               	 	@if($client->client_type == "new")
                                        <div class="badge badge-info">
                                            {{ $client->client_type }}
                                        </div>
                                    @elseif($client->client_type == "active")
                                        <div class="badge badge-success">
                                            {{ $client->client_type }}
                                        </div>
                                    @elseif($client->client_type == "dead")
                                        <div class="badge badge-danger">
                                            {{ $client->client_type }}
                                        </div>
                                    @elseif($client->client_type == "rejoin")
                                        <div class="badge badge-warning">
                                            {{ $client->client_type }}
                                        </div>
                                    @elseif($client->client_type == "reject")
                                        <div class="badge badge-secondary">
                                            {{ $client->client_type }}
                                        </div>
                                    @else
                                        <div class="badge badge-dark">
                                            {{ $client->client_type }}
                                        </div>
                                    @endif
                            </td>
                            <!-- <td class="text-center" style="width: 150px;">
                                <div class="pie-sparkline"><canvas width="45" height="45" style="display: inline-block; width: 45px; height: 45px; vertical-align: top;"></canvas></div>
                            </td> -->
                            <td class="text-center">
                                <a href="client/view/{{$client->client_uniquekey}}">
                                    <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn  btn-outline-info btn-sm"><i class="fa fa-eye"></i></button>
                                </a>

                                {{--<a href="client/{{$client->client_uniquekey}}/edit">
                                    <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-warning"><i class="fa fa-pencil" ></i></button>
                                </a>--}}
                                <form action="{{ url('client/destroy',['id'=>$client->client_uniquekey]) }}" method="POST" class="d-inline-block" onsubmit="return confirm('Are you sure want to delete?')">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-danger"><i class="fa fa-trash"> </i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-block text-center card-footer">
                <div class="">
                    {{ $data_clients->appends($_GET)->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@if(session('successMsg')!=NULL)
@section('script')
    <script>
        statusAlert("{{session('successMsg')}}");
    </script>
@endsection
@endif
