@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading ">
                    <div>Client Edit
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{ url('client/') }}" class="btn theme-color text-white">
                            <i class="pe-7s-back btn-icon-wrapper">Back</i>
                        </a>
                        <!--<button type="submit" class="btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper">Save</i></button>-->
                    </div>
                </div>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="needs-validation" action="{{ route('client.update', $clients->id) }}" method="POST"
            enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <ul class="tabs-animated-shadow nav-justified tabs-animated nav">
                        <li class="nav-item">
                            <a role="tab" class="nav-link active" id="tab-c1-0" data-toggle="tab" href="#clientinfo"
                                aria-selected="true">
                                <span class="nav-text">Personal Details</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-c1-1" data-toggle="tab" href="#familyinfo"
                                aria-selected="false">
                                <span class="nav-text">Family Info</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-c1-2" data-toggle="tab" href="#address"
                                aria-selected="false">
                                <span class="nav-text">Address</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-c1-3" data-toggle="tab" href="#employee"
                                aria-selected="false">
                                <span class="nav-text">JOB</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-c1-4" data-toggle="tab" href="#surveyowner"
                                aria-selected="false">
                                <span class="nav-text">Survey&Ownership</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-c1-5" data-toggle="tab" href="#clienttype"
                                aria-selected="false">
                                <span class="nav-text">ClientType</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-c1-6" data-toggle="tab" href="#image"
                                aria-selected="false">
                                <span class="nav-text">Image</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="clientinfo" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3">
                                        <div class="card-body">
                                            <div class="position-relative form-group form-row">
                                                <div class="col-md-12">
                                                    <!-- <label for="exampleEmail" class="">Client UniqueKey <span style="color: red">*</span></label> -->
                                                    <input type="text" disabled
                                                        placeholder="{{ $clients->client_uniquekey }}" class="form-control">
                                                    <input name="client_uniquekey" id="client_uniquekey"
                                                        value="{{ $clients->client_uniquekey }}" type="hidden"
                                                        class="form-control">
                                                </div>
                                            </div>
                                            <div class="position-relative form-group form-row">
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Name <span
                                                            style="color: red">*</span></label>
                                                    <input name="name" id="name" value="{{ $clients->name }}"
                                                        type="text" class="form-control">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('name') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Name MM </label>
                                                    <input name="name_mm" id="name_mm" value="{{ $clients->name_mm }}"
                                                        type="text" class="form-control">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('name') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="claender" style="font-weight: 600">Date of Birth <span
                                                            style="color: red">*</span></label>
                                                    <input name="dob" id="name" value="{{ $clients->dob }}"
                                                        type="date" class="form-control">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('dob') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Primary Phone
                                                    </label>
                                                    <input name="phone_primary" value="{{ $clients->phone_primary }}"
                                                        id="name" type="text" class="form-control"
                                                        maxlength="12">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('phone_primary') }}
                                                    </div>
                                                </div>
                                            </div>
                                            @if (!empty($clients->nrc))
                                                <div class="position-relative form-group form-row">
                                                    <div class="col-md-6 ">
                                                        <label for="validationCustom01" style="font-weight: 600">New NRC</label>
                                                        <div class="position-relative form-group form-row">
                                                            <div class="col-md-3">
                                                                <select name="nrc_1" id="nrc_1"
                                                                    class="form-control">
                                                                    <option value=""></option>
                                                                    @for ($i = 1; $i <= 14; $i++)
                                                                        <option value="{{ $i }}"
                                                                            @if ($exploded_nrc[0] == $i) selected="selected" @endif>
                                                                            {{ $i }}</option>
                                                                    @endfor
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="nrc_2" id="nrc_2"
                                                                    class="form-control">
                                                                    <option value="{{ $exploded_nrc[1] }}">
                                                                        {{ $exploded_nrc[1] }}</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3 ">
                                                                <select type="select" id="nrc_3" name="nrc_3"
                                                                    class="form-control">
                                                                    <option value="(N)"
                                                                        @if ($exploded_nrc[2] == 'N') selected="selected" @endif>
                                                                        (N)</option>
                                                                    <option value="(C)"
                                                                        @if ($exploded_nrc[2] == 'C') selected="selected" @endif>
                                                                        (C)</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3 ">
                                                                <input type="number" class="form-control" id="nrc_4"
                                                                    name="nrc_4" value="{{ $exploded_nrc[3] }}"
                                                                    placeholder=""
                                                                    oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                                    maxlength="6">
                                                            </div>
                                                        </div>

                                                        <div class="text-danger form-control-feedback">
                                                            {{ $errors->first('nrc_4') }}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <label for="exampleEmail" style="font-weight: 600">NRC Card ID </label>
                                                        <input name="nrc_card_id" id="nrc_card_id" type="text"
                                                            class="form-control" value="{{ $clients->nrc_card_id }}">
                                                        <div class="text-danger form-control-feedback">
                                                            {{ $errors->first('nrc_card_id') }}
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif(!empty($clients->old_nrc))
                                                <div class="position-relative form-group form-row">
                                                    <div class="col-md-6 ">
                                                        <label for="validationCustom01">Old NRC </label>
                                                        <div class="position-relative form-group form-row">
                                                            <div class="col-md-12">
                                                                <input type="text" class="form-control" id="old_nrc"
                                                                    name="old_nrc" value="{{ $clients->old_nrc }}"
                                                                    placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        <label for="exampleEmail" style="font-weight: 600">NRC Card ID</label>
                                                        <input name="nrc_card_id" id="nrc_card_id" type="text"
                                                            class="form-control" value="{{ $clients->nrc_card_id }}">
                                                        <div class="text-danger form-control-feedback">
                                                            {{ $errors->first('nrc_card_id') }}
                                                        </div>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="position-relative form-group form-row">
                                                    <div class="col-md-12 ">
                                                        <div class="position-relative form-group form-row">
                                                            <div class="col-md-12 ">
                                                                <input type="text" disabled class="form-control"
                                                                    placeholder="Empty NRC" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="position-relative form-group form-row">
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Gender <span
                                                            style="color: red">*</span></label>
                                                    <select type="select" id="exampleCustomSelect" name="gender"
                                                        class="form-control">
                                                        <!-- <option value="{{ $clients->gender }}">{{ $clients->gender }}</option> -->
                                                        <option @if ($clients->gender == 'Male') selected @endif>Male
                                                        </option>
                                                        <option @if ($clients->gender == 'Female') selected @endif>Female
                                                        </option>
                                                        <option @if ($clients->gender == 'Others') selected @endif>Others
                                                        </option>

                                                    </select>
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('gender') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Email </label>
                                                    <input name="email" id="name" value="{{ $clients->email }}"
                                                        type="email" class="form-control">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('email') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Secondary
                                                        Phone</label>
                                                    <input name="phone_secondary" value="{{ $clients->phone_secondary }}"
                                                        id="phone_secondary" type="number" class="form-control"
                                                        minlength="01000000"
                                                        oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                        maxlength="11">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('phone_secondary') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Blood Type</label>
                                                    <select type="select" id="blood" name="blood_type"
                                                        class="form-control">
                                                        <!-- <option value="">Choose Blood Type</option> -->
                                                        <option {{ $clients->blood_type == 'A' ? 'selected' : '' }}>A
                                                        </option>
                                                        <option {{ $clients->blood_type == 'B' ? 'selected' : '' }}>B
                                                        </option>
                                                        <option {{ $clients->blood_type == 'O' ? 'selected' : '' }}>O
                                                        </option>
                                                        <option {{ $clients->blood_type == 'AB' ? 'selected' : '' }}>AB
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="position-relative form-group form-row">
                                                <div class="col-md-3 mb-3">
                                                    <label for="claender" style="font-weight: 600">Religion </label>
                                                    <select type="select" id="religion" name="religion"
                                                        class="form-control">
                                                        <option value="">Choose Religion</option>
                                                        <option {{ $clients->religion == 'Buddhist' ? 'selected' : '' }}>
                                                            Buddhist</option>
                                                        <option {{ $clients->religion == 'Christian' ? 'selected' : '' }}>
                                                            Christian</option>
                                                        <option {{ $clients->religion == 'Hindu' ? 'selected' : '' }}>Hindu
                                                        </option>
                                                        <option {{ $clients->religion == 'Islamic' ? 'selected' : '' }}>
                                                            Islamic</option>
                                                        <option {{ $clients->religion == 'Jewish' ? 'selected' : '' }}>
                                                            Jewish</option>
                                                        <option
                                                            {{ $clients->religion == 'Other Religions' ? 'selected' : '' }}>
                                                            Other Religions</option>
                                                        <option
                                                            {{ $clients->religion == 'Not Religious' ? 'selected' : '' }}>
                                                            Not Religious</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Nationality</label>
                                                    <select type="select" id="nationality" name="nationality"
                                                        class="form-control">
                                                        <option value="">Choose Nationality</option>
                                                        <option {{ $clients->nationality == 'Kachin' ? 'selected' : '' }}>
                                                            Kachin</option>
                                                        <option {{ $clients->nationality == 'Kayin' ? 'selected' : '' }}>
                                                            Kayin</option>
                                                        <option {{ $clients->nationality == 'Kayah' ? 'selected' : '' }}>
                                                            Kayah</option>
                                                        <option {{ $clients->nationality == 'Chin' ? 'selected' : '' }}>
                                                            Chin</option>
                                                        <option {{ $clients->nationality == 'Mon' ? 'selected' : '' }}>Mon
                                                        </option>
                                                        <option {{ $clients->nationality == 'Bamar' ? 'selected' : '' }}>
                                                            Bamar</option>
                                                        <option {{ $clients->nationality == 'Rakhine' ? 'selected' : '' }}>
                                                            Rakhine</option>
                                                        <option {{ $clients->nationality == 'Shan' ? 'selected' : '' }}>
                                                            Shan</option>
                                                        <option {{ $clients->nationality == 'Unknown' ? 'selected' : '' }}>
                                                            Unknown</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Education</label>
                                                    <select type="select" id="education_id" name="education_id"
                                                        class="form-control">
                                                        @foreach ($educations as $education)
                                                            <option value="{{ $education->id }}"
                                                                @if ($clients->education_id == $education->id) selected @endif>
                                                                {{ $education->education_name }} </option>
                                                        @endforeach
                                                    </select>
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('education') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Other
                                                        Education</label>
                                                    <input name="other_education" value="{{ $clients->other_education }}"
                                                        id="other_eduction" type="text" class="form-control">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('other_education') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="familyinfo" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3">
                                        <div class="card-body">
                                            <div class="position-relative form-group form-row">
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Father Name</label>
                                                    <input name="father_name" value="{{ $clients->father_name }}"
                                                        id="client_uniquekey" type="text" class="form-control">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('father_name') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Marital
                                                        Status</label>
                                                    <select type="select" id="marital_status" name="marital_status"
                                                        class="form-control" required>
                                                        <!-- <option value="">Choose Marital Status</option> -->
                                                        <option
                                                            {{ $clients->marital_status == 'Single' ? 'selected' : '' }}>
                                                            Single</option>
                                                        <option
                                                            {{ $clients->marital_status == 'Married' ? 'selected' : '' }}>
                                                            Married</option>
                                                        <option
                                                            {{ $clients->marital_status == 'Divorced' ? 'selected' : '' }}>
                                                            Divorced</option>
                                                        <option
                                                            {{ $clients->marital_status == 'Single Father' ? 'selected' : '' }}>
                                                            Single Father</option>
                                                        <option
                                                            {{ $clients->marital_status == 'Single Mother' ? 'selected' : '' }}>
                                                            Single Mother</option>
                                                        <option {{ $clients->marital_status == 'None' ? 'selected' : '' }}>
                                                            None</option>

                                                    </select>
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('marital_status') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="claender" style="font-weight: 600">Spouse Name</label>
                                                    <input name="spouse_name" id="name"
                                                        value="{{ $clients->spouse_name }}" type="text"
                                                        class="form-control">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('spouse_name') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Occupation Of
                                                        Spouse</label>
                                                    <input name="occupation_of_spouse" id="name"
                                                        value="{{ $clients->occupation_of_spouse }}" type="text"
                                                        class="form-control">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('occupation_of_spouse') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="position-relative form-group form-row">

                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">No Of
                                                        Family</label>
                                                    <input type="text" id="no_of_family"
                                                        value="{{ $clients->no_of_family }}" name="no_of_family"
                                                        class="form-control">

                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('no_of_family') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">No of Working
                                                        Family</label>
                                                    <input type="text" id="no_of_working_family"
                                                        value="{{ $clients->no_of_working_family }}"
                                                        name="no_of_working_family" class="form-control">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('no_of_working_family') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">No Of
                                                        Children</label>
                                                    <input type="text" id="no_of_children"
                                                        value="{{ $clients->no_of_children }}" name="no_of_children"
                                                        class="form-control">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('no_of_children') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">No Of Working
                                                        Progeny</label>
                                                    <input type="text" id="no_of_working_progeny"
                                                        value="{{ $clients->no_of_working_progeny }}"
                                                        name="no_of_working_progeny" class="form-control">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('no_of_working_progeny') }}
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- <div class="print-pdf " >
                                            <div class="d-inline-block ">
                                                <a href="" class="btn btn-success">
                                                    <i class="fa fa-print fa-2x"></i>
                                                </a>
                                                <a href="" class="btn btn-danger">
                                                    <i class="fa fa-file-pdf-o fa-2x"></i>
                                                </a>
                                            </div>
                                        </div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="address" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3 ">
                                        <div class="card-body">
                                            <div class="position-relative form-group form-row">

                                                <div class="col-md-4 mb-4">
                                                    <label for="exampleEmail"
                                                        style="font-weight: 600">State/Division</label>
                                                    <select type="select" id="division_id" name="division_id"
                                                        class="form-control">
                                                        @foreach ($divisions as $division)
                                                            <option value="{{ $division->id }}"
                                                                @if ($clients->division_id == $division->id) selected @endif>
                                                                {{ $division->division_name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('division_id') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-4 mb-4">
                                                    <label for="exampleEmail" style="font-weight: 600">City</label>
                                                    <select type="select" id="city_id" name="city_id"
                                                        class="form-control">
                                                        @foreach ($cities as $city)
                                                            <option value="{{ $city->id }}"
                                                                @if ($clients->city_id == $city->id) selected @endif>
                                                                {{ $city->city_name }} </option>
                                                        @endforeach
                                                    </select>
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('city_id') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-4 mb-4">
                                                    <label for="exampleEmail" style="font-weight: 600">District</label>
                                                    <select type="select" id="district_id" name="district_id"
                                                        class="form-control">
                                                        @foreach ($districts as $district)
                                                            <option value="{{ $district->id }}"
                                                                @if ($clients->district_id == $district->id) selected @endif>
                                                                {{ $district->district_name }} </option>
                                                        @endforeach
                                                    </select>
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('district_id') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="position-relative form-group form-row">
                                                <div class="col-md-4 mb-4">
                                                    <label for="exampleEmail"
                                                        style="font-weight: 600">Township/Province</label>
                                                    <select type="select" id="township_id" name="township_id"
                                                        class="form-control">
                                                        @foreach ($townships as $township)
                                                            <option value="{{ $township->id }}"
                                                                @if ($clients->township_id == $township->id) selected @endif>
                                                                {{ $township->township_name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('township_id') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-4 mb-6">
                                                    <label for="exampleEmail" style="font-weight: 600">Town/Village/Group
                                                        Village</label>
                                                    <select type="select" id="village_id" name="village_id"
                                                        class="form-control">
                                                        @foreach ($villages as $village)
                                                            <option value="{{ $village->id }}"
                                                                @if ($clients->village_id == $village->id) selected @endif>
                                                                {{ $village->village_name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('village_id') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-4 mb-6">
                                                    <label for="exampleEmail" style="font-weight: 600">Quarter</label>
                                                    <select type="select" id="quarter_id" name="quarter_id"
                                                        class="form-control">
                                                        <option selected disabled>Select Quarter</option>
                                                        @foreach ($quarters as $quarter)
                                                            <option value="{{ $quarter->id }}"
                                                                @if ($clients->quarter_id == $quarter->id) selected @endif>
                                                                {{ $quarter->quarter_name }} </option>
                                                        @endforeach
                                                    </select>
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('quarter_id') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="position-relative form-group form-row">
                                                <div class="col-md-12 mb-12">
                                                    <label for="exampleEmail" style="font-weight: 600">Address</label>
                                                    <textarea name="address_primary" id="address_primary" value="" class="form-control">{{ $clients->address_primary }}</textarea>
                                                    <!-- <input name="address" id="name"   type="text" class="form-control"> -->
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('address') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="position-relative form-group form-row">
                                                <div class="col-md-12 mb-12">
                                                    <label for="exampleEmail" style="font-weight: 600">Address2</label>
                                                    <textarea name="address_secondary" id="address_secondary" value="" class="form-control">{{ $clients->address_secondary }}</textarea>
                                                    <!-- <input name="address2" id="name"   type="text" class="form-control"> -->
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('address2') }}
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="employee" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3 ">
                                        <div class="card-body">
                                            <div class="position-relative form-group form-row">
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Job Status</label>
                                                    <select type="select" id="job_status" name="job_status"
                                                        class="form-control">
                                                        <!-- <option value="{{ $clients->job_status }}">{{ $clients->job_status }}</option> -->
                                                        <option value="Has Job"
                                                            @if ($clients->job_status == 'Has Job') selected @endif>Has Job
                                                        </option>
                                                        <option value="None"
                                                            @if ($clients->job_status == 'None') selected @endif>None
                                                        </option>
                                                        <option value="Current Business"
                                                            @if ($clients->job_status == 'Current Business') selected @endif>Current
                                                            Business</option>
                                                        <option value="Yob"
                                                            @if ($clients->job_status == 'Yob') selected @endif>Yob</option>
                                                    </select>
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('position') }}
                                                    </div>
                                                </div>

                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Industry
                                                        Name</label>
                                                    <select name="industry_id" id="industry_id" class="form-control">
                                                        <option value=""></option>
                                                        @foreach ($industries as $industry)
                                                            <option value="{{ $industry->id }}"
                                                                @if ($clients->industry_id == $industry->id) selected @endif>
                                                                {{ $industry->industry_name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('industry_id') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Department
                                                        Name</label>
                                                    <select name="department_id" id="department_id" class="form-control">
                                                        <option value=""></option>
                                                        @foreach ($departments as $department)
                                                            <option value="{{ $department->id }}"
                                                                @if ($clients->department_id == $department->id) selected @endif>
                                                                {{ $department->department_name }} </option>
                                                        @endforeach
                                                    </select>
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('department_id') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Job Position
                                                        Name</label>
                                                    <select type="select" id="job_position_id" name="job_position_id"
                                                        class="form-control">
                                                        <option value=""></option>
                                                        @foreach ($jobpositions as $jobposition)
                                                            <option value="{{ $jobposition->id }}"
                                                                @if ($clients->job_position_id == $jobposition->id) selected @endif>
                                                                {{ $jobposition->job_position_name }} </option>
                                                        @endforeach
                                                    </select>
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('job_position_id') }}
                                                    </div>
                                                </div>

                                            </div>
                                            @if (!empty($clients->current_business && $clients->business_income))
                                                <div class="position-relative form-group form-row">
                                                    <div class="col-md-6 " id="business_name2">
                                                        <label for="exampleEmail" style="font-weight: 600">Current
                                                            Business Name</label>
                                                        <input name="current_business" id="current_business"
                                                            type="text" class="form-control"
                                                            value="{{ $clients->current_business }}">
                                                        <div class="text-danger form-control-feedback">
                                                            {{ $errors->first('current_business') }}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" id="business2">
                                                        <label for="exampleEmail" style="font-weight: 600">Business
                                                            Income(Monthly)</label>
                                                        <input name="business_income" id="business_income" type="number"
                                                            class="form-control"
                                                            value="{{ $clients->business_income }}">
                                                        <div class="text-danger form-control-feedback">
                                                            {{ $errors->first('business_income') }}
                                                        </div>
                                                        <div id="income">
                                                            <p id="income1"></p>
                                                        </div>

                                                    </div>
                                                </div>
                                            @endif
                                            <div class="position-relative form-group form-row">
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Company
                                                        Name</label>
                                                    <input name="company_name" id="company_name"
                                                        value="{{ $clients->company_name }}" type="text"
                                                        class="form-control">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('company_name') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Working
                                                        Phone</label>
                                                    <input name="company_phone" value="{{ $clients->company_phone }}"
                                                        id="working_phone" type="text" class="form-control"
                                                        minlength="01000000"
                                                        oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                        maxlength="11">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('work_phone') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Salary</label>
                                                    <input name="salary" id="salary" value="{{ $clients->salary }}"
                                                        type="number" class="form-control">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('salary') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Salary
                                                        Yearly</label>
                                                    <input name="salary_yearly" id="salary_yearly" value=""
                                                        type="number" disabled class="form-control">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('experience') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="position-relative form-group form-row">
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Experience
                                                        Year</label>
                                                    <input name="experience" id="experience"
                                                        value="{{ $clients->experience }}" type="text"
                                                        class="form-control"
                                                        oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                        maxlength="2">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('experience') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-3">
                                                    <label for="exampleEmail" style="font-weight: 600">Experience
                                                        Month</label>
                                                    <input name="experience2" id="experience2"
                                                        value="{{ $clients->experience2 }}" type="text"
                                                        class="form-control"
                                                        oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                                        maxlength="2">
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('experience') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="exampleEmail" style="font-weight: 600">Company
                                                        Address</label>
                                                    <textarea name="company_address" value="" id="company_address" class="form-control">{{ $clients->company_address }}</textarea>
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('company_address') }}
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="surveyowner" role="tabpanel">
                            <div class="position-relative form-group row">
                                
                                <div class="col-md-6 ">
                                    <div class="position-relative form-group row">
                                        @if ($assetstype_unremovable)
                                            <div class="col-md-6">
                                                <div class="main-card">
                                                    <div class="card-body">
                                                        <h5 class="card-title">Unremovable Assets</h5>
                                                        <div class="position-relative form-group">
                                                            <div>
                                                                @php
                                                                    $values2 = explode(',', $assetstype_unremovable);
                                                                @endphp
                                                                <div class="custom-checkbox custom-control">
                                                                    <input type="checkbox" id="unremovableCheckbox"
                                                                        name="unremovable_checkbox[]"
                                                                        @if (in_array('Building', $values2)) checked @endif
                                                                        value="Building" class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                        for="unremovableCheckbox">Building</label>
                                                                </div>
                                                                <div class="custom-checkbox custom-control">
                                                                    <input type="checkbox" id="unremovableCheckbox2"
                                                                        name="unremovable_checkbox[]"
                                                                        @if (in_array('Land', $values2)) checked @endif
                                                                        value="Land" class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                        for="unremovableCheckbox2">Land</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        @if ($assetstype_removable)
                                            <div class="col-md-6">
                                                <div class="main-card">
                                                    <div class="card-body">
                                                        <h5 class="card-title">Removable Assets</h5>
                                                        <div class="position-relative form-group">
                                                            <div>
                                                                @php
                                                                    $values3 = explode(',', $assetstype_removable);
                                                                @endphp
                                                                <div class="custom-checkbox custom-control">
                                                                    <input type="checkbox" id="removableCheckbox"
                                                                        name="removable_checkbox[]"
                                                                        @if (in_array('Vehicle', $values3)) checked @endif
                                                                        value="Vehicle" class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                        for="removableCheckbox">Vehicle</label>
                                                                </div>
                                                                <div class="custom-checkbox custom-control">
                                                                    <input type="checkbox" id="removableCheckbox1"
                                                                        name="removable_checkbox[]"
                                                                        @if (in_array('Machine for Agri', $values3)) checked @endif
                                                                        value="Machine for Agri"
                                                                        class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                        for="removableCheckbox1">Machine for Agri</label>
                                                                </div>
                                                                <div class="custom-checkbox custom-control">
                                                                    <input type="checkbox" id="removableCheckbox2"
                                                                        name="removable_checkbox[]"
                                                                        @if (in_array('Machine for Product Making', $values3)) checked @endif
                                                                        value="Machine for Product Making"
                                                                        class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                        for="removableCheckbox2">Machine for Product
                                                                        Making</label>
                                                                </div>
                                                                <div class="custom-checkbox custom-control">
                                                                    <input type="checkbox" id="removableCheckbox3"
                                                                        name="removable_checkbox[]"
                                                                        @if (in_array('Vissable Asset', $values3)) checked @endif
                                                                        value="Vissable Asset"
                                                                        class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                        for="removableCheckbox3">Vissable Asset</label>
                                                                </div>
                                                                <div class="custom-checkbox custom-control">
                                                                    <input type="checkbox" id="removableCheckbox4"
                                                                        name="removable_checkbox[]"
                                                                        @if (in_array('Stock in Hand', $values3)) checked @endif
                                                                        value="Stock in Hand"
                                                                        class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                        for="removableCheckbox4">Stock in Hand</label>
                                                                </div>
                                                                <div class="custom-checkbox custom-control">
                                                                    <input type="checkbox" id="removableCheckbox5"
                                                                        name="removable_checkbox[]"
                                                                        @if (in_array('Animal', $values3)) checked @endif
                                                                        value="Animal" class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                        for="removableCheckbox5">Animal</label>
                                                                </div>
                                                                <div class="custom-checkbox custom-control">
                                                                    <input type="checkbox" id="removableCheckbox6"
                                                                        name="removable_checkbox[]"
                                                                        @if (in_array('Other Removable Asset', $values3)) checked @endif
                                                                        value="Other Removable Asset"
                                                                        class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                        for="removableCheckbox6">Other Removable
                                                                        Asset</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div>
                                        <label for="exampleEmail" style="font-weight: 600">Purchased Price in Kyats <span
                                                style="color: red">*</span></label>
                                        <input name="purchase_price" id="purchase_price" type="number"
                                            class="form-control" placeholder="Enter Purchase Price of Your Ownerships"
                                            value="{{ $clients->purchase_price }}"
                                            oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                            maxlength="9" >
                                        <div class="text-danger form-control-feedback">
                                            {{ $errors->first('purchase_price') }}
                                        </div>
                                    </div><br>
                                    <div>
                                        <label for="exampleEmail" style="font-weight: 600">Current Value in Kyats <span
                                                style="color: red">*</span></label>
                                        <input name="current_value" id="current_value" type="number"
                                            class="form-control" placeholder="Enter Current Value Your Ownerships"
                                            value="{{ $clients->current_value }}"
                                            oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                            maxlength="9" >
                                        <div class="text-danger form-control-feedback">
                                            {{ $errors->first('current_value') }}
                                        </div>
                                    </div><br>
                                    <div>
                                        <label for="exampleEmail" style="font-weight: 600">Quantity <span
                                                style="color: red">*</span></label>
                                        <select type="select" id="exampleCustomSelect" name="quantity"
                                            class="form-control" >
                                            <option value="">Choose Quantity</option>
                                            <option {{ $clients->quantity == '1' ? 'selected' : '' }} value="1">1
                                            </option>
                                            <option {{ $clients->quantity == '2' ? 'selected' : '' }} value="2">2
                                            </option>
                                            <option {{ $clients->quantity == '3' ? 'selected' : '' }} value="3">3
                                            </option>
                                            <option {{ $clients->quantity == '4' ? 'selected' : '' }} value="4">4
                                            </option>
                                            <option {{ $clients->quantity == '5' ? 'selected' : '' }} value="5">5
                                            </option>
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{ $errors->first('gender') }}
                                        </div>
                                    </div><br>
                                </div>
                            </div>

                            <div class="position-relative form-group row">
                                <div class="col-md-3">
                                    <label for="exampleEmail" style="font-weight: 600">Document Photo 1</label>
                                    <input name="ownership_docu_photo1" id="name" type="file"
                                        class="form-control"><br>
                                    @if ($clients->attach_1)
                                        <img src="{{ asset('storage/app/public/client_ownership_documents/' . $clients->attach_1) }}"
                                            width='200px' height="130px" alt="" title="">
                                    @endif
                                    <div class="text-danger form-control-feedback">
                                        {{ $errors->first('ownership_docu_photo1') }}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="exampleEmail" style="font-weight: 600">Document Photo 2</label>
                                    <input name="ownership_docu_photo2" id="name" type="file"
                                        class="form-control"><br>
                                    @if ($clients->attach_2)
                                        <img src="{{ asset('storage/app/public/client_ownership_documents/' . $clients->attach_2) }}"
                                            width='200px' height="130px" alt="" title="">
                                    @endif
                                    <div class="text-danger form-control-feedback">
                                        {{ $errors->first('ownership_docu_photo2') }}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="exampleEmail" style="font-weight: 600">Document Photo 3</label>
                                    <input name="ownership_docu_photo3" id="name" type="file"
                                        class="form-control"><br>
                                    @if ($clients->attach_3)
                                        <img src="{{ asset('storage/app/public/client_ownership_documents/' . $clients->attach_3) }}"
                                            width='200px' height="130px" alt="" title="">
                                    @endif
                                    <div class="text-danger form-control-feedback">
                                        {{ $errors->first('ownership_docu_photo3') }}
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="exampleEmail" style="font-weight: 600">Document Photo 4</label>
                                    <input name="ownership_docu_photo4" id="name" type="file"
                                        class="form-control"><br>
                                    @if ($clients->attach_4)
                                        <img src="{{ asset('storage/app/public/client_ownership_documents/' . $clients->attach_4) }}"
                                            width='200px' height="130px" alt="" title="">
                                    @endif
                                    <div class="text-danger form-control-feedback">
                                        {{ $errors->first('ownership_docu_photo4') }}
                                    </div>
                                </div>
                            </div>



                            {{-- <div class="print-pdf " >
                            <div class="d-inline-block ">
                                <a href="" class="btn btn-success">
                                    <i class="fa fa-print fa-2x"></i>
                                </a>
                                <a href="" class="btn btn-danger">
                                    <i class="fa fa-file-pdf-o fa-2x"></i>
                                </a>
                            </div>
                        </div> --}}
                        </div>
                        <div class="tab-pane" id="clienttype" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3">
                                        <div class="card-body">

                                            <div class="position-relative form-group form-row">
                                                <div class="col-md-3 ">
                                                    <label for="exampleEmail" style="font-weight: 600">Client Type
                                                    </label>
                                                    <select type="select" id="client_type" name="client_type"
                                                        class="form-control">
                                                        <!-- <option value="{{ $clients->client_type }}">{{ $clients->client_type }}</option> -->
                                                        <option {{ $clients->client_type == 'new' ? 'selected' : '' }}
                                                            value="new">New Member</option>
                                                        <option {{ $clients->client_type == 'active' ? 'selected' : '' }}
                                                            value="active">Active Member</option>
                                                        <option {{ $clients->client_type == 'dropout' ? 'selected' : '' }}
                                                            value="dropout">Drop-Out Member</option>
                                                        <option {{ $clients->client_type == 'dead' ? 'selected' : '' }}
                                                            value="dead">Dead Member</option>
                                                        <option {{ $clients->client_type == 'rejoin' ? 'selected' : '' }}
                                                            value="rejoin">Rejoin Member</option>
                                                        <option
                                                            {{ $clients->client_type == 'substitute' ? 'selected' : '' }}
                                                            value="substitute">Substitude Member</option>
                                                        <option {{ $clients->client_type == 'dormant' ? 'selected' : '' }}
                                                            value="dormant">Dormant Member</option>
                                                        <option {{ $clients->client_type == 'reject' ? 'selected' : '' }}
                                                            value="reject">Reject Member</option>
                                                    </select>

                                                </div>
                                                <div class="col-md-3 ">
                                                    <label for="exampleEmail" style="font-weight: 600">Client Type-Center
                                                    </label>
                                                    <select class="form-control" name="client_type_center"
                                                        id="client_type_center">
                                                        <option selected>Choose Center Type</option>
                                                        @foreach ($data_center as $centerlist)
                                                            <option value="{{ $centerlist->center_uniquekey }}"
                                                                {{ $clients->center_id == $centerlist->center_uniquekey ? 'selected' : '' }}>
                                                                {{ $centerlist->center_uniquekey }}
                                                            </option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <div class="col-md-3 ">
                                                    <label for="exampleEmail" style="font-weight: 600">Client Type-Group
                                                    </label>
                                                    <select class="form-control" name="client_type_group"
                                                        id="client_type_group">
                                                        <option selected>Choose Group Type</option>
                                                        @foreach ($data_group as $grouplist)
                                                            <option value="{{ $grouplist->group_uniquekey }}"
                                                                {{ $clients->group_id == $grouplist->group_uniquekey ? 'selected' : '' }}>
                                                                {{ $grouplist->group_uniquekey }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="col-md-3 ">
                                                    <label for="exampleEmail" style="font-weight: 600">Guarantors</label>
                                                    <select class="form-control" name="client_guarantor">
                                                        <option selected>Choose Guarantor</option>
                                                        @foreach ($data_guarantor as $guarantorlist)
                                                            <option value="{{ $guarantorlist->guarantor_uniquekey }}"
                                                                {{ $clients->guarantors_id == $guarantorlist->guarantor_uniquekey ? 'selected' : '' }}>
                                                                {{ $guarantorlist->name }}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>
                                                    
                                                <div class="position-relative form-group form-row">
                                            		<div class="col-md-6">
                                            			<label for="you_are_a_center_leader" style="font-weight: 600">You are a center leader</label>
                                                		<select id="you_are_a_center_leader" name="you_are_a_center_leader" class="form-control">
                                                			<option value="no" @if($clients->you_are_a_center_leader == "no") selected @endif>no</option>
                                                			<option value="yes" @if($clients->you_are_a_center_leader == "yes") selected @endif>yes</option>
                                                		</select>
                                            		</div>
                                            		<div class="col-md-6">
                                            			<label for="you_are_a_group_leader" style="font-weight: 600">You are a group leader</label>
                                                		<select id="you_are_a_group_leader" name="you_are_a_group_leader" class="form-control">
                                                			<option value="no" @if($clients->you_are_a_group_leader == "no") selected @endif>no</option>
                                                			<option value="yes" @if($clients->you_are_a_group_leader == "yes") selected @endif>yes</option>
                                                		</select>
                                            		</div>
                                        		</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="image" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="main-card mb-3 ">
                                        <div class="card-body">
                                            <div class="position-relative form-group form-row">
                                                <div class="col-md-12 mb-12">
                                                    <label for="exampleEmail" style="font-weight: 600">Client
                                                        Photo</label>
                                                    <input name="client_photo" id="name" type="file"
                                                        class="form-control">
                                                    @if($clients->client_photo)
    													<img src="{{ asset('public/storage/clientphotos/' . $clients->client_photo) }}"
                                                        width="100px" height="100px" alt="" title="">
													@else
    													<img src="{{ asset('storage/app/public/clientphotos/No_Image_Available.jpg') }}"
                                                        width="100px" height="100px" alt="" title="">
													@endif

                                                    
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('client_photo') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="position-relative form-group form-row">
                                                @if (empty($clients->recognition_front && $clients->recognition_back))
                                                    <div class="col-md-12 mb-2">
                                                        <label for="exampleEmail" style="font-weight: 600">NRC
                                                            Recommendation Photo</label>
                                                        <input name="nrc_recommendation" id="name" type="file"
                                                            class="form-control">
                                                        @if($clients->nrc_recommendation)
                                                        	<img src="{{ asset('public/storage/clientphotos/' . $clients->nrc_recommendation) }}"
                                                            width="100px" height="100px" alt="" title="">
                                                        @else
    														<img src="{{ asset('storage/app/public/clientphotos/No_Image_Available.jpg') }}"
                                                        	width="100px" height="100px" alt="" title="">
														@endif
                                                        <div class="text-danger form-control-feedback">
                                                            {{ $errors->first('recongnition_front_photo') }}
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="col-md-6 mb-6">
                                                        <label for="exampleEmail" style="font-weight: 600">NRC
                                                            Front</label>
                                                        <input name="recognition_front_photo" id="name"
                                                            type="file" class="form-control">
                                                        @if($clients->recognition_front)
                                                        	<img src="{{ asset('public/storage/clientphotos/' . $clients->recognition_front) }}"
                                                            width="100px" height="100px" alt="" title="">
                                                        @else
    														<img src="{{ asset('storage/app/public/clientphotos/No_Image_Available.jpg') }}"
                                                        	width="100px" height="100px" alt="" title="">
														@endif
                                                        <div class="text-danger form-control-feedback">
                                                            {{ $errors->first('recongnition_front_photo') }}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 mb-6">
                                                        <label for="exampleEmail" style="font-weight: 600">NRC
                                                            Back</label>
                                                        <input name="recognition_back_photo" id="name"
                                                            type="file" class="form-control">
                                                        @if($clients->recognition_back)
                                                        	<img src="{{ asset('public/storage/clientphotos/' . $clients->recognition_back) }}"
                                                            width="100px" height="100px" alt="" title="">
                                                        @else
    														<img src="{{ asset('storage/app/public/clientphotos/No_Image_Available.jpg') }}"
                                                        	width="100px" height="100px" alt="" title="">
														@endif
                                                        <div class="text-danger form-control-feedback">
                                                            {{ $errors->first('recognition_back_photo') }}
                                                        </div>
                                                    </div>
                                                @endif

                                            </div>
                                            <div class="position-relative form-group form-row">
                                                <div class="col-md-6 mb-6">
                                                    <label for="exampleEmail" style="font-weight: 600">Registeration
                                                        Family form Front</label>
                                                    <input name="registration_family_front_photo" id="name"
                                                        type="file" class="form-control">
                                                    @if($clients->registration_family_form_front)	
                                                    	<img src="{{ asset('public/storage/clientphotos/' . $clients->registration_family_form_front) }}"
                                                        width="100px" height="100px" alt="" title="">
                                                    @else
    													<img src="{{ asset('storage/app/public/clientphotos/No_Image_Available.jpg') }}"
                                                       	width="100px" height="100px" alt="" title="">
													@endif
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('registration_family_front_photo') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mb-6">
                                                    <label for="exampleEmail" style="font-weight: 600">Registeration
                                                        Family form back</label>
                                                    <input name="registration_family_back_photo" id="name"
                                                        type="file" class="form-control">
                                            		@if($clients->registration_family_form_back)
                                                    	<img src="{{ asset('public/storage/clientphotos/' . $clients->registration_family_form_back) }}"
                                                        width="100px" height="100px" alt="" title="">
                                                    @else
    													<img src="{{ asset('storage/app/public/clientphotos/No_Image_Available.jpg') }}"
                                                      	width="100px" height="100px" alt="" title="">
													@endif
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('registration_family_back_photo') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="position-relative form-group form-row">
                                                <div class="col-md-6 mb-6">
                                                    <label for="exampleEmail" style="font-weight: 600">Finger Print
                                                        Bio</label>
                                                    <input name="finger_print_bio" id="name" type="file"
                                                        class="form-control">
                                                    @if($clients->finger_print_bio)
                                                    	<img src="{{ asset('public/storage/clientphotos/' . $clients->finger_print_bio) }}"
                                                        width="100px" height="100px" alt="" title="">
                                                    @else
    													<img src="{{ asset('storage/app/public/clientphotos/No_Image_Available.jpg') }}"
                                                       	width="100px" height="100px" alt="" title="">
                                                    @endif
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('finger_print_bio') }}
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mb-6">
                                                    <label for="exampleEmail" style="font-weight: 600">Government
                                                        Recommendations</label>
                                                    <input name="government_recommendations_photo" id="name"
                                                        type="file" class="form-control">
                                                    @if($clients->government_recommendations_photo)
                                                    	<img src="{{ asset('public/storage/clientphotos/' . $clients->government_recommendations_photo) }}"
                                                        width="100px" height="100px" alt="" title="">
                                                    @else
   														<img src="{{ asset('storage/app/public/clientphotos/No_Image_Available.jpg') }}"
                                                       	width="100px" height="100px" alt="" title="">
                                                    @endif
                                                    <div class="text-danger form-control-feedback">
                                                        {{ $errors->first('government_recommendation_photo') }}
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- <div class="print-pdf " >
                                            <div class="d-inline-block ">
                                                <a href="" class="btn btn-success">
                                                    <i class="fa fa-print fa-2x"></i>
                                                </a>
                                                <a href="" class="btn btn-danger">
                                                    <i class="fa fa-file-pdf-o fa-2x"></i>
                                                </a>
                                            </div>
                                        </div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="mt-1 mb-2 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save &
                    Back </i></button>
            <a href="{{ url('client/') }}" class="btn btn-secondary mb-2 mt-1">
                <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
            </a>
        </form>
    </div>
@endsection
