@extends('layouts.app')
@section('content')


<div class="app-main__inner">
    <div class="app-page-title mb-0">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Client Create</div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                    <a href="{{url('client/')}}" class="btn theme-color text-light">
                        <i class="pe-7s-back btn-icon-wrapper">Back</i>
                    </a>
					{{--<button type="submit" class="btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper">Save</i></button>--}}
                </div>
            </div>
        </div>
    </div>

    <form class="needs-validation"   action="{{route('client.store')}}" method="POST" enctype="multipart/form-data" novalidate id="clientCreateForm">
        @csrf
        <div class="main-card mb-3 card">
            <div class="card-body">
               <ul class="tabs-animated-shadow nav-justified tabs-animated nav">
                    <li class="nav-item">
                        <a role="tab" class="nav-link active" id="tab-c1-0" data-toggle="tab" href="#clientinfo" aria-selected="true">
                            <span class="nav-text">Personal Details</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a role="tab" class="nav-link" id="tab-c1-1" data-toggle="tab" href="#familyinfo" aria-selected="false">
                            <span class="nav-text">Family Info</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a role="tab" class="nav-link" id="tab-c1-2" data-toggle="tab" href="#address" aria-selected="false">
                            <span class="nav-text">Address</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a role="tab" class="nav-link" id="tab-c1-3" data-toggle="tab" href="#employee" aria-selected="false">
                            <span class="nav-text">JOB</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a role="tab" class="nav-link" id="tab-c1-4" data-toggle="tab" href="#surveyowner" aria-selected="false">
                            <span class="nav-text">Survey&Ownership</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a role="tab" class="nav-link" id="tab-c1-5" data-toggle="tab" href="#clienttype" aria-selected="false">
                            <span class="nav-text">ClientType</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a role="tab" class="nav-link" id="tab-c1-6" data-toggle="tab" href="#image" aria-selected="false">
                            <span class="nav-text">Image</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="clientinfo" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="main-card  ">
                                    <div class="card-body">
                                        <div class="position-relative form-group form-row" hidden>
                                            <div class="col-md-12 ">
                                                <input name="client_uniquekey_show" id="client_uniquekey" placeholder="Auto Generate Client Uniquekey" type="text" disabled class="form-control">
                                            </div>
                                        </div>
                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Name <span style="color: red">*</span></label>
                                                <input name="name" id="name"    type="text" class="form-control" autofocus="autofocus"  placeholder="Enter Name" value="{{ old('name') }}" required>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('name')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Name MM </label>
                                                <input name="name_mm" id="name_mm"    type="text" class="form-control" placeholder="Enter Name MM" value="{{ old('name_mm') }}">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('name_mm')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="claender" style="font-weight: 600">Date of Birth <span style="color: red">*</span></label>
                                                <input name="dob" id="name"   type="date" class="form-control" value="{{ old('dob') }}" required>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('dob')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Primary Phone <span style="color: red">*</span></label>
                                                <input name="phone_primary" id="name"   type="number" class="form-control" placeholder="Example : 091234567" value="{{ old('phone_primary') }}" minlength="01000000" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "11" required>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('phone_primary')}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-3 ">
                                                <div class="position-relative form-group form-row">
                                                    <div class="col-md-6 ">
                                                        <label for="exampleEmail" style="font-weight: 600">Has NRC <span style="color: red">*</span></label>
                                                        <select type="select" id="has_nrc" name="has_nrc" class="form-control" >
                                                            <option value="" selected disabled>Has NRC??</option>
                                                            <option {{ old('nrc_type') == 'Yes' ? 'selected' : '' }}>Yes</option>
                                                            <option {{ old('nrc_type') == 'No' ? 'selected' : '' }}>No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6 " id="nrc_type1">
                                                        <label for="exampleEmail" style="font-weight: 600">NRC Type <span style="color: red">*</span></label>
                                                        <select type="select" id="nrc_type_select1" name="nrc_type" class="form-control"  >
                                                            <option value="" selected disabled>Choose NRC Type</option>
                                                            <option {{ old('nrc_type') == 'Old NRC' ? 'selected' : '' }}>Old NRC</option>
                                                            <option {{ old('nrc_type') == 'New NRC' ? 'selected' : '' }}>New NRC</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6 " id="nrc_type2">
                                                <label for="exampleEmail" style="font-weight: 600">NRC Type <span style="color: red">*</span></label>
                                                <select type="select" id="nrc_type_select2" name="nrc_type" class="form-control" >
                                                    <option value="" selected disabled>Not Has NRC</option>
                                                    <option {{ old('nrc_type') == 'Still Applying' ? 'selected' : '' }}>Still Applying(လျှောက်ထားဆဲ)</option>
                                                    <option {{ old('nrc_type') == 'Loss NRC' ? 'selected' : '' }}>Loss NRC(ပျောက်ဆုံး)</option>
                                                </select>
                                            </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6" id="oldNRC">
                                                <label for="validationCustom01">Old NRC <span style="color: red">*</span></label>
                                                <input name="old_nrc" id="nrc"   type="text" class="form-control"  value="{{ old('old_nrc') }}">

                                            </div>
                                            <div class="col-md-6" id="stillNRC">
                                                <label for="validationCustom01">Still Applying <span style="color: red">*</span></label>
                                                <input name="nrc" id="nrc"   type="" class="form-control" placeholder="Please select recommendation file in the image tag" value="{{ old('nrc') }}" disabled>

                                            </div>
                                            <div class="col-md-6" id="lossNRC">
                                                <label for="validationCustom01"> Loss NRC <span style="color: red">*</span></label>
                                                <input name="nrc" id="nrc"   type="" class="form-control" placeholder="Please select recommendation file in the image tag" value="{{ old('nrc') }}" disabled>

                                            </div>
                                            <div class="col-md-6 " id="newNRC">
                                                <label for="validationCustom01">New NRC <span style="color: red">*</span></label>
                                                    <div class="position-relative form-group form-row">
                                                        <div class="col-md-3">
                                                            <select name="nrc_1" id="nrc_1" class="form-control">
                                                                <option value=""></option>
                                                                @for ($i = 1; $i <= 14; $i++)
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <select name="nrc_2" id="nrc_2" class="form-control">
                                                                <option value=""></option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3 ">
                                                            <select type="select" id="nrc_3" name="nrc_3" class="form-control">
                                                                <option value=""></option>
                                                                <option value="(N)">(N)</option>
                                                                <option value="(C)">(C)</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-3 ">
                                                        <input type="number" class="form-control" id="nrc_4" name="nrc_4" value="{{ old('nrc_4') }}" placeholder="" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "6">
                                                    </div>
                                                </div>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('nrc_4')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">NRC Card ID <span style="color: red">*</span></label>
                                                <input name="nrc_card_id" id="nrc_card_id"   type="text" class="form-control" placeholder="Example : AA 123456" value="{{ old('nrc_card_id') }}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                        maxlength="9" required>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('nrc_card_id')}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Gender</label>
                                                <select type="select" id="exampleCustomSelect" name="gender" class="form-control">
                                                    <option value="">Choose Gender</option>
                                                    <option {{ old('gender') == 'Male' ? 'selected' : '' }}>Male</option> 
                                                    <option {{ old('gender') == 'Female' ? 'selected' : '' }}>Female</option>
                                                    <option {{ old('gender') == 'Others' ? 'selected' : '' }}>Others</option>

                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('gender')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Email</label>
                                                <input name="email" id="name"   type="email" class="form-control" placeholder="Example : example@gmail.com" value="{{ old('email') }}" >
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('email')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Secondary Phone</label>
                                                <input name="phone_secondary" id="phone_secondary"   type="number" class="form-control" placeholder="Example : 091234567" value="{{ old('phone_secondary') }}" minlength="01000000" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "11">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('phone_secondary')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Blood Type </label>
                                                <select type="select" id="blood" name="blood_type" class="form-control" >
                                                    <option value="">Choose Blood Type</option>
                                                    <option {{ old('blood_type') == 'A' ? 'selected' : '' }}>A</option>
                                                    <option {{ old('blood_type') == 'B' ? 'selected' : '' }}>B</option>
                                                    <option {{ old('blood_type') == 'O' ? 'selected' : '' }}>O</option>
                                                    <option {{ old('blood_type') == 'AB' ? 'selected' : '' }}>AB</option>
                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('blood_type')}}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-3 ">
                                                <label for="claender" style="font-weight: 600">Religion</label>
                                                <select type="select" id="religion" name="religion" class="form-control" >
                                                    <option value="">Choose Religion</option>
                                                    <option {{ old('religion') == 'Buddhist' ? 'selected' : '' }}>Buddhist</option>
                                                    <option {{ old('religion') == 'Christian' ? 'selected' : '' }}>Christian</option>
                                                    <option {{ old('religion') == 'Hindu' ? 'selected' : '' }}>Hindu</option>
                                                    <option {{ old('religion') == 'Islamic' ? 'selected' : '' }}>Islamic</option>
                                                    <option {{ old('religion') == 'Jewish' ? 'selected' : '' }}>Jewish</option>
                                                    <option {{ old('religion') == 'Other Religions' ? 'selected' : '' }}>Other Religions</option>
                                                    <option {{ old('religion') == 'Not Religious' ? 'selected' : '' }}>Not Religious</option>
                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('religion')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Nationality</label>
                                                <select type="select" id="nationality" name="nationality" class="form-control" >
                                                    <option value="">Choose Religion</option>
                                                    <option {{ old('nationality') == 'Kachin' ? 'selected' : '' }}>Kachin</option>
                                                    <option {{ old('nationality') == 'Kayin' ? 'selected' : '' }}>Kayin</option>
                                                    <option {{ old('nationality') == 'Kayah' ? 'selected' : '' }}>Kayah</option>
                                                    <option {{ old('nationality') == 'Chin' ? 'selected' : '' }}>Chin</option>
                                                    <option {{ old('nationality') == 'Mon' ? 'selected' : '' }}>Mon</option>
                                                    <option {{ old('nationality') == 'Bamar' ? 'selected' : '' }}>Bamar</option>
                                                    <option {{ old('nationality') == 'Rakhine' ? 'selected' : '' }}>Rakhine</option>
                                                    <option {{ old('nationality') == 'Shan' ? 'selected' : '' }}>Shan</option>
                                                    <option {{ old('nationality') == 'Unknown' ? 'selected' : '' }}>Unknown</option>
                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('nationality')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Education </label>
                                                <select name="education_id" id="education" class="form-control">
                                                    <option value="">Select Education</option>
                                                        @foreach ($educations as $education)
                                                        <option value="{{ $education->id }}" {{(old("education_id") == $education->education_name ? "selected":"") }} >{{ $education->education_name }}</option>
                                                        @endforeach
                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('education')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Other Education</label>
                                                <input name="other_education" id="other_eduction"   type="text" class="form-control" placeholder="Example : Diploma in Business" value="{{ old('other_education') }}">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('other_education')}}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="reset_btn" hidden>
                                            <div class="d-inline-block ">
                                                <button type="reset"  class="btn-wide btn-outline-2x mr-md-2 btn btn-outline-focus btn-sm">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="tab-pane" id="familyinfo" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="main-card ">
                                    <div class="card-body">
                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Father Name  </label>
                                                <input name="father_name" id="father_name" autofocus="autofocus"    type="text" class="form-control" placeholder="Example : U Hla" value="{{ old('father_name') }}">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('father_name')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Marital Status</label>
                                                <select type="select" id="marital_status" name="marital_status" class="form-control">
                                                    <option value="">Choose Marital Status</option>
                                                    <option value="Single">Single</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Divorced">Divorced</option>
                                                    <option value="Single Father">Single Father</option>
                                                    <option value="Single Mother">Single Mother</option>

                                                    <option value="None">None</option>

                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('marital_status')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="claender" style="font-weight: 600">Spouse Name</label>
                                                <input name="spouse_name" id="spouse_name"   type="text" disabled class="form-control" placeholder="Example : Mya Mya" value="{{ old('spouse_name') }}">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('spouse_name')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Occupation Of Spouse</label>
                                                <input name="occupation_of_spouse" id="occupation" disabled type="text" class="form-control" placeholder="Example : Teacher" value="{{ old('occupation_of_spouse') }}">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('occupation_of_spouse')}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="position-relative form-group form-row">

                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">No Of Dependent</label>
                                                <input type="number" id="no_of_family" name="no_of_family" class="form-control" placeholder="Example : 4" value="{{ old('no_of_family') }}" >

                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('no_of_family')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">No of Working Family </label>
                                                <input type="number" id="no_of_working_family" name="no_of_working_family" class="form-control" placeholder="Example : 2" value="{{ old('no_of_working_family') }}" >
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('no_of_working_family')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">No Of Children</label>
                                                <input type="number" id="no_of_children" name="no_of_children" class="form-control" placeholder="Example : 2" value="{{ old('no_of_children') }}">

                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('no_of_children')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">No of person in family</label>
                                                <input type="number" id="no_of_working_progeny" name="no_of_working_progeny" class="form-control" placeholder="Example : 1" value="{{ old('no_of_working_progeny') }}">

                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('no_of_working_progeny')}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="reset_btn " hidden>
                                            <div class="d-inline-block ">
                                                <button type="reset"  class="btn btn-warning">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="address" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="main-card  ">
                                    <div class="card-body">

                                        <div class="position-relative form-group form-row">

                                            <div class="col-md-4 ">
                                                <label for="exampleEmail" style="font-weight: 600">State / Region <span style="color: red">*</span></label>
                                                <select type="select" id="division_id" name="division_id" class="form-control" required>
                                                    <option value="">Select State</option>
                                                    @foreach ($divisions as $division)
                                                        <option value="{{ $division->code }}" {{(old("division_id")== $division->name ? "selected":"")}} >{{ $division->name }} / {{$division->description}}</option>
                                                    @endforeach
                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('division_id')}}
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <label for="exampleEmail" style="font-weight: 600">District / Division <span style="color: red">*</span></label>
                                                <select type="select" id="district_id" name="district_id" class="form-control" required>
                                                    <option></option>
                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('district_id')}}
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <label for="exampleEmail" style="font-weight: 600">Township<span style="color: red">*</span></label>
                                                <select type="select" id="township_id" name="township_id" class="form-control" required>
                                                    <option></option>
                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('township_id')}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-4 ">
                                                <label for="exampleEmail" style="font-weight: 600">Town / Village / Group Village<span style="color: red">*</span></label>
                                                <select type="select" id="village_id" name="village_id" class="form-control" required>
                                                    <option></option>
                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('village_id')}}
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <label for="exampleEmail" style="font-weight: 600">Ward / Small Village</label>
                                                <select type="select" id="ward_id" name="ward_id" class="form-control">
                                                    <option></option>
                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('ward_id')}}
                                                </div>
                                            </div>
                                            {{-- <div class="col-md-4 ">
                                                <label for="exampleEmail" style="font-weight: 600">Quarter</label>
                                                <select type="select" id="quarter_id" name="quarter_id" class="form-control">
                                                    <option  ></option>
                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('quarter_id')}}
                                                </div>
                                            </div> --}}
                                            <div class="col-md-4">
                                                <label for="exampleEmail" style="font-weight: 600">Home No</label>
                                                <input type="text" name="home_no" id="home_no" autofocus="autofocus"  class="form-control" placeholder="No(1) " value="{{ old('home_no') }}">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('home_no')}}
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <div class="position-relative form-group form-row"> --}}
                                            {{-- <div class="col-md-4">
                                                <label for="exampleEmail" style="font-weight: 600">Home No</label>
                                                <input type="text" name="home_no" id="home_no" autofocus="autofocus"  class="form-control" placeholder="No(1) " value="{{ old('home_no') }}">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('home_no')}}
                                                </div>
                                            </div> --}}
                                            {{-- <div class="col-md-4">
                                                <label for="exampleEmail" style="font-weight: 600">Street Name</label>
                                                <input type="text" name="street_name" id="street_name" autofocus="autofocus"  class="form-control" placeholder="Thumingalar Street " value="{{ old('street_name') }}">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('street_name')}}
                                                </div>
                                            </div> --}}
                                        {{-- </div> --}}
                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-12 ">
                                                <label for="exampleEmail" style="font-weight: 600">Address</label>
                                                <textarea name="address_primary" id="address_primary" readonly  class="form-control" placeholder="No(1), 18th Street, Latha Township, Yangon" required>{{ old('address_primary') }}</textarea>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('address_primary')}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-12">
                                                <label for="exampleEmail" style="font-weight: 600">Address2</label>
                                                <textarea name="address_secondary" id="address_secondary" placeholder="No(1), 18th Street, Latha Township, Yangon" class="form-control">{{ old('address_secondary') }}</textarea>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('address_secondary')}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="reset_btn " hidden>
                                            <div class="d-inline-block ">
                                                <button type="reset"  class="btn ">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="employee" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="main-card ">
                                    <div class="card-body">

                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Job Status </label>
                                                <select type="select" id="job_status" name="job_status" class="form-control" >
                                                    <option >Choose Job Status</option>
                                                    <option {{ old('job_status') == 'Has Job' ? 'selected' : '' }} value="Has Job">Has job</option>
                                                    <option {{ old('job_status') == 'None' ? 'selected' : '' }} value="None">None</option>
                                                    <option {{ old('job_status') == 'Current Business' ? 'selected' : '' }} value="Current Business">Current Business</option>
                                                    <option {{ old('job_status') == 'Yob' ? 'selected' : '' }} value="Yob">Yob</option>
                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('position')}}
                                                </div>
                                            </div>

                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Industry Name</label>
                                                <select name="industry_id" id="industry_id" placeholder="Select Industry" disabled  class="form-control" type="select" >
                                                    <option></option>
                                                        @foreach ($industries as $industry)
                                                        <option value="{{ $industry->id }}" {{(old("industry_id") == $industry->industry_name)}} >{{ $industry->industry_name }}</option>
                                                        @endforeach
                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('industry_id')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Department Name</label>
                                                <select name="department_id" id="department_id" type="select"placeholder="Select Department" disabled  class="form-control">
                                                    <option></option>
                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('department_id')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Job Position Name</label>
                                                <select type="select" id="job_position_id" name="job_position_id" placeholder="Select Job" disabled  class="form-control">
                                                    <option></option>
                                                </select>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('job_position_id')}}
                                                </div>
                                            </div>

                                        </div>
                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-6 " id="business_name">
                                                <label for="exampleEmail" style="font-weight: 600">Current Business Name</label>
                                                <input name="current_business" id="current_business"   type="text" class="form-control" placeholder="Enter Your own business" value="{{ old('current_business') }}">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('current_business')}}
                                                </div>
                                            </div>
                                            <div class="col-md-6" id="business">
                                                <label for="exampleEmail" style="font-weight: 600">Business Income(Monthly)</label>
                                                <input name="business_income" id="business_income"   type="number" class="form-control" placeholder="091234567" value="{{ old('business_income') }}">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('business_income')}}
                                                </div>
                                                <div id="income"><p id="income1"></p></div>

                                            </div>

                                        </div>
                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Company Name</label>
                                                <input name="company_name" id="company_name"   type="text" disabled class="form-control" placeholder="Mikko Food Industry Co., Ltd." value="{{ old('company_name') }}">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('company_name')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Working Phone</label>
                                                <input name="company_phone" id="working_phone"   type="number" disabled class="form-control" placeholder="091234567" value="{{ old('company_phone') }}" minlength="01000000" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "11">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('work_phone')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Salary(Per Month)</label>
                                                <input name="salary" id="salary"   type="number" disabled class="form-control" placeholder="200000" value="{{ old('salary') }}" onkeyup="if(this.value<0){this.value= 0}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "8">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('salary')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Salary(Yearly)</label>
                                                <input name="salary_yearly" id="salary_yearly"   type="number" disabled class="form-control" disabled placeholder="2400000" value="{{ old('salary_yearly') }}">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('experience')}}
                                                </div>
                                            </div>

                                        </div>
                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Experience Years</label>
                                                <input name="experience" id="experience"   type="text" disabled class="form-control" placeholder="3 Years" value="{{ old('experience') }}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "2">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('experience')}}
                                                </div>
                                            </div>
                                            <div class="col-md-3 ">
                                                <label for="exampleEmail" style="font-weight: 600">Experience Months</label>
                                                <input name="experience2" id="experience2"   type="text" disabled class="form-control" placeholder=" 3 Months" value="{{ old('experience2') }}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "2">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('experience2')}}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="exampleEmail" style="font-weight: 600">Company Address</label>
                                                <textarea name="company_address" id="company_address" disabled  placeholder="No. 6, Street U Tun Myat Alley, Yangon" class="form-control" >{{ old('company_address') }}</textarea>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('company_address')}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="reset_btn " hidden>
                                            <div class="d-inline-block ">
                                                <button type="reset"  class="btn ">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="surveyowner" role="tabpanel">
                        <div class="position-relative form-group row">
                            <div class="col-md-6 ">
                                <div>
                                    <label for="exampleEmail" style="font-weight: 600">Assets Type</label>
                                        <select type="select" id="assets_type" name="assets_type" class="form-control" >
                                            <option value="">Choose Assets Type</option>
                                            <option value="removable">Removable Assets</option>
                                            <option value="unremovable">Unremovable Assets</option>
                                        </select>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('assets_type')}}
                                    </div>
                                </div><br>
                                <div class="position-relative form-group row">
                                    <div class="col-md-6" id="unremovable_assets">
                                        <div class="main-card ">
                                            <div class="card-body"><h5 class="card-title">Unremovable Assets</h5>
                                                <div class="position-relative form-group">
                                                    <div>
                                                        <div class="custom-checkbox custom-control">
                                                            <input type="checkbox" id="unremovableCheckbox" name="unremovable_checkbox[]" value="Building" @if(is_array(old('unremovable_checkbox')) && in_array('Building', old('unremovable_checkbox'))) checked @endif class="custom-control-input">
                                                            <label class="custom-control-label" for="unremovableCheckbox">Building</label>
                                                        </div>
                                                        <div class="custom-checkbox custom-control">
                                                            <input type="checkbox" id="unremovableCheckbox2" name="unremovable_checkbox[]" value="Land" @if(is_array(old('unremovable_checkbox')) && in_array('Land', old('unremovable_checkbox'))) checked @endif class="custom-control-input">
                                                            <label class="custom-control-label" for="unremovableCheckbox2">Land</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" id="removable_assets">
                                        <div class="main-card">
                                            <div class="card-body"><h5 class="card-title">Removable Assets</h5>
                                                <div class="position-relative form-group">
                                                    <div>
                                                        <div class="custom-checkbox custom-control">
                                                            <input type="checkbox" id="removableCheckbox" name="removable_checkbox[]" value="Vehicle" @if(is_array(old('removable_checkbox')) && in_array('Vehicle', old('removable_checkbox'))) checked @endif class="custom-control-input">
                                                            <label class="custom-control-label" for="removableCheckbox">Vehicle</label>
                                                        </div>
                                                        <div class="custom-checkbox custom-control">
                                                            <input type="checkbox" id="removableCheckbox1" name="removable_checkbox[]" value="Machine for Agri" @if(is_array(old('removable_checkbox')) && in_array('Machine for Agri', old('removable_checkbox'))) checked @endif class="custom-control-input">
                                                            <label class="custom-control-label" for="removableCheckbox1">Machine for Agri</label>
                                                        </div>
                                                        <div class="custom-checkbox custom-control">
                                                            <input type="checkbox" id="removableCheckbox2" name="removable_checkbox[]" value="Machine for Product Making" @if(is_array(old('removable_checkbox')) && in_array('Machine for Product Making', old('removable_checkbox'))) checked @endif  class="custom-control-input">
                                                            <label class="custom-control-label" for="removableCheckbox2">Machine for Product Making</label>
                                                        </div>
                                                        <div class="custom-checkbox custom-control">
                                                            <input type="checkbox" id="removableCheckbox3" name="removable_checkbox[]" value="Vissable Asset" @if(is_array(old('removable_checkbox')) && in_array('Vissable Asset', old('removable_checkbox'))) checked @endif  class="custom-control-input">
                                                            <label class="custom-control-label" for="removableCheckbox3">Vissable Asset</label>
                                                        </div>
                                                        <div class="custom-checkbox custom-control">
                                                            <input type="checkbox" id="removableCheckbox4" name="removable_checkbox[]" value="Stock in Hand" @if(is_array(old('removable_checkbox')) && in_array('Stock in Hand', old('removable_checkbox'))) checked @endif class="custom-control-input">
                                                            <label class="custom-control-label" for="removableCheckbox4">Stock in Hand</label>
                                                        </div>
                                                        <div class="custom-checkbox custom-control">
                                                            <input type="checkbox" id="removableCheckbox5" name="removable_checkbox[]" value="Animal" @if(is_array(old('removable_checkbox')) && in_array('Animal', old('removable_checkbox'))) checked @endif class="custom-control-input">
                                                            <label class="custom-control-label" for="removableCheckbox5">Animal</label>
                                                        </div>
                                                        <div class="custom-checkbox custom-control">
                                                            <input type="checkbox" id="removableCheckbox6" name="removable_checkbox[]" value="Other Removable Asset" @if(is_array(old('removable_checkbox')) && in_array('Other Removable Asset', old('removable_checkbox'))) checked @endif class="custom-control-input">
                                                            <label class="custom-control-label" for="removableCheckbox6">Other Removable Asset</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div>
                                    <label for="exampleEmail" style="font-weight: 600">Purchased Price in Kyats</label>
                                    <input name="purchase_price" id="purchase_price" type="number" class="form-control" placeholder="Enter Purchase Price of Your Ownerships" value="{{ old('purchase_price') }}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "9" >
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('purchase_price')}}
                                    </div>
                                </div><br>
                                <div>
                                    <label for="exampleEmail" style="font-weight: 600">Current Value in Kyats</label>
                                    <input name="current_value" id="current_value" type="number" class="form-control" placeholder="Enter Current Value Your Ownerships" value="{{ old('current_value') }}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" maxlength = "9" >
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('current_value')}}
                                    </div>
                                </div><br>
                                <div>
                                    <label for="exampleEmail" style="font-weight: 600">Quantity </label>
                                    <select type="select" id="exampleCustomSelect" name="quantity" class="form-control" >
                                        <option value="">Choose Quantity</option>
                                        <option {{ old('quantity') == '1' ? 'selected' : '' }}>1</option>
                                        <option {{ old('quantity') == '2' ? 'selected' : '' }}>2</option>
                                        <option {{ old('quantity') == '3' ? 'selected' : '' }}>3</option>
                                        <option {{ old('quantity') == '4' ? 'selected' : '' }}>4</option>
                                        <option {{ old('quantity') == '5' ? 'selected' : '' }}>5</option>
                                    </select>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('quantity')}}
                                    </div>
                                </div><br>
                            </div>
                        </div>
                        <div class="position-relative form-group row">
                            <div class="col-md-3">
                                <label for="exampleEmail" style="font-weight: 600">Document Photo 1</label>
                                <input name="ownership_docu_photo1" id="name" type="file" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('ownership_docu_photo1')}}
                                </div>
                            </div><br>
                            <div class="col-md-3">
                                <label for="exampleEmail" style="font-weight: 600">Document Photo 2</label>
                                <input name="ownership_docu_photo2" id="name" type="file" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('ownership_docu_photo2')}}
                                </div>
                            </div><br>
                            <div class="col-md-3">
                                <label for="exampleEmail" style="font-weight: 600">Document Photo 3</label>
                                <input name="ownership_docu_photo3" id="name" type="file" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('ownership_docu_photo3')}}
                                </div>
                            </div><br>
                            <div class="col-md-3">
                                <label for="exampleEmail" style="font-weight: 600">Document Photo 4</label>
                                <input name="ownership_docu_photo4" id="name" type="file" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('ownership_docu_photo4')}}
                                </div>
                            </div>
                        </div>
                        <div class="reset_btn " hidden>
                            <div class="d-inline-block ">
                                <button type="reset"  class="btn ">Reset</button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="clienttype" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="main-card ">
                                    <div class="card-body">

                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-4 ">
                                                <label for="exampleEmail" style="font-weight: 600">Client Type <span style="color: red">*</span></label>
                                                <select type="select" id="client_type" name="client_type" class="form-control" >
                                                    <option value="">Choose client type</option>
                                                    <option value="new" >New Member</option>
                                                    <option value="active" >Active Member</option>
                                                    <option value="dropout" >Drop-Out Member</option>
                                                    <option value="dead" >Dead Member</option>
                                                    <option value="rejoin" >Rejoin Member</option>
                                                    <option value="substitude" >Substitude Member</option>
                                                    <option value="dormant" >Dormant Member</option>
                                                    <option value="reject" >Reject Member</option>
                                                </select>

                                            </div>

                                            <div class="col-md-4 ">
                                                <label for="exampleEmail" style="font-weight: 600">Client Type-Center <span style="color: red">*</span></label>
                                                <select class="form-control" name="client_type_center" id="client_type_center" required>
                                                    <option class="text-left text-black" value="" disabled selected>Choose Center</option>
                                                    @foreach($data_center as $centerlist)
                                                    <option class="text-left text-black" value="{{ $centerlist->center_uniquekey }}">{{ $centerlist->main_center_code != NULL ? $centerlist->main_center_code : $centerlist->center_uniquekey }}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                            <div class="col-md-4 ">
                                                <label for="exampleEmail" style="font-weight: 600">Client Type-Group <span style="color: red">*</span></label>
                                                <select class="form-control" name="client_type_group" id="client_type_group" required>
                                                    {{-- @foreach($data_group as $grouplist)
                                                    <option class="text-left text-black" value="{{ $grouplist->group_uniquekey }}">{{ $grouplist->group_uniquekey}}</option>
                                                    @endforeach --}}
                                                </select>
                                            </div>

                                        </div>
                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-6">
                                            	<label for="you_are_a_center_leader" style="font-weight: 600">You are a center leader</label>
                                                <select id="you_are_a_center_leader" name="you_are_a_center_leader" class="form-control">
                                                	<option value="no" selected>no</option>
                                                	<option value="yes">yes</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                            	<label for="you_are_a_group_leader" style="font-weight: 600">You are a group leader</label>
                                                <select id="you_are_a_group_leader" name="you_are_a_group_leader" class="form-control">
                                                	<option value="no" selected>no</option>
                                                	<option value="yes">yes</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-6 ">
                                                <label for="exampleEmail" style="font-weight: 600">Guarantors</label>
                                                <select class="form-control" name="client_guarantor">
                                                    @foreach($data_guarantor as $guarantorlist)
                                                    <option class="text-left text-black" value="{{ $guarantorlist->guarantor_uniquekey }}">{{ $guarantorlist->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-6 ">
                                                <label for="exampleEmail" style="font-weight: 600">Loan Officer Name<span style="color: red"> *</span></label>
                                                <select class="form-control" name="loan_officer" id="loan_officer" required>
                                                    {{--@foreach($data_loan_officers as $loan_officer)
                                                    <option class="text-left text-black" value="{{ $loan_officer->staff_code }}">{{ $loan_officer->name}}</option>
                                                    @endforeach--}}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-6 ">
                                                <label for="calendar" style="font-weight: 600">Registration Date <span style="color: red">*</span></label>
                                                <input name="reg_date" id="date" type="date" class="form-control" value="{{ old('reg-date') }}" required>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('reg-date')}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="reset_btn " hidden>
                                            <div class="d-inline-block ">
                                                <button type="reset"  class="btn ">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="image" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="main-card mb-3">
                                    <div class="card-body">

                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-12 ">
                                                <label for="exampleEmail" style="font-weight: 600">Client Photo <span style="color: red">*</span></label>
                                                <input name="client_photo" id="name"   type="file" class="form-control" required>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('client_photo')}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="position-relative form-group form-row" id="noNrcPhoto">
                                            <div class="col-md-12 ">
                                                <label for="exampleEmail" style="font-weight: 600">Please Upload Your Recommendations for NRC <span style="color: red">*</span></label>
                                                <input name="nrc_recommendation" id="nrcRecom"   type="file" class="form-control" >
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('recongnition_front_photo')}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="position-relative form-group form-row" id="hasNrcPhoto">
                                            <div class="col-md-6 ">
                                                <label for="exampleEmail" style="font-weight: 600">NRC Front <span style="color: red">*</span></label>
                                                <input name="recognition_front_photo" id="nrcFront"   type="file" class="form-control" >
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('recongnition_front_photo')}}
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <label for="exampleEmail" style="font-weight: 600">NRC Back <span style="color: red">*</span></label>
                                                <input name="recognition_back_photo" id="nrcBack"   type="file" class="form-control">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('recognition_back_photo')}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="position-relative form-group form-row">
                                            <div class="col-md-6 ">
                                                <label for="exampleEmail" style="font-weight: 600">Registeration Family form Front <span style="color: red">*</span></label>
                                                <input name="registration_family_front_photo" id="name"   type="file" class="form-control" required>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('registration_family_front_photo')}}
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <label for="exampleEmail" style="font-weight: 600">Registeration Family form back <span style="color: red">*</span></label>
                                                <input name="registration_family_back_photo" id="name"   type="file" class="form-control" required>
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('registration_family_back_photo')}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="position-relative form-group form-row">
                                        <div class="col-md-6 ">
                                                <label for="exampleEmail" style="font-weight: 600">Finger Print Bio</label>
                                                <input name="finger_print_bio" id="name"   type="file" class="form-control">
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('finger_print_bio')}}
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <label for="exampleEmail" style="font-weight: 600">Government Recommendations</label>
                                                <input name="government_recommendations_photo" id="name"   type="file" class="form-control" >
                                                <div class="text-danger form-control-feedback">
                                                    {{$errors->first('government_recommendations_photo')}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="reset_btn " hidden>
                                            <div class="d-inline-block ">
                                                <button type="reset"  class="btn ">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="mt-1 mb-2 btn btn-success" id="clientSubmit"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                <a href="{{url('client/')}}" class="btn btn-secondary mb-2 mt-1">
                    <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                </a>
            </div>
        </div>
    </form>
</div>

@endsection
                                                
@if ($errors->any())
    @section('script')

    @php
    $errlist = "";
        foreach($errors->all() as $e) {
            $errlist .= $e."<br>";
        }
    @endphp
    <script>
        errorAlert("{{ $errlist }}");
    </script>
    @endsection
@endif

