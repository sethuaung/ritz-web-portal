@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading ">
                    <div>Client Details View
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{ url('client/') }}" class="btn theme-color">
                            <i class="pe-7s-back btn-icon-wrapper text-light">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                <ul class="tabs-animated-shadow nav-justified tabs-animated nav">
                    <li class="nav-item">
                        <a role="tab" class="nav-link active" id="tab-c1-0" data-toggle="tab" href="#clientinfo"
                            aria-selected="true">
                            <span class="nav-text font-weight-bold">PERSONAL DETAILS</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a role="tab" class="nav-link" id="tab-c1-4" data-toggle="tab" href="#surveyowner"
                            aria-selected="false">
                            <span class="nav-text font-weight-bold">DOCUMENTS</span>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active py-3" id="clientinfo" role="tabpanel">
                        <div class="card rounded ml-4">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        @if ($clients->client_photo)
                                            <img src="{{ asset('public/storage/clientphotos/' . $clients->client_photo) }}"
                                                class="rounded-circle border-secondary ml-4" width='100px'height="100px"
                                                style="border: 3px solid; object-fit: cover;">
                                        @else
                                            <img src="{{ asset('storage/app/public/clientphotos/default_user_icon.jpg') }}"
                                                class="rounded-circle border-secondary ml-4" width='100px'height="100px"
                                                style=" object-fit: cover;">
                                        @endif


                                    </div>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-row">
                                                    <div class="col-md-5 text-left">
                                                        <label class="font-weight-bold">Client ID</label>
                                                    </div>
                                                    <div class="col-md-7 ">
                                                        : {{ $clients->client_uniquekey }}
                                                    </div>
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-5 text-left">
                                                        <label class="font-weight-bold">Name </label>
                                                    </div>
                                                    <div class="col-md-7 ">
                                                        : {{ $clients->name }}
                                                    </div>
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-5 text-left">
                                                        <label class="font-weight-bold">Name MM </label>
                                                    </div>
                                                    @if (!empty($clients->name_mm))
                                                        <div class="col-md-7 ">
                                                            : {{ $clients->name_mm }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-7 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-row">
                                                    <div class="col-md-5 text-left">
                                                        <label class="font-weight-bold">Date Of Birth </label>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        : {{ $clients->dob }}
                                                    </div>
                                                </div><br>
                                                @if (!empty($clients->nrc))
                                                    <div class="form-row">
                                                        <div class="col-md-5 text-left">
                                                            <label class="font-weight-bold">Nationality ID </label>
                                                        </div>
                                                        @if (!empty($clients->nrc))
                                                            <div class="col-md-7 ">
                                                                : {{ $clients->nrc }}
                                                            </div>
                                                        @else
                                                            <div class="col-md-7 ">
                                                                : N/A
                                                            </div>
                                                        @endif
                                                    </div><br>
                                                    <div class="form-row">
                                                        <div class="col-md-5 text-left">
                                                            <label class="font-weight-bold">NRC Card ID </label>
                                                        </div>
                                                        @if (!empty($clients->nrc_card_id))
                                                            <div class="col-md-7 ">
                                                                : {{ $clients->nrc_card_id }}
                                                            </div>
                                                        @else
                                                            <div class="col-md-7 ">
                                                                : N/A
                                                            </div>
                                                        @endif
                                                    </div>
                                                @elseif(!empty($clients->old_nrc))
                                                    <div class="form-row">
                                                        <div class="col-md-5 text-left">
                                                            <label class="font-weight-bold">Old NRC </label>
                                                        </div>
                                                        <div class="col-md-7 ">
                                                            : {{ $clients->old_nrc }}
                                                        </div>
                                                    </div><br>
                                                    <div class="form-row">
                                                        <div class="col-md-5 text-left">
                                                            <label class="font-weight-bold">NRC Card ID </label>
                                                        </div>
                                                        <div class="col-md-7 ">
                                                            : {{ $clients->nrc_card_id }}
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="form-row">
                                                        <div class="col-md-5 text-left">
                                                            <label class="font-weight-bold">Nationality ID </label>
                                                        </div>

                                                        <div class="col-md-7 ">
                                                            : N/A
                                                        </div>
                                                    </div><br>
                                                    <div class="form-row">
                                                        <div class="col-md-5 text-left">
                                                            <label class="font-weight-bold">NRC Card ID </label>
                                                        </div>
                                                        <div class="col-md-7 ">
                                                            : N/A
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-row">
                                                    <div class="col-md-6 text-left">
                                                        <label class="font-weight-bold">Phone Primary</label>
                                                    </div>
                                                    @if (!empty($clients->phone_primary))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->phone_primary }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 text-left">
                                                        <label class="font-weight-bold">Phone Secondary</label>
                                                    </div>
                                                    @if (!empty($clients->phone_secondary))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->phone_secondary }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 text-left">
                                                        <label class="font-weight-bold">Email</label>
                                                    </div>
                                                    @if (!empty($clients->email))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->email }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-card mb-2 py-4 ">
                            <div class="card-body">
                                <div class="position-relative form-group form-row mb-4 ml-2">
                                    <div class="col-md-4">
                                        <div class="card">
                                            <div class="card-header theme-color">
                                                <h5 class="text-light">
                                                    <i class="fas fa-info-circle"></i>
                                                    <span class="ml-2">DETAILS</span>
                                                </h5>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-row">
                                                    <div class="col-md-6 text-left">
                                                        <label class="font-weight-bold">Gender</label>
                                                    </div>
                                                    <div class="col-md-6 ">
                                                        : {{ $clients->gender }}
                                                    </div>
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 text-left">
                                                        <label class="font-weight-bold">Blood Type</label>
                                                    </div>
                                                    @if (!empty($clients->blood_type))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->blood_type }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 text-left">
                                                        <label class="font-weight-bold">Religion</label>
                                                    </div>
                                                    @if (!empty($clients->religion))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->religion }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 text-left">
                                                        <label class="font-weight-bold">Nationality</label>
                                                    </div>
                                                    @if (!empty($clients->nationality))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->nationality }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 text-left">
                                                        <label class="font-weight-bold">Education</label>
                                                    </div>
                                                    @if (!empty($clients->education_name))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->education_name }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 text-left">
                                                        <label class="font-weight-bold">Other Education</label>
                                                    </div>
                                                    @if (!empty($clients->other_education))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->other_education }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div>
                                                <hr>
                                                <div class="form-row">
                                                    <div class="col-md-6">
                                                        <label class="font-weight-bold">Division/State</label>
                                                    </div>
                                                    @if (!empty($division->name))
                                                        <div class="col-md-6 ">
                                                            : {{ $division->name }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 ">
                                                        <label class="font-weight-bold">District</label>
                                                    </div>
                                                    @if (!empty($district->name))
                                                        <div class="col-md-6 ">
                                                            : {{ $district->name }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 ">
                                                        <label class="font-weight-bold">Township/Province</label>
                                                    </div>
                                                    @if (!empty($township->name))
                                                        <div class="col-md-6 ">
                                                            : {{ $township->name }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 ">
                                                        <label class="font-weight-bold">Quarter</label>
                                                    </div>
                                                    @if (!empty($ward->name))
                                                        <div class="col-md-6 ">
                                                            : {{ $ward->name }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 ">
                                                        <label class="font-weight-bold">Village</label>
                                                    </div>
                                                    @if (!empty($village->name))
                                                        <div class="col-md-6 ">
                                                            : {{ $village->name }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-4 ">
                                                        <label class="font-weight-bold">Address Primary</label>
                                                    </div>
                                                    <div class="col-md-8 ">
                                                        : {{ $clients->address_primary }}
                                                    </div>
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 ">
                                                        <label class="font-weight-bold">Address Secondary</label>
                                                    </div>
                                                    @if (!empty($clients->address_secondary))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->address_secondary }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br><br>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 pl-4">
                                        <div class="card">
                                            <div class="card-header theme-color">
                                                <h5 class="text-light">
                                                    <i class="fas fa-user"></i>
                                                    <span class="ml-2 text-uppercase">FAMILY INFO</span>
                                                </h5>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-row">
                                                    <div class="col-md-8 mb-3">
                                                        <label class="font-weight-bold">Father Name</label>
                                                    </div>
                                                    @if (!empty($clients->father_name))
                                                        <div class="col-md-4 ">
                                                            : {{ $clients->father_name }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-4 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-8 mb-3">
                                                        <label class="font-weight-bold">Marital Status</label>
                                                    </div>
                                                    <div class="col-md-4 ">
                                                        : {{ $clients->marital_status }}
                                                    </div>
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-8 mb-3">
                                                        <label class="font-weight-bold">Spouse Name</label>
                                                    </div>
                                                    @if (!empty($clients->spouse_name))
                                                        <div class="col-md-4 ">
                                                            : {{ $clients->spouse_name }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-4 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-8 mb-3">
                                                        <label class="font-weight-bold">Occupation Of Spouse</label>
                                                    </div>
                                                    @if (!empty($clients->occupation_of_spouse))
                                                        <div class="col-md-4 ">
                                                            : {{ $clients->occupation_of_spouse }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-4 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-8 mb-3">
                                                        <label class="font-weight-bold">No Of Family</label>
                                                    </div>
                                                    <div class="col-md-4 ">
                                                        : {{ $clients->no_of_family }}
                                                    </div>
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-8 mb-3">
                                                        <label class="font-weight-bold">No Of Working Family</label>
                                                    </div>
                                                    <div class="col-md-4 ">
                                                        : {{ $clients->no_of_working_family }}
                                                    </div>
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-8 mb-3">
                                                        <label class="font-weight-bold">No of Children</label>
                                                    </div>
                                                    @if (!empty($clients->no_of_children))
                                                        <div class="col-md-4 ">
                                                            : {{ $clients->no_of_children }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-4 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-8 ">
                                                        <label class="font-weight-bold">No Of Working Progeny</label>
                                                    </div>
                                                    @if (!empty($clients->no_of_working_progeny))
                                                        <div class="col-md-4 ">
                                                            : {{ $clients->no_of_working_progeny }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-4 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br><br><br><br><br><br><br><br><br>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 pl-4">
                                        <div class="card">
                                            <div class="card-header theme-color">
                                                <h5>
                                                    <i class="fas fa-briefcase text-light"></i>
                                                    <span class="ml-2 text-light">JOB</span>
                                                </h5>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <label class="font-weight-bold">Job Status</label>
                                                    </div>
                                                    @if (!empty($clients->job_status))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->job_status }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <label class="font-weight-bold">Industry Name</label>
                                                    </div>
                                                    @if (!empty($job->industry_name))
                                                        <div class="col-md-6 ">
                                                            : {{ $job->industry_name }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <label class="font-weight-bold">Department Name</label>
                                                    </div>
                                                    @if (!empty($job->depart_name))
                                                        <div class="col-md-6 ">
                                                            : {{ $job->depart_name }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <label class="font-weight-bold">Job Position</label>
                                                    </div>
                                                    @if (!empty($job->job_position_name))
                                                        <div class="col-md-6 ">
                                                            : {{ $job->job_position_name }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                @if (!empty($clients->current_business && $clients->business_income))
                                                    <div class="form-row">
                                                        <div class="col-md-6 mb-3">
                                                            <label class="font-weight-bold">Current Business</label>
                                                        </div>
                                                        @if (!empty($clients->current_business))
                                                            <div class="col-md-6 ">
                                                                : {{ $clients->current_business }}
                                                            </div>
                                                        @else
                                                            <div class="col-md-6 ">
                                                                : N/A
                                                            </div>
                                                        @endif
                                                    </div><br>
                                                    <div class="form-row">
                                                        <div class="col-md-6 mb-3">
                                                            <label class="font-weight-bold">Business Income Monthly</label>
                                                        </div>
                                                        @if (!empty($clients->business_income))
                                                            <div class="col-md-6 ">
                                                                : {{ $clients->business_income }}
                                                            </div>
                                                        @else
                                                            <div class="col-md-6 ">
                                                                : N/A
                                                            </div>
                                                        @endif
                                                    </div><br>
                                                @endif
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <label class="font-weight-bold">Company Name</label>
                                                    </div>
                                                    @if (!empty($clients->company_name))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->company_name }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <label class="font-weight-bold">Company Phone</label>
                                                    </div>
                                                    @if (!empty($clients->company_phone))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->company_phone }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <label class="font-weight-bold">Salary</label>
                                                    </div>
                                                    @if (!empty($clients->salary))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->salary }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <label class="font-weight-bold">Experience Years</label>
                                                    </div>
                                                    @if (!empty($clients->experience))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->experience }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <label class="font-weight-bold">Experience Months</label>
                                                    </div>
                                                    @if (!empty($clients->experience2))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->experience2 }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div><br>
                                                <div class="form-row">
                                                    <div class="col-md-6 mb-3">
                                                        <label class="font-weight-bold">Company Address</label>
                                                    </div>
                                                    @if (!empty($clients->company_address))
                                                        <div class="col-md-6 ">
                                                            : {{ $clients->company_address }}
                                                        </div>
                                                    @else
                                                        <div class="col-md-6 ">
                                                            : N/A
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="surveyowner" role="tabpanel">
                        <div class="position-relative form-group row">
                            <div class="col-md-6 ">
                                <div class="position-relative form-group row">
                                    @if ($clients->assetstype_unremovable)
                                        <div class="col-md-6">
                                            <div class="main-card">
                                                <div class="card-body">
                                                    <h5 class="card-title">Unremovable Assets</h5>
                                                    <div class="position-relative form-group">
                                                        <div>
                                                            @php
                                                                $values2 = explode(',', $clients->assetstype_unremovable);
                                                            @endphp
                                                            <div class="custom-checkbox custom-control">
                                                                <input type="checkbox" id="unremovableCheckbox"
                                                                    name="unremovable_checkbox[]"
                                                                    @if (in_array('Building', $values2)) checked @endif
                                                                    value="Building" class="custom-control-input">
                                                                <label class="custom-control-label"
                                                                    for="unremovableCheckbox">Building</label>
                                                            </div>
                                                            <div class="custom-checkbox custom-control">
                                                                <input type="checkbox" id="unremovableCheckbox2"
                                                                    name="unremovable_checkbox[]"
                                                                    @if (in_array('Land', $values2)) checked @endif
                                                                    value="Land" class="custom-control-input">
                                                                <label class="custom-control-label"
                                                                    for="unremovableCheckbox2">Land</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($clients->assetstype_removable)
                                        <div class="col-md-6">
                                            <div class="main-card">
                                                <div class="card-body">
                                                    <h5 class="card-title">Removable Assets</h5>
                                                    <div class="position-relative form-group">
                                                        <div>
                                                            @php
                                                                $values3 = explode(',', $clients->assetstype_removable);
                                                            @endphp
                                                            <div class="custom-checkbox custom-control">
                                                                <input type="checkbox" id="removableCheckbox"
                                                                    name="removable_checkbox[]"
                                                                    @if (in_array('Vehicle', $values3)) checked @endif
                                                                    value="Vehicle" class="custom-control-input">
                                                                <label class="custom-control-label"
                                                                    for="removableCheckbox">Vehicle</label>
                                                            </div>
                                                            <div class="custom-checkbox custom-control">
                                                                <input type="checkbox" id="removableCheckbox1"
                                                                    name="removable_checkbox[]"
                                                                    @if (in_array('Machine for Agri', $values3)) checked @endif
                                                                    value="Machine for Agri" class="custom-control-input">
                                                                <label class="custom-control-label"
                                                                    for="removableCheckbox1">Machine for Agri</label>
                                                            </div>
                                                            <div class="custom-checkbox custom-control">
                                                                <input type="checkbox" id="removableCheckbox2"
                                                                    name="removable_checkbox[]"
                                                                    @if (in_array('Machine for Product Making', $values3)) checked @endif
                                                                    value="Machine for Product Making"
                                                                    class="custom-control-input">
                                                                <label class="custom-control-label"
                                                                    for="removableCheckbox2">Machine for Product
                                                                    Making</label>
                                                            </div>
                                                            <div class="custom-checkbox custom-control">
                                                                <input type="checkbox" id="removableCheckbox3"
                                                                    name="removable_checkbox[]"
                                                                    @if (in_array('Vissable Asset', $values3)) checked @endif
                                                                    value="Vissable Asset" class="custom-control-input">
                                                                <label class="custom-control-label"
                                                                    for="removableCheckbox3">Vissable Asset</label>
                                                            </div>
                                                            <div class="custom-checkbox custom-control">
                                                                <input type="checkbox" id="removableCheckbox4"
                                                                    name="removable_checkbox[]"
                                                                    @if (in_array('Stock in Hand', $values3)) checked @endif
                                                                    value="Stock in Hand" class="custom-control-input">
                                                                <label class="custom-control-label"
                                                                    for="removableCheckbox4">Stock in Hand</label>
                                                            </div>
                                                            <div class="custom-checkbox custom-control">
                                                                <input type="checkbox" id="removableCheckbox5"
                                                                    name="removable_checkbox[]"
                                                                    @if (in_array('Animal', $values3)) checked @endif
                                                                    value="Animal" class="custom-control-input">
                                                                <label class="custom-control-label"
                                                                    for="removableCheckbox5">Animal</label>
                                                            </div>
                                                            <div class="custom-checkbox custom-control">
                                                                <input type="checkbox" id="removableCheckbox6"
                                                                    name="removable_checkbox[]"
                                                                    @if (in_array('Other Removable Asset', $values3)) checked @endif
                                                                    value="Other Removable Asset"
                                                                    class="custom-control-input">
                                                                <label class="custom-control-label"
                                                                    for="removableCheckbox6">Other Removable Asset</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6 my-4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-row">
                                            <div class="col-md-6 ">
                                                <label class="font-weight-bold">Purchased Price of Owned Assets</label>
                                            </div>
                                            @if (!empty($clients->purchase_price))
                                                <div class="col-md-6 ">
                                                    : {{ $clients->purchase_price }}
                                                </div>
                                            @else
                                                <div class="col-md-6 ">
                                                    : N/A
                                                </div>
                                            @endif
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="col-md-6 ">
                                                <label class="font-weight-bold">Current Value of Owned Assets</label>
                                            </div>
                                            @if (!empty($clients->current_value))
                                                <div class="col-md-6 ">
                                                    : {{ $clients->current_value }}
                                                </div>
                                            @else
                                                <div class="col-md-6 ">
                                                    : N/A
                                                </div>
                                            @endif
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="col-md-6 ">
                                                <label class="font-weight-bold">Quantity</label>
                                            </div>
                                            @if (!empty($clients->quantity))
                                                <div class="col-md-6 ">
                                                    : {{ $clients->quantity }}
                                                </div>
                                            @else
                                                <div class="col-md-6 ">
                                                    : N/A
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="position-relative form-group row">
                            <div class="col-md-6 pl-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="text-secondary font-weight-bold mb-4">OWNERSHIP DOCUMENTS</h5>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    @if ($clients->attach_1)
                                        <div class="col-md-6 ">
                                            <label for="" class="font-weight-bold">Document Photo 1</label><br>
                                            <img src="{{ asset('public/storage/client_ownership_documents/' . $clients->attach_1) }}"
                                                class="rounded" width='200px' height="130px" alt=""
                                                title="">
                                        </div>
                                    @endif
                                    @if ($clients->attach_2)
                                        <div class="col-md-6">
                                            <label for="" class="font-weight-bold">Document Photo 2</label><br>
                                            <img src="{{ asset('public/storage/client_ownership_documents/' . $clients->attach_2) }}"
                                                class="rounded" width='200px' height="130px" alt=""
                                                title="">
                                        </div>
                                    @endif
                                </div>

                                <div class="row">
                                    @if ($clients->attach_3)
                                        <div class="col-md-6">
                                            <label for="" class="font-weight-bold">Document Photo 3</label><br>
                                            <img src="{{ asset('public/storage/client_ownership_documents/' . $clients->attach_3) }}"
                                                class="rounded" width='200px' height="130px" alt=""
                                                title="">
                                        </div>
                                    @endif
                                    @if ($clients->attach_3)
                                        <div class="col-md-6">
                                            <label for="" class="font-weight-bold">Document Photo 4</label><br>
                                            <img src="{{ asset('public/storage/client_ownership_documents/' . $clients->attach_4) }}"
                                                class="rounded" width='200px' height="130px" alt=""
                                                title="">
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header theme-color">
                                        <h5 class="text-light">
                                            <i class="fas fa-user"></i>
                                            <span class="ml-2">CLIENT TYPE</span>
                                        </h5>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-row">
                                            <div class="col-md-6 ">
                                                <label class="font-weight-bold">Client Type</label>
                                            </div>
                                            @if (!empty($clients->client_type))
                                                <div class="col-md-6 text-capitalize">
                                                    : {{ $clients->client_type }}
                                                </div>
                                            @else
                                                <div class="col-md-6 ">
                                                    : N/A
                                                </div>
                                            @endif
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="col-md-6 ">
                                                <label class="font-weight-bold">Client Type-Group</label>
                                            </div>
                                            @if (!empty($client_type->group_uniquekey))
                                                <div class="col-md-6 ">
                                                    :
                                                    {{ DB::table('tbl_main_join_group')->where('portal_group_id', $client_type->group_uniquekey)->first()->main_group_code ?? $client_type->group_uniquekey }}
                                                    ( {{ $client_type->you_are_a_group_leader ?? '' }} leader )
                                                </div>
                                            @else
                                                <div class="col-md-6 ">
                                                    : N/A
                                                </div>
                                            @endif
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="col-md-6 ">
                                                <label class="font-weight-bold">Client Type-Center</label>
                                            </div>
                                            @if (!empty($client_type->center_uniquekey))
                                                <div class="col-md-6 ">
                                                    :
                                                    {{ DB::table('tbl_main_join_center')->where('portal_center_id', $client_type->center_uniquekey)->first()->main_center_code ?? $client_type->center_uniquekey }}
                                                    ( {{ $client_type->you_are_a_center_leader ?? '' }} leader )
                                                </div>
                                            @else
                                                <div class="col-md-6 ">
                                                    : N/A
                                                </div>
                                            @endif
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="col-md-6 ">
                                                <label class="font-weight-bold">Guarantors</label>
                                            </div>
                                            @if (!empty($client_type->name))
                                                <div class="col-md-6 ">
                                                    : {{ $client_type->name }}
                                                </div>
                                            @else
                                                <div class="col-md-6 ">
                                                    : N/A
                                                </div>
                                            @endif
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="col-md-6 ">
                                                <label class="font-weight-bold">Client Registration Date</label>
                                            </div>
                                            @if (!empty($client_type->registration_date))
                                                <div class="col-md-6 ">
                                                    : {{ $client_type->registration_date }}
                                                </div>
                                            @else
                                                <div class="col-md-6 ">
                                                    : N/A
                                                </div>
                                            @endif
                                        </div>
                                        <hr><br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h5 class="text-secondary font-weight-bold mb-4 pl-3">CLIENT DOCUMENTS</h5>
                        <div class="position-relative form-group row pl-3">
                            @if (empty($clients->recognition_front && $clients->recognition_back))
                                <div class="col-md-3 ">
                                    <label class="font-weight-bold">NRC Recommendation Photo</label><br><br>
                                    <img src="{{ asset('public/storage/clientphotos/' . $clients->nrc_recommendation) }}"
                                        width='200px' height="130px" alt="" title="" class="rounded">
                                </div>
                            @else
                                <div class="col-md-3 ">
                                    <label class="font-weight-bold">NRC Front</label><br><br>
                                    <img src="{{ asset('public/storage/clientphotos/' . $clients->recognition_front) }}"
                                        width='200px' height="130px" alt="" title="" class="rounded">
                                </div>

                                <div class="col-md-3 ">
                                    <label class="font-weight-bold">NRC Back</label><br><br>
                                    <img src="{{ asset('public/storage/clientphotos/' . $clients->recognition_back) }}"
                                        width='200px' height="130px" alt="" title="" class="rounded">
                                </div>
                            @endif

                            <div class="col-md-3 ">
                                <label class="font-weight-bold">Registeration Family form Front</label><br><br>
                                <img src="{{ asset('public/storage/clientphotos/' . $clients->registration_family_form_front) }}"
                                    width='200px' height="130px" alt="" title="" class="rounded">
                            </div>

                            <div class="col-md-3">
                                <label class="font-weight-bold">Registeration Family form Back</label><br><br>
                                <img src="{{ asset('public/storage/clientphotos/' . $clients->registration_family_form_back) }}"
                                    width='200px' height="130px" alt="" title="" class="rounded">
                            </div>
                            @if ($clients->finger_print_bio)
                                <div class="col-md-3 my-4">
                                    <label class="font-weight-bold">Finger Print Bio</label><br><br>

                                    <img src="{{ asset('public/storage/clientphotos/' . $clients->finger_print_bio) }}"
                                        width='200px' height="130px" alt="" title="" class="rounded">

                                </div>
                            @endif
                            @if ($clients->government_recommendations_photo)
                                <div class="col-md-3 my-4">
                                    <label class="font-weight-bold">Government Recommendations</label><br><br>

                                    <img src="{{ asset('public/storage/clientphotos/' . $clients->government_recommendations_photo) }}"
                                        width='200px' height="130px" alt="" title="" class="rounded">

                                </div>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
