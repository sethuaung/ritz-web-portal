@extends('layouts.app')
@section('content')
    <div class="app-main__inner">

        {{-- Navigation Tabs --}}
        <div class="row">

            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-default">
                    <div class="widget-content-wrapper text-dark">
                        <div class="widget-content-left">
                            <div class="widget-heading">Total Clients/Borrowers</div>
                            <a href="{{ url('client/') }}">
                                <div class="widget-sub-heading pt-3">View Clients > </div>
                            </a>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-dark"><span>{{ $total_clients }}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-default">
                    <div class="widget-content-wrapper text-dark">
                        <div class="widget-content-left">
                            <div class="widget-heading">Total Disbursed Amount</div>
                            <a href="">
                                <div class="widget-sub-heading pt-3">View Disbursed > </div>
                            </a>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-dark"><span>{{ $total_disbursement }} Ks</span></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-default">
                    <div class="widget-content-wrapper text-dark">
                        <div class="widget-content-left">
                            <div class="widget-heading">Total Outstanding Amount</div>
                            <a href="{{ url('loan/') }}">
                                <div class="widget-sub-heading pt-3">View Outstanding > </div>
                            </a>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-dark"><span>{{ $total_outstanding }} Ks</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-default">
                    <div class="widget-content-wrapper text-dark">
                        <div class="widget-content-left">
                            <div class="widget-heading">Total Loan Applications</div>
                            <a href="{{ url('loan/') }}">
                                <div class="widget-sub-heading pt-3">View Loan Applications > </div>
                            </a>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-dark"><span>{{ $total_loans }}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-default">
                    <div class="widget-content-wrapper text-dark">
                        <div class="widget-content-left">
                            <div class="widget-heading">Loan in Progress/Pending</div>
                            <a href="{{ url('loan?disbursement_status=Pending') }}">
                                <div class="widget-sub-heading pt-3">View Loan in progress > </div>
                            </a>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-dark"><span>{{ $pending }}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-default">
                    <div class="widget-content-wrapper text-dark">
                        <div class="widget-content-left">
                            <div class="widget-heading">Total Loan Approved/Active Loan</div>
                            <a href="{{ url('loan?disbursement_status=Activated') }}">
                                <div class="widget-sub-heading pt-3">View Loan approved > </div>
                            </a>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-dark"><span>{{ $activated }}</span></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-4" hidden>
                <div class="card mb-3 widget-content bg-default">
                    <div class="widget-content-wrapper text-dark">
                        <div class="widget-content-left">
                            <div class="widget-heading">Overdue in days</div>
                            <a href="">
                                <div class="widget-sub-heading pt-3">View Overdue days > </div>
                            </a>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-dark"><span>212 Days</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4" hidden>
                <div class="card mb-3 widget-content bg-default">
                    <div class="widget-content-wrapper text-dark">
                        <div class="widget-content-left">
                            <div class="widget-heading">Loan in Progress/Pending</div>
                            <a href="">
                                <div class="widget-sub-heading pt-3">View Overdue % > </div>
                            </a>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-dark"><span>30</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4" hidden>
                <div class="card mb-3 widget-content bg-default">
                    <div class="widget-content-wrapper text-dark">
                        <div class="widget-content-left">
                            <div class="widget-heading">Overdue %</div>
                            <a href="">
                                <div class="widget-sub-heading pt-3">View Received > </div>
                            </a>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-dark"><span>34 %</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- 2 PieCharts --}}
        <div class="row">
            <div class="col-md-6">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Deliquent (Clients) </h5>
                        <canvas id="PieChart" width="397" height="198"
                            style="display: block; width: 397px; height: 198px;" class="chartjs-render-monitor"></canvas>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <h5 class="card-title">Loan Products %</h5>
                        <canvas id="PieChart2" width="397" height="198"
                            style="display: block; width: 397px; height: 198px;" class="chartjs-render-monitor"></canvas>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            {{-- Upcoming Payments Table --}}
            <div class="col-md-8">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <h4>Upcoming Payments/Collections</h4>
                            <div class="row">
                                {{-- <div class="col-sm-12 col-md-6">
                                <div id="example_filter" class="dataTables_filter">
                                    <label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example"></label>
                                </div>
                            </div> --}}
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table style="width: 100%;" id="example"
                                        class="table table-hover table-striped table-bordered dataTable dtr-inline"
                                        role="grid" aria-describedby="example_info">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 53.2px;"
                                                    aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending">No.</th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 72.2px;"
                                                    aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending">Ref</th>
                                                <th class="sorting" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 53.2px;"
                                                    aria-label="Position: activate to sort column ascending">Clients</th>
                                                <th class="sorting" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 111.2px;"
                                                    aria-label="Office: activate to sort column ascending">Date</th>
                                                <th class="sorting" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 27.2px;"
                                                    aria-label="Age: activate to sort column ascending">Principle</th>
                                                <th class="sorting" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 59.2px;"
                                                    aria-label="Start date: activate to sort column ascending">Interest
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($upcoming as $key => $loan)
                                                <tr role="row" class="odd">
                                                    <td>{{ $key + 1 }}</td>
                                                    <td class="sorting_1 dtr-control">{{ $loan->loan_unique_id }}</td>
                                                    <td>{{ $loan->name }}</td>
                                                    <td>{{ $loan->month }}</td>
                                                    <td>{{ $loan->capital }} Ks</td>
                                                    <td>{{ $loan->interest }} Ks</td>
                                                </tr>
                                            @endforeach
                                        </tbody>

                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            {{-- Buttons Related with Payments --}}
            <div class="col-md-4 col-xl-4" style="display:none;">
                <div class="card mb-3 widget-content" style="background: #5edb98;">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Total Repayments</div>
                            <a href="">
                                <div class="widget-sub-heading pt-3">View Repayments > </div>
                            </a>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>800000 Ks</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            {{-- Amount Collected Table --}}
            <div class="col-md-8">
                <div class="main-card mb-3 card" style="height: auto;">
                    <div class="card-body">
                        <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <h4>Amount Collected for today ({{ $branch_name }})</h4>

                            <div class="row">
                                <div class="col-sm-12"
                                    style="padding-left:auto; width:auto;overflow:auto; max-height:400px;">
                                    <table style="width: 100%;" id="example"
                                        class="table table-hover table-striped table-bordered dataTable dtr-inline"
                                        role="grid" aria-describedby="example_info">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 72.2px;"
                                                    aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending">No.</th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 72.2px;"
                                                    aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending">Ref</th>
                                                <th class="sorting" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 111.2px;"
                                                    aria-label="Position: activate to sort column ascending">Clients</th>
                                                <th class="sorting" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 53.2px;"
                                                    aria-label="Office: activate to sort column ascending">Date</th>
                                                <th class="sorting" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 27.2px;"
                                                    aria-label="Age: activate to sort column ascending">Principle</th>
                                                <th class="sorting" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 59.2px;"
                                                    aria-label="Start date: activate to sort column ascending">Interest
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 59.2px;"
                                                    aria-label="Start date: activate to sort column ascending">Loan Officer
                                                    Name</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @for ($i = 0; $i < count($todayCollect); $i++)
                                                <tr role="row" class="odd">

                                                    <td>{{ $i }}</td>
                                                    <td class="sorting_1 dtr-control">
                                                        {{ $todayCollect[$i]['loan_unique_id'] }}</td>
                                                    <td>{{ $todayCollect[$i]['name'] }}</td>
                                                    <td>{{ $todayCollect[$i]['payment_date'] }}</td>
                                                    <td>{{ $todayCollect[$i]['principal'] }} Ks</td>
                                                    <td>{{ $todayCollect[$i]['interest'] }} Ks</td>
                                                    <td> </td>
                                                </tr>
                                            @endfor

                                        </tbody>

                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            {{-- Buttons Related with Today Amount --}}
            <div class="container col-md-4">
                <div class="card mb-3 widget-content" style="background: #5edb98;display:none;">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Today Collection Target</div>
                            <a href="">
                                <div class="widget-sub-heading pt-3">View Payments > </div>
                            </a>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>50000 Ks</span></div>
                        </div>
                    </div>
                </div>

                <div class="card mb-3 widget-content" style="background: #5edb98;">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Today Total Received</div>
                            <a href="">
                                <div class="widget-sub-heading pt-3">Today Total Received > </div>
                            </a>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>{{ $todayReceivedAmount }} Ks</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Line Graph --}}
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-12 card d-none">
                    <div class="card-body">
                        <h5 class="card-title">Monthly Outstanding</h5>
                        <canvas id="myChart" style="width:100%;max-height:30%"></canvas>
                    </div>
                </div>

            </div>
        </div>

    </div>
<script>
        var ctxL = document.getElementById("myChart").getContext('2d');
        var myLineChart = new Chart(ctxL, {
            type: 'line',
            data: {
                labels: <?php if($graph_labels){echo json_encode($graph_labels);} ?>,
                datasets: [
                    <?php if(count($B_code) < 31){ ?> {
                		// labels: "aa",
                        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        backgroundColor: [
                            'rgba(235, 10, 10, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    }
                    <?php }else{?>
                    //0 AB
                    {
                        label: <?php echo json_encode($B_code[0]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[0]); ?>,
                        backgroundColor: [
                            'rgba(235, 10, 10, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //1 AMP
                    {
                        label: <?php echo json_encode($B_code[1]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[1]); ?>,
                        backgroundColor: [
                            'rgba(235, 77, 10, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //2 EM
                    {
                        label: <?php echo json_encode($B_code[2]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[2]); ?>,
                        backgroundColor: [
                            'rgba(235, 149, 10, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //3 KG
                    {
                        label: <?php echo json_encode($B_code[3]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[3]); ?>,
                        backgroundColor: [
                            'rgba(235, 205, 10, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //4 KM
                    {
                        label: <?php echo json_encode($B_code[4]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[4]); ?>,
                        backgroundColor: [
                            'rgba(194, 235, 10, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //5 KS1
                    {
                        label: <?php echo json_encode($B_code[5]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[5]); ?>,
                        backgroundColor: [
                            'rgba(130, 235, 10, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //6 KU
                    {
                        label: <?php echo json_encode($B_code[6]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[6]); ?>,
                        backgroundColor: [
                            'rgba(40, 235, 10, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //7 MD1
                    {
                        label: <?php echo json_encode($B_code[7]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[7]); ?>,
                        backgroundColor: [
                            'rgba(10, 235, 89, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //8 MD2
                    {
                        label: <?php echo json_encode($B_code[8]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[8]); ?>,
                        backgroundColor: [
                            'rgba(10, 235, 171, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //9 MG
                    {
                        label: <?php echo json_encode($B_code[9]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[9]); ?>,
                        backgroundColor: [
                            'rgba(10, 216, 235, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //10 MTL
                    {
                        label: <?php echo json_encode($B_code[10]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[10]); ?>,
                        backgroundColor: [
                            'rgba(10, 167, 235, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //11 NU
                    {
                        label: <?php echo json_encode($B_code[11]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[11]); ?>,
                        backgroundColor: [
                            'rgba(10, 107, 235, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //12 POL
                    {
                        label: <?php echo json_encode($B_code[12]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[12]); ?>,
                        backgroundColor: [
                            'rgba(10, 14, 235, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //13 PY
                    {
                        label: <?php echo json_encode($B_code[13]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[13]); ?>,
                        backgroundColor: [
                            'rgba(104, 10, 235, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //14 SB1
                    {
                        label: <?php echo json_encode($B_code[14]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[14]); ?>,
                        backgroundColor: [
                            'rgba(175, 10, 235, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //15 SG
                    {
                        label: <?php echo json_encode($B_code[15]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[15]); ?>,
                        backgroundColor: [
                            'rgba(201, 10, 235, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //16 SK
                    {
                        label: <?php echo json_encode($B_code[16]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[16]); ?>,
                        backgroundColor: [
                            'rgba(235, 10, 171, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //17 TDU
                    {
                        label: <?php echo json_encode($B_code[17]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[17]); ?>,
                        backgroundColor: [
                            'rgba(235, 10, 89, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //18 TGO
                    {
                        label: <?php echo json_encode($B_code[18]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[18]); ?>,
                        backgroundColor: [
                            'rgba(235, 10, 10, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //19 YU
                    {
                        label: <?php echo json_encode($B_code[19]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[19]); ?>,
                        backgroundColor: [
                            'rgba(200, 45, 45, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //20 KP
                    {
                        label: <?php echo json_encode($B_code[20]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[20]); ?>,
                        backgroundColor: [
                            'rgba(200, 128, 45, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //21 MY
                    {
                        label: <?php echo json_encode($B_code[21]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[21]); ?>,
                        backgroundColor: [
                            'rgba(200, 189, 45, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //22 KS2
                    {
                        label: <?php echo json_encode($B_code[22]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[22]); ?>,
                        backgroundColor: [
                            'rgba(130, 200, 45, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //23 PD
                    {
                        label: <?php echo json_encode($B_code[23]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[23]); ?>,
                        backgroundColor: [
                            'rgba(45, 200, 71, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //24 WL
                    {
                        label: <?php echo json_encode($B_code[24]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[24]); ?>,
                        backgroundColor: [
                            'rgba(45, 200, 156, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //25 SB2
                    {
                        label: <?php echo json_encode($B_code[25]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[25]); ?>,
                        backgroundColor: [
                            'rgba(45, 161, 200, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //26 WD
                    {
                        label: <?php echo json_encode($B_code[26]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[26]); ?>,
                        backgroundColor: [
                            'rgba(56, 70, 199, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //27 HO
                    {
                        label: <?php echo json_encode($B_code[27]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[27]); ?>,
                        backgroundColor: [
                            'rgba(130, 56, 199, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //28 MDYHO
                    {
                        label: <?php echo json_encode($B_code[28]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[28]); ?>,
                        backgroundColor: [
                            'rgba(187, 56, 199, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //29 KT
                    {
                        label: <?php echo json_encode($B_code[29]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[29]); ?>,
                        backgroundColor: [
                            'rgba(199, 56, 158, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },
                    //30 KL
                    {
                        label: <?php echo json_encode($B_code[30]->branch_code); ?>,
                        data: <?php echo json_encode($graph_values[30]); ?>,
                        backgroundColor: [
                            'rgba(199, 56, 94, 0.48)',
                        ],
                        borderColor: [
                            'rgba(200, 99, 132, .7)',
                        ],
                        borderWidth: 2
                    },

                    <?php } ?>

                ]
            },
            options: {
                responsive: true
            }
        });
    </script>


    {{-- PieChart --}}
    <script>
        var xValues = ["30 days", "31 to 60 days", "61 to 90 days", "over 90 days"];
        var yValues = [16, 3, 18, 22];
        var barColors = [
            // "#9999FF",
            // "#6B7DB3",
            // "#E6ECFF",
            // "#CCD9FF",
            '#006cb4',
            '#2680be',
            '#4f98cb',
            '#74aed4',
        ];

        new Chart("PieChart", {
            type: "pie",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                title: {
                    display: true,
                    text: ""
                }
            }
        });
    </script>

    {{-- PieChart2 --}}
    <script>
        // var xValues = ["General loan", "Agri loan", "Teacher loan", "Extra loan", "Internal Staff loan","External Staff loan","Micro Enterprise Loan"];
        // var yValues = [40, 20, 10, 10, 10 , 10 , 5];
        var barColors = [
            // "#FFFF99",
            // "#f3dba5",
            // "#D6FF99",
            // "#ffeb99",
            // "#b3a46b",
            // "#ffffe6",
            // "#f3b4a5",
            '#012b51',
            '#003c69',
            '#00558e',
            '#006cb4',
            '#2680be',
            '#4f98cb',
            '#99c5e0'
        ];

        new Chart("PieChart2", {
            type: "pie",
            data: {
                labels: <?php echo json_encode($labels); ?>,
                datasets: [{
                    backgroundColor: barColors,
                    data: <?php echo json_encode($values); ?>,
                }]
            },
            options: {
                title: {
                    display: true,
                    text: ""
                }
            }
        });
    </script>
@endsection
