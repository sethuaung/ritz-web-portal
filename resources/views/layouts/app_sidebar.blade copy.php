<div class="scrollbar-sidebar">
    <div class="app-sidebar__inner">
        <ul class="vertical-nav-menu">
            <li class="app-sidebar__heading">Dashboards</li>
            <li>
                <a href="{{url('/')}}">
                    <i class="metismenu-icon pe-7s-display1"></i>
                    Dashboard
                </a>
            </li>
            <li class="app-sidebar__heading"> Manage Client </li>
            <li>
                <a href="{{url('client/create')}}">
                    <i class="metismenu-icon pe-7s-plus"></i>
                    Create Client
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="metismenu-icon pe-7s-note2"></i>
                    Client
                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                </a>
                <ul>
                    <li>
                        <a href="{{url('client')}}">
                            <i class="metismenu-icon"></i> All Client
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="metismenu-icon pe-7s-network"></i>
                    Group
                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                </a>
                <ul>
                    <li>
                        <a href="{{url('groups/')}}">
                            <i class="metismenu-icon">
                            </i> All Process
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="metismenu-icon pe-7s-share"></i>
                    Branch
                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                </a>
                <ul>
                    <li>
                        <a href="{{url('branches/')}}">
                            <i class="metismenu-icon">
                            </i> All Process
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="tables-regular.html">
                    <i class="metismenu-icon pe-7s-display2"></i>
                    Center
                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                </a>
                <ul>
                    <li>
                        <a href="{{url('centers/')}}">
                            <i class="metismenu-icon">
                            </i> All Process
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="metismenu-icon pe-7s-users"></i>
                    Staff
                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                </a>
                <ul>
                    <li>
                        <a href="{{url('staffs/')}}">
                            <i class="metismenu-icon">
                            </i> All Process
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="metismenu-icon pe-7s-user"></i>
                    Guarantor
                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                </a>
                <ul>
                    <li>
                        <a href="{{url('guarantors/')}}">
                            <i class="metismenu-icon">
                            </i> All Process
                        </a>
                    </li>
                </ul>
            </li>
            <li class="app-sidebar__heading">Manage Loan</li>
            <li>
                <a href="dashboard-boxes.html">
                    <i class="metismenu-icon pe-7s-calculator"></i>
                    Loan Calculator
                </a>
            </li>
            <li>

           
            <!--  -->
                <a href="{{url('loan/create')}}">
                    <i class="metismenu-icon pe-7s-plus"></i>
                    Create Loan
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="metismenu-icon pe-7s-graph2"></i>
                    Loan Process
                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                </a>
                <ul>
                    <li>
                    <!--  -->
                        <a href="{{url('loan/')}}">
                            <i class="metismenu-icon">
                            </i> All Process
                        </a>
                    </li>
                    <!--  -->
                    <li>
                        <a href="{{url('loan/')}}">
                            <i class="metismenu-icon">
                            </i>  Loan Disbursement
                        </a>
                    </li>
                </ul>
            </li>
            <li class="app-sidebar__heading">Data Entry</li>
            <li>
                <a href="forms-controls.html">
                    <i class="metismenu-icon pe-7s-plus">
                    </i>Forms Controls
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="metismenu-icon pe-7s-home"></i>
                    Address
                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                </a>
                <ul>
                    <li>
                        <a href="{{url('divisions/')}}">
                            <i class="metismenu-icon">
                            </i>Division
                        </a>
                    </li>
                    <li>
                        <a href="{{url('districts/')}}">
                            <i class="metismenu-icon">
                            </i>District
                        </a>
                    </li>
                    <li>
                        <a href="{{url('cities/')}}">
                            <i class="metismenu-icon">
                            </i>City
                        </a>
                    </li>
                    <li>
                        <a href="{{url('townships/')}}">
                            <i class="metismenu-icon">
                            </i>Townships
                        </a>
                    </li>
                    <li>
                        <a href="{{url('provinces/')}}">
                            <i class="metismenu-icon">
                            </i>Province
                        </a>
                    </li>
                    <li>
                        <a href="{{url('quarters/')}}">
                            <i class="metismenu-icon">
                            </i>Quarter
                        </a>
                    </li>
                    <li>
                        <a href="{{url('villages/')}}">
                            <i class="metismenu-icon">
                            </i>Village
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="metismenu-icon pe-7s-alarm"></i>
                    Job Industry
                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                </a>
                <ul>
                    <li>
                        <a href="{{url('industries/')}}">
                            <i class="metismenu-icon">
                            </i>Industry
                        </a>
                    </li>
                    <li>
                        <a href="{{url('departments/')}}">
                            <i class="metismenu-icon">
                            </i>Department
                        </a>
                    </li>
                    <li>
                        <a href="{{url('jobpositions/')}}">
                            <i class="metismenu-icon">
                            </i>Job Position
                        </a>
                    </li>
                    
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="metismenu-icon pe-7s-culture"></i>
                    Education
                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                </a>
                <ul>
                    <li>
                        <a href="{{url('educations/')}}">
                            <i class="metismenu-icon">
                            </i>Education Type
                        </a>
                    </li>
                    
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="metismenu-icon pe-7s-date"></i>
                    Calendar
                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                </a>
                <ul>
                    <li>
                    <!--  -->
                        <a href="{{url('loancalendars/')}}">
                            <i class="metismenu-icon">
                            </i>Calendar 
                        </a>
                    </li>
                    
                </ul>
            </li>
        </ul>
    </div>
</div>                   