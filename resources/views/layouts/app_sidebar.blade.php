<div class="scrollbar-sidebar">
    <div class="app-sidebar__inner">
        <ul class="vertical-nav-menu">
            <?php
            $staff_code = Session::get('staff_code');
            $branchid = DB::table('tbl_staff')
                ->where('staff_code', $staff_code)
                ->first()->branch_id;
            $B_code = DB::table('tbl_staff')
                ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
                ->select('tbl_branches.branch_code')
                ->where('tbl_staff.staff_code', $staff_code)
                ->first();
            $b_code = strtolower($B_code->branch_code);
            $all = DB::table('tbl_client_join')
                // ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_join.branch_id', $branchid)
                ->count();
            $new = DB::table('tbl_client_join')
                ->join('tbl_client_basic_info', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_basic_info.client_type', 'new')
                ->where('tbl_client_join.branch_id', $branchid)
                ->count();
            $active = DB::table('tbl_client_join')
                ->rightJoin('tbl_client_basic_info', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_basic_info.client_type', 'active')
                ->where('tbl_client_join.branch_id', $branchid)
                ->count();
            $dormant = DB::table('tbl_client_join')
                ->leftJoin('tbl_client_basic_info', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_basic_info.client_type', 'dormant')
                ->where('tbl_client_join.branch_id', $branchid)
                // ->get()
                ->count();
            $rejoin = DB::table('tbl_client_basic_info')
                ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_basic_info.client_type', 'rejoin')
                ->where('tbl_client_join.branch_id', $branchid)
                // ->get()
                ->count();
            $substitute = DB::table('tbl_client_basic_info')
                ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_basic_info.client_type', 'substitute')
                ->where('tbl_client_join.branch_id', $branchid)
                // ->get()
                ->count();
            $dropout = DB::table('tbl_client_basic_info')
                ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_basic_info.client_type', 'dropout')
                ->where('tbl_client_join.branch_id', $branchid)
                // ->get()
                ->count();
            $dead = DB::table('tbl_client_basic_info')
                ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_basic_info.client_type', 'dead')
                ->where('tbl_client_join.branch_id', $branchid)
                // ->get()
                ->count();
            $reject = DB::table('tbl_client_basic_info')
                ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_basic_info.client_type', 'reject')
                ->where('tbl_client_join.branch_id', $branchid)
                // ->get()
                ->count();
            
            $permissions = DB::table('role_has_permissions')
                ->leftJoin('permissions', 'permissions.id', '=', 'role_has_permissions.permission_id')
                ->leftJoin('model_has_permissions', 'model_has_permissions.permission_id', '=', 'permissions.id')
                ->where('role_has_permissions.role_id', '=', Session::get('role_id'))
                ->orWhere('model_has_permissions.model_id', Session::get('staff_code'))
                ->select('permissions.name')
                ->pluck('name')
                ->toArray();

                $permission_groups = DB::table('role_has_permissions')
                ->leftJoin('permissions', 'permissions.id', '=', 'role_has_permissions.permission_id')
                ->leftJoin('model_has_permissions', 'model_has_permissions.permission_id', '=', 'permissions.id')
                ->where('role_has_permissions.role_id', '=', Session::get('role_id'))
                ->orWhere('model_has_permissions.model_id', Session::get('staff_code'))
                ->select('permissions.group_name')
                ->pluck('group_name')
                ->toArray();
            
            ?>
            <?php if(in_array("dashboard",$permissions)){ ?>
            <li class="app-sidebar__heading">Dashboards</li>
            <li>
                <a href="{{ url('dashboard/') }}">
                    <i class="metismenu-icon fa fa-home"></i>
                    Dashboard
                </a>
            </li>
            <?php } ?>
            @if(in_array('MANAGE CLIENT', $permission_groups))
            <li class="app-sidebar__heading"> Manage Client </li>
            <?php if(in_array("add-client",$permissions)){ ?>
            <li>
                <a href="{{ url('client/create') }}">
                    <i class="metismenu-icon fa fa-plus"></i>
                    Create Client
                </a>
            </li>
            <?php } ?>
            <?php if(in_array("client-list",$permissions)){ ?>
            <li>
                <a href="#">
                    <i class="metismenu-icon fa fa-list"></i>
                    Client Process
                    <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                </a>

                <ul class="mm-collapse">
                    <li>
                        <a href="{{ url('client') }}">
                            <i class="metismenu-icon"></i> View All Client
                            <span class="badge badge-pill badge-success"
                                style="margin-left: 10px">{{ $all }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('client?clienttype=new') }}">
                            <i class="metismenu-icon"></i> New Client
                            <span class="badge badge-pill badge-primary"
                                style="margin-left: 10px">{{ $new }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('client?clienttype=active') }}">
                            <i class="metismenu-icon"></i> Active Client
                            <span class="badge badge-pill badge-success"
                                style="margin-left: 10px">{{ $active }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('client?clienttype=dormant') }}">
                            <i class="metismenu-icon"></i> Dormant Client
                            <span class="badge badge-pill badge-primary"
                                style="margin-left: 10px">{{ $dormant }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('client?clienttype=rejoin') }}">
                            <i class="metismenu-icon"></i> Rejoin Client
                            <span class="badge badge-pill badge-warning"
                                style="margin-left: 10px">{{ $rejoin }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('client?clienttype=substitute') }}">
                            <i class="metismenu-icon"></i> Substitute Client
                            <span class="badge badge-pill badge-primary"
                                style="margin-left: 10px">{{ $substitute }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('client?clienttype=dropout') }}">
                            <i class="metismenu-icon"></i> Drop-out Client
                            <span class="badge badge-pill badge-primary"
                                style="margin-left: 10px">{{ $dropout }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('client?clienttype=dead') }}">
                            <i class="metismenu-icon"></i> Dead Client
                            <span class="badge badge-pill badge-danger"
                                style="margin-left: 10px">{{ $dead }}</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('client?clienttype=reject') }}">
                            <i class="metismenu-icon"></i> Reject Client
                            <span class="badge badge-pill badge-warning"
                                style="margin-left: 10px">{{ $reject }}</span>
                        </a>
                    </li>
                </ul>

            </li>
            <?php } ?>
            @endif

            @if(in_array("MANAGE LOAN", $permission_groups))
            <li class="app-sidebar__heading">Manage Loan</li>
            <?php if(in_array("create-loan",$permissions)){ ?>
            <li>
                <a href="{{ url('loan/create') }}">
                    <i class="metismenu-icon fa fa-plus"></i>
                    Create Loan
                </a>
            </li>
            <?php } ?>
            <li>
                <a href="#">
                    <i class="metismenu-icon fa fa-list"></i>
                    Loan Process
                    <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                </a>
                <?php
                $allLoan = DB::table($b_code . '_loans')
                    ->get()
                    ->count();
                $pending = DB::table($b_code . '_loans')
                    ->where('disbursement_status', 'Pending')
                    ->get()
                    ->count();
                $approved = DB::table($b_code . '_loans')
                    ->where('disbursement_status', 'Approved')
                    ->get()
                    ->count();
                $deposited = DB::table($b_code . '_loans')
                    ->where('disbursement_status', 'Deposited')
                    ->get()
                    ->count();
                $activated = DB::table($b_code . '_loans')
                    ->where('disbursement_status', 'Activated')
                    ->get()
                    ->count();
                $closed = DB::table($b_code . '_loans')
                    ->where('disbursement_status', 'Closed')
                    ->get()
                    ->count();
                $declined = DB::table($b_code . '_loans')
                    ->where('disbursement_status', 'Declined')
                    ->get()
                    ->count();
                $canceled = DB::table($b_code . '_loans')
                    ->where('disbursement_status', 'Canceled')
                    ->get()
                    ->count();
                ?>
                <ul class="mm-collapse">
                    <?php if(in_array("view-all-loan",$permissions)){ ?>
                    <li>
                        <a href="{{ url('loan/') }}">
                            <i class="metismenu-icon">
                            </i> View All Loan <span class="badge badge-pill badge-success"
                                style="margin-left: 10px">{{ $allLoan }}</span>
                        </a>
                    </li>
                    <?php } ?>

                    <?php if(in_array("loan-approved",$permissions) || in_array("group-pending-approve",$permissions)){ ?>
                    <li>
                        <a href="{{ url('loan?disbursement_status=Pending') }}">
                            <i class="metismenu-icon">
                            </i> Loan Approval <span class="badge badge-pill badge-primary"
                                style="margin-left: 10px">{{ $pending }}</span>
                        </a>
                    </li>
                    <?php } ?>
                    <?php if(in_array("loan-approved",$permissions)){ ?>
                    <li>
                        <a href="{{ url('loan?disbursement_status=Approved') }}">
                            <i class="metismenu-icon">
                            </i> Deposit Loan <span class="badge badge-pill badge-primary"
                                style="margin-left: 10px">{{ $approved }}</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('loan?disbursement_status=Deposited') }}">
                            <i class="metismenu-icon">
                            </i> Disbursement Loan <span class="badge badge-pill badge-primary"
                                style="margin-left: 10px">{{ $deposited }}</span>
                        </a>
                    </li>
                    <?php } ?>
                    <?php if(in_array("loan-activated",$permissions)){ ?>
                    <li>
                        <a href="{{ url('loan?disbursement_status=Activated') }}">
                            <i class="metismenu-icon">
                            </i> Active Loan <span class="badge badge-pill badge-success"
                                style="margin-left: 10px">{{ $activated }}</span>
                        </a>
                    </li>
                    <?php } ?>
                    <?php if(in_array("loan-completed",$permissions)){ ?>

                    <li>
                        <a href="{{ url('loan?disbursement_status=Closed') }}">
                            <i class="metismenu-icon">
                            </i> Loan Complete <span class="badge badge-pill badge-info"
                                style="margin-left: 10px">{{ $closed }}</span>
                        </a>
                    </li>
                    <?php } ?>
                    <?php if(in_array("loan-declined",$permissions)){ ?>
                    <li>
                        <a href="{{ url('loan?disbursement_status=Declined') }}">
                            <i class="metismenu-icon">
                            </i> Loan Declined <span class="badge badge-pill badge-info"
                                style="margin-left: 10px">{{ $declined }}</span>
                        </a>
                    </li>
                    <?php } ?>
                    
                    <?php if(in_array("loan-canceled",$permissions)){ ?>
                    <li>
                        <a href="{{ url('loan?disbursement_status=Canceled') }}">
                            <i class="metismenu-icon">
                            </i> Canceled Loan <span class="badge badge-pill badge-danger"
                                style="margin-left: 10px">{{ $canceled }}</span>
                        </a>
                    </li>
                    <?php } ?>

                </ul>
            </li>
            @endif

            {{-- group deposit --}}
            @if(in_array("group-deposit",$permissions) || in_array("group-disbursement", $permissions))
            <li>
                <a href="#">
                    <i class="metismenu-icon fa fa-list"></i>
                    Group Process
                    <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                </a>

                <ul class="mm-collapse">
                    <?php if(in_array("group-deposit",$permissions)){ ?>

                    <li>
                        <a href="{{ url('group-deposit') }}">
                            <i class="metismenu-icon"></i> Group Deposit
                        </a>
                    </li>
                    <?php } ?>
                    <?php if(in_array("group-disburse",$permissions)){ ?>

                    <li>
                        <a href="{{ url('/loan_disbursements/group') }}">
                            <i class="metismenu-icon"></i> Group Disburse
                        </a>
                    </li>
                    <?php } ?>

                </ul>

            </li>
            @endif
            {{-- group deposit --}}

            <li>
            <li style="display: none">
                <a href="{{ url('loancalendars/') }}">
                    <i class="metismenu-icon fa fa-calendar"></i> Loan Calculator Calendar
                </a>
            </li>
            </li>

            <li class="app-sidebar__heading">Data Entry</li>

            <li>
                <a href="{{ url('news/') }}">
                    <i class="metismenu-icon fa fa-newspaper-o"></i> News Entry
                </a>
            </li>

            <li>
                <a href="{{ url('announcements/') }}">
                    <i class="metismenu-icon fa fa-bullhorn"></i> Announcements Entry
                </a>
            </li>
            <?php if(in_array("add-withdrawal",$permissions)){ ?>
            <li>
                <a href="{{ url('savingwithdrawl/create') }}">
                    <i class="metismenu-icon fa fa-money"></i> Add Withdrawl
                </a>
            </li>
            <?php } ?>

            @if(in_array("MANAGE LOAN", $permission_groups))
            <li>
                <a href="#">
                    <i class="metismenu-icon fa fa-plus"></i>General Entry
                    <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                </a>
                <ul class="mm-collapse">
                    <?php if(in_array("centers",$permissions)){ ?>

                    <li>
                        <a href="tables-regular.html">
                            <i class="metismenu-icon fa fa-plus"></i>Center
                            <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('centers/') }}">
                                    <i class="metismenu-icon">
                                    </i> All Process
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('centers/create') }}">
                                    <i class="metismenu-icon">
                                    </i> Create
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/center_leaders') }}">
                                    <i class="metismenu-icon">
                                    </i> Center Leaders
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php } ?>

                    <li>
                        <a href="#">
                            <i class="metismenu-icon fa fa-plus"></i>Group
                            <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('groups/') }}">
                                    <i class="metismenu-icon">
                                    </i> All Process
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('groups/create') }}">
                                    <i class="metismenu-icon">
                                    </i> Create
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="metismenu-icon fa fa-plus"></i>Guarantor
                            <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                        </a>
                        <ul>
                            <?php if(in_array("guarantor-list",$permissions)){ ?>
                            <li>
                                <a href="{{ url('guarantors/') }}">
                                    <i class="metismenu-icon">
                                    </i> All Process
                                </a>
                            </li>
                            <?php } ?>

                            <?php if(in_array("add-guarantor",$permissions)){ ?>
                            <li>
                                <a href="{{ url('guarantors/create') }}">
                                    <i class="metismenu-icon">
                                    </i> Create
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>
                </ul>
            </li>
            @endif

            @if(in_array('GENERAL SETTING', $permission_groups))
            <li>
                <a href="#">
                    <i class="metismenu-icon fa fa-plus"></i>Client Entry
                    <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                </a>
                <ul class="mm-collapse">
                    <?php if(in_array("add-address",$permissions)){ ?>

                    <li>
                        <a href="{{ url('#') }}">
                            <i class="metismenu-icon"></i>Address
                            <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('divisions/') }}">
                                    <i class="metismenu-icon"></i>Division
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('districts/') }}">
                                    <i class="metismenu-icon"></i>District
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('cities/') }}">
                                    <i class="metismenu-icon"></i>City
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('townships/') }}">
                                    <i class="metismenu-icon"></i>Townships
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('provinces/') }}">
                                    <i class="metismenu-icon"></i>Province
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('quarters/') }}">
                                    <i class="metismenu-icon"></i>Quarter
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('villages/') }}">
                                    <i class="metismenu-icon"></i>Village
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php } ?>

                    <li>
                        <a href="#">
                            <i class="metismenu-icon fa fa-plus"></i>Job Industry
                            <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('industries/') }}">
                                    <i class="metismenu-icon">
                                    </i>Industry
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('departments/') }}">
                                    <i class="metismenu-icon">
                                    </i>Department
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('jobpositions/') }}">
                                    <i class="metismenu-icon">
                                    </i>Job Position
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="metismenu-icon fa fa-plus"></i>Education
                            <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('educations/') }}">
                                    <i class="metismenu-icon">
                                    </i>Education Type
                                </a>
                            </li>

                        </ul>
                    </li>

                    <?php if(in_array("branches",$permissions)){ ?>
                    <li>
                        <a href="#">
                            <i class="metismenu-icon fa fa-plus"></i>Branch
                            <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('branches/') }}">
                                    <i class="metismenu-icon">
                                    </i> All Process
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('branches/create') }}">
                                    <i class="metismenu-icon">
                                    </i> Create
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php } ?>
                    <?php if(in_array("list-users",$permissions)){ ?>
                    <li>
                        <a href="#">
                            <i class="metismenu-icon fa fa-plus"></i> Staff
                            <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                        </a>
                        <ul>
                            <li>
                                <a href="{{ url('staffs/') }}">
                                    <i class="metismenu-icon">
                                    </i> All Process
                                </a>
                            </li>
                            <?php if(in_array("role-permission",$permissions)){ ?>
                            <li>
                                <a href="{{ url('staffs/create') }}">
                                    <i class="metismenu-icon">
                                    </i> Create
                                </a>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php } ?>
                            

                </ul>
            </li>
            @endif

            <li>
                <?php if(in_array("service-charges",$permissions) || in_array("compulsories-list",$permissions)){ ?>
                <a href="#">
                    <i class="metismenu-icon fa fa-plus"></i>Loan Entry
                    <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                </a>
                <?php } ?>
                <ul class="mm-collapse">
                    <?php if(in_array("service-charges",$permissions)){ ?>
                    <li>
                        <a href="{{ url('loancharges/') }}">
                            <i class="metismenu-icon"></i> Charges
                        </a>
                    </li>
                    <?php } ?>
                </ul>
                <ul class="mm-collapse">
                    <?php if(in_array("compulsories-list",$permissions) || in_array("compulsory-product",$permissions)){ ?>
                    <li>
                        <a href="{{ url('loancompulsory/') }}">
                            <i class="metismenu-icon"></i> Compulsory Products
                        </a>
                    </li>
                    <?php } ?>

                </ul>
            </li>

            @if(in_array('GENERAY SETTING', $permission_groups))
            <li>
                <a href="#">
                    <i class="metismenu-icon fa fa-plus"></i>Other Entry
                    <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                </a>
                <ul>
                    <li>
                        <a href="{{ url('companies/') }}">
                            <i class="metismenu-icon"></i> Company
                        </a>
                    </li>
                </ul>
                <ul>
                    <li>
                        <a href="{{ url('announce_departments/') }}">
                            <i class="metismenu-icon"></i> Department
                        </a>
                    </li>
                </ul>
                <ul>
                    <li>
                        <a href="{{ url('business_types/') }}">
                            <i class="metismenu-icon"></i> Business Types
                        </a>
                    </li>
                </ul>
                <ul>
                    <li>
                        <a href="{{ url('business_categories/') }}">
                            <i class="metismenu-icon"></i> Business Categories
                        </a>
                    </li>
                </ul>
            </li>
            @endif
            <?php if(in_array("chart-of-account",$permissions)){ ?>
            <li>
                <a href="#">
                    <i class="metismenu-icon fa fa-plus"></i>Account Entry
                    <i class="metismenu-state-icon fa fa-angle-down caret-left"></i>
                </a>
                <ul>
                    <li>
                        <a href="{{ url('/accounts') }}">
                            <i class="metismenu-icon"></i> Account
                        </a>
                    </li>
                </ul>
            </li>
            <?php } ?>

            @if(in_array("REPORTS", $permission_groups))
            <li class="app-sidebar__heading">Reports</li>
            <li>
                <a href="{{ url('client_reports') }}">
                    <i class="metismenu-icon fa fa-list"></i>
                    Client reports
                </a>
            </li>
            <?php if(in_array("loan-reports",$permissions)){ ?>
            <li>
                <a href="{{ url('loan_reports') }}">
                    <i class="metismenu-icon fa fa-list"></i>
                    Loan reports
                </a>
            </li>

            <?php } ?>
            @endif
            <li class="app-sidebar__heading">Crawl Process</li>
            <li>
                <a href="{{ url('crawl/') }}"">
                    <i class="metismenu-icon fas fa-sync"></i>
                    Crawl
                </a>
            </li>
            <li>
                <a href="{{ url('reverse_crawl/') }}"">
                    <i class="metismenu-icon fas fa-sync"></i>
                    Reverse Crawl
                </a>
            </li>


            </li>
        </ul>
    </div>
</div>
