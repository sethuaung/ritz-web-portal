<div class="modal fade" id="exampleModalLong" tabindex="-2" role="dialog" aria-labelledby="exampleModalLongTitle"
    style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg col-md-12">
        <form action="{{ route('loan_repayment.create') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header d-block" style="background-color:#fff;">

                    <div class="d-flex">
                        <h5 class="modal-title text-dark h4 font-weight-bold" id="scheduleHeading"> Repayment Process
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div><br>
                    <div class="row">
                        <div class="col-md-4" style="padding-left: 7px;">
                            <div class="card mb-0 widget-content " style="width: 257px; background:#115087;">
                                <div class="widget-content-wrapper text-light">
                                    <div class="widget-content-left">
                                        <i class="fas fa-user"></i>
                                        <span id="scheduleName" class="m-2 font-weight-bold"></span>
                                        <br><br>
                                        <i class="fas fa-id-badge"></i>
                                        <span class="m-2 font-weight-bold" id="scheduleNrc"></span>
                                        <br><br>
                                        <i class="fas fa-money-bill-wave"></i>
                                        <span class="m-1 font-weight-bold" id="pricipalOutstanding"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card mb-0 widget-content" style="width: 250px; background:#115087;">
                                <div class="widget-content-wrapper text-light">
                                    <div class="widget-content-left">
                                        <i class="fas fa-user-tie"></i>
                                        <span class="m-1 font-weight-bold" id="scheduleOfficerName"></span>
                                        <br><br>
                                        <i class="fas fa-building"></i>
                                        <span class="m-1 font-weight-bold" id="scheduleBranch"></span>
                                        <br><br>
                                        <i class="fas fa-money-bill-wave"></i>
                                        <span class="m-1 font-weight-bold" id="interestOutstanding"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card mb-0 widget-content" style="width: 240px; background:#115087;">
                                <div class="widget-content-wrapper text-light">
                                    <div class="widget-content-left">
                                        <i class="fas fa-money-bill-wave"></i>
                                        <span class="m-1 font-weight-bold" id="scheduleLoanAmount"></span>
                                        <br><br>
                                        <i class="fas fa-money-bill-wave"></i>
                                        <span class="m-1 font-weight-bold" id="scheduleInterest"></span>
                                        <br><br>
                                        <i class="fas fa-money-bill-wave"></i>
                                        <span class="m-1 font-weight-bold" id="totalOutstanding"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-body">

                    <table class="align-middle mb-0 table table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th class="text-center">No</th>
                                <th>Date</th>
                                <th>Principal</th>
                                <th>Interest</th>
                                <th>Payment</th>
                                <th>Balance</th>
                                <th class="text-nowrap">Repayment Type</th>
                                <th>Payment Date</th>
                            </tr>
                        </thead>
                        <tbody id="scheduleTbody">


                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn text-light" style="background:#115087;">Add Payment</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="exampleModalShort" tabindex="-1" role="dialog" aria-labelledby="exampleModalShortTitle"
    style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg col-md-12">
        <form action="{{ route('loan_approval.approveSingle') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header d-block" style="background-color:#fff;">

                    <div class="d-flex">
                        <h5 class="modal-title text-dark h4 font-weight-bold" id="scheduleHeading"
                            style="text-align: center">Loan Details</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12" style="padding-left: 7px;">
                            <div class="widget-content-wrapper text-dark">
                                <div class="widget-content-left">

                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Client ID</label>
                                        <input name="cid" id="cid" placeholder="For Example : "
                                            value="" type="text" class="form-control" readonly>
                                    </div>
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Loan ID</label>
                                        <input name="lid" id="lid" placeholder="For Example : "
                                            value="" type="text" class="form-control" readonly>
                                    </div>
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Loan Application Date</label>
                                        <input name="loanappdate" id="loanappdate" placeholder="For Example : "
                                            value="" type="text" class="form-control" readonly>
                                    </div>
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Requested Loan Amount</label>
                                        <input name="reqamount" id="reqamount" placeholder="For Example : "
                                            value="" type="text" class="form-control" readonly>
                                    </div>
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Plan Disburse Date</label>
                                        <input name="fdate" id="" placeholder=""
                                            value="{{ date('Y-m-d') }}" type="date" class="form-control">
                                            {{-- <input name="first_installment_date" id="first_installment_date" placeholder=""
                                    value="{{ date('Y-m-d', strtotime($first_installment_date)) }}" type="date" class="form-control" required> --}}
                                    </div>
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Approve Loan Amount</label>
                                        <input name="approveloanamount" id="approveloanamount" placeholder="For Example : "
                                            value="" type="text" class="form-control">
                                    </div>
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Approve Date</label>
                                        <input name="approvedate" id="approvedate" placeholder=""
                                            value="{{ date('Y-m-d') }}" type="date" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn text-light" style="background:#115087;">Approve</button>
                </div>
            </div>
        </form>
    </div>
</div>
