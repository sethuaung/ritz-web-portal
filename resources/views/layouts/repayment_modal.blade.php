<div class="modal fade" id="repaymentModal" tabindex="-2" role="dialog" aria-labelledby="repaymentModalTitle"
    style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg col-md-12">
        <div class="modal-content">
            <div class="modal-header">
                <h4 style="font-weight: bolder">Repayment Detail</h4>
            </div>
            <div class="modal-body">
                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th>Payment Date</th>
                            <th>Cash In</th>
                            <th>Payment Number</th>
                            <th>Principle</th>
                            <th>Interest</th>
                            <th>Owed Amount</th>
                        </tr>
                    </thead>
                    <tbody id="repaymentTBody">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn text-light" style="background:#115087;">Add Payment</button>
            </div>
        </div>
    </div>
</div>
