<!doctype html>
<html lang="en">

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>MKT Microfinance</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="This is an example dashboard created using build-in elements and components.">
    <meta name="msapplication-tap-highlight" content="no">

    <link rel="shortcut icon" href="{{ asset('admin/assets/css/images/logo_circle.png') }}" />
    <style>
        #printDeposit {
            display: none;
        }

        input[type="number"]::-webkit-outer-spin-button,
        input[type="number"]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        input[type="number"] {
            -moz-appearance: textfield;
        }

        @font-face {
            font-family: 'Font Awesome 5 Free';
            font-style: normal;
            font-weight: 900;
            src: url("{{ asset('admin/assets/fonts/fa-solid-900.eot') }}");
            src: url("{{ asset('admin/assets/fonts/fa-solid-900.eot?#iefix') }}") format('embedded-opentype'),
                url("{{ asset('admin/assets/fonts/fa-solid-900.woff2') }}") format('woff2'),
                url("{{ asset('admin/assets/fonts/fa-solid-900.woff') }}") format('woff'),
                url("{{ asset('admin/assets/fonts/fa-solid-900.ttf') }}") format('truetype'),
                url("{{ asset('admin/assets/fonts/fa-solid-900.svg#fontawesome') }}") format('svg')
        }

        @font-face {
            font-family: "Pe-icon-7-stroke";
            font-style: bold;
            src: url("http://db.onlinewebfonts.com/t/7218a2da5a03f622ca18085806382043.eot");
            /* IE9*/
            src: url("http://db.onlinewebfonts.com/t/7218a2da5a03f622ca18085806382043.eot?#iefix") format("embedded-opentype"),
                /* IE6-IE8 */
                url("http://db.onlinewebfonts.com/t/7218a2da5a03f622ca18085806382043.woff2") format("woff2"),
                /* chrome firefox */
                url("http://db.onlinewebfonts.com/t/7218a2da5a03f622ca18085806382043.woff") format("woff"),
                /* chrome firefox */
                url("http://db.onlinewebfonts.com/t/7218a2da5a03f622ca18085806382043.ttf") format("truetype"),
                /* chrome firefox opera Safari, Android, iOS 4.2+*/
                url("http://db.onlinewebfonts.com/t/7218a2da5a03f622ca18085806382043.svg#Pe-icon-7-stroke") format("svg");
            /* iOS 4.1- */
        }

        .scrollbar-sidebar {
            width: 300px;
            height: 100px;
            overflow-y: scroll;
            /* background-color: #f6f6f6; */
        }

        .scrollbar-sidebar::-webkit-scrollbar {
            background-color: transparent;
            width: 0px;
        }

        .scrollbar-sidebar:hover::-webkit-scrollbar {
            width: 7px;
        }

        .scrollbar-sidebar::-webkit-scrollbar-thumb {
            background-color: rgba(0, 0, 0, .2);
            border-radius: 10px;
        }

        .reset_btn {
            text-align: right;
        }

        .reset_btn .btn {
            background-color: #FFE7E7;
            border-radius: 100px;
            box-shadow: rgba(44, 187, 99, .2) 0 -25px 18px -14px inset, rgba(44, 187, 99, .15) 0 1px 2px, rgba(44, 187, 99, .15) 0 2px 4px, rgba(44, 187, 99, .15) 0 4px 8px, rgba(44, 187, 99, .15) 0 8px 16px, rgba(44, 187, 99, .15) 0 16px 32px;
            color: #D33A2C;
            cursor: pointer;
            display: inline-block;
            padding: 7px 20px;
            text-align: center;
            text-decoration: none;
            transition: all 250ms;
            border: 0;
            font-size: 16px;
            user-select: none;
            -webkit-user-select: none;
            touch-action: manipulation;
        }

        .reset_btn .btn {
            box-shadow: rgba(44, 187, 99, .35) 0 -25px 18px -14px inset, rgba(44, 187, 99, .25) 0 1px 2px, rgba(44, 187, 99, .25) 0 2px 4px, rgba(44, 187, 99, .25) 0 4px 8px, rgba(44, 187, 99, .25) 0 8px 16px, rgba(44, 187, 99, .25) 0 16px 32px;
            transform: scale(1.05) rotate(-1deg);
        }

        #clientCard,
        #guarantorACard,
        #guarantorBCard,
        #guarantorCCard,
        #clientEditCard,
        #guarantorEditACard,
        #guarantorEditBCard,
        #guarantorEditCCard {
            animation: myani .2s ease-out;
        }

        @keyframes myani {
            from {
                opacity: 0.3;
                transform: scale(0.5);
            }

            to {
                opacity: 1;
                transform: scale(1);
            }
        }
    </style>
    <!-- <link href="{{ asset('admin/assets/css/main.css') }}" rel="stylesheet"></head> -->

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">

    <link href="{{ asset('admin/assets/css/main_update.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/assets/css/color.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/assets/css/color_tab.css') }}" rel="stylesheet">
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


<script src="{{ asset('admin/assets/scripts/jchart.js') }}"></script>
@yield('style')

<!-- <link href="https://db.onlinewebfonts.com/c/7218a2da5a03f622ca18085806382043?family=Pe-icon-7-stroke" rel="stylesheet" type="text/css"/> -->

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow bg-night-sky header-text-light">
            <div class="app-header__logo">
                <div class="logo-src"></div>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                            data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button"
                        class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>
            <div class="app-header__content">
                <!-- Header Bar Left -->
                <div class="app-header-left">
                    {{-- <div class="search-wrapper">
                    <div class="input-holder">
                        <input type="text" class="search-input" placeholder="Type to search">
                        <button class="search-icon"><span></span></button>
                    </div>
                    <button class="close"></button>
                </div> --}}
                    <ul class="header-menu nav">
                        <?php
                        $branchId = DB::table('tbl_staff')
                            ->where('staff_code', Session::get('staff_code'))
                            ->select('branch_id')
                            ->first();
                        $name = DB::table('tbl_branches')
                            ->where('id', $branchId->branch_id)
                            ->select('branch_name')
                            ->first();
                        ?>
                        @if (count(DB::table('user_branches')->where('user_id', session()->get('staff_code'))->get()) == 0)
                            <li class="nav-item">
                                <form action="{{ route('changeBranch') }}" method="POST" id="changeBranchForm">
                                    @csrf
                                    <select class="form-control" name="branch_id" style="width: 180px"
                                        id="changeBranch">
                                        @php
                                            $branches = DB::table('tbl_branches')->get();
                                            $loginBranch = DB::table('tbl_staff')
                                                ->where('staff_code', session()->get('staff_code'))
                                                ->first()->branch_id;
                                        @endphp
                                        @foreach ($branches as $branch)
                                            <option @if ($loginBranch == $branch->id) selected @endif
                                                value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
                                        @endforeach
                                    </select>
                                </form>
                            </li>
                        @endif
                        @if (count(DB::table('user_branches')->where('user_id', session()->get('staff_code'))->get()) > 1)
                            @php
                                $userbranches = DB::table('user_branches')->where('user_id', session()->get('staff_code'))->get();
                            @endphp
                            <li class="nav-item">
                                <form action="{{ route('changeBranch') }}" method="POST" id="changeBranchForm">
                                    @csrf
                                    <select class="form-control" name="branch_id" style="width: 180px"
                                        id="changeBranch">
                                        @php
                                            $loginBranch = DB::table('tbl_staff')
                                                ->where('staff_code', session()->get('staff_code'))
                                                ->first()->branch_id;
                                        @endphp
                                        @foreach ($userbranches as $userbranch)
                                            <option @if ($loginBranch == $userbranch->branch_id) selected @endif
                                                value="{{ $userbranch->branch_id }}">{{ DB::table('tbl_branches')->where('id', $userbranch->branch_id)->first()->branch_name }}</option>
                                        @endforeach
                                    </select>
                                </form>
                            </li>
                        @endif
                        @if (count(DB::table('user_branches')->where('user_id', session()->get('staff_code'))->get()) == 1)
                            <li class="nav-item">
                                <a href="javascript:void(0);" class="nav-link">
                                    <i class="nav-link-icon fa fa-home"> </i>

                                    {{ $name->branch_name }}
                                </a>
                            </li>
                        @endif
                        <li class="btn-group nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                <i class="nav-link-icon fa fa-users"></i>
                                <?php
                                $total_clients = DB::table('tbl_client_join')
                                    ->where('branch_id', $branchId->branch_id)
                                    ->count();
                                // dd($total_clients);
                                ?>
                                {{ $total_clients }}
                            </a>
                        </li>
                        <li class="btn-group nav-item">
                            <a href="javascript:void(0);" class="nav-link">
                                <i class="nav-link-icon fa fa-briefcase"></i>
                                <?php
                                $staff_code = Session::get('staff_code');
                                $B_code = DB::table('tbl_staff')
                                    ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
                                    ->select('tbl_branches.branch_code')
                                    ->where('tbl_staff.staff_code', $staff_code)
                                    ->first();
                                $b_code = strtolower($B_code->branch_code);
                                
                                $total_loans = DB::table($b_code . '_loans')->count();
                                // dd($total_loans);
                                ?>
                                {{ $total_loans }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- Header Bar Right -->
                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                {{-- test --}}
                                <div class="btn-group position-relative pf-popup">
                                    <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                        class="p-0 btn pf-btn">
                                        <img width="42" class="rounded-circle"
                                            src="{{ asset('public/storage/clientphotos/' .DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->photo) }}"
                                            alt="">
                                        <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                    </a>
                                    <div class="widget-content-left  ml-3 header-user-info pt-2">
                                        <div class="widget-heading">
                                            {{ \App\Models\ClientDataEnteries\Staff::where('staff_code', session()->get('staff_code'))->first()->name }}
                                        </div>
                                        <a href="{{ route('signout') }}">
                                            <div class="widget-subheading">
                                                Logout
                                            </div>
                                        </a>
                                    </div>
                                    <div tabindex="-1" role="menu" aria-hidden="true"
                                        class="rm-pointers dropdown-menu-lg dropdown-menu dropdown-menu-right p-0 pf-popup"
                                        x-placement="bottom-end" style="border-radius: 7px">
                                        <div class="dropdown-menu-header">
                                            <div class="dropdown-menu-header-inner bg-info"
                                                style="border-radius: 7px">
                                                <div class="menu-header-image opacity-2" style=""></div>
                                                <div class="menu-header-content text-left">
                                                    <div class="widget-content p-0"
                                                        style="background: #0a66b7; border-radius: 7px 7px 0 0">
                                                        <div class="widget-content-wrapper p-3">
                                                            <div class="widget-content-left mr-3">
                                                                @php
                                                                    $staffPhoto =
                                                                        DB::table('tbl_staff')
                                                                            ->where('staff_code', session()->get('staff_code'))
                                                                            ->first()->photo != null
                                                                            ? DB::table('tbl_staff')
                                                                                ->where('staff_code', session()->get('staff_code'))
                                                                                ->first()->photo
                                                                            : 'default_user_photo.jpg';
                                                                @endphp
                                                                <img width="42" class="rounded-circle"
                                                                    src="{{ asset('public/storage/clientphotos/' . $staffPhoto) }}"
                                                                    alt="">
                                                            </div>
                                                            <div class="widget-content-left">
                                                                <div class="widget-heading">
                                                                    {{ DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->name }}
                                                                </div>
                                                                <div class="widget-subheading opacity-8">
                                                                    {{ DB::table('tbl_branches')->where('id',DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id)->first()->branch_name }}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="scroll-area-xs" style="height: auto">
                                            <div class="scrollbar-container ps">
                                                <ul class="nav flex-column">
                                                    <li class="nav-item">
                                                        <p class="nav-link mb-0">
                                                            <i class="fas fa-info-circle mr-2"></i>
                                                            {{ DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->staff_code }}
                                                        </p>
                                                    </li>
                                                    <li class="nav-item">
                                                        <p class="nav-link mb-0">
                                                            <i class="fas fa-envelope mr-2"></i>
                                                            {{ DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->email }}
                                                        </p>
                                                    </li>
                                                    <li class="nav-item">
                                                        <p class="nav-link mb-0">
                                                            <i class="fas fa-id-card mr-2"></i>
                                                            {{ DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->nrc }}
                                                        </p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <ul class="nav flex-column">
                                            <li class="nav-item-divider nav-item">
                                            </li>
                                            <li class="nav-item-btn text-center nav-item">
                                                <div class="widget-content-right mr-2">
                                                    <a href="{{ route('signout') }}"
                                                        class="btn-pill btn-shadow btn-shine btn btn-focus">Logout</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                {{-- test --}}

                                <div class="widget-content-right header-user-info ml-3">
                                    <a href="{{ url('loancalendars/') }}">
                                        <button type="button" class="btn-shadow p-1 btn btn-primary btn-sm">
                                            <i class="fa text-white fa-calendar pr-1 pl-1"></i>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="app-main">
            <div class="app-sidebar sidebar-shadow">
                <!-- Header logo -->
                <div class="app-header__logo">
                    <div class="logo-src"></div>
                    <div class="header__pane ml-auto">
                        <div>
                            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                                data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <!-- Left Slider Menu / mobile-menu -->
                <div class="app-header__mobile-menu">
                    <div>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
                <!-- Buttom Menu -->
                <div class="app-header__menu">
                    <span>
                        <button type="button"
                            class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                            </span>
                        </button>
                    </span>
                </div>
                <!-- Left Slider Menu -->
                @include('layouts.app_sidebar')
            </div>
            <div class="app-main__outer">
                <!-- Content -->
                <div>
                    @yield('content')
                </div>
                <!-- Footer -->
                <!-- <div class="app-wrapper-footer">
                <div class="app-footer">
                    <div class="app-footer__inner">
                        <div class="app-footer-left">
                        </div>
                        <div class="app-footer-right">
                            <ul class="nav">
                                <li class="nav-item">
                                    <a href="javascript:void(0);" class="nav-link">
                                        <div class="badge badge-success mr-1 ml-0">
                                            <small>RITZ</small>
                                        </div>
                                        Copyright © 2021. All rights reserved.
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>     -->
            </div>
        </div>
    </div>


    <!-- modal blade add -->
    @include('layouts.app_modal')
    @include('layouts.repayment_modal')
    <script type="text/javascript" src="{{ asset('admin/assets/scripts/main.js') }}"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.5.0.js"></script> -->

    {{-- html2pdf --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.min.js"
        integrity="sha512-pdCVFUWsxl1A4g0uV6fyJ3nrnTGeWnZN2Tl/56j45UvZ1OMdm9CIbctuIHj+yBIRTUUyv6I9+OivXj4i0LPEYA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    {{-- html2excel --}}
    <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>


    <!-- <script type="text/javascript" src="{{ asset('admin/assets/scripts/custom.js') }}"></script> -->

    {{-- sweetalert --}}
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        // sweetalert
        function statusAlert(msg) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: 'success',
                title: msg
            })
        }

        function errorAlert(msg) {
            Swal.fire({
                icon: 'error',
                text: msg
            })
        }

        //has nrc
        $(function() {
            $('#nrc_type1').hide();
            $('#nrc_type2').hide();

            $('#has_nrc').change(function() {

                var nrc_status = $(this).val();
                if (nrc_status == "Yes") {
                    $('#nrc_type1').show();
                    $('#nrc_type2').hide();
                    $('#newNRC').hide();
                    $('#oldNRC').hide();
                    $('#stillNRC').hide();
                    $('#lossNRC').hide();
                    $('#noNrcPhoto').hide();
                    $('#hasNrcPhoto').hide();
                    $('#nrc_card_id').prop('disabled', false);
                    $('#nrc_card_id').prop('required', true);
                    $('#nrcRecom').prop('required', false);


                } else if (nrc_status == 'No') {
                    $('#nrc_type1').hide();
                    $('#nrc_type2').show();
                    $('#newNRC').hide();
                    $('#oldNRC').hide();
                    $('#stillNRC').hide();
                    $('#lossNRC').hide();
                    $('#noNrcPhoto').hide();
                    $('#hasNrcPhoto').hide();
                    $('#nrc_card_id').prop('disabled', true);
                    $('#nrc_card_id').prop('required', false);
                    $('#nrcFront').prop('required', false);
                    $('#nrcBack').prop('required', false);
                    $('#nrcRecom').prop('required', true);


                } else {
                    $('#nrc_type1').hide();
                    $('#nrc_type2').hide();
                    $('#newNRC').hide();
                    $('#oldNRC').hide();
                    $('#stillNRC').hide();
                    $('#lossNRC').hide();
                    $('#noNrcPhoto').hide();
                    $('#hasNrcPhoto').hide();
                    $('#nrc_card_id').prop('disabled', false);
                    $('#nrc_card_id').prop('required', true);

                }
            });
        });


        //nrc type (old,new or no)
        $(function() {
            $('#newNRC').hide();
            $('#oldNRC').hide();
            $('#stillNRC').hide();
            $('#lossNRC').hide();
            $('#noNrcPhoto').hide();
            $('#hasNrcPhoto').hide();

            $('#nrc_type_select1').change(function() {
                var nrc_type_status = $(this).val();
                if (nrc_type_status == "Old NRC") {
                    $('#oldNRC').show();
                    $('#newNRC').hide();
                    $('#stillNRC').hide();
                    $('#lossNRC').hide();
                    $('#noNrcPhoto').hide();
                    $('#hasNrcPhoto').show();
                    $('#nrcFront').prop('required', true);
                    $('#nrcBack').prop('required', true);



                } else if (nrc_type_status == "New NRC") {
                    $('#newNRC').show();
                    $('#noNrcPhoto').hide();
                    $('#hasNrcPhoto').show();
                    $('#oldNRC').hide();
                    $('#stillNRC').hide();
                    $('#lossNRC').hide();
                    $('#nrcFront').prop('required', true);
                    $('#nrcBack').prop('required', true);



                } else {
                    $('#newNRC').hide();
                    $('#oldNRC').hide();
                    $('#stillNRC').hide();
                    $('#lossNRC').hide();
                    $('#noNrcPhoto').hide();
                    $('#hasNrcPhoto').hide();
                    $('#nrcFront').prop('required', false);
                    $('#nrcBack').prop('required', false);


                }


            });
            $('#nrc_type_select2').change(function() {
                var nrc_type_status = $(this).val();
                if (nrc_type_status == "Still Applying(လျှောက်ထားဆဲ)") {
                    $('#stillNRC').show();
                    $('#noNrcPhoto').show();
                    $('#hasNrcPhoto').hide();
                    $('#newNRC').hide();
                    $('#oldNRC').hide();
                    $('#lossNRC').hide();

                } else if (nrc_type_status == "Loss NRC(ပျောက်ဆုံး)") {
                    $('#lossNRC').show();
                    $('#newNRC').hide();
                    $('#oldNRC').hide();
                    $('#stillNRC').hide();
                    $('#noNrcPhoto').show();
                    $('#hasNrcPhoto').hide();

                } else {
                    $('#newNRC').hide();
                    $('#oldNRC').hide();
                    $('#stillNRC').hide();
                    $('#lossNRC').hide();
                    $('#hasNrcPhoto').hide();
                    $('#noNrcPhoto').hide();
                }
            });
        });

        // salary yearly auto cal
        $('#salary,#business_income,#income').keyup(function() {
            var salary = $('#salary').val();
            var income = $('#business_income').val();
            var g_income = $('#income').val();
            $('#salary_yearly').val(salary * 12);
            $('#g_income_yearly').val(g_income * 12);

            $('#income1').text("Yearly Income (" + income + "x12)" + "=" + income * 12)

        }).keyup();

        // address autofill in field
        // 	$('#village_id').keyup(function () {
        //         var addressArray = [$("#village_id" option:selected).text(), $("#quarter_id option:selected").text(), $("#township_id option:selected").text(), $("#district_id option:selected").text(), $("#division_id").find(":selected").text()]

        //         function displayVals() {
        //             $('#address_primary').text(addressArray.join(', '));
        //         }

        //         $('select').keyup(displayVals);
        //         displayVals();

        //     }).keyup();

        $('#home_no').keyup(function() {
            var addressArray = [$('#home_no').val(),
             $("#ward_id").find(":selected").text(),
                $("#village_id").find(":selected").text(),
                 $("#quarter_id option:selected").text(), 
                 $("#township_id option:selected").text(),
                  $("#district_id option:selected").text(),
                   $("#division_id").find(":selected").text()
            ]

            function displayVals() {
                $('#address_primary').text(addressArray.join(','));
            }

            $('select').keyup(displayVals);
            displayVals();

        }).keyup();

        $('#village_id').change(function() {
            var addressArray = [$('#home_no').val(),
                $("#ward_id").find(":selected").text(),
                $("#village_id").find(":selected").text(),
                $("#township_id option:selected").text(),
                $("#district_id option:selected").text(),
                $("#division_id").find(":selected").text()
            ]

            function displayVals() {
                $('#address_primary').text(addressArray.join(','));
            }

            $('select').keyup(displayVals);
            displayVals();
        });
        // function displayVals(){
        //     var addressArray = [$('#home_no').val(),$('#street_name').val(),$("select#village_id").val(),$("select#quarter_id").val(),$("select#township_id").val(),$("select#district_id").val(),$("select#division_id").val()]
        //     var home_no =$('#home_no').val();
        //     $("#home_no,#street_name").keyup(function(){
        //         $('#address_primary').text(addressArray.join(','));
        //     }).keyup();
        // }
        // $('select,input').change(displayVals);
        // displayVals();


        //hasjob
        $(function() {
            $('#business_name').hide();
            $('#business').hide();

            $('#job_status').change(function() {

                var job_status = $(this).val();


                if (job_status == "Has Job") {
                    $('#industry_id').prop('disabled', false);
                    $('#department_id').prop('disabled', false);
                    $('#job_position_id').prop('disabled', false);
                    $('#company_name').prop('disabled', false);
                    $('#working_phone').prop('disabled', false);
                    $('#salary').prop('disabled', false);
                    $('#experience').prop('disabled', false);
                    $('#experience2').prop('disabled', false);
                    $('#company_address').prop('disabled', false);
                    $('#business_name').hide();
                    $('#business').hide();
                } else if (job_status == "Current Business") {
                    $('#industry_id').prop('disabled', false);
                    $('#department_id').prop('disabled', false);
                    $('#job_position_id').prop('disabled', false);
                    $('#company_name').prop('disabled', false);
                    $('#working_phone').prop('disabled', false);
                    $('#salary').prop('disabled', false);
                    $('#experience').prop('disabled', false);
                    $('#experience2').prop('disabled', false);
                    $('#company_address').prop('disabled', false);
                    $('#business_name').show();
                    $('#business').show();

                } else {
                    $('#industry_id').val("").prop('disabled', true);
                    $('#department_id').val("").prop('disabled', true);
                    $('#job_position_id').val("").prop('disabled', true);
                    $('#company_name').val("").prop('disabled', true);
                    $('#working_phone').val("").prop('disabled', true);
                    $('#salary').val("").prop('disabled', true);
                    $('#salary_yearly').val("").prop('disabled', true);
                    $('#experience').val("").prop('disabled', true);
                    $('#experience2').val("").prop('disabled', true);
                    $('#company_address').val("").prop('disabled', true);
                    $('#business_name').hide();
                    $('#business').hide();
                }
            });
        })


        //has marital status

        $('#marital_status').change(function() {
            var marital_status = $(this).val();
            if (marital_status == "Married") {
                document.getElementById("spouse_name").disabled = false;
                document.getElementById("occupation").disabled = false;
                document.getElementById("no_of_children").disabled = false;

            } else if (marital_status == "Single") {
                document.getElementById("spouse_name").disabled = true;
                document.getElementById("occupation").disabled = true;
                document.getElementById("no_of_children").disabled = true;

            } else {
                document.getElementById("spouse_name").disabled = true;
                document.getElementById("occupation").disabled = true;

            }
        });


        $(document).ready(function() {
            $('input').attr('autocomplete', 'off');

            //    need to fix for Center Edit
            var typestatus = $('#type_status').children(":selected").val();


        });
        // staff_create_autogenerate_start
        $('#branch_id').change(function() {
            var addrcode = $(this).find('option:selected').data('address');
            var branchid = $(this).val();
            if (addrcode != "") {
                $.ajax({
                    url: "{{ url('staff/uniqueid') }}",
                    method: "GET",
                    data: {
                        branchid: branchid
                    },
                    success: function(data) {
                        $('#staff_code').val(addrcode + '-' + data);
                    }
                });

            } else {
                $("#staff_code").val("");
            }
        });
        // staff_create_autogenerate_end



        // centercreate

        // auto select with session start
        if ($('#branch_id_center').val()) {
            var addrcode = $("#branch_id_center").find('option:selected').data('address');
            var branchid = $("#branch_id_center").val();

            $.ajax({
                url: "{{ url('center/uniqueid') }}",
                method: "GET",
                data: {
                    branchid: branchid
                },
                success: function(data) {
                    $('#center_uniquekey').val(addrcode + '-05-' + data);
                }
            });


            $.ajax({
                type: "GET",
                url: "{{ url('getStaff') }}?branch_id=" + branchid,
                success: function(res) {

                    if (res) {
                        $("#staff_id_center").empty();
                        $("#staff_id_center").append(
                            '<option value="">Choose Staff</option>'
                        );
                        $.each(res, function(key, value) {
                            $("#staff_id_center").append(
                                '<option value="' + key + '">' + value + '</option>'
                            );
                        });
                    } else {
                        $("#staff_id_center").empty();
                    }
                },
            });
        }
        //auto select with session end

        $('#branch_id_center').change(function() {
            var addrcode = $(this).find('option:selected').data('address');
            var branchid = $(this).val();
            if (addrcode != "") {
                $.ajax({
                    url: "{{ url('center/uniqueid') }}",
                    method: "GET",
                    data: {
                        branchid: branchid
                    },
                    success: function(data) {
                        $('#center_uniquekey').val(addrcode + '-05-' + data);
                    }
                });

            } else {
                $("#center_uniquekey").val("");
            }

            var branch_id = $(this).val();
            if (branch_id) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('getStaff') }}?branch_id=" + branch_id,
                    success: function(res) {
                        if (res) {
                            $("#staff_id_center").empty();
                            $("#staff_id_center").append(
                                '<option value="">Choose Staff</option>'
                            );
                            $.each(res, function(key, value) {
                                $("#staff_id_center").append(
                                    '<option value="' + key + '">' + value + '</option>'
                                );
                            });
                        } else {
                            $("#staff_id_center").empty();
                        }
                    },
                });
            }
        });
        // centercreate_end

        // groupcreate

        if ($("#branch_id_group").val()) {
            var addrcode = $("#branch_id_group").find('option:selected').data('address');
            var branchid = $("#branch_id_group").val();

            $.ajax({
                url: "{{ url('group/uniqueid') }}",
                method: "GET",
                data: {
                    branchid: branchid
                },
                success: function(data) {
                    $('#group_uniquekey').val(addrcode + '-06-' + data);
                }
            });
        }

        $('#branch_id_group').change(function() {
            // var _token = $('input[name="_token"]').val();
            var addrcode = $(this).find('option:selected').data('address');

            var branchid = $(this).val();
            if (addrcode != "") {
                $.ajax({
                    url: "{{ url('group/uniqueid') }}",
                    method: "GET",
                    data: {
                        branchid: branchid
                    },
                    success: function(data) {
                        $('#group_uniquekey').val(addrcode + '-06-' + data);
                    }
                });

            } else {
                $("#group_uniquekey").val("");
            }

            // $.ajax({
            //     url: "{{ url('getLatestId') }}",
            //     method: "POST",
            //     data: {bcode: addr, _token: _token},
            //     success: function (data) {
            //         $('#group_uniquekey').val(addr + '-06-' + idnum);
            //     }
            // });

            var branch_id = $(this).val();
            if (branch_id) {
                //center
                $.ajax({
                    type: "GET",
                    url: "{{ url('getCenter') }}?branch_id=" + branch_id,
                    success: function(res) {
                        if (res) {
                            $("#center_id_group").empty();
                            $("#center_id_group").append(
                                '<option value="">Choose Center</option>'
                            );
                            $.each(res, function(key, value) {
                                $("#center_id_group").append(
                                    '<option value="' + value + '">' + value + '</option>'
                                );
                            });
                        } else {
                            $("#center_id_group").empty();
                        }
                    },
                });
                //loan officer
                $.ajax({
                    type: "GET",
                    url: "{{ url('getStaff') }}?branch_id=" + branch_id,
                    success: function(res) {
                        if (res) {
                            $("#staff_id").empty();
                            $("#staff_id").append(
                                '<option value="">Choose Staff</option>'
                            );
                            $.each(res, function(key, value) {
                                $("#staff_id").append(
                                    '<option value="' + key + '">' + value + '</option>'
                                );
                            });
                        } else {
                            $("#staff_id").empty();
                        }
                    },
                });
            } else {
                $("#center_id_group").empty();

            }
        });

        $('#center_id_group').change(function() {

            let center_id = $(this).val();
            if (center_id) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('getStaff') }}?center_id=" + center_id,
                    success: function(res) {
                        $('#staff_id_group').val(res[0].staff_code);
                        $('#staff_name_group').val(res[0].name);
                    }
                })
            } else {
                $('#staff_id_group').empty();
                $('#staff_name_group').empty();

            }
        });
        // groupcreate_end

        $('#type_status').val('client');

        if ($('#type_status option:selected')) {
            var type_status = $("#type_status").val();
            if (type_status) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('getStaffClient') }}?type_status=" + type_status,
                    success: function(res) {
                        if (res) {
                            $("#staff_client_id").empty();
                            $("#staff_client_id").append('<option value="">Select Center Leader</option>');
                            $.each(res, function(key, value) {
                                $("#staff_client_id").append('<option value="' + key + '">' + value +
                                    '(' + key + ')' + '</option>');
                            });

                        } else {
                            $("#staff_client_id").empty();
                        }
                    }
                });
            }
        }

        $('#type_status').change(function() {
            $("#staff_client_id").empty();
            var type_status = $(this).val();
            if (type_status) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('getStaffClient') }}?type_status=" + type_status,
                    success: function(res) {
                        if (res) {
                            $("#staff_client_id").empty();
                            $("#staff_client_id").append(
                                '<option value="">Select Center Leader</option>');
                            $.each(res, function(key, value) {
                                $("#staff_client_id").append('<option value="' + value + '">' +
                                    value + '</option>');
                            });

                        } else {
                            $("#staff_client_id").empty();
                        }
                    }
                });
            } else {
                $("#staff_client_id").empty();
                $("#type_status").empty();
            }
        });

        //   nrc
        $('#nrc_1').change(function() {
            $("#nrc_2").empty();
            var nrc_1 = $(this).val();
            if (nrc_1) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('getNRC') }}?nrc_1=" + nrc_1,
                    success: function(res) {
                        if (res) {
                            $("#nrc_2").empty();

                            $.each(res, function(key, value) {
                                $("#nrc_2").append('<option value="' + value.name_en + '">' +
                                    value.name_en + '</option>');
                            });

                        } else {
                            $("#staff_client_id").empty();
                        }
                    }
                });
            } else {
                $("#staff_client_id").empty();
                $("#type_status").empty();
            }
        });

        // group_client_staff_leader

        if ($("#level_status").val()) {
            var level_status = $("#level_status").val();
            if (level_status == "leader") {
                document.getElementById("client_id").disabled = false;
            } else {
                document.getElementById("client_id").disabled = false;
            }
        }

        $('#level_status').change(function() {
            var level_status = $(this).val();
            if (level_status == "leader") {
                document.getElementById("client_id").disabled = false;
            } else {
                document.getElementById("client_id").disabled = false;
            }
        });
        // group_client_staff_leader_end


        $(document).ready(function() {

            // staff_create
            var OldValue_branch = "{{ old('branch_id') }}";
            if (OldValue_branch !== '') {
                $('#branch_id').val(OldValue_branch);
            }

            // township_create
            var OldValue_district = "{{ old('district_id') }}";
            if (OldValue_district !== '') {
                $('#district_id').val(OldValue_district);
            }

            // quarter_create
            var OldValue_township = "{{ old('township_id') }}";
            if (OldValue_township !== '') {
                $('#township_id').val(OldValue_township);
            }

            // department_create
            var OldValue_industry = "{{ old('industry_id') }}";
            if (OldValue_industry !== '') {
                $('#industry_id').val(OldValue_industry);
            }
        });


        $(document).ready(function() {

            var animating = false,
                submitPhase1 = 1100,
                submitPhase2 = 400,
                logoutPhase1 = 800,
                $login = $(".login"),
                $app = $(".app");

            function ripple(elem, e) {
                $(".ripple").remove();
                var elTop = elem.offset().top,
                    elLeft = elem.offset().left,
                    x = e.pageX - elLeft,
                    y = e.pageY - elTop;
                var $ripple = $("<div class='ripple'></div>");
                $ripple.css({
                    top: y,
                    left: x
                });
                elem.append($ripple);
            };

            $(document).on("click", ".login__submit", function(e) {
                if (animating) return;
                animating = true;
                var that = this;
                ripple($(that), e);
                $(that).addClass("processing");
                setTimeout(function() {
                    $(that).addClass("success");
                    setTimeout(function() {
                        $app.show();
                        $app.css("top");
                        $app.addClass("active");
                    }, submitPhase2 - 70);
                    setTimeout(function() {
                        $login.hide();
                        $login.addClass("inactive");
                        animating = false;
                        $(that).removeClass("success processing");
                    }, submitPhase2);
                }, submitPhase1);
            });

            $(document).on("click", ".app__logout", function(e) {
                if (animating) return;
                $(".ripple").remove();
                animating = true;
                var that = this;
                $(that).addClass("clicked");
                setTimeout(function() {
                    $app.removeClass("active");
                    $login.show();
                    $login.css("top");
                    $login.removeClass("inactive");
                }, logoutPhase1 - 120);
                setTimeout(function() {
                    $app.hide();
                    animating = false;
                    $(that).removeClass("clicked");
                }, logoutPhase1);
            });

        });

        $('#industry_id').change(function() {
            var industry_id = $(this).val();
            if (industry_id) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('getDept') }}?industry_id=" + industry_id,
                    success: function(res) {
                        if (res) {
                            $("#department_id").empty();
                            $("#department_id").append('<option value="">Select Department</option>');
                            $.each(res, function(key, value) {
                                $("#department_id").append('<option value="' + key + '">' +
                                    value + '</option>');
                            });

                        } else {
                            $("#department_id").empty();
                        }
                    }
                });
            } else {
                $("#department_id").empty();
                $("#industry_id").empty();
            }
        });
        //getJob
        $("#department_id").change(function() {
            var department_id = $(this).val();
            if (department_id) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('getJob') }}?department_id=" + department_id,
                    success: function(res) {
                        if (res) {
                            $("#job_position_id").empty();
                            $("#job_position_id").append(
                                '<option value="">Select Job Position</option>'
                            );
                            $.each(res, function(key, value) {
                                $("#job_position_id").append(
                                    '<option value="' + key + '">' + value + '</option>'
                                );
                            });
                        } else {
                            $("#job_position_id").empty();
                        }
                    },
                });
            } else {
                $("#department_id").empty();
                $("#job_position_id").empty();
            }
        });


        //getDistrict
        $("#division_id").change(function() {
            $("#district_id").empty();
            $("#township_id").empty();
            $("#village_id").empty();
            $("#ward_id").empty();

            var division_id = $(this).val();
            if (division_id) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('getDistrict') }}?division_id=" + division_id,
                    success: function(res) {

                        if (res) {
                            $("#district_id").empty();
                            $("#district_id").append(
                                '<option value="">Select Division</option>'
                            );
                            $.each(res, function(key, value) {

                                $("#district_id").append(
                                    '<option value="' + value.code + '">' + value.name +
                                    ' / ' + value.description + '</option>'
                                );
                            });
                        } else {
                            $("#district_id").empty();
                        }
                    },
                });
            } else {
                $("#division_id").empty();
                $("#district_id").empty();
            }
        });
        //getCity
        $("#division_id").change(function() {
            var division_id = $(this).val();
            if (division_id) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('getCity') }}?division_id=" + division_id,
                    success: function(res) {
                        if (res) {
                            $("#city_id").empty();
                            $("#city_id").append(
                                '<option value="">Select City</option>'
                            );
                            $.each(res, function(key, value) {
                                $("#city_id").append(
                                    '<option value="' + key + '">' + value + '</option>'
                                );
                            });
                        } else {
                            $("#city_id").empty();
                        }
                    },
                });
            } else {
                $("#division_id").empty();
                $("#city_id").empty();
            }
        });
        //getTwonship
        $("#district_id").change(function() {
            $("#township_id").empty();
            $("#village_id").empty();
            $("#ward_id").empty();
            var district_id = $(this).val();
            if (district_id) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('getTownship') }}?district_id=" + district_id,
                    success: function(res) {
                        if (res) {
                            $("#township_id").empty();
                            $("#township_id").append(
                                '<option value="">Select Township</option>'
                            );
                            $.each(res, function(key, value) {
                                $("#township_id").append(
                                    '<option value="' + value.code + '">' + value.name +
                                    ' / ' + value.description + '</option>'
                                );
                            });
                        } else {
                            $("#township_id").empty();
                        }
                    },
                });
            } else {
                $("#township_id").empty();
                $("#district_id").empty();
            }
        });


        //getVillage
        $("#township_id").change(function() {

            $("#village_id").empty();
            $("#ward_id").empty();
            var township_id = $(this).val();
            if (township_id) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('getVillage') }}?township_id=" + township_id,
                    success: function(res) {
                        if (res) {
                            $("#village_id").empty();
                            // $("#village_id").append(
                            //     '<option value="" selected disabled>Select Town</option>'
                            // );
                            $.each(res, function(key, value) {
                                $("#village_id").append(
                                    '<option value="' + value.code + '">' + value.name +
                                    ' / ' + value.description + '</option>'
                                );
                            });
                        } else {
                            $("#village_id").empty();
                        }
                    },
                });
            } else {
                $("#village_id").empty();
                $("#township_id").empty();
            }
        });

        //getQuarter
        $("#village_id").change(function() {

            $("#ward_id").empty();
            var village_id = $(this).val();
            if (village_id) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('getQuarter') }}?village_id=" + village_id,
                    success: function(res) {
                        if (res) {
                            $("#ward_id").empty();
                            // $("#ward_id").append(
                            //     '<option value="" selected disabled>Select Ward</option>'
                            // );
                            $.each(res, function(key, value) {
                                $("#ward_id").append(
                                    '<option value="' + value.code + '">' + value.name +
                                    ' / ' + value.description + '</option>'
                                );
                            });
                        } else {
                            $("#ward_id").empty();
                        }
                    },
                });
            } else {
                $("#ward_id").empty();
                // $("#township_id").empty();
            }
        });
    </script>
    <script type="text/javascript">
        // LoanSchedule
        $(document).ready(function() {


            var input = document.getElementById("loan_amount");
            input.addEventListener("keyup", function() {
                calculate();
            });

            var input2 = document.getElementById("loan_term_value");
            input2.addEventListener("keyup", function() {
                calculate();
            });

            var input3 = document.getElementById("loan_term");
            input3.addEventListener("change", function() {
                calculate();
            });

            var input4 = document.getElementById("interest_rate");
            input4.addEventListener("change", function() {
                calculate();
            });

            function calculate() {
                var loan_amount = Number(document.getElementById("loan_amount").value);
                var loan_term_value = Number(document.getElementById("loan_term_value").value);
                var interest_rate = Number(document.getElementById("interest_rate").value);
                var h = document.getElementById("loan_term");
                var loan_term_dropdown = h.options[h.selectedIndex].text;

                var today = new Date();
                if (loan_amount) {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('getLoanSchedule') }}",
                        data: {
                            loan_amount: loan_amount,
                            loan_term_value: loan_term_value,
                            loan_term_dropdown: loan_term_dropdown,
                            interest_rate: interest_rate,
                            date: today
                        },
                        success: function(res) {

                            $("#myTableBody tr").empty();
                            var total_interest = 0;
                            var total_principal = 0;
                            var total_total = 0;
                            if (res) {
                                $.each(res, function(key, value) {

                                    key = key + 1;
                                    var html = '<tr>' +
                                        '<td>' + key + '</td>' +
                                        '<td>' + value.date + '</td>' +

                                        '<td>' + value.principal + '</td>' +
                                        '<td>' + value.interest + '</td>' +
                                        '<td>' + value.total + '</td>' +
                                        '<td>' + value.balance + '</td>' +
                                        '</tr>';
                                    total_principal += value.principal;
                                    total_interest += value.interest;
                                    total_total += value.total;

                                    $('#myTableBody tr').last().after(html);
                                });
                                var html = '<tr>' +
                                    '<td colspan="2">Total</td>' +

                                    '<td>' + total_principal + '</td>' +
                                    '<td>' + total_interest + '</td>' +
                                    '<td>' + total_total + '</td>' +
                                    '<td></td>' +
                                    '</tr>';
                                $('#myTableBody tr').last().after(html);
                            } else {
                                $("#myTableBody tr").empty();
                            }
                        },
                    });
                } else {

                }
            };


        });

        // Client
        $(document).ready(function() {

            $('#clientCard').hide();
            $('#guarantorACard').hide();
            $('#guarantorBCard').hide();
            $('#guarantorCCard').hide();

            $('#client_name').keyup(function() {
                var query = $(this).val();
                if (query != '') {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ url('getClientInfo') }}",
                        method: "POST",
                        data: {
                            query: query,
                            _token: _token,
                            branch_id: "{{ DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id }}"
                        },
                        success: function(data) {
                            $('#client_list').fadeIn();
                            $('#client_list').html(data);
                        }
                    });
                }
            });

            //    $('.clickMe').click(function () {
            $(document).on('click', '.cli_click', function() {


                var cli = $(this).data('val');
                document.getElementById("client_id").disabled = true;
                document.getElementById("client_name").addEventListener("search", function(event) {
                    document.getElementById("client_id").disabled = false;
                    document.getElementById('client_nrc').value = '';
                    document.getElementById('solo_client_name').value = '';
                    document.getElementById('client_phone').value = '';
                    document.getElementById('saving_amount').value = '';
                });

                var _token = $('input[name="_token"]').val();

                $('#client_name').val($(this).text());
                $('#client_id_hidden').val($(this).data('val'));
                $('#client_list').fadeOut();
                $.ajax({
                    url: "{{ url('getSingleClientInfo') }}",
                    method: "POST",
                    data: {
                        cli: cli,
                        _token: _token
                    },
                    success: function(data) {
                        $('#client_nrc').val(data.nrc);
                        $('#solo_client_name').val(data.name);
                        $('#client_phone').val(data.phone_primary);
                        $('#saving_amount').val(0);

                        $('#clientCard').show();
                        $('#clientID').text(data.main_client_code != null ? 'ID - ' + data
                            .main_client_code : 'ID - ' + data.client_uniquekey);
                        $('#clientName').text("Name - " + data.name);
                        $('#clientNrc').text("NRC - " + data.nrc);
                        $('#clientPhone').text("Phone - " + data.phone_primary);
                        $('#clientFather').text("Father's name - " + data.father_name);
                        $('#DOB').text("DOB - " + data.dob);

                        let path = "{{ asset('public/storage/clientphotos') }}";
                        $('#clientPhoto').attr('src', path + '/' + (data.client_photo != null ?
                            data.client_photo : 'default_user_icon.jpg'));

                        $('#clientEditID').text(data.main_client_code != null ? 'ID - ' + data
                            .main_client_code : 'ID - ' + data.client_uniquekey);
                        $('#clientEditName').text("Name - " + data.name);
                        $('#clientEditNrc').text("NRC - " + data.nrc);
                        $('#clientEditPhone').text("Phone - " + data.phone_primary);
                        $('#clientEditFather').text("Father's name - " + data.father_name);
                        $('#clientEditDOB').text("DOB - " + data.dob);

                        //                         let path = "{{ asset('public/storage/clientphotos') }}";
                        $('#clientEditPhoto').attr('src', path + '/' + (data.client_photo !=
                            null ? data.client_photo : 'default_user_icon.jpg'));

                        $('#client_nrc').val(data.nrc);
                        $('#solo_client_name').val(data.name);
                        $('#client_phone').val(data.phone_primary);
                        $('#saving_amount').val(0);
                        $('#client_photo_container').html(
                            '<h4 style="width: 200px;">Client - </h4><img src="{{ asset('public/storage/clientphotos') }}/' +
                            (data.client_photo != null ? data.client_photo :
                                'default_user_icon.jpg') +
                            '" style="height: 100px;" class="rounded ml-5" />');
                        $('#client_branch').val(data.branch_id);
                        $('#client_center').val(data.center_id);
                        // $('#client_group').val(data.group_id);
                        $('#loan_officer').empty();
                        $('#loan_officer').html(
                            `<option selected value="${data.staff_id}">${data.staff_name}</option>`
                        );
                        // $('#loan_officer').val(data.staff_id);
                        {{--                $('#loan_officer').val({{ DB::table('tbl_staff')->where('branch_id', data.branch_id)->get('id') }}) --}}

                        $.ajax({
                            type: "GET",
                            url: "{{ url('getGroupp') }}?center=" + data.center_id,
                            success: function(res) {
                                if (res) {
                                    // console.log(res);
                                    $("#client_group").empty();
                                    $("#client_group").append(
                                        '<option value="" selected disabled>Choose Group</option>'
                                    );
                                    $.each(res, function(key, value) {
                                        $("#client_group").append(
                                            '<option value="' + value
                                            .group_uniquekey + '">' + (
                                                value.main_group_code !=
                                                null ? value
                                                .main_group_code : value
                                                .group_uniquekey) +
                                            '</option>'
                                        );
                                    });
                                } else {
                                    $("#client_group").empty();
                                }
                            },
                        });

                    }
                });



                $.ajax({
                    url: "{{ url('getSingleClientAccInfo') }}",
                    method: "POST",
                    data: {
                        cli: cli,
                        _token: _token
                    },
                    success: function(data) {

                        $('#branch_id').val(data.branch_name);
                        $('#center_id').val(data.center_uniquekey);
                        $('#group_id').val(data.group_uniquekey);
                    }
                });


            });

            $('#client_id').change(function() {
                var e = document.getElementById("client_id");
                var client_dropdown = e.options[e.selectedIndex].value;
                // var client_dropdown = $('#client_id').val()

                if ($(this).find('option:selected').text() == "Choose Client")
                    document.getElementById("client_name").disabled = false;
                else
                    document.getElementById("client_name").disabled = true;

                if (client_dropdown != '') {

                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ url('getSingleClientInfo') }}",
                        method: "POST",
                        data: {
                            cli: client_dropdown,
                            _token: _token
                        },
                        success: function(data) {
                            //create
                            // console.log(data);
                            let nrc;
                            if (data.nrc == null && data.old_nrc == null) {
                                nrc = "Still Applying";
                            } else if (data.old_nrc == null) {
                                nrc = data.nrc;
                            } else {
                                nrc = data.old_nrc;
                            }

                            $('#clientCard').show();
                            $('#clientID').text(data.main_client_code != null ? 'ID - ' + data
                                .main_client_code : 'ID - ' + data.client_uniquekey);
                            $('#clientName').text("Name - " + data.name);
                            $('#clientNrc').text("NRC - " + nrc);
                            $('#clientPhone').text("Phone - " + data.phone_primary);
                            $('#clientFather').text("Father's name - " + data.father_name);
                            $('#DOB').text("DOB - " + data.dob);

                            let path = "{{ asset('public/storage/clientphotos') }}";
                            $('#clientPhoto').attr('src', path + '/' + (data.client_photo !=
                                null ? data.client_photo : 'default_user_icon.jpg'));

                            $('#clientEditID').text(data.main_client_code != null ? 'ID - ' +
                                data.main_client_code : 'ID - ' + data.client_uniquekey);
                            $('#clientEditName').text("Name - " + data.name);
                            $('#clientEditNrc').text("NRC - " + nrc);
                            $('#clientEditPhone').text("Phone - " + data.phone_primary);
                            $('#clientEditFather').text("Father's name - " + data.father_name);
                            $('#clientEditDOB').text("DOB - " + data.dob);


                            $('#clientEditPhoto').attr('src', path + '/' + (data.client_photo !=
                                null ? data.client_photo : 'default_user_icon.jpg'));

                            $('#client_nrc').val(data.nrc);
                            $('#solo_client_name').val(data.name);
                            $('#client_phone').val(data.phone_primary);
                            // $('#saving_amount').val(data.available_withdrawl);
                            $('#client_photo_container').html(
                                '<h4 style="width: 200px;">Client - </h4><img src="{{ asset('/storage/app/public/clientphotos/') }}/' +
                                (data.client_photo != null ? data.client_photo :
                                    'default_user_icon.jpg') +
                                '" style="height: 100px;" class="rounded ml-5" />');
                            $('#client_branch').val(data.branch_id);
                            $('#client_center').val(data.center_id);
                            // $('#client_group').val(data.group_id);
                            $('#loan_officer').empty();
                            $('#loan_officer').html(
                                `<option selected value="${data.staff_id}">${data.staff_name}</option>`
                            );
                            // $('#loan_officer').val(data.staff_id);
                            {{--                $('#loan_officer').val({{ DB::table('tbl_staff')->where('branch_id', data.branch_id)->get('id') }}) --}}
                            //                         console.log(data);

                            $.ajax({
                                type: "GET",
                                url: "{{ url('getGroupp') }}?center=" + data.center_id,
                                success: function(res) {
                                    if (res) {
                                        // console.log(res);
                                        $("#client_group").empty();
                                        $("#client_group").append(
                                            '<option value="" selected disabled>Choose Group</option>'
                                        );
                                        $.each(res, function(key, value) {
                                            $("#client_group").append(
                                                '<option value="' +
                                                value.group_uniquekey +
                                                '">' + (value
                                                    .main_group_code !=
                                                    null ? value
                                                    .main_group_code :
                                                    value
                                                    .group_uniquekey) +
                                                '</option>'
                                            );
                                        });
                                    } else {
                                        $("#client_group").empty();
                                    }
                                },
                            });
                        }
                    });



                    $.ajax({
                        url: "{{ url('getSavingAmount') }}",
                        method: "POST",
                        data: {
                            cli: client_dropdown,
                            _token: _token
                        },
                        success: function(data) {
                            $('#saving_amount').val(data);
                        }
                    });
                }
            });

        });

        $("#client_center").change(function() {
            var center = $(this).val();

            $.ajax({
                type: "GET",
                url: "{{ url('getGroupp') }}?center=" + center,
                success: function(res) {
                    if (res) {
                        console.log(res);
                        $("#client_group").empty();
                        $("#client_group").append(
                            '<option value="" selected disabled>Choose Group</option>'
                        );
                        $.each(res, function(key, value) {
                            $("#client_group").append(
                                '<option value="' + value.group_uniquekey + '">' + (value
                                    .main_group_code != null ? value.main_group_code : value
                                    .group_uniquekey) +
                                '</option>'
                            );
                        });
                    } else {
                        $("#client_group").empty();
                    }
                },
            });
        });


        //start loan create grarantor hide and show
        $(document).ready(function() {
            function hideGuarantors() {
                $('#guarantorA, #guarantorB, #guarantorC').hide();
                $('#guarantor_b #guarantor_b_hidden').prop('disabled', true);
                $('#guarantor_c #guarantor_c_hidden').prop('disabled', true);

                $('#guarantorADoc, #guarantorBDoc, #guarantorCDoc').hide();
            }

            hideGuarantors();

            $('#guarantor_select').change(function() {
                if (this.value == 3) {
                    hideGuarantors();
                    $('#guarantorA, #guarantorB, #guarantorC').show();
                    $('#guarantor_b #guarantor_b_hidden').prop('disabled', false);
                    $('#guarantor_c #guarantor_c_hidden').prop('disabled', false);
                    $('#guarantorADoc, #guarantorBDoc, #guarantorCDoc').show();
                } else if (this.value == 2) {
                    hideGuarantors();
                    $('#guarantorA, #guarantorB').show();
                    $('#guarantor_b #guarantor_b_hidden').prop('disabled', true);
                    $('#guarantorADoc, #guarantorBDoc').show();
                } else {
                    hideGuarantors();
                    $('#guarantorA').show();
                    $('#guarantorADoc').show();
                }
            });

        })
        //end loan create grarantor hide and show

        // GuarantorA
        $(document).ready(function() {

            $('#guarantor_name_a').keyup(function() {
                var query = $(this).val();
                if (query != '') {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ url('getGuarantorAInfo') }}",
                        method: "POST",
                        data: {
                            query: query,
                            _token: _token
                        },
                        success: function(data) {
                            $('#guarantor_list_a').fadeIn();
                            $('#guarantor_list_a').html(data);
                        }
                    });
                }
            });


            $(document).on('click', '.gua_click', function() {

                document.getElementById("guarantor_a").disabled = true;
                document.getElementById("guarantor_name_a").addEventListener("search", function(event) {
                    document.getElementById("guarantor_a").disabled = false;
                    document.getElementById('guarantor_a_nrc').value = '';
                    document.getElementById('guarantor_a_name').value = '';
                    document.getElementById('guarantor_a_id').value = '';
                });

                var guarantor_a = $(this).text();
                var _token = $('input[name="_token"]').val();

                $('#guarantor_name_a').val($(this).text());
                $('#guarantor_a_hidden').val($(this).data('val'));
                $('#guarantor_list_a').fadeOut();

                $.ajax({
                    url: "{{ url('getSingleGuarantorAInfo') }}",
                    method: "POST",
                    data: {
                        guarantor_a: guarantor_a,
                        _token: _token
                    },
                    success: function(data) {
                        $('#guarantor_a_nrc').val(data.nrc);
                        $('#guarantor_a_name').val(data.name);
                        $('#guarantor_a_id').val(data.guarantor_uniquekey);

                        $('#guarantorACard').show();
                        $('#guarantorAID').text("ID - " + data.guarantor_uniquekey);
                        $('#guarantorAName').text("Name - " + data.name);
                        $('#guarantorANrc').text("NRC - " + data.nrc);
                        $('#guarantorAPhone').text("Phone - " + data.phone_primary);
                        $('#guarantorADOB').text("DOB - " + data.dob);

                        let path = "{{ asset('public/storage/guarantor_photos') }}";
                        $('#guarantorAPhoto').attr('src', path + '/' + data.guarantor_photo);

                        $('#guarantorEditAID').text("ID - " + data.guarantor_uniquekey);
                        $('#guarantorEditAName').text("Name - " + data.name);
                        $('#guarantorEditANrc').text("NRC - " + data.nrc);
                        $('#guarantorEditAPhone').text("Phone - " + data.phone_primary);
                        $('#guarantorEditADOB').text("DOB - " + data.dob);

                        // let path = "{{ asset('public/storage/guarantor_photos') }}";
                        $('#guarantorEditAPhoto').attr('src', path + '/' + data
                            .guarantor_photo);

                        $('#guarantor_a_nrc').val(data.nrc);
                        $('#guarantor_a_name').val(data.name);
                        $('#guarantor_a_id').val(data.guarantor_uniquekey);
                        $('#guarantor_a_photo_container').html(
                            '<h4 style="width: 200px;">Guarantor A - </h4><img src="{{ asset('public/storage/guarantor_photos') }}/' +
                            data.guarantor_photo +
                            '" style="height: 100px;" class="rounded ml-5" />');
                    }
                });
            });

            $('#guarantor_a').change(function() {
                var e = document.getElementById("guarantor_a");
                var guarantor_a_dropdown = e.options[e.selectedIndex].value;

                if ($(this).find('option:selected').text() == "Choose Guarantor A")
                    document.getElementById("guarantor_name_a").disabled = false;
                else
                    document.getElementById("guarantor_name_a").disabled = true;

                if (guarantor_a_dropdown != '') {

                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ url('getSingleGuarantorAInfo') }}",
                        method: "POST",
                        data: {
                            guarantor_a: guarantor_a_dropdown,
                            _token: _token
                        },
                        success: function(data) {

                            let nrc;
                            if (data.nrc == null && data.old_nrc == null) {
                                nrc = "Still Applying";
                            } else if (data.old_nrc == null) {
                                nrc = data.nrc;
                            } else {
                                nrc = data.old_nrc;
                            }

                            $('#guarantorACard').show();
                            $('#guarantorAID').text("ID - " + data.guarantor_uniquekey);
                            $('#guarantorAName').text("Name - " + data.name);
                            $('#guarantorANrc').text("NRC - " + nrc);
                            $('#guarantorAPhone').text("Phone - " + data.phone_primary);
                            $('#guarantorADOB').text("DOB - " + data.dob);

                            let path = "{{ asset('public/storage/guarantor_photos') }}";
                            $('#guarantorAPhoto').attr('src', path + '/' + data
                                .guarantor_photo);

                            $('#guarantorEditAID').text("ID - " + data.guarantor_uniquekey);
                            $('#guarantorEditAName').text("Name - " + data.name);
                            $('#guarantorEditANrc').text("NRC - " + nrc);
                            $('#guarantorEditAPhone').text("Phone - " + data.phone_primary);
                            $('#guarantorEditADOB').text("DOB - " + data.dob);

                            // let path = "{{ asset('public/storage/guarantor_photos') }}";
                            $('#guarantorEditAPhoto').attr('src', path + '/' + data
                                .guarantor_photo);

                            $('#guarantor_a_nrc').val(data.nrc);
                            $('#guarantor_a_name').val(data.name);
                            $('#guarantor_a_id').val(data.guarantor_uniquekey);
                            $('#guarantor_a_photo_container').html(
                                '<h4 style="width: 200px;">Guarantor A - </h4><img src="{{ asset('public/storage/guarantor_photos') }}/' +
                                data.guarantor_photo +
                                '" style="height: 100px;" class="rounded ml-5" />');
                        }
                    });
                }
            });


        });

        // GuarantorB
        $(document).ready(function() {

            $('#guarantor_name_b').keyup(function() {
                var query = $(this).val();
                if (query != '') {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ url('getGuarantorBInfo') }}",
                        method: "POST",
                        data: {
                            query: query,
                            _token: _token
                        },
                        success: function(data) {
                            $('#guarantor_list_b').fadeIn();
                            $('#guarantor_list_b').html(data);
                        }
                    });
                }
            });


            $(document).on('click', '.gua_click2', function() {

                document.getElementById("guarantor_b").disabled = true;
                document.getElementById("guarantor_name_b").addEventListener("search", function(event) {
                    document.getElementById("guarantor_b").disabled = false;
                    document.getElementById('guarantor_b_nrc').value = '';
                    document.getElementById('guarantor_b_name').value = '';
                    document.getElementById('guarantor_b_id').value = '';
                });

                var guarantor_b = $(this).text();
                var _token = $('input[name="_token"]').val();

                $('#guarantor_name_b').val($(this).text());
                $('#guarantor_b_hidden').val($(this).data('val'));
                $('#guarantor_list_b').fadeOut();

                $.ajax({
                    url: "{{ url('getSingleGuarantorBInfo') }}",
                    method: "POST",
                    data: {
                        guarantor_b: guarantor_b,
                        _token: _token
                    },
                    success: function(data) {
                        $('#guarantor_b_nrc').val(data.nrc);
                        $('#guarantor_b_name').val(data.name);
                        $('#guarantor_b_id').val(data.guarantor_uniquekey);

                        $('#guarantorBCard').show();
                        $('#guarantorBID').text("ID - " + data.guarantor_uniquekey);
                        $('#guarantorBName').text("Name - " + data.name);
                        $('#guarantorBNrc').text("NRC - " + data.nrc);
                        $('#guarantorBPhone').text("Phone - " + data.phone_primary);
                        $('#guarantorBDOB').text("DOB - " + data.dob);

                        let path = "{{ asset('public/storage/guarantor_photos') }}";
                        $('#guarantorBPhoto').attr('src', path + '/' + data.guarantor_photo);

                        $('#guarantorEditBID').text("ID - " + data.guarantor_uniquekey);
                        $('#guarantorEditBName').text("Name - " + data.name);
                        $('#guarantorEditBNrc').text("NRC - " + data.nrc);
                        $('#guarantorEditBPhone').text("Phone - " + data.phone_primary);
                        $('#guarantorEditBDOB').text("DOB - " + data.dob);

                        // let path = "{{ asset('public/storage/guarantor_photos') }}";
                        $('#guarantorEditBPhoto').attr('src', path + '/' + data
                            .guarantor_photo);

                        $('#guarantor_b_nrc').val(data.nrc);
                        $('#guarantor_b_name').val(data.name);
                        $('#guarantor_b_id').val(data.guarantor_uniquekey);
                        $('#guarantor_b_photo_container').html(
                            '<h4 style="width: 200px;">Guarantor B - </h4><img src="{{ asset('public/storage/guarantor_photos') }}/' +
                            data.guarantor_photo +
                            '" style="height: 100px;" class="rounded ml-5" />');
                    }
                });
            });

            $('#guarantor_b').change(function() {
                var e = document.getElementById("guarantor_b");
                var guarantor_b_dropdown = e.options[e.selectedIndex].value;

                if ($(this).find('option:selected').text() == "Choose Guarantor B")
                    document.getElementById("guarantor_name_b").disabled = false;
                else
                    document.getElementById("guarantor_name_b").disabled = true;

                if (guarantor_b_dropdown != '') {

                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ url('getSingleGuarantorBInfo') }}",
                        method: "POST",
                        data: {
                            guarantor_b: guarantor_b_dropdown,
                            _token: _token
                        },
                        success: function(data) {

                            let nrc;
                            if (data.nrc == null && data.old_nrc == null) {
                                nrc = "Still Applying";
                            } else if (data.old_nrc == null) {
                                nrc = data.nrc;
                            } else {
                                nrc = data.old_nrc;
                            }

                            $('#guarantorBCard').show();
                            $('#guarantorBID').text("ID - " + data.guarantor_uniquekey);
                            $('#guarantorBName').text("Name - " + data.name);
                            $('#guarantorBNrc').text("NRC - " + nrc);
                            $('#guarantorBPhone').text("Phone - " + data.phone_primary);
                            $('#guarantorBDOB').text("DOB - " + data.dob);

                            let path = "{{ asset('public/storage/guarantor_photos') }}";
                            $('#guarantorBPhoto').attr('src', path + '/' + data
                                .guarantor_photo);

                            $('#guarantorEditBID').text("ID - " + data.guarantor_uniquekey);
                            $('#guarantorEditBName').text("Name - " + data.name);
                            $('#guarantorEditBNrc').text("NRC - " + nrc);
                            $('#guarantorEditBPhone').text("Phone - " + data.phone_primary);
                            $('#guarantorEditBDOB').text("DOB - " + data.dob);

                            // let path = "{{ asset('public/storage/guarantor_photos') }}";
                            $('#guarantorEditBPhoto').attr('src', path + '/' + data
                                .guarantor_photo);

                            $('#guarantor_b_nrc').val(data.nrc);
                            $('#guarantor_b_name').val(data.name);
                            $('#guarantor_b_id').val(data.guarantor_uniquekey);
                            $('#guarantor_b_photo_container').html(
                                '<h4 style="width: 200px;">Guarantor B - </h4><img src="{{ asset('public/storage/guarantor_photos') }}/' +
                                data.guarantor_photo +
                                '" style="height: 100px;" class="rounded ml-5" />');
                        }
                    });
                }
            });

        });

        // GuarantorC
        $(document).ready(function() {

            $('#guarantor_name_c').keyup(function() {
                var query = $(this).val();
                if (query != '') {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ url('getGuarantorCInfo') }}",
                        method: "POST",
                        data: {
                            query: query,
                            _token: _token
                        },
                        success: function(data) {
                            $('#guarantor_list_c').fadeIn();
                            $('#guarantor_list_c').html(data);
                        }
                    });
                }
            });


            $(document).on('click', '.gua_click3', function() {

                document.getElementById("guarantor_c").disabled = true;
                document.getElementById("guarantor_name_c").addEventListener("search", function(event) {
                    document.getElementById("guarantor_c").disabled = false;
                    document.getElementById('guarantor_c_nrc').value = '';
                    document.getElementById('guarantor_c_name').value = '';
                    document.getElementById('guarantor_c_id').value = '';
                });

                var guarantor_c = $(this).text();
                var _token = $('input[name="_token"]').val();

                $('#guarantor_name_c').val(guarantor_c);
                $('#guarantor_c_hidden').val($(this).data('val'));
                $('#guarantor_list_c').fadeOut();

                $.ajax({
                    url: "{{ url('getSingleGuarantorCInfo') }}",
                    method: "POST",
                    data: {
                        guarantor_c: guarantor_c,
                        _token: _token
                    },
                    success: function(data) {
                        $('#guarantor_c_nrc').val(data.nrc);
                        $('#guarantor_c_name').val(data.name);
                        $('#guarantor_c_id').val(data.guarantor_uniquekey);


                        $('#guarantorCCard').show();
                        $('#guarantorCID').text("ID - " + data.guarantor_uniquekey);
                        $('#guarantorCName').text("Name - " + data.name);
                        $('#guarantorCNrc').text("NRC - " + data.nrc);
                        $('#guarantorCPhone').text("Phone - " + data.phone_primary);
                        $('#guarantorCDOB').text("DOB - " + data.dob);

                        let path = "{{ asset('public/storage/guarantor_photos') }}";
                        $('#guarantorCPhoto').attr('src', path + '/' + data.guarantor_photo);

                        $('#guarantorEditCID').text("ID - " + data.guarantor_uniquekey);
                        $('#guarantorEditCName').text("Name - " + data.name);
                        $('#guarantorEditCNrc').text("NRC - " + data.nrc);
                        $('#guarantorEditCPhone').text("Phone - " + data.phone_primary);
                        $('#guarantorEditCDOB').text("DOB - " + data.dob);

                        // let path = "{{ asset('public/storage/guarantor_photos') }}";
                        $('#guarantorEditCPhoto').attr('src', path + '/' + data
                            .guarantor_photo);

                        $('#guarantor_c_nrc').val(data.nrc);
                        $('#guarantor_c_name').val(data.name);
                        $('#guarantor_c_id').val(data.guarantor_uniquekey);
                    }
                });
            });

            $('#guarantor_c').change(function() {
                var e = document.getElementById("guarantor_c");
                var guarantor_c_dropdown = e.options[e.selectedIndex].value;

                if ($(this).find('option:selected').text() == "Choose Guarantor C")
                    document.getElementById("guarantor_name_c").disabled = false;
                else
                    document.getElementById("guarantor_name_c").disabled = true;

                if (guarantor_c_dropdown != '') {

                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ url('getSingleGuarantorCInfo') }}",
                        method: "POST",
                        data: {
                            guarantor_c: guarantor_c_dropdown,
                            _token: _token
                        },
                        success: function(data) {

                            let nrc;
                            if (data.nrc == null && data.old_nrc == null) {
                                nrc = "Still Applying";
                            } else if (data.old_nrc == null) {
                                nrc = data.nrc;
                            } else {
                                nrc = data.old_nrc;
                            }

                            $('#guarantorCCard').show();
                            $('#guarantorCID').text("ID - " + data.guarantor_uniquekey);
                            $('#guarantorCName').text("Name - " + data.name);
                            $('#guarantorCNrc').text("NRC - " + nrc);
                            $('#guarantorCPhone').text("Phone - " + data.phone_primary);
                            $('#guarantorCDOB').text("DOB - " + data.dob);

                            let path = "{{ asset('public/storage/guarantor_photos') }}";
                            $('#guarantorCPhoto').attr('src', path + '/' + data
                                .guarantor_photo);

                            $('#guarantorEditCID').text("ID - " + data.guarantor_uniquekey);
                            $('#guarantorEditCName').text("Name - " + data.name);
                            $('#guarantorEditCNrc').text("NRC - " + nrc);
                            $('#guarantorEditCPhone').text("Phone - " + data.phone_primary);
                            $('#guarantorEditCDOB').text("DOB - " + data.dob);

                            // let path = "{{ asset('public/storage/guarantor_photos') }}";
                            $('#guarantorEditCPhoto').attr('src', path + '/' + data
                                .guarantor_photo);

                            $('#guarantor_c_nrc').val(data.nrc);
                            $('#guarantor_c_name').val(data.name);
                            $('#guarantor_c_id').val(data.guarantor_uniquekey);
                            $('#guarantor_c_photo_container').html(
                                '<h4 style="width: 200px;">Guarantor C - </h4><img src="{{ asset('public/storage/guarantor_photos') }}/' +
                                data.guarantor_photo +
                                '" style="height: 100px;" class="rounded ml-5" />');
                        }
                    });
                }
            });

        });
        //GuarantorCreate
        //has nrc
        $(function() {
            $('#nrc_type1').hide();
            $('#nrc_type2').hide();

            $('#has_nrc').change(function() {

                var nrc_status = $(this).val();
                if (nrc_status == "Yes") {
                    $('#newNRC').hide();
                    $('#oldNRC').hide();
                    $('#GuarantorstillNRC').hide();
                    $('#GuarantorlossNRC').hide();
                    $('#nrc_card_id').prop('disabled', false);
                    $('#lossnrc').removeAttr('required');
                    $('#stillappnrc').removeAttr('required');
                    $('#guarantor_nrc_front_photo').show();
                    $('#guarantor_nrc_back_photo').show();

                } else if (nrc_status == 'No') {
                    $('#guarantor_nrc_front_photo').hide();
                    $('#guarantor_nrc_back_photo').hide();
                    $('#gnfp').removeAttr('required');
                    $('#gnbp').removeAttr('required');
                    $('#nrc_type1').hide();
                    $('#nrc_type2').show();
                    $('#newNRC').hide();
                    $('#oldNRC').hide();
                    $('#GuarantorstillNRC').hide();
                    $('#GuarantorlossNRC').hide();
                    $('#nrc_card_id').prop('disabled', true);

                } else {
                    $('#nrc_type1').hide();
                    $('#nrc_type2').hide();
                    $('#newNRC').hide();
                    $('#oldNRC').hide();
                    $('#GuarantorstillNRC').hide();
                    $('#GuarantorlossNRC').hide();
                    $('#nrc_card_id').prop('disabled', false);
                    $('#lossnrc').removeAttr('required');
                    $('#stillappnrc').removeAttr('required');
                }
            });
        });


        //nrc type (old,new or no)
        $(function() {
            $('#newNRC').hide();
            $('#oldNRC').hide();
            $('#GuarantorstillNRC').hide();
            $('#GuarantorlossNRC').hide();

            $('#nrc_type_select1').change(function() {
                var nrc_type_status = $(this).val();
                if (nrc_type_status == "Old NRC") {
                    $('#oldNRC').show();
                    $('#newNRC').hide();
                    $('#GuarantorstillNRC').hide();
                    $('#GuarantorlossNRC').hide();

                } else if (nrc_type_status == "New NRC") {
                    $('#newNRC').show();
                    $('#oldNRC').hide();
                    $('#GuarantorstillNRC').hide();
                    $('#GuarantorlossNRC').hide();

                } else {
                    $('#newNRC').hide();
                    $('#oldNRC').hide();
                    $('#GuarantorstillNRC').hide();
                    $('#GuarantorlossNRC').hide();
                }


            });
            $('#nrc_type_select2').change(function() {
                var nrc_type_status = $(this).val();
                if (nrc_type_status == "Still Applying") {
                    $('#GuarantorlossNRC').hide();
                    $('#GuarantorstillNRC').show();
                    $('#newNRC').hide();
                    $('#oldNRC').hide();
                    $('#lossnrc').removeAttr('required');
                    $('#stillappnrc').prop('disabled', false);
                    $('#lossnrc').prop('disabled', true);


                } else if (nrc_type_status == "Loss NRC") {
                    $('#GuarantorlossNRC').show();
                    $('#newNRC').hide();
                    $('#oldNRC').hide();
                    $('#GuarantorstillNRC').hide();
                    $('#stillappnrc').removeAttr('required');
                    $('#lossnrc').prop('disabled', false);
                    $('#stillappnrc').prop('disabled', true);

                } else {
                    $('#newNRC').hide();
                    $('#oldNRC').hide();
                    $('#GuarantorstillNRC').hide();
                    $('#GuarantorlossNRC').hide();
                    $('#lossnrc').prop('disabled', true);
                    $('#stillappnrc').prop('disabled', true);
                }
            });
        });

        // LoanType
        $(document).ready(function() {
            $("#chargeAndCompulsory").hide();

            $('#loan_type').change(function() {
                $("#chargeAndCompulsory").show();
                var loan_type_id = $(this).val();

                // console.log(loan_type_id);

                $.ajax({
                    type: "GET",
                    url: "{{ url('getChargesCompulsory') }}?loan_type_id=" + loan_type_id,
                    success: function(res) {
                        console.log(res);
                        let compulsory = res[1];
                        $("#compulsoryTbody").empty();
                        for (let i = 0; i <= compulsory.length - 1; i++) {
                            $("#compulsoryTbody").append(
                                `
                                <tr>
                                    <td>
                                        <input type="text" readonly class="form-control" value="${compulsory[i].compulsory_name}">
                                        <input type="hidden" name="loan_compulsory${i+1}_id" value="${compulsory[i].loan_compulsory_id}" >
                                        <input type="hidden" name="loan_compulsory_count" value="${compulsory.length}">
                                    </td>
                                    <td>
                                        <input type="text" readonly class="form-control" value="${compulsory[i].saving_amount}">
                                    </td>
                                    <td>
                                        <select id="" class="form-control" readonly>
                                            <option ${compulsory[i].charge_option == 1 ? 'selected' : ''}>Fixed amount</option>
                                            <option ${compulsory[i].charge_option == 2 ? 'selected' : ''}>Of Loan amount</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="number" value="${compulsory[i].interest_rate}" class="form-control" readonly>
                                    </td>
                                    <td>
                                        <select name="" id="" class="form-control" readonly>
                                            <option ${compulsory[i].charge_type == 1 ? 'selected' : ''}>Deposit Before Disbursement</option>
                                            <option ${compulsory[i].charge_type == 2 ? 'selected' : ''}>Deduct from loan disbursement</option>
                                            <option ${compulsory[i].charge_type == 3 ? 'selected' : ''}>Every Repayments</option>
                                        </select>
                                    </td>
                                    <td>
                                        <select id="" class="form-control" readonly>
                                            <option value="active" ${compulsory[i].status == 'active' ? 'selected': ''}>Yes</option>
                                            <option value="unactive" ${compulsory[i].status == 'unactive' ? 'selected': ''}>No</option>
                                        </select>
                                    </td>
                                </tr>
                            `
                            );
                        }

                        let charge = res[0];
                        // if(res[0].length=0)

                        $("#chargeTbody").empty();
                        for (let i = 0; i <= charge.length - 1; i++) {
                            $("#chargeTbody").append(
                                `
                            <tr>
                                <td>
                                    <input type="text" readonly class="form-control" value="${charge[i].charge_name}">
                                    <input type="hidden" name="loan_charge${i+1}_id" value="${charge[i].loan_charge_id}" >
                                    <input type="hidden" name="loan_charge_count" value="${charge.length}">
                                </td>
                                <td>
                                    <input type="text" readonly class="form-control" value="${charge[i].amount}">
                                </td>
                                <td>
                                    <select name="" id="" class="form-control" readonly>
                                        <option value="1" ${charge[i].charge_option == 1 ? 'selected' : ''}>Fixed amount</option>
                                        <option value="2" ${charge[i].charge_option == 2 ? 'selected' : ''}>Of Loan amount</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="" id="" class="form-control" readonly>
                                        <option value="1" ${charge[i].charge_type == 1 ? 'selected' : ''}>Deposit Before Disbursement</option>
                                        <option value="1" ${charge[i].charge_type == 2 ? 'selected' : ''}>Deduct from loan disbursement</option>
                                        <option value="1" ${charge[i].charge_type == 3 ? 'selected' : ''}>Every Repayments</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="" id="" class="form-control" readonly>
                                        <option value="active" ${charge[i].status == 'active' ? 'selected' : ''}>Yes</option>
                                        <option value="unactive" ${charge[i].status == 'unactive' ? 'selected' : ''}>No</option>
                                    </select>
                                </td>
                            </tr>
                        `
                            );
                        }



                    }
                })

                $.ajax({
                    url: "{{ url('getLoanTypeInfo') }}?loan_type_id=" + loan_type_id,
                    method: "GET",
                    success: function(data) {
                        // if (data.id == 2){
                        $('#loan_term_value').val(data.loan_term_value);
                        // }
                        $('#loan_term').val(data.loan_term);
                        $('#loan_term option').each(function() {
                            if ($(this).val() != data.loan_term) {
                                $(this).attr('disabled', true);
                            } else {
                                $(this).attr('disabled', false);
                            }
                        });
                        $('#loan_amount').val(data.description);
                        $('#principal_formula').val(data.principal_formula);
                        $('#interest_rate').val(data.interest_rate);
                        $('#disbursement_status').val("Pending");
                        $('#disbursement_status option').each(function() {
                            if ($(this).val() != "Pending") {
                                $(this).attr('disabled', true);
                            } else {
                                $(this).attr('disabled', false);
                            }
                        });
                        $('#interest_method').val(data.interest_method);
                        $('#interest_rate_period').val(data.interest_rate_period);
                        $('#interest_rate_period option').each(function() {
                            if ($(this).val() != data.interest_rate_period) {
                                $(this).attr('disabled', true);
                            } else {
                                $(this).attr('disabled', false);
                            }
                        });
                        var current_datetime = new Date();
                        var formatted_date = current_datetime.getFullYear() + "-" + (
                                current_datetime.getMonth() + 1) + "-" + current_datetime
                            .getDate();
                        // var formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth()+1) +"-" +(current_datetime.getDate()) +" " +(current_datetime.getHours()) +":" +(current_datetime.getMinutes()) +":" +(current_datetime.getSeconds());
                        // var formatted_date = current_datetime.toLocalDateString();
                        // console.log(formatted_date);
                        {{-- $('#loan_application_date').val(formatted_date); --}}
                        {{-- $('#first_installment_date').val(formatted_date); --}}

                        // tesstart


                        $.ajax({
                            type: "GET",
                            url: "{{ url('getajaxLoanSchedule') }}",
                            data: {
                                loan_type_id: loan_type_id,
                                loan_amount: data.description,
                                loan_term_value: $("#loan_term_value").val(),
                                loan_term_dropdown: data.loan_term,
                                interest_rate: data.interest_rate,
                                date: formatted_date
                            },
                            success: function(res) {
                                $("#myTableBody tr").empty();
                                var total_interest = 0;
                                var total_principal = 0;
                                var total_total = 0;
                                if (res) {
                                    $.each(res.loan_schedule, function(key, value) {

                                        key = key + 1;
                                        var html = '<tr>' +
                                            '<td>' + value.repay_count +
                                            '</td>' +
                                            '<td>' + value.date + '</td>' +
                                            '<td>' + value.repay_principal
                                            .toLocaleString('en-US') +
                                            '</td>' +
                                            '<td>' + value.repay_interest
                                            .toLocaleString('en-US') +
                                            '</td>' +
                                            '<td>' + (value
                                                .repay_principal + value
                                                .repay_interest)
                                            .toLocaleString('en-US') +
                                            '</td>' +
                                            '<td>' + value.next_balance
                                            .toLocaleString('en-US') +
                                            '</td>' +
                                            '</tr>';

                                        $('#myTableBody tr').last().after(
                                            html);


                                        total_principal += value
                                            .repay_principal;
                                        total_interest += value
                                            .repay_interest;
                                        total_total = total_principal +
                                            total_interest;
                                    });
                                    var html = '<tr>' +
                                        '<td colspan="2">Total</td>' +

                                        '<td>' + total_principal.toLocaleString(
                                            'en-US') + '</td>' +
                                        '<td>' + total_interest.toLocaleString(
                                            'en-US') + '</td>' +
                                        '<td>' + total_total.toLocaleString(
                                            'en-US') + '</td>' +
                                        '<td></td>' +
                                        '</tr>';
                                    $('#myTableBody tr').last().after(html);
                                } else {
                                    $("#myTableBody tr").empty();
                                }
                            },
                        });
                        //    testend
                    }
                });
                // var addr = $(this).find('option:selected').data('address');
                // $('#group_uniquekey').val(addr+'-G-');
            });

            //loan amount start
            $("#loan_amount").change(function() {
                let loan_amount = $(this).val();
                var current_datetime = new Date();
                var formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() +
                    1) + "-" + current_datetime.getDate();
                var loan_term_value = $("#loan_term_value").val();

                $.ajax({
                    type: "GET",
                    url: "{{ url('getajaxLoanSchedule') }}",
                    data: {
                        loan_type_id: $("#loan_type").val(),
                        loan_amount: loan_amount,
                        loan_term_value: loan_term_value,
                        loan_term_dropdown: $("#loan_term").val(),
                        interest_rate: $("#interest_rate").val(),
                        date: formatted_date
                    },
                    success: function(res) {
                        $("#myTableBody tr").empty();
                        var total_interest = 0;
                        var total_principal = 0;
                        var total_total = 0;
                        if (res) {
                            // console.log(res.loan_schedule);
                            $.each(res.loan_schedule, function(key, value) {

                                key = key + 1;
                                var html = '<tr>' +
                                    '<td>' + value.repay_count + '</td>' +
                                    '<td>' + value.date + '</td>' +
                                    '<td>' + value.repay_principal + '</td>' +
                                    '<td>' + value.repay_interest + '</td>' +
                                    '<td>' + (value.repay_principal + value
                                        .repay_interest) + '</td>' +
                                    '<td>' + value.next_balance + '</td>' +
                                    '</tr>';
                                total_principal += value.repay_principal;
                                total_interest += value.repay_interest;
                                total_total += (value.repay_principal + value
                                    .repay_interest);

                                $('#myTableBody tr').last().after(html);
                            });
                            var html = '<tr>' +
                                '<td colspan="2">Total</td>' +

                                '<td>' + total_principal + '</td>' +
                                '<td>' + total_interest + '</td>' +
                                '<td>' + total_total + '</td>' +
                                '<td></td>' +
                                '</tr>';
                            $('#myTableBody tr').last().after(html);
                        } else {
                            $("#myTableBody tr").empty();
                        }
                    },
                });
            })
            // loan amount end

            //first installment date start
            $("#first_installment_date").change(function() {
                let disbursement_date = $(this).val();
                var current_datetime = new Date();
                // console .log(formatted_date);
                var loan_term_value = $("#loan_term_value").val();

                $.ajax({
                    type: "GET",
                    url: "{{ url('getajaxLoanSchedule') }}",
                    data: {
                        loan_type_id: $("#loan_type").val(),
                        loan_amount: $('#loan_amount').val(),
                        loan_term_value: loan_term_value,
                        loan_term_dropdown: $("#loan_term").val(),
                        interest_rate: $("#interest_rate").val(),
                        date: disbursement_date
                    },
                    success: function(res) {
                        $("#myTableBody tr").empty();
                        var total_interest = 0;
                        var total_principal = 0;
                        var total_total = 0;
                        if (res) {
                            // console.log(res.loan_schedule);
                            $.each(res.loan_schedule, function(key, value) {

                                key = key + 1;
                                var html = '<tr>' +
                                    '<td>' + value.repay_count + '</td>' +
                                    '<td>' + value.date + '</td>' +
                                    '<td>' + value.repay_principal + '</td>' +
                                    '<td>' + value.repay_interest + '</td>' +
                                    '<td>' + (value.repay_principal + value
                                        .repay_interest) + '</td>' +
                                    '<td>' + value.next_balance + '</td>' +
                                    '</tr>';
                                total_principal += value.repay_principal;
                                total_interest += value.repay_interest;
                                total_total += (value.repay_principal + value
                                    .repay_interest);

                                $('#myTableBody tr').last().after(html);
                            });
                            var html = '<tr>' +
                                '<td colspan="2">Total</td>' +

                                '<td>' + total_principal + '</td>' +
                                '<td>' + total_interest + '</td>' +
                                '<td>' + total_total + '</td>' +
                                '<td></td>' +
                                '</tr>';
                            $('#myTableBody tr').last().after(html);
                        } else {
                            $("#myTableBody tr").empty();
                        }
                    },
                });
            })
            // disbursement end
        });

        //has branch for announcements
        $('#has_branch').change(function() {
            var branch_name = $('#has_branch').val();
            if (branch_name == "MKT CO. LTD.,") {
                document.getElementById("branches").disabled = false;
            } else {
                document.getElementById("branches").disabled = true;
            }
        });

        //getBusinessCategories
        $("#business_type_id").change(function() {
            var business_type_id = $(this).val();
            if (business_type_id) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('getBusinessCategory') }}?business_type_id=" + business_type_id,
                    success: function(res) {
                        if (res) {
                            $("#business_category_id").empty();
                            $("#business_category_id").append(
                                '<option value="">Select Business Category</option>'
                            );
                            $.each(res, function(key, value) {

                                $("#business_category_id").append(
                                    '<option value="' + key + '">' + value + '</option>'
                                );
                            });
                        } else {
                            $("#business_category_id").empty();
                        }
                    },
                });
            } else {
                $("#business_type_id").empty();
                $("#business_category_id").empty();
            }
        });

        //removable & unremovable assets

        $(document).ready(function() {
            function hideAssets() {
                $('#removable_assets, #unremovable_assets').hide();
            }

            //for create assets jquery
            $('#assets_type').change(function() {
                if (this.value == "removable") {
                    hideAssets();
                    $('#removable_assets').show();

                } else if (this.value == "unremovable" || "#assets_type".selected == "unremovable") {
                    hideAssets();
                    $('#unremovable_assets').show();

                } else {
                    hideAssets();
                }
            });
            //for edit assets jquery
            var selected_val = $('#assets_type :selected').val();
            if (selected_val == "removable") {
                hideAssets();
                $('#removable_assets').show();

            } else if (selected_val == "unremovable") {
                hideAssets();
                $('#unremovable_assets').show();

            } else {
                hideAssets();
            }

        })

        //loan deposit
        $(document).ready(function() {
            var current_datetime = new Date();
            var formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" +
                current_datetime.getDate();
            $("#loan_deposit_date").val(formatted_date);
            $("#loan_disbursement_date").val(formatted_date);

        })

        //loan repayment
        $(document).ready(function() {
            $(".get__loan__schedule").each(function(index, value) {

                $(this).click(function() {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('getLoanSchedule') }}?loan_id=" + $(this).data(
                            'loan'),
                        success: function(res) {
                            var nrc = "";
                            if (res[0].nrc == null && res[0].old_nrc == null) {
                                nrc = 'N/A';
                            } else if (res[0].old_nrc == null) {
                                nrc = res[0].nrc;
                            } else {
                                nrc = res[0].old_nrc;
                            }
                            $("#scheduleHeading").text("Repayment Process for " + res[0]
                                .loan_unique_id)
                            $("#scheduleName").text(res[0].name);
                            $("#scheduleNrc").text(nrc);
                            $("#scheduleOfficerName").text("Officer - " + res[0]
                                .loan_officer_name);
                            $("#scheduleLoanAmount").text("Pricipal - " + res[0]
                                .loan_amount);
                            $("#scheduleBranch").text("Branch - " + res[0].branch_name);

                            let totalInterest = 0;
                            for (let i = 0; i < res[1].length; i++) {
                                totalInterest += parseInt(res[1][i].interest);
                            }

                            let pricipalOutstanding = 0;
                            for (let i = 0; i < res[1].length; i++) {
                                if (res[1][i].status != "paid") {
                                    pricipalOutstanding += parseInt(res[1][i].capital);
                                }
                            }

                            let interestOutstanding = 0;
                            for (let i = 0; i < res[1].length; i++) {
                                if (res[1][i].status != "paid") {
                                    interestOutstanding += parseInt(res[1][i].interest);
                                }
                            }

                            // console.log(interestOutstanding);
                            $("#scheduleInterest").text("Interest - " + totalInterest);
                            $("#pricipalOutstanding").text("Outstanding (P) - " +
                                pricipalOutstanding);
                            $("#interestOutstanding").text("Outstanding (I) - " +
                                interestOutstanding);
                            $("#totalOutstanding").text("Outstanding (T) - " + (
                                pricipalOutstanding + interestOutstanding));

                            let data = res[1];
                            // console.log(data);
                            let text = "";
                            // let pricipal = "";
                            // let paidAmount = 0;

                            //reset table
                            $("#scheduleTbody").html("");

                            for (let i = 0; i <= data.length - 1; i++) {
                                pricipal = data[i].capital;
                                let date = new Date(data[i].month);
                                let formattedDate = date.getDate() + "-" + (date
                                    .getMonth() + 1) + "-" + date.getFullYear();
                                let paymentColor = '';
                                let paymentType = '';
                                if (data[i].repayment_type == 'late') {
                                    paymentColor = 'badge badge-warning';
                                    paymentType = 'late';
                                } else if (data[i].repayment_type == 'due') {
                                    paymentColor = 'badge badge-success';
                                    paymentType = 'due';
                                } else if (data[i].repayment_type == 'pre') {
                                    paymentColor = 'badge badge-info';
                                    paymentType = 'pre';
                                } else {
                                    paymentColor = 'badge badge-secondary';
                                    paymentType = 'not yet';
                                }

                                let paymentDate = new Date(data[i].repayment_date);
                                paymentDate = paymentDate.getDate() + "-" + (paymentDate
                                        .getMonth() + 1) + "-" + paymentDate
                                    .getFullYear();
                                // $.ajax({
                                //     type: "GET",
                                //     url: "{{ url('get_repayment_date') }}",
                                //     data: {
                                //         schedule_id: data[i].schedule_id
                                //     },
                                //     success: function(res) {
                                //         let d = new Date(res);
                                //         console.log(res.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate());
                                //         paymentDate = res.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate();
                                //     }
                                // });

                                // if (data[i].due_date != null) {
                                //     paymentDate = new Date(data[i].due_date);
                                //     paymentDate = paymentDate.getDate() + "-" + (
                                //             paymentDate.getMonth() + 1) + "-" +
                                //         paymentDate.getFullYear();
                                // } else if (data[i].late_date != null) {
                                //     paymentDate = new Date(data[i].late_date);
                                //     paymentDate = paymentDate.getDate() + "-" + (
                                //             paymentDate.getMonth() + 1) + "-" +
                                //         paymentDate.getFullYear();
                                // } else if (data[i].pre_date != null) {
                                //     paymentDate = new Date(data[i].late_date);
                                //     paymentDate = paymentDate.getDate() + "-" + (
                                //             paymentDate.getMonth() + 1) + "-" +
                                //         paymentDate.getFullYear();
                                // } else {
                                //     paymentDate = 'Unpaid';
                                // }

                                // console.log(data);
                                // paidAmount += parseInt(pricipal);
                                text = `
                                <tr>
                                    <td>
<!--                                        <div class="row m-1">-->
<!--                                            <div class="mb-2">-->
                                                <input id="repayment_check_${i + 1}" name="repayment_r${i + 1}" type="${data[i].status == 'paid'? 'hidden' : 'checkbox'}" value="checked" class="" ${data[i].status == 'paid'? 'disabled' : ''}>
                                                <i class="fas fa-check text-success ${data[i].status == 'paid' ? '' : 'd-none'}"></i>
                                                <input id="repayment_check_${i + 1}"  name="repayment_r${i + 1}_value" value="${data[i].schedule_id}" hidden>
                                                <input type="hidden" name="loan_unique_id" value="${data[i].loan_unique_id}">
                                                <input type="hidden" name="count" value="${res[2]}">
<!--                                            </div>-->
<!--                                        </div>-->

                                    </td>
                                    <td>${i + 1}</td>
                                    <td class='text-nowrap'>${formattedDate}</td>
                                    <td>${Math.round(pricipal).toLocaleString('en-US')}</td>
                                    <td>${Math.round(data[i].interest).toLocaleString('en-US')}</td>
                                    <td>${Math.round(parseInt(data[i].interest == null ? 0 : data[i].interest) + parseInt(data[i].capital)).toLocaleString('en-US')}</td>
                                    <td>${Math.round(data[i].final_amount).toLocaleString('en-US')}</td>
                            		<td>
                            			<span class="${paymentColor}">${paymentType}</span>
                            		</td>
                                    <td>
                                        ${data[i].repayment_date != null ? paymentDate : "Unpaid"}
                                    </td>
                                </tr>
                            `;
                                $("#scheduleTbody").append(text);

                                if (i != 0) {
                                    $(`#repayment_check_${i + 1}`).attr('disabled',
                                        true);

                                    // if($(`#repayment_check_${i}`).attr('checked', true)) {
                                    //     $(`#repayment_check_${i+1}`).attr('disabled', false);
                                    // }
                                }

                                $(`#repayment_check_${i + 1}`).click(function() {
                                    if ($(this).prop('checked') === true) {
                                        if (data[i].repayment_type == 'late') {

                                        } else {
                                            $(`#repayment_check_${i + 2}`).attr(
                                                'disabled', false);
                                        }

                                    } else if ($(this).prop('checked') ===
                                        false) {
                                        for (let j = i; j <= data.length; j++) {
                                            $(`#repayment_check_${j + 2}`).attr(
                                                'disabled', true);
                                        }
                                    };
                                })
                            }

                            for (let i = 0; i <= data.length - 1; i++) {
                                if (data[i].status == "paid") {
                                    $(`#repayment_check_${i + 2}`).attr('disabled',
                                        false);
                                }
                            }

                        }
                    });
                })

            });
        });

        //loan repayment jquery

        if ($('#repayment_type').val() == 'late') {
            $('#principal').attr("readonly", false);
            $('#interest').attr("readonly", false);
            $('#payment').attr("readonly", false);
            // $('#repayment_type').attr("readonly", true);
        }

        $('#repayment_type').change(function() {
            if (this.value == "late") {
                $('#principal').attr("readonly", false);
                $('#interest').attr("readonly", false);
                $('#payment').attr("readonly", false);

            } else if (this.value == "pre") {
                $('#interest').attr("readonly", false);
                $('#principal').attr("readonly", true);
                $('#payment').attr("readonly", true);

            } else {
                $('#interest').attr("readonly", true);
                $('#principal').attr("readonly", true);
                $('#payment').attr("readonly", true);
            }
        });

        //pre repayment input calculation
        $(document).ready(function() {
            var principal = $('#principal').val();
            var total_amount_topay = $('#total_balance').val();
            var interest = $('#interest').val();


            $('#interest').on('keyup', function(e) {
                var pay_total = Number($('#principal').val()) + Number($('#interest').val());
                // var payment_amount=Number(interest) + Number($(this).val());
                $("#payment").val(Math.round(pay_total));

                // var outstanding_calc=Number(pay_total) - Number(payment_amount);
                var owed_amount = Number($('#total_balance').val()) - Number(pay_total);
                $("#outstanding").val(Math.round(owed_amount));

            });

            $('#principal').on('keyup', function(e) {
                var pay_total = Number($('#principal').val()) + Number($('#interest').val());
                // var payment_amount=Number(interest) + Number($(this).val());
                $("#payment").val(Math.round(pay_total));

                // var outstanding_calc=Number(pay_total) - Number(payment_amount);
                var owed_amount = Number($('#total_balance').val()) - Number(pay_total);
                $("#outstanding").val(Math.round(owed_amount));
            });

            $('#payment').on('keyup', function(e) {
                $("#outstanding").val(Number(total_amount_topay) - Number($(this).val()));

                // if (!principal < $("#payment").val() < total_amount_topay) {
                //     $("#payment").val(Math.round(total_amount_topay));
                // }
            });


            $("#printThis").click(function() {
                $(this).preventDefault;
                PrintDiv();
            });

            $("#deposit").submit(function() {
                PrintDiv();
            })

            function PrintDiv() {
                var divToPrint = document.getElementById('printDeposit');
                var popupWin = window.open('', '_blank', 'width=1366,height=768');
                popupWin.document.open();
                popupWin.document.write('<html><body onload="window.print();window.close();">' + divToPrint
                    .innerHTML + '</html>');
                popupWin.document.close();
            }

            $("#printDisBtn").click(function() {
                $(this).preventDefault;
                PrintDis();
            });


            $("#disbursementForm").submit(function() {
                PrintDis();
            })

            function PrintDis() {
                var divToPrint = document.getElementById('printDisbursement');
                var popupWin = window.open('', '_blank', 'width=1366,height=768');
                popupWin.document.open();
                popupWin.document.write('<html><body onload="window.print();window.close();">' + divToPrint
                    .innerHTML + '</html>');
                popupWin.document.close();
            }

            $("#columnSelect").click(function() {
                var openPopup = $(this).attr('aria-expanded');
                $('#columnPopup #column').toggleClass('show');
                // if(openPopup) {
                //     $('#columnPopup #column').classList.add("show");
                // }
                // $('#columnPopup #column').classList.remove("show");
            });

            $("#exportSelect").click(function() {
                var openPopup = $(this).attr('aria-expanded');
                $('#exportPopup #export').toggleClass('show');
                // if(openPopup) {
                //     $('#columnPopup #column').classList.add("show");
                // }
                // $('#columnPopup #column').classList.remove("show");
            });

            $(document).click(function(e) {
                $("#columnPopup #column").removeClass('show');
                $("#exportPopup #export").removeClass('show');
            });

            $(".c-report-check, .l-report-check").attr("checked", false).click(function() {
                var column = "." + $(this).attr("name");
                $(column).toggle();
            });

            $("#clientReportBranch").change(function() {
                $("#clientReportBranchForm").submit();
            });

            $("#clientReportType").change(function() {
                $("#clientReportTypeForm").submit();
            });

            $("#loanReportStatus").change(function() {
                $("#loanReportStatusForm").submit();
            });

            // html2pdf
            $("#clientReportExcelBtn").click(function() {
                // html2pdf($("#clientReportPdf"));

                $("#clientReport").table2excel({
                    exclude: ".excludeThisClass",
                    name: "Worksheet Name",
                    filename: "Customer Info.xls", // do include extension
                    preserveColors: false // set to true if you want background colors and font colors preserved
                });
            });

            $("#loanReportExcelBtn").click(function() {
                // html2pdf($("#loanReportPdf"));

                $("#loanReport").table2excel({
                    exclude: ".excludeThisClass",
                    name: "Worksheet Name",
                    filename: "Loan Info.xls", // do include extension
                    preserveColors: false // set to true if you want background colors and font colors preserved
                });
            });

            //pfpopup
            $(".pf-btn").click(function() {
                $(".pf-popup").toggleClass("show");
                // $(".pf-popup").hasClass("show") ? $(this).attr("aria-expanded").val(true) : $(this).attr("aria-expanded").val(false);
            })

            var nowDate = new Date();
            var formattedNowDate = nowDate.getDate() + "/" + (nowDate.getMonth() + 1) + "/" + nowDate.getFullYear();
            $(".date").text(formattedNowDate);

        });
        //loan approve
        $("#approve_select").click(function() {
            $("#approveForm").submit();
        });

        $("#approve_d").change(function() {
            $("#approve_date").val($(this).val());
        });

        //client_length
        $("#clientLength").change(function() {
            $('#clientLengthForm').submit();
        });

        //loan_length
        $("#loanLength").change(function() {
            $('#loanLengthForm').submit();
        });

        //client_report_length
        $("#clientReportLength").change(function() {
            $('#clientReportLengthForm').submit();
        });
        //loan_report_length
        $("#loanReportLength").change(function() {
            $('#loanReportLengthForm').submit();
        });

        //     $('#saving_unique_id').change(function () {
        //             // var e = document.getElementById("saving_unique_id");
        //             // var saving_dropdown = e.options[e.selectedIndex].val;
        //         var saving_dropdown=$('#saving_unique_id').val();

        //         if (saving_dropdown != '') {

        //             $.ajax({
        //                 url: "{{ url('getSavingWithdrawlInfo') }}",
        //                 method: "POST",
        //                 data: {id: saving_dropdown,_token: "{{ csrf_token() }}"},
        //                 success: function (data) {

        //                     $('#loan_id').val(data[0].loan_unique_id);
        //                     $('#client_name').val(data[0].client_name);
        //                     $('#c_saving').val(data[0].compulsory_saving);
        //                     $('#cha_saving').val(data[0].charges_saving);
        //                     $('#withdrew').val(data[0].total_withdrew);
        //                     $('#available_amount').val(data[0].available_withdrawl);

        //                 }
        //             });
        //         }



        //     });

        $('#main_loan_id').change(function() {
            var loan_id = $('#main_loan_id').val();
            $('#principle_saving').val('0');
            $('#interest_saving').val('0');
            $('#total_saving').val('0');
            $('#total_withdrew').val('0');
            $('#principle_balance').val('0');
            $('#interest_balance').val('0');
            $('#total_balance').val('0');
            $.ajax({
                url: "{{ url('getSavingClientInfo') }}",
                method: "POST",
                data: {
                    loan_id: loan_id,
                    _token: "{{ csrf_token() }}"
                },
                success: function(data) {
                    console.log(data);
                    $('#client_name').val(data.name);
                    $('#client_nrc').val(data.nrc);

                }
            })

            $.ajax({
                url: "{{ url('getSavingWithdrawlInfo') }}",
                method: "POST",
                data: {
                    loan_id: loan_id,
                    _token: "{{ csrf_token() }}"
                },
                success: function(data) {

                    console.log(data);
                    $('#principle_saving').val(data.principle);
                    $('#interest_saving').val(data.interest);
                    $('#total_saving').val(data.total_saving);

                    // $('#principle_withdrawal').val(data.principle_withdrawl);
                    // $('#interest_withdrawal').val(data.interest_withdrawl);
                    // $('#total_withdrew').val(data.total_withdrawl);

                    $('#total_withdrew').val(data.total_withdrew);

                    $('#principle_balance').val(data.principle_balance);
                    $('#interest_balance').val(data.interest_balance);
                    $('#total_balance').val(data.total_balance);
                    // console.log(data.last_tran);
                    // $('#last_tran_date').text(data.last_tran);



                }
            })
            //              $.ajax({
            //                     url: "{{ url('getSavingWithdrawlInterest') }}",
            //                     method: "POST",
            //                     data: {
            //                     loan_id: loan_id,
            //                     _token: "{{ csrf_token() }}"
            //                 },
            //                     success: function(data) {
            //                     	console.log(data);

            //                         $('#interest_saving').val(data);                   	

            //                     }
            //                 });


        });

        // $('#principle_saving').change(function(){
        // 	var total_saving = Number($('#principle_saving').val()) + Number($('#interest_saving').val());
        // 	$('#total_saving').val(total_saving);
        // });

        $("#saving_client_name").keyup(function() {
            $('#saving_client_list').fadeIn();
            var query = $(this).val();
            if (query != '') {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url: "{{ url('getSavingClientInfo') }}",
                    method: "POST",
                    data: {
                        query: query,
                        _token: _token
                    },
                    success: function(data) {

                        $('#saving_client_list').html(data);
                    }
                });
            }
        });
        $('#withdrawl_type').change(function() {
            if (this.value == "Normal") {
                var total_saving_amount = $('#total_saving_amount').val();
                var total_interest = $('#total_interest').val();
                var total_available = parseInt(total_saving_amount) + parseInt(total_interest);
                $('#withdrawl_amount').val(total_available);
                $('#withdrawl_amount').attr("readonly", true);

            } else {
                $('#withdrawl_amount').val('');
                $('#withdrawl_amount').attr("readonly", false);
            }
        });



        $(document).on('click', '.saving_cli_click', function() {

            var cli = $(this).data('val');
            // $('#loan_id').val('');
            $('#saving_client_name').val('');
            // $('#c_saving').val('');
            // $('#cha_saving').val('');
            $('#withdrew').val('');
            $('#available_amount').val('');

            var _token = $('input[name="_token"]').val();

            $('#saving_client_name').val($(this).text());
            // $('#client_id_hidden').val($(this).data('val'));
            $('#saving_client_list').fadeOut();

            $.ajax({
                url: "{{ url('getSavingWithdrawlInfo') }}",
                method: "POST",
                data: {
                    id: cli,
                    _token: "{{ csrf_token() }}"
                },
                success: function(data) {

                    $('#loan_id').val(data[0].loan_unique_id);
                    $('#client_name').val(data[0].client_name);
                    $('#c_saving').val(data[0].compulsory_saving);
                    $('#cha_saving').val(data[0].charges_saving);
                    $('#withdrew').val(data[0].total_withdrew);
                    $('#available_amount').val(data[0].available_withdrawl);
                    $('#saving_unique_id').val(data[0].saving_unique_id);

                }
            });


        });

        //getFrdCode
        $("#acc_id").change(function() {
            var acc_id = $(this).val();
            if (acc_id) {
                $.ajax({
                    type: "GET",
                    url: "{{ url('getFrdCode') }}?acc_id=" + acc_id,
                    success: function(res) {
                        if (res) {
                            $("#frd_coa_code_id").empty();
                            $("#frd_coa_code_id").append(
                                '<option value="">Choose FRD COA Code</option>'
                            );
                            $.each(res, function(key, value) {
                                $("#frd_coa_code_id").append(
                                    '<option value="' + key + '">' + value + '</option>'
                                );
                            });
                        } else {
                            $("#frd_coa_code_id").empty();
                        }
                    },
                });
            } else {
                $("#acc_id").empty();
                $("#frd_coa_code_id").empty();
            }
        });

        // guarantor_create_autogenerate_start
        $(document).ready(function() {

            $.ajax({
                type: "GET",
                url: "{{ url('guarantor/uniqueid') }}",
                success: function(res) {
                    $('#guarantor_uniquekey').val(res);
                },
            });
        });
        // guarantor_create_autogenerate_end

        //Account Report
        $(document).ready(function() {
            $("#getButton").click(function() {
                $("#accReportStatusForm").submit();
            });
        });



        //Disbursement Form
        $(document).ready(function() {
            $('#button-search').click(function() {
                var center = $('#center_disbursement_select').val();
                var loan_product = $('#loan_product_disbursement_select').val();
                var group = $('#group_disbursement_select').val();
                $('#disbursementsearchForm').submit();

            });
        });

        //select all disburse
        $('#selectAllDisburse').change(function() {

            if ($('#selectAllDisburse').prop('checked') == true) {
                $("[id=selectDisburseCheck]").prop('checked', true);
            } else {
                $("[id=selectDisburseCheck]").prop('checked', false);
            }
        });

        $("#disburse_selected").click(function() {
            $("#disburseForm").submit();
        });



        $("[id=selectDisburseCheck]").change(function() {
            //   $('.DINNER').text(active);
            var selected_ids = this.value;

        });

        //select all deposit
        $('#selectAllDeposit').change(function() {

            if ($('#selectAllDeposit').prop('checked') == true) {
                $('.selectDepositCheck').prop('checked', true);
            } else {
                $('.selectDepositCheck').prop('checked', false);
            }
        });

        $("#deposit_all").click(function() {
            $("#depositForm").submit();
        });

        //select all approve
        $('#approveAll').change(function() {

            if ($('#approveAll').prop('checked') == true) {
                $('.approveCheck').prop('checked', true);
            } else {
                $('.approveCheck').prop('checked', false);
            }
        });

        //ClientCreate
        $(document).ready(function() {
            //         $(function () {
            //         $('#has_nrc').change(function () {
            //             var nrc_status = $(this).val();
            //             if (nrc_status == "No") {
            //                 $('#guarantor_nrc_front_photo').hide();
            //                 $('#guarantor_nrc_back_photo').hide();
            //                 $('#gnfp').removeAttr('required');
            //                 $('#gnbp').removeAttr('required');
            //                 $('#nrc_type_select2').change(function(){
            //                     var nrc_type=$(this).val();
            //                     if(nrc_type == "Still Applying"){
            //                         $('#stillNRC').show();
            //                         $('#lossnrc').removeAttr('required');
            //                         $('#stillappnrc').prop('disabled',false);
            //                         $('#lossnrc').prop('disabled',true);


            //                     }else
            //                     {
            //                         $('#lossNRC').show();
            //                         $('#stillappnrc').removeAttr('required');
            //                         $('#lossnrc').prop('disabled',false);
            //                         $('#stillappnrc').prop('disabled',true);

            //                     }
            //                 })

            //             }else{
            //                 $('#guarantor_nrc_front_photo').show();
            //                 $('#guarantor_nrc_back_photo').show();
            //                 $('#lossnrc').removeAttr('required');
            //                 $('#stillappnrc').removeAttr('required');
            //             }                       
            //         });
            $('#client_type_center').change(function() {
                var center = $('#client_type_center').val();

                $.ajax({
                    type: "GET",
                    url: "{{ url('getGroupp') }}?center=" + center,
                    success: function(res) {
                        if (res) {
                            console.log(res);
                            $("#client_type_group").empty();
                            $("#client_type_group").append(
                                '<option value="" selected disabled>Choose Group</option>'
                            );
                            $.each(res, function(key, value) {
                                $("#client_type_group").append(
                                    '<option value="' + value.portal_group_id +
                                    '">' + (value.main_group_code != null ? value
                                        .main_group_code : value.group_uniquekey) +
                                    '</option>'
                                );
                            });
                        } else {
                            $("#client_type_group").empty();
                        }
                    },
                });
                $.ajax({
                    type: "GET",
                    url: "{{ url('getLO') }}?center=" + center,
                    success: function(res) {
                        if (res) {
                            console.log(res);
                            $("#loan_officer").empty();
                            $("#loan_officer").append(
                                '<option value="" selected disabled>Choose Loan Officer</option>'
                            );
                            $("#loan_officer").append(
                                '<option value="' + res.staff_code + '">' + res.name +
                                '</option>'
                            );
                        } else {
                            $("#loan_officer").empty();
                        }
                    },
                });
            })
        });

        //clientcrawl
        $('#clientCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling data...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/clients') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);
                    $('#clientCrawlCount').text(res.data);
                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Client crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })

                }
            })
        });

        //staff
        $('#staffCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling data...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/staffs') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    $("#staffCrawlCount").text(res.data);
                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Staff crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //center
        $('#centerCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling centers...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/centers') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);
                    $("#centerCrawlCount").text(res.data);
                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Center crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //group
        $('#groupCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling groups...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/groups') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    $("#groupCrawlCount").text(res.data);
                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Group crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //guarantor
        $('#guarantorCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling guarantors...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/guarantors') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    $('#guarantorCrawlCount').text(res.data);
                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Guarantor crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //loan
        $('#loanCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling loans...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/loans') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Loan crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //deposit
        $('#depositCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling deposits...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/deposits') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Deposit crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //disbursement
        $('#disbursementCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling disbursments...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/disbursements') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Disbursement crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //repayment
        $('#repaymentCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling repayments...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/repayments') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Repayment crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //savings
        $('#saving_withdrawlCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling savings...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/savings') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Saving crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

         //add holidays
         $('#crawl_holidayCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling holidays...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/holidays') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Holiday crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });


        //Reverse
        //staff reverse
        $('#rev_staffCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling data...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/staffs/reverse') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    $("#rev_staffCrawlCount").text(res.data);
                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Staff crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //center reverse
        $('#rev_centerCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling data...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/centers/reverse') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    $("#rev_centerCrawlCount").text(res.data);
                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Center crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //group reverse
        $('#rev_groupCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling data...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/groups/reverse') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    $("#rev_groupCrawlCount").text(res.data);
                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Group crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //guarantor reverse
        $('#rev_guarantorCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling data...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/guarantors/reverse') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    $("#rev_guarantorCrawlCount").text(res.data);
                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Guarantor crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //client reverse
        $('#rev_clientCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling data...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/clients/reverse') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);
                    $("#rev_lientCrawlCount").text(res.data);
                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Client crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);
                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //loan reverse
        $('#rev_loanCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling data...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/loans/reverse') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);
                    $("#rev_loanCrawlCount").text(res.data);
                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Loan crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);
                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //loan deposit
        $('#rev_depositCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling data...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/deposit/reverse') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    $("#rev_depositCrawlCount").text(res.data);
                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Deposit crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //loan disbursement
        $('#rev_disbursementCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling data...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/disbursements/reverse') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    $("#rev_disbursementCrawlCount").text(res.data);
                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Disbursement crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //loan repayment
        $('#rev_repaymentCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling data...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/repayments/reverse') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    $("#rev_repaymentCrawlCount").text(res.data);
                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Repayment crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //Saving reverse
        $('#rev_saving_withdrawlCrawl').click(function(e) {
            e.preventDefault();
            Swal.fire({
                icon: 'info',
                title: 'Crawling data...',
                html: 'Please wait...',
                allowEscapeKey: false,
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading()
                }
            });
            $.ajax({
                method: 'GET',
                url: "{{ url('api/savings/reverse') }}",
                headers: {
                    'secretekey': "{{ env('KEY_SECER') }}"
                },
                data: {
                    staff_code: "{{ session()->get('staff_code') }}"
                },
                success: function(res) {
                    console.log(res);

                    $("#rev_savingCrawlCount").text(res.data);
                    Swal.close();
                    Swal.fire({
                        icon: 'success',
                        title: 'Saving crawling success',
                        html: 'count ' + res.data
                    })
                },
                error: function(err) {
                    console.log(err);

                    Swal.close();
                    Swal.fire({
                        icon: 'error',
                        title: 'Encounter error',
                        html: err.responseJSON.message
                    })
                }
            })
        });

        //client create
        $('#clientSubmit').click(function(e) {
            e.preventDefault();

            let message = showRequiredError();

            if (message != '') {
                Swal.fire({
                    icon: 'error',
                    html: message,
                });
            } else {
                $('#clientCreateForm').submit();
            }

        });

        //loan create
        $('#loanSubmit').click(function(e) {
            e.preventDefault();

            let message = showRequiredError();

            if (message != '') {
                Swal.fire({
                    icon: 'error',
                    html: message,
                });
            } else {
                $('#loanCreateForm').submit();
            }

        });

        //create require field error
        function showRequiredError() {
            let message = '';

            $('input, select, textarea').filter('[required]').each(function(index, element) {
                if (element.value == '') {
                    let inputName = element.getAttribute('name');
                    message += inputName + ' field required<br/>';
                }
            });

            return message;
        }

        $('#level_status').val('leader');

        //change branch
        $('#changeBranch').change(function() {
            $('#changeBranchForm').submit();
            // console.log($(this).val());
        });

        //overday count
        $('#payment_date').change(function() {
            // console.log($(this).val());

            var date1 = new Date($('#schedule_date').val());
            var date2 = new Date($(this).val());
            // console.log(date2);
            var timeDiff = date2.getTime() - date1.getTime();
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            // alert(diffDays);
            $('#over_days').val(diffDays);

            if ($('#over_days').val() < 0) {
                $('#over_days').val('0');
            }
        });

        //loan_approval_modal_form
        $(document).ready(function() {
            $(".get__loan__approval__info").each(function(index, value) {

                $(this).click(function() {
                    $.ajax({
                        type: "GET",
                        url: "{{ url('getLoanInfo') }}?loan_id=" + $(this).data(
                            'loan'),
                        success: function(res) {
                            console.log(res);
                            // console.log((res.first_installment_date).getDate());
                            var firstdate = res.first_installment_date;
                            var date = new Date(firstdate);
                            // console.log(date);
                            var fdate = date.getFullYear() +
                                "-" + (((date.getMonth() + 1) < 10 ? "0" + (date
                                    .getMonth() + 1) : (date.getMonth() + 1))) +
                                "-" + (date.getDate() < 10 ? "0" + date.getDate() : date
                                    .getDate());

                            // console.log(fdate);
                            $('#cid').val(res.client_id);
                            $('#lid').val(res.loan_unique_id);
                            $('#loanappdate').val(res.loan_application_date);
                            $('#reqamount').val(res.loan_amount);
                            $('#fdate').val(fdate);
                            $('#approveloanamount').val(res.loan_amount);
                            // $('#cid').val(res.client_id);


                        }
                    });
                });
            })
        });

        //roles and permission select
        $(".role-check").each(function(index, ele) {
            $(this).click(function() {

                $.ajax({
                    type: "POST",
                    url: "{{ url('role_has_permissions') }}",
                    data: {
                        role: $(this).val(),
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(res) {
                        // console.log(res);
                        var data = res.data;
                        $(".permission-check").each(function() {
                            $(this).prop('checked', false);
                            $(this).prop('disabled', true);
                            for (var i = 0; i < data.length; i++) {
                                if ($(this).val() == data[i].permission_id) {
                                    $(this).prop('checked', true);
                                }
                            }
                        })
                    }
                })
            })
        });

        $("#uncheckRoles").change(function() {
            if($(this).prop('checked', true)) {
                $('.role-check').prop('checked', false);
                $('.permission-check').prop('checked', false);
                $('.permission-check').prop('disabled', false);
            }
        })
    </script>

    @yield('script')

</body>

</html>
