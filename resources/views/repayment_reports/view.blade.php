@extends('layouts.app')
@section('content')

    <div class="col-md-12">
        <div class="mb-3"></div>
        <div class="main-card mb-3 card">
            <div class="card-header">
                Repayment Reports
                <div class="btn-actions-pane-right">
                    <div role="group" class="btn-group-sm btn-group">
                        <a href="{{url('loan_reports/')}}">
                            <button type="button"  title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color">
                                <i class="pe-7s-back btn-icon-wrapper text-light">Back To List</i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
            @if(session('successMsg')!=NULL)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('successMsg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="table-responsive pl-3 pr-3 my-2">
                <table class="align-middle mb-0 table  table-striped table-hover" id="loanReport">
                    <thead>
                    <tr class="d-none">
                        <th colspan="11" class="text-center">Loan Info</th>
                    </tr>
                    <tr style="height: 40px;line-height:40px;">
                        <th class="text-center" scope="col"> No</th>
                        <th scope="col"> Loan ID</th>
                        <th>Repayment Date</th>
                        <th> Principal</th>
                        <th> Interest</th>
                        <th> Balance</th>
                        <th>Repayment Type</th>
                        <th> Status</th>
                    </tr>
                    </thead>
                    <tbody>
                        @php $i = 1; @endphp
                        @foreach ($repayment as $repay)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$repay->loan_unique_id}}</td>
                                <td>{{$repay->month}}</td>
                                <td>{{$repay->capital}}</td>
                                <td>{{$repay->interest}}</td>
                                <td>{{$repay->final_amount}}</td>
                                <td class="text-capitalize">{{$repay->repayment_type}}</td>
                                <td class="text-capitalize">{{$repay->status}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-block text-center card-footer">
                
            </div>
        </div>
    </div>
    </div>

@endsection


