@extends('layouts.app')
@section('content')

    <div class="col-md-12">
        <div class="mb-3"></div>
        <div class="main-card mb-3 card">
            <div class="card-header">

                Loan Information
                <div class="btn-actions-pane-right">
                    <div role="group" class="btn-group-sm btn-group">
                        <a href="{{url('loan/create')}}">
                            <button type="button" data-toggle="tooltip" title="" data-placement="bottom"
                                    class="btn-shadow mr-1 btn theme-color text-white" data-original-title="Example Tooltip">
                                <i class="fa fa-plus"></i>
                                Add
                            </button>
                        </a>
                        <a href="{{url('loan_reports?date=week')}}">
                            <button type="button" data-toggle="tooltip" title="" data-placement="bottom"
                                    class="btn-shadow mr-1 btn theme-color text-white" data-original-title="This Week">
                                <i class="fa fa-calendar"></i>
                                This Week
                            </button>
                        </a>
                        <a href="{{url('loan_reports?date=month')}}">
                            <button type="button" data-toggle="tooltip" title="" data-placement="bottom"
                                    class="btn-shadow mr-1 btn theme-color text-white" data-original-title="This Month">
                                <i class="fa fa-calendar"></i>
                                This Month
                            </button>
                        </a>
                        <a href="{{url('loan_reports?date=year')}}">
                            <button type="button" data-toggle="tooltip" title="" data-placement="bottom"
                                    class="btn-shadow mr-1 btn theme-color text-white" data-original-title="This Week">
                                <i class="fa fa-calendar"></i>
                                This Year
                            </button>
                        </a>
                        <a href="{{url('loan_reports?date=date')}}">
                            <button type="button" data-toggle="tooltip" title="" data-placement="bottom"
                                    class="btn-shadow mr-1 btn theme-color text-white" data-original-title="Today">
                                <i class="fa fa-calendar"></i>
                                Today
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            <div class="widget-content p-2">
                <div class="widget-content-wrapper">
                    <div class="widget-content-left mr-3 ml-3">
                        <label class="m-0"> Show </label>
                    </div>
                    <div class="widget-content-left mr-1">
                        <div class="widget-heading">
                            <form method="GET" id="loanReportLengthForm">
                                <select name="loan_report_length" id="loanReportLength" aria-controls="example"
                                        class="custom-select custom-select-sm form-control form-control-sm">
                                    <option value="10"  @if(request()->loan_report_length == 10) selected @endif>10</option>
                                    <option value="25"  @if(request()->loan_report_length == 25) selected @endif>25</option>
                                    <option value="50"  @if(request()->loan_report_length == 50) selected @endif>50</option>
                                    <option value="100"  @if(request()->loan_report_length == 100) selected @endif>100</option>
                                </select>
                            </form>
                        </div>
                    </div>
                    {{-- <div class="widget-content-left ">
                        <label class="m-p">  </label>
                        <div class="btn-group">
                            <select class="custom-select custom-select-sm form-control form-control-sm text-center text-black" name="branchlist">
                                @foreach($data_branch as $branchlist)
                                  <option class="text-left text-black" value="{{ $branchlist->id }}">{{ $branchlist->branch_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> --}}

                    <div class="widget-content-left ">
                        <label class="ml-2"> </label>
                        <div class="btn-group">
                            <form action="{{ route('loan_reports') }}" id="loanReportStatusForm" method="GET">
                                <select
                                    class="custom-select custom-select-sm form-control form-control-sm text-center text-black"
                                    name="disbursement_status" id="loanReportStatus">
                                    <option value="" selected disabled>Disbursement Status</option>
                                    <option
                                        value="none" {{ request()->disbursement_status == 'none' ? 'selected' : '' }}>
                                        None
                                    </option>
                                    <option
                                        value="Pending" {{ request()->disbursement_status == 'Pending' ? 'selected' : '' }}>
                                        Pending
                                    </option>
                                    <option
                                        value="Approved" {{ request()->disbursement_status == 'Approved' ? 'selected' : '' }}>
                                        Approved
                                    </option>
                                    <option
                                        value="Declined" {{ request()->disbursement_status == 'Declined' ? 'selected' : '' }}>
                                        Declined
                                    </option>
                                    <option
                                        value="Withdrawn" {{ request()->disbursement_status == 'Withdrawn' ? 'selected' : '' }}>
                                        Withdrawn
                                    </option>
                                    <option
                                        value="Written-Off" {{ request()->disbursement_status == 'Written-Off' ? 'selected' : '' }}>
                                        Written-Off
                                    </option>
                                    <option
                                        value="Closed" {{ request()->disbursement_status == 'Closed' ? 'selected' : '' }}>
                                        Closed
                                    </option>
                                    <option
                                        value="Activated" {{ request()->disbursement_status == 'Activated' ? 'selected' : '' }}>
                                        Activated
                                    </option>
                                    <option
                                        value="Canceled" {{ request()->disbursement_status == 'Canceled' ? 'selected' : '' }}>
                                        Canceled
                                    </option>
                                </select>
                            </form>
                        </div>
                    </div>

                    <div class="widget-content-right">
                        {{-- export --}}
                        <div class="dropdown d-inline-block ml-auto" id="exportPopup">
                            <button type="button" id="exportSelect" aria-haspopup="true" aria-expanded="false"
                                    data-toggle="dropdown" class="mr-2 dropdown-toggle btn btn-dark">
                                <i class="fas fa-file-export"></i>
                                Export
                            </button>
                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu" id="export" style="">
                                <div class="text-left pl-2">
                                    <p class="btn btn-outline-dark w-100 mb-0" id="loanReportPdfBtn">Export Pdf</p>
                                    <p class="btn btn-outline-dark w-100 mb-0" id="loanReportExcelBtn">Export Excel</p>
                                </div>
                            </div>
                        </div>
                        {{-- export --}}
{{--                        <button class="btn btn-dark" id="loanReportPdfBtn">Export Pdf</button>--}}
{{--                        <button class="btn btn-dark" id="loanReportExcelBtn">Export Excel</button>--}}
                        {{-- column --}}
                        <div class="dropdown d-inline-block ml-auto" id="columnPopup">
                            <button type="button" id="columnSelect" aria-haspopup="true" aria-expanded="false"
                                    data-toggle="dropdown" class="mr-2 dropdown-toggle btn btn-success">
                                <i class="fas fa-eye-slash"></i>
                                Column visibility
                            </button>
                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu" id="column" style="">
                                <div class="text-left pl-2">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input l-report-check" name="id" id="loanreportid">
                                        <label class="custom-control-label" for="loanreportid">Loan ID</label>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input l-report-check" name="name" id="loanreportname">
                                        <label class="custom-control-label" for="loanreportname">Name</label>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input l-report-check" name="nrc_no"
                                               id="loanreportnrc">
                                        <label class="custom-control-label" for="loanreportnrc">NRC Number</label>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input l-report-check" name="cen"
                                               id="loanreportcenter">
                                        <label class="custom-control-label" for="loanreportcenter">Center</label>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input l-report-check" name="type"
                                               id="loanreportloantype">
                                        <label class="custom-control-label" for="loanreportloantype">Loan Type</label>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input l-report-check" name="amount"
                                               id="loanreportamount">
                                        <label class="custom-control-label" for="loanreportamount">Loan Amount</label>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input l-report-check" name="branch"
                                               id="loanreportbranch">
                                        <label class="custom-control-label" for="loanreportbranch">Branch</label>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input l-report-check" name="dis_status"
                                               id="loanreportstatus">
                                        <label class="custom-control-label" for="loanreportstatus">Disbursement
                                            Status</label>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input l-report-check" name="dis_date"
                                               id="loanreportdate">
                                        <label class="custom-control-label" for="loanreportdate">Disbursement Date</label>
                                    </div>
                                </div>
                            </div>
                            {{-- column --}}
                        <div class="btn-group">
                            <!-- <label class="m-2"> Search </label> -->
                            <form method="GET">
                                <div class="input-group">
                                    <input type="text" name="search_loanslist" id="search_loanslist"
                                           value="{{ request()->get('search_loanslist') }}"
                                           class="form-control mr-1"
                                           placeholder="Search..."
                                           aria-label="Search"
                                           aria-describedby="button-addon2"
                                    >

                                    <button class="btn theme-color text-white mr-1" type="submit" id="button-addon2">
                                        <i class="fa fa-search"></i>
                                    </button>

                                    <a href="{{url('loan_reports')}}" class="btn theme-color text-white">
                                        <i class="fa fa-refresh"></i>
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            @if(session('successMsg')!=NULL)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('successMsg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="table-responsive pl-3 pr-3 my-2">
{{--                <h3 class="d-none text-center">Loan Info</h3>--}}
                <table class="align-middle mb-0 table  table-striped table-hover" id="loanReport">
                    <thead>
                    <tr class="d-none">
                        <th colspan="11" class="text-center">Loan Info</th>
                    </tr>
                    <tr style="height: 40px;line-height:40px;">
                        <th class="text-center" scope="col"> No</th>
                        <th class="sorting id" scope="col"> Loan ID</th>
                        <th class="name"> Name</th>
                        <th class="nrc_no"> NRC Number</th>
                        <th class="cen"> Center</th>
                        <th class="type"> Loan Type</th>
                        <th class="amount"> Amount</th>
                        <th class="branch"> Branch</th>
                        <th class="dis_status"> Status</th>
                        <th class="dis_date"> Date</th>
                        <th> Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $i=1; @endphp
                    @foreach ($data_loans as $loan)
                        <tr style="height: 50px;">
                            <td class="text-center text-muted">{{$i++}}</td>
                            <td class="id">{{$loan->loan_unique_id}}</td>
                            <td class="name">{{$loan->client_name}}</td>
                            <td class="nrc_no">{{$loan->nrc == null ? $loan->old_nrc : $loan->nrc}}</td>
                            <td class="cen">{{$loan->center_uniquekey}}</td>
                            <td class="type">{{$loan->loan_type_name}}</td>
                            <td class="text-center amount">{{$loan->loan_amount}}</td>
                            <td class="branch">{{$loan->branch_name}}</td>
                            <td class="text-center dis_status">{{$loan->disbursement_status}}</td>
                            <td class="dis_date">{{$loan->disbursement_date}}</td>
                            <td class="text-center text-nowrap">
                                <a href="loan/view/{{$loan->loan_unique_id}}">
                                    <button type="button" id="PopoverCustomT-1"
                                            class="border-0 btn-transition btn  btn-outline-info btn-sm"><i
                                            class="fa fa-eye"></i></button>
                    			</a>
                    			<a href="repayment_reports/view/{{$loan->loan_unique_id}}">
                                    <button type="button" id="PopoverCustomT-1"
                                        class="border-0 btn-transition btn  btn-outline-info btn-sm"><i
                                        class="fa fa-money-bill"></i></button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-block text-center card-footer">
                <div class="">
                    {{ $data_loans->appends($_GET)->links() }}
                </div>


                {{--                <div class="text-left">--}}
                {{--                    <div class="custom-control custom-switch">--}}
                {{--                        <input type="checkbox" class="custom-control-input" name="id" id="loanreportid">--}}
                {{--                        <label class="custom-control-label" for="loanreportid">Loan ID</label>--}}
                {{--                    </div>--}}
                {{--                    <div class="custom-control custom-switch">--}}
                {{--                        <input type="checkbox" class="custom-control-input" name="name" id="loanreportname">--}}
                {{--                        <label class="custom-control-label" for="loanreportname">Name</label>--}}
                {{--                    </div>--}}
                {{--                    <div class="custom-control custom-switch">--}}
                {{--                        <input type="checkbox" class="custom-control-input" name="nrc_no" id="loanreportnrc">--}}
                {{--                        <label class="custom-control-label" for="loanreportnrc">NRC Number</label>--}}
                {{--                    </div>--}}
                {{--                    <div class="custom-control custom-switch">--}}
                {{--                        <input type="checkbox" class="custom-control-input" name="cen" id="loanreportcenter">--}}
                {{--                        <label class="custom-control-label" for="loanreportcenter">Center</label>--}}
                {{--                    </div>--}}
                {{--                    <div class="custom-control custom-switch">--}}
                {{--                        <input type="checkbox" class="custom-control-input" name="type" id="loanreportloantype">--}}
                {{--                        <label class="custom-control-label" for="loanreportloantype">Loan Type</label>--}}
                {{--                    </div>--}}
                {{--                    <div class="custom-control custom-switch">--}}
                {{--                        <input type="checkbox" class="custom-control-input" name="amount" id="loanreportamount">--}}
                {{--                        <label class="custom-control-label" for="loanreportamount">Loan Amount</label>--}}
                {{--                    </div>--}}
                {{--                    <div class="custom-control custom-switch">--}}
                {{--                        <input type="checkbox" class="custom-control-input" name="branch" id="loanreportbranch">--}}
                {{--                        <label class="custom-control-label" for="loanreportbranch">Branch</label>--}}
                {{--                    </div>--}}
                {{--                    <div class="custom-control custom-switch">--}}
                {{--                        <input type="checkbox" class="custom-control-input" name="dis_status" id="loanreportstatus">--}}
                {{--                        <label class="custom-control-label" for="loanreportstatus">Disbursement Status</label>--}}
                {{--                    </div>--}}
                {{--                    <div class="custom-control custom-switch">--}}
                {{--                        <input type="checkbox" class="custom-control-input" name="dis_date" id="loanreportdate">--}}
                {{--                        <label class="custom-control-label" for="loanreportdate">Disbursement Date</label>--}}
                {{--                    </div>--}}
                {{--                    <input type="checkbox" name="id" id="loanreportid"> <label for="loanreportid">Loan ID</label>--}}
                {{--                    <input type="checkbox" name="name" id="loanreportname"> <label for="loanreportname">Name</label>--}}
                {{--                    <input type="checkbox" name="nrc_no" id="loanreportnrc"> <label for="loanreportnrc">NRC Number</label>--}}
                {{--                    <input type="checkbox" name="cen" id="loanreportcenter"> <label for="loanreportcenter">Center</label>--}}
                {{--                    <input type="checkbox" name="type" id="loanreportloantype"> <label for="loanreportloantype">Loan Type</label>--}}
                {{--                    <input type="checkbox" name="amount" id="loanreportamount"> <label for="loanreportamount">Loan Amount</label>--}}
                {{--                    <input type="checkbox" name="branch" id="loanreportbranch"> <label for="loanreportbranch">Branch</label>--}}
                {{--                    <input type="checkbox" name="dis_status" id="loanreportstatus"> <label for="loanreportstatus">Disbursement Status</label>--}}
                {{--                    <input type="checkbox" name="dis_date" id="loanreportdate"> <label for="loanreportdate">Disbursement Date</label>--}}


            </div>
        </div>
    </div>
    </div>

    <script>
        var loanReportPdfBtn = document.getElementById('loanReportPdfBtn');
        var loanReportPdf = document.getElementById('loanReport');

        //loanReportToPdf
        loanReportPdfBtn.addEventListener("click", function () {
            // alert("ok");
            html2pdf(loanReportPdf);
        });
    </script>
@endsection


