<!DOCTYPE html>
<html lang="en">
<head>
	<title>MKT Microfinance</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->	
	<link rel="shortcut icon" href="{{asset('admin/assets/css/images/logo_circle.png')}}"/>
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('admin/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('admin/login/fonts/iconic/css/material-design-iconic-font.min.css')}}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('admin/login/vendor/animate/animate.css')}}">
    <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="{{asset('admin/login/vendor/css-hamburgers/hamburgers.min.css')}}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('admin/login/vendor/animsition/css/animsition.min.css')}}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('admin/login/vendor/select2/select2.min.css')}}">
    <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="{{asset('admin/login/vendor/daterangepicker/daterangepicker.css')}}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('admin/login/css/util.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('admin/login/css/main.css')}}">
    <!--===============================================================================================-->

	<style>

		input::-webkit-input-placeholder { color: #808080;}
		input:-moz-placeholder { color: #808080;}
		input::-moz-placeholder { color: #808080;}
		input:-ms-input-placeholder { color: #808080;}

		/*//////////////////////////////////////////////////////////////////
		[ login ]*/

		.wrap-login100{
			width: 400px;
			background: #fff;
			opacity: 0.9;
		}

		.container-login100::before {
			content: "";
			display: block;
			position: absolute;
			z-index: -1;
			width: 100%;
			height: 100%;
			top: 0;
			left: 0;
			background-color: rgba(0, 0, 0, 0.3);
		}

		.container-login100 {
			width: 100%;  
			min-height: 100vh;
			display: -webkit-box;
			display: -webkit-flex;
			display: -moz-box;
			display: -ms-flexbox;
			display: flex;
			flex-wrap: wrap;
			justify-content: center;
			align-items: center;
			padding: 15px;

			background-repeat: no-repeat;
			background-position: center;
			background-size: cover;
			position: relative;
			z-index: 1;  
		}

		/*------------------------------------------------------------------
		[ Form ]*/
		
		.login100-form-logo {
			font-size: 60px; 
			color: #333333;

			display: -webkit-box;
			display: -webkit-flex;
			display: -moz-box;
			display: -ms-flexbox;
			display: flex;
			justify-content: center;
			align-items: center;
			width: 120px;
			height: 100px;
			border-radius: 50%;
			background-color: #fff;
			margin: 0 auto;
			/* border: 1px solid #000; */
			-webkit-transform: translateY(-35%);
		}

			.login100-form {
				width: 100%;
		}

		/*------------------------------------------------------------------
		[ Input ]*/

		.input100 {
			font-family: Poppins-Regular;
			font-size: 16px;
			color: #000;
			line-height: 1.2;

			display: block;
			width: 100%;
			height: 45px;
			background: transparent;
			padding: 0 5px 0 38px;
		}

		.wrap-input100 {
			width: 100%;
			position: relative;
			border-bottom: 2px solid #808080;
			/* rgba(255,255,255,0.24) */
			margin-bottom: 30px;
		}
		/*---------------------------------------------*/ 
		.focus-input100::before {
			content: "";
			display: block;
			position: absolute;
			bottom: -2px;
			left: 0;
			width: 0;
			height: 2px;

			-webkit-transition: all 0.4s;
			-o-transition: all 0.4s;
			-moz-transition: all 0.4s;
			transition: all 0.4s;

			background: #808080;
		}

		.focus-input100::after {
			font-family: Material-Design-Iconic-Font;
			font-size: 22px;
			color: #808080;

			content: attr(data-placeholder);
			display: block;
			width: 100%;
			position: absolute;
			top: 6px;
			left: 0px;
			padding-left: 5px;

			-webkit-transition: all 0.4s;
			-o-transition: all 0.4s;
			-moz-transition: all 0.4s;
			transition: all 0.4s;
		}

		/*==================================================================
		[ Restyle Checkbox ]*/

		.contact100-form-checkbox {
			padding-left: 5px;
			padding-top: 5px;
			padding-bottom: 35px;
		}

		.input-checkbox100 {
			display: none;
		}

			.label-checkbox100 {
			font-family: Poppins-Regular;
			font-size: 13px;
			color: #000;
			line-height: 1.2;

			display: block;
			position: relative;
			padding-left: 26px;
			cursor: pointer;
			}

		.label-checkbox100::before {
			content: "\f26b";
			font-family: Material-Design-Iconic-Font;
			font-size: 13px;
			color: transparent;

			display: -webkit-box;
			display: -webkit-flex;
			display: -moz-box;
			display: -ms-flexbox;
			display: flex;
			justify-content: center;
			align-items: center;
			position: absolute;
			width: 16px;
			height: 16px;
			border-radius: 2px;
			background: #808080;
			left: 0;
			top: 50%;
			-webkit-transform: translateY(-50%);
			-moz-transform: translateY(-50%);
			-ms-transform: translateY(-50%);
			-o-transform: translateY(-50%);
			transform: translateY(-50%);
			}

		.input-checkbox100:checked + .label-checkbox100::before {
			color: #fff;
		}

		/*----------------Login in Button*/
		.container-login100-form-btn {
			width: 100%;
			display: -webkit-box;
			display: -webkit-flex;
			display: -moz-box;
			display: -ms-flexbox;
			display: flex;
			flex-wrap: wrap;
			justify-content: center;
			}

		.login100-form-btn {
			font-family: Poppins-Medium;
			font-size: 16px;
			color: #fff;
			line-height: 1.2;

			display: -webkit-box;
			display: -webkit-flex;
			display: -moz-box;
			display: -ms-flexbox;
			display: flex;
			justify-content: center;
			align-items: center;
			padding: 0 20px;
			min-width: 120px;
			height: 50px;
			border-radius: 25px;
			position: relative;
			z-index: 1;

			-webkit-transition: all 0.4s;
			-o-transition: all 0.4s;
			-moz-transition: all 0.4s;
			transition: all 0.4s;
		}

		.login100-form-btn::before {
			content: "";
			display: block;
			position: absolute;
			z-index: -1;
			width: 100%;
			height: 100%;
			border-radius: 25px;
			background-color: #3d6cb9;
			top: 0;
			left: 0;
			opacity: 1;

			-webkit-transition: all 0.4s;
			-o-transition: all 0.4s;
			-moz-transition: all 0.4s;
			transition: all 0.4s;
		}

		.login100-form-btn:hover {
			color: #fff;
		}

		.login100-form-btn:hover:before {
			opacity: 0;
		}
	</style>

</head>
<body>
	<div class="limiter">
		<div class="container-login100" style="background-image: url('admin/login/images/mkt_logo_mdy.jpg');">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="POST" action="{{route('login.custom')}}">
                @csrf
                    <span>
                        <img class="login100-form-logo" src="{{asset('admin/login/images/mktlogo.jpg')}}" alt="">
                    </span>

					<div class="wrap-input100 validate-input" data-validate = "Enter Email Address">
						<input class="input100" type="email" name="email" placeholder="Email">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
                        @if ($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        @endif
					</div>


					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
                        @if ($errors->has('password'))
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        @endif
					</div>

					<div class="contact100-form-checkbox">
						<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me" class="form-check-input" {{ old('remember-me') ? 'checked' : '' }}>
						<label class="label-checkbox100" for="ckb1">
							Remember me
						</label>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="{{asset('admin/login/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('admin/login/vendor/animsition/js/animsition.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('admin/login/vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{asset('admin/login/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('admin/login/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('admin/login/vendor/daterangepicker/moment.min.js')}}"></script>
	<script src="{{asset('admin/login/vendor/daterangepicker/daterangepicker.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('admin/login/vendor/countdowntime/countdowntime.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('admin/login/js/main.js')}}"></script>

</body>
</html>