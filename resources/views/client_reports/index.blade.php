@extends('layouts.app')
@section('content')

    <div class="col-md-12">
        <div class="mb-3"></div>
        <div class="main-card mb-3 card">
            <div class="card-header">

                Client Information
                <div class="btn-actions-pane-right">
                    <div role="group" class="btn-group-sm btn-group">
                        <a href="{{url('client/create')}}">
                            <button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-white" data-original-title="Example Tooltip">
                                <i class="fa fa-plus"></i>
                                Add
                            </button>
                        </a>
                        <a href="{{url('client_reports?date=week')}}">
                            <button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-white" data-original-title="This Week">
                                <i class="fa fa-calendar"></i>
                                This Week
                            </button>
                        </a>
                        <a href="{{url('client_reports?date=month')}}">
                            <button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-white" data-original-title="This Month">
                                <i class="fa fa-calendar"></i>
                                This Month
                            </button>
                        </a>
                        <a href="{{url('client_reports?date=year')}}">
                            <button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-white" data-original-title="This Week">
                                <i class="fa fa-calendar"></i>
                                This Year
                            </button>
                        </a>
                        <a href="{{url('client_reports?date=date')}}">
                            <button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-white" data-original-title="Today">
                                <i class="fa fa-calendar"></i>
                                Today
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            <div class="widget-content p-2">
                <div class="widget-content-wrapper">
                    {{-- <div class="widget-content-left mr-3 ml-3">
                        <label class="m-0"> Show </label>
                    </div> --}}
					<div class="widget-content-left mr-1">
                        <div class="widget-heading">
                            <form method="GET" id="clientReportLengthForm">
                                <select name="client_report_length" id="clientReportLength" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm">
                                    <option value="10" @if(request()->client_report_length == 10) selected @endif>10</option>
                                    <option value="25" @if(request()->client_report_length == 25) selected @endif>25</option>
                                    <option value="50" @if(request()->client_report_length == 50) selected @endif>50</option>
                                    <option value="100" @if(request()->client_report_length == 100) selected @endif>100</option>
                                </select>
                            </form>
                        </div>
                    </div>
                    {{--<div class="widget-content-left ">
                        <label class="m-p">  </label>
                        <div class="btn-group">
                            <form action="{{ route('client_reports') }}" id="clientReportBranchForm" method="GET">
                            <select class="custom-select custom-select-sm form-control form-control-sm text-center text-black" id="clientReportBranch" name="searchbybranch">
                                @foreach($data_branch as $branchlist)
                                    <option class="text-left text-black" value="{{ $branchlist->id }}" {{ request()->searchbybranch == $branchlist->id ? 'selected' : '' }}>{{ $branchlist->branch_name}}</option>
                                @endforeach
                            </select>
                            </form>
                        </div>
                    </div>--}}
                    <div class="widget-content-left ">
                        <label class="ml-2">  </label>
                        <div class="btn-group">
                            <form action="{{ route('client_reports') }}" id="clientReportTypeForm" method="GET">
                            <select class="custom-select custom-select-sm form-control form-control-sm text-center text-black" id="clientReportType" name="searchbytype" id="client_type">
                                <option value="" disabled selected>Choose Client Type</option>
                                <option value="new" {{ request()->searchbytype == 'new' ? 'selected' : '' }}>New Member</option>
                                <option value="active" {{ request()->searchbytype == 'active' ? 'selected' : '' }}>Active Member</option>
                                <option value="dropout" {{ request()->searchbytype == 'dropout' ? 'selected' : '' }}>Drop-Out Member</option>
                                <option value="dead" {{ request()->searchbytype == 'dead' ? 'selected' : '' }}>Dead Member</option>
                                <option value="rejoin" {{ request()->searchbytype == 'rejoin' ? 'selected' : '' }}>Rejoin Member</option>
                                <option value="substitude" {{ request()->searchbytype == 'substitude' ? 'selected' : '' }}>Substitude Member</option>
                                <option value="dormant" {{ request()->searchbytype == 'dormant' ? 'selected' : '' }}>Dormant Member</option>
                                <option value="reject" {{ request()->searchbytype == 'reject' ? 'selected' : '' }}>Reject Member</option>
                            </select>
                            </form>
                        </div>
                    </div>
                    <div class="widget-content-right">
                        {{-- export --}}
                        <div class="dropdown d-inline-block ml-auto" id="exportPopup">
                            <button type="button" id="exportSelect" aria-haspopup="true" aria-expanded="false"
                                    data-toggle="dropdown" class="mr-2 dropdown-toggle btn btn-dark">
                                <i class="fas fa-file-export"></i>
                                Export
                            </button>
                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu" id="export" style="">
                                <div class="text-left pl-2">
                                    <button class="btn btn-outline-dark w-100 mb-0" id="clientReportPdfBtn">Export Pdf</button>
                                    <button class="btn btn-outline-dark w-100 mb-0" id="clientReportExcelBtn">Export Excel</button>
                                </div>
                            </div>
                        </div>
                        {{-- export --}}
{{--                        <button class="btn btn-dark" id="clientReportPdfBtn">Export Pdf</button>--}}
{{--                        <button class="btn btn-dark" id="clientReportExcelBtn">Export Excel</button>--}}
                        {{-- test --}}
                        <div class="dropdown d-inline-block ml-auto" id="columnPopup">
                            <button type="button" id="columnSelect" aria-haspopup="true" aria-expanded="false"
                                    data-toggle="dropdown" class="mr-2 dropdown-toggle btn btn-success">
                                <i class="fas fa-eye-slash"></i>
                                Column visibility
                            </button>
                            <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu" id="column" style="">
                                <div class="text-left pl-2">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input c-report-check" name="client" id="reportclient">
                                        <label class="custom-control-label" for="reportclient">Client ID</label>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input c-report-check" name="name" id="reportname">
                                        <label class="custom-control-label" for="reportname">Name</label>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input c-report-check" name="dob"
                                               id="reportdob">
                                        <label class="custom-control-label" for="reportdob">DOB</label>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input c-report-check" name="gen"
                                               id="reportgen">
                                        <label class="custom-control-label" for="reportgen">Gender</label>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input c-report-check" name="ph"
                                               id="reportph">
                                        <label class="custom-control-label" for="reportph">Phone</label>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input c-report-check" name="sta"
                                               id="reportsta">
                                        <label class="custom-control-label" for="reportsta">Status</label>
                                    </div>
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input c-report-check" name="edu"
                                               id="reportedu">
                                        <label class="custom-control-label" for="reportedu">Education</label>
                                    </div>

                                </div>
                            </div>
                            {{-- test --}}
                        <div class="btn-group">
                            <!-- <label class="m-2"> Search </label> -->
                            <form method="GET" >
                                <div class="input-group">
                                    <input type="text" name="search_clientlist" id="search_clientlist"
                                           value="{{ request()->get('search_clientlist') }}"
                                           class="form-control mr-1"
                                           placeholder="Search..."
                                           aria-label="Search"
                                           aria-describedby="button-addon2"
                                    >

                                    <button class="btn theme-color text-white mr-1" type="submit" id="button-addon2">
                                        <i class="fa fa-search"></i>
                                    </button>

                                    <a href="{{url('client_reports')}}" class="btn theme-color text-white">
                                        <i class="fa fa-refresh"></i>
                                    </a>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            @if(session('successMsg')!=NULL)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('successMsg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table  table-striped table-hover" id="clientReport">
                    <thead>
                    <tr class="d-none">
                        <th colspan="8" class="text-center">Customer Info</th>
                    </tr>
                    <tr>
                        <th class="text-center" scope="col"> No </th>
                        <th class="sorting client" scope="col"> Client ID </th>
                        <th scope="col" class="name"> Name </th>
                        <th scope="col" class="dob"> DOB </th>
                        <th scope="col" class="gen"> Gender </th>
                        <th scope="col" class="ph"> Phone </th>
                        <th scope="col" class="sta"> Status </th>
                        <th scope="col" class="edu">Education</th>
                        <th> Action </th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $i=1; @endphp
                    @foreach ($data_clients as $client)
                        <tr>
                            <td class="text-center text-muted">{{$i++}}</td>
                            <td class="client">{{$client->client_uniquekey}}</td>
                            <td class="name">
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left mr-3">
                                            <div class="widget-content-left">
                                                <img width="40" class="rounded-circle" src="{{asset('storage/app/public/clientphotos/'.$client->client_photo)}}" alt="">
                                            </div>
                                        </div>
                                        <div class="widget-content-left flex2">
                                            <div class="widget-heading">{{$client->name}}</div>
                                            <div class="widget-subheading pt-3">{{$client->nrc == null ? $client->old_nrc : $client->nrc}}</div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="dob">{{$client->dob}}</td>
                            <td class="gen">{{$client->gender}}</td>
                            <td class="ph">{{$client->phone_primary}}</td>
                            <td class="sta">{{$client->client_type}}</td>
                            <td class="edu">{{$client->education_name}}</td>
                            <td class="text-center text-nowrap">
                                <a href="client/view/{{$client->client_uniquekey}}">
                                    <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn  btn-outline-info btn-sm"><i class="fa fa-eye"></i></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-inline text-center card-footer">
                <div class="">
                    {{ $data_clients->appends($_GET)->links() }}
                </div>

                {{-- <div class="widget-content-left ">
                    <label class="ml-2">  </label>
                    <div class="btn-group">
                        <select class="custom-select custom-select-sm form-control form-control-sm text-center text-black visibility" name="novisibility" id="novisibility">
                            <option value="">Column Visibility</option>
                            <option value="id">Client ID</option>
                            <option value="">Name</option>
                            <option value="">DOB</option>
                            <option value="">Gender</option>
                            <option value="">Phone</option>
                            <option value="">Status</option>
                            <option value="">Education</option>
                        </select>
                    </div>
                </div>                 --}}
{{--                <div>--}}
{{--                    <input type="checkbox" name="client" id="reportclient"> <label for="reportclient">Client ID</label>--}}
{{--                    <input type="checkbox" name="name" id="reportname"> <label for="reportname">Name</label>--}}
{{--                    <input type="checkbox" name="dob" id="reportdob"> <label for="reportdob">DOB</label>--}}
{{--                    <input type="checkbox" name="gen" id="reportgen"> <label for="reportgen">Gender</label>--}}
{{--                    <input type="checkbox" name="ph" id="reportph"> <label for="reportph">Phone</label>--}}
{{--                    <input type="checkbox" name="sta" id="reportsta"> <label for="reportsta">Status</label>--}}
{{--                    <input type="checkbox" name="edu" id="reportedu"> <label for="reportedu">Education</label>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>

        <script>
            var clientReportPdfBtn = document.getElementById('clientReportPdfBtn');
            var clientReportPdf = document.getElementById('clientReport');

            //clientReportToPdf
            clientReportPdfBtn.addEventListener("click", function (e) {
                // alert("ok");
                html2pdf(clientReportPdf);
            });
        </script>
@endsection

