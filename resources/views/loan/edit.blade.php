@extends('layouts.app')
@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Loan Edit</div>
            </div>
            <div class="page-title-actions">
                <div class="d-inline-block ">
                    <a href="{{url('loan/')}}" class="btn theme-color text-white">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>
                </div>
            </div>
              </div>
    </div>

    @if ($errors->any())
				<div class="alart alart-danger">
					<ul>
						@foreach($errors->all() as $error)
						<li>{{$error}}</li>
						@endforeach
					</ul>
				</div>
			@endif

			<form class="needs-validation" method="POST" action="{{route('loan.update', $loan->loan_unique_id)}}" enctype="multipart/form-data" >
            @csrf
            @method('PUT')
            <div class="main-card mb-3 card">
            <div class="card-body">
                <ul class="tabs-animated-shadow nav-justified tabs-animated nav">
                    <li class="nav-item">
                        <a role="tab" class="nav-link active" id="tab-c1-0" data-toggle="tab" href="#tab-animated1-0" aria-selected="true">
                            <span class="nav-text">Client</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a role="tab" class="nav-link" id="tab-c1-1" data-toggle="tab" href="#tab-animated1-1" aria-selected="false">
                            <span class="nav-text">Account</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a role="tab" class="nav-link" id="tab-c1-2" data-toggle="tab" href="#tab-animated1-2" aria-selected="false">
                            <span class="nav-text">Documents</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a role="tab" class="nav-link" id="tab-c1-3" data-toggle="tab" href="#tab-animated1-3" aria-selected="false">
                            <span class="nav-text">Payment Schedule</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-animated1-0" role="tabpanel">
                    <br>

                    <div class="form-row">
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label for="" style="font-weight: 600">Client</label>
                                <input name="client_name" id="client_name" placeholder="For Example : Mya Mya"  value="{{ old('client_name') }}" type="search" class="form-control" >
                               <input type="hidden" name="client_id" id="client_id_hidden" value="">
                                <div id="client_list">
                                 </div>
                                <select type="select" id="client_id" name="client_id" class="custom-select" required>
                                    <option value="">Choose Client</option>
                                    @foreach ($clients as $client)
                                        <option value="{{ $client->client_uniquekey}}" @if($loan->client_id == $client->client_uniquekey) selected @endif >{{ $client->name }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('client_id')}}
                                </div>
                            </div>
                        </div>

                                {{--                                client card edit data start--}}
                                <div class="col-12 col-md-9">
                                    <div class="row">
                                        <div class="col-12 col-lg-8">
                                            <div class="card user-card" id="clientEditCard">
                                                <div class="card-body">
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <img class="img-fluid rounded img-radius"
                                                             id="clientEditPhoto"
                                                             src="{{ asset('public/storage/clientphotos/'.$loan->client_photo) }}"
                                                             style="width: 70px"
                                                             alt="card-img">
                                                    </div>
                                                    <div class="ml-3 text-nowrap">
                                                        <p id="clientEditID">ID - {{ $loan->client_uniquekey }}</p>
                                                        <p id="clientEditName">Name - {{ $loan->name }}</p>
                                                        <p id="clientEditNrc" class="mb-0">NRC - {{ $loan->nrc == null ? $loan->old_nrc : $loan->nrc }}</p>
                                                    </div>
                                					<div class="text-nowrap ml-3">
                                                        <p id="clientEditPhone">Phone - {{ $loan->phone_primary }}</p>
                                                        <p id="clientEditFather">Father - {{ $loan->father_name }}</p>
                                                        <p id="clientEditDOB" class="mb-0">DOB - {{ $loan->dob }}</p>
                                					</div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                {{--                                client card edit data end--}}

                         <div class="col-md-3">
                            <div class="position-relative form-group">
                            	{{-- <label for="" style="font-weight: 600">Client NRC</label> --}}
                                <input name="client_nrc" id="client_nrc" placeholder=""  value="{{ $loan->nrc }}" type="hidden" class="form-control" disabled>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                {{-- <label for="" style="font-weight: 600">Client ID</label> --}}
                                <input name="solo_client_name" id="solo_client_name" placeholder=""  value="{{ $loan->client_uniquekey }}" type="hidden" class="form-control" disabled>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('solo_client_name')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                {{-- <label for="" style="font-weight: 600">Client Phone</label> --}}
                                <input name="client_phone" id="client_phone" placeholder=""  value="{{ $loan->phone_primary }}" type="hidden" class="form-control" disabled>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('client_phone')}}
                                </div>
                            </div>
                        </div>
                        </div>

                        <div class="form-row">

                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label for="" style="font-weight: 600">Saving Amount</label>
                                <input name="saving_amount" id="saving_amount" placeholder="0"  value="{{ old('saving_amount') }}" type="text" class="form-control" disabled >
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('saving_amount')}}
                                </div>
                            </div>
                        </div>
                     <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label for="" style="font-weight: 600">Business Type</label>
                                <select type="select" id="business_type_id" name="business_type_id" class="form-control" required>
                                    @foreach ($business_types as $business_type)
                                        <option value="{{ $business_type->id}}"
                                            @if($business_type->id == $loan->business_type_id) selected="selected" @endif {{(old("business_type_id")== $business_type->business_type_name ? "selected":"")}}>{{ $business_type->business_type_name }}</option>
                                    @endforeach

                                    {{-- @foreach ($centers as $center)
                                    <option value="{{ $center->id }}"
                                            @if($group->center_id == $center->id) selected="selected" @endif >{{ $center->center_uniquekey }}</option>
                                    @endforeach --}}

                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('business_type_id')}}
                                </div>
                            </div>

                       </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label for="" >Business Category</label>
                                <select type="select" id="business_category_id" name="business_category_id" class="form-control" required>
                               	 	@foreach ($business_categories as $business_category)
                                        <option value="{{ $business_category->id}}"
                                            @if($business_category->id == $loan->business_category_id) selected="selected" @endif >{{ $business_category->business_category_name }}</option>
                                    @endforeach
                                {{-- <option  value="{{ $loan->business_category_id }}">{{ $loan->business_category_name }}</option> --}}
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('business_category_id')}}
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($guarantor_a_loan_doc)
                    <div class="form-row">
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label for="" style="font-weight: 600">Guarantor A</label>
                                <input name="guarantor_name_a" id="guarantor_name_a"
                                       placeholder="For Example : Mya Mya" value="{{ old('guarantor_name_a') }}"
                                       type="search" class="form-control">
                                <input type="hidden" name="guarantor_a" id="guarantor_a_hidden" value="">
                                <div id="guarantor_list_a">
                                </div>
                                <select type="select" id="guarantor_a" name="guarantor_a" class="custom-select"
                                        required>
                                    <option value="" selected disabled>Choose Guarantor A</option>
                                    @foreach ($guarantors as $guarantor)
                                        <option value="{{ $guarantor->guarantor_uniquekey}}"
                                                @if($loan->guarantor_a == $guarantor->guarantor_uniquekey) selected @endif >{{ $guarantor->name }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('guarantor_a')}}
                                </div>
                            </div>
                        </div>

                                        {{--                                guarantor a edit card start--}}
                                <div class="col-12 col-md-9">
                                    <div class="row align-items-end">
                                        <div class="col-12 col-lg-8">
                                            <div class="card user-card" id="guarantorEditACard">
                                                <div class="card-body">
                                                <div class="d-flex align-items-start">
                                                    <div>
                                                        <img class="img-fluid rounded img-radius"
                                                             id="guarantorEditAPhoto"
                                                             src="{{ asset('public/storage/guarantor_photos/'.$loan->a_photo) }}"
                                                             style="width: 70px"
                                                             alt="card-img">
                                                    </div>
                                                    <div class="ml-3 text-nowrap">
                                                        <p id="guarantorEditAID">ID - {{ $loan->a_key }}</p>
                                                        <p id="guarantorEditAName">Name - {{ $loan->a_name }}</p>
                                                        <p id="guarantorEditANrc" class="mb-0">NRC - {{ $loan->a_nrc }}</p>
                                                    </div>
                                					<div class="ml-3 text-nowrap">
                                                        <p id="guarantorEditAPhone">Phone - {{ $loan->a_phone }}</p>
                                                        <p id="guarantorEditADOB" class="mb-0">DOB - {{ $loan->a_dob }}</p>
                                					</div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                {{--                                guarantor a edit card end--}}

                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                {{-- <label for="" style="font-weight: 600">Guarantor NRC</label> --}}
                                <input name="guarantor_a_nrc" id="guarantor_a_nrc" placeholder=""
                                       value="{{ $loan->a_nrc }}" type="hidden" class="form-control" disabled>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('guarantor_a_nrc')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                {{--  <label for="" style="font-weight: 600">Guarantor ID</label> --}}
                                <input name="guarantor_a_name" id="guarantor_a_name" placeholder=""
                                       value="{{ $loan->a_key }}" type="hidden" class="form-control" disabled>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('guarantor_a_name')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                {{-- <label for="" style="font-weight: 600">Guarantor Phone</label> --}}
                                <input name="guarantor_a_id" id="guarantor_a_id" placeholder=""
                                       value="{{ $loan->a_phone }}" type="hidden" class="form-control" disabled>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('guarantor_a_id')}}
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if($guarantor_b_loan_doc)
                        <div class="form-row">
                            <div class="col-md-3">
                                <div class="position-relative form-group">
                                    <label for="" style="font-weight: 600">Guarantor B</label>
                                    <input name="guarantor_name_b" id="guarantor_name_b"
                                           placeholder="For Example : Mya Mya"
                                           value="{{ old('guarantor_name_b') }}"
                                           type="search" class="form-control">
                                    <input type="hidden" name="guarantor_b" id="guarantor_b_hidden" value="">
                                    <div id="guarantor_list_b">
                                    </div>
                                    <select type="select" id="guarantor_b" name="guarantor_b"
                                            class="custom-select"
                                            required>
                                        <option value="" selected disabled>Choose Guarantor B</option>
                                        @foreach ($guarantors as $guarantor)
                                            <option value="{{ $guarantor->guarantor_uniquekey}}"
                                                    @if($loan->guarantor_b== $guarantor->guarantor_uniquekey) selected @endif >{{ $guarantor->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('guarantor_b')}}
                                    </div>
                                </div>
                            </div>

                                    {{--                                guarantor b edit card start--}}
                                <div class="col-12 col-md-9">
                                    <div class="row align-items-end">
                                        <div class="col-12 col-lg-8">
                                            <div class="card user-card" id="guarantorEditBCard">
                                                <div class="card-body">
                                                <div class="d-flex align-items-start">
                                                    <div>
                                                        <img class="img-fluid rounded img-radius"
                                                             id="guarantorEditBPhoto"
                                                             src="{{ asset('public/storage/guarantor_photos/'.$loan->b_photo) }}"
                                                             style="width: 70px"
                                                             alt="card-img">
                                                    </div>
                                                    <div class="ml-3 text-nowrap">
                                                        <p id="guarantorEditBID">ID - {{ $loan->b_key }}</p>
                                                        <p id="guarantorEditBName">Name - {{ $loan->b_name }}</p>
                                                        <p id="guarantorEditBNrc" class="mb-0">NRC - {{ $loan->b_nrc }}</p>
                                                    </div>
                                					<div class="ml-3 text-nowrap">
                                                        <p id="guarantorEditBPhone">Phone - {{ $loan->a_phone }}</p>
                                                        <p id="guarantorEditBDOB" class="mb-0">DOB - {{ $loan->b_dob }}</p>
                                					</div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                {{--                                guarantor b edit card end--}}

                            <div class="col-md-3">
                                <div class="position-relative form-group">
                                    {{-- <label for="" style="font-weight: 600">Guarantor NRC</label> --}}
                                    <input name="guarantor_b_nrc" id="guarantor_b_nrc" placeholder=""
                                           value="{{ $loan->b_nrc }}" type="hidden" class="form-control" disabled>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('guarantor_b_nrc')}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="position-relative form-group">
                                    {{-- <label for="" style="font-weight: 600">Guarantor ID</label> --}}
                                    <input name="guarantor_b_name" id="guarantor_b_name" placeholder=""
                                           value="{{ $loan->b_key }}" type="hidden" class="form-control" disabled>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('guarantor_b_name')}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="position-relative form-group">
                                    {{-- <label for="" style="font-weight: 600">Guarantor Phone</label> --}}
                                    <input name="guarantor_b_id" id="guarantor_b_id" placeholder=""
                                           value="{{ $loan->b_phone }}" type="hidden" class="form-control"
                                           disabled>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('guarantor_b_id')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($guarantor_c_loan_doc)
                        <div class="form-row">
                            <div class="col-md-3">
                                <div class="position-relative form-group">
                                    <label for="" style="font-weight: 600">Guarantor C</label>
                                    <input name="guarantor_name_c" id="guarantor_name_c"
                                           placeholder="For Example : Mya Mya"
                                           value="{{ old('guarantor_name_c') }}"
                                           type="search" class="form-control">
                                    <input type="hidden" name="guarantor_c" id="guarantor_c_hidden" value="">
                                    <div id="guarantor_list_c">
                                    </div>
                                    <select type="select" id="guarantor_c" name="guarantor_c"
                                            class="custom-select"
                                            required>
                                        <option value="" selected disabled>Choose Guarantor C</option>
                                        @foreach ($guarantors as $guarantor)
                                            <option value="{{ $guarantor->guarantor_uniquekey}}"
                                                    @if($loan->guarantor_c== $guarantor->guarantor_uniquekey) selected @endif >{{ $guarantor->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('guarantor_c')}}
                                    </div>
                                </div>
                            </div>

                                    {{--                                guarantor c edit card start--}}
                                <div class="col-12 col-md-9">
                                    <div class="row align-items-end">
                                        <div class="col-12 col-lg-8">
                                            <div class="card user-card" id="guarantorEditCCard">
                                                <div class="card-body">
                                                <div class="d-flex align-items-start">
                                                    <div>
                                                        <img class="img-fluid rounded img-radius"
                                                             id="guarantorEditCPhoto"
                                                             src="{{ asset('public/storage/guarantor_photos/'.$loan->c_photo) }}"
                                                             style="width: 70px"
                                                             alt="card-img">
                                                    </div>
                                                    <div class="ml-3 text-nowrap">
                                                        <p id="guarantorEditCID">ID - {{ $loan->c_key }}</p>
                                                        <p id="guarantorEditCName">Name - {{ $loan->c_name }}</p>
                                                        <p id="guarantorEditCNrc" class="mb-0">NRC - {{ $loan->c_nrc }}</p>
                                                    </div>
                                					<div class="ml-3 text-nowrap">
                                                        <p id="guarantorEditCPhone">Phone - {{ $loan->c_phone }}</p>
                                                        <p id="guarantorEditCDOB" class="mb-0">DOB - {{ $loan->c_dob }}</p>
                                					</div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                {{--                                guarantor c edit card end--}}
                            <div class="col-md-3">
                                <div class="position-relative form-group">
                                    {{-- <label for="" style="font-weight: 600">Guarantor NRC</label> --}}
                                    <input name="guarantor_c_nrc" id="guarantor_c_nrc" placeholder=""
                                           value="{{ $loan->c_nrc }}" type="hidden" class="form-control" disabled>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('guarantor_c_nrc')}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="position-relative form-group">
                                    {{-- <label for="" style="font-weight: 600">Guarantor ID</label> --}}
                                    <input name="guarantor_b_name" id="guarantor_b_name" placeholder=""
                                           value="{{ $loan->c_key }}" type="hidden" class="form-control" disabled>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('guarantor_c_name')}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="position-relative form-group">
                                    {{-- <label for="" style="font-weight: 600">Guarantor Phone</label> --}}
                                    <input name="guarantor_c_id" id="guarantor_c_id" placeholder=""
                                           value="{{ $loan->c_phone }}" type="hidden" class="form-control"
                                           disabled>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('guarantor_c_id')}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                 </div>

                 <div class="tab-pane" id="tab-animated1-1" role="tabpanel">
                <br>
                <div class="form-row">

                <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" style="font-weight: 600">Loan Unique ID</label>
                                <input name="loan_unique_id" id="" placeholder="For Example : LG-000001"  value="{{ $loan->loan_unique_id }}" type="text" class="form-control" required>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_unique_id')}}
                                </div>
                            </div>
                        </div>


                  <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" style="font-weight: 600">Branch</label>
                                <select type="select" id="" name="branch_id" class="custom-select" required>
                                    <option value="">Choose Branch</option>
                                    @foreach ($branches as $branch)
                                            <option value="{{ $branch->id }}" @if($loan->branch_id== $branch->id) selected @endif >{{ $branch->branch_name }}</option>
                                        @endforeach
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('branch_id')}}
                                </div>
                            </div>
                        </div>

                         <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" style="font-weight: 600">Group</label>
                                <select type="select" id="" name="group_id" class="custom-select">
                                    <option value="">Choose Group</option>
                                    @foreach ($groups as $group)
                                            <option value="{{ $group->group_uniquekey }}" @if($loan->group_id == $group->group_uniquekey) selected @endif >{{ $group->group_uniquekey }}</option>
                                        @endforeach
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('group_id')}}
                                </div>
                            </div>
                        </div>
                </div>

                 <div class="form-row">

                       <div class="col-md-4">
                           <div class="position-relative form-group">
                               <label for="" style="font-weight: 600">Center</label>
                                   <select type="select" id="" name="center_id" class="custom-select">
                                       <option value="">Choose Center</option>
                                       @foreach ($centers as $center)
                                           <option value="{{ $center->center_uniquekey }}" @if($loan->center_id == $center->center_uniquekey) selected @endif >{{ $center->center_uniquekey }}</option>
                                       @endforeach
                                   </select>
                               <div class="text-danger form-control-feedback">
                                   {{$errors->first('center_id')}}
                               </div>
                           </div>
                       </div>

                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" style="font-weight: 600">Loan Officer</label>
                                <select type="select" id="" name="loan_officer_id" class="custom-select" required>
                                    <option value="">Choose Loan Officer</option>
                                    @foreach ($staffs as $staff)
                                            <option value="{{ $staff->staff_code }}" @if($loan->loan_officer_id == $staff->staff_code) selected @endif >{{ $staff->name }}</option>
                                        @endforeach
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_officer_id')}}
                                </div>
                            </div>
                        </div>

                          <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" style="font-weight: 600">Loan Type</label>
                                <select type="select" id="loan_type" name="loan_type_id" class="custom-select" required>
                                    <option value="">Select Loan Type</option>
                                    @foreach ($loantypes as $loantype)
                                        <option value="{{ $loantype->id }}" @if($loan->loan_type_id == $loantype->id) selected @endif >{{ $loantype->name }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_type_id')}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">


                      <div class="col-md-4">
                          <div class="position-relative form-group">
                              <label for="" style="font-weight: 600">Loan Amount</label>
                              <input name="loan_amount" id="loan_amount"   placeholder="For Example : 200000"  value="{{ $loan->loan_amount }}" type="number" class="form-control" required>
                              <div class="text-danger form-control-feedback">
                                  {{$errors->first('loan_amount')}}
                              </div>
                          </div>
                      </div>
                              
                        <div class="col-md-4 d-none">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Principal Formula</label>
                                        <input name="principal_formula" id="principal_formula" placeholder="For Example : 0.083"
                                               value="{{ $loan->principal_formula }}" type="text" class="form-control"
                                               >
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('principal_formula')}}
                                        </div>
                                    </div>
                                </div>

                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" style="font-weight: 600">Estimate Receivable Amount</label>
                                <input name="estimate_receivable_amount" id="" placeholder="For Example : 100000"  value="{{ $loan->estimate_receivable_amount }}" type="number" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('estimate_receivable_amount')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Term Value</label>
                                            <input name="loan_term_value" id="loan_term_value"  placeholder="For Example : "  value="{{ $loan->loan_term_value }}" type="number" class="form-control" required>
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_term_value')}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Interest Rate</label>
                                            <input name="interest_rate" id="interest_rate"  placeholder="For Example : "  value="{{ $loan->interest_rate }}" type="number" step="any" class="form-control" >
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_term_value')}}
                                            </div>
                                        </div>
                                    </div>
                               </div>
                            </div>
                        </div>
                     </div>
                  <div class="form-row">

                      <div class="col-md-4">
                          <div class="position-relative form-group">
                              <label for="" style="font-weight: 600">Loan Term</label>
                              <select type="select" id="loan_term" name="loan_term" class="custom-select" required>
                                  <option value="">Choose Loan Term</option>
                                  <option {{ $loan->loan_term == 'none' ? 'selected' : '' }}>None</option>
                                  <option {{ $loan->loan_term == 'Day' ? 'selected' : '' }}>Day</option>
                                  <option {{ $loan->loan_term == 'Week' ? 'selected' : '' }}>Week</option>
                                  <option {{ $loan->loan_term == 'Two-Weeks' ? 'selected' : '' }}>Two-Weeks</option>
                                  <option {{ $loan->loan_term == 'Month' ? 'selected' : '' }}>Month</option>
                                  <option {{ $loan->loan_term == 'Year' ? 'selected' : '' }}>Year</option>
                              </select>
                              <div class="text-danger form-control-feedback">
                                  {{$errors->first('loan_term')}}
                              </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" style="font-weight: 600">Interest Rate Period</label>
                                <select type="select" id="interest_rate_period" name="interest_rate_period" class="custom-select" required>
                                    <option value="">Choose Interest Rate Period</option>
                                    <option {{ $loan->interest_rate_period == 'none' ? 'selected' : '' }}>None</option>
                                    <option {{ $loan->interest_rate_period == 'Daily' ? 'selected' : '' }}>Daily</option>
                                    <option {{ $loan->interest_rate_period == 'Weekly' ? 'selected' : '' }}>Weekly</option>
                                    <option {{ $loan->interest_rate_period == 'Two-Weeks' ? 'selected' : '' }}>Two-Weeks</option>
                                    <option {{ $loan->interest_rate_period == 'Monthly' ? 'selected' : '' }}>Monthly</option>
                                    <option {{ $loan->interest_rate_period == 'Yearly' ? 'selected' : '' }}>Yearly</option>
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('interest_rate_period')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" style="font-weight: 600">Disbursement Status</label>
                                <select type="select" id="disbursement_status" name="disbursement_status" class="custom-select" required>
                                    <option value="">Choose Disbursement Status</option>
                                    <option {{ $loan->disbursement_status == 'none' ? 'selected' : '' }}>None</option>
                                    <option {{ $loan->disbursement_status == 'Pending' ? 'selected' : '' }}>Pending</option>
                                    <option {{ $loan->disbursement_status == 'Approved' ? 'selected' : '' }}>Approved</option>
                                    <option {{ $loan->disbursement_status == 'Declined' ? 'selected' : '' }}>Declined</option>
                                    <option {{ $loan->disbursement_status == 'Withdrawn' ? 'selected' : '' }}>Withdrawn</option>
                                    <option {{ $loan->disbursement_status == 'Written-Off' ? 'selected' : '' }}>Written-Off</option>
                                    <option {{ $loan->disbursement_status == 'Closed' ? 'selected' : '' }}>Closed</option>
                                    <option {{ $loan->disbursement_status == 'Activated' ? 'selected' : '' }}>Activated</option>
                                    <option {{ $loan->disbursement_status == 'Canceled' ? 'selected' : '' }}>Canceled</option>
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('disbursement_status')}}
                                </div>
                            </div>
                        </div>
                  </div>
                  <div class="form-row">
                  <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" style="font-weight: 600">Interest Method</label>
                                <input name="interest_method" id="interest_method" placeholder="For Example : "  value="{{ $loan->interest_method }}" type="text" class="form-control" required>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('interest_method')}}
                                </div>
                            </div>
                        </div>

                          <div class="col-md-4">
                                <div class="position-relative form-group">
                                    <label for="" style="font-weight: 600">Remark</label>
                                    <input name="remark" id="remark" placeholder="For Example : "  value="{{ $loan->remark }}" type="text" class="form-control">
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('remark')}}
                                    </div>
                                </div>
                            </div>

                             <div class="col-md-4">
                                    <div class="position-relative form-group">
                                    <label for="" style="font-weight: 600">Loan Application Date</label>
                                    <input name="loan_application_date" id="loan_application_date" placeholder=""  value="{{ $dates['loan_application_date'] }}" type="datetime-local" class="form-control" required>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('loan_application_date')}}
                                    </div>
                                </div>
                            </div>
                  </div>

                   <div class="form-row">
                            <div class="col-md-4">
                                <div class="position-relative form-group">
                                    <label for="" style="font-weight: 600">First Installment Date</label>
                                    <input name="first_installment_date" id="first_installment_date" placeholder=""  value="{{ $dates['first_installment_date'] }}" type="datetime-local" class="form-control" required readonly>
                                    <div class="text-danger form-control-feedback">
                                        {{$errors->first('first_installment_date')}}
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-4 d-none">
                            <div class="position-relative form-group">
                                <label for="" style="font-weight: 600">Disbursement Date</label>
                                <input name="disbursement_date" id="dis_date" placeholder=""  value="{{ $dates['disbursement_date'] }}" type="datetime-local" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('disbursement_date')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group d-none">
                                <label for="" style="font-weight: 600">Cancel Date</label>
                                <input name="cancel_date" id="cancel_date" disabled placeholder=""  value="{{ $dates['cancel_date'] }}" type="datetime-local" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('cancel_date')}}
                                </div>
                            </div>
                        </div>
                    </div>

                                {{--                            charges and saving start--}}
                     <div>
                         <h3>Charge</h3>
                         <table class="table table-bordered">
                             <thead>
                             <tr>
                                 <th>Service Name</th>
                                 <th>Service Amount</th>
                                 <th>Charge Option</th>
                                 <th>Charge Type</th>
                                 <th>Status</th>
                             </tr>
                             </thead>
                             <tbody id="chargeTbody">
                             @foreach($loan_charges as $charge)
                                 <tr>
                                     <td>
                                         <input type="text" readonly class="form-control" value="{{ $charge->charge_name." (".$charge->amount.")" }}">
                                     </td>
                                     <td>
                                         <input type="text" readonly class="form-control" value="{{ $charge->amount }}">
                                     </td>
                                     <td>
                                         <select name="" id="" class="form-control">
                                             <option value="1" @if($charge->charge_option == 1) selected @endif>Of Loan amount</option>
                                             <option value="2" @if($charge->charge_option == 2) selected @endif>Fixed amount</option>
                                         </select>
                                     </td>
                                     <td>
                                         <select name="" id="" class="form-control">
                                             <option value="1" @if($charge->charge_type == 1) selected @endif>Deposit Before Disbursement</option>
                                             <option value="2" @if($charge->charge_type == 2) selected @endif>Deduct from loan disbursement</option>
                                             <option value="3" @if($charge->charge_type == 3) selected @endif>Every Repayments</option>
                                         </select>
                                     </td>
                                     <td>
                                         <select name="" id="" class="form-control">
                                             <option value="active" @if($charge->status == "active") selected @endif>Yes</option>
                                             <option value="unactive" @if($charge->status == "unactive") selected @endif>No</option>
                                         </select>
                                     </td>
                                 </tr>

                             @endforeach

                             </tbody>
                         </table>
                         {{--                            charges and saving end--}}

                         {{--                            compulsory saving start--}}
                         <h3>Compulsory Saving</h3>
                         <table class="table table-bordered">
                             <thead>
                             <tr>
                                 <th>Saving Name</th>
                                 <th>Saving Amount</th>
                                 <th>Charge Option</th>
                                 <th>Monthly Interest</th>
                                 <th>Product Type</th>
                                 <th>Status</th>
                             </tr>
                             </thead>
                             <tbody id="compulsoryTbody">
                             @foreach($loan_compulsory as $compulsory)
                                 <tr>
                                     <td>
                                         <input type="text" readonly class="form-control" value="{{ $compulsory->compulsory_name  }}">
                                     </td>
                                     <td>
                                         <input type="text" readonly class="form-control" value="{{ $compulsory->saving_amount." %" }}">
                                     </td>
                                     <td>
                                         <select name="" id="" class="form-control">
                                             <option value="1" selected>Of Loan amount</option>
                                             <option value="2">Fixed amount</option>
                                         </select>
                                     </td>
                                     <td>
                                         <input type="number" value="{{ $compulsory->interest_rate }}" class="form-control">
                                     </td>
                                     <td>
                                         <select name="" id="" class="form-control">
                                             <option>Deposit Before Disbursement</option>
                                             <option>Deduct from loan disbursement</option>
                                             <option>Every Repayments</option>
                                         </select>
                                     </td>
                                     <td>
                                         <select name="" id="" class="form-control">
                                             <option value="active" @if($compulsory->status == "active") selected @endif>Yes</option>
                                             <option value="unactive" @if($compulsory->status == "unactive") selected @endif>No</option>
                                         </select>
                                     </td>
                                 </tr>
                                @endforeach
                             </tbody>
                         </table>
                     </div>
                     {{--                            compulsory saving end--}}




                </div>

                <div class="tab-pane" id="tab-animated1-2" role="tabpanel">
                @if($client_loan_doc != null)
                    <br>
                <h5 class="menu-header-title text-capitalize mb-3 fsize-3">Client Documents</h5>
                    <div class="form-row">
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="">Loan Document 1</label>
                                <input name="loan_document_1" id="" placeholder="For Example : LG-000001"  value="{{ old('loan_document_1') }}" type="file" class="form-control">
                                @if($client_loan_doc->loan_document_1)
                                <img src="{{ asset('public/storage/loan_document_photos/'.$client_loan_doc->loan_document_1) }}" width="100px" height="100px" alt="" title="">
                                @endif
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_document_1')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="">Loan Document 2</label>
                                <input name="loan_document_2" id="" placeholder="For Example : "  value="{{ old('loan_document_2') }}" type="file" class="form-control">
                                @if($client_loan_doc->loan_document_2)
                                <img src="{{ asset('public/storage/loan_document_photos/'.$client_loan_doc->loan_document_2) }}" width="100px" height="100px" alt="" title="">
                                @endif
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_document_2')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                                <div class="position-relative form-group">
                                <label for="" class="">Loan Document 3</label>
                                <input name="loan_document_3" id="" placeholder="For Example : "  value="{{ old('loan_document_3') }}" type="file" class="form-control">
                                @if($client_loan_doc->loan_document_3)
                                <img src="{{ asset('public/storage/loan_document_photos/'.$client_loan_doc->loan_document_3) }}" width="100px" height="100px" alt="" title="">
                                @endif
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_document_3')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="">Loan Document 4</label>
                                <input name="loan_document_4" id="" placeholder="For Example : "  value="{{ old('loan_document_4') }}" type="file" class="form-control">
                                @if($client_loan_doc->loan_document_4)
                                <img src="{{ asset('public/storage/loan_document_photos/'.$client_loan_doc->loan_document_4) }}" width="100px" height="100px" alt="" title="">
                                @endif
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_document_4')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="">Loan Document 5</label>
                                <input name="loan_document_5" id="" placeholder="For Example : "  value="{{ old('loan_document_5') }}" type="file" class="form-control">
                                @if($client_loan_doc->loan_document_5)
                                <img src="{{ asset('public/storage/loan_document_photos/'.$client_loan_doc->loan_document_5) }}" width="100px" height="100px" alt="" title="">
                                @endif
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_document_5')}}
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-4">
                                <div class="position-relative form-group">
                                <label for="" class="">Status</label>
                                    <select type="select" id="" name="status" class="custom-select">
                                    <option value="">Choose Status</option>
                                    <option {{ old('status') == 'none' ? 'selected' : '' }}>None</option>
                                    <option {{ old('status') == 'client' ? 'selected' : '' }}>Client</option>
                                    <option {{ old('status') == 'Guarantor' ? 'selected' : '' }}>Guarantor</option>
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('status')}}
                                </div>
                            </div>
                        </div> -->
                    </div>
                    @endif

                     <br>
                {{--                            Guarantor A--}}
                    @if($loan->guarantor_a != null && $guarantor_a_loan_doc != null)
                <h5 class="menu-header-title text-capitalize mb-3 fsize-3">Guarantor A Documents</h5>
                <div class="form-row">
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label for="" class="">Loan Document 1</label>
                            <input name="loan_document_6" id="" placeholder="For Example : LG-000001"
                                   value="{{ old('loan_document_6') }}" type="file" class="form-control">
                            @if($guarantor_a_loan_doc->loan_document_1)
                            <img
                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_a_loan_doc->loan_document_1) }}"
                                width="100px" height="100px" alt="" title="">
                            @endif
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('loan_document_6')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label for="" class="">Loan Document 2</label>
                            <input name="loan_document_7" id="" placeholder="For Example : "
                                   value="{{ old('loan_document_7') }}" type="file" class="form-control">
                            @if($guarantor_a_loan_doc->loan_document_2)
                            <img
                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_a_loan_doc->loan_document_2) }}"
                                width="100px" height="100px" alt="" title="">
                            @endif
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('loan_document_7')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label for="" class="">Loan Document 3</label>
                            <input name="loan_document_8" id="" placeholder="For Example : "
                                   value="{{ old('loan_document_8') }}" type="file" class="form-control">
                            @if($guarantor_a_loan_doc->loan_document_3)
                            <img
                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_a_loan_doc->loan_document_3) }}"
                                width="100px" height="100px" alt="" title="">
                            @endif
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('loan_document_8')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label for="" class="">Loan Document 4</label>
                            <input name="loan_document_9" id="" placeholder="For Example : "
                                   value="{{ old('loan_document_9') }}" type="file" class="form-control">
                            @if($guarantor_a_loan_doc->loan_document_4)
                            <img
                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_a_loan_doc->loan_document_4) }}"
                                width="100px" height="100px" alt="" title="">
                            @endif
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('loan_document_9')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label for="" class="">Loan Document 5</label>
                            <input name="loan_document_10" id="" placeholder="For Example : "
                                   value="{{ old('loan_document_10') }}" type="file" class="form-control">
                            @if($guarantor_a_loan_doc->loan_document_5)
                            <img
                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_a_loan_doc->loan_document_5) }}"
                                width="100px" height="100px" alt="" title="">
                            @endif
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('loan_document_10')}}
                            </div>
                        </div>
                    </div>
                <!-- <div class="col-md-4">
                    <div class="position-relative form-group">
                    <label for="" class="">Status</label>
                        <select type="select" id="" name="status" class="custom-select">
                        <option value="">Choose Status</option>
                        <option {{ old('status') == 'none' ? 'selected' : '' }}>None</option>
                        <option {{ old('status') == 'client' ? 'selected' : '' }}>Client</option>
                        <option {{ old('status') == 'Guarantor' ? 'selected' : '' }}>Guarantor</option>
                    </select>
                    <div class="text-danger form-control-feedback">
                        {{$errors->first('status')}}
                    </div>
                </div>
            </div> -->
                </div>
                    @endif

                @if($loan->guarantor_b != null && $guarantor_b_loan_doc != null)
                    <br>
                    {{--                            Guarantor B--}}
                    <h5 class="menu-header-title text-capitalize mb-3 fsize-3">Guarantor B Documents</h5>
                    <div class="form-row">
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="">Loan Document 1</label>
                                <input name="loan_document_11" id="" placeholder="For Example : LG-000001"
                                       value="{{ old('loan_document_11') }}" type="file"
                                       class="form-control">
                                @if($guarantor_b_loan_doc->loan_document_1)
                                <img
                                    src="{{ asset('public/storage/loan_document_photos/'.$guarantor_b_loan_doc->loan_document_1) }}"
                                    width="100px" height="100px" alt="" title="">
                                @endif
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_document_11')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="">Loan Document 2</label>
                                <input name="loan_document_12" id="" placeholder="For Example : "
                                       value="{{ old('loan_document_12') }}" type="file"
                                       class="form-control">
                                @if($guarantor_b_loan_doc->loan_document_2)
                                <img
                                    src="{{ asset('public/storage/loan_document_photos/'.$guarantor_b_loan_doc->loan_document_2) }}"
                                    width="100px" height="100px" alt="" title="">
                                @endif
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_document_12')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="">Loan Document 3</label>
                                <input name="loan_document_13" id="" placeholder="For Example : "
                                       value="{{ old('loan_document_13') }}" type="file"
                                       class="form-control">
                                @if($guarantor_b_loan_doc->loan_document_3)
                                <img
                                    src="{{ asset('public/storage/loan_document_photos/'.$guarantor_b_loan_doc->loan_document_3) }}"
                                    width="100px" height="100px" alt="" title="">
                                @endif
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_document_13')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="">Loan Document 4</label>
                                <input name="loan_document_14" id="" placeholder="For Example : "
                                       value="{{ old('loan_document_14') }}" type="file"
                                       class="form-control">
                                @if($guarantor_b_loan_doc->loan_document_4)
                                <img
                                    src="{{ asset('public/storage/loan_document_photos/'.$guarantor_b_loan_doc->loan_document_4) }}"
                                    width="100px" height="100px" alt="" title="">
                                @endif
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_document_14')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="">Loan Document 5</label>
                                <input name="loan_document_15" id="" placeholder="For Example : "
                                       value="{{ old('loan_document_15') }}" type="file"
                                       class="form-control">
                                @if($guarantor_b_loan_doc->loan_document_5)
                                <img
                                    src="{{ asset('public/storage/loan_document_photos/'.$guarantor_b_loan_doc->loan_document_5) }}"
                                    width="100px" height="100px" alt="" title="">
                                @endif
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_document_15')}}
                                </div>
                            </div>
                        </div>
                    <!-- <div class="col-md-4">
                    <div class="position-relative form-group">
                    <label for="" class="">Status</label>
                        <select type="select" id="" name="status" class="custom-select">
                        <option value="">Choose Status</option>
                        <option {{ old('status') == 'none' ? 'selected' : '' }}>None</option>
                        <option {{ old('status') == 'client' ? 'selected' : '' }}>Client</option>
                        <option {{ old('status') == 'Guarantor' ? 'selected' : '' }}>Guarantor</option>
                    </select>
                    <div class="text-danger form-control-feedback">
                        {{$errors->first('status')}}
                        </div>
                    </div>
                </div> -->
                    </div>
                @endif

                @if($loan->guarantor_c != null && $guarantor_c_loan_doc != null)
                    <br>
                    {{--                            Guarantor C--}}
                    <h5 class="menu-header-title text-capitalize mb-3 fsize-3">Guarantor C Documents</h5>
                    <div class="form-row">
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="">Loan Document 1</label>
                                <input name="loan_document_16" id="" placeholder="For Example : LG-000001"
                                       value="{{ old('loan_document_16') }}" type="file"
                                       class="form-control">
                                @if($guarantor_c_loan_doc->loan_document_1)
                                <img
                                    src="{{ asset('public/storage/loan_document_photos/'.$guarantor_c_loan_doc->loan_document_1) }}"
                                    width="100px" height="100px" alt="" title="">
                                @endif
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_document_16')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="">Loan Document 2</label>
                                <input name="loan_document_17" id="" placeholder="For Example : "
                                       value="{{ old('loan_document_17') }}" type="file"
                                       class="form-control">
                                @if($guarantor_c_loan_doc->loan_document_2)
                                <img
                                    src="{{ asset('public/storage/loan_document_photos/'.$guarantor_c_loan_doc->loan_document_2) }}"
                                    width="100px" height="100px" alt="" title="">
                                @endif
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_document_17')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="">Loan Document 3</label>
                                <input name="loan_document_18" id="" placeholder="For Example : "
                                       value="{{ old('loan_document_18') }}" type="file"
                                       class="form-control">
                                @if($guarantor_c_loan_doc->loan_document_3)
                                <img
                                    src="{{ asset('public/storage/loan_document_photos/'.$guarantor_c_loan_doc->loan_document_3) }}"
                                    width="100px" height="100px" alt="" title="">
                                @endif
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_document_18')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="">Loan Document 4</label>
                                <input name="loan_document_19" id="" placeholder="For Example : "
                                       value="{{ old('loan_document_19') }}" type="file"
                                       class="form-control">
                                @if($guarantor_c_loan_doc->loan_document_4)
                                <img
                                    src="{{ asset('public/storage/loan_document_photos/'.$guarantor_c_loan_doc->loan_document_4) }}"
                                    width="100px" height="100px" alt="" title="">
                                @endif
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_document_19')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label for="" class="">Loan Document 5</label>
                                <input name="loan_document_20" id="" placeholder="For Example : "
                                       value="{{ old('loan_document_20') }}" type="file"
                                       class="form-control">
                                @if($guarantor_c_loan_doc->loan_document_5)
                                <img
                                    src="{{ asset('public/storage/loan_document_photos/'.$guarantor_c_loan_doc->loan_document_5) }}"
                                    width="100px" height="100px" alt="" title="">
                                @endif
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_document_20')}}
                                </div>
                            </div>
                        </div>
                    <!-- <div class="col-md-4">
                    <div class="position-relative form-group">
                    <label for="" class="">Status</label>
                        <select type="select" id="" name="status" class="custom-select">
                        <option value="">Choose Status</option>
                        <option {{ old('status') == 'none' ? 'selected' : '' }}>None</option>
                        <option {{ old('status') == 'client' ? 'selected' : '' }}>Client</option>
                        <option {{ old('status') == 'Guarantor' ? 'selected' : '' }}>Guarantor</option>
                    </select>
                    <div class="text-danger form-control-feedback">
                        {{$errors->first('status')}}
                        </div>
                    </div>
                </div> -->
                    </div>
                @endif


                </div>

                    <div class="tab-pane" id="tab-animated1-3" role="tabpanel">
                    <div class="card-body">
                        <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12">
                                <table style="width: 100%;" id="myTable" class="table table-hover table-striped table-bordered dataTable dtr-inline" role="grid" aria-describedby="example_info">
                                    <thead>
                                    <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 253.2px;" aria-sort="ascending" aria-label="Name: activate to sort column descending">No</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 385.2px;" aria-label="Position: activate to sort column ascending">Date</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 187.2px;" aria-label="Office: activate to sort column ascending">Interest</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 106.2px;" aria-label="Age: activate to sort column ascending">Principal</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 192.2px;" aria-label="Start date: activate to sort column ascending">Total</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 148.2px;" aria-label="Salary: activate to sort column ascending">Balance</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 148.2px;" aria-label="Salary: activate to sort column ascending">Balance</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 148.2px;" aria-label="Salary: activate to sort column ascending">Balance</th></tr>

                                    </thead>
                                    <tbody id="myTableBody">
                                    @foreach($schedules as $key => $schedule)
                                    <tr role="row" class="odd">
                                        <td id="" class="sorting_1 dtr-control">{{ $key+1 }}</td>
                                        <td id=""> {{ $schedule->month }}</td>
                                        <td id=""> {{ $schedule->capital }}</td>
                                        <td id=""> {{ $schedule->interest }}</td>
                                        <td id=""> {{ $schedule->discount }}</td>
                                        <td id=""> {{ $schedule->refund_interest }}</td>
                                        <td id=""> {{ $schedule->refund_amount }}</td>
                                        <td id=""> {{ $schedule->final_amount }}</td>
                                    </tr>
                                    @endforeach
                                   </tbody>
                                   <tfoot>
                                    <tr role="row" class="odd">
                                        <td colspan="2" id="" class="sorting_1 dtr-control">Total</td>
                                        <td id=""> {{ $total_capital }}</td>
                                        <td id=""> {{ $total_interest }}</td>
                                        <td id=""> {{ $total_final_amount }}</td>
                                    </tr>
                                    </tfoot>
                                </table>
                                </div>
                            </div>
                                <!-- <div class="row">
                                        <div class="col-sm-12 col-md-5">
                                            <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entrie
                                                s</div>
                                            </div>
                                            <div class="col-sm-12 col-md-7">
                                                <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                                    <ul class="pagination">
                                                        <li class="paginate_button page-item previous disabled" id="example_previous">
                                                            <a href="#" aria-controls="example" data-dt-idx="0" tabindex="0" class="page-link">Previous</a>
                                                        </li>
                                                        <li class="paginate_button page-item active">
                                                            <a href="#" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">1</a>
                                                        </li>
                                                        <li class="paginate_button page-item ">
                                                            <a href="#" aria-controls="example" data-dt-idx="2" tabindex="0" class="page-link">2</a>
                                                        </li>
                                                        <li class="paginate_button page-item ">
                                                            <a href="#" aria-controls="example" data-dt-idx="3" tabindex="0" class="page-link">3</a>
                                                        </li>
                                                        <li class="paginate_button page-item ">
                                                            <a href="#" aria-controls="example" data-dt-idx="4" tabindex="0" class="page-link">4</a>
                                                        </li>
                                                        <li class="paginate_button page-item ">
                                                            <a href="#" aria-controls="example" data-dt-idx="5" tabindex="0" class="page-link">5</a>
                                                        </li>
                                                        <li class="paginate_button page-item ">
                                                            <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0" class="page-link">6</a>
                                                        </li>
                                                        <li class="paginate_button page-item next" id="example_next">
                                                            <a href="#" aria-controls="example" data-dt-idx="7" tabindex="0" class="page-link">Next</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                </div> -->
                        </div>
                    </div>
                </div>
                                        <button type="submit" class="mt-1 mb-2 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                <a href="{{url('loan/')}}" class="btn btn-secondary mb-2 mt-1">
                    <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                </a>
                                    </form>
                                </div>
                                </div>


                                @endsection
