@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Loan Create</div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{url('loan/')}}" class="btn theme-color text-light">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <form class="needs-validation" method="POST" action="{{route('loan.store')}}" enctype="multipart/form-data" id='loanCreateForm'>
            @csrf
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <ul class="tabs-animated-shadow nav-justified tabs-animated nav">
                        <li class="nav-item">
                            <a role="tab" class="nav-link active" id="tab-c1-0" data-toggle="tab"
                               href="#tab-animated1-0" aria-selected="true">
                                <span class="nav-text">Client</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-c1-1" data-toggle="tab" href="#tab-animated1-1"
                               aria-selected="false">
                                <span class="nav-text">Account</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-c1-2" data-toggle="tab" href="#tab-animated1-2"
                               aria-selected="false">
                                <span class="nav-text">Documents</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-c1-3" data-toggle="tab" href="#tab-animated1-3"
                               aria-selected="false">
                                <span class="nav-text">Payment Schedule</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-animated1-0" role="tabpanel">
                            <br>

                            <div class="form-row justify-content-between align-items-end">
                                <div class="col-12 col-lg-3">
                                    <div class="position-relative form-group mb-0">
                                        <label for="" style="font-weight: 600">Client</label>
                                        <input name="client_name" id="client_name" placeholder="For Example : Mya Mya"
                                               value="{{ old('client_name') }}" type="search" class="form-control"
                                               >
                                        <input type="hidden" name="client_id" id="client_id_hidden" value="">
                                        <div id="client_list">
                                        </div>
                                        <select type="select" id="client_id" name="client_id" class="custom-select"
                                                >
                                            <option value="" selected disabled>Choose Client</option>
                                            @foreach ($clients as $client)
                                        		@php
                                        			$mainID = $client->main_client_code != NULL ? $client->main_client_code : $client->client_uniquekey;
                                        		@endphp
                                                <option value="{{ $client->client_uniquekey }}">{{ $mainID .' / '. $client->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('client_id')}}
                                        </div>
                                    </div>
                                </div>

                                {{--                                client card start--}}
                                <div class="col-12 col-md-9">
                                    <div class="row">
                                        <div class="col-12 col-lg-8">
                                            <div class="card user-card" id="clientCard">
                                                <div class="card-body">
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <img class="img-fluid rounded img-radius"
                                                             id="clientPhoto"
                                                             src=""
                                                             style="width: 70px">
                                                    </div>
                                                    <div class="ml-3 text-nowrap">
                                                        <p id="clientID"></p>
                                                        <p id="clientName"></p>
                                                        <p id="clientNrc" class="mb-0"></p>
                                                    </div>
                                					<div class="text-nowrap ml-3">
                                                        <p id="clientPhone"></p>
                                                        <p id="clientFather"></p>
                                                        <p id="DOB" class="mb-0"></p>
                                					</div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                {{--                                client card end--}}

                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        {{-- <label for="" style="font-weight: 600">Client NRC</label> --}}
                                        <input name="client_nrc" id="client_nrc" placeholder=""
                                               value="{{ old('client_nrc') }}" type="hidden" class="form-control"
                                               disabled>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        {{-- <label for="" style="font-weight: 600">Client Name</label> --}}
                                        <input name="solo_client_name" id="solo_client_name" placeholder=""
                                               value="{{ old('solo_client_name') }}" type="hidden" class="form-control"
                                               disabled required>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('solo_client_name')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        {{-- <label for="" style="font-weight: 600">Client Phone</label> --}}
                                        <input name="client_phone" id="client_phone" placeholder=""
                                               value="{{ old('client_phone') }}" type="hidden" class="form-control"
                                               disabled>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('client_phone')}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">

                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Saving Amount</label>
                                        <input name="saving_amount" id="saving_amount" placeholder="0"
                                               value="{{ old('saving_amount') }}" type="text" class="form-control"
                                               disabled>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('saving_amount')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Choose guarantors</label>
                                        <select type="select" id="guarantor_select" name="guarantor_select"
                                                class="custom-select">
                                            <option value="" selected disabled>Choose Guarantor</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('saving_amount')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Business Type</label>
                                        <select type="select" id="business_type_id" name="business_type_id" class="form-control">
                                            <option value="" selected disabled>Choose Business Type</option>
                                            @foreach ($business_types as $business_type)
                                                <option value="{{ $business_type->id}}" {{(old("business_type_id")== $business_type->business_type_name ? "selected":"")}}>{{ $business_type->business_type_name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('business_type_id')}}
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Business Category</label>
                                        <select type="select" id="business_category_id" name="business_category_id" class="form-control">
                                            <option></option>
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('business_category_id')}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row align-items-end" id="guarantorA">
                                <div class="col-md-3">
                                    <div class="position-relative form-group mb-0">
                                        <label for="" style="font-weight: 600">Guarantor A</label>
                                        <input name="guarantor_name_a" id="guarantor_name_a"
                                               placeholder="For Example : Mya Mya" value="{{ old('guarantor_name_a') }}"
                                               type="search" class="form-control">
                                        <input type="hidden" name="guarantor_a" id="guarantor_a_hidden" value="">
                                        <div id="guarantor_list_a">
                                        </div>
                                        <select type="select" id="guarantor_a" name="guarantor_a" class="custom-select">
                                            <option value="">Choose Guarantor A</option>
                                            @foreach ($guarantors as $guarantor)
                                                <option value="{{ $guarantor->guarantor_uniquekey}}">{{ $guarantor->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('guarantor_a')}}
                                        </div>
                                    </div>
                                </div>

                                        {{--                                guarantor a card start--}}
                                <div class="col-12 col-md-9">
                                    <div class="row align-items-end">
                                        <div class="col-12 col-lg-8">
                                            <div class="card user-card" id="guarantorACard">
                                                <div class="card-body">
                                                <div class="d-flex align-items-start">
                                                    <div>
                                                        <img class="img-fluid rounded img-radius"
                                                             id="guarantorAPhoto"
                                                             src=""
                                                             style="width: 70px">
                                                    </div>
                                                    <div class="ml-3 text-nowrap">
                                                        <p id="guarantorAID"></p>
                                                        <p id="guarantorAName"></p>
                                                        <p id="guarantorANrc" class="mb-0"></p>
                                                    </div>
                                					<div class="ml-3 text-nowrap">
                                                        <p id="guarantorAPhone"></p>
                                                        <p id="guarantorADOB" class="mb-0"></p>
                                					</div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                {{--                                guarantor a card end--}}

                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        {{-- <label for="" style="font-weight: 600">Guarantor NRC</label> --}}
                                        <input name="guarantor_a_nrc" id="guarantor_a_nrc" placeholder=""
                                               value="{{ old('guarantor_a_nrc') }}" type="hidden" class="form-control"
                                               disabled>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('guarantor_a_nrc')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        {{-- <label for="" style="font-weight: 600">Guarantor Name</label> --}}
                                        <input name="guarantor_a_name" id="guarantor_a_name" placeholder=""
                                               value="{{ old('guarantor_a_name') }}" type="hidden" class="form-control"
                                               disabled>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('guarantor_a_name')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        {{-- <label for="" style="font-weight: 600">Guarantor ID</label> --}}
                                        <input name="guarantor_a_id" id="guarantor_a_id" placeholder=""
                                               value="{{ old('guarantor_a_id') }}" type="hidden" class="form-control"
                                               disabled>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('guarantor_a_id')}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row align-items-end" id="guarantorB">
                                <div class="col-md-3">
                                    <div class="position-relative form-group mb-0">
                                        <label for="" style="font-weight: 600">Guarantor B</label>
                                        <input name="guarantor_name_b" id="guarantor_name_b"
                                               placeholder="For Example : Mya Mya" value="{{ old('guarantor_name_b') }}"
                                               type="search" class="form-control">
                                        <input type="hidden" name="guarantor_b" id="guarantor_b_hidden" value="">
                                        <div id="guarantor_list_b">
                                        </div>
                                        <select type="select" id="guarantor_b" name="guarantor_b" class="custom-select"
                                        >
                                            <option value="">Choose Guarantor B</option>
                                            @foreach ($guarantors as $guarantor)
                                                <option value="{{ $guarantor->guarantor_uniquekey}}">{{ $guarantor->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('guarantor_b')}}
                                        </div>
                                    </div>
                                </div>

                                        {{--                                guarantor b card start--}}
                                <div class="col-12 col-md-9">
                                    <div class="row align-items-end">
                                        <div class="col-12 col-lg-8">
                                            <div class="card user-card" id="guarantorBCard">
                                                <div class="card-body">
                                                <div class="d-flex">
                                                    <div>
                                                        <img class="img-fluid rounded img-radius"
                                                             id="guarantorBPhoto"
                                                             src=""
                                                             style="width: 70px">
                                                    </div>
                                                    <div class="ml-3 text-nowrap">
                                                        <p id="guarantorBID"></p>
                                                        <p id="guarantorBName"></p>
                                                        <p id="guarantorBNrc" class="mb-0"></p>
                                                    </div>
                                					<div class="ml-3 text-nowrap">
                                                        <p id="guarantorBPhone"></p>
                                                        <p id="guarantorBDOB" class="mb-0"></p>
                                					</div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                {{--                                guarantor b card end--}}

                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        {{-- <label for="" style="font-weight: 600">Guarantor NRC</label> --}}
                                        <input name="guarantor_b_nrc" id="guarantor_b_nrc" placeholder=""
                                               value="{{ old('guarantor_b_nrc') }}" type="hidden" class="form-control"
                                               disabled>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('guarantor_b_nrc')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        {{-- <label for="" style="font-weight: 600">Guarantor Name</label> --}}
                                        <input name="guarantor_b_name" id="guarantor_b_name" placeholder=""
                                               value="{{ old('guarantor_b_name') }}" type="hidden" class="form-control"
                                               disabled>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('guarantor_b_name')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        {{-- <label for="" style="font-weight: 600">Guarantor ID</label> --}}
                                        <input name="guarantor_b_id" id="guarantor_b_id" placeholder=""
                                               value="{{ old('guarantor_b_id') }}" type="hidden" class="form-control"
                                               disabled>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('guarantor_b_id')}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row align-items-end" id="guarantorC">
                                <div class="col-md-3">
                                    <div class="position-relative form-group mb-0">
                                        <label for="" style="font-weight: 600">Guarantor C</label>
                                        <input name="guarantor_name_c" id="guarantor_name_c"
                                               placeholder="For Example : Mya Mya" value="{{ old('guarantor_name_c') }}"
                                               type="search" class="form-control">
                                        <input type="hidden" name="guarantor_c" id="guarantor_c_hidden" value="">
                                        <div id="guarantor_list_c">
                                        </div>
                                        <select type="select" id="guarantor_c" name="guarantor_c" class="custom-select"
                                        >
                                            <option value="">Choose Guarantor C</option>
                                            @foreach ($guarantors as $guarantor)
                                                <option value="{{ $guarantor->guarantor_uniquekey}}">{{ $guarantor->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('guarantor_c')}}
                                        </div>
                                    </div>
                                </div>

                                        {{--                                guarantor c card start--}}
                                <div class="col-12 col-md-9">
                                    <div class="row align-items-end">
                                        <div class="col-12 col-lg-8">
                                            <div class="card user-card" id="guarantorCCard">
                                                <div class="card-body">
                                                <div class="d-flex">
                                                    <div>
                                                        <img class="img-fluid rounded img-radius"
                                                             id="guarantorCPhoto"
                                                             src=""
                                                             style="width: 70px">
                                                    </div>
                                                    <div class="ml-3 text-nowrap">
                                                        <p id="guarantorCID"></p>
                                                        <p id="guarantorCName"></p>
                                                        <p id="guarantorCNrc" class="mb-0"></p>
                                                    </div>
                                					<div class="ml-3 text-nowrap">
                                                        <p id="guarantorCPhone"></p>
                                                        <p id="guarantorCDOB" class="mb-0"></p>
                                					</div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                {{--                                guarantor c card end--}}

                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        {{-- <label for="" style="font-weight: 600">Guarantor NRC</label> --}}
                                        <input name="guarantor_c_nrc" id="guarantor_c_nrc" placeholder=""
                                               value="{{ old('guarantor_c_nrc') }}" type="hidden" class="form-control"
                                               disabled>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('guarantor_c_nrc')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        {{-- <label for="" style="font-weight: 600">Guarantor Name</label> --}}
                                        <input name="guarantor_c_name" id="guarantor_c_name" placeholder=""
                                               value="{{ old('guarantor_c_name') }}" type="hidden" class="form-control"
                                               disabled>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('guarantor_c_name')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        {{-- <label for="" style="font-weight: 600">Guarantor ID</label> --}}
                                        <input name="guarantor_c_id" id="guarantor_c_id" placeholder=""
                                               value="{{ old('guarantor_c_id') }}" type="hidden" class="form-control"
                                               disabled>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('guarantor_c_id')}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="tab-pane" id="tab-animated1-1" role="tabpanel">
                            <br>
                            <div class="form-row">

                                {{-- <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Loan Unique ID</label>
                                        <input name="loan_unique_id" id="" placeholder="For Example : LG-000001"
                                               value="{{ $loanuniquekey }}" type="text" class="form-control" required>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('loan_unique_id')}}
                                        </div>
                                    </div>
                                </div> --}}


                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Branch</label>
                                        <select type="select" id="client_branch" name="branch_id" class="custom-select" 
                                                required>
                                            // <option value="">Choose Branch</option>
                                            @foreach ($branches as $branch)
                                                <option value="{{ $branch->id }}">{{ $branch->branch_name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('branch_id')}}
                                        </div>
                                    </div>
                                </div>

                                 <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Center <span style="color: red">*</span></label>
                                        <select type="select" id="client_center" name="center_id" class="custom-select" required >
                                            <option value="">Choose Center</option>
                                            @foreach ($centers as $center)
                                                <option
                                                    value="{{ $center->center_uniquekey }}">{{ $center->main_center_code != NULL ? $center->main_center_code : $center->center_uniquekey  }}</option>
                                            @endforeach
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('center_id')}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Group <span style="color: red">*</span></label>
                                        <select type="select" id="client_group" name="group_id" class="custom-select" required >
                                            <option value="">Choose Group</option>
                                        {{--@foreach ($groups as $group)
                                                 <option value="{{ $group->group_uniquekey }}">{{ $group->main_group_code != NULL ? $group->main_group_code : $group->group_uniquekey }}</option>
                                             @endforeach--}}
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('group_id')}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Loan Officer</label>
                                        <select type="select" id="loan_officer" name="loan_officer_id" class="custom-select"
                                                required >
                                            <option value="">Choose Loan Officer</option>
                                            @foreach ($staffs as $staff)
                                                <option value="{{ $staff->staff_code }}">{{ $staff->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('loan_officer_id')}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Loan Type <span style="color: red">*</span></label>
                                        <select type="select" id="loan_type" name="loan_type_id" class="custom-select"
                                                required>
                                            <option value="">Select Loan Type</option>
                                            @foreach ($loantypes as $loantype)
                                                <option value="{{ $loantype->id }}">{{ $loantype->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('loan_type_id')}}
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Loan Amount</label>
                                        <input name="loan_amount" id="loan_amount" placeholder="For Example : 200000"
                                               value="{{ old('loan_amount') }}" type="number" class="form-control"
                                               required>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('loan_amount')}}
                                        </div>
                                    </div>
                                </div>
                                        
                                <div class="col-md-4 d-none">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Principal Formula</label>
                                        <input name="principal_formula" id="principal_formula" placeholder="For Example : 0.083"
                                               value="{{ old('principal_formula') }}" type="text" class="form-control"
                                               >
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('principal_formula')}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 d-none">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Estimate Receivable Amount</label>
                                        <input name="estimate_receivable_amount" id=""
                                               placeholder="For Example : 100000"
                                               value="{{ old('estimate_receivable_amount') }}" type="number"
                                               class="form-control">
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('estimate_receivable_amount')}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <div class="position-relative form-group">
                                                    <label for="" style="font-weight: 600">Loan Term Value</label>
                                                    <input name="loan_term_value" id="loan_term_value"
                                                           placeholder="For Example : "
                                                           value="{{ old('loan_term_value') }}" type="number"
                                                           class="form-control" required readonly>
                                                    <div class="text-danger form-control-feedback">
                                                        {{$errors->first('loan_term_value')}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="position-relative form-group">
                                                    <label for="" style="font-weight: 600">Interest Rate</label>
                                                    <input name="interest_rate" id="interest_rate"
                                                           placeholder="For Example : "
                                                           value="{{ old('interest_rate') }}" type="number" step="any"
                                                           class="form-control" readonly>
                                                    <div class="text-danger form-control-feedback">
                                                        {{$errors->first('loan_term_value')}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Loan Term</label>
                                        <select type="select" id="loan_term" name="loan_term" class="custom-select"
                                                required readonly>
                                            <option value="">Choose Loan Term</option>
                                            <option {{ old('loan_term') == 'none' ? 'selected' : '' }}>None</option>
                                            <option {{ old('loan_term') == 'Day' ? 'selected' : '' }}>Day</option>
                                            <option {{ old('loan_term') == 'Week' ? 'selected' : '' }}>Week</option>
                                            <option {{ old('loan_term') == 'Two-Weeks' ? 'selected' : '' }}>Two-Weeks
                                            </option>
                                            <option {{ old('loan_term') == 'Four-Weeks' ? 'selected' : '' }}>Four-Weeks
                                            </option>
                                            <option {{ old('loan_term') == 'Month' ? 'selected' : '' }}>Month</option>
                                            <option {{ old('loan_term') == 'Year' ? 'selected' : '' }}>Year</option>
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('loan_term')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Interest Rate Period</label>
                                        <select type="select" id="interest_rate_period" name="interest_rate_period"
                                                class="custom-select" required readonly>
                                            <option value="">Choose Interest Rate Period</option>
                                            <option {{ old('interest_rate_period') == 'none' ? 'selected' : '' }}>None
                                            </option>
                                            <option {{ old('interest_rate_period') == 'Daily' ? 'selected' : '' }}>
                                                Daily
                                            </option>
                                            <option {{ old('interest_rate_period') == 'Weekly' ? 'selected' : '' }}>
                                                Weekly
                                            </option>
                                            <option {{ old('interest_rate_period') == 'Two-Weeks' ? 'selected' : '' }}>
                                                Two-Weeks
                                            </option>
                                            <option {{ old('interest_rate_period') == 'Monthly' ? 'selected' : '' }}>
                                                Monthly
                                            </option>
                                            <option {{ old('interest_rate_period') == 'Yearly' ? 'selected' : '' }}>
                                                Yearly
                                            </option>
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('interest_rate_period')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Disbursement Status</label>
                                        <select type="select" id="disbursement_status" name="disbursement_status"
                                                class="custom-select" required readonly>
                                            <option value="">Choose Disbursement Status</option>
                                            <option {{ old('disbursement_status') == 'none' ? 'selected' : '' }}>None
                                            </option>
                                            <option {{ old('disbursement_status') == 'Pending' ? 'selected' : '' }}>
                                                Pending
                                            </option>
                                            <option {{ old('disbursement_status') == 'Approved' ? 'selected' : '' }}>
                                                Approved
                                            </option>
                                        	<option {{ old('disbursement_status') == 'Deposited' ? 'selected' : '' }}>
                                                Deposited
                                            </option>
                                            <option {{ old('disbursement_status') == 'Declined' ? 'selected' : '' }}>
                                                Declined
                                            </option>
                                            <option {{ old('disbursement_status') == 'Withdrawn' ? 'selected' : '' }}>
                                                Withdrawn
                                            </option>
                                            <option {{ old('disbursement_status') == 'Written-Off' ? 'selected' : '' }}>
                                                Written-Off
                                            </option>
                                            <option {{ old('disbursement_status') == 'Closed' ? 'selected' : '' }}>
                                                Closed
                                            </option>
                                            <option {{ old('disbursement_status') == 'Activated' ? 'selected' : '' }}>
                                                Activated
                                            </option>
                                            <option {{ old('disbursement_status') == 'Canceled' ? 'selected' : '' }}>
                                                Canceled
                                            </option>
                                        </select>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('disbursement_status')}}
                                        </div>
                                    </div>
                                </div>
                                        
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Interest Method</label>
                                        <input name="interest_method" id="interest_method" placeholder="For Example : "
                                               value="{{ old('interest_method') }}" type="text" class="form-control"
                                               required readonly>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('interest_method')}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Remark</label>
                                        <input name="remark" id="remark" placeholder="For Example : "
                                               value="{{ old('remark') }}" type="text" class="form-control">
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('remark')}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Loan Application Date<span style="color: red"> *</span></label>
                                        <input name="loan_application_date" id="loan_application_date" placeholder=""
                                               value="{{ $loan_application_date }}" type="date"
                                               class="form-control" required >
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('loan_application_date')}}
                                        </div>
                                    </div>
                                </div>
                                        
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">First Installment Date<span style="color: red"> *</span></label>
                                        <input name="first_installment_date" id="first_installment_date" placeholder=""
                                               value="{{ $first_installment_date }}" type="date"
                                               class="form-control" required>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('first_installment_date')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 d-none">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Disbursement Date <span style="color: red">*</span></label>
                                        <input name="disbursement_date" id="dis_date" placeholder=""
                                               value="{{ old('disbursement_date') }}" type="date"
                                               class="form-control">
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('disbursement_date')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4" hidden>
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Cancel Date</label>
                                        <input name="cancel_date" id="" placeholder="" value="{{ old('cancel_date') }}"
                                               type="datetime" class="form-control" disabled>
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('cancel_date')}}
                                        </div>
                                    </div>
                                </div>
                            </div>

{{--                            charges and saving start--}}
                            <div id="chargeAndCompulsory">
                                <h5 class="mb-4">Charge</h5>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Service Name</th>
                                        <th>Service Amount</th>
                                        <th>Charge Option</th>
                                        <th>Charge Type</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody id="chargeTbody">

                                    </tbody>
                                </table>
{{--                            charges and saving end--}}

{{--                            compulsory saving start--}}
                                <h5 class="mb-4">Compulsory Saving</h5>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Saving Name</th>
                                        <th>Saving Amount</th>
                                        <th>Charge Option</th>
                                        <th>Monthly Interest</th>
                                        <th>Product Type</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody id="compulsoryTbody">


                                    </tbody>
                                </table>
                            </div>
{{--                            compulsory saving end--}}


                        </div>

                        <div class="tab-pane" id="tab-animated1-2" role="tabpanel">
                            <br>
                            <h5 class="menu-header-title text-capitalize mb-4">Client Documents</h5>
                            <div class="form-row">
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Loan Document 1</label>
                                        <input name="loan_document_1" id="" placeholder="For Example : LG-000001"
                                               value="{{ old('loan_document_1') }}" type="file" class="form-control">
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('loan_document_1')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Loan Document 2</label>
                                        <input name="loan_document_2" id="" placeholder="For Example : "
                                               value="{{ old('loan_document_2') }}" type="file" class="form-control">
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('loan_document_2')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Loan Document 3</label>
                                        <input name="loan_document_3" id="" placeholder="For Example : "
                                               value="{{ old('loan_document_3') }}" type="file" class="form-control">
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('loan_document_3')}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Loan Document 4</label>
                                        <input name="loan_document_4" id="" placeholder="For Example : "
                                               value="{{ old('loan_document_4') }}" type="file" class="form-control">
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('loan_document_4')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" style="font-weight: 600">Loan Document 5</label>
                                        <input name="loan_document_5" id="" placeholder="For Example : "
                                               value="{{ old('loan_document_5') }}" type="file" class="form-control">
                                        <div class="text-danger form-control-feedback">
                                            {{$errors->first('loan_document_5')}}
                                        </div>
                                    </div>
                                </div>
                            <!-- <div class="col-md-4">
                                <div class="position-relative form-group">
                                <label for="" style="font-weight: 600">Status</label>
                                    <select type="select" id="" name="status" class="custom-select">
                                    <option value="">Choose Status</option>
                                    <option {{ old('status') == 'none' ? 'selected' : '' }}>None</option>
                                    <option {{ old('status') == 'client' ? 'selected' : '' }}>Client</option>
                                    <option {{ old('status') == 'Guarantor' ? 'selected' : '' }}>Guarantor</option>
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('status')}}
                                </div>
                            </div>
                        </div> -->
                            </div>

                            <br>
                            <div id="guarantorADoc">
                                <h5 class="menu-header-title text-capitalize mb-4">Guarantor A Documents</h5>
                                <div class="form-row">
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Document 1</label>
                                            <input name="loan_document_6" id="" placeholder="For Example : LG-000001"
                                                   value="{{ old('loan_document_6') }}" type="file"
                                                   class="form-control">
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_document_6')}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Document 2</label>
                                            <input name="loan_document_7" id="" placeholder="For Example : "
                                                   value="{{ old('loan_document_7') }}" type="file"
                                                   class="form-control">
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_document_7')}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Document 3</label>
                                            <input name="loan_document_8" id="" placeholder="For Example : "
                                                   value="{{ old('loan_document_8') }}" type="file"
                                                   class="form-control">
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_document_8')}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Document 4</label>
                                            <input name="loan_document_9" id="" placeholder="For Example : "
                                                   value="{{ old('loan_document_9') }}" type="file"
                                                   class="form-control">
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_document_9')}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Document 5</label>
                                            <input name="loan_document_10" id="" placeholder="For Example : "
                                                   value="{{ old('loan_document_10') }}" type="file"
                                                   class="form-control">
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_document_10')}}
                                            </div>
                                        </div>
                                    </div>
                                <!-- <div class="col-md-4">
                        <div class="position-relative form-group">
                        <label for="" style="font-weight: 600">Status</label>
                            <select type="select" id="" name="status" class="custom-select">
                            <option value="">Choose Status</option>
                            <option {{ old('status') == 'none' ? 'selected' : '' }}>None</option>
                            <option {{ old('status') == 'client' ? 'selected' : '' }}>Client</option>
                            <option {{ old('status') == 'Guarantor' ? 'selected' : '' }}>Guarantor</option>
                        </select>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('status')}}
                                    </div>
                                </div>
                            </div> -->
                                </div>
                            </div>

                            <br>
                            <div id="guarantorBDoc">
                                <h5 class="menu-header-title text-capitalize mb-4">Guarantor B Documents</h5>
                                <div class="form-row">
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Document 1</label>
                                            <input name="loan_document_11" id="" placeholder="For Example : LG-000001"
                                                   value="{{ old('loan_document_11') }}" type="file"
                                                   class="form-control">
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_document_11')}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Document 2</label>
                                            <input name="loan_document_12" id="" placeholder="For Example : "
                                                   value="{{ old('loan_document_12') }}" type="file"
                                                   class="form-control">
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_document_12')}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Document 3</label>
                                            <input name="loan_document_13" id="" placeholder="For Example : "
                                                   value="{{ old('loan_document_13') }}" type="file"
                                                   class="form-control">
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_document_13')}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Document 4</label>
                                            <input name="loan_document_14" id="" placeholder="For Example : "
                                                   value="{{ old('loan_document_14') }}" type="file"
                                                   class="form-control">
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_document_14')}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Document 5</label>
                                            <input name="loan_document_15" id="" placeholder="For Example : "
                                                   value="{{ old('loan_document_15') }}" type="file"
                                                   class="form-control">
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_document_15')}}
                                            </div>
                                        </div>
                                    </div>
                                <!-- <div class="col-md-4">
                        <div class="position-relative form-group">
                        <label for="" style="font-weight: 600">Status</label>
                            <select type="select" id="" name="status" class="custom-select">
                            <option value="">Choose Status</option>
                            <option {{ old('status') == 'none' ? 'selected' : '' }}>None</option>
                            <option {{ old('status') == 'client' ? 'selected' : '' }}>Client</option>
                            <option {{ old('status') == 'Guarantor' ? 'selected' : '' }}>Guarantor</option>
                        </select>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('status')}}
                                    </div>
                                </div>
                            </div> -->
                                </div>
                            </div>

                            <br>

                            <div id="guarantorCDoc">
                                <h5 class="menu-header-title text-capitalize mb-4">Guarantor C Documents</h5>
                                <div class="form-row">
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Document 1</label>
                                            <input name="loan_document_16" id="" placeholder="For Example : LG-000001"
                                                   value="{{ old('loan_document_16') }}" type="file"
                                                   class="form-control">
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_document_16')}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Document 2</label>
                                            <input name="loan_document_17" id="" placeholder="For Example : "
                                                   value="{{ old('loan_document_17') }}" type="file"
                                                   class="form-control">
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_document_17')}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Document 3</label>
                                            <input name="loan_document_18" id="" placeholder="For Example : "
                                                   value="{{ old('loan_document_18') }}" type="file"
                                                   class="form-control">
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_document_18')}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Document 4</label>
                                            <input name="loan_document_19" id="" placeholder="For Example : "
                                                   value="{{ old('loan_document_19') }}" type="file"
                                                   class="form-control">
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_document_19')}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" style="font-weight: 600">Loan Document 5</label>
                                            <input name="loan_document_20" id="" placeholder="For Example : "
                                                   value="{{ old('loan_document_20') }}" type="file"
                                                   class="form-control">
                                            <div class="text-danger form-control-feedback">
                                                {{$errors->first('loan_document_20')}}
                                            </div>
                                        </div>
                                    </div>
                                <!-- <div class="col-md-4">
                        <div class="position-relative form-group">
                        <label for="" style="font-weight: 600">Status</label>
                            <select type="select" id="" name="status" class="custom-select">
                            <option value="">Choose Status</option>
                            <option {{ old('status') == 'none' ? 'selected' : '' }}>None</option>
                            <option {{ old('status') == 'client' ? 'selected' : '' }}>Client</option>
                            <option {{ old('status') == 'Guarantor' ? 'selected' : '' }}>Guarantor</option>
                        </select>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('status')}}
                                    </div>
                                </div>
                            </div> -->
                                </div>
                            </div>


                        </div>

                        <div class="tab-pane" id="tab-animated1-3" role="tabpanel">
                            <div class="card-body">
                                <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table style="width: 100%;" id="myTable"
                                                   class="table table-hover table-striped table-bordered dataTable dtr-inline"
                                                   role="grid" aria-describedby="example_info">
                                                <thead>
                                                <tr role="row">
                                                    <th class="sorting_asc" tabindex="0" aria-controls="example"
                                                        rowspan="1" colspan="1" style="width: 253.2px;"
                                                        aria-sort="ascending"
                                                        aria-label="Name: activate to sort column descending">No
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                                        colspan="1" style="width: 385.2px;"
                                                        aria-label="Position: activate to sort column ascending">Date
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                                        colspan="1" style="width: 187.2px;"
                                                        aria-label="Office: activate to sort column ascending">Principal
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                                        colspan="1" style="width: 106.2px;"
                                                        aria-label="Age: activate to sort column ascending">Interest
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                                        colspan="1" style="width: 192.2px;"
                                                        aria-label="Start date: activate to sort column ascending">Total
                                                    </th>
                                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1"
                                                        colspan="1" style="width: 148.2px;"
                                                        aria-label="Salary: activate to sort column ascending">Balance
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody id="myTableBody">
                                                <tr role="row" class="odd">
                                                    <td id="" class="sorting_1 dtr-control"></td>
                                                    <td id=""></td>
                                                    <td id=""></td>
                                                    <td id=""></td>
                                                    <td id=""></td>
                                                    <td id=""></td>
                                                </tr>
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>
                                    <!-- <div class="row">
                                            <div class="col-sm-12 col-md-5">
                                                <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entrie
                                                    s</div>
                                                </div>
                                                <div class="col-sm-12 col-md-7">
                                                    <div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
                                                        <ul class="pagination">
                                                            <li class="paginate_button page-item previous disabled" id="example_previous">
                                                                <a href="#" aria-controls="example" data-dt-idx="0" tabindex="0" class="page-link">Previous</a>
                                                            </li>
                                                            <li class="paginate_button page-item active">
                                                                <a href="#" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">1</a>
                                                            </li>
                                                            <li class="paginate_button page-item ">
                                                                <a href="#" aria-controls="example" data-dt-idx="2" tabindex="0" class="page-link">2</a>
                                                            </li>
                                                            <li class="paginate_button page-item ">
                                                                <a href="#" aria-controls="example" data-dt-idx="3" tabindex="0" class="page-link">3</a>
                                                            </li>
                                                            <li class="paginate_button page-item ">
                                                                <a href="#" aria-controls="example" data-dt-idx="4" tabindex="0" class="page-link">4</a>
                                                            </li>
                                                            <li class="paginate_button page-item ">
                                                                <a href="#" aria-controls="example" data-dt-idx="5" tabindex="0" class="page-link">5</a>
                                                            </li>
                                                            <li class="paginate_button page-item ">
                                                                <a href="#" aria-controls="example" data-dt-idx="6" tabindex="0" class="page-link">6</a>
                                                            </li>
                                                            <li class="paginate_button page-item next" id="example_next">
                                                                <a href="#" aria-controls="example" data-dt-idx="7" tabindex="0" class="page-link">Next</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="mt-1 mb-2 btn btn-success" id='loanSubmit'><i
                                class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                        <a href="{{url('loan/')}}" class="btn btn-secondary mb-2 mt-1">
                            <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                        </a>
        </form>
    </div>
    </div>
@endsection
                                    
@if ($errors->any())
    @section('script')

    @php
    $errlist = "";
        foreach($errors->all() as $e) {
            $errlist .= $e;
        }
    @endphp
    <script>
        errorAlert("{{ $errlist }}");
    </script>
    @endsection
@endif

