@extends('layouts.app')
@section('content')

    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Loan Details View
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{url('loan/')}}" class="btn theme-color">
                            <i class="pe-7s-back btn-icon-wrapper text-light">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                <ul class="tabs-animated-shadow nav-justified tabs-animated nav">
                    <li class="nav-item">
                        <a role="tab" class="nav-link active" id="tab-c1-0" data-toggle="tab" href="#tab-animated1-0"
                           aria-selected="true">
                            <span class="nav-text">Loan Info</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a role="tab" class="nav-link" id="tab-c1-1" data-toggle="tab" href="#tab-animated1-1"
                           aria-selected="false">
                            <span class="nav-text">Loan Documents</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a role="tab" class="nav-link" id="tab-c1-2" data-toggle="tab" href="#tab-animated1-2"
                           aria-selected="false">
                            <span class="nav-text">Payment Schedule</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-animated1-0" role="tabpanel">

                        <hr>
                        <div class="row mb-4">
                <div class="col-md-6">
                    <div class="card rounded">
                        <div class="card-body">
                            <h5 class="card-title">Client Information</h5>
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="{{ asset('public/storage/clientphotos/'.($client_photo->client_photo != NULL ? $client_photo->client_photo : 'default_user_icon.jpg')) }}" class="rounded-circle" style="width: 70px; height:70px; object-fit:cover" alt="" />
                                </div>
                                <div class="col-md-5">
                                    <div class="form-row">
                                        <div class="col-md-3 text-left">
                                            <label class="font-weight-bold">Main CID</label>
                                        </div>
                                        <div class="col-md-9 ">
                                            : {{ $main_client_id }} 
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-3 text-left">
                                            <label class="font-weight-bold">Portal CID</label>
                                        </div>
                                        <div class="col-md-9 ">
                                            : {{ $loan->client_id }} 
                                        </div>
                                    </div>
                                    
                                    <div class="form-row">
                                        <div class="col-md-3 text-left">
                                            <label class="font-weight-bold">Name</label>
                                        </div>
                                        <div class="col-md-9 ">
                                            : {{ $loan->client_name }} 
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-3 text-left">
                                            <label class="font-weight-bold">NRC</label>
                                        </div>
                                        <div class="col-md-9 ">
                                            @if ($loan->client_nrc)
                                            : {{ $loan->client_nrc }} 
                                            @else
                                            : N/A
                                            @endif 
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-row">
                                        <div class="col-md-6 text-left">
                                            <label class="font-weight-bold">Father's Name</label>
                                        </div>
                                        <div class="col-md-6 ">
                                            : {{ $loan->client_father_name }}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 text-left">
                                            <label class="font-weight-bold">DOB</label>
                                        </div>
                                        <div class="col-md-6 ">
                                            : {{ $loan->client_dob }} 
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-6 text-left">
                                            <label class="font-weight-bold">Phone</label>
                                        </div>
                                        <div class="col-md-6 ">
                                            : {{ $loan->client_phone }} 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if($loan->guarantor_a)
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Guarantor A</h5>
                            <div class="row">
                                <div class="col-md-2">
                                    <img src="{{ asset('public/storage/guarantor_photos/'.$guarantor_a_photo->guarantor_photo) }}" class="rounded-circle" style="width: 70px" alt="{{ $guarantor_a_photo->guarantor_photo }}" />
                                </div>
                                <div class="col-md-5">
                                    <div class="form-row">
                                        <div class="col-md-3 text-left">
                                            <label class="font-weight-bold">ID</label>
                                        </div>
                                        <div class="col-md-9 ">
                                            : {{ $loan->guarantor_a_id }}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-3 text-left">
                                            <label class="font-weight-bold">Name</label>
                                        </div>
                                        <div class="col-md-9 ">
                                            : {{ $loan->guarantor_a }}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-3 text-left">
                                            <label class="font-weight-bold">NRC</label>
                                        </div>
                                        <div class="col-md-9 ">
                                            @if ($loan->guarantor_a_nrc)
                                            : {{ $loan->guarantor_a_nrc }} 
                                            @else
                                            : N/A
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-row">
                                        <div class="col-md-4 text-left">
                                            <label class="font-weight-bold">DOB</label>
                                        </div>
                                        <div class="col-md-8 ">
                                            : {{ $loan->guarantor_a_dob }}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-4 text-left">
                                            <label class="font-weight-bold">Phone</label>
                                        </div>
                                        <div class="col-md-8 ">
                                            : {{ $loan->guarantor_a_phone }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="row mb-4">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header theme-color">
                            <h5 class="text-light">
                                <i class="fas fa-briefcase"></i>
                                <span class="ml-2">LOAN INFORMATION</span>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Main Loan ID</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                     : {{$main_loan_id}}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Portal Loan ID</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                     : {{$loan->loan_unique_id}}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Branch</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->branch_name}}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Loan Officer</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->loan_officer_name }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Group</label>
                                </div>
                                <div class="col-md-6 mb-9">
                                 : {{$main_group_id}}
                                    </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Center</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$main_center_id}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                  @if($loan->guarantor_b)
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Guarantor B</h5>
                                    @if($loan->guarantor_b)
                                    <div class="row">
                                        <div class="col-md-2">
                                            <img src="{{ asset('public/storage/guarantor_photos/'.$guarantor_b_photo->guarantor_photo) }}" class="rounded-circle" style="width: 70px" alt="{{ $guarantor_b_photo->guarantor_photo }}" />
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-row">
                                                <div class="col-md-3 text-left">
                                                    <label class="font-weight-bold">ID</label>
                                                </div>
                                                <div class="col-md-9 ">
                                                    : {{ $loan->guarantor_b_id }}
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-3 text-left">
                                                    <label class="font-weight-bold">Name</label>
                                                </div>
                                                <div class="col-md-9 ">
                                                    : {{ $loan->guarantor_b }}
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-3 text-left">
                                                    <label class="font-weight-bold">NRC</label>
                                                </div>
                                                <div class="col-md-9 ">
                                                    @if ($loan->guarantor_b_nrc)
                                            		: {{ $loan->guarantor_b_nrc }} 
                                            		@else
                                            		: N/A
                                            		@endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-row">
                                                <div class="col-md-4 text-left">
                                                    <label class="font-weight-bold">DOB</label>
                                                </div>
                                                <div class="col-md-8 ">
                                                    : {{ $loan->guarantor_b_dob }}
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-4 text-left">
                                                    <label class="font-weight-bold">Phone</label>
                                                </div>
                                                <div class="col-md-8 ">
                                                    : {{ $loan->guarantor_b_phone }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if($loan->guarantor_c)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Guarantor C</h5>

                                    @if($loan->guarantor_c)
                                    <div class="row">
                                        <div class="col-md-2">
                                            <img src="{{ asset('public/storage/guarantor_photos/'.$guarantor_c_photo->guarantor_photo) }}" class="rounded-circle" style="width: 70px" alt="{{ $guarantor_c_photo->guarantor_photo }}" />
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-row">
                                                <div class="col-md-3 text-left">
                                                    <label class="font-weight-bold">ID</label>
                                                </div>
                                                <div class="col-md-9 ">
                                                    : {{ $loan->guarantor_c_id }}
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-3 text-left">
                                                    <label class="font-weight-bold">Name</label>
                                                </div>
                                                <div class="col-md-9 ">
                                                    : {{ $loan->guarantor_c }}
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-3 text-left">
                                                    <label class="font-weight-bold">NRC</label>
                                                </div>
                                                <div class="col-md-9 ">
                                                    @if ($loan->guarantor_c_nrc)
                                            		: {{ $loan->guarantor_c_nrc }} 
                                            		@else
                                            		: N/A
                                            		@endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-row">
                                                <div class="col-md-4 text-left">
                                                    <label class="font-weight-bold">DOB</label>
                                                </div>
                                                <div class="col-md-8 ">
                                                    : {{ $loan->guarantor_c_dob }}
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-4 text-left">
                                                    <label class="font-weight-bold">Phone</label>
                                                </div>
                                                <div class="col-md-8 ">
                                                    : {{ $loan->guarantor_c_phone }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header theme-color">
                            <h5 class="text-light">
                                <i class="fas fa-money-bill-wave"></i>
                                <span class="ml-2 uppercase">Repayment Term</span>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Business Type</label>
                                </div>
                                @if( !empty($business_type->business_type_name) )
                                <div  class="col-md-6 mb-9 ">
                                     : {{$business_type->business_type_name }}
                                </div>
                                @else
                                <div  class="col-md-6 mb-9 ">
                                     : N/A
                                </div>
                                @endif
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Business Category</label>
                                </div>
                                @if( !empty($loan->business_category) )
                                <div  class="col-md-6 mb-9 ">
                                     : {{$loan->business_category }}
                                </div>
                                @else
                                <div  class="col-md-6 mb-9 ">
                                     : N/A
                                </div>
                                @endif
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Loan Type</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->loan_type_name }}
                                </div>
                            </div>
                
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Loan Amount</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->loan_amount }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Estimate Receivable Amount</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->estimate_receivable_amount }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Loan Term Value</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->loan_term_value }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Loan Term</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->loan_term }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Interest Rate Period</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->interest_rate_period }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Disbursement Status</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->disbursement_status }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Interest Method</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->interest_method }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Remark</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->remark }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Status Del</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->status_del }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header theme-color">
                            <h5 class="text-light">
                                <i class="fas fa-calendar"></i>
                                <span class="ml-2 uppercase">Date</span>
                            </h5>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Loan Application Date</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->loan_application_date }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">First Installment Date</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->first_installment_date }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Disbursement Date</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->disbursement_date }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01" class="font-weight-bold">Cancel Date</label>
                                </div>
                                <div  class="col-md-6 mb-9 ">
                                 : {{$loan->cancel_date }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    </div>
                    <div class="tab-pane" id="tab-animated1-1" role="tabpanel">
                        @if($client_document)
                            <hr>
                            <h5 class="menu-header-title text-uppercase mb-3 fsize-2 text-secondary font-weight-bold">Client Documents</h5>
                            <div class="form-row">
                                @if($client_document->loan_document_1)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 1</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$client_document->loan_document_1) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                                @if($client_document->loan_document_2)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 2</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$client_document->loan_document_2) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                                @if($client_document->loan_document_3)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 3</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$client_document->loan_document_3) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="form-row">
                                @if($client_document->loan_document_4)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 4</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$client_document->loan_document_4) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                                @if($client_document->loan_document_5)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 5</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$client_document->loan_document_5) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endif

                        @if($guarantor_a_document)
                            <hr>
                            {{--                        Guarantor A--}}
                           <h5 class="menu-header-title text-uppercase mb-3 fsize-2 text-secondary font-weight-bold">Guarantor A Documents</h5>
                            <div class="form-row">
                                @if($guarantor_a_document->loan_document_1)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 1</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_a_document->loan_document_1) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                                @if($guarantor_a_document->loan_document_2)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 2</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_a_document->loan_document_2) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                                @if($guarantor_a_document->loan_document_3)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 3</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_a_document->loan_document_3) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="form-row">
                                @if($guarantor_a_document->loan_document_4)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 4</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_a_document->loan_document_4) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                                @if($guarantor_a_document->loan_document_5)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 5</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_a_document->loan_document_5) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endif


                        @if($guarantor_b_document)
                            <hr>
                            {{--                        Guarantor B--}}
                            <h5 class="menu-header-title text-uppercase mb-3 fsize-2 text-secondary font-weight-bold">Guarantor B Documents</h5>
                            <div class="form-row">
                                @if($guarantor_b_document->loan_document_1)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 1</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_b_document->loan_document_1) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                                @if($guarantor_b_document->loan_document_2)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 2</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_b_document->loan_document_2) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                                @if($guarantor_b_document->loan_document_3)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 3</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_b_document->loan_document_3) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="form-row">
                                @if($guarantor_b_document->loan_document_4)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 4</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_b_document->loan_document_4) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                                @if($guarantor_b_document->loan_document_5)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 5</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_b_document->loan_document_5) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endif

                        @if($guarantor_c_document)
                            <hr>
                            {{--                        Guarantor C--}}
                           <h5 class="menu-header-title text-uppercase mb-3 fsize-2 text-secondary font-weight-bold">Guarantor C Documents</h5>
                            <div class="form-row">
                                @if($guarantor_c_document->loan_document_1)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 1</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_c_document->loan_document_1) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                                @if($guarantor_c_document->loan_document_2)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 2</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_c_document->loan_document_2) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                                @if($guarantor_c_document->loan_document_3)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 3</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_c_document->loan_document_3) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="form-row">
                                @if($guarantor_c_document->loan_document_4)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 4</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_c_document->loan_document_4) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                                @if($guarantor_c_document->loan_document_5)
                                    <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Loan Document 5</label>
                                            <br>
                                            <img
                                                src="{{ asset('public/storage/loan_document_photos/'.$guarantor_c_document->loan_document_5) }}"
                                                width='200px' height="130px" alt="" title="">
                                        </div>
                                    </div>
                                @endif
                            </div>
                        @endif
                    </div>
                    <div class="tab-pane" id="tab-animated1-2" role="tabpanel">
                        <hr>

                        <!-- Datatable_start -->
                        <div class="card-body">
                            <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                <p class="menu-header-title text-capitalize mb-3 fsize-3 text-center font-weight-bold">
                                    Repayment Schedule</p><br>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label>Client ID</label>
                                    </div>
                                    <div class="col-md-3">
                                        {{ $loan->client_id }}
                                    </div>
                                    <div class="col-md-3">
                                        Interest Rate
                                    </div>
                                    <div class="col-md-3">
                                        {{$loan->interest_rate}}
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label>Client Name</label>
                                    </div>
                                    <div class="col-md-3">
                                        {{$loan->client_name }}
                                    </div>
                                    <div class="col-md-3">
                                        Loan Term
                                    </div>
                                    <div class="col-md-3">
                                        {{$loan->loan_term }}
                                    </div>
                                </div>
	                            
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label>Loan Type</label>
                                    </div>
                                    <div class="col-md-3">
                                        {{$loan->loan_type_name }}
                                    </div>
                                    <div class="col-md-3">
                                        Repayment Term
                                    </div>
                                    <div class="col-md-3">
                                        {{$loan->interest_rate_period}}
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label>Loan Application Date</label>
                                    </div>
                                    <div class="col-md-3">
                                        {{$loan->loan_application_date }}
                                    </div>
                                    <div class="col-md-3">
                                        Disbursement Amount
                                    </div>
                                    <div class="col-md-3">
                                        {{$loan->loan_amount }}
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label>First Installment Date</label>
                                    </div>
                                    <div class="col-md-3">
                                        {{$loan->first_installment_date }}
                                    </div>
                                    <div class="col-md-3">
                                        Branch Name
                                    </div>
                                    <div class="col-md-3">
                                        {{$loan->branch_name}}
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">
                                        <label>Center Leader Name</label>
                                    </div>
                                    <div class="col-md-3">
                                        {{$center_leader_name}}
                                    </div>
                                    <div class="col-md-3">
                                        Loan Officer Name
                                    </div>
                                    <div class="col-md-3">
                                        {{$loan->loan_officer_name }}
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-3">

                                    </div>
                                    <div class="col-md-3">
                                    
                                    </div>
                                    <div class="col-md-3">
                                        Loan Term Value
                                    </div>
                                    <div class="col-md-3">
                                        {{$loan->loan_term_value }}
                                    </div>
                                </div>
                                <br><br>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table style="width: 100%;" id="myTable"
                                               class="table table-hover table-striped table-bordered dataTable dtr-inline"
                                               role="grid" aria-describedby="example_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc text-center align-middle" tabindex="0"
                                                    aria-controls="example" rowspan="2" aria-sort="ascending"
                                                    aria-label="Name: activate to sort column descending">No
                                                </th>
                                                <th class="sorting text-center align-middle" tabindex="0"
                                                    aria-controls="example" rowspan="2"
                                                    aria-label="Position: activate to sort column ascending">Repayment
                                                    Date
                                                </th>
                                                <th class="sorting text-center" tabindex="0" aria-controls="example"
                                                    colspan="3" aria-label="Office: activate to sort column ascending">
                                                    Repayment Amount
                                                </th>
                                                <th class="sorting text-center" tabindex="0" aria-controls="example"
                                                    colspan="3" aria-label="Age: activate to sort column ascending">
                                                    Balance
                                                </th>
                                            </tr>
                                            <tr role="row">
                                                <th class="sorting text-center" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 187.2px;"
                                                    aria-label="Office: activate to sort column ascending">Principal
                                                </th>
                                                <th class="sorting text-center" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 106.2px;"
                                                    aria-label="Age: activate to sort column ascending">Interest
                                                </th>
                                                <th class="sorting text-center" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 192.2px;"
                                                    aria-label="Start date: activate to sort column ascending">Total
                                                </th>
                                                {{--<th class="sorting text-center" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 187.2px;"
                                                    aria-label="Office: activate to sort column ascending">Principal
                                                </th>
                                                <th class="sorting text-center" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 106.2px;"
                                                    aria-label="Age: activate to sort column ascending">Interest
                                                </th>--}}
                                                <th class="sorting text-center" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 192.2px;"
                                                    aria-label="Start date: activate to sort column ascending">Balance
                                                </th>
                                                <th class="sorting text-center" tabindex="0" aria-controls="example"
                                                    rowspan="1" colspan="1" style="width: 192.2px;"
                                                    aria-label="Start date: activate to sort column ascending">Remark
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody id="myTableBody">
                                            @foreach($schedules as $key => $schedule)
                                                <tr role="row" class="odd">
                                                    <td id="" class="sorting_1 dtr-control">{{ $key+1 }}</td>
                                                    <td id="" class="text-nowrap"> {{ $schedule->month }}</td>
                                                    <td id=""> {{ number_format($schedule->capital) }}</td>
                                                    <td id=""> {{ number_format($schedule->interest) }}</td>
                                                    <td id=""> {{ number_format($schedule->capital + $schedule->interest) }}</td>
                                                    {{-- <td id=""> {{ $schedule->refund_interest }}</td>
                                                    <td id=""> {{ number_format($schedule->refund_amount) }}</td> --}}
                                                    <td id=""> {{ number_format($schedule->final_amount) }}</td>
                                                    <td id=""></td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                            <tfoot>
                                            <tr role="row" class="odd">
                                                <td colspan="2" id="" class="sorting_1 dtr-control">Total</td>
                                                <td id=""> {{ number_format($total_capital) }}</td>
                                                <td id=""> {{ number_format($total_interest) }}</td>
                                                <td id=""> {{ number_format($total_capital + $total_interest) }}</td>
                                                <td id=""></td>
                                                <td id=""></td>
                                                <td id=""></td>
                                            </tr>
                                            </tfoot>

                                        </table>
                                    </div>
                                </div>
                                <!-- <div class="row">
                                <div class="col-sm-12 col-md-5"><div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-12 col-md-7"><div class="dataTables_paginate paging_simple_numbers" id="example_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="example_previous"><a href="#" aria-controls="example" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example" data-dt-idx="4" tabindex="0" class="page-link">4</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example" data-dt-idx="5" tabindex="0" class="page-link">5</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example" data-dt-idx="6" tabindex="0" class="page-link">6</a></li><li class="paginate_button page-item next" id="example_next"><a href="#" aria-controls="example" data-dt-idx="7" tabindex="0" class="page-link">Next</a></li></ul></div></div></div></div>
                            </div> -->
                            </div>
                        </div>
                        <!-- End -->
                    </div>
                </div>
            </div>


@endsection
