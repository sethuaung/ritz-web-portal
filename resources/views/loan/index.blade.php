@extends('layouts.app')
@section('content')
    <div class="col-md-12">
        <div class="mb-3"></div>
        <div class="main-card mb-3 card">
            <div class="card-header">
                {{ $data_loanstitle }}
                <div class="btn-actions-pane-right">
                    <div role="group" class="btn-group-sm btn-group @if (request()->has('disbursement_status')) d-none @endif">
                        <a href="{{ url('loan/create') }}">
                            <button type="button" title="" data-placement="bottom"
                                class="btn-shadow mr-1 btn theme-color text-white">
                                <i class="fa fa-plus"></i>
                                Add
                            </button>
                        </a>
                        <a href="{{ url('loan?date=week') }}">
                            <button type="button" title="" data-placement="bottom"
                                class="btn-shadow mr-1 btn theme-color text-white">
                                <i class="fa fa-calendar"></i>
                                This Week
                            </button>
                        </a>
                        <a href="{{ url('loan?date=month') }}">
                            <button type="button" title="" data-placement="bottom"
                                class="btn-shadow mr-1 btn theme-color text-white">
                                <i class="fa fa-calendar"></i>
                                This Month
                            </button>
                        </a>
                        <a href="{{ url('loan?date=year') }}">
                            <button type="button" title="" data-placement="bottom"
                                class="btn-shadow mr-1 btn theme-color text-white">
                                <i class="fa fa-calendar"></i>
                                This Year
                            </button>
                        </a>
                        <a href="{{ url('loan?date=date') }}">
                            <button type="button" title="" data-placement="bottom"
                                class="btn-shadow mr-1 btn theme-color text-white">
                                <i class="fa fa-calendar"></i>
                                Today
                            </button>
                        </a>
                    </div>
                </div>
            </div>

            <div class="widget-content p-2">
                <div class="widget-content-wrapper ">
                    @if (empty(request('disbursement_status')))
                        <div class="widget-content-left mr-3 ml-3">
                            <label class="m-0"> Show </label>
                        </div>
                        <div class="widget-content-left mr-1">
                            <div class="widget-heading">
                                <form method="GET" id="loanLengthForm">
                                    <select name="loan_length" id="loanLength" aria-controls="example"
                                        class="custom-select custom-select-sm form-control form-control-sm">
                                        <option value="10" @if (request()->loan_length == 10) selected @endif>10</option>
                                        <option value="25" @if (request()->loan_length == 25) selected @endif>25</option>
                                        <option value="50" @if (request()->loan_length == 50) selected @endif>50</option>
                                        <option value="100" @if (request()->loan_length == 100) selected @endif>100
                                        </option>
                                    </select>
                                </form>
                            </div>
                        </div>
                    @endif

                    @if (request('disbursement_status') === 'Pending')
                        <div class="widget-content-left ml-2">
                            <input type="date" class="form-control form-control-sm" id="approve_d">
                        </div>
                        <div class="widget-content-left ml-2">
                            <button class="btn btn-sm btn-success" id="approve_select">Approved selected</button>
                        </div>
                    @endif
                    <div class="widget-content-right ">
                        <div class="btn-group">
                            <!-- <label class="m-2"> Search </label> -->
                            <form method="GET">
                                <div class="input-group">
                                    <input type="text" name="search_loanslist" id="search_loanslist"
                                        value="{{ request()->get('search_loanslist') }}" class="form-control mr-1"
                                        placeholder="Search..." aria-label="Search" aria-describedby="button-addon2">
                                    @if (request()->has('disbursement_status'))
                                        <input type="hidden" name="disbursement_status"
                                            value="{{ request()->get('disbursement_status') }}">
                                    @endif

                                    <button class="btn theme-color text-light mr-1" type="submit" id="button-addon2">
                                        <i class="fa fa-search"></i>
                                    </button>

                                    <a href="{{ url('loan') }}"
                                        class="btn theme-color text-light @if (request()->has('disbursement_status')) d-none @endif">
                                        <i class="fa fa-refresh"></i>
                                    </a>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            @if (request('disbursement_status') === 'Pending')
                                <th>
                                    <input type="checkbox" id="approveAll">
                                </th>
                                <th>

                                </th>
                            @endif
                            @if (request('disbursement_status') === 'Activated')
                                <th>View</th>
                            @endif
                            <th>Portal Loan ID</th>
                            <th>Main Loan ID</th>
                            <th>Loan Officer</th>
                            <th>Client ID</th>
                            <th>Client</th>
                            <th>Guarantors</th>
                            <th>Loan Type</th>
                            <th>Status</th>
                            <th class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <form action="{{ route('loan_approval.store') }}" method="POST" id="approveForm">
                            @csrf
                            <input type="date" name="approve_date" id="approve_date" class="d-none">
                            @foreach ($data_loans as $key => $loan)
                                <tr>
                                    <td class="text-center text-muted">{{ $key + 1 }}</td>
                                    @if (request('disbursement_status') === 'Pending')
                                        <td>

                                            <input type="checkbox" class="checkbox approveCheck"
                                                name="check_{{ $key + 1 }}" value="checked">
                                            <input type="hidden" name="check_{{ $key + 1 . '_value' }}"
                                                value="{{ $loan->loan_unique_id }}">
                                            <input type="hidden" name="count" value="{{ count($data_loans) }}">
                                        </td>
                                        {{-- <td >
                                            <button type="button" aria-expanded="true" class="border-0 btn-transition btn btn-outline-dark btn-sm"
                                                    aria-controls="exampleAccordion1" data-toggle="collapse"
                                                    data-target="#exampleModalLong" class="m-0 p-0 btn btn-link"><i class="fa fa-plus-square-o"></i>
                                            </button>
                                                
                                        </td> --}}
                                        <td>
                                            <button type="button"
                                                class="border-0 btn-transition btn btn-outline-dark btn-sm get__loan__approval__info"
                                                data-loan="{{ $loan->loan_unique_id }}" data-toggle="modal"
                                                data-target="#exampleModalShort"><i
                                                    class="fa fa-plus-square-o"></i></button>
                                        </td>
                                        
                                    @endif
                                    @if (request('disbursement_status') === 'Activated')
                                        <td>
                                            <button type="button"
                                                class="border-0 btn-transition btn btn-outline-dark btn-sm get__loan__schedule"
                                                data-loan="{{ $loan->loan_unique_id }}" data-toggle="modal"
                                                data-target="#exampleModalLong"><i
                                                    class="fa fa-plus-square-o"></i></button>
                                        </td>
                                    @endif

                                    <td>{{ $loan->loan_unique_id }}</td>
                                    <td>
                                        @if (DB::table('tbl_main_join_loan')->where('portal_loan_id', $loan->loan_unique_id)->first())
                                            {{ DB::table('tbl_main_join_loan')->where('portal_loan_id', $loan->loan_unique_id)->first()->main_loan_code }}
                                        @endif
                                    </td>
                                    <td>{{ $loan->loan_officer_name }}</td>
                                    <td>{{ DB::table('tbl_main_join_client')->where('portal_client_id', $loan->client_id)->first()->main_client_code ?? $loan->client_id }}
                                    </td>
                                    <td>{{ $loan->client_name }}</td>
                                    <td>

                                        @if ($loan->guarantor_a)
                                            <span class="badge badge-pill badge-secondary mt-2 theme-color">
                                                {{ DB::table('tbl_guarantors')->where('guarantor_uniquekey', $loan->guarantor_a)->first()->name }}
                                            </span>
                                        @endif
                                        <br>
                                        @if ($loan->guarantor_b)
                                            <span class="badge badge-pill badge-secondary mt-2 theme-color">
                                                {{ DB::table('tbl_guarantors')->where('guarantor_uniquekey', $loan->guarantor_b)->first()->name }}
                                            </span>
                                        @endif
                                        <br>
                                        @if ($loan->guarantor_c)
                                            <span class="badge badge-pill badge-secondary mt-2 theme-color">
                                                {{ DB::table('tbl_guarantors')->where('guarantor_uniquekey', $loan->guarantor_c)->first()->name }}
                                            </span>
                                        @endif
                                    </td>
                                    <td>{{ $loan->loan_type_name }}</td>
                                    <td>
                                        @if ($loan->disbursement_status == 'Pending')
                                            <div class="badge badge-warning">
                                                {{ $loan->disbursement_status }}
                                            </div>
                                        @endif
                                        @if ($loan->disbursement_status == 'Approved')
                                            <div class="badge badge-info">
                                                {{ $loan->disbursement_status }}
                                            </div>
                                        @endif
                                        @if ($loan->disbursement_status == 'Activated')
                                            <div class="badge badge-success">
                                                {{ $loan->disbursement_status }}
                                            </div>
                                        @endif
                                        @if ($loan->disbursement_status == 'Closed')
                                            <div class="badge badge-dark">
                                                {{ $loan->disbursement_status }}
                                            </div>
                                        @endif
                                        @if ($loan->disbursement_status == 'Deposited')
                                            <div class="badge badge-secondary">
                                                {{ $loan->disbursement_status }}
                                            </div>
                                        @endif
                                        @if ($loan->disbursement_status == 'Canceled')
                                            <div class="badge badge-danger">
                                                {{ $loan->disbursement_status }}
                                            </div>
                                        @endif
                                    </td>
                                    <td class="text-center text-nowrap">
                                        @if (request('disbursement_status') === 'Approved' || request('disbursement_status') === 'Pending')
                                            @if (request('disbursement_status') === 'Approved')
                                                <a href="{{ route('loandeposit.create', $loan->loan_unique_id) }}"
                                                    class="btn btn-sm btn-outline-success">Deposit</a>
                                            @endif
                                            <a href="{{ url('loancancel/' . $loan->loan_unique_id . '/' . session()->get('staff_code')) }}"
                                                onclick="return confirm('Are you sure?')"
                                                class="btn btn-sm btn-outline-danger">
                                                Cancel
                                            </a>
                                        @elseif(request('disbursement_status') === 'Deposited')
                                            <a href="{{ route('loandisbursement.create', $loan->loan_unique_id) }}"
                                                class="btn btn-sm btn-outline-success">Disburse</a>
                                            <a href="{{ url('loancancel/' . $loan->loan_unique_id . '/' . session()->get('staff_code')) }}"
                                                onclick="return confirm('Are you sure?')"
                                                class="btn btn-sm btn-outline-danger">
                                                Cancel
                                            </a>
                                        @else
                                            <a href="loan/view/{{ $loan->loan_unique_id }}">
                                                <button type="button" id="PopoverCustomT-1"
                                                    class="border-0 btn-transition btn  btn-outline-info btn-sm"><i
                                                        class="fa fa-eye"></i></button>
                                            </a>
                                            {{-- @if (request('disbursement_status') != 'Pending' && request('disbursement_status') != 'Activated')
                                    <a href="loan/{{$loan->loan_unique_id}}/edit">
                                        <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-warning"><i class="fa fa-pencil" ></i></button>
                                    </a>
                                    @endif --}}
                                            {{-- @if (!request('disbursement_status'))
                                        <form action="{{ url('loan/destroy',['id'=>$loan->loan_unique_id]) }}" method="POST" class="d-inline-block" onsubmit="return confirm('Are you sure want to delete?')">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-danger"><i class="fa fa-trash"> </i></button>
                                    </form>
                                    @endif --}}
                                        @endif
                                    </td>

                                </tr>
                                
                            @endforeach
                        </form>
                    </tbody>
                </table>
            </div>
            <div class="d-block text-center card-footer">
                <div class="col-md-12 col-sm-12 grid-margin mt-2">
                    <div class="">
                        {{ $data_loans->appends($_GET)->links() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@if (session('successMsg') != null)
    @section('script')
        <script>
            statusAlert("{{ session('successMsg') }}");
        </script>
    @endsection
@endif

@if ($errors->any())
    @section('script')
        @php
            $errlist = '';
            foreach ($errors->all() as $e) {
                $errlist .= $e;
            }
        @endphp
        <script>
            errorAlert("{{ $errlist }}");
        </script>
    @endsection
@endif

@if (session('check') != null)
    @section('script')
        <script>
            errorAlert("{{ session('check') }}");
        </script>
    @endsection
@endif
