@extends('layouts.app')
@section('content')

    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Loan Charge Details
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{url('loancharges/')}}" class="btn btn-info">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>    </div>
        </div>            <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Charge Code</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        {{$loancharge->charge_code}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Charge Name</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        {{$loancharge->charge_name}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Charge Description</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        {{$loancharge->charge_description}}
                    </div>
                </div>
                {{-- <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Branch</label>
                    </div>
                    <div class="col-md-9 mb-9">
                        {{$loan_charge_type->branch_name}}
                    </div>
                </div> --}}
                {{-- <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Loan Type</label>
                    </div>
                    <div class="col-md-9 mb-9">
                        {{$loan_charge_type->name}}
                    </div>
                </div> --}}
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Product Type</label>
                    </div>
                    <div class="col-md-9 mb-9">
                        {{$loancharge->product_type}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Amount</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        {{$loancharge->amount}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Charge Option</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        {{-- {{$loancharge->charge_option}} --}}
                        @if ($loancharge->charge_option == 1)
                            Fixed Amount
                        @else
                            Loan Amount
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Charge Type</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        {{-- {{$loancharge->charge_type}} --}}
                        @if($loancharge->charge_type == 1)
                            Deposit Before Disbursement
                        @elseif($loancharge->charge_type == 2)
                            Deduct from loan disbursement
                        @else
                            Every Repayments
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Accounting</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        {{$account_type->code}} - {{$account_type->name}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Charge</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        {{$loancharge->charge}}
                    </div>
                </div>
                {{-- <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Sequence</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        {{$loancharge->seq}}
                    </div>
                </div> --}}
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01">Status</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        {{$loancharge->status}}
                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection
