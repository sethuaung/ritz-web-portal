@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Charges Edit</div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{url('loancharges/')}}" class="btn btn-info">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alart alart-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="needs-validation" method="POST" action="{{route('loancharges.update', $loancharge->charge_code)}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Branch</label>
                            <select type="select" id="branch_id" name="branch_id" class="form-control">
                                <option value="">Choose Branch</option>
                                @foreach ($branches as $branch)
                                    <option value="{{$branch->id}}" @if ($loan_charge_type->branch_id == $branch->id) selected="selected" @endif>{{$branch->branch_name}}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('product_type')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Loan Type</label>
                            <select type="select" id="loan_type_id" name="loan_type_id" class="form-control">
                                <option value="">Choose Loan Type</option>
                                @foreach ($loantypes as $loantype)
                                    <option value="{{$loantype->id}}" @if($loan_charge_type->loan_type_id==$loantype->id) selected="selected" @endif>{{$loantype->name}}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('product_type')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Charge Code </label>
                            <input type="text" class="form-control" id="" name="charge_code" value="{{$loancharge->charge_code}}" placeholder="Example : AB-C-12345" required>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('charge_code')}}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Charge Name </label>
                            <input type="text" class="form-control" id="" name="charge_name" value="{{$loancharge->charge_name}}" placeholder="" required>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('charge_name')}}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom01">Charge Description </label>
                            <input type="text" class="form-control" id="" name="charge_description" value="{{$loancharge->charge_description}}" placeholder="" required>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('charge_description')}}
                            </div>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Product Type</label>
                            <select type="select" id="" name="product_type" class="form-control">
                                <option value="test">Choose Product Type</option>
                                <option value="loan" {{($loancharge->product_type=="loan")?"selected":""}}>Loan</option>
                                <option value="savings" {{($loancharge->product_type=="savings")?"selected":""}}>Savings</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('product_type')}}
                            </div>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Accounting </label>
                            <select type="select" id="accounting_id" name="accounting_id" class="form-control">
                                <option value="">Choose a category</option>
                                @foreach ($accounts as $account)
                                    <option value="{{$account->id}}" @if ($account_type->accounting_id == $account->id) selected="selected" @endif>{{$account_type->code}} - {{$account->name}}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('accounting_id')}}
                            </div>
                        </div>
                        <div class="col-md-2 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Charge Option </label>
                            {{-- <input type="text" class="form-control" id="" name="charge_option" value="{{$loancharge->charge_option}}" placeholder="" required> --}}
                            <select type="select" id="" name="charge_option" class="form-control">
                                <option value="test">Choose Charge Option</option>
                                <option value="1" {{($loancharge->charge_option=="1")?"selected":""}}>Fixed Amount</option>
                                <option value="2" {{($loancharge->charge_option=="2")?"selected":""}}>Of Loan Amount</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('charge_option')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Charge Type </label>
                            {{-- <input type="text" class="form-control" id="" name="charge_type" value="{{$loancharge->charge_type}}" placeholder="" required> --}}
                            <select type="select" id="" name="charge_type" class="form-control">
                                <option value="test">Choose Charge Type</option>
                                <option value="1" {{($loancharge->charge_type=="1")?"selected":""}}>Deposit Before Disbursement</option>
                                <option value="2" {{($loancharge->charge_type=="2")?"selected":""}}>Deduct from loan disbursement</option>
                                <option value="3" {{($loancharge->charge_type=="3")?"selected":""}}>Every Repayments</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('charge_type')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Amount </label>
                            <input type="text" class="form-control" id="" name="amount" value="{{$loancharge->amount}}" placeholder="" required>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('amount')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Charge </label>
                            <input type="text" class="form-control" id="" name="charge" value="{{$loancharge->charge}}" placeholder="" required>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('charge')}}
                            </div>
                        </div>
                        {{-- <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Sequence </label>
                            <input type="text" class="form-control" id="" name="seq" value="{{$loancharge->seq}}" placeholder="" required>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('seq')}}
                                </div>
                        </div> --}}
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Status </label>
                            <select type="select" id="" name="status" class="form-control">
                                <option value="test">Choose Status</option>
                                <option value="none" {{($loancharge->status=="none")?"selected":""}}>None</option>
                                <option value="active" {{($loancharge->status=="active")?"selected":""}}>Active</option>
                                <option value="unactive" {{($loancharge->status=="unactive")?"selected":""}}>Unactive</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('status')}}
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                    <a href="{{url('loancharges/')}}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
                </div>
            </div>
        </form>
    </div>
    </div>
@endsection
