@extends('layouts.app')

@section('content')
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-header">Calendar
                <div class="btn-actions-pane-right">
                    <div role="group" class="btn-group-sm btn-group">
                    <a href="" class="active btn btn-focus">Add Holiday</a>
                    </div>
                </div>
            </div>
            @if(session('successMsg')!=NULL)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('successMsg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="table-responsive" id="customcalendar">
            </div>
            <div class="d-block text-center card-footer">
            <div class="col-md-12 col-sm-12 grid-margin mt-2">
                         
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    function createCalendar(elem, year, month) {

      let mon = month - 1; // months in JS are 0..11, not 1..12
      let d = new Date(year, mon);
    //   console.log(d.getDay());

      let table = '<table class="align-middle mb-0 table table-borderless table-striped table-hover"><tr><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th><th>Sun</th></tr><tr>';
      // spaces for the first row
      // from Monday till the first day of the month
      // * * * 1  2  3  4
      for (let i = 0; i < getDay(d); i++) {
         
        table += '<td></td>';
        
      }
      

      // <td> with actual dates
      while (d.getMonth() == mon) {
        table += '<td>' + d.getDate() + '</td>';

        if (getDay(d) % 7 == 6) { // sunday, last day of week - newline
          table += '</tr><tr>';
        }

        d.setDate(d.getDate() + 1);
      }

      // add spaces after last days of month for the last row
      // 29 30 31 * * * *
      if (getDay(d) != 0) {
        for (let i = getDay(d); i < 7; i++) {
          table += '<td></td>';
        }
      }

      // close the table
      table += '</tr></table>';

      elem.innerHTML = table;
    }

    function getDay(date) { // get day number from 0 (monday) to 6 (sunday)
      let day = date.getDay();
      if (day == 0) day = 7; // make Sunday (0) the last day
      return day - 1;
    }

    createCalendar(customcalendar, 2021, 12);
  </script>
@endsection
