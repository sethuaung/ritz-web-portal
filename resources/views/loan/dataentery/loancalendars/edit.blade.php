@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
@endsection
@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Event Create</div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('loancalendars/')}}" class="btn btn-info">
                        <i class="pe-7s-back btn-icon-wrapper">Back To Calendar</i>
                    </a>                    
                </div>
            </div>  
        </div>
    </div>    
    <div class="main-card mb-3 card">
        <div class="card-body">
        @if ($errors->any())
            <div class="alart alart-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif    
        <form class="needs-validation" method="POST" action="{{route('loancalendars.update',$loancalendar->id)}}" enctype="multipart/form-data" >
        @csrf
        @method('PATCH')
            <div class="form-row">
                <div class="col-md-4">
                    <div class="position-relative form-group">
                        <label for="" class="">Staff</label>
                        <select type="select" id="" name="staff_id" class="form-control" required>
                            <option value="">Choose Staff</option>
                            @foreach($staff as $staff)
                            <option value="{{$staff->id}}">{{$staff->name}}</option>
                            @endforeach
                        </select>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('staff_id')}}                                        
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="position-relative form-group">
                        <label for="" class="">Date</label>
                        <select type="select" id="" name="day" class="form-control selectpicker" disabled  data-show-subtext="true" data-live-search="true" required>
                            <!-- <option value="">Choose Day</option> -->
                            <option value="{{$loancalendar->date}}" >{{$loancalendar->date}}</option>
                            
                        </select>  
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('day')}}                                        
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                        <div class="position-relative form-group">
                        <label for="" class="">Day Name</label>
                        <select type="select" id="" name="day_name" disabled class="form-control" required>
                            <option value="{{$loancalendar->day_name}}">{{$loancalendar->day_name}}</option>
                            
                        </select>                        
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('day_name')}}                                        
                        </div>
                    </div>
                </div>
            </div>
                                      
                                       
            <div class="form-row">
                <div class="col-md-4">
                    <div class="position-relative form-group">
                        <label for="" class="">Month</label>
                        <select type="select" id="" name="month" disabled class="form-control" required>
                                <option value="{{$loancalendar->month}}">{{$loancalendar->month}}</option>
                                
                            </select>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('month')}}                                        
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                        <div class="position-relative form-group">
                        <label for="" class="">Year</label>
                        <select type="select" id="" name="year" disabled class="form-control" required>
                            <option value="{{$loancalendar->year}}">{{$loancalendar->year}}</option>
                            
                        </select>                        
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('year')}}                                        
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="position-relative form-group">
                        <label for="exampleState" class="">Status</label>
                        <select type="select" id="" name="status" class="form-control" required>
                            <option value="">Choose Status</option>
                            <option value="none" {{ old('status') == 'none' ? 'selected' : '' }}>None</option>
                            <option value="on" {{ old('status') == 'on' ? 'selected' : '' }}>On</option>
                            <option value="off" {{ old('status') == 'off' ? 'selected' : '' }}>Off</option>
                            <option value="gazetted" {{ old('status') == 'gazetted' ? 'selected' : '' }}>Gazetted</option>
                            <option value="special_off" {{ old('status') == 'special_off' ? 'selected' : '' }}>Special Off</option>
                            
                        </select>
                        <div class="text-danger form-control-feedback">
                            {{$errors->first('status')}}                                        
                        </div>
                    </div>
                </div>
                
            </div>
                                       
            <button type="submit" class="mt-1 mb-2 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
            <a href="{{url('loancalendars/')}}" class="btn btn-secondary mb-2 mt-1">
                <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
            </a>
        </form>
    </div>
</div>


@endsection
@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" ></script>
<script>
    $(document).ready(function () {
      $('select').selectize({
            // create: true,
            sortField: 'text'
      });
  });
</script>

@endsection