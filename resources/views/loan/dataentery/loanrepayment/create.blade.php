@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Loan Repayment</div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{ url('loan?disbursement_status=Activated') }}" class="btn btn-info">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alart alart-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="needs-validation" method="POST" action="{{ route('loan_repayment.store') }}"
            enctype="multipart/form-data">
            @csrf
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="form-row">
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Payment Number</label>
                                <input name="payment_num" id="payment_num" placeholder="0" value="{{ $payment_num }}"
                                    type="text" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('payment_num') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Client ID</label>
                                <input name="client_id" id="client_id" placeholder="0"
                                    value="{{ $data_info[0]->client_uniquekey }}" type="text" class="form-control"
                                    readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('client_id') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Client Name</label>
                                <input name="client_name" id="client_name" placeholder="0" value="{{ $data_info[0]->name }}"
                                    type="text" class="form-control" readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('client_name') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Loan ID</label>
                                <input name="loan_id" id="loan_id" placeholder="0"
                                    value="{{ $data_info[0]->loan_unique_id }}" type="text" class="form-control"
                                    readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('loan_id') }}
                                </div>
                            </div>
                        </div>

                    </div>
                    <div>
                        <input name="disbursement_id" id="disbursement_id" placeholder="0"
                            value="{{ $disbursement_id[0]->disbursement_id }}" type="text" class="form-control" hidden>
                        <div class="text-danger form-control-feedback">
                            {{ $errors->first('disbursement_id') }}
                        </div>
                    </div>
                    <div hidden>
                        <?php
                        $i=0;
                        foreach($loanschedule_id as $scheduleid) {
                        ++$i;
                        ?>

                        <div class="custom-checkbox custom-control">
                            <input type="text" id="removableCheckbox" name="schedule_id[]" value="{{ $scheduleid }}"
                                checked class="custom-control-input">
                        </div>

                        <?php
                        }
                        ?>
                    </div>
                    <div class="form-row">
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Receipt No.</label>
                                <input name="receipt_no" id="receipt_no" placeholder="0" value="" type="text"
                                    class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('receipt_no') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Over Days</label>
                                @php
                                    // $date1 = date('Y-m-d', strtotime($month[0]->month));
                                    // $date2 = date('Y-m-d');
                                    
                                    // $diff = abs(strtotime($date2) - strtotime($date1));
                                    
                                    // $years = floor($diff / (365 * 60 * 60 * 24));
                                    // $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                    // $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

                                    $date1 = strtotime($month->month);
                                    $date2 = strtotime(date('Y-m-d'));
                                    $days = ($date2 - $date1) / 60/60/24;
                                    if($days < 0) {
                                        $days = 0;
                                    }
                                @endphp
                                <input name="over_days" id="over_days" placeholder="0"
                                    value="{{ $days }}" type="text" pattern="[0-*]*"
                                    class="form-control" readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('over_days') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3" hidden>
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Penalty Amount</label>
                                <input name="penalty_amount" id="penalty_amount" placeholder="0" value=""
                                    type="text" class="form-control" readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('penalty_amount') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Repayment Type<span
                                        style="color: red"> *</span></label>
                                <select type="select" id="repayment_type" name="repayment_type" class="custom-select"
                                    required>
                                    <option value="">Choose Disbursement Status</option>
                                    <option value="due" @if ($repayment_type == 'late') disabled @endif>Due</option>
                                    <option value="late" @if ($repayment_type == 'late') selected @endif
                                        @if (!$is_late) disabled @endif>Late</option>
                                    <option value="pre" @if ($repayment_type == 'late') disabled @endif>Pre</option>
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('repayment_type') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Schedule Pay Date</label>
                                <input name="spd" id="spd" placeholder="0" value="{{ $schedule_pay_date }}"
                                    type="text" class="form-control" readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('spd') }}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Principal</label>
                                <input name="principal" id="principal" placeholder="0" value="{{ $total_principal }}"
                                    type="text" class="form-control" readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('principal') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Interest</label>
                                <input name="interest" id="interest" placeholder="0"
                                    value="{{ round($total_interest_to_pay) }}" type="text" class="form-control"
                                    readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('interest') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Total Amount to Pay</label>
                                <input name="total_balance" id="total_balance" placeholder="0"
                                    value="{{ round($total_amount_to_pay) }}" type="text" class="form-control"
                                    readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('total_balance') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Payment</label>
                                <input name="payment" id="payment" placeholder="0"
                                    value="{{ round($total_amount_to_pay) }}" type="text" class="form-control"
                                    readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('payment') }}
                                </div>
                            </div>
                        </div>



                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Owed Amount</label>
                                <input name="outstanding" id="outstanding" placeholder="0" value="0"
                                    type="text" class="form-control" readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('outstanding') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Principal Balance</label>
                                <input name="principal_balance" id="principal_balance" placeholder="0"
                                    value="{{ $principal_balance }}" type="text" class="form-control" readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('principal_balance') }}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Payment Method</label>
                                <select type="select" id="payment_method" name="payment_method" class="custom-select"
                                    required>
                                    <option value="cash">Cash</option>
                                    <option value="online">Online</option>
                                    <option value="banktransfer">Bank Transfer</option>
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('payment_method') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Cash In <span
                                        style="color: red">*</span></label>
                                <select type="select" id="cash_acc_id" name="cash_acc_id" class="custom-select"
                                    required>
                                    <option value="">Choose a category</option>
                                    @foreach ($accounts as $account)
                                        <option value="{{ $account->id }}" selected>{{ $account->external_acc_name }}
                                        </option>
                                    @endforeach

                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('cash_acc_id') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Payment Date <span
                                        style="color: red">*</span></label>
                                <input name="payment_date" id="payment_date" placeholder=""
                                    value="{{ date('Y-m-d') }}" type="date"
                                    class="form-control" required>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('payment_date') }}
                                </div>
                            </div>
                        </div>

                        <input name="schedule_date" id="schedule_date" placeholder=""
                        value="{{ date('Y-m-d', strtotime($month->month)) }}" type="hidden"
                        class="form-control">

                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Document</label>
                                <input name="document" id="" placeholder="" value="" type="file"
                                    class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('document') }}
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
            <button type="submit" class="mt-1 mb-2 btn btn-success">
                <i class="pe-7s-diskette btn-icon-wrapper"> Add Payment </i></button>
            <a href="{{ url('loan?disbursement_status=Activated') }}" class="btn btn-secondary mb-2 mt-1">
                <i class="pe-7s-close-circle btn-icon-wrapper">Back</i>
            </a>

        </form>
    </div>
    </div>
@endsection
