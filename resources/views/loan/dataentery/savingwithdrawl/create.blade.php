@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Saving Withdrawl</div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{ url('/dashboard') }}" class="btn btn-info">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alart alart-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="form-row">
                    <div class="col-md-4 d-none">
                        <div class="position-relative form-group">

                            <label style="font-weight: 600" for="saving_client_name" class="">Search Client
                                Name</label>
                            <input type="text" id="saving_client_name" class="form-control">
                            <div id="saving_client_list">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label style="font-weight: 600" for="" class="">Loan ID<span style="color: red">
                                    *</span></label>
                            <select type="select" id="main_loan_id" name="main_loan_id" class="form-control" required>
                                <option value="">Choose an account</option>
                                @foreach ($saving_active_loans as $info)
                                    <option value="{{ $info['main_id'] }}"> {{ $info['portal_loan_id'] }} / {{$info['main_loan_code'] }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('main_loan_id') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label style="font-weight: 600" for="" class="">Client Name</label>
                            <input name="client_name" id="client_name"  value="" type="text"
                                class="form-control" readonly>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('client_name') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label style="font-weight: 600" for="" class="">Client NRC</label>
                            <input name="client_nrc" id="client_nrc" value="" type="text"
                                class="form-control" readonly>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('client_nrc') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label style="font-weight: 600" for="" class="">Saving Principle</label>
                            <input name="principle_saving" id="principle_saving" placeholder="0" value=""
                                type="text" class="form-control" readonly>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('principle_saving') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label style="font-weight: 600" for="" class="">Interest</label>
                            <input name="interest_saving" id="interest_saving" placeholder="0" value="" type="text"
                                class="form-control" readonly>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('interest_saving') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label style="font-weight: 600" for="" class="">Total Saving</label>
                            <input name="total_saving" id="total_saving" placeholder="0" value="" type="text"
                                class="form-control" readonly>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('total_saving') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-none">
                        <div class="position-relative form-group">
                            <label style="font-weight: 600" for="" class="">Principle Withdrawal</label>
                            <input name="principle_withdrawal" id="principle_withdrawal" placeholder="0" value="" type="text"
                                class="form-control" readonly>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('principle_withdrawal') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-none">
                        <div class="position-relative form-group">
                            <label style="font-weight: 600" for="" class="">Interest Withdrawal</label>
                            <input name="interest_withdrawal" id="interest_withdrawal" placeholder="0" value="" type="text"
                                class="form-control" readonly>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('interest_withdrawal') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label style="font-weight: 600" for="" class="">Total Withdrew Amount</label>
                            <input name="total_withdrew" id="total_withdrew" placeholder="0" value="" type="text"
                                class="form-control" readonly>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('total_withdrew') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label style="font-weight: 600" for="" class="">Principle Balance</label>
                            <input name="principle_balance" id="principle_balance" placeholder="0" value="" type="text"
                                class="form-control" readonly>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('principle_balance') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label style="font-weight: 600" for="" class="">Interest Balance</label>
                            <input name="interest_balance" id="interest_balance" placeholder="0" value="" type="text"
                                class="form-control" readonly>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('interest_balance') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label style="font-weight: 600" for="" class="">Total Balance</label>
                            <input name="total_balance" id="total_balance" placeholder="0" value="" type="text"
                                class="form-control" readonly>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('total_balance') }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 d-none">
                        <div class="position-relative form-group">
                            <label style="font-weight: 600" for="" class="">Last Transaction Date</label>
                            <input name="last_tran_date" id="last_tran_date" placeholder="0" value="" type="text"
                                class="form-control" readonly>
                            <div class="text-danger form-control-feedback">
                                {{ $errors->first('last_tran_date') }}
                            </div>
                        </div>
                    </div>
                            
                            
                </div>
            </div>
        </div>

    </div>
@endsection
