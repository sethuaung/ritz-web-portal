@extends('layouts.app')
@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Loan Type Create</div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('loantypes/')}}" class="btn btn-info">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>                    
                </div>
            </div>  
              </div>
    </div>    
    <div class="main-card mb-3 card">
    <div class="card-body">
    @if ($errors->any())
				<div class="alart alart-danger">
					<ul>
						@foreach($errors->all() as $error)
						<li>{{$error}}</li>
						@endforeach
					</ul>
				</div>
			@endif    
                                   <form class="needs-validation" method="POST" action="{{route('loantypes.store')}}" enctype="multipart/form-data" >
            @csrf
                                        <div class="form-row">
                                            <div class="col-md-4">
                                                <div class="position-relative form-group">
                                                    <label for="" class="">Loan Type Name</label>
                                                    <input name="name" id="" placeholder="For Example : Teacher Loan"  value="{{ old('name') }}" type="text" class="form-control" required>
                                                    <div class="text-danger form-control-feedback">
                            {{$errors->first('name')}}                                        
                        </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="position-relative form-group">
                                                    <label for="" class="">Description</label>
                                                    <input name="description" id="" placeholder="For Example : For Teachers"  value="{{ old('description') }}" type="text" class="form-control" required>
                                                    <div class="text-danger form-control-feedback">
                            {{$errors->first('description')}}                                        
                        </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                 <div class="position-relative form-group">
                                                    <label for="" class="">Interest Rate</label>
                                                    <input name="interest_rate" id="" placeholder="For Example : 5%"  value="{{ old('interest_rate') }}" type="number" class="form-control" required>
                                                    <div class="text-danger form-control-feedback">
                            {{$errors->first('interest_rate')}}                                        
                        </div>
                                                 </div>
                                            </div>
                                        </div>
                                      
                                       
                                        <div class="form-row">
                                        <div class="col-md-4">
                                        <div class="position-relative form-group">
                                            <label for="" class="">Interest Rate Period</label>
                                            <select type="select" id="" name="interest_rate_period" class="" required>
                                                    <option value="">Choose Interest Rate Period</option>
                                                    <option {{ old('interest_rate_period') == 'none' ? 'selected' : '' }}>None</option>
                                                    <option {{ old('interest_rate_period') == 'Daily' ? 'selected' : '' }}>Daily</option>
                                                    <option {{ old('interest_rate_period') == 'Weekly' ? 'selected' : '' }}>Weekly</option>
                                                    <option {{ old('interest_rate_period') == 'Two-Weeks' ? 'selected' : '' }}>Two-Weeks</option>
                                                    <option {{ old('interest_rate_period') == 'Monthly' ? 'selected' : '' }}>Monthly</option>
                                                    <option {{ old('interest_rate_period') == 'Yearly' ? 'selected' : '' }}>Yearly</option>
                                                </select>
                                            <div class="text-danger form-control-feedback">
                            {{$errors->first('interest_rate_period')}}                                        
                        </div>
                                        </div>
                                        </div>
                                            <div class="col-md-4">
                                                <div class="position-relative form-group">
                                                    <label for="" class="">Loan Term</label>
                                                    <select type="select" id="" name="loan_term" class="" required>
                                                    <option value="">Choose Loan Term</option>
                                                    <option {{ old('loan_term') == 'none' ? 'selected' : '' }}>None</option>
                                                    <option {{ old('loan_term') == 'Day' ? 'selected' : '' }}>Day</option>
                                                    <option {{ old('loan_term') == 'Week' ? 'selected' : '' }}>Week</option>
                                                    <option {{ old('loan_term') == 'Two-Weeks' ? 'selected' : '' }}>Two-Weeks</option>
                                                    <option {{ old('loan_term') == 'Month' ? 'selected' : '' }}>Month</option>
                                                    <option {{ old('loan_term') == 'Year' ? 'selected' : '' }}>Year</option>
                                                </select>
                                                    <div class="text-danger form-control-feedback">
                            {{$errors->first('loan_term')}}                                        
                        </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="position-relative form-group">
                                                    <label for="exampleState" class="">Status</label>
                                                    <select type="select" id="" name="status" class="" required>
                                                    <option value="">Choose Loan Term</option>
                                                    <option {{ old('status') == 'none' ? 'selected' : '' }}>None</option>
                                                    <option {{ old('status') == 'active' ? 'selected' : '' }}>Active</option>
                                                    <option {{ old('status') == 'unactive' ? 'selected' : '' }}>Unactive</option>
                                                   
                                                </select>
                                                    <div class="text-danger form-control-feedback">
                            {{$errors->first('status')}}                                        
                        </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <!-- <div class="position-relative form-check">
                                            <input name="check" id="exampleCheck" type="checkbox" class="form-check-input">
                                            <label for="exampleCheck" class="form-check-label">Active</label>
                                        </div> -->
                                        <button type="submit" class="mt-1 mb-2 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                                        <a href="{{url('loantypes/')}}" class="btn btn-secondary mb-2 mt-1">
                                            <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                                        </a>
                                    </form>
                                </div>
                                </div>


                                @endsection