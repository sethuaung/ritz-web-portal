@extends('layouts.app')
@section('content')

<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div>Loan Type Details          
                </div>
            </div>
            <div class="page-title-actions">                
                <div class="d-inline-block ">
                    <a href="{{url('loantypes/')}}" class="btn btn-info">
                        <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                    </a>                    
                </div>
            </div>    </div>
    </div>            <div class="main-card mb-3 card">
        <div class="card-body">
        <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Loan Type Name</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                    {{$loantype->name}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Description</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                    {{$loantype->description}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Interest Rate</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$loantype->interest_rate}}%
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Interest Rate Period</label>
                </div>
                <div class="col-md-9 mb-9">
                {{$loantype->interest_rate_period}}
                    </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Loan Term</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$loantype->loan_term}}
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-3 mb-3">
                    <label for="validationCustom01">Status</label>
                </div>
                <div  class="col-md-9 mb-9 ">
                {{$loantype->status }}
                </div>
            </div>
         
        </div>
    </div>
    
</div>
@endsection
