@extends('layouts.app')
@section('content')
<br>
<div class="col-md-12">
                                <div class="main-card mb-3 card">
                                    <div class="card-header">Active Loan Type
                                        <div class="btn-actions-pane-right">
                                            <div role="group" class="btn-group-sm btn-group">
                                            <a href="{{url('loantypes/create')}}" class="active btn btn-focus">
                           Add New Loan Type
                        </a>
                                                <!-- <button class="active btn btn-focus">Create New Loan</button> -->
                                            </div>
                                        </div>
                                    </div>
                                    @if(session('successMsg')!=NULL)
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Success!</strong> {{session('successMsg')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                                    <div class="table-responsive">
                                        <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">No</th>
                                                    <th class="text-center">Loan Type Name</th>
                                                    <th class="text-center">Description</th>
                                                    <th class="text-center">Interest Rate</th>
                                                    <th class="text-center">Status</th>
                                                    <th class="text-center">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($loantypes as $key=>$loantype)
                                            <tr>
                                <td class="text-center text-muted">{{$key+1}}</td>
                                <td class="text-center">{{ $loantype->name }}</td>
                                <td class="text-center">{{ $loantype->description }}</td>
                                <td class="text-center">{{ $loantype->interest_rate }}%</td>
                                <td class="text-center"> <div class="badge badge-warning">{{ $loantype->status }}</div></td>
                                
                                <td class="text-center">
                                    <a href="loantypes/view/{{$loantype->id}}">
                                        <button type="button" id="PopoverCustomT-1" class="btn btn-info btn-sm"><i class="pe-7s-look btn-icon-wrapper"></i></button>
                                    </a>
                                                                      
                                    <a href="loantypes/{{$loantype->id}}/edit">
                                        <button type="button" id="PopoverCustomT-1" class="btn btn-warning btn-sm"><i class="pe-7s-edit btn-icon-wrapper"></i></button>
                                    </a>
                                    <form action="{{ url('loantypes/destroy',['id'=>$loantype->id]) }}" method="POST" class="d-inline-block" onsubmit="return confirm('Are you sure want to delete?')">
                                        @csrf
                                        @method('DELETE')                                       
                                            <button type="submit" id="PopoverCustomT-1" class="btn btn-danger btn-sm"><i class="pe-7s-trash btn-icon-wrapper"> </i></button>                                        
                                    </form>  

                                </td>
                            </tr>
                            @endforeach


                                                <!-- <tr>
                                                    <td class="text-center text-muted">#345</td>
                                                    <td>
                                                        <div class="widget-content p-0">
                                                            <div class="widget-content-wrapper">
                                                                <div class="widget-content-left mr-3">
                                                                    <div class="widget-content-left">
                                                                        <img width="40" class="rounded-circle" src="assets/images/avatars/4.jpg" alt="">
                                                                    </div>
                                                                </div>
                                                                <div class="widget-content-left flex2">
                                                                    <div class="widget-heading">John Doe</div>
                                                                    <div class="widget-subheading opacity-7">Web Developer</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">Madrid</td>
                                                    <td class="text-center">
                                                        <div class="badge badge-warning">Pending</div>
                                                    </td>
                                                    <td class="text-center" style="width: 150px;">
                                                        <div class="pie-sparkline"><canvas width="45" height="45" style="display: inline-block; width: 45px; height: 45px; vertical-align: top;"></canvas></div>
                                                    </td>
                                                    <td class="text-center">
                                                        <button type="button" id="PopoverCustomT-1" class="btn btn-primary btn-sm">Details</button>
                                                    </td>
                                                </tr> -->
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="d-block text-center card-footer">
                                    <div class="col-md-12 col-sm-12 grid-margin mt-2">
                        <div class="">
                        @if ($loantypes->lastPage() > 1)
                            <ul class="pagination d-flex flex-wrap justify-content-center ">
                                <li class="page-item {{ ($loantypes->currentPage() == 1) ? ' disabled' : '' }}">
                                    <a class="page-link" href="{{ $loantypes->url(1) }}">Previous</a>
                                </li>
                                @for ($i = 1; $i <= $loantypes->lastPage(); $i++)
                                    <li class="page-item {{ ($loantypes->currentPage() == $i) ? ' active' : '' }}">
                                        <a class="page-link" href="{{ $loantypes->url($i) }}">{{ $i }}</a>
                                    </li>
                                @endfor
                                <li class="page-item {{ ($loantypes->currentPage() == $loantypes->lastPage()) ? ' disabled' : '' }}">
                                    <a class="page-link" href="{{ $loantypes->url($loantypes->currentPage()+1) }}" >Next</a>
                                </li>
                            </ul>
                            @endif
                        </div>
                    </div>

                                     
                                    </div>
                                </div>
                            </div>
                            @endsection