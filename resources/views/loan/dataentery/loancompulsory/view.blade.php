@extends('layouts.app')
@section('content')

    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Loan Compulsory Details View
                    </div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{url('loancompulsory/')}}" class="btn theme-color text-white">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-card mb-3 card">
            <div class="card-body">
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Branch</label>
                    </div>
                    <div class="col-md-9 mb-9 ">
                        : {{$compulsory_product->branch_name}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Compulsory Code</label>
                    </div>
                    <div class="col-md-9 mb-9 ">
                        : {{$compulsory->compulsory_code}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Name</label>
                    </div>
                    <div class="col-md-9 mb-9 ">
                        : {{$compulsory->compulsory_name}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Description</label>
                    </div>
                    <div class="col-md-9 mb-9 ">
                        : {{$compulsory->compulsory_description}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Saving Amount</label>
                    </div>
                    <div class="col-md-9 mb-9 ">
                        : {{$compulsory->saving_amount}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Interest Rate</label>
                    </div>
                    <div class="col-md-9 mb-9 ">
                        : {{$compulsory->interest_rate}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Charge Option</label>
                    </div>
                    <div  class="col-md-9 mb-9 ">
                        {{-- {{$loancharge->charge_option}} --}}
                        @if ($compulsory->charge_option == 1)
                            : Fixed Amount
                        @else
                            : Loan Amount
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Accounting</label>
                    </div>
                    <div class="col-md-9 mb-9 ">
                        : {{$account_type->code}} - {{$account_type->name}}
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-3 mb-3">
                        <label for="validationCustom01" class="font-weight-bold">Status</label>
                    </div>
                    <div class="col-md-9 mb-9 text-capitalize">
                        : {{$compulsory->status}}
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
