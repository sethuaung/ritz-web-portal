@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Compulsory Edit</div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{url('loancompulsory/')}}" class="btn theme-color text-white">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alart alart-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="needs-validation" method="POST" action="{{route('loancompulsory.update', $compulsory->compulsory_code)}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="position-relative form-group form-row">
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Branch</label>
                            <select type="select" id="branch_id" name="branch_id" class="form-control">
                                <option value="">Choose Branch</option>
                                @foreach ($branches as $branch)
                                    <option value="{{$branch->id}}" @if ($compulsory_product->branch_id == $branch->id) selected="selected" @endif>{{$branch->branch_name}}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('product_type')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Compulsory Code</label>
                            <input type="text" class="form-control" id="" name="compulsory_code" value="{{$compulsory->compulsory_code}}" placeholder="Example : AB-C-12345" required>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('compulsory_code')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Name</label>
                            <input type="text" class="form-control" id="" name="compulsory_name" value="{{$compulsory->compulsory_name}}" placeholder="" required>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('compulsory_code')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Description</label>
                            <input type="text" class="form-control" id="" name="compulsory_description" value="{{$compulsory->compulsory_description}}" placeholder="" required>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('compulsory_code')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Saving Amount</label>
                            <input type="text" class="form-control" id="" name="saving_amount" value="{{$compulsory->saving_amount}}" placeholder="" required>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('compulsory_code')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Interest Rate</label>
                            <input type="text" class="form-control" id="" name="interest_rate" value="{{$compulsory->interest_rate}}" placeholder="" required>
                            <div class="text-danger form-control-feedback">

                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Accounting</label>
                            <select type="select" id="accounting_id" name="accounting_id" class="form-control">
                                <option value="">Choose a category</option>
                                @foreach ($accounts as $account)
                                    <option value="{{$account->id}}" @if ($account_type->accounting_id == $account->id) selected="selected" @endif>{{$account_type->code}} - {{$account->name}}</option>
                                @endforeach
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('accounting_id')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Charge Option </label>
                            {{-- <input type="text" class="form-control" id="" name="charge_option" value="{{$loancharge->charge_option}}" placeholder="" required> --}}
                            <select type="select" id="" name="charge_option" class="form-control">
                                <option value="test">Choose Charge Option</option>
                                <option value="1" {{($compulsory->charge_option=="1")?"selected":""}}>Fixed Amount</option>
                                <option value="2" {{($compulsory->charge_option=="2")?"selected":""}}>Of Loan Amount</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('charge_option')}}
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label style="font-weight: 600" for="validationCustom03">Status </label>
                            <select type="select" id="" name="status" class="form-control">
                                <option value="test">Choose Status</option>
                                <option value="none" {{($compulsory->status=="none")?"selected":""}}>None</option>
                                <option value="active" {{($compulsory->status=="active")?"selected":""}}>Active</option>
                                <option value="unactive" {{($compulsory->status=="unactive")?"selected":""}}>Unactive</option>
                            </select>
                            <div class="text-danger form-control-feedback">
                                {{$errors->first('status')}}
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="m-1 btn btn-success"><i class="pe-7s-diskette btn-icon-wrapper"> Save & Back </i></button>
                    <a href="{{url('loancompulsory/')}}" class="btn btn-secondary m-1">
                        <i class="pe-7s-close-circle btn-icon-wrapper">Cancel</i>
                    </a>
                </div>
            </div>
        </form>
    </div>
    </div>
@endsection
