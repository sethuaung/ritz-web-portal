@extends('layouts.app')
@section('content')

    <div id="printDeposit">


    </div>

    <div class="card m-3">
        <div class="card-header d-flex justify-content-between">
            Loan deposit
            <a href="{{ url('group-deposit') }}" class="btn btn-info">
                <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
            </a>
        </div>
        <div class="card-body">

            <form action="{{ route('groupdeposit.store') }}" method="POST">
                <div class="form-row">
                    @csrf

                    @foreach ($total_loans as $key => $loan)
                        <input type="text" name="loan_id_{{ $key + 1 }}" value="{{ $loan }}" class="d-none">
                    @endforeach

                    <input type="hidden" name="count" value="{{ $count }}">

                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_date">Deposit Pay Date<span style="color: red">
                                *</span></label>
                        <input type="date" name="deposit_pay_date" id="loan_deposit_date" class="form-control" required>
                    </div>

                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_ref_no">Reference number<span style="color: red">
                                *</span></label>
                        <input type="text" name="ref_no" id="loan_deposit_ref_no" class="form-control" required>
                    </div>

                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_invoice_no">Invoice Number<span
                                style="color: red"> *</span></label>
                        <input type="text" name="invoice_no" id="loan_deposit_invoice_no" class="form-control"
                            maxlength="100" required>
                    </div>


                    {{-- compulsory --}}
                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label for="" style='font-weight: 600'>Compulsory</label>
                        <input type="text" value="{{ $compulsory_deposit }}" class="form-control" readonly>
                    </div>

                        


                    {{ $errors->first('cash_paid_bankaccount_id') }}

                    {{-- charge --}}
                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label for="" style='font-weight: 600'>Charge</label>
                        <input type="text" value="{{ $charge_deposit }}" class="form-control" readonly>
                    </div>


                    

                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_total_deposit">Total deposit</label>
                        <input type="text" name="total_deposit_balance" value="{{ $total_deposit }}"
                            id="loan_deposit_total_deposit" class="form-control" readonly required>
                    </div>

                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_pay_deposit">Pay deposit<span style="color: red">
                                *</span></label>
                        <input type="text" name="paid_deposit_balance" value="{{ $total_deposit }}"
                            id="loan_deposit_pay_deposit" class="form-control" required>
                    </div>

                </div>
        </div>

        <div class="col-12 mb-3">
            <label style="font-weight: 600" for="loan_deposit_note">Note</label>
            <textarea name="note" id="loan_deposit_note" class="form-control"></textarea>
        </div>

        <div class="col-12">
            <button class="btn btn-success">
                <i class="pe-7s-diskette btn-icon-wrapper">Pay deposit</i>
            </button>
        </div>
    </div>
    </form>
    </div>
    </div>

@endsection
            
@if ($errors->any())
    @section('script')

    @php
    $errlist = "";
        foreach($errors->all() as $e) {
            $errlist .= $e;
        }
    @endphp
    <script>
        errorAlert("{{ $errlist }}");
    </script>
    @endsection
@endif
