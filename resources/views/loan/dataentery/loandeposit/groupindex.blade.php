@extends('layouts.app')
@section('content')
    <div class="col-md-12">
        <div class="mb-3"></div>
        <div class="main-card mb-3 card">
            <div class="card-header">
                {{ $data_loanstitle }}
                <div class="btn-actions-pane-right">
                    <div role="group" class="btn-group-sm btn-group">
                
                        {{-- <a href="{{ url('loan/create') }}">
                            <button type="button" data-toggle="tooltip" title="" data-placement="bottom"
                                class="btn-shadow mr-1 btn theme-color text-light" data-original-title="Example Tooltip">
                                <i class="fa fa-plus"></i>
                                Add
                            </button>
                        </a>
                       <a href="{{url('loan?date=week')}}">
                           <button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-light" data-original-title="This Week">
                               <i class="fa fa-calendar"></i>
                               This Week
                           </button>
                       </a>
                       <a href="{{url('loan?date=month')}}">
                           <button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-light" data-original-title="This Month">
                           <i class="fa fa-calendar"></i>
                           This Month
                           </button>
                       </a>
                       <a href="{{url('loan?date=year')}}">
                           <button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-light" data-original-title="This Week">
                           <i class="fa fa-calendar"></i>
                           This Year
                           </button>
                       </a>
                       <a href="{{url('loan?date=date')}}">
                           <button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-1 btn theme-color text-light" data-original-title="Today">
                           <i class="fa fa-calendar"></i>
                           Today
                           </button>
                       </a> --}}
                    </div>
                </div>
            </div>

            <div class="widget-content p-2">
                <div class="widget-content-wrapper">
                    <form method="GET" id="depositSearch" class="d-flex">
                        <div>
                            <div class="widget-content-right mr-1">
                                <div class="widget-heading">
                                        <select name="type" style="max-width: 200px;" class="custom-select custom-select-sm form-control form-control-sm"
                                            id="depositSelectType">
                                            <option selected disabled>Choose type</option>
                                            @foreach ($types as $type)
                                                <option value="{{ $type->id }}"
                                                    @if (request()->get('type') == $type->id) selected @endif>
                                                    {{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                        </div>
        
                        <div>
                            {{-- <div class="widget-content-right mr-1 ml-1">
                                <label for="center" class="m-0">Center</label>
                            </div> --}}
                            <div class="widget-content-right mr-1">
                                <div class="widget-heading">
                                        <select name="center" class="custom-select custom-select-sm form-control form-control-sm"
                                            id="depositSelectCenter">
                                            <option selected disabled>Choose center</option>
                                            @foreach ($centers as $center)
                                                <option value="{{ $center->center_uniquekey }}"
                                                    @if (request()->get('center') == $center->center_uniquekey) selected @endif>
                                                    {{ $center->main_center_code }}</option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                        </div>
        
                        <div>
                            {{-- <div class="widget-content-right mr-1 ml-1">
                                <label for="group" class="m-0">Group</label>
                            </div> --}}
                            <div class="widget-content-right mr-1">
                                <div class="widget-heading">
                                        <select name="group" class="custom-select custom-select-sm form-control form-control-sm"
                                            id="depositSelectGroup">
                                            <option selected disabled>Choose group</option>
                                            @foreach ($groups as $group)
                                                <option value="{{ $group->group_uniquekey }}"
                                                    @if (request()->get('group') == $group->group_uniquekey) selected @endif>{{ $group->main_group_code }}
                                                </option>
                                            @endforeach
                                        </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="widget-content-left ml-2">
                            <button class="btn theme-color text-light" id="deposit_filter" name="depositfilter">Filter</button>
                        </div>
                    </form>



                    <div class="widget-content-right">
                        <div class="btn-group">
                            <form method="GET">
                                <div class="input-group">
                                    <input type="text" name="search_loanslist" id="search_loanslist"
                                        value="{{ request()->get('search_loanslist') }}" class="form-control mr-1"
                                        placeholder="Search..." aria-label="Search" aria-describedby="button-addon2">

                                    <button class="btn theme-color text-light mr-1" type="submit" id="button-addon2">
                                        <i class="fa fa-search"></i>
                                    </button>

                                    <a href="{{ url('group-deposit') }}" class="btn theme-color text-light">
                                        <i class="fa fa-refresh"></i>
                                    </a>
                                </div>
                            </form>
                        </div>

                    </div>

                    
                </div>
            {{-- </div>

            <div class="d-flex justify-content-start ml-3 mb-2"> --}}
                
            </div>


            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th>
                                <input type="checkbox" id="selectAllDeposit">
                            </th>
                            <th>Loan ID </th>
            				<th>Main Loan ID</th>
                            <th>Loan Officer</th>
                            <th>Client ID</th>
                            <th>Client</th>
                            <th>Guarantors</th>
                            <th>Loan Type</th>
                            <th>Status</th>
                            {{-- <th class="text-center">Actions</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        <form action="{{ url('groupdeposit/create') }}" method="POST" id="depositForm">
                            @csrf
                            {{-- <input type="date" name="approve_date" id="approve_date" class="d-none"> --}}
                            @foreach ($loans as $key => $loan)
                                <tr>
                                    <td class="text-center text-muted">{{ $key + 1 }}</td>
                                    <td>
                                        <input type="checkbox" class="checkbox selectDepositCheck"
                                            name="check_{{ $key + 1 }}" value="checked">
                                        <input type="hidden" name="check_{{ $key + 1 . '_value' }}"
                                            value="{{ $loan->loan_unique_id }}">
                                        <input type="hidden" name="count" value="{{ count($loans) }}">
                                    </td>

                                    <td>{{ $loan->loan_unique_id }}</td>
                            		<td>{{ $loan->main_loan_code != NULL ? $loan->main_loan_code : '' }}</td>
                                    <td>{{ $loan->loan_officer_name }}</td>
                                    <td>{{ DB::table('tbl_main_join_client')->where('portal_client_id', $loan->client_id)->first()->main_client_code ?? $loan->client_id }}</td>
                                    <td>{{ $loan->client_name }}</td>
                                    <td>

                                        @if ($loan->guarantor_a)
                                        <span class="badge badge-pill badge-secondary mt-2 theme-color">
                                            {{ DB::table('tbl_guarantors')->where('guarantor_uniquekey', $loan->guarantor_a)->first()->name }}
                                        </span>
                                        @endif
                                        <br>
                                        @if ($loan->guarantor_b)
                                            <span class="badge badge-pill badge-secondary mt-2 theme-color">
                                                {{ DB::table('tbl_guarantors')->where('guarantor_uniquekey', $loan->guarantor_b)->first()->name }}
                                            </span>
                                        @endif
                                        <br>
                                        @if ($loan->guarantor_c)
                                            <span class="badge badge-pill badge-secondary mt-2 theme-color">
                                                {{ DB::table('tbl_guarantors')->where('guarantor_uniquekey', $loan->guarantor_c)->first()->name }}
                                            </span>
                                        @endif
                                    </td>
                                    <td>{{ $loan->loan_type_name }}</td>
                                    <td>
                                        @if ($loan->disbursement_status == 'Pending')
                                            <div class="badge badge-warning">
                                                {{ $loan->disbursement_status }}
                                            </div>
                                        @endif
                                        @if ($loan->disbursement_status == 'Approved')
                                            <div class="badge badge-info">
                                                {{ $loan->disbursement_status }}
                                            </div>
                                        @endif
                                        @if ($loan->disbursement_status == 'Activated')
                                            <div class="badge badge-success">
                                                {{ $loan->disbursement_status }}
                                            </div>
                                        @endif
                                        @if ($loan->disbursement_status == 'Closed')
                                            <div class="badge badge-dark">
                                                {{ $loan->disbursement_status }}
                                            </div>
                                        @endif
                                        @if ($loan->disbursement_status == 'Deposited')
                                            <div class="badge badge-secondary">
                                                {{ $loan->disbursement_status }}
                                            </div>
                                        @endif
                                        @if ($loan->disbursement_status == 'Canceled')
                                            <div class="badge badge-danger">
                                                {{ $loan->disbursement_status }}
                                            </div>
                                        @endif
                                    </td>
                                    {{-- <td class="text-center text-nowrap">
                                    <a href="{{ route('loandeposit.create', $loan->loan_unique_id) }}" class="btn btn-sm btn-outline-success">Deposit</a>
                                    <a href="{{ route('loancancel', $loan->loan_unique_id) }}" onclick="return confirm('Are you sure?')" class="btn btn-sm btn-outline-danger">
                                        Cancel
                                    </a> --}}

                                    {{-- <a href="loan/view/{{$loan->loan_unique_id}}">
                                        <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn  btn-outline-info btn-sm"><i class="fa fa-eye"></i></button>
                                    </a>
                                    @if (request('disbursement_status') != 'Pending' && request('disbursement_status') != 'Activated')
                                    <a href="loan/{{$loan->loan_unique_id}}/edit">
                                        <button type="button" id="PopoverCustomT-1" class="border-0 btn-transition btn btn-outline-warning"><i class="fa fa-pencil" ></i></button>
                                    </a>
                                    @endif --}}
                                    {{-- </td> --}}

                                </tr>
                            @endforeach
                        </form>
                    </tbody>
                </table>
                <div class="widget-content-left my-2 text-right">
                    <button class="btn btn-sm btn-success" id="deposit_all">Deposit selected</button>
                </div>
            </div>
            <div class="d-block text-center card-footer">
                <div class="col-md-12 col-sm-12 grid-margin mt-2">
                    <div class="">
                        {{ $loans->appends($_GET)->links() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@if (session('successMsg') != null)
    @section('script')
        <script>
            statusAlert("{{ session('successMsg') }}");
        </script>
    @endsection
@endif
        
@if ($errors->any())
    @section('script')

    @php
    $errlist = "";
        foreach($errors->all() as $e) {
            $errlist .= $e;
        }
    @endphp
    <script>
        errorAlert("{{ $errlist }}");
    </script>
    @endsection
@endif
    
@if (session('check') != null)
@section('script')
    <script>
        errorAlert("{{ session('check') }}");
    </script>
@endsection
@endif
