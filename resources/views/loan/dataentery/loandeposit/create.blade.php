@extends('layouts.app')
@section('content')
    <div id="printDeposit">

        <div id="invoice-POS">
            <div id="top" style="display: flex; font-size: 12px; align-items: end; margin-bottom: 0px;">
                <div style="width: 50%; ">
                    <p>ချေးငွေအရာရှိ : <br><span
                            style="border-bottom: 1px solid grey">{{ DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->name }}</span>
                    </p>
                </div>
                <div style="width: 50%; text-align: right; line-height: 6px">
                    <p>Deposit</p>
                    <p>ရုံးခွဲ : <span
                            style="border-bottom: 1px solid grey">{{ DB::table('tbl_branches')->where('id',DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id)->first()->branch_name }}</span>
                    </p>
                    <p>ရက်စွဲ : <span style="border-bottom: 1px solid grey" class="date"></span></p>
                </div>
            </div>
            <div id="mid">
                <div class="info">
                    <h3 class="text-center"
                        style="text-align: center; font-size: 14px; border-bottom: 1px solid grey; margin-top: 0">
                        စရံငွေလုပ်ငန်းစဥ်</h3>
                    <div style="line-height: 6px;">
                        <p style="font-size: 12px;">
                            ချေးငွေအမှတ်စဥ် : {{ $loan->loan_unique_id }}
                        </p>
                        <p style="font-size: 12px;">
                            အမည် : {{ $loan->name }}
                        </p>

                        <p style="font-size: 12px;">
                            ချေးငွေပမဏ : {{ $loan->loan_amount }} ကျပ်
                        </p>

                        <p style="font-size: 12px;">
                            စရံ : {{ $charge_deposit }} ကျပ်
                        </p>

                        <p style="font-size: 12px;">
                            စုဆောင်းငွေ : {{ $compulsory_deposit }} ကျပ်
                        </p>

                        <p style="font-size: 12px;">
                            စုစုပေါင်းစရံ : {{ $total_deposit }} ကျပ်
                        </p>
                    </div>
                </div>
            </div>
            <!--End Invoice Mid-->


            <div id="bot" style="border-top: 1px solid grey">

                <div id="legalcopy" style="text-align: center">
                    <p class="legal" style="font-size: 6px">
                        <strong>Thank you for using mkt!</strong><br><br>
                        Myat Kyun Thar Microfinance (MKT)<br>
                        www.mkt.com.mm<br>
                        0912345677
                    </p>
                </div>
            </div>
            <!--End InvoiceBot-->
        </div>
        <!--End Invoice-->
    </div>

    <div class="card m-3">
        <div class="card-header d-flex justify-content-between">
            Loan deposit
            <a href="{{ url('loan?disbursement_status=Approved') }}" class="btn btn-info">
                <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
            </a>
        </div>
        <div class="card-body">

            <form action="{{ route('loandeposit.store') }}" method="POST" id="deposit">
                <div class="form-row">
                    @csrf
                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_id">Loan ID</label>
                        <input type="text" id="loan_deposit_id" value="{{ $loan->loan_unique_id }}" class="form-control"
                            readonly required>
                    </div>

                    <input type="hidden" name="loan_id" value="{{ $loan->loan_unique_id }}" class="form-control" required>

                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_date">Deposit Pay Date<span style="color: red">
                                *</span></label>
                        <input type="date" name="deposit_pay_date" id="loan_deposit_date" class="form-control" required>
                    </div>

                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_ref_no">Reference number<span style="color: red">
                                *</span></label>
                        <input type="text" name="ref_no" id="loan_deposit_ref_no" class="form-control" required>
                    </div>

                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_client_id">Client ID</label>
                        <input type="text" id="loan_deposit_client_id" value="{{ $loan->client_uniquekey }}"
                            class="form-control" readonly required>
                    </div>

                    <input type="hidden" name="client_id" value="{{ $loan->client_uniquekey }}" class="form-control"
                        required>

                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_client_name">Client Name</label>
                        <input type="text" name="client_name" id="loan_deposit_client_name" value="{{ $loan->name }}"
                            class="form-control" readonly required>
                    </div>

                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_client_nrc">Client NRC</label>
                        <input type="text" name="client_nrc" id="loan_deposit_client_nrc" value="{{ $loan->nrc }}"
                            class="form-control" readonly required>
                    </div>

                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_invoice_no">Invoice Number<span
                                style="color: red"> *</span></label>
                        <input type="text" name="invoice_no" id="loan_deposit_invoice_no" class="form-control"
                            maxlength="100" required>
                    </div>

                    

                    {{-- compulsory --}}
                    @foreach ($compulsory as $com)
                        <div class="col-12 col-md-6 col-lg-4 mb-3">
                            <label style="font-weight: 600"
                                for="{{ $com->compulsory_name }}">{{ $com->compulsory_name }}</label>
                            <input type="text" name="compulsory_saving" id="{{ $com->compulsory_name }}"
                                value="{{ $com->charge_option == '2' ? $loan->loan_amount * ($com->saving_amount / 100) : $com->saving_amount }}"
                                class="form-control" readonly>
                        </div>
                    @endforeach

                   

                    {{-- charge --}}
                    @foreach ($charge as $c)
                        <div class="col-12 col-md-6 col-lg-4 mb-3">
                            <label style="font-weight: 600"
                                for="{{ $c->charge_name }}">{{ $c->charge_option == '2' ? $c->charge_name : $c->charge_name }}</label>
                            <input type="text" name="charges" id="{{ $c->charge_name }}"
                                value="{{ $c->charge_option == '2' ? $loan->loan_amount * ($c->amount / 100) : $c->amount }}"
                                class="form-control" readonly>
                        </div>
                    @endforeach

                    

                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_total_deposit">Cash Out Acc ID</label>
                        <select type="select" id="cash_acc_id" name="cash_acc_id" class="form-control">
                            <option value="">Choose a category</option>
                            @foreach ($accounts as $acc)
                                <option value="{{ $acc->id }}" selected>{{ $acc->external_acc_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_total_deposit">Total deposit</label>
                        <input type="text" name="total_deposit_balance" value="{{ $total_deposit }}"
                            id="loan_deposit_total_deposit" class="form-control" readonly required>
                    </div>

                    <div class="col-12 col-md-6 col-lg-4 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_pay_deposit">Pay deposit<span
                                style="color: red"> *</span></label>
                        <input type="text" name="paid_deposit_balance" value="{{ $total_deposit }}"
                            id="loan_deposit_pay_deposit" class="form-control" required>
                    </div>

                    <div class="col-12 mb-3">
                        <label style="font-weight: 600" for="loan_deposit_note">Note</label>
                        <textarea name="note" id="loan_deposit_note" class="form-control"></textarea>
                    </div>

                    <div class="col-12">
                        <button class="btn btn-success">
                            <i class="pe-7s-diskette btn-icon-wrapper">Pay deposit</i>
                        </button>

                        <a class="btn btn-primary text-white" id="printThis">
                            <i class="fas fa-print"></i>
                            Print
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@if ($errors->any())
    @section('script')
        @php
            $errlist = '';
            foreach ($errors->all() as $e) {
                $errlist .= $e;
            }
        @endphp
        <script>
            errorAlert("{{ $errlist }}");
        </script>
    @endsection
@endif
