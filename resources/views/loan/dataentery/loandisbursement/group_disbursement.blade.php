@extends('layouts.app')
@section('content')
    <div class="col-md-12">
        <div class="mb-3"></div>
        <div class="main-card mb-3 card">
        <div class="card-header">
            {{ $data_loanstitle}}
               <div class="btn-actions-pane-right" hidden>
                   <div role="group" class="btn-group-sm btn-group">
                       <a href="{{url('loan/create')}}">
                           <button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-1 btn btn-primary" data-original-title="Example Tooltip">
                               <i class="fa fa-plus"></i>
                               Add
                           </button>
                       </a>
                       <a href="{{url('loan?date=week')}}">
                           <button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-1 btn btn-primary" data-original-title="This Week">
                               <i class="fa fa-calendar"></i>
                               This Week
                           </button>
                       </a>
                       <a href="{{url('loan?date=month')}}">
                           <button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-1 btn btn-primary" data-original-title="This Month">
                           <i class="fa fa-calendar"></i>
                           This Month
                           </button>
                       </a>
                       <a href="{{url('loan?date=year')}}">
                           <button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-1 btn btn-primary" data-original-title="This Week">
                           <i class="fa fa-calendar"></i>
                           This Year
                           </button>
                       </a>
                       <a href="{{url('loan?date=date')}}">
                           <button type="button" data-toggle="tooltip" title="" data-placement="bottom" class="btn-shadow mr-1 btn btn-primary" data-original-title="Today">
                           <i class="fa fa-calendar"></i>
                           Today
                           </button>
                       </a>
                   </div>
               </div>
           </div>

           <div class="widget-content p-2">
               <div class="widget-content-wrapper">
                   
                <form method="GET" id="disbursementsearchForm">
                    <div class="input-group">
                        <div class="widget-content-left ml-2">
                            <div class="widget-heading">
                                    <select name="loan_product" id="loan_product_disbursement_select" style="max-width: 200px;" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm">
                                        <option value="" selected disabled>Choose Type</option>
                                        @foreach($loan_types as $loan_type)
                                            <option value="{{$loan_type->id}}" @if(request()->loan_product == $loan_type->id) selected @endif>{{$loan_type->name}}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        <div class="widget-content-left ml-2">
                            <div class="widget-heading">
                                    <select name="center" id="center_disbursement_select" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm">
                                        <option value="" selected disabled>Choose Center</option>
                                        @foreach($centers as $center)
                                            <option value="{{$center->center_uniquekey}}" @if(request()->center == $center->center_uniquekey) selected @endif>{{$center->main_center_code}}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        <div class="widget-content-left ml-2">
                            <div class="widget-heading">
                                    <select name="group" id="group_disbursement_select" aria-controls="example" class="custom-select custom-select-sm form-control form-control-sm">
                                        <option value="" selected disabled>Choose Group</option>
                                        @foreach($groups as $group)
                                            <option value="{{$group->group_uniquekey}}" @if(request()->group == $group->group_uniquekey) selected @endif>{{$group->main_group_code}}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        <button class="btn-sm btn theme-color text-light ml-2" type="submit" id="button-search">
                            Filter
                        </button>
                        {{-- <a href="{{url('/loan_disbursements/group')}}">
                            <button class="btn btn-warning ml-2"  id="button-refresh">
                                Refresh
                            </button>
                        </a> --}}
                    </div>
                    

                </form>
                   <div class="widget-content-right">
                       <div class="btn-group">
                           <!-- <label class="m-2"> Search </label> -->
                           <form method="GET" >
                               <div class="input-group">
                                   <input type="text" name="search_loanslist" id="search_loanslist"
                                       value="{{ request()->get('search_loanslist') }}"
                                       class="form-control mr-1"
                                       placeholder="Search..."
                                       aria-label="Search"
                                       aria-describedby="button-addon2"
                                       >

                                   <button class="btn theme-color text-light mr-1" type="submit" id="button-addon2">
                                       <i class="fa fa-search"></i>
                                   </button>

                                   <a href="{{url('/loan_disbursements/group')}}" class="btn theme-color text-light">
                                       <i class="fa fa-refresh"></i>
                                   </a>
                               </div>
                           </form>
                       </div>

                   </div>
               </div>
           </div>

            <div class="table-responsive pl-3 pr-3">
                <div class="tableFixHead">
                    <table class="align-middle mb-0 table table-bordered">
                        <thead>
                            <tr style="text-align: center">
                                <th><input type="checkbox" class="checkbox" name="" id="selectAllDisburse" value="checked"></th> 
                                <th>No</th>                               
                                <th>Loan ID </th>
                    			<th>Main Loan ID</th>
                                <th>Loan Officer</th>
                                <th>Client ID</th>
                                <th>Client</th>
                                <th>Guarantors</th>
                                <th>Loan Type</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <form action="{{ route('loan_disbursements.group_disburse_create') }}" method="POST" id="disburseForm">
                                @csrf
                                @foreach($loans as $key=>$loan)
                                <tr style="text-align: center">
                                    <td>
                                        <input type="checkbox" class="checkbox" name="check_{{ $key+1 }}" id="selectDisburseCheck" value="checked">
                                        {{-- <input type="checkbox" class="checkbox" name="disCheck" id="selectDisburseCheck" value="{{ $loan->loan_unique_id }}"> --}}
                                        <input type="hidden" name="check_{{ ($key+1)."_value" }}" value="{{ $loan->loan_unique_id }}">
                                        <input type="hidden" name="count" value="{{ count($loans) }}">
                                    </td>
                                    <td class="text-center text-muted">{{$key+1}}</td>

                                    <td>{{ $loan->loan_unique_id }}</td>
                                    <td>{{ $loan->main_loan_code != NULL ? $loan->main_loan_code : '' }}</td>
                                    <td>{{ $loan->loan_officer_name }}</td>
                                    <td>{{  DB::table('tbl_main_join_client')->where('portal_client_id', $loan->client_id)->first()->main_client_code ?? $loan->client_id }}</td>
                                    <td>{{ $loan->client_name }}</td>
                                    <td>
        
                                        @if($loan->guarantor_a)
                                        <span class="badge badge-pill badge-secondary mt-2 theme-color">
                                            {{ DB::table('tbl_guarantors')->where('guarantor_uniquekey', $loan->guarantor_a)->first()->name }}
                                        </span>
                                        @endif
                                        <br>
                                        @if($loan->guarantor_b)
                                        <span class="badge badge-pill badge-secondary mt-2 theme-color">
                                            {{ DB::table('tbl_guarantors')->where('guarantor_uniquekey', $loan->guarantor_b)->first()->name }}
                                        </span>
                                        @endif
                                        <br>
                                        @if($loan->guarantor_c)
                                        <span class="badge badge-pill badge-secondary mt-2 theme-color">
                                            {{ DB::table('tbl_guarantors')->where('guarantor_uniquekey', $loan->guarantor_c)->first()->name }}
                                        </span>
                                            @endif
                                    </td>
                                    <td>{{ $loan->loan_type_name}}</td>
                                        <td>
                                        
                                        @if($loan->disbursement_status == "Deposited")
                                            <div class="badge badge-secondary">
                                                {{ $loan->disbursement_status }}
                                            </div>
                                        @endif
                                    </td>    
                                </tr>
                                @endforeach
                            </form>
                        </tbody>
                    </table>
                    <div class="widget-content p-2">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-right">
                                <button id="disburse_selected" class="btn btn-success">
                                    Disburse Selected
                                </button>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-block text-center card-footer">
            <div class="col-md-12 col-sm-12 grid-margin mt-2">
                <div class="">
                     {{ $loans->appends($_GET)->links() }}

                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection

@if(session('successMsg')!=NULL)
@section('script')
    <script>
        statusAlert("{{session('successMsg')}}");
    </script>
@endsection
@endif
    
@if ($errors->any())
    @section('script')

    @php
    $errlist = "";
        foreach($errors->all() as $e) {
            $errlist .= $e."<br>";
        }
    @endphp
    <script>
        errorAlert("{{ $errlist }}");
    </script>
    @endsection
@endif

@if (session('check') != null)
@section('script')
    <script>
        errorAlert("{{ session('check') }}");
    </script>
@endsection
@endif
