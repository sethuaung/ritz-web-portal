@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Approved Loans(Disbursement Page)</div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{ url('loan?disbursement_status=Deposited') }}" class="btn btn-info">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>


        <form class="needs-validation" id="disbursementForm" method="POST" action="{{ route('loan_disbursements.store') }}"
            enctype="multipart/form-data">
            @csrf
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="form-row">
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Loan ID</label>
                                <input name="loan_id" id="loan_id" placeholder="0"
                                    value="{{ $disbursement_loan->loan_unique_id }}" type="text" class="form-control"
                                    readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('loan_id') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Client Name</label>
                                <input name="client_name" id="client_name" placeholder="0"
                                    value="{{ $disbursement_loan->client_name }}" type="text" class="form-control"
                                    readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('client_name') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Loan Officer</label>
                                <input name="loan_officer" id="loan_officer" placeholder="0"
                                    value="{{ $disbursement_loan->loan_officer_name }}" type="text" class="form-control"
                                    readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('loan_officer') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Loan Type</label>
                                <input name="loan_type" id="loan_type" placeholder="0"
                                    value="{{ $disbursement_loan->loan_type_name }}" type="text" class="form-control"
                                    readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('loan_type') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Center ID</label>
                                <input name="center_id" id="center_id" placeholder="0"
                                    value="{{ $disbursement_loan->center_uniquekey }}" type="text" class="form-control"
                                    readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('center_id') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Group ID</label>
                                <input name="group_id" id="group_id" placeholder="0"
                                    value="{{ $disbursement_loan->group_uniquekey }}" type="text" class="form-control"
                                    readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('group_id') }}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Branch</label>
                                <input name="branch" id="branch" placeholder="0"
                                    value="{{ $disbursement_loan->branch_name }}" type="text" class="form-control"
                                    readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('branch') }}
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="col-md-4" style="display: none">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Cash Out Account ID <span
                                        style="color: red"> *</span></label>
                                <select type="select" id="cash_acc_id" name="cash_acc_id" class="form-control"
                                    required>
                                    <option value="">Choose an account</option>
                                    @foreach ($service_receivable_acccode as $account)
                                        <option value="{{ $account->id }}" selected>{{ $account->sub_acc_code }} -
                                            {{ $account->sub_acc_name }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('cash_acc_id') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Cash Out Account ID <span
                                        style="color: red"> *</span></label>
                                <select type="select" id="cashout_acc_id" name="cashout_acc_id" class="form-control"
                                    required>
                                    <option value="">Choose an account</option>
                                    @foreach ($accounts as $acc)
                                        <option value="{{ $acc->id }}" selected>{{ $acc->external_acc_name }}
                                        </option>
                                    @endforeach
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('cash_acc_id') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Loan Request Amount</label>
                                <input name="loan_amount" id="loan_amount" placeholder="0"
                                    value="{{ $disbursement_loan->loan_amount }}" type="number" class="form-control"
                                    readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('loan_amount') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Paid Deposit Amount</label>
                                <input name="paid_deposit_balance" id="paid_deposit_balance" placeholder=""
                                    value="{{ $disbursement_loan->paid_deposit_balance }}" type="number"
                                    class="form-control" readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('paid_deposit_balance') }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Total Disbursement Amount
                                    <span style="color: red">*</span></label>
                                <input name="disbursed_amount" id="disbursed_amount" placeholder="0"
                                    value="{{ $disbursement_loan->loan_amount }}" type="number" class="form-control"
                                    required>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('disbursed_amount') }}
                                </div>
                            </div>
                        </div>

                        {{-- <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Interest Rate</label>
                                <input name="interest" id="interest" placeholder="0"
                                       value="{{ $interest_rate->interest_rate }}" type="number" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('interest')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Penalty</label>
                                <input name="penalty" id="penalty" placeholder="0"
                                       value="{{ old('penalty') }}" type="number" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('penalty')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Service Charges</label>
                                <input name="service_charges" id="service_charges" placeholder="0"
                                       value="{{ old('service_charges') }}" type="number" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('service_charges')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Total Loan Balance</label>
                                <input name="total_balance" id="total_balance" placeholder="0"
                                       value="{{ $disbursement_loan->loan_amount }}" type="number" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('total_balance')}}
                                </div>
                            </div>
                        </div> --}}

                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Paid By <span
                                        style="color: red">*</span></label>
                                <select type="select" id="paid_by" name="paid_by" class="custom-select" required>
                                    <option value="cash">Cash</option>
                                    <option value="online">Online</option>
                                    <option value="banktransfer">Bank Transfer</option>
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('paid_by') }}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Cash Pay Amount <span
                                        style="color: red"> *</span></label>
                                <input name="cash_pay" id="cash_pay" placeholder="0"
                                    value="{{ $disbursement_loan->loan_amount }}" type="number" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('cash_pay') }}
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">First Installment
                                    Date<span style="color: red"> *</span></label>
                                <input name="first_installment_date" id="first_installment_date" placeholder=""
                                    value="{{ date('Y-m-d', strtotime($first_installment_date)) }}" type="date" class="form-control" required>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('first_installment_date') }}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Disbursement Status</label>
                                <input name="disbursement_status" id="disbursement_status" placeholder="0"
                                    value="{{ $disbursement_loan->disbursement_status }}" type="text"
                                    class="form-control" readonly>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('disbursement_status') }}
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="loan_disbursement_date">Disbursement Date<span
                                        style="color: red"> *</span></label>
                                <input name="loan_disbursement_date" id="loan_disbursement_date" placeholder=""
                                    value="{{ date('Y-m-d', strtotime($dis_date)) }}" type="date" class="form-control" required>
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('loan_disbursement_date') }}
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Remark</label>
                                <input name="remark" id="remark" placeholder="" value="{{ old('remark') }}"
                                    type="text" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{ $errors->first('remark') }}
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <button type="submit" class="mt-1 mb-2 btn btn-success">
                <i class="pe-7s-diskette btn-icon-wrapper"> Activate Loan </i></button>
            <a href="" class="btn btn-primary mb-2 mt-1" id="printDisBtn">
                <i class="fas fa-print">Print</i>
            </a>

        </form>
    </div>
    <div id="printDisbursement" style="display: none">
        <div id="invoice-POS">

            <div id="top" style="display: flex; font-size: 12px; align-items: end;">
                <div style="width: 50%;">
                    <p>ချေးငွေအရာရှိ : <br><span
                            style="border-bottom: 1px solid grey">{{ DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->name }}</span>
                    </p>
                </div>
                <div style="width: 50%; text-align: right; line-height: 6px">
                    <p>Disbursement</p>
                    <p>ရုံးခွဲ : <span
                            style="border-bottom: 1px solid grey">{{ DB::table('tbl_branches')->where('id',DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id)->first()->branch_name }}</span>
                    </p>
                    <p>ရက်စွဲ : <span style="border-bottom: 1px solid grey" class="date"></span></p>
                </div>
            </div>

            <div id="mid">
                <div class="info">
                    <h3 class="text-center"
                        style="text-align: center; font-size: 14px; border-bottom: 1px solid grey; margin-top: 0">
                        ချေးငွေလုပ်ငန်းစဥ်</h3>
                    <div style="line-height: 6px;">
                        <p style="font-size: 12px">
                            ချေးငွေအမှတ်စဥ် : {{ $disbursement_loan->loan_unique_id }}
                        </p>
                        <p style="font-size: 12px">
                            အမည် : {{ $disbursement_loan->client_name }}
                        </p>
                        <p style="font-size: 12px">
                            ချပေးငွေ : {{ $disbursement_loan->loan_amount }} ကျပ်
                        </p>
                    </div>
                </div>
            </div>
            <!--End Invoice Mid-->


            <div id="bot">

                <div id="legalcopy" style="text-align: center; border-top: 1px solid grey">
                    <p class="legal" style="font-size: 6px">
                        <strong>Thank you!</strong><br><br>
                        Myat Kyun Thar Microfinance (MKT)<br>
                        www.mkt.com.mm<br>
                        0912345677
                    </p>
                </div>
            </div>
            <!--End InvoiceBot-->
        </div>
        <!--End Invoice-->

    </div>
    </div>
@endsection

@if ($errors->any())
    @section('script')
        @php
            $errlist = '';
            foreach ($errors->all() as $e) {
                $errlist .= $e;
            }
        @endphp
        <script>
            errorAlert("{{ $errlist }}");
        </script>
    @endsection
@endif
