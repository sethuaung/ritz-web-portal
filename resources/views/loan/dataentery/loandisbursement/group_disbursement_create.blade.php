@extends('layouts.app')
@section('content')
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div>Disburse Loans by Group</div>
                </div>
                <div class="page-title-actions">
                    <div class="d-inline-block ">
                        <a href="{{url('/loan_disbursements/group')}}" class="btn btn-info">
                            <i class="pe-7s-back btn-icon-wrapper">Back To List</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <form class="needs-validation" id="groupdisbursementForm" method="POST" action="{{route('loan_disbursements.group_disburse_store')}}" enctype="multipart/form-data">
            @csrf
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="form-row">
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Loan ID</label>
                                <input name="loan_id" id="loan_id" placeholder="0"
                                       value="{{ $ids }}" type="text" class="form-control" readonly>
                                {{-- <input type="text" name="selected_ids" value="{{$selected_ids}}" hidden> --}}
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_id')}}
                                </div>
                            </div>
                        </div>
                        
                    
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Center ID</label>
                                <input name="center_id" id="center_id" placeholder="0"
                                       value="{{ $disbursement_loan[0]->center_id }}" type="text" class="form-control" readonly>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('center_id')}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Group ID</label>
                                <input name="group_id" id="group_id" placeholder="0"
                                       value="{{ $disbursement_loan[0]->group_id }}" type="text" class="form-control" readonly>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('group_id')}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Branch</label>
                                <input name="branch" id="branch" placeholder="0"
                                       value="{{ $disbursement_loan[0]->branch_name }}" type="text" class="form-control" readonly>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('branch')}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Total Loan Request Amount</label>
                                <input name="loan_amount" id="loan_amount" placeholder="0"
                                       value="{{ $total_principal }}" type="number" class="form-control" readonly>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_amount')}}
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Total Disbursement Amount <span style="color: red">*</span></label>
                                <input name="disbursed_amount" id="disbursed_amount" placeholder="0"
                                       value="{{ $total_principal }}" type="number" class="form-control" required>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('disbursed_amount')}}
                                </div>
                            </div>
                        </div>
                        
                       
                        
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Paid By <span style="color: red">*</span></label>
                                {{-- <input name="paid_by" id="paid_by" placeholder="0"
                                       value="{{ old('paid_by') }}" type="text" class="form-control"> --}}
                                <select type="select" id="paid_by" name="paid_by" class="custom-select" required>
                                    <option value="cash">Cash</option>
                                    <option value="online">Online</option>
                                    <option value="banktransfer">Bank Transfer</option>
                                </select>
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('paid_by')}}
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="" class="">Cash Pay Amount <span style="color: red"> *</span></label>
                                <input name="cash_pay" id="cash_pay" placeholder="0"
                                value="{{ $total_principal }}" type="number" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('cash_pay')}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="position-relative form-group">
                                <label style="font-weight: 600" for="loan_disbursement_date">Disbursement Date<span style="color: red"> *</span></label>
                                <input type="date" name="loan_disbursement_date" id="loan_disbursement_date" class="form-control">
                                <div class="text-danger form-control-feedback">
                                    {{$errors->first('loan_disbursement_date')}}
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="mt-1 mb-2 btn btn-success">
                <i class="pe-7s-diskette btn-icon-wrapper"> Activate Loans </i></button>
            {{-- <a href="" class="btn btn-primary mb-2 mt-1" id="printDisBtn">
                <i class="fas fa-print">Print</i>
            </a> --}}

        </form>
    </div>
    
        
    </div>
@endsection
        
@if ($errors->any())
    @section('script')

    @php
    $errlist = "";
        foreach($errors->all() as $e) {
            $errlist .= $e;
        }
    @endphp
    <script>
        errorAlert("{{ $errlist }}");
    </script>
    @endsection
@endif
