@extends('layouts.app')
@section('content')

    <div class="col-md-12">
        <div class="mb-3"></div>
        <div class="main-card mb-3 card">
            <div class="card-header">

                Loan Deposit Reverse Crawl Information
            </div>


            @if(session('successMsg')!=NULL)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('successMsg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table  table-striped table-hover" id="clientReport">
                    <thead>
                        <tr>
                            <th> No </th>
                            <th scope="col"> Module </th>
                            <th scope="col"> Route Name </th>
                            <th scope="col"> Data Count </th>
                            <th scope="col"> Status </th>
                            <th scope="col"> Crawl Date </th>
                            <th scope="col" class="text-center"> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=1; @endphp
                    @foreach ($reverse_crawl_deposits as $reverse_crawl_deposit)
                        <tr>
                            <td class="text-center text-muted">{{$i++}}</td>
                            <td>{{$reverse_crawl_deposit->module_name}}</td>
                            <td>{{$reverse_crawl_deposit->route_name}}</td>
                            <td id="depositReverseCrawlCount">{{$reverse_crawl_deposit->data_count}}</td>
                            <td>
                                <span class="badge badge-pill badge-warning px-3">{{$reverse_crawl_deposit->status}}</span>
                            </td>
                            <td>{{$reverse_crawl_deposit->crawl_date}}</td>
                            <td class="text-center text-nowrap">
                                <a href="" >
                                    <button class="btn-transition btn btn-outline-primary" id="depositReverseCrawl">Start
                                    </button>
                                </a>
                                <a href="reverse_client_crawl/">
                                    <button class="btn-transition btn btn-outline-success">Finish
                                    </button>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-inline text-center card-footer">
                {{-- <div class="">
                    @if ($crawl_clients->lastPage() > 1)
                        <ul class="pagination d-flex flex-wrap justify-content-center ">
                            <!-- <i class="pe-7s-prev btn-icon-wrapper"></i> -->
                            <li class="page-item {{ ($crawl_clients->currentPage() == 1) ? ' disabled' : '' }}">
                                <a class="page-link" href="{{ $crawl_clients->url(1) }}">Previous</a>
                            </li>
                            @for ($i = 1; $i <= $crawl_clients->lastPage(); $i++)
                                <li class="page-item {{ ($crawl_clients->currentPage() == $i) ? ' active' : '' }}">
                                    <a class="page-link" href="{{ $crawl_clients->url($i) }}">{{ $i }}</a>
                                </li>
                            @endfor
                            <li class="page-item {{ ($crawl_clients->currentPage() == $crawl_clients->lastPage()) ? ' disabled' : '' }}">
                                <a class="page-link" href="{{ $crawl_clients->url($crawl_clients->currentPage()+1) }}" >Next</a>
                            </li>
                        </ul>
                    @endif
                </div> --}}
            </div>
        </div>
    </div>
@endsection

