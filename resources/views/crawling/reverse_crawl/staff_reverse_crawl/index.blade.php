@extends('layouts.app')
@section('content')

    <div class="col-md-12">
        <div class="mb-3"></div>
        <div class="main-card mb-3 card">
            <div class="card-header">

                Staff Reverse Crawl Information
            </div>


            @if(session('successMsg')!=NULL)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('successMsg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table  table-striped table-hover" id="clientReport">
                    <thead>
                    <tr>
                        <th> No </th>
                        <th scope="col"> Module </th>
                        <th scope="col"> Route Name </th>
                        <th scope="col"> Data Count </th>
                        <th scope="col"> Status </th>
                        <th scope="col"> Crawl Date </th>
                        <th scope="col" class="text-center"> Action </th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $i=1; @endphp
                    @foreach ($reverse_staffs as $reverse_staff)
                        <tr>
                            <td class="text-center text-muted">{{$i++}}</td>
                            <td>{{$reverse_staff->module_name}}</td>
                            <td>{{$reverse_staff->route_name}}</td>
                            <td id="staffReverseCrawlCount">{{$reverse_staff->data_count}}</td>
                            <td>
                                <span class="badge badge-pill badge-warning px-3">{{$reverse_staff->status}}</span>
                            </td>
                            <td>{{$reverse_staff->crawl_date}}</td>
                            <td class="text-center text-nowrap">
                                <a href="{{url('https://localhost/backup/mkt_webportal/api/clients/reverse')}}" >
                                    <button class="btn-transition btn btn-outline-primary" id="staffReverseCrawl">Start
                                    </button>
                                </a>
                                <a href="reverse_client_crawl/">
                                    <button class="btn-transition btn btn-outline-success">Finish
                                    </button>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-inline text-center card-footer">
            </div>
        </div>
    </div>
@endsection

