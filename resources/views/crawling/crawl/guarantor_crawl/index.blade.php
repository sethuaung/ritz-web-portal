@extends('layouts.app')
@section('content')

    <div class="col-md-12">
        <div class="mb-3"></div>
        <div class="main-card mb-3 card">
            <div class="card-header">

                Guarantor Crawl Information
            </div>

           
            @if(session('successMsg')!=NULL)
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('successMsg')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table  table-striped table-hover" id="clientReport">
                    <thead>
                        <tr>
                            <th> No </th>
                            <th scope="col"> Module </th>
                            <th scope="col"> Route Name </th>
                            <th scope="col"> Data Count </th>
                            <th scope="col"> Status </th>
                            <th scope="col"> Crawl Date </th>
                            <th scope="col" class="text-center"> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=1; @endphp
                    @foreach ($crawl_guarantors as $crawl_guarantor)
                        <tr>
                            <td class="text-group text-muted">{{$i++}}</td>
                            <td>{{$crawl_guarantor->module_name}}</td>
                            <td>{{$crawl_guarantor->route_name}}</td>
                            <td id="guarantorCrawlCount">{{$crawl_guarantor->data_count}}</td>
                            <td>
                                <span class="badge badge-pill badge-warning px-3">{{$crawl_guarantor->status}}</span>
                            </td>
                            <td>{{$crawl_guarantor->crawl_date}}</td>
                            <td class="text-center text-nowrap">
                                <a href="{{url('http://172.16.254.36/mkt_webportal/api/guarantors')}}" >
                                    <button class="btn-transition btn btn-outline-primary" id="guarantorCrawl">Start
                                    </button>
                                </a>
                                <a href="group_crawl/">
                                    <button class="btn-transition btn btn-outline-success">Finish
                                    </button>
                                </a>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-inline text-center card-footer">
                <div class="">
                </div>
            </div>
        </div>
    </div>
@endsection

