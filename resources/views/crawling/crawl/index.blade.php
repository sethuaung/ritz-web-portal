@extends('layouts.app')
@section('content')

    <div class="col-md-12">
        <div class="mb-3"></div>
        <div class="main-card mb-3 card">
            <div class="card-header">

                Crawl Information
            </div>
            <div class="table-responsive pl-3 pr-3">
                <table class="align-middle mb-0 table  table-striped table-hover" id="clientReport">
                    <thead>
                        <tr>
                            <th> No </th>
                            <th scope="col"> Module </th>
                            {{-- <th scope="col"> Route Name </th> --}}
                            <th scope="col"> Data Count </th>
                            <th scope="col"> Status </th>
                            <th scope="col"> Crawl Date </th>
                            <th scope="col" class="text-center"> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                    @php $i=1; @endphp
                    @foreach ($crawls as $crawl)
                    {{--<tr class="@if($crawl->module_name == 'saving_withdrawl') d-none @endif">--}}
                        <tr>
                            <td class="text-center text-muted">{{$i++}}</td>
                            <td class="text-capitalize">{{$crawl->module_name}}</td>
                            {{-- <td>{{$crawl->route_name}}</td> --}}
                            <td id="{{ $crawl->module_name.'CrawlCount' }}">{{$crawl->data_count}}</td>
                            <td>
                                <span class="badge badge-pill badge-success px-3">{{$crawl->status}}</span>
                            </td>
                            <td>{{$crawl->crawl_date}}</td>
                            <td class="text-center text-nowrap">
                                <a href="{{url('http://172.16.254.36/mkt_webportal/api/centers')}}" >
                                    <button class="btn-transition btn btn-outline-primary" id="{{ $crawl->module_name.'Crawl' }}">Start
                                    </button>
                                </a>                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-inline text-center card-footer">
                <div class="">
                </div>
            </div>
        </div>
    </div>
@endsection

