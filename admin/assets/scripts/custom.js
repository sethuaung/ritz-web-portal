  
    //hasjob
    $('#job_status').change(function(){
    var job_status = $(this).val(); 
    if(job_status == "Has Job"){
        document.getElementById("industry_id").disabled = false;
        document.getElementById("department_id").disabled = false;
        document.getElementById("job_position_id").disabled = false;
        document.getElementById("company_name").disabled = false;
        document.getElementById("working_phone").disabled = false;
        document.getElementById("salary").disabled = false;
        document.getElementById("experience").disabled = false;
        document.getElementById("company_address").disabled = false;
    }
    else{
        document.getElementById("industry_id").disabled = true;
        document.getElementById("department_id").disabled = true;
        document.getElementById("job_position_id").disabled = true;
        document.getElementById("company_name").disabled = true;
        document.getElementById("working_phone").disabled = true;
        document.getElementById("salary").disabled = true;
        document.getElementById("experience").disabled = true;
        document.getElementById("company_address").disabled = true;
        }
    });

   //has marital status

   $('#marital_status').change(function(){
    var marital_status = $(this).val(); 
    if(marital_status == "Married"){
        document.getElementById("spouse_name").disabled = false;
        document.getElementById("occupation").disabled = false;
        document.getElementById("no_of_children").disabled = false;

    }else if(marital_status == "Single"){
        document.getElementById("spouse_name").disabled = true;
        document.getElementById("occupation").disabled = true;
        document.getElementById("no_of_children").disabled = true;
    }else{
        document.getElementById("spouse_name").disabled = true;
        document.getElementById("occupation").disabled = true;

    }
    });


    $( document ).ready(function() {
        $('input').attr('autocomplete','off');

    //    need to fix for Center Edit
        var typestatus = $('#type_status').children(":selected").val();
        // console.log(typestatus);

        
    });
    // staff_create_autogenerate_start
    $('#branch_id').change(function(){
        var addr = $(this).find('option:selected').data('address');
        if(addr!=""){
        $('#staff_code').val(addr+'-');
        }
        else{
            $("#staff_code").val("");
        }
    });
    // staff_create_autogenerate_end

    // centercreate
    $('#branch_id_center').change(function(){
        var addr = $(this).find('option:selected').data('address');
        $('#center_uniquekey').val(addr+'-C-');
    });
    // centercreate_end

    // groupcreate
    $('#branch_id_group').change(function(){
        var addr = $(this).find('option:selected').data('address');
        $('#group_uniquekey').val(addr+'-G-');
    });
    // groupcreate_end

    $('#type_status').change(function(){
        $("#staff_client_id").empty();
    var type_status = $(this).val();  
    if(type_status){
        $.ajax({
        type:"GET",
        url:"{{url('getStaffClient')}}?type_status="+type_status,
        success:function(res){    
        if(res){
            console.log(res);
            $("#staff_client_id").empty();
            $("#staff_client_id").append('<option value="">Select Center Leader</option>');
            $.each(res,function(key,value){
            $("#staff_client_id").append('<option value="'+key+'">'+value+'</option>');
            });
        
        }else{
            $("#staff_client_id").empty();
        }
        }
        });
    }else{
        $("#staff_client_id").empty();
        $("#type_status").empty();
    }   
    });

    //   nrc
    $('#nrc_1').change(function(){
        $("#nrc_2").empty();
    var nrc_1 = $(this).val();  
    if(nrc_1){
        $.ajax({
        type:"GET",
        url:"{{url('getNRC')}}?nrc_1="+nrc_1,
        success:function(res){    
        if(res){
            $("#nrc_2").empty();
            console.log(res);
            
            $.each(res,function(key,value){
            $("#nrc_2").append('<option value="'+value.name_en+'">'+value.name_en+'</option>');
            });
        
        }else{
            $("#staff_client_id").empty();
        }
        }
        });
    }else{
        $("#staff_client_id").empty();
        $("#type_status").empty();
    }   
    });
    
    // group_client_staff_leader
    $('#level_status').change(function(){
        var level_status = $(this).val(); 
        if(level_status == "leader"){
    document.getElementById("client_id").disabled = true;
    }
    else{
        document.getElementById("client_id").disabled = false;
    }
    });
    // group_client_staff_leader_end


    $(document).ready(function() {
        
        // staff_create
        var OldValue_branch = "{{ old('branch_id') }}";
        if(OldValue_branch !== '') {
        $('#branch_id').val(OldValue_branch );
        }

            // township_create
        var OldValue_district = "{{ old('district_id') }}";
        if(OldValue_district !== '') {
        $('#district_id').val(OldValue_district );
        }

        // quarter_create
        var OldValue_township = "{{ old('township_id') }}";
        if(OldValue_township !== '') {
        $('#township_id').val(OldValue_township );
        }

        // department_create
        var OldValue_industry = "{{ old('industry_id') }}";
        if(OldValue_industry !== '') {
        $('#industry_id').val(OldValue_industry );
        }
    });



    $(document).ready(function() {
  
    var animating = false,
        submitPhase1 = 1100,
        submitPhase2 = 400,
        logoutPhase1 = 800,
        $login = $(".login"),
        $app = $(".app");
    
    function ripple(elem, e) {
        $(".ripple").remove();
        var elTop = elem.offset().top,
            elLeft = elem.offset().left,
            x = e.pageX - elLeft,
            y = e.pageY - elTop;
        var $ripple = $("<div class='ripple'></div>");
        $ripple.css({top: y, left: x});
        elem.append($ripple);
    };
    
    $(document).on("click", ".login__submit", function(e) {
        if (animating) return;
        animating = true;
        var that = this;
        ripple($(that), e);
        $(that).addClass("processing");
        setTimeout(function() {
        $(that).addClass("success");
        setTimeout(function() {
            $app.show();
            $app.css("top");
            $app.addClass("active");
        }, submitPhase2 - 70);
        setTimeout(function() {
            $login.hide();
            $login.addClass("inactive");
            animating = false;
            $(that).removeClass("success processing");
        }, submitPhase2);
        }, submitPhase1);
    });
    
    $(document).on("click", ".app__logout", function(e) {
        if (animating) return;
        $(".ripple").remove();
        animating = true;
        var that = this;
        $(that).addClass("clicked");
        setTimeout(function() {
        $app.removeClass("active");
        $login.show();
        $login.css("top");
        $login.removeClass("inactive");
        }, logoutPhase1 - 120);
        setTimeout(function() {
        $app.hide();
        animating = false;
        $(that).removeClass("clicked");
        }, logoutPhase1);
    });
    
    });

    // LoanSchedule
  
$(document).ready(function() {
   
    var input = document.getElementById("loan_amount");
        input.addEventListener("keyup", function () {
            calculate();
        });
        var input2 = document.getElementById("loan_term_value");
        input2.addEventListener("keyup", function () {
            calculate();
        });
    function calculate() {
    var loan_amount = Number(document.getElementById("loan_amount").value);
    var loan_term_value = Number(document.getElementById("loan_term_value").value);
    var today = new Date(); 
        if (loan_amount) {
            $.ajax({
                type: "GET",
                url: "{{url('getLoanSchedule')}}",
                data : {loan_amount:loan_amount,loan_term_value:loan_term_value,date:today},
                success: function (res) {
                
                    $("#myTableBody tr").empty();
                    if (res) {
                        $.each(res, function (key,value) {
                             key = key+1;
                            var html = '<tr>'+
                            '<td>' + key + '</td>' +
                            '<td>' + value.date + '</td>' +
                                        '<td>' + value.interest + '</td>' +
                                        '<td>' + value.principal + '</td>' +
                                        '<td>' + value.total + '</td>' +
                                        '<td>' + value.balance + '</td>' +
                                    '</tr>';   

                     $('#myTableBody tr').first().after(html);
                        });
                    } else {
                        $("#myTableBody tr").empty();
                    }
                },
            });
        } else {
          
        }
};

});

$('#industry_id').change(function(){
    var industry_id = $(this).val();  
    if(industry_id){
        $.ajax({
        type:"GET",
        url:"{{url('getDept')}}?industry_id="+industry_id,
        success:function(res){    
        if(res){
            $("#department_id").empty();
            $("#department_id").append('<option value="">Select Department</option>');
            $.each(res,function(key,value){
            $("#department_id").append('<option value="'+key+'">'+value+'</option>');
            });
        
        }else{
            $("#department_id").empty();
        }
        }
        });
    }else{
        $("#department_id").empty();
        $("#industry_id").empty();
    }   
    });
    //getJob
    $("#department_id").change(function () {
        var department_id = $(this).val();
        if (department_id) {
            $.ajax({
                type: "GET",
                url: "{{url('getJob')}}?department_id=" + department_id,
                success: function (res) {
                    if (res) {
                        $("#job_position_id").empty();
                        $("#job_position_id").append(
                            '<option value="">Select Job Position</option>'
                        );
                        $.each(res, function (key, value) {
                            $("#job_position_id").append(
                               '<option value="'+key+'">'+value+'</option>'
                            );
                        });
                    } else {
                        $("#job_position_id").empty();
                    }
                },
            });
        } else {
            $("#department_id").empty();
            $("#job_position_id").empty();
        }
    });


      //getDistrict
    $("#division_id").change(function () {
        var division_id = $(this).val();
        if (division_id) {
            $.ajax({
                type: "GET",
                url: "{{url('getDistrict')}}?division_id=" + division_id,
                success: function (res) {
                    if (res) {
                        $("#district_id").empty();
                        $("#district_id").append(
                            '<option value="">Select District</option>'
                        );
                        $.each(res, function (key, value) {
                            $("#district_id").append(
                               '<option value="'+key+'">'+value+'</option>'
                            );
                        });
                    } else {
                        $("#district_id").empty();
                    }
                },
            });
        } else {
            $("#division_id").empty();
            $("#district_id").empty();
        }
    });
    //getCity
    $("#division_id").change(function () {
        var division_id = $(this).val();
        if (division_id) {
            $.ajax({
                type: "GET",
                url: "{{url('getCity')}}?division_id=" + division_id,
                success: function (res) {
                    if (res) {
                        $("#city_id").empty();
                        $("#city_id").append(
                            '<option value="">Select City</option>'
                        );
                        $.each(res, function (key, value) {
                            $("#city_id").append(
                               '<option value="'+key+'">'+value+'</option>'
                            );
                        });
                    } else {
                        $("#city_id").empty();
                    }
                },
            });
        } else {
            $("#division_id").empty();
            $("#city_id").empty();
        }
    });
    //getTwonship
    $("#district_id").change(function () {
        var district_id = $(this).val();
        if (district_id) {
            $.ajax({
                type: "GET",
                url: "{{url('getTownship')}}?district_id=" + district_id,
                success: function (res) {
                    if (res) {
                        $("#township_id").empty();
                        $("#township_id").append(
                            '<option value="">Select Township</option>'
                        );
                        $.each(res, function (key, value) {
                            $("#township_id").append(
                               '<option value="'+key+'">'+value+'</option>'
                            );
                        });
                    } else {
                        $("#township_id").empty();
                    }
                },
            });
        } else {
            $("#township_id").empty();
            $("#district_id").empty();
        }
    });
    //getProvinnce
    $("#district_id").change(function () {
        var district_id = $(this).val();
        if (district_id) {
            $.ajax({
                type: "GET",
                url: "{{url('getProvince')}}?district_id=" + district_id,
                success: function (res) {
                    if (res) {
                        $("#province_id").empty();
                        $("#province_id").append(
                            '<option value="">Select Province</option>'
                        );
                        $.each(res, function (key, value) {
                            $("#province_id").append(
                               '<option value="'+key+'">'+value+'</option>'
                            );
                        });
                    } else {
                        $("#province_id").empty();
                    }
                },
            });
        } else {
            $("#township_id").empty();
            $("#district_id").empty();
        }
    });

    
    //getQuarter
    $("#township_id").change(function () {
        var township_id = $(this).val();
        if (township_id) {
            $.ajax({
                type: "GET",
                url: "{{url('getQuarter')}}?township_id=" + township_id,
                success: function (res) {
                    console.log(res);
                    if (res) {
                        $("#quarter_id").empty();
                        $("#quarter_id").append(
                            '<option value="">Select Quarter</option>'
                        );
                        $.each(res, function (key, value) {
                            $("#quarter_id").append(
                               '<option value="'+key+'">'+value+'</option>'
                            );
                        });
                    } else {
                        $("#quarter_id").empty();
                    }
                },
            });
        } else {
            $("#quarter_id").empty();
            $("#township_id").empty();
        }
    });
    //getVillage
    $("#township_id").change(function () {
        var township_id = $(this).val();
        if (township_id) {
            $.ajax({
                type: "GET",
                url: "{{url('getVillage')}}?township_id=" + township_id,
                success: function (res) {
                    if (res) {
                        $("#village_id").empty();
                        $("#village_id").append(
                            '<option value="">Select Village</option>'
                        );
                        $.each(res, function (key, value) {
                            $("#village_id").append(
                               '<option value="'+key+'">'+value+'</option>'
                            );
                        });
                    } else {
                        $("#village_id").empty();
                    }
                },
            });
        } else {
            $("#village_id").empty();
            $("#township_id").empty();
        }
    });
