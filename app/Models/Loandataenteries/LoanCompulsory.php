<?php

namespace App\Models\Loandataenteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanCompulsory extends Model
{
    use HasFactory;
    protected $table = 'loan_compulsory_type';
    protected $guarded =['id'];
}
