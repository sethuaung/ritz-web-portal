<?php

namespace App\Models\Loandataenteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanCompulsoryProduct extends Model
{
    use HasFactory;
    protected $table = 'loan_compulsory_product';
    protected $guarded =['id'];
}
