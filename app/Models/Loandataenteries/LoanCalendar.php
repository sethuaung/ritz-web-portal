<?php

namespace App\Models\Loandataenteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanCalendar extends Model
{
    use HasFactory;
    protected $table = 'loan_custom_calendar';
    protected $guarded =['id'];
}
