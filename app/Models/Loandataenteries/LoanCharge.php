<?php

namespace App\Models\Loandataenteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanCharge extends Model
{
    use HasFactory;
    protected $table = 'loan_charges_type';
    protected $guarded =['id'];
}
