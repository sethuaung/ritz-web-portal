<?php

namespace App\Models\Loandataenteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanChargeType extends Model
{
    use HasFactory;
    protected $table = 'loan_charges_and_type';
    protected $guarded =['id'];
}
