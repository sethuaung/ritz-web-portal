<?php

namespace App\Models\Loandataenteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanType extends Model
{
    use HasFactory;
    protected $table = 'loan_type';
    protected $guarded =['id'];
}
