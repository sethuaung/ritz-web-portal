<?php

namespace App\Models\Loans;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class LoanJoin extends Model
{
    use HasFactory;
    protected $table = 'loan_charges_compulsory_join';
    protected $guarded =['id'];

	public function getTableColumns() {
        return collect(DB::select('describe '.$this->getTable()))->map(function ($item) {
            return $item;
        });
    }
}
