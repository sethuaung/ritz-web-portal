<?php

namespace App\Models\Loans;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Loan extends Model
{
    use HasFactory;
    protected $guarded =['id'];
    protected $table = 'loans';
    
    public function getTableColumns() {
        return collect(DB::select('describe '.$this->getTable()))->map(function ($item) {
            return $item;
        });
    }
}
