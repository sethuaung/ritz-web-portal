<?php

namespace App\Models\Loans;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RepaymentLate extends Model
{
    use HasFactory;
    protected $table = 'repayment_late';
    protected $guarded =['id'];

    public function getTableColumns() {
        return collect(DB::select('describe '.$this->getTable()))->map(function ($item) {
            return $item;
        });
    }
}
