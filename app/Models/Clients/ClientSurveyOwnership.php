<?php

namespace App\Models\Clients;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientSurveyOwnership extends Model
{
    use HasFactory;
    protected $table ='tbl_client_survey_ownership';
   
 	protected $fillable =[
        'client_uniquekey', 'assetstype_removable', 'assetstype_unremovable', 'purchase_price', 'current_value', 'quantity', 'attach_1', 'attach_2', 'attach_3', 'attach_4'
    ];
}
