<?php

namespace App\Models\Clients;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientPhoto extends Model
{
    use HasFactory;

    protected $table='tbl_client_photo';

    protected $fillable=[
        'client_uniquekey', 'client_photo', 'recognition_front', 'recognition_back', 'nrc_recommendation', 'recognition_status', 'registration_family_form_front', 'registration_family_form_back', 'finger_print_bio', 'government_recommendations_photo'
    ];
}
