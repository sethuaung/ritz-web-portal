<?php

namespace App\Models\Clients;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientJoin extends Model
{
    use HasFactory;
    protected $table='tbl_client_join';

    protected $fillable =[
        'client_id', 'branch_id', 'staff_id', 'guarantors_id', 'group_id', 'center_id', 'loan_id', 'deposit_id', 'disbursement_id', 'repayment_id', 'saving_withdrawal_id', 'client_status', 'created_at', 'updated_at', 'registration_date'
    ];
}
