<?php

namespace App\Models\Clients;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientJob extends Model
{
    use HasFactory;
    protected $table='tbl_client_job_main';

    protected $fillable =[
        'client_uniquekey', 'industry_id', 'job_status', 'job_position_id', 'department_id', 'experience', 'experience2', 'salary', 'company_name', 'company_phone', 'company_address', 'current_business', 'business_income'
    ];
}
