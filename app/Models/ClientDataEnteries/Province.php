<?php

namespace App\Models\ClientDataEnteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;
    protected $table ='tbl_provinces';
    protected $fillable=['district_id', 'province_name','province_name_mm'];

}
