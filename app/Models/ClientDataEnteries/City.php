<?php

namespace App\Models\ClientDataEnteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;
    protected $table ='tbl_cities';
    protected $fillable =['division_id','city_name','city_name_mm'];

}
