<?php

namespace App\Models\ClientDataEnteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    use HasFactory;
    protected $table = 'tbl_industry_type';
    protected $guarded =['id'];
    public function deptname(){
        return $this->hasMany(Department::class,'id');
    }
}
