<?php

namespace App\Models\ClientDataEnteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guarantor extends Model
{
    use HasFactory;
    protected $table = 'tbl_guarantors';
    protected $guarded =['guarantor_uniquekey', 'name', 'dob', 'nrc','old_nrc', 'nrc_card_id','gender', 'phone_primary', 'phone_secondary', 'email', 'blood_type', 'religion', 'nationality', 'education_id', 'other_education', 'village_id', 'province_id', 'quarter_id', 'township_id', 'district_id', 'division_id', 'city_id', 'address_primary', 'address_secondary', 'status'];
}
