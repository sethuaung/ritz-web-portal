<?php

namespace App\Models\ClientDataEnteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Model_Has_Permission extends Model
{
    use HasFactory;
    protected $table = 'model_has_permissions';
    protected $guarded =['id'];
}
