<?php

namespace App\Models\ClientDataEnteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;
    protected $table = 'tbl_group';
    protected $guarded =['group_uniquekey', 'staff_id', 'client_id', 'branch_id', 'level_status', 'del_status'];
}
