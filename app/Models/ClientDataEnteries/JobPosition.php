<?php

namespace App\Models\ClientDataEnteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobPosition extends Model
{
    use HasFactory;
    protected $table = 'tbl_job_position';
    protected $guarded =['id'];
    public function department(){
        return $this->belongsTo(Department::class,'department_id');
    }
}
