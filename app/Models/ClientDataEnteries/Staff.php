<?php

namespace App\Models\ClientDataEnteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory;
    protected $table = 'tbl_staff';
    protected $guarded =['id'];
    public function branchname(){
        return $this->belongsTo(Branch::class,'branch_id');
    }
}
