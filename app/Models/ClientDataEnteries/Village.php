<?php

namespace App\Models\ClientDataEnteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    use HasFactory;
    protected $table ='tbl_villages';
    protected $fillable =['twonship_province_id','village_name','village_name_mm'];
}
