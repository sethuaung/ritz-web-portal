<?php

namespace App\Models\ClientDataEnteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Center extends Model
{
    use HasFactory;
    protected $table = 'tbl_center';
    protected $guarded =['center_uniquekey', 'staff_client_id', 'branch_id', 'type_status', 'del_status'];
}
