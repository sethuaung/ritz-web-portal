<?php

namespace App\Models\ClientDataEnteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    use HasFactory;
    protected $table ='tbl_educations';
    protected $fillable =['education_name', 'education_name_mm'];

}
