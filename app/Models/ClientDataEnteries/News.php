<?php

namespace App\Models\ClientDataEnteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;
    protected $table ='tbl_news';
    protected $fillable =['title','author','department','company','description','photo'];
}
