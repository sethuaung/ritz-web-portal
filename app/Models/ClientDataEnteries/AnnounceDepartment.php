<?php

namespace App\Models\ClientDataEnteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnnounceDepartment extends Model
{
    use HasFactory;
    protected $table ='tbl_announcement_department';
    protected $fillable =['department_name'];
}
