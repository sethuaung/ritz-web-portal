<?php

namespace App\Models\ClientDataEnteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;
    protected $table = 'tbl_job_department';
    protected $fillable =['industry_id','department_name','department_name_mm'];
    protected $guarded =['id'];

    public function industryname(){
        return $this->belongsTo(Industry::class,'industry_id');
    }

    public function job_position()
    {
        return $this->hasMany(Job_position::class);
    }
    
}
