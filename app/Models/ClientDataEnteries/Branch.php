<?php

namespace App\Models\ClientDataEnteries;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    use HasFactory;
    protected $table = 'tbl_branches';

    protected $fillable =['branch_name', 'branch_code', 'client_prefix', 'branch_photo', 'title', 'phone_primary', 'phone_secondary', 'phone_tertiary', 'description', 'village_id', 'province_id', 'quarter_id', 'township_id', 'district_id', 'division_id', 'city_id', 'location', 'full_address', 'del_status'];

    protected $guarded =['id'];
}
