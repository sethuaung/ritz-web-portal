<?php

namespace App\Models\Crawl;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Crawl extends Model
{
    use HasFactory;
    protected $table = 'tbl_crawling';
    protected $guarded =['id'];
}
