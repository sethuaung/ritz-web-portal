<?php

namespace App\Models\Crawl;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Crawl_Group extends Model
{
    use HasFactory;
    protected $guarded =['id'];
    protected $table = 'tbl_main_join_group';
}
