<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\ClientDataEnteries\Staff;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Loan\LoanController;
use App\Models\Loans\LoanSchedule;

class LoanDisbursementController extends Controller
{
    public function getBranchCode()
    {
        $staff_code = Session::get('staff_code');
        // dd($staff_code);
        $B_code = DB::table('tbl_staff')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->select('tbl_branches.branch_code')
            ->where('tbl_staff.staff_code', $staff_code)
            ->first();
        $b_code = strtolower($B_code->branch_code);
        return $b_code;
    }

    public function index(Request $request)
    {
    }

    public function groupDisburse(Request $request)
    {
        $this->permissionFilter("group-disburse");
        $bcode = $this->getBranchCode();

        $BCODE = strtoupper($bcode);
        $branchid = DB::table('tbl_branches')->where('branch_code', '=', $BCODE)->get('id')->first();

        $data_loanstitle = 'Deposited Loans List';
        $loan_types = DB::table('loan_type')->get();
        $centers = DB::table('tbl_center')
            ->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', 'tbl_center.center_uniquekey')
            ->where('branch_id', '=', $branchid->id)
            ->get();
        $groups = DB::table('tbl_group')
            ->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', 'tbl_group.group_uniquekey')
            ->where('branch_id', '=', $branchid->id)
            ->get();

        $search_loan_product = $request->loan_product;
        $search_center = $request->center;
        $search_group = $request->group;


        $data_loanstitle = 'View All Loan by group';
        if (request()->has('search_loanslist')) {
            $searchvalue = request()->get('search_loanslist');
            $query = DB::table($bcode . '_loans')
                ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
                ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
                ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_a')
                ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode . '_loans.loan_unique_id')
                ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->orWhere(function ($query) {
                    $bcode = $this->getBranchCode();
                    $searchvalue = request()->get('search_loanslist');
                    $query->where($bcode . '_loans.loan_unique_id', 'LIKE', $searchvalue . '%')
                        ->orWhere('tbl_staff.name', 'LIKE', $searchvalue . '%')
                        ->orWhere('tbl_client_basic_info.name', 'LIKE', $searchvalue . '%')
                        ->orWhere('tbl_guarantors.name', 'LIKE', $searchvalue . '%')
                        ->orWhere('loan_type.name', 'LIKE', $searchvalue . '%')
                        ->orWhere('tbl_main_join_loan.main_loan_code', 'LIKE', $searchvalue . '%')
                        ->orWhere('tbl_main_join_client.main_client_code', 'LIKE', $searchvalue . '%')
                        ->orWhere('tbl_client_basic_info.client_uniquekey', 'LIKE', $searchvalue . '%');
                })
                ->where($bcode . '_loans.disbursement_status', 'Deposited');
            // ->where('tbl_main_join_loan.branch_id', DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id);
            $query->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name', 'tbl_main_join_loan.main_loan_code');
            $loans = $query->paginate(10);
        } else {
            $query = DB::table($bcode . '_loans')
                ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
                ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
                ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_a')
                ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode . '_loans.loan_unique_id')
                ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where($bcode . '_loans.disbursement_status', 'Deposited');
            // ->where('tbl_main_join_loan.branch_id', DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id);

            if ($search_loan_product) {
                $query->where($bcode . '_loans.loan_type_id', $search_loan_product);
            }
            if ($search_center) {
                $query->where($bcode . '_loans.center_id', $search_center);
            }
            if ($search_group) {
                $query->where($bcode . '_loans.group_id', $search_group);
            }

            $query->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name', 'tbl_main_join_loan.main_loan_code')->orderBy($bcode.'_loans.id', 'desc');
            $loans = $query->paginate(10);
        }

        // dd($loans);

        return view('loan.dataentery.loandisbursement.group_disbursement', compact('data_loanstitle', 'loan_types', 'centers', 'groups', 'loans'));
    }

    public function create($id)
    {
        $this->permissionFilter("loan-disbursements");
        $bcode = $this->getBranchCode();

        $disbursement_loan = DB::table($bcode . '_loans')
            ->leftJoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', $bcode . '_loans.branch_id')
            ->leftJoin('tbl_center', 'tbl_center.center_uniquekey', '=', $bcode . '_loans.center_id')
            ->leftJoin('tbl_group', 'tbl_group.group_uniquekey', '=', $bcode . '_loans.group_id')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
            ->leftJoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
            ->leftJoin($bcode . '_deposit', $bcode . '_deposit.loan_id', '=', $bcode . '_loans.loan_unique_id')
            ->where($bcode . '_loans.loan_unique_id', '=', $id)
            ->select($bcode . '_loans.*', 'loan_type.name AS loan_type_name', $bcode . '_deposit.paid_deposit_balance', 'loan_type.interest_rate', 'tbl_branches.branch_name', 'tbl_center.center_uniquekey', 'tbl_group.group_uniquekey', 'tbl_client_basic_info.client_uniquekey AS client_id', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name')
            ->first();


        $interest_rate = DB::table($bcode . '_loans')
            ->leftJoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
            ->where($bcode . '_loans.loan_unique_id', '=', $id)
            ->first();


        $sra = DB::table('accounts_loantypes')
            ->leftJoin('accounts', 'accounts.sub_acc_code', '=', 'accounts_loantypes.sub_acc_code')
            ->where('accounts_loantypes.loan_type_id', $disbursement_loan->loan_type_id)
            ->get();
        if ($sra->count() != 0) {
            $service_receivable_acccode = $sra;
        } else {
            $service_receivable_acccode = DB::table('accounts')->where('sub_acc_code', 'LIKE', '185' . '%')->get();
        }
        $this_branch_id = DB::table('tbl_staff')->where('staff_code', '=', Session::get('staff_code'))->select('branch_id')->first();
        $accounts = DB::table('account_chart_externals')->where('branch_id', '=', $this_branch_id->branch_id)->where('external_acc_code', 'LIKE', '1-11' . '%')->get();
        $date = DB::table($bcode . '_loans_schedule')->where('loan_unique_id', $disbursement_loan->loan_unique_id)->get('month')->first();
        $first_installment_date = date('Y-m-d h:m:s', strtotime($date->month));
        $dis_date = date('Y-m-d h:m:s', strtotime($date->month));
        // dd($disbursement_date);

        if (!isset($disbursement_loan->client_id)) {
            return redirect()->back()->withErrors("Client Unique Key Not Found! Cannot Disburse");
        } else {
            return view('loan.dataentery.loandisbursement.create', compact('accounts', 'id', 'service_receivable_acccode', 'disbursement_loan', 'interest_rate', 'first_installment_date', 'dis_date'));
        }
    }

    public function groupDisburseCreate(Request $request)
    {

        $this->permissionFilter("group-disburse");
        $bcode = $this->getBranchCode();

        $checkCount = array_count_values($request->all());
        if (array_key_exists('checked', $checkCount)) {

            $selected_ids = array();
            for ($x = 1; $x <= $request->count; $x++) {
                $name = "check_" . $x;
                $value = "check_" . $x . "_value";
                if ($request->$name === "checked") {
                    array_push($selected_ids, $request->$value);
                }
            }
            $ids = implode(" / ", $selected_ids);
            $disbursement_loan = DB::table($bcode . '_loans')
                ->leftJoin('tbl_branches', 'tbl_branches.id', '=', $bcode . '_loans.branch_id')
                ->whereIn($bcode . '_loans.loan_unique_id', $selected_ids)
                ->where($bcode . '_loans.disbursement_status', '=', 'Deposited')
                // ->orderBy($bcode.'_loans.id', 'asc')
                ->get();

            // dd($disbursement_loan);
            $count = count($disbursement_loan);
            $total_principal = 0;
            // dd($disbursement_loan[0]->loan_amount);

            for ($i = 0; $i < $count; $i++) {
                if (!isset($disbursement_loan[$i]->client_id)) {
                    return redirect()->back()->with('check', 'Client Unique Key Not Found In a Loan! Please check again!');
                } else {
                    $total_principal += $disbursement_loan[$i]->loan_amount;
                }
            }
            // dd($total_principal);
            return view('loan.dataentery.loandisbursement.group_disbursement_create', compact('disbursement_loan', 'ids', 'total_principal'));
        } else {
            return redirect()->back()->with('check', 'please check at least one loan');
        }
    }

    public function view($id)
    {
    }

    public function store(Request $request)
    {
        $this->permissionFilter("loan-disbursements");

        $bcode = $this->getBranchCode();

        $disbursement_loan = DB::table($bcode . '_loans')
            ->leftJoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', $bcode . '_loans.branch_id')
            ->leftJoin('tbl_center', 'tbl_center.center_uniquekey', '=', $bcode . '_loans.center_id')
            ->leftJoin('tbl_group', 'tbl_group.group_uniquekey', '=', $bcode . '_loans.group_id')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
            ->leftJoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
            ->leftJoin($bcode . '_deposit', $bcode . '_deposit.loan_id', '=', $bcode . '_loans.loan_unique_id')
            ->where($bcode . '_loans.loan_unique_id', '=', $request->loan_id)
            ->select($bcode . '_loans.*', $bcode . '_deposit.*', 'loan_type.*', 'tbl_client_basic_info.*', 'tbl_client_basic_info.client_uniquekey AS client_id', 'tbl_staff.*')
            ->first();

        // dd($disbursement_loan);

        $loan_id = DB::table($bcode . '_loans')
            ->where($bcode . '_loans.loan_unique_id', '=', $request->loan_id)
            ->first();

        // $acc_id = DB::table('accounts_loantypes')
        //     ->leftJoin('account_chart_external_details', 'account_chart_external_details.main_acc_code', '=', 'accounts_loantypes.sub_acc_code')
        //     ->leftJoin('account_chart_externals', 'account_chart_externals.external_acc_code', '=', 'account_chart_external_details.external_acc_code')
        //     ->where('accounts_loantypes.loan_type_id', $disbursement_loan->loan_type_id)
        //     ->first();
        // $this_branch_id = DB::table('tbl_staff')->where('staff_code', '=', Session::get('staff_code'))->select('branch_id')->first();
        // $accounts = DB::table('account_chart_externals')->where('branch_id', '=', $this_branch_id->branch_id)->where('external_acc_code', 'LIKE', '1-11' . '%')->first();

        //         $cash_acc_id = $accounts->id;
        //         $acc_code = [$acc_id->id, $cash_acc_id];

        //         for ($i = 0; $i < count($acc_code); $i++) {
        //             DB::table($bcode . '_disbursement')->insert([
        //                 'loan_id' => $loan_id->loan_unique_id,
        //                 'client_id' => $disbursement_loan->client_id,
        //                 'center_id' => $disbursement_loan->center_id,
        //                 'group_id' => $disbursement_loan->group_id,
        //                 'branch_id' => $disbursement_loan->branch_id,
        //                 'loan_officer_id' => $disbursement_loan->loan_officer_id,
        //                 'loan_product_id' => $disbursement_loan->loan_type_id,
        //                 'cash_acc_id' => $acc_code[$i],
        //                 'disbursed_amount' => $request->disbursed_amount,
        //                 'principal' => $request->loan_amount,
        //                 'paid_deposit_balance' => $request->paid_deposit_balance,
        //                 'paid_by' => $request->paid_by,
        //                 'cash_by' => $request->cash_pay,
        //                 'remark' => $request->remark,
        //                 'disbursement_status' => "activated",
        //                 'first_installment_date' => $request->first_installment_date,
        //                 'processing_date' => $request->loan_disbursement_date
        //             ]);
        //         }
        $acc_code = DB::table('account_chart_external_details')->where('account_chart_external_details.external_acc_id', $request->cashout_acc_id)->first()->main_acc_code;
        $main_acc_code = [$acc_code];
        // dd($main_acc_code);
        for ($a = 0; $a < count($main_acc_code); $a++) {

            DB::table($bcode . '_disbursement')->insert([
                'loan_id' => $loan_id->loan_unique_id,
                'client_id' => $disbursement_loan->client_id,
                'center_id' => $disbursement_loan->center_id,
                'group_id' => $disbursement_loan->group_id,
                'branch_id' => $disbursement_loan->branch_id,
                'loan_officer_id' => $disbursement_loan->loan_officer_id,
                'loan_product_id' => $disbursement_loan->loan_type_id,
                'cash_acc_id' => $main_acc_code[$a],
                'disbursed_amount' => $disbursement_loan->loan_amount,
                'principal' => $request->loan_amount,
                'paid_deposit_balance' => $request->paid_deposit_balance,
                'paid_by' => $request->paid_by,
                'cash_by' => $disbursement_loan->loan_amount,
                'remark' => $request->remark,
                'disbursement_status' => "activated",
                'first_installment_date' => $request->first_installment_date,
                'processing_date' => $request->loan_disbursement_date
            ]);
        }
        DB::table($bcode . '_loans')
            ->where($bcode . '_loans.loan_unique_id', '=', $loan_id->loan_unique_id)
            ->update([
                'disbursement_status' => "Activated",
                'disbursement_date' => $request->loan_disbursement_date,
                // 'loan_amount' => $disbursement_loan->loan_amount,
                'first_installment_date' => $request->first_installment_date,
            ]);

        //re schedule

        DB::table($bcode . '_loans_schedule')
            ->where($bcode . '_loans_schedule.loan_unique_id', $loan_id->loan_unique_id)
            ->delete();

        // Repayment date
        if ($disbursement_loan->loan_term == "Day") {
            $loan_term = 1;
        } else if ($disbursement_loan->loan_term == "Week") {
            $loan_term = 7;
        } else if ($disbursement_loan->loan_term == "Two-Weeks") {
            $loan_term = 14;
        } else if ($disbursement_loan->loan_term == "Four-Weeks") {
            $loan_term = 28;
        } else if ($disbursement_loan->loan_term == "Month") {
            $loan_term = 30;
        } else if ($disbursement_loan->loan_term == "Year") {
            $loan_term = 30;
        }

        if ($disbursement_loan->loan_type_id == 1 or $disbursement_loan->loan_type_id == 2) {
            // GeneralWeekly14D
            $loanschedule = LoanController::fixedLoanMethod(
                $disbursement_loan->loan_amount,
                $loan_term,
                $disbursement_loan->loan_term_value,
                $disbursement_loan->interest_rate,
                $disbursement_loan->principal_formula,
                // $start_date);
                $request->first_installment_date,
            );
        } elseif ($disbursement_loan->loan_type_id == 3 or $disbursement_loan->loan_type_id == 4) {
            // ExtraLoanWeekly
            $loanschedule = LoanController::principalFinalMethod30D(
                $disbursement_loan->loan_amount,
                $loan_term,
                $disbursement_loan->loan_term_value,
                $disbursement_loan->interest_rate,
                $disbursement_loan->principal_formula,
                // $start_date);
                $request->first_installment_date,
            );
        } elseif ($disbursement_loan->loan_type_id > 4 && $disbursement_loan->loan_type_id < 20) {
            // 5 to 19
            // ExtraLoanWeekly
            $loanschedule = LoanController::fixedPrincipalMethod30D(
                $disbursement_loan->loan_amount,
                $loan_term,
                $disbursement_loan->loan_term_value,
                $disbursement_loan->interest_rate,
                $disbursement_loan->principal_formula,
                // $start_date);
                $request->first_installment_date,
            );
        } elseif ($disbursement_loan->loan_type_id > 19 && $disbursement_loan->loan_type_id < 23) {
            // ExtraLoanWeekly
            $loanschedule = LoanController::principalFinalMethod28D(
                $disbursement_loan->loan_amount,
                $loan_term,
                $disbursement_loan->loan_term_value,
                $disbursement_loan->interest_rate,
                $disbursement_loan->principal_formula,
                // $start_date);
                $request->first_installment_date,
            );
        } elseif ($disbursement_loan->loan_type_id > 22 && $disbursement_loan->loan_type_id < 26) {
            // GeneralMonthly28D
            $loanschedule = LoanController::fixedPrincipalMethod28D(
                $disbursement_loan->loan_amount,
                $loan_term,
                $disbursement_loan->loan_term_value,
                $disbursement_loan->interest_rate,
                $disbursement_loan->principal_formula,
                // $start_date);
                $request->first_installment_date,
            );
        } elseif ($disbursement_loan->loan_type_id == 26 or $disbursement_loan->loan_type_id == 27) {
            // ExtraLoanWeekly
            $loanschedule = LoanController::fixedPrincipalMethod30D(
                $disbursement_loan->loan_amount,
                $loan_term,
                $disbursement_loan->loan_term_value,
                $disbursement_loan->interest_rate,
                $disbursement_loan->principal_formula,
                // $start_date);
                $request->first_installment_date,
            );
        }

        $loan_amount = $disbursement_loan->loan_amount;

        foreach ($loanschedule as $schedule) {
            $entrydate = $schedule['date'];
            // $total = $schedule->repay_interest + $schedule->repay_principal;
            $loan_amount -=  $schedule['repay_principal'];

            $loan_schedule = new LoanSchedule;
            $loan_schedule->setTable($bcode . '_loans_schedule');

            $loan_schedule->loan_unique_id = $loan_id->loan_unique_id;
            $loan_schedule->loan_type_id = $disbursement_loan->loan_type_id;
            $loan_schedule->month = $entrydate;
            $loan_schedule->capital = $schedule['repay_principal'];
            $loan_schedule->interest = $schedule['repay_interest'];
            $loan_schedule->discount = 0;
            $loan_schedule->refund_interest = 1;
            $loan_schedule->refund_amount = 1;
            $loan_schedule->final_amount = $loan_amount;
            $loan_schedule->status = "none";
            $loan_schedule->save();
        }

        // live disburse
        $this->liveDisburse($loan_id->loan_unique_id, $request);

        //update main join schedule
        $LoansSchedule = DB::connection('portal')->table($bcode . '_loans_schedule')->where('loan_unique_id', $loan_id->loan_unique_id)->get();
        $main_disbursement_id = DB::connection('portal')->table('tbl_main_join_loan')->where('portal_loan_id', $loan_id->loan_unique_id)->where('branch_id', $disbursement_loan->branch_id)->first()->main_loan_id;
        $RpDetail = DB::connection('main')->table('loan_disbursement_calculate_' . $disbursement_loan->branch_id)->where('disbursement_id', $main_disbursement_id)->get();

        //delete old main join schedule
        DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_id', $loan_id->loan_unique_id)->delete();

        for ($g = 0; $g < count($LoansSchedule); $g++) {
            if ($RpDetail[$g]->no == ($g + 1)) {
                DB::table('tbl_main_join_repayment_schedule')->insert([
                    'portal_disbursement_id' => $LoansSchedule[$g]->loan_unique_id,
                    'portal_disbursement_schedule_id' => $LoansSchedule[$g]->id,
                    'main_disbursement_schedule_id' => $RpDetail[$g]->id,
                    'branch_id' => $disbursement_loan->branch_id
                ]);
            }
        }

        return redirect('/loan?disbursement_status=Activated')->with("successMsg", 'Disbursement Success!!');
    }

    public function groupStore(Request $request)
    {
        // dd($request);
        $this->permissionFilter("group-disburse");
        $bcode = $this->getBranchCode();
        $ids = explode(" / ", $request->loan_id);
        $branch_id = DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id;

        $loan = DB::table($bcode . '_loans')
            ->leftJoin($bcode . '_deposit', $bcode . '_deposit.loan_id', '=', $bcode . '_loans.loan_unique_id')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', $bcode . '_loans.branch_id')
            ->leftJoin('accounts_loantypes', 'accounts_loantypes.loan_type_id', '=', $bcode . '_loans.loan_type_id')
            ->leftJoin('accounts', 'accounts.sub_acc_code', '=', 'accounts_loantypes.sub_acc_code')
            ->select(
                $bcode . '_loans.loan_unique_id AS loan_id',
                $bcode . '_loans.client_id AS client_id',
                $bcode . '_loans.branch_id AS branch_id',
                $bcode . '_loans.loan_type_id AS loan_product_id',
                $bcode . '_loans.loan_amount AS loan_amount',
                $bcode . '_deposit.paid_deposit_balance AS paid_deposit_balance',
                'accounts.id AS cash_acc_id'
            )
            ->whereIn($bcode . '_loans.loan_unique_id', $ids)->get();

        for ($i = 0; $i < count($ids); $i++) {
            $acc_id = DB::table('accounts_loantypes')
                ->leftJoin('account_chart_external_details', 'account_chart_external_details.main_acc_code', '=', 'accounts_loantypes.sub_acc_code')
                ->leftJoin('account_chart_externals', 'account_chart_externals.external_acc_code', '=', 'account_chart_external_details.external_acc_code')
                ->where('accounts_loantypes.loan_type_id', $loan[$i]->loan_product_id)
                ->first();
            $this_branch_id = DB::table('tbl_staff')->where('staff_code', '=', Session::get('staff_code'))->select('branch_id')->first();
            $accounts = DB::table('account_chart_externals')->where('branch_id', '=', $this_branch_id->branch_id)->where('external_acc_code', 'LIKE', '1-11' . '%')->first();
            $cash_acc_id = $accounts->id;
            // $acc_code = [$acc_id->id, $cash_acc_id];
            $acc_code = DB::table('account_chart_external_details')->where('account_chart_external_details.external_acc_id', $cash_acc_id)->first()->main_acc_code;
            $main_acc_code = [$acc_code];
            for ($a = 0; $a < count($main_acc_code); $a++) {
                DB::table($bcode . '_disbursement')->insert([
                    'loan_id' => $ids[$i],
                    'client_id' => $loan[$i]->client_id,
                    'center_id' => $request->center_id,
                    'group_id' => $request->group_id,
                    'branch_id' => $loan[$i]->branch_id,
                    'loan_officer_id' => session()->get('staff_code'),
                    'loan_product_id' => $loan[$i]->loan_product_id,
                    'cash_acc_id' => $main_acc_code[$a],
                    'disbursed_amount' => $loan[$i]->loan_amount,
                    'principal' => $loan[$i]->loan_amount,
                    'paid_deposit_balance' => $loan[$i]->paid_deposit_balance,
                    'paid_by' => $request->paid_by,
                    'cash_by' => $loan[$i]->loan_amount,
                    'disbursement_status' => "activated",
                    'processing_date' => $request->loan_disbursement_date
                ]);
            }


            DB::table($bcode . '_loans')
                ->where($bcode . '_loans.loan_unique_id', '=', $ids[$i])
                ->update([
                    'disbursement_status' => "Activated",
                    'disbursement_date' => $request->loan_disbursement_date
                ]);

            $this->liveDisburse($ids[$i], $request);
            //end live disburse

            //update main join schedule
            $LoansSchedule = DB::connection('portal')->table($bcode . '_loans_schedule')->where('loan_unique_id', $ids[$i])->get();
            $main_disbursement_id = DB::connection('portal')->table('tbl_main_join_loan')->where('portal_loan_id', $ids[$i])->where('branch_id', $loan[$i]->branch_id)->first()->main_loan_id;
            $RpDetail = DB::connection('main')->table('loan_disbursement_calculate_' . $loan[$i]->branch_id)->where('disbursement_id', $main_disbursement_id)->get();

            //delete old main join schedule
            DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_id', $ids[$i])->delete();

            for ($g = 0; $g < count($LoansSchedule); $g++) {
                if ($RpDetail[$g]->no == ($g + 1)) {
                    DB::table('tbl_main_join_repayment_schedule')->insert([
                        'portal_disbursement_id' => $LoansSchedule[$g]->loan_unique_id,
                        'portal_disbursement_schedule_id' => $LoansSchedule[$g]->id,
                        'main_disbursement_schedule_id' => $RpDetail[$g]->id,
                        'branch_id' => $loan[$i]->branch_id
                    ]);
                }
            }
        }
        return redirect('/loan?disbursement_status=Activated')->with("successMsg", 'Disbursement Success!!');
    }



    public function cancel($id)
    {
        $bcode = $this->getBranchCode();

        DB::table($bcode . '_loans')
            ->where($bcode . '_loans.loan_unique_id', '=', $id)
            ->update([
                'disbursement_status' => "Canceled"

            ]);

        return redirect('/loan')->with("successMsg", 'Loan Canceled!!');
    }

    public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }

    // public function liveDisburse($disburseDetail)
    // {
    //     $branch_id = DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id;
    //     $bcode = $this->getBranchCode();
    //     $main_loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $disburseDetail->loan_id)->first();
    //     if ($main_loan_id) {
    //         $main_loan_id = $main_loan_id->main_loan_id;

    //         $clientPID = DB::table($bcode . '_loans')->where('loan_unique_id', $disburseDetail->loan_id)->first()->client_id;
    //         $clientMID = DB::table('tbl_main_join_client')->where('portal_client_id', $clientPID)->first()->main_client_id;
    //         $clientMDetail = DB::connection('main')->table('clients')->where('id', $clientMID)->first();

    //         $route = $this->getMainUrl37();

    //         $route_name = 'create-disbursement';
    //         $routeurl = $route . $route_name;
    //         $authorization = $this->loginToken();

    //         $acc_code = DB::table('account_chart_external_details')->where('account_chart_external_details.external_acc_id', $disburseDetail->cashout_acc_id)->first()->main_acc_id;
    //         $main_acc_code = [$acc_code];
    //         // dd($main_acc_code);
    //         $loan_amount = DB::table($bcode . '_loans')->where('loan_unique_id', $disburseDetail->loan_id)->first()->loan_amount;

    //         $client = new \GuzzleHttp\Client(['verify' => false]);
    //         $result = $client->post($routeurl, [
    //             'headers' => [
    //                 'Content-Type' => 'application/x-www-form-urlencoded',
    //                 'Authorization' => 'Bearer ' . $authorization,
    //             ],
    //             'form_params' => [
    //                 'contract_id' => $main_loan_id,
    //                 'paid_disbursement_date' => $disburseDetail->loan_disbursement_date,
    //                 'first_payment_date' =>  $disburseDetail->first_installment_date,
    //                 'client_id' => $clientMDetail->id,
    //                 'reference' => $disburseDetail->remark == NULL ? 0 : $disburseDetail->remark, //$disbursements[$i]->remark,
    //                 'client_name' => $clientMDetail->name,
    //                 'client_nrc' => $clientMDetail->nrc_number,
    //                 'invoice_no' => 'inv-02345678',
    //                 'compulsory_saving' => 0,
    //                 'loan_amount' => $loan_amount,
    //                 'total_money_disburse' => $loan_amount,
    //                 'cash_out_id' => $main_acc_code[0],
    //                 'paid_by_tran_id' => 2,
    //                 'cash_pay' => $loan_amount,
    //                 'disburse_by' => 'loan-officer',
    //                 'branch_id' => $branch_id,
    //             ]
    //         ]);

    //         $response = (string) $result->getBody();
    //         $response = json_decode($response); // Using this you can access any key like below
    //         if ($response->status_code == 200) {
    //             DB::table('tbl_main_join_loan_disbursement')->insert([
    //                 'main_loan_disbursement_id' => $main_loan_id,
    //                 'main_loan_disbursement_client_id' => DB::connection('main')->table('loans_' . $branch_id)->where('id', $main_loan_id)->first()->client_id,
    //                 'portal_loan_disbursement_id' => $disburseDetail->loan_id
    //             ]);

    //             $LoansSchedule = DB::connection('portal')->table($bcode . '_loans_schedule')->where('loan_unique_id', $disburseDetail->loan_id)->get();
    //             $main_disbursement_id = DB::connection('portal')->table('tbl_main_join_loan')->where('portal_loan_id', $disburseDetail->loan_id)->first()->main_loan_id;
    //             $RpDetail = DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $main_disbursement_id)->get();

    //             for ($y = 0; $y < count($LoansSchedule); $y++) {
    //                 if ($RpDetail[$y]->no == ($y + 1)) {
    //                     DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$y]->id)->update([
    //                         'portal_disbursement_id' => $LoansSchedule[$y]->loan_unique_id,
    //                         'portal_disbursement_schedule_id' => $LoansSchedule[$y]->id,
    //                         'main_disbursement_schedule_id' => $RpDetail[$y]->id,
    //                         'branch_id' => $branch_id
    //                     ]);
    //                 }
    //             }
    //         }
    //     }
    // }

    public function liveDisburse($loan_id, $request)
    {
        $branch_code = $this->getBranchCode();
        $branch_id = DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id;

        $route = $this->getMainUrl37();
        $route_name = 'create-disbursement';
        $routeurl = $route . $route_name;
        $authorization = $this->loginToken();

        $disbursements = DB::connection('portal')->table($branch_code . '_disbursement')->where('loan_id', $loan_id)->first();

        // Loan id null or not found
        $loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $disbursements->loan_id)->where('branch_id', $branch_id)->first();
        if ($loan_id) {
            $loan_id = $loan_id->main_loan_id;
        }

        // client
        $clientid = DB::table('tbl_main_join_client')->where('portal_client_id', $disbursements->client_id)->first();
        if ($clientid) {
            $clientid = $clientid->main_client_id;
        } else {
            $clientid = '';
        }


        // staff
        $staffid = DB::table('tbl_main_join_staff')->where('portal_staff_id', $disbursements->loan_officer_id)->first();
        if ($staffid) {
            $staffid = $staffid->main_staff_id;
        } else {
            $staffid = '';
        }

        // group
        $groupid = DB::table('tbl_main_join_group')->where('portal_group_id', $disbursements->group_id)->first();
        if ($groupid) {
            $groupid = $groupid->main_group_id;
        } else {
            $groupid = '';
        }

        DB::connection('main')->table('loans_' . $branch_id)->where('id', $loan_id)->update([
            'disbursement_status' => 'Activated'
        ]);

        $depclient_info = DB::connection('portal')->table('tbl_client_basic_info')->where('client_uniquekey', $disbursements->client_id)->first();
        $main_acc_code = DB::table('account_chart_external_details')->where('main_acc_code', $disbursements->cash_acc_id)->first()->main_acc_id;

        $client = new \GuzzleHttp\Client(['verify' => false]);
        $result = $client->post($routeurl, [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Bearer ' . $authorization,
            ],
            'form_params' => [
                'contract_id' => $loan_id,
                'paid_disbursement_date' => $request->loan_disbursement_date ?? $disbursements->processing_date,
                'first_payment_date' =>  $request->first_installment_date ?? $disbursements->first_installment_date,
                'client_id' => $clientid,
                'reference' => 002250, //$disbursements[$i]->remark,
                'client_name' => $depclient_info->name,
                'client_nrc' => $depclient_info->nrc == NULL ? $depclient_info->old_nrc : $depclient_info->nrc,
                'invoice_no' => 'inv-02345678',
                'compulsory_saving' => 0,
                'loan_amount' => $disbursements->disbursed_amount,
                'total_money_disburse' => $disbursements->disbursed_amount,
                'cash_out_id' => $main_acc_code,
                'paid_by_tran_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', session()->get('staff_code'))->first()->main_staff_id,
                'cash_pay' =>  $disbursements->disbursed_amount,
                'disburse_by' => 'loan-officer',
                'branch_id' => $branch_id,

                // 'loan_id'=> $loan_id,
                // 'welfare_fund'=> 0,
                // 'loan_process_fee'=> 0,
                'disburse_amount'=> $disbursements->disbursed_amount,
                // 'seq'=> 0,
                'user_id'=> DB::table('tbl_main_join_staff')->where('portal_staff_id', session()->get('staff_code'))->first()->main_staff_id,
                // 'contract_no'=> '',
                // 'seq_contract'=> 0,
                'created_by'=>  DB::table('tbl_main_join_staff')->where('portal_staff_id', session()->get('staff_code'))->first()->main_staff_id,
                'updated_by'=>  DB::table('tbl_main_join_staff')->where('portal_staff_id', session()->get('staff_code'))->first()->main_staff_id,
                // 'total_service_charge'=> 0,
                // 'acc_code'=> '153620',
                // 'group_tran_id'=> $groupid,
                // 'product_id'=> 0,
                // 'deposit'=> 0,
                // 'service_charge'=> null,
                // 'service_charge_id'=> null,
                // 'charge_id'=> null,
            ]
        ]);

        // return $result;
        $response = (string) $result->getBody();
        $response = json_decode($response); // Using this you can access any key like below
        if ($response->status_code == 200) {
            // return response()->json($response);
            DB::table('tbl_main_join_loan_disbursement')->insert([
                'main_loan_disbursement_id' => DB::connection('main')->table('loans_' . $branch_id)->orderBy('id', 'desc')->first()->id,
                'main_loan_disbursement_client_id' => DB::connection('main')->table('loans_' . $branch_id)->orderBy('id', 'desc')->first()->client_id,
                'portal_loan_disbursement_id' => $disbursements->loan_id,
                'branch_id' => $branch_id,
            ]);

            // $portal_schedule = DB::connection('portal')->table($branch_code. '_loans_schedule')->where('loan_unique_id', $loans->loan_unique_id)->get();
            // $main_schedule = DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $main_loan_id)->get();
            // for ($i = 0; $i < count($main_schedule); $i++) {
            //     DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $main_loan_id)->where('no', $i + 1)->update([
            //         'date_s' => date('Y-m-d h:m:s', strtotime($portal_schedule[$i]->month)),
            //         'principal_s' => $portal_schedule[$i]->capital,
            //         'interest_s' => $portal_schedule[$i]->interest,
            //         'total_s' => $portal_schedule[$i]->capital + $portal_schedule[$i]->interest,
            //         'balance_s' => $portal_schedule[$i]->final_amount,
            //     ]);
            // }

            $LoansSchedule = DB::connection('portal')->table($branch_code . '_loans_schedule')->where('loan_unique_id', $disbursements->loan_id)->get();
            $main_disbursement_id = DB::connection('portal')->table('tbl_main_join_loan')->where('portal_loan_id', $disbursements->loan_id)->where('branch_id', $branch_id)->first()->main_loan_id;
            $RpDetail = DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $main_disbursement_id)->get();

            for ($i = 0; $i < count($RpDetail); $i++) {
                DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $main_disbursement_id)->where('no', $i + 1)->update([
                    'date_s' => date('Y-m-d h:m:s', strtotime($LoansSchedule[$i]->month)),
                    'principal_s' => $LoansSchedule[$i]->capital,
                    'interest_s' => $LoansSchedule[$i]->interest,
                    'total_s' => $LoansSchedule[$i]->capital + $LoansSchedule[$i]->interest,
                    'balance_s' => $LoansSchedule[$i]->final_amount,
                ]);
            }

            for ($g = 0; $g < count($LoansSchedule); $g++) {
                if ($RpDetail[$g]->no == ($g + 1)) {
                    DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$g]->id)->update([
                        'portal_disbursement_id' => $LoansSchedule[$g]->loan_unique_id,
                        'portal_disbursement_schedule_id' => $LoansSchedule[$g]->id,
                        'main_disbursement_schedule_id' => $RpDetail[$g]->id,
                        'branch_id' => $branch_id
                    ]);
                }
            }
        }
    }
}
