<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Models\ClientDataEnteries\Branch;

class DashboardController extends Controller
{
    public function getBranchCode()
    {
        $staff_code = Session::get('staff_code');
        // dd($staff_code);
        $B_code = DB::table('tbl_staff')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->select('tbl_branches.branch_code')
            ->where('tbl_staff.staff_code', $staff_code)
            ->first();
        $b_code = strtolower($B_code->branch_code);
        return $b_code;
    }

    public function index()
    {
        $bcode = $this->getBranchCode();

        // if(Session::get('staff_code') == 'KS-0000002') {
        //     $total_clients = DB::table('tbl_client_join')->count();
        // }else{
        //     $branchId = DB::table('tbl_staff')->where('staff_code',Session::get('staff_code'))->select('branch_id')->first();
        //     $total_clients = DB::table('tbl_client_join')->where('branch_id',$branchId->branch_id)->count();
        // }

        $branchId = DB::table('tbl_staff')->where('staff_code', Session::get('staff_code'))->select('branch_id')->first();
        $total_clients = DB::table('tbl_client_join')->where('branch_id', $branchId->branch_id)->count();

        $total_loans = DB::table($bcode . '_loans')->count();

        $pending = DB::table($bcode . '_loans')->where('disbursement_status', 'Pending')->get()->count();

        $activated = DB::table($bcode . '_loans')->where('disbursement_status', 'Activated')->get()->count();

        $total_disbursement = DB::table($bcode . '_disbursement')->sum('disbursed_amount');


        $principal_outstanding = DB::table($bcode . '_loans_schedule')->where('status', '=', 'unpaid')->sum('capital');
        $interest_outstanding = DB::table($bcode . '_loans_schedule')->where('status', '=', 'unpaid')->sum('interest');
        $total_outstanding = $principal_outstanding + $interest_outstanding;

        $principal_repayment = DB::table($bcode . '_loans_schedule')->where('status', '=', 'paid')->sum('capital');
        $interest_repayment = DB::table($bcode . '_loans_schedule')->where('status', '=', 'paid')->sum('capital');

        $total_repayment = $principal_repayment + $interest_repayment;
        $branch = Branch::where('id', '=', $branchId->branch_id)->select('branch_name')->first();
        $branch_name = $branch->branch_name;


        //get labels and values for chart
        $values = array();
        $values_data = DB::table($bcode . '_loans')
            ->leftJoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
            ->select('loan_type.name')
            ->get();
        for ($i = 0; $i < count($values_data); $i++) {
            $data = explode(' ', $values_data[$i]->name);
            array_push($values, $data[0] . " Loans");
        }
        $values = (array_count_values($values));

        $labels = array_keys($values);
        $values = array_values($values);

        $today = now()->format("Y-m-d 00:00:00");
        $next30 = now()->addDays(30)->format("d/m/Y");
        // dd($next30);
        $upcoming = DB::table($bcode . '_loans_schedule')
            ->leftJoin($bcode . '_loans', $bcode . '_loans.loan_unique_id', '=', $bcode . '_loans_schedule.loan_unique_id')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
            ->where($bcode . '_loans_schedule.status', '=', 'unpaid')
            ->orWhere($bcode . '_loans_schedule.status', '=', 'none')
            ->select($bcode . '_loans_schedule.loan_unique_id', $bcode . '_loans_schedule.month', $bcode . '_loans_schedule.capital', $bcode . '_loans_schedule.interest', 'tbl_client_basic_info.name')
            ->orderBy($bcode . '_loans_schedule.id', 'asc')
            ->paginate(5);
        // dd($upcoming);
        $todayCollect = array();
        $todayCollectDueExists = DB::table($bcode . '_repayment_due')
            ->leftJoin($bcode . '_loans_schedule', $bcode . '_loans_schedule.id', '=', $bcode . '_repayment_due.schedule_id')
            ->leftJoin($bcode . '_loans', $bcode . '_loans.loan_unique_id', '=', $bcode . '_loans_schedule.loan_unique_id')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
            ->where('payment_date', '=', $today)
            ->select($bcode . '_loans_schedule.loan_unique_id', 'tbl_client_basic_info.name', $bcode . '_repayment_due.principal', $bcode . '_repayment_due.interest', $bcode . '_repayment_due.payment_date')
            ->get();
        if ($todayCollectDueExists) {
            foreach ($todayCollectDueExists as $key => $value) {
                array_push(
                    $todayCollect,
                    [
                        'loan_unique_id' => $value->loan_unique_id,
                        'name' => $value->name,
                        'principal' => $value->principal,
                        'interest' => $value->interest,
                        'payment_date' => $value->payment_date,
                        'repayment_type' => 'Due'
                    ]
                );
            }
        }

        $todayCollectLateExists = DB::table($bcode . '_repayment_late')
            ->leftJoin($bcode . '_loans_schedule', $bcode . '_loans_schedule.id', '=', $bcode . '_repayment_late.schedule_id')
            ->leftJoin($bcode . '_loans', $bcode . '_loans.loan_unique_id', '=', $bcode . '_loans_schedule.loan_unique_id')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
            ->where('payment_date', '=', $today)
            ->select($bcode . '_loans_schedule.loan_unique_id', 'tbl_client_basic_info.name', $bcode . '_repayment_late.principal_paid', $bcode . '_repayment_late.interest_paid', $bcode . '_repayment_late.payment_date')
            ->get();
        if ($todayCollectLateExists) {
            foreach ($todayCollectLateExists as $key => $value) {
                array_push(
                    $todayCollect,
                    [
                        'loan_unique_id' => $value->loan_unique_id,
                        'name' => $value->name,
                        'principal' => $value->principal_paid,
                        'interest' => $value->interest_paid,
                        'payment_date' => $value->payment_date,
                        'repayment_type' => 'Late'
                    ]
                );
            }
        }

        $todayCollectPreExists = DB::table($bcode . '_repayment_pre')
            ->leftJoin($bcode . '_loans_schedule', $bcode . '_loans_schedule.id', '=', $bcode . '_repayment_pre.schedule_id')
            ->leftJoin($bcode . '_loans', $bcode . '_loans.loan_unique_id', '=', $bcode . '_loans_schedule.loan_unique_id')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
            ->where('payment_date', '=', $today)
            ->select($bcode . '_loans_schedule.loan_unique_id', 'tbl_client_basic_info.name', $bcode . '_repayment_pre.principal', $bcode . '_repayment_pre.interest', $bcode . '_repayment_pre.payment_date')
            ->get();

        if ($todayCollectPreExists) {
            foreach ($todayCollectPreExists as $key => $value) {
                array_push(
                    $todayCollect,
                    [
                        'loan_unique_id' => $value->loan_unique_id,
                        'name' => $value->name,
                        'principal' => $value->principal,
                        'interest' => $value->interest,
                        'payment_date' => $value->payment_date,
                        'repayment_type' => 'Pre'
                    ]
                );
            }
        }
        $todayReceivedAmount = 0;
        foreach ($todayCollect as $data) {
            $total = $data['principal'] + $data['interest'];
            $todayReceivedAmount += $total;
        }
        /////////////////////////////////
        $B_code = DB::table('tbl_branches')
            ->select('branch_code')
            ->get();
        $graph = array();
        $graph_values = array();
        foreach ($B_code as $bcode) {
            $b_code = strtolower($bcode->branch_code);

            $monthNamount = array();
            $graph_labels = array();
            for ($i = 0; $i < 12; $i++) {
                $firstDayofPreviousMonth = Carbon::now()->startOfMonth()->subMonth();
                $lastDayofPreviousMonth = Carbon::now()->endOfMonth()->subMonth();

                $first_day_of_month = $firstDayofPreviousMonth->copy()->subMonth($i);
                $last_day_of_month = $first_day_of_month->copy()->endOfMonth();
                $Month = Carbon::now()->subMonth($i + 1)->format('M');

                $disburse_amount_for_a_month = DB::table($b_code . '_disbursement')->whereBetween('processing_date', [$first_day_of_month, $last_day_of_month])->sum('disbursed_amount');

                array_push($monthNamount, [$Month => $disburse_amount_for_a_month]);
                array_push($graph_labels, $Month);
                array_push($graph_values, $disburse_amount_for_a_month);
            }
            array_push($graph, [
                'branch' => $b_code,
                'data' => array_reverse(array_values($monthNamount))
            ]);
        }
        $graph_labels = array_reverse($graph_labels);

        $graph_values = array_reverse($graph_values);
        $graph_values = array_chunk($graph_values, 12);
    	$color_codes=array();
    	$color_codes=['rgba(235, 10, 10, 0.48)',
						'rgba(235, 77, 10, 0.48)',
						'rgba(235, 149, 10, 0.48)',
						'rgba(235, 205, 10, 0.48)',
						'rgba(194, 235, 10, 0.48)',
						'rgba(130, 235, 10, 0.48)',
						'rgba(40, 235, 10, 0.48)',
						'rgba(10, 235, 89, 0.48)',
						'rgba(10, 235, 171, 0.48)',
						'rgba(10, 216, 235, 0.48)',
						'rgba(10, 167, 235, 0.48)',
						'rgba(10, 107, 235, 0.48)',
						'rgba(10, 14, 235, 0.48)',
						'rgba(104, 10, 235, 0.48)',
						'rgba(175, 10, 235, 0.48)',
						'rgba(201, 10, 235, 0.48)',
						'rgba(235, 10, 171, 0.48)',
						'rgba(235, 10, 89, 0.48)',
						'rgba(235, 10, 10, 0.48)',
						'rgba(200, 45, 45, 0.48)',
						'rgba(200, 128, 45, 0.48)',
						'rgba(200, 189, 45, 0.48)',
						'rgba(130, 200, 45, 0.48)',
						'rgba(45, 200, 71, 0.48)',
						'rgba(45, 200, 156, 0.48)',
						'rgba(45, 161, 200, 0.48)',
						'rgba(56, 70, 199, 0.48)',
						'rgba(130, 56, 199, 0.48)',
						'rgba(187, 56, 199, 0.48)',
						'rgba(199, 56, 158, 0.48)',
						'rgba(199, 56, 94, 0.48)',
                     ];
        // dd($graph_values);
    
        return view('dashboard.index', compact('color_codes','graph_labels', 'graph_values', 'B_code', 'labels', 'values', 'total_clients', 'total_loans', 'pending', 'activated', 'total_disbursement', 'total_outstanding', 'total_repayment', 'branch_name', 'upcoming', 'todayCollect', 'todayReceivedAmount'));
    }

	public function changeBranch(Request $request) {
    	DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->update([
        	'branch_id' => $request->branch_id,
        ]);
                                      
        return back()->with('Branch successfully changed!');
    }
}
