<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Branch;
use App\Models\ClientDataEnteries\Center;
use App\Models\ClientDataEnteries\Group;
use App\Models\ClientDataEnteries\Guarantor;
use App\Models\ClientDataEnteries\Staff;
use App\Models\ClientDataEnteries\BusinessType;
use App\Models\ClientDataEnteries\BusinessCategory;
use App\Models\Clients\ClientBasicInfo;
use App\Models\Loandataenteries\LoanType;
use App\Models\Loans\Loan;
use App\Models\Loans\LoanCycle;
use App\Models\Loans\LoanDocument;
use App\Models\Loans\LoanJoin;
use App\Models\Loans\LoanSchedule;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Void_;
use Illuminate\Support\Facades\Http;
use App\Models\Crawl\Crawl_Loan_Repayment_Schedule;

class LoanController extends Controller
{

    //auto get branch id
    public function getBranchID($staffid){
        //dd($request->staffid);
        $obj_branchcode = DB::table('tbl_staff')
            ->join('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->where('tbl_staff.staff_code',$staffid)
            ->select('tbl_branches.branch_code')
            ->first();
        //dd( $staffid);
        $res = strtolower((string)$obj_branchcode->branch_code); 
        return $res;               
    }  
    //auto generate loans uniquekey
    public function generateLoanID($staffid,$branchcode)
    {
        $obj_branchcode = DB::table('tbl_staff')
            ->join('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->where('tbl_staff.staff_code',$staffid)
            ->select('tbl_branches.branch_code')
            ->get();
        
    
        if (count(array($obj_branchcode)) > 0) {
            $subcode = substr($obj_branchcode[0]->branch_code, 0, 2); //split branch code
            $obj_fund = DB::table($branchcode.'_loans')->where('branch_id', Staff::where('staff_code', $staffid)->first()->branch_id)->get()->last();
            //$obj_fund = DB::table($branchcode.'_loans')->where('branch_id', Staff::where('staff_code', $staffid)->get()->last());
            if ($obj_fund) {
                //LG-00001
                $last_inserted_id = $obj_fund->loan_unique_id;
                $bstr = substr($last_inserted_id, 3);
                $id = substr($bstr, -5, 5);
                $bstr_cusm = $id + 1;
                $bstr_cusm = (string)str_pad($bstr_cusm, 9, '0', STR_PAD_LEFT);

                // get loan_uniquekey in tbl_loans
                $loanuniquekey = $subcode; // Branch Code
                $loanuniquekey .= '-';
            	$loanuniquekey .= '03';
            	$loanuniquekey .= '-';
                $loanuniquekey .= $bstr_cusm; // Loan ID in branch
            } else {
                $loanuniquekey = $subcode . '-03-000000001';
            }

            return $loanuniquekey;
        } else {
            $subcode = substr($obj_branchcode[0]->branch_code,0,2); //split branch code
            $loanuniquekey = $subcode . '-03-000000001';
        }
    }
// 	public function generateLoanID($staffid,$branchcode)
//     {
//         $bcode = $this->getBranchID($staffid);

//         $obj_branchcode = DB::table('tbl_branches')->where('id', Staff::where('staff_code',$staffid)->first()->branch_id)->select('branch_code')->get();
//         if (count(array($obj_branchcode)) > 0) {
//             $obj_branchcode = DB::table('tbl_branches')->where('id', Staff::where('staff_code', $staffid)->first()->branch_id)->first()->branch_code;
//             $obj_fund = DB::table('amp_loans')->where('branch_id',Staff::where('staff_code', $staffid)->first()->branch_id)->get()->last();
// 			if ($obj_fund) {
//                 //LG-00001
//                 $last_inserted_id = $obj_fund->loan_unique_id;
//             	$loanuniquekey = ++$last_inserted_id;

//             } else {
//                 $loanuniquekey = $obj_branchcode . '-03-000000001';
//             }

//             return $loanuniquekey;
//         } else {
//             return response()->json(['status_code' => 500, 'message' => 'loan id generate fail', 'data' => null]);
//         }
//     }
    // Storage new client
    public function createLoan (Request $request)
    {
     	date_default_timezone_set('Asia/Yangon');
        // generate client id
        //return response()->json(['status_code'=>200,'message'=>Staff::where('staff_code', $request->loan_officer_id)->first()->id,'data'=>null]);
        $staffid = $request->staffid;
        $branchid = $request->branchid;
      	$bcode = $this->getBranchID($staffid);
        $loanuniid = $this->generateLoanID($staffid,$bcode);
    
	//return response()->json(['status_code'=>200,'message'=>$loanuniid,'data'=>null]);
        //dd( $loanuniid);
            // *** Loan
            $loan = new Loan;
            $loan->setTable($bcode.'_loans');

            $loan->loan_unique_id = $loanuniid;
            $loan->process_join_id = 1;
            $loan->branch_id = $request->branch_id;
            $loan->group_id = $request->group_id;
            $loan->center_id = $request->center_id;
     		$loan->loan_officer_id =  $request->loan_officer_id;
        	$loan->business_category_id = $request->business_category_id;
            $loan->client_id = $request->client_id;
            $loan->guarantor_a = $request->guarantor_a ?? "";
            $loan->guarantor_b = $request->guarantor_b ?? "";
            $loan->guarantor_c = $request->guarantor_c ?? "";
            $loan->loan_type_id = $request->loan_type_id;
            //$loan->loan_amount = $request->loan_amount;
            $loan->loan_amount = $request->estimate_receivable_amount;

            $loan->estimate_receivable_amount = $request->estimate_receivable_amount;
            $loan->loan_term_value = $request->loan_term_value;
            $loan->loan_term = $request->loan_term;
            $loan->interest_rate_period = $request->interest_rate_period;
            $loan->disbursement_status =  $request->disbursement_status;
            $loan->interest_method = $request->interest_method;
            $loan->remark = $request->remark;
            $loan->status_del = "none";
            $loan->loan_application_date = date('Y-m-d H:i:s', strtotime($request->loan_application_date)) ;
            $loan->first_installment_date = date('Y-m-d H:i:s', strtotime($request->first_installment_date));
            $loan->disbursement_date = date('Y-m-d H:i:s', strtotime($request->first_installment_date));
            //$loan->cancel_date = date('Y-m-d H:i:s', strtotime($request->cancel_date));
            $loan->save();
   
            if($loan){


                //*** loan cycle
                $loan_cycle = new LoanCycle();
                $loan_cycle->setTable($bcode.'_loan_cycle');

                $loan_cycle->loan_unique_id = $loanuniid;
                $loan_cycle->client_unique_id = $request->client_id;
                $loan_cycle->loan_amount = $request->loan_amount;
                $loan_cycle->loan_final_amount = $request->loan_amount;
                $loan_cycle->description = $request->guarantor_a.','.$request->guarantor_b.','.$request->guarantor_c;
                $loan_cycle->cycle = 1;
                $loan_cycle->now_status = 1;
                $loan_cycle->save();
                // loan schedule
                //add schedule start-----------------------------------------------------------
                $loantypeid = $request->loan_type_id;
                $loan_amount = $request->loan_amount;
               
                $loan_term_value = $request->loan_term_value;
                $loan_term = $request->loan_term;
                // $interest_rate = $request->interest_formula;
                $interest_rate = $request->interest_rate;
            
            	$principal_formula = $request->principal_formula;
                $disbursement_date = $request->first_installment_date;

                $entrydate = $loan->first_installment_date;

                // Repayment date
                if ($loan_term == "Day") {
                    $loan_term = 1;
                } else if ($loan_term == "Week") {
                    $loan_term = 7;
                } else if ($loan_term == "Two-Weeks") {
                    $loan_term = 14;
                } else if ($loan_term == "Four-Weeks") {
                    $loan_term = 28;
                }  else if ($loan_term == "Month") {
                    $loan_term = 30;
                } else if ($loan_term == "Year") {
                    $loan_term = 30;
                } 
            
                if(($loantypeid == 1) or ($loantypeid ==  2)){
                    // GeneralWeekly14D
                    $loanschedule = $this->fixedLoanMethod(
                        $loan_amount,
                        $loan_term,$loan_term_value,
                        $interest_rate,
                        $principal_formula,
                        // $start_date);
                        $entrydate);
                }else if(($loantypeid == 3) or ($loantypeid == 4)){
                    // ExtraLoanWeekly
                    $loanschedule = $this->principalFinalMethod30D(
                        $loan_amount,
                        $loan_term,$loan_term_value,
                        $interest_rate,
                        $principal_formula,
                        // $start_date);
                        $entrydate);
                }else if(($loantypeid > 4) && ($loantypeid < 20)){
                    // 5 to 19
                    // ExtraLoanWeekly
                    $loanschedule = $this->fixedPrincipalMethod30D(
                        $loan_amount,
                        $loan_term,$loan_term_value,
                        $interest_rate,
                        $principal_formula,
                        // $start_date);
                        $entrydate);
                }else if(($loantypeid > 19) && ($loantypeid < 23)){
                    // ExtraLoanWeekly
                    $loanschedule = $this->principalFinalMethod28D(
                        $loan_amount,
                        $loan_term,$loan_term_value,
                        $interest_rate,
                        $principal_formula,
                        // $start_date);
                        $entrydate);
                }else if(($loantypeid > 22) && ($loantypeid < 26)){
                    // GeneralMonthly28D
                    $loanschedule = $this->fixedPrincipalMethod28D(
                        $loan_amount,
                        $loan_term,$loan_term_value,
                        $interest_rate,
                        $principal_formula,
                        // $start_date);
                        $entrydate);
                }else if(($loantypeid == 26) or ($loantypeid == 27)){
                    // ExtraLoanWeekly
                    $loanschedule = $this->fixedPrincipalMethod30D(
                        $loan_amount,
                        $loan_term,$loan_term_value,
                        $interest_rate,
                        $principal_formula,
                        // $start_date);
                        $entrydate);
                }
                // dd($loanschedule);

                foreach($loanschedule as $schedule) {
                    $entrydate = $schedule['date'];
                    // $total = $schedule->repay_interest + $schedule->repay_principal;
                    $loan_amount -=  $schedule['repay_principal'];

                    $loan_schedule = new LoanSchedule;
                    $loan_schedule->setTable($bcode.'_loans_schedule');

                    $loan_schedule->loan_unique_id = $loanuniid;
                    $loan_schedule->loan_type_id = $request->loan_type_id;
                    $loan_schedule->month = $entrydate;
                    $loan_schedule->capital = $schedule['repay_principal'];
                    $loan_schedule->interest = $schedule['repay_interest'];
                    $loan_schedule->discount = 0;
                    $loan_schedule->refund_interest = 1;
                    $loan_schedule->refund_amount = 1;
                    $loan_schedule->final_amount = $loan_amount;
                    $loan_schedule->status = "none";
                    $loan_schedule->save();
                }

                // *** loan charge and compulsory join
                // charge_array [ KS-C-0001, MS-C-89991 ]
                $chargearray = str_replace("[", "",$request->charge_array);
                $chargearray = str_replace("]", "",$chargearray);
                $chargearray = preg_split("/[,]/",$chargearray);
                foreach( $chargearray as $index=>$datachargearray){
                    $loan_join = new LoanJoin();
                    $loan_join->setTable($bcode.'_loan_charges_compulsory_join');

                    $loan_join->loan_unique_id =  $loanuniid;
                    $loan_join->loan_charge_id = str_replace(' ', '', $datachargearray);
                    $loan_join->loan_type_id = $request->loan_type_id;
                    $loan_join->branch_id = $branchid;
                    $loan_join->save();
                }
               
                 // charge_array [ KS-C-0001, MS-C-89991 ]
                $compulsorysavingarray = str_replace("[", "",$request->compulsory_saving_array);
                $compulsorysavingarray = str_replace("]", "",$compulsorysavingarray);
                $compulsorysavingarray = preg_split("/[,]/",$compulsorysavingarray);
                foreach($compulsorysavingarray as $index=>$datacompulsorysavingarray){
                    $loan_join = new LoanJoin();
                    $loan_join->setTable($bcode.'_loan_charges_compulsory_join');

                    $loan_join->loan_unique_id =  $loanuniid;
                    $loan_join->loan_compulsory_id = str_replace(' ', '', $datacompulsorysavingarray);
                    $loan_join->loan_type_id = $request->loan_type_id;
                    $loan_join->branch_id = $branchid;
                    $loan_join->save();
                }

            
                
                // *** Loan Document
                // user_type_status  1,2,3,4 (1 mean client doc, 2 mean guarantor doc etc....)
                for($i = 0 ; $i <4 ; $i++){
                    $loansaved_id =  $loanuniid; // get loan table in latest save data
                    $loantypename = LoanType::where('id', $request->loan_type_id)->first(); // loan type get
                    // save client document 
                    if(isset($request->user_type_status[0])){
                        $type_status = "client";
                        $result = $this->saveLoanDocument($request,$bcode,$loansaved_id,
                        $loantypename->name,$type_status,0,
                        $request->loan_document_client_1,$request->loan_document_client_2,
                        $request->loan_document_client_3,$request->loan_document_client_4,$request->loan_document_client_5);
                    }
                    // save guarantor_a document 
                    if(isset($request->user_type_status[1])){
                        $type_status = "guarantor_a";
                        $result = $this->saveLoanDocument($request,$bcode,$loansaved_id,
                            $loantypename->name,$type_status,$request->guarantor_a,
                            $request->loan_document_guarantor_a1,$request->loan_document_guarantor_a2,
                            $request->loan_document_guarantor_a3,$request->loan_document_guarantor_a4, $request->loan_document_guarantor_a5);
                        }
                    // save guarantor_b document 
                    if(isset($request->user_type_status[2])){
                        $type_status = "guarantor_b";
                        $this->saveLoanDocument($request,$bcode,$loansaved_id,
                        $loantypename->name, $type_status,$request->guarantor_b,
                        $request->loan_document_guarantor_b1,$request->loan_document_guarantor_b2,
                        $request->loan_document_guarantor_b3,$request->loan_document_guarantor_b4,$request->loan_document_guarantor_b5);
                    }
                    // save guarantor_c document 
                    if(isset($request->user_type_status[3])){
                        $type_status = "guarantor_c";
                        $this->saveLoanDocument($request,$bcode,$loansaved_id,
                        $loantypename->name,$type_status,$request->guarantor_c,
                        $request->loan_document_guarantor_c1,$request->loan_document_guarantor_c2,
                        $request->loan_document_guarantor_c3,$request->loan_document_guarantor_c4,$request->loan_document_guarantor_c5);
                    }
                }     
            
            	$this->liveReverseLoans($request, $loanuniid);

                return response()->json(['status_code'=>200,'message'=>'success to create new loan','data'=>null]);
            }else{
                return response()->json(['status_code'=>422,'message'=>'fail to create new loan','data'=>null]);
            }


    }

    //decode base64 string
    public function converttoImage($dataloantid,$dataimage)
    {
        if($dataimage != ''){
            $image = str_replace('data:image/png;base64,', '', $dataimage);
            $image_name = uniqid().'_'.$dataloantid.'_'.time().'.jpg';
            // save to local storage
            $disk = Storage::disk('loan_document_photos')->put($image_name, base64_decode($image));
        }else{
            $image_name = '';
        }
       
        return $image_name;
    }

    public function saveLoanDocument($request,$branchcode,$loanid,$loantypename,$type_status,$guarantor_id,$doc_1,$doc_2,$doc_3,$doc_4,$doc_5){

        //return response()->json(['status_code'=>200,'message'=>$type_status,'data'=>null]);
        $loan_document = new LoanDocument();
        $loan_document->setTable($branchcode.'_loan_document');

        $loan_document->loan_unique_id = $loanid;
        $loan_document->loan_type_id = $request->loan_type_id;
        $loan_document->loan_type_name = $loantypename;
        $loan_document->client_id = $request->client_id;
        $loan_document->guarantor_id = $guarantor_id;
        $loan_document->status = $type_status;
        $loan_document->loan_document_1 = $this->converttoImage($loanid,$doc_1);
        $loan_document->loan_document_2 = $this->converttoImage($loanid,$doc_2);
        $loan_document->loan_document_3 = $this->converttoImage($loanid,$doc_3);
        $loan_document->loan_document_4 = $this->converttoImage($loanid,$doc_4);
        $loan_document->loan_document_5 = $this->converttoImage($loanid,$doc_5);
        $loan_document->save();
    }
    
//fixed principal and interest
    public function fixedLoanMethod($loan_amount,$loan_term,$loan_term_value,$interest_rate,$principal_formula,$start_date) {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if( $loan_term_value == 1){ // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = $start_date;
        $principal = round($loan_amount / $loan_term_value);
        $interest = round($principal * 14/100);

        for ($i = 1; $i <= $loan_term_value; $i++) {
           
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            if($i == 1) {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . '0' . 'days'));
            } else {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            }
            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "date" => $schedule_date
            );
        }
        return $loanschedule;
    }

    //pay principal at final 30days
    public function principalFinalMethod30D($loan_amount,$loan_term,$loan_term_value,$interest_rate,$principal_formula,$start_date) {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if( $loan_term_value == 1){ // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = $start_date;
        $principal = 0;
        $next_balance = $loan_amount;
        for ($i = 1; $i <= $loan_term_value; $i++) {
            // $paid_principal = 0;
            if($i == $loan_term_value) {
                $principal = $loan_amount;
            }
            // $paid_principal += $principal;

            $interest = round($loan_amount * (($interest_rate/ 100) / 12));
            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            if($i == 1) {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . '0' . 'days'));
            } else {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            }

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }

    //pay principal at final 28 days
    public function principalFinalMethod28D($loan_amount,$loan_term,$loan_term_value,$interest_rate,$principal_formula,$start_date) {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if( $loan_term_value == 1){ // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = $start_date;
        $principal = 0;
        $next_balance = $loan_amount;
        $interest = round($loan_amount * (($interest_rate/ 100) / 365) * 28);

        for ($i = 1; $i <= $loan_term_value; $i++) {
            // $paid_principal = 0;
            if($i == $loan_term_value) {
                $principal = $loan_amount;
            }
            // $paid_principal += $principal;

            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            if($i == 1) {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . '0' . 'days'));
            } else {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            }

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }

    //fixed principal and interest change 30 days
    public function fixedPrincipalMethod30D($loan_amount,$loan_term,$loan_term_value,$interest_rate,$principal_formula,$start_date) {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if( $loan_term_value == 1){ // 1 mean 12 months
            $loan_term_value = 12;
        }
        // dd($principal_formula);
        $schedule_date = $start_date;
        $principal = round($loan_amount * $principal_formula);
        // dd($principal);
        $next_balance = $loan_amount;
        for ($i = 1; $i <= $loan_term_value; $i++) {

            $paid_principal = 0;
            if($i == $loan_term_value) {
                $principal = $loan_amount - $paid_principal;
            }
            $paid_principal += $principal;

            $interest = round($next_balance * ($interest_rate/ 100) / 12);
            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            if($i == 1) {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . '0' . 'days'));
            } else {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            }

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }

     //fixed principal and interest change 28 days
     public function fixedPrincipalMethod28D($loan_amount,$loan_term,$loan_term_value,$interest_rate,$principal_formula,$start_date) {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if( $loan_term_value == 1){ // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = $start_date;
        $principal = round($loan_amount * $principal_formula);
        $next_balance = $loan_amount;
        for ($i = 1; $i <= $loan_term_value; $i++) {

            $paid_principal = 0;
            if($i == $loan_term_value) {
                $principal = $loan_amount - $paid_principal;
            }
            $paid_principal += $principal;

            $interest = round($next_balance * (($interest_rate/ 100) / 365) * 28);
            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            if($i == 1) {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . '0' . 'days'));
            } else {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            }

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }

	public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }

    public function getBranchCode($staff_code)
    {
        $B_code = DB::table('tbl_staff')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->select('tbl_branches.branch_code')
            ->where('tbl_staff.staff_code', $staff_code)
            ->first();
        $b_code = strtolower($B_code->branch_code);
        return $b_code;
    }

    public function liveReverseLoans($request, $loan_id)
    {
        $staffid = $request->staffid;

        $route = $this->getMainUrl37();
        $route_name = 'create-loan';
        $routeurl = $route . $route_name;
        $authorization = $this->loginToken();

        $branch_id = DB::table('tbl_staff')->where('staff_code', '=', $staffid)->first()->branch_id;
        $branch_code = strtolower($this->getBranchCode($staffid));

        $loans = DB::connection('portal')->table($branch_code . '_loans')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $branch_code . '_loans.client_id')
            ->where($branch_code . '_loans.loan_unique_id', $loan_id)
            ->first();
        // dd($loans);
        

        $interest_rate = DB::connection('portal')->table('loan_type')->where('id', $loans->loan_type_id)->first()->interest_rate;

        // staff
        $staffid = DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans->loan_officer_id)->first();
        if ($staffid) {
            $staffid = $staffid->main_staff_id;
        } 

        // center
        $centerid = DB::table('tbl_main_join_center')->where('portal_center_id', $loans->center_id)->first();
        if ($centerid) {
            $centerid = $centerid->main_center_id;
        } 

        // group
        $groupid = DB::table('tbl_main_join_group')->where('portal_group_id', $loans->group_id)->first();
        if ($groupid) {
            $groupid = $groupid->main_group_id;
        } 

        // client
        $clientid = DB::table('tbl_main_join_client')->where('portal_client_id', $loans->client_id)->first();
        if ($clientid) {
            $clientid = $clientid->main_client_id;
        } 

        // Guarantor_a
        $guarantor_a = DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id', $loans->guarantor_a)->first();
        if ($guarantor_a) {
            $guarantor_a = $guarantor_a->main_guarantor_id;
        } 

        // Guarantor_b
        $guarantor_b = DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id', $loans->guarantor_b)->first();
        if ($guarantor_b) {
            $guarantor_b = $guarantor_b->main_guarantor_id;
        } 

        // Guarantor_c
        $guarantor_c = DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id', $loans->guarantor_c)->first();
        if ($guarantor_c) {
            $guarantor_c = $guarantor_c->main_guarantor_id;
        } 

        // latest loan id generate
        $dataloan = DB::connection('main')->table('loans_' . $branch_id)->orderBy('id', 'desc')->first();
        $loanCount = DB::connection('main')->table('loans_' . $branch_id)->count();

        // $main_loan_generate_id = $dataloan->disbursement_number;
        // $bMainCode = DB::connection('main')->table('branches')->where('id', $y)
        $mainbcode = DB::connection('main')->table('branches')->where('id',$branch_id)->first()->code;
        $main_loan_generate_id = 'LID-' . $mainbcode . '-2023-';
        $num = $loanCount + 1;
        $main_loan_generate_id .= $num;

        // return response()->json($main_loan_generate_id);

        // save new loan
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $routeurl = env('MAIN_URL') . "create-loan";

        $chargeDetail = DB::table($branch_code . '_loan_charges_compulsory_join')
            ->leftJoin('loan_charges_type', 'loan_charges_type.charge_code', '=', $branch_code . '_loan_charges_compulsory_join.loan_charge_id')
            ->where($branch_code . '_loan_charges_compulsory_join.loan_unique_id', $loans->loan_unique_id)
            ->where($branch_code . '_loan_charges_compulsory_join.loan_charge_id', '!=', NULL)
            ->select('loan_charges_type.*', 'loan_charges_type.id as charge_id')
            ->get();
        // return response()->json($loans[$i]->loan_term);

        $repayment_term = '';
        if ($loans->loan_term == 'Month') {
            $repayment_term = 'Monthly';
        } else if ($loans->loan_term == 'Day') {
            $repayment_term = 'Daily';
        } else if ($loans->loan_term == 'Week') {
            $repayment_term = 'Weekly';
        } else if ($loans->loan_term == 'Two-Weeks') {
            $repayment_term = 'Two-Weeks';
        } else if ($loans->loan_term == 'Four-Weeks') {
            $repayment_term = 'Four-Weeks';
        } else {
            $repayment_term = 'Yearly';
        }

        // if(DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->nrc){
        //     if(DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->old_nrc){
        //         $nrc=DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->old_nrc;
        //     }else{
        //         $nrc=NULL;
        //     }
        // }else{
        //     $nrc=DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->nrc;
        // }
        if($loans->nrc == NULL){
            if($loans->old_nrc == NULL){
                $nrc=NULL;
            }else{
                $nrc=$loans->old_nrc;
            }
        }else{
            $nrc=$loans->nrc;
        }

        if (count($chargeDetail) > 2) {
            $result = $client->post($routeurl, [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Authorization' => 'Bearer ' . $authorization,
                ],
                'form_params' => [
                    'client_id' => $clientid,
                    // 'client_nrc_number' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->nrc == NULL ? DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->old_nrc : DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->nrc,
                    'client_nrc_number'=>$nrc,
                    'client_name' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->name,
                    'client_phone' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->phone_primary,
                    'you_are_a_group_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_group_leader,
                    'you_are_a_center_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_center_leader,
                    'business_proposal' => 'business_proposal',
                    'guarantor_id' => $guarantor_a,
                    'guarantor2_id' => $guarantor_b,
                    'guarantor3_id' => $guarantor_c,
                    'disbursement_number' => $main_loan_generate_id,
                    'branch_id' => $branch_id,
                    'loan_officer_id' => $staffid,
                    'loan_production_id' => $loans->loan_type_id,
                    'loan_application_date' => $loans->loan_application_date,
                    'first_installment_date' => $loans->first_installment_date,
                    'loan_amount' => $loans->loan_amount,
                    'interest_rate' => $interest_rate,
                    'interest_rate_period' => $loans->interest_rate_period,
                    'loan_term' => $loans->loan_term,
                    'loan_term_value' => $loans->loan_term_value,
                    'repayment_term' => $repayment_term,
                    'currency_id' => 1,
                    'transaction_type_id' => 2,
                    'group_loan_id' => $groupid,
                    'center_leader_id' => $centerid,
                    'charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
                    'charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
                    'charge_id[6981667280779875]' => $chargeDetail[2]->charge_id,
                    'name[3061667280779394]' => $chargeDetail[0]->charge_name,
                    'name[6471667280779921]' => $chargeDetail[1]->charge_name,
                    'name[6981667280779875]' => $chargeDetail[2]->charge_name,
                    'amount[3061667280779394]' => $chargeDetail[0]->amount,
                    'amount[6471667280779921]' => $chargeDetail[1]->amount,
                    'amount[6981667280779875]' => $chargeDetail[2]->amount,
                    'charge_option[3061667280779394]' => $chargeDetail[0]->charge_option,
                    'charge_option[6471667280779921]' => $chargeDetail[1]->charge_option,
                    'charge_option[6981667280779875]' => $chargeDetail[2]->charge_option,
                    'charge_type[3061667280779394]' => $chargeDetail[0]->charge_type,
                    'charge_type[6471667280779921]' => $chargeDetail[1]->charge_type,
                    'charge_type[6981667280779875]' => $chargeDetail[2]->charge_type,
                    'status[3061667280779394]' => 'Yes',
                    'status[6471667280779921]' => 'Yes',
                    'status[6981667280779875]' => 'Yes',
                    // 'charge_id'=> $charge_detail->id,
                    // 'name'=> $charge_detail->charge_name, //'Sinking Fun(0.5%)'
                    // 'amount'=> $charge_detail->amount,//'0.5',
                    // 'charge_option'=> $charge_detail->charge_option,//'2',
                    // 'charge_type'=>$charge_detail->charge_type, //'1'
                    // 'status'=>$charge_detail->status,//'Yes',
                    'compulsory_id' => 5,
                    'compound_interest' => 1,
                    'override_cycle' => 'no',
                    'product_name' => 'Saving 5%',
                    'saving_amount' => 5,
                    'c_charge_option' => 2,
                    'c_interest_rate' => 1.25,
                    'compulsory_product_type_id' => 1,
                    'c_status' => 'Yes',
                    'loan_charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
                    'loan_charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
                    'loan_charge_id[6981667280779875]' => $chargeDetail[2]->charge_id,
                    // 'loan_charge_id'=> 'Null',
                    'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans->loan_officer_id)->first()->main_staff_id,
                ]
            ]);

            $response = (string) $result->getBody();
            // return response()->json($result);
            $response = json_decode($response); // Using this you can access any key like below
            // return response()->json(['status_code' => 200, 'message' => 're-crawling loan success', 'data' => $response]);
            if ($response->status_code == 200) {
                // return response()->json($response);
                $dataloan = DB::connection('main')->table('loans_' . $branch_id)->orderBy('id', 'desc')->first();
                DB::table('tbl_main_join_loan')->insert([
                    'main_loan_id' => $dataloan->id,
                    'main_loan_code' => $dataloan->disbursement_number,
                    'portal_loan_id' => $loans->loan_unique_id,
                    'branch_id' => $branch_id
                ]);
                $getID = DB::table('tbl_main_join_loan')->where('portal_loan_id', $loans->loan_unique_id)->where('branch_id', $branch_id)->first()->main_loan_id;

                //add scheduels join
                $LoansSchedule = DB::connection('portal')->table($branch_code . '_loans_schedule')->where('loan_unique_id', $loans->loan_unique_id)->get();
                $mainSchedule = DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $getID)->get();

                for ($t = 0; $t < count($LoansSchedule); $t++) {
                    if (DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$t]->id)->where('branch_id', $branch_id)->first()) {
                        break;
                    } else {
                        if ($mainSchedule[$t]->no == ($t + 1)) {
                            $mainJoinSchedule = new Crawl_Loan_Repayment_Schedule();
                            $mainJoinSchedule->portal_disbursement_id = $LoansSchedule[$t]->loan_unique_id;
                            $mainJoinSchedule->portal_disbursement_schedule_id = $LoansSchedule[$t]->id;
                            $mainJoinSchedule->main_disbursement_schedule_id = $mainSchedule[$t]->id;
                            $mainJoinSchedule->branch_id = $branch_id;
                            $mainJoinSchedule->save();
                        }
                    }
                }

            } else {
                //return redirect('loan/')->with('Loan Create Fail');
            }
        } else {
            $result = $client->post($routeurl, [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Authorization' => 'Bearer ' . $authorization,
                ],
                'form_params' => [
                    'client_id' => $clientid,
                    'client_nrc_number' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->nrc == NULL ? DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->old_nrc : DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->nrc,
                    'client_name' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->name,
                    'client_phone' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->phone_primary,
                    'you_are_a_group_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_group_leader,
                    'you_are_a_center_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_center_leader,
                    'business_proposal' => 'business_proposal',
                    'guarantor_id' => $guarantor_a,
                    'guarantor2_id' => $guarantor_b,
                    'guarantor3_id' => $guarantor_c,
                    'disbursement_number' => $main_loan_generate_id,
                    'branch_id' => $branch_id,
                    'loan_officer_id' => $staffid,
                    'loan_production_id' => $loans->loan_type_id,
                    'loan_application_date' => $loans->loan_application_date,
                    'first_installment_date' => $loans->first_installment_date,
                    'loan_amount' => $loans->loan_amount,
                    'interest_rate' => $interest_rate,
                    'interest_rate_period' => $loans->interest_rate_period,
                    'loan_term' => $loans->loan_term,
                    'loan_term_value' => $loans->loan_term_value,
                    'repayment_term' => $repayment_term,
                    'currency_id' => 1,
                    'transaction_type_id' => 2,
                    'group_loan_id' => $groupid,
                    'center_leader_id' => $centerid,
                    'charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
                    'charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
                    'name[3061667280779394]' => $chargeDetail[0]->charge_name,
                    'name[6471667280779921]' => $chargeDetail[1]->charge_name,
                    'amount[3061667280779394]' => $chargeDetail[0]->amount,
                    'amount[6471667280779921]' => $chargeDetail[1]->amount,
                    'charge_option[3061667280779394]' => $chargeDetail[0]->charge_option,
                    'charge_option[6471667280779921]' => $chargeDetail[1]->charge_option,
                    'charge_type[3061667280779394]' => $chargeDetail[0]->charge_type,
                    'charge_type[6471667280779921]' => $chargeDetail[1]->charge_type,
                    'status[3061667280779394]' => 'Yes',
                    'status[6471667280779921]' => 'Yes',
                    // 'charge_id'=> $charge_detail->id,
                    // 'name'=> $charge_detail->charge_name, //'Sinking Fun(0.5%)'
                    // 'amount'=> $charge_detail->amount,//'0.5',
                    // 'charge_option'=> $charge_detail->charge_option,//'2',
                    // 'charge_type'=>$charge_detail->charge_type, //'1'
                    // 'status'=>$charge_detail->status,//'Yes',
                    'compulsory_id' => 5,
                    'compound_interest' => 1,
                    'override_cycle' => 'no',
                    'product_name' => 'Saving 5%',
                    'saving_amount' => 5,
                    'c_charge_option' => 2,
                    'c_interest_rate' => 1.25,
                    'compulsory_product_type_id' => 1,
                    'c_status' => 'Yes',
                    'loan_charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
                    'loan_charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
                    // 'loan_charge_id'=> 'Null',
                    'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans->loan_officer_id)->first()->main_staff_id,
                ]
            ]);

            $response = (string) $result->getBody();
            // return response()->json($result);
            $response = json_decode($response); // Using this you can access any key like below
            // return response()->json(['status_code' => 200, 'message' => 're-crawling loan success', 'data' => $response]);
            if ($response->status_code == 200) {
                // return response()->json($response);\
                $dataloan = DB::connection('main')->table('loans_' . $branch_id)->orderBy('id', 'desc')->first();
                DB::table('tbl_main_join_loan')->insert([
                    'main_loan_id' => $dataloan->id,
                    'main_loan_code' => $dataloan->disbursement_number,
                    'portal_loan_id' => $loans->loan_unique_id,
                    'branch_id' => $branch_id
                ]);
                $getID = DB::table('tbl_main_join_loan')->where('portal_loan_id', $loans->loan_unique_id)->where('branch_id', $branch_id)->first();
                    //add scheduels join
                    $LoansSchedule = DB::connection('portal')->table($branch_code . '_loans_schedule')->where('loan_unique_id', $loans->loan_unique_id)->get();
                    $mainSchedule = DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $getID->main_loan_id)->get();

                    for ($t = 0; $t < count($LoansSchedule); $t++) {
                        if (DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$t]->id)->where('branch_id', $branch_id)->first()) {
                            break;
                        } else {
                            if ($mainSchedule[$t]->no == ($t + 1)) {
                                $mainJoinSchedule = new Crawl_Loan_Repayment_Schedule();
                                $mainJoinSchedule->portal_disbursement_id = $LoansSchedule[$t]->loan_unique_id;
                                $mainJoinSchedule->portal_disbursement_schedule_id = $LoansSchedule[$t]->id;
                                $mainJoinSchedule->main_disbursement_schedule_id = $mainSchedule[$t]->id;
                                $mainJoinSchedule->branch_id = $branch_id;
                                $mainJoinSchedule->save();
                            }
                        }
                    }


                    // // activate
                    // if ($loans->disbursement_status == 'Approved') {
                    //     DB::connection('main')->table('loans_' . $branch_id)->where('id', $getID->main_loan_id)->update([
                    //         'disbursement_status' => 'Approved',
                    //         'status_note_date_approve' => $loans->approved_date,
                    //         'status_note_approve_by_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans->loan_officer_id)->first()->main_staff_id
                    //     ]);
                    // }

                    // $dataloan = DB::connection('main')->table('loans_' . $y)->orderBy('id', 'desc')->first();
                    // DB::table('tbl_main_join_loan')->insert([
                    //     'main_loan_id' => $dataloan->id,
                    //     'main_loan_code' => $dataloan->disbursement_number,
                    //     'portal_loan_id' => $loans[$i]->loan_unique_id,
                    //     'branch_id' => $y
                    // ]);
                
            } else {
                //return redirect('loan/')->with('Loan Create Fail');
            }
        }
    }

}