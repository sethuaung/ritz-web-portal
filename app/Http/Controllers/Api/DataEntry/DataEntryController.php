<?php

namespace App\Http\Controllers\Api\DataEntry;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class DataEntryController extends Controller
{
    public function crawlClients()
    {
        //required fields: id, staff_client_id, branch_id, type_status
        $clients = DB::connection('main')->table('clients')->get();
    	return response()->json($clients);

        //clear portal table
        // DB::connection('portal')->statement('DELETE FROM tbl_client_basic_info');
        // DB::connection('portal')->statement('DELETE FROM tbl_client_family_info');
        // DB::connection('portal')->statement('DELETE FROM tbl_client_join');
        // DB::connection('portal')->statement('DELETE FROM tbl_client_job_main');
        // DB::connection('portal')->statement('DELETE FROM tbl_client_photo');
        // DB::connection('portal')->statement('DELETE FROM tbl_client_survey_ownership');
        DB::connection('portal')->statement('ALTER TABLE tbl_client_basic_info AUTO_INCREMENT = 1');
        DB::connection('portal')->statement('ALTER TABLE tbl_client_family_info AUTO_INCREMENT = 1');
        DB::connection('portal')->statement('ALTER TABLE tbl_client_join AUTO_INCREMENT = 1');
        DB::connection('portal')->statement('ALTER TABLE tbl_client_job_main AUTO_INCREMENT = 1');
        DB::connection('portal')->statement('ALTER TABLE tbl_client_photo AUTO_INCREMENT = 1');
        DB::connection('portal')->statement('ALTER TABLE tbl_client_survey_ownership AUTO_INCREMENT = 1');

                
        //insert data to portal's table

        foreach ($clients as $one_client) {
            if($one_client->nrc_type == "Old Format" ) {
                    DB::connection('portal')->table('tbl_client_basic_info')->insert([
                        //'id' => $one_client->id == null ? '' : $one_client->id ,
                        'client_uniquekey'=> $one_client->client_number == null ? '' : $one_client->client_number,
                        'name' => $one_client->name == null ? '' : $one_client->name,
                        //'name' => $one_client->name,
                        'name_mm' => $one_client->name_other == null ? '' : $one_client->name_other,
                        'dob' => $one_client->dob == null ? '' : $one_client->dob,
                        //'nrc' => '',
                        'old_nrc' => $one_client->nrc_number == null ? '' : $one_client->nrc_number,
                        //'nrc_card_id' => '',
                        'gender' => $one_client->gender == null ? '' : $one_client->gender,
                        'phone_primary' => $one_client->primary_phone_number == null ? '' : $one_client->primary_phone_number,
                        'phone_secondary' => $one_client->alternate_phone_number == null ? '' : $one_client->alternate_phone_number,
                        //'email' => '',
                        //'blood_type' => '',
                        //'religion' => '',
                        //'nationality' => '',
                        'education_id' => $one_client->education == null ? '' : $one_client->education,
                        //'other_education' => '',
                        'village_id' => $one_client->village_id == null ? '' : $one_client->village_id,
                        'province_id' => $one_client->province_id == null ? '' : $one_client->province_id,
                        'quarter_id' => $one_client->commune_id == null ? '' : $one_client->commune_id,
                        //'township_id' => '',
                        'district_id' => $one_client->district_id == null ? '' : $one_client->district_id,
                        //'division_id' => '',
                        //'city_id' => '',
                        'address_primary' => $one_client->address1 == null ? '' : $one_client->address1,
                        'address_secondary' => $one_client->address2 == null ? '' : $one_client->address2,
                        //'client_type' => $one_client->condition,
                        'status' => 'none' //default
                    ]);
            }
            else if($one_client->nrc_type == "New Format" ) {
                DB::connection('portal')->table('tbl_client_basic_info')->insert([
                        //'id' => $one_client->id == null ? '' : $one_client->id,
                        'client_uniquekey'=> $one_client->client_number == null ? '' : $one_client->client_number,
                        'name' => $one_client->name == null ? '' : $one_client->name,
                        //'name' => $one_client->name,
                        'name_mm' => $one_client->name_other == null ? '' : $one_client->name_other,
                        'dob' => $one_client->dob == null ? '' : $one_client->dob,
                        'nrc' => $one_client->nrc_number == null ? '' : $one_client->nrc_number,
                        //'old_nrc' => $one_client->nrc_number,
                        //'nrc_card_id' => '',
                        'gender' => $one_client->gender == null ? '' : $one_client->gender,
                        'phone_primary' => $one_client->primary_phone_number == null ? '' : $one_client->primary_phone_number,
                        'phone_secondary' => $one_client->alternate_phone_number == null ? '' : $one_client->alternate_phone_number,
                        //'email' => '',
                        //'blood_type' => '',
                        //'religion' => '',
                        //'nationality' => '',
                        'education_id' => $one_client->education == null ? '' : $one_client->education,
                        //'other_education' => '',
                        'village_id' => $one_client->village_id == null ? '' : $one_client->village_id,
                        'province_id' => $one_client->province_id == null ? '' : $one_client->province_id,
                        'quarter_id' => $one_client->commune_id == null ? '' : $one_client->commune_id,
                        //'township_id' => '',
                        'district_id' => $one_client->district_id == null ? '' : $one_client->district_id,
                        //'division_id' => '',
                        //'city_id' => '',
                        'address_primary' => $one_client->address1 == null ? '' : $one_client->address1,
                        'address_secondary' => $one_client->address2 == null ? '' : $one_client->address2,
                        //'client_type' => $one_client->condition,
                        'status' => 'none'
                ]);      
            }

            DB::connection('portal')->table('tbl_client_family_info')->insert([
                //'id' => $one_client->id == null ? '' : $one_client->id,
                'client_uniquekey' => $one_client->client_number == null ? '' : $one_client->client_number,
                'father_name' => $one_client->father_name == null ? '' : $one_client->father_name,
                'marital_status' => $one_client->marital_status == null ? '' : $one_client->marital_status,
                'spouse_name' => $one_client->husband_name == null ? '' : $one_client->husband_name,
                'occupation_of_spouse' => $one_client->occupation_of_husband == null ? '' : $one_client->occupation_of_husband,
                'no_of_family' => $one_client->no_of_person_in_family == null ? '' : $one_client->no_of_person_in_family,
                'no_of_working_family' => $one_client->no_of_working_people == null ? '' : $one_client->no_of_working_people,
                'no_of_children' => $one_client->no_children_in_family == null ? '' : $one_client->no_children_in_family,
                //'no_of_working_progeny' => $one_client->no_of_working_people
            ]);

            DB::connection('portal')->table('tbl_client_job_main')->insert([
                //'id' => $one_client->id == null ? '' : $one_client->id,
                'client_uniquekey' => $one_client->client_number == null ? '' : $one_client->client_number,
                //'industry_id' => '',
                //'job_status' what to put?
                //'job_position_id' => $one_client->occupation,
                //'department_id' what to put?
                //'experience'
                //'experience2'
                //'salary' => $one_client->
                //'company_name'
                //'company_phone'
                'company_address' => $one_client->company_address1 == null ? '' : $one_client->company_address1
                //'current_business' 
                //'business_income'
            ]);

            DB::connection('portal')->table('tbl_client_join')->insert([
                //'id' =>$one_client->id == null ? '' : $one_client->id,
                'client_id' =>$one_client->client_number == null ? '' : $one_client->client_number,
                'branch_id' => $one_client->branch_id == null ? '' : $one_client->branch_id,
                'staff_id' => $one_client->loan_officer_id == null ? '' : $one_client->loan_officer_id,
                //'guarantors_id'
                'group_id' => $one_client->customer_group_id == null ? '' : $one_client->customer_group_id,
                //'center_id' => $one_client->center_code,
                //'client_status' => $one_client->condition 
                'registration_date' => $one_client->register_date == null ? '' : $one_client->register_date    
            ]);

            DB::connection('portal')->table('tbl_client_photo')->insert([
                'client_uniquekey' =>$one_client->client_number == null ? '' : $one_client->client_number,
                'client_photo' => $one_client->photo_of_client == null ? '' : $one_client->photo_of_client,
                //'recognition_front' => '',
                //'recognition_back' => '',
                //'nrc_recommendation' => '',
                ////'recognition_status' => '',
                'registration_family_form_front' => $one_client->family_registration_copy == null ? '' : $one_client->family_registration_copy, 
                //'registration_family_form_back' => '',
                'finger_print_bio' => $one_client->scan_finger_print == null ? '' : $one_client->scan_finger_print,
                //'government_recommendations_photo' => ''
            ]);
            
            DB::connection('portal')->table('tbl_client_survey_ownership')->insert([
                'client_uniquekey' => $one_client->client_number == null ? '' : $one_client->client_number ,
                // 'client_photo' => '',
                // 'assetstype_removable' => '',
                // 'assetstype_unremovable' => '',
                // 'purchase_price' => '',
                // 'current_value' => '',
                // 'quantity' => '', 
            ]);
        }

        $verify = DB::connection('portal')->table('tbl_client_basic_info')->get();
        if ($verify) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => count($verify)]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }

}




