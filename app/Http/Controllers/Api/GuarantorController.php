<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Education;
use App\Models\ClientDataEnteries\Guarantor;
use App\Models\ClientDataEnteries\Staff;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class GuarantorController extends Controller
{
    //auto generate client uniquekey
    public function generateGuarantorID($staffid){
        $guarantorkey = Guarantor::where('branch_id', Staff::where('staff_code', $staffid)->first()->branch_id)->get()->last();
            // dd($guarantorkey->guarantor_uniquekey);
        if ($guarantorkey == null) {
                $bcode = strtoupper($this->getBranchCode($staffid));
                $guarantorkey = $bcode;
                $guarantorkey .= '-';
                $guarantorkey .= '02';
                $guarantorkey .= '-';
                return $guarantorkey .= '0000001';
        } else {
                $guarantorkey = $guarantorkey->guarantor_uniquekey;
                $bstr = substr($guarantorkey, 3);
                // dd($bstr);
                $id = substr($bstr, -7, 7);
                // dd($id);
                $bstr_cusm = $id + 1;
                $bstr_cusm = (string)str_pad($bstr_cusm, 7, '0', STR_PAD_LEFT);

                // get loan_uniquekey in tbl_loans
                $guarantorkey = strtoupper($this->getBranchCode($staffid)); // Branch Code
                $guarantorkey .= '-';
                $guarantorkey .= '02';
                $guarantorkey .= '-';
                return  $guarantorkey .= $bstr_cusm;
        }
    }

    public function getBranchCode($staff_code)
    {
        //dd($staff_code);
        $B_code = DB::table('tbl_staff')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->select('tbl_branches.branch_code')
            ->where('tbl_staff.staff_code', $staff_code)
            ->first();
        $b_code = strtolower($B_code->branch_code);
        return $b_code;
    }
    // Storage new guarantor
    public function createGuarantor (Request $request)
    {
        $staffid = $request->staffid;
        $branchid = $request->branchid;
        $guarantoruniid = $this->generateGuarantorID($staffid);

        if($request->education){
            $obj_education = DB::table('tbl_educations')->where('education_name', 'LIKE', $request->education.'%')->first();
        }
    
		// return response()->json(['status_code'=>200,'message'=> $branchid]);
        $entery = Guarantor::insert([
            'guarantor_uniquekey'=> $guarantoruniid, 
            'branch_id'=> $branchid,
            'name'=> $request->name_en,
            'name_mm'=> $request->name_mm, 
            'dob'=> date('Y-m-d', strtotime($request->dob)), 
            'gender'=> strtolower($request->gender), 
        	'nrc'=> $request->nrc_number,
        	'old_nrc'=> $request->nrc_old ?? '',
        	'nrc_card_id'=> '', 
            'phone_primary'=> $request->primary_phone, 
            'phone_secondary'=> $request->secondary_phone, 
            'email'=> $request->mail, 
            'blood_type'=> $request->blood_type, 
            'religion'=> $request->religion, 
            'nationality'=> $request->nationality, 
            'education_id' => $obj_education->id ?? '',
            'other_education'=> "null", 

            'village_id'=> $request->village_id, 
            'city_id' => "0", 
            'quarter_id'=> $request->quarter_id, 
            'township_id'=> $request->township_id, 
            'district_id'=> $request->district_id,  
            'division_id'=> $request->division_id,  
            'address_primary'=> $request->address1, 
            'address_secondary'=> $request->address2, 
            'status'=> "active",  

            'current_job'=> $request->current_business, 
            'income'=> $request->current_income,  

            'guarantor_photo' => $this->converttoImage($guarantoruniid,$request->photo_of_guarantor) , 
            'guarantor_nrc_front_photo' => $this->converttoImage($guarantoruniid,$request->nrc_photo_front), 
            'guarantor_nrc_back_photo' => $this->converttoImage($guarantoruniid,$request->nrc_photo_back), 
            'kyc_photo' => $this->converttoImage($guarantoruniid,$request->registration_family_form_front),
            'nrc_recommendation' => $this->converttoImage($guarantoruniid,$request->registration_family_form_back),
        ]);
    	
    	// $this->liveReverseGuarantors($guarantoruniid);

        if($entery){
            return response()->json(['status_code'=>200,'message'=>'success to create new guarantor','data'=>$entery]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail to create new guarantor','data'=>null]);
        }
    }

    //decode base64 string
    public function converttoImage($dataclientid,$dataimage)
    {
        $image = str_replace('data:image/png;base64,', '', $dataimage);
        $image_name = uniqid().'_'.$dataclientid.'_'.time().'.jpg';
    
        $disk = Storage::disk('guarantor-photos')->put($image_name, base64_decode($image));
        return $image_name;
    }

// 	public function liveReverseGuarantors($guarantorkey)
//     {

//         $guarantors = DB::connection('portal')
//             ->table('tbl_guarantors')
//             ->leftJoin('tbl_center', 'tbl_center.branch_id', '=', 'tbl_guarantors.branch_id')
//             ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_guarantors.branch_id')
//             ->where('guarantor_uniquekey',$guarantorkey)
//             ->select('tbl_center.*', 'tbl_guarantors.*', 'tbl_branches.id as branch_id')
//             ->first();


//         if ($guarantors->staff_id) {
//             if (
//                 DB::table('tbl_main_join_guarantor')
//                 ->where('portal_guarantor_id', $guarantors->guarantor_uniquekey)
//                 ->first()
//             ) {
//             }else{
//                 DB::connection('main')
//                 ->table('guarantors')
//                 ->insert([
//                     'nrc_number' => $guarantors->nrc == null ? $guarantors->old_nrc : $guarantors->nrc,
//                     'full_name_en' => $guarantors->name,
//                     'full_name_mm' => $guarantors->name_mm,
//                     'mobile' => $guarantors->phone_primary,
//                     'phone' => $guarantors->phone_secondary,
//                     'email' => $guarantors->email,
//                     'dob' => $guarantors->dob,
//                     'photo' => $guarantors->guarantor_photo,
//                     'address' => $guarantors->address_primary,
//                     'branch_id' => $guarantors->branch_id,
//                     'center_leader_id' => $guarantors->staff_client_id,
//                     'income' => $guarantors->income,
//                     'created_at' => $guarantors->created_at,
//                     'updated_at' => $guarantors->updated_at,

//                 ]);

//             DB::table('tbl_main_join_guarantor')->insert([
//                 'main_guarantor_id' => DB::connection('main')
//                     ->table('guarantors')
//                     ->where('nrc_number', $guarantors->old_nrc)
//                     ->orWhere('nrc_number', $guarantors->nrc)
//                     ->first()->id,
//                 'portal_guarantor_id' => $guarantors->guarantor_uniquekey,
//             ]);
//             }
            
//         }
            
        
//     }
}
