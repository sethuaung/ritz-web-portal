<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Clients\ClientBasicInfo;
use App\Models\Clients\ClientFamilyInfo;
use App\Models\Clients\ClientJob;
use App\Models\Clients\ClientJoin;
use App\Models\Clients\ClientPhoto;
use App\Models\Clients\ClientSurveyOwnership;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ClientController extends Controller
{
    //auto generate client uniquekey
    public function generateClientID($staffid){
        
        $obj_branchcode = DB::table('tbl_staff')
                            ->join('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
                            ->where('tbl_staff.staff_code',$staffid)
                            ->select('tbl_branches.branch_code')
                            ->get();
                           
        if (count(array($obj_branchcode)) > 0) {
            $subcode = substr($obj_branchcode[0]->branch_code,0,2); //split branch code
            // get client_uniquekey in tbl_client_basic_info
            $obj_fund = DB::table('tbl_client_basic_info')->where('client_uniquekey', 'LIKE', $subcode.'%')->get()->last();
            if($obj_fund != null){
                if (count(array($obj_fund)) > 0) {
                    $subnum = substr($obj_fund->client_uniquekey, 6); // sub-string to 01-000001
                    $addnum = $subnum+1; // add id in +1
                    $cusmclientidnum =  (string)str_pad($addnum, 7, '0', STR_PAD_LEFT); // add generate in zero
                    $clientid = $subcode; // Branch Code
                    $clientid .= '-';
                    $clientid .= '01';
                    $clientid .= '-';
                    $clientid .= $cusmclientidnum; // Client ID in branch
                    return $clientid;
                }else{
                    return response()->json(['status_code'=>500,'message'=>'client create fail','data'=>null]);
                }
            }else{
                $subcode = substr($obj_branchcode[0]->branch_code,0,2); //split branch code
                return $clientid = $subcode.'-01-0000001';
            }
           
        } else {
            $subcode = substr($obj_branchcode[0]->branch_code,0,2); //split branch code
            return $clientid = $subcode.'-01-0000001';
        }
    }

    // Storage new client
    public function createClient (Request $request)
    {
    	//dd($request);
        // generate client id
        $staffid = $request->staffid;
        $clientuniid = $this->generateClientID($staffid);
      
        if($request->education){
            $obj_education = DB::table('tbl_educations')->where('education_name', 'LIKE', $request->education.'%')->first();
        }

        if($request->industry_id){
            $obj_industry = DB::table('tbl_industry_type')->where('industry_name', 'LIKE', $request->industry_id.'%')->first();
        }
        
        if($request->department_id){
            $obj_department = DB::table('tbl_job_department')->where('department_name', 'LIKE', $request->department_id.'%')->first();
        }
       
        if($request->job_position_id){
            $obj_job_position = DB::table('tbl_job_position')->where('job_position_name', 'LIKE', $request->job_position_id.'%')->first();
        }
        // Add data to database
    	//return response()->json(['status_code'=>422,'message'=>$request->job_status,'data'=>null]);
        $data_client = ClientBasicInfo::insert([
            
            'client_uniquekey' =>  $clientuniid,
            'name' => $request->name_en ?? '', 
            'name_mm' => $request->name_mm ?? '', 
            'dob' => $request->dob ?? '', 
            'nrc' => $request->nrc_number ?? '', 
            'old_nrc' => $request->nrc_number ?? '', 
            'nrc_card_id' => "", 
            'gender' => $request->gender ?? 'Others', 
            'phone_primary' => $request->primary_phone ?? '', 
            'phone_secondary' => $request->secondary_phone ?? '', 
            'email' => "", 
            'blood_type' => $request->blood_type ?? 'none', 
            'religion' => $request->religion ?? '', 
            'nationality' => $request->nationality ?? '', 
            'education_id' => $obj_education->id ?? '',
            'other_education' => "", 
            'village_id' => $request->village_id ?? '', 
            'province_id' => $request->province_id ?? '', 
            'quarter_id' => $request->quarter_id ?? '', 
            'township_id' => $request->blood_type ?? '', 
            'district_id' => $request->district_id ?? '', 
            'division_id' => $request->division_id ?? '', 
            'city_id' => "" ?? '', 
            'address_primary' => $request->address1 ?? '', 
            'address_secondary' => $request->address2 ?? '',
            'client_type' =>  $request->clinettype ?? 'new', 
            'status' => 'active',
        ]);
        //return response()->json(['status_code'=>200,'message'=> $data_client,'data'=>null]);
        
        ClientFamilyInfo::insert([
            'client_uniquekey' => $clientuniid,
            'father_name' => $request->father_name ?? '',
            'marital_status' => $request->marital_status ?? 'None',
            'spouse_name' => $request->spouse_name ?? '',
            'occupation_of_spouse' => $request->occupation_of_spouse ?? '',
            'no_of_family' => $request->no_of_family ?? '',
            'no_of_working_family' => $request->no_of_working_family ?? '',
            'no_of_children' => $request->no_of_children ?? '',
            'no_of_working_progeny' => $request->no_of_working_progeny ?? '',
        ]);

        ClientJob::insert([
            'client_uniquekey' => $clientuniid, 
            'current_business' => $request->current_business ?? '',
            'business_income' => $request->current_income ?? '',
            'job_status' => $request->job_status ?? '',
            'industry_id' => $obj_industry->id ?? '',
            'department_id' => $obj_department->id ?? '',
            'job_position_id' => $obj_job_position->id ?? '', 
            'experience' => $request->experience ?? '',
            'salary' => $request->salary ?? '', 
            'company_name' => $request->company_name ?? '', 
            'company_phone' => $request->company_phone ?? '',
            'company_address' => $request->company_address ?? '',
        ]);

        ClientSurveyOwnership::insert([
            'client_uniquekey' => $clientuniid,
            //'land' => $request->land,
            //'ownership' => $request->ownership,
            'assetstype_removable' => $request->assetstype_removable ?? '',
            'assetstype_unremovable' => $request->assetstype_unremovable ?? '',
            'purchase_price' => $request->purchase_price ?? '',
            'current_value' => $request->current_value ?? '',
            'quantity' => $request->quantity ?? '',
            'attach_1' => $this->converttoImageforSurveyOwnership($clientuniid,$request->attach_1), 
            'attach_2' => $this->converttoImageforSurveyOwnership($clientuniid,$request->attach_2),
            'attach_3' => $this->converttoImageforSurveyOwnership($clientuniid,$request->attach_3),
            'attach_4' => $this->converttoImageforSurveyOwnership($clientuniid,$request->attach_4),
        ]);

        ClientPhoto::insert([
            'client_uniquekey'=> $clientuniid, 
            'client_photo' => $this->converttoImageforClient($clientuniid,$request->photo_of_client) , 
            'recognition_front' => $this->converttoImageforClient($clientuniid,$request->nrc_photo_front), 
            'recognition_back' => $this->converttoImageforClient($clientuniid,$request->nrc_photo_back), 
            'recognition_status' => $request->recognition_status, 
            'registration_family_form_front' => $this->converttoImageforClient($clientuniid,$request->registration_family_form_front),
            'registration_family_form_back' => $this->converttoImageforClient($clientuniid,$request->registration_family_form_back), 
            'finger_print_bio' => 'null', 
            'government_recommendations_photo' => 'null',
        ]);

        $entery = ClientJoin::insert([
            'client_id' => $clientuniid,
            'branch_id' => $request->join_branch_id,
            'staff_id' => $request->join_staff_id,
            'guarantors_id' => $request->join_guarantors_id,
            'group_id' => $request->join_group_id,
            'center_id' => $request->join_center_id,
            'loan_id' => '00',
            'deposit_id' => '00',
            'disbursement_id' => '00',
            'repayment_id' => '00',
            'saving_withdrawal_id' => '00',
            'registration_date' => $request->join_register_date,
            'client_status' => $request->join_client_status,
        ]);

        $this->liveReverseClient($entery->client_id);

        if($entery){
            return response()->json(['status_code'=>200,'message'=>'success to create new client','data'=>$entery]);
        }else{
            return response()->json(['status_code'=>422,'message'=>'fail to create new client','data'=>null]);
        }
    }

    //decode base64 string for client photo
    public function converttoImageforClient($dataclientid,$dataimage)
    {
        $image = str_replace('data:image/png;base64,', '', $dataimage);
        $image_name = uniqid().'_'.$dataclientid.'_'.time().'.jpg';
    
        $disk = Storage::disk('clients-photo')->put($image_name, base64_decode($image));
        return $image_name;
    }

    //decode base64 string for surveyownership
    public function converttoImageforSurveyOwnership($dataclientid,$dataimage)
    {
        $image = str_replace('data:image/png;base64,', '', $dataimage);
        $image_name = uniqid().'_'.$dataclientid.'_'.time().'.jpg';
    
        $disk = Storage::disk('clients-ownership-photo')->put($image_name, base64_decode($image));
        return $image_name;
    }

    public function liveReverseClient($portal_client_id)
    {
        //live crawl start
        $clients = DB::table('tbl_client_basic_info')
            ->leftjoin('tbl_client_family_info', 'tbl_client_family_info.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftjoin('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftjoin('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftjoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftjoin('tbl_client_survey_ownership', 'tbl_client_join.client_id', '=', 'tbl_client_survey_ownership.client_uniquekey')
            // ->leftJoin('tbl_group', function ($advancedLeftJoin) {
            //     $advancedLeftJoin->on('tbl_client_join.group_id', '=', 'tbl_group.group_uniquekey');
            // })
            // ->leftJoin('tbl_center', function ($advancedLeftJoin) {
            //     $advancedLeftJoin->on('tbl_client_join.center_id', '=', 'tbl_center.center_uniquekey');
            // })
            ->leftJoin('tbl_educations', function ($advancedLeftJoin) {
                $advancedLeftJoin->on('tbl_client_basic_info.education_id', '=', 'tbl_educations.id');
            })
            ->leftJoin('tbl_branches', function ($advancedLeftJoin) {
                $advancedLeftJoin->on('tbl_branches.id', '=', 'tbl_client_join.branch_id'); //edited
            })
            ->leftJoin('tbl_center', 'tbl_center.staff_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftJoin('tbl_group', 'tbl_group.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
            ->where('tbl_client_basic_info.client_uniquekey', $portal_client_id)
            ->select('tbl_group.client_id AS group_leader_id', 'tbl_client_basic_info.*', 'tbl_client_basic_info.created_at AS Ccreated_at', 'tbl_client_basic_info.updated_at AS Cupdated_at', 'tbl_client_basic_info.phone_primary AS client_phone', 'tbl_client_family_info.*', 'tbl_client_job_main.*', 'tbl_client_photo.*', 'tbl_client_join.*', 'tbl_client_survey_ownership.*', 'tbl_educations.*', 'tbl_branches.branch_name AS branch_name', 'tbl_center.staff_client_id')
            ->first();
        // dd($clients);
        //client_type
        if ($clients->client_status == 'new') {
            $customer_group_id = "1";
        } elseif ($clients->client_status == 'active') {
            $customer_group_id = "1";
        } elseif ($clients->client_status == 'dropout') {
            $customer_group_id = "3";
        } elseif ($clients->client_status == 'dead') {
            $customer_group_id = "2";
        } elseif ($clients->client_status == 'rejoin') {
            $customer_group_id = "4";
        } elseif ($clients->client_status == 'substitute') {
            $customer_group_id = "5";
        } elseif ($clients->client_status == 'dormant') {
            $customer_group_id = "13";
        } elseif ($clients->client_status == 'reject') {
            $customer_group_id = "13";
        }
        // center
        $centerid = DB::table('tbl_main_join_center')->where('portal_center_id', $clients->center_id)->first();
        if ($centerid) {
            $centerid = $centerid->main_center_id;
        }
        $groupid = DB::table('tbl_main_join_group')->where('portal_group_id', $clients->group_id)->first();
        if ($groupid) {
            $groupid = $groupid->main_group_id;
        }
        $house_number=explode(",",$clients->address_primary);
        $house_number=$house_number[0];


        // latest clients id generate
        $dataclients = DB::connection('main')->table('clients')->where('branch_id', $clients->branch_id)->orderBy('client_number', 'desc')->first();
        $main_client_generate_id = $dataclients->client_number;
        $main_client_generate_id = ++$main_client_generate_id;

        DB::connection('main')->table('clients')->insert([
            'branch_id' => $clients->branch_id,
            'name' => $clients->name,
            'name_other' => $clients->name_mm == null ? $clients->name : $clients->name_mm,
            // 'center_code' => $centerid, // change center
            // 'center_name' => $clients[$i]->branch_name, // change later
            'register_date' => date('Y-m-d', strtotime($clients->registration_date)),
            'client_number' => $main_client_generate_id,
            'customer_group_id' => $customer_group_id, //change group
            'gender' => $clients->gender,
            'dob' => date('Y-m-d', strtotime($clients->dob)),
            'education' => $clients->education_name,
            'primary_phone_number' => $clients->client_phone,
            'alternate_phone_number' => $clients->phone_secondary,
            'nrc_number' => $clients->nrc == null ? $clients->old_nrc : $clients->nrc,
            'nrc_type' => $clients->nrc == null ? 'Old Format' : 'New Format',
            'marital_status' => $clients->marital_status,
            'status' => 'Active',
            'father_name' => $clients->father_name,
            'husband_name' => $clients->spouse_name,
            'occupation_of_husband' => $clients->occupation_of_spouse,
            'no_children_in_family' => $clients->no_of_children == null ? 0 : $clients->no_of_children,
            'no_of_working_people' => $clients->no_of_working_family == null ? 0 : $clients->no_of_working_family,
            'no_of_dependent' => $clients->no_of_working_progeny == null ? 0 : $clients->no_of_working_progeny,
            'no_of_person_in_family' => $clients->no_of_family == null ? 0 : $clients->no_of_family,
            'address1' => $clients->address_primary,
            'address2' => $clients->address_secondary,
            'family_registration_copy' => $clients->registration_family_form_front,
            'photo_of_client' => $clients->client_photo,
            'nrc_photo' => $clients->recognition_front,
            'scan_finger_print' => $clients->finger_print_bio,
            'survey_id' => $clients->id,
            'remark' => 'active',
            'center_leader_id' => $centerid,
            // 'loan_officer_id'=>,
            'you_are_a_group_leader' => $clients->you_are_a_group_leader,
            'you_are_a_center_leader' => $clients->you_are_a_center_leader,
            //'ownership_of_farmland'=>$clients[$i]->land,
            //'ownership'=>$clients[$i]->ownership,
            'province_id' => $clients->division_id,
            'district_id' => $clients->district_id,
            'commune_id' => $clients->township_id,
            'village_id' => $clients->village_id,
            'ward_id' => $clients->quarter_id,
            'house_number'=> $house_number,
            'loan_officer_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $clients->staff_id)->first()->main_staff_id,
            'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $clients->staff_id)->first()->main_staff_id,
            'updated_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $clients->staff_id)->first()->main_staff_id,
            'created_at' => date('Y-m-d H:i:s', strtotime($clients->Ccreated_at)),
            'updated_at' => date('Y-m-d H:i:s', strtotime($clients->Cupdated_at)),
            'company_address1' => $clients->company_address,
        ]);
        $main_client_id = DB::connection('main')->table('clients')->where('client_number', $main_client_generate_id)->first()->id;
        $job = DB::table('tbl_client_job_main')->where('client_uniquekey', $clients->client_uniquekey)
            ->leftJoin('tbl_industry_type', 'tbl_industry_type.id', 'tbl_client_job_main.industry_id')
            ->leftJoin('tbl_job_position', 'tbl_job_position.id', 'tbl_client_job_main.job_position_id')
            ->leftJoin('tbl_job_department', 'tbl_job_department.id', 'tbl_client_job_main.department_id')
            ->select(
                'tbl_client_job_main.*',
                'tbl_industry_type.industry_name AS employment_industry',
                'tbl_job_position.job_position_name AS position',
                'tbl_job_department.department_name AS department'

            )->first();
        if ($job->job_status == 'Has Job') {
            $employment_status = 'Active';
        } else {
            $employment_status = 'Inactive';
        }
        if(DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id',$clients->guarantors_id)->first()){
            $main_guarantor_id=DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id',$clients->guarantors_id)->first()->main_guarantor_id;
            //insert client_guarantor into main
            DB::connection('main')->table('client_guarantor')->insert([
                'client_id'=>$main_client_id,
                'guarantor_id'=>$main_guarantor_id,
                'created_at' => date('Y-m-d H:i:s', strtotime($clients->Ccreated_at)),
                'updated_at' => date('Y-m-d H:i:s', strtotime($clients->Cupdated_at)),
            ]);
        }
        DB::connection('main')->table('employee_status')->insert([
            'client_id' => $main_client_id,
            'position' => $job->position,
            'employment_status' => $employment_status,
            'employment_industry' => strtolower($job->employment_industry),
            'senior_level' => null,
            'branch' => $clients->branch_name,
            'company_name' => $job->company_name,
            'department' => $job->department,
            'work_phone' => $job->company_phone,
            'work_phone2' => $job->company_phone,
            'working_experience' => $job->experience,
            'basic_salary' => $job->salary,
            'company_address' => $job->company_address

        ]);

        DB::table('tbl_main_join_client')->insert([
            'main_client_id' => $main_client_id,
            'portal_client_id' => $clients->client_uniquekey,
            'portal_branch_id' => $clients->branch_id,
            'main_client_code' => $main_client_generate_id
        ]);

        //live crawl end
    }
}
