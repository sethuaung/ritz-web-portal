<?php

namespace App\Http\Controllers\AccountDataEntery;

use App\Http\Controllers\Controller;
use App\Models\AccountDataEnteries\Account;
use App\Models\ClientDataEnteries\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class AccountController extends Controller
{
    public function index()
    {
        
        $accounts = DB::table('accounts')
                ->leftJoin('accounts_main','accounts_main.id','=','accounts.acc_main_id')
                ->select('accounts.*','accounts_main.acc_code','accounts_main.acc_name')
                ->Paginate(10);
        return view('account.index',compact('accounts'));
    }

    public function create()
    {
       	$this->permissionFilter("frd-accounts");

        $acc_code_name = DB::table('accounts_main')->select('acc_code','acc_name','id')->get();
        $branches = Branch::select('branch_name','id')->get();
        $frd_code = DB::table('accounts_frd')->select('frd_coa','acc_id','id')->get();
        return view('account.create',compact('branches','acc_code_name','frd_code'));
    }

    public function store(Request $request)
    {
       	$this->permissionFilter("frd-accounts");

        $validator = $request->validate([
            'branch_id' => ['required','string','max:255'],
            'acc_main_id' => ['required','string','max:255'],
            'type' => ['required','string','max:255'],
            'dr_cr' => ['required','string','max:255'],
            // 'acc_code_id' => ['required','string','max:255'],
            // 'acc_name_id' => ['required','string','max:255'],
            // 'sub_acc_code' => ['required','string','max:255'],
            // 'sub_acc_name' => ['required','string','max:255'],
            'status' => ['required','string','max:255'],
        ]);
        //dd($validator);
        if($validator){
            $account = new Account();
            $account->branch_id = $request->branch_id;
            $account->acc_main_id = $request->acc_main_id;
            $account->type = $request->type;
            $account->dr_cr = $request->dr_cr;
            // $account->acc_code_id = $request->acc_code_id;
            // $account->acc_name_id = $request->acc_name_id;
            $account->sub_acc_code = $request->sub_acc_code;
            $account->sub_acc_name = $request->sub_acc_name;
            $account->frd_coa_code_id = $request->frd_coa_code_id;
            $account->status = $request->status;
            $account->save();
            return redirect('accounts/')->with("successMsg",'New Account is Added in your data');
        }
        else{
            return redirect()->back()->withErrors($validator);
        }
    }

    public function edit($id)
    {
       	$this->permissionFilter("frd-accounts");

        $account = DB::table('accounts')
                ->leftJoin('accounts_frd','accounts_frd.id','accounts.frd_coa_code_id') 
                ->select('accounts.*','accounts_frd.acc_id','accounts_frd.frd_coa') 
                ->where('accounts.id','=',$id)->first();  
                // dd($account);    
        // $account_code_name = DB::table('accounts')
        //                 ->leftJoin('accounts_main','accounts_main.id','=','accounts.acc_main_id')
        //                 ->where('accounts.id','=',$id)
        //                 ->first();
        $acc_code_name = DB::table('accounts_main')->select('acc_code','acc_name','id')->get();
        $branches = Branch::select('branch_name','id')->get();
        $frd_code = DB::table('accounts_frd')->select('frd_coa','acc_id','id')->get();
        return view('account.edit',compact('branches','account','acc_code_name','frd_code'));
    }

    public function update(Request $request, $id)
    {
       	$this->permissionFilter("frd-accounts");
        $validator = $request->validate([
            'branch_id' => ['required','string','max:255'],
            'acc_main_id' => ['required','string','max:255'],
            'type' => ['required','string','max:255'],
            'dr_cr' => ['required','string','max:255'],
            // 'sub_acc_code' => ['required','string','max:255'],
            // 'sub_acc_name' => ['required','string','max:255'],
            'status' => ['required','string','max:255'],
        ]);
        //dd($validator);
        if($validator){
            $account = Account::find($id);

            $frd_code=DB::table('accounts_frd')->where('id',$request->frd_coa_code_id)->select('frd_coa')->first();
            // dd($frd_code);

            $account->branch_id = $request->branch_id;
            $account->acc_main_id = $request->acc_main_id;
            $account->type = $request->type;
            $account->dr_cr = $request->dr_cr;
            $account->sub_acc_code = $request->sub_acc_code;
            $account->sub_acc_name = $request->sub_acc_name;
            $account->frd_coa_code_id = $frd_code->frd_coa;
            $account->status = $request->status;
            $account->save();
            return redirect('accounts/')->with("successMsg",'Update Account is Added in your data');
        }
        else{
            return redirect()->back()->withErrors($validator);
        }
    }

    public function view($id)
    {
        $account = Account::find($id);
        $acc_code_name = DB::table('accounts')
            ->leftJoin('tbl_branches','tbl_branches.id','=','accounts.branch_id')
            ->leftJoin('accounts_main','accounts_main.id','=','accounts.acc_main_id')
            ->leftJoin('accounts_frd','accounts_frd.id','accounts.frd_coa_code_id')
            ->where('accounts.id','=',$id)
            ->first();
        return view('account.view',compact('account','acc_code_name'));
    }

    public function destroy($id)
    {
        $account = Account::find($id);
        $account->delete();
        return redirect('accounts/')->with("successMsg",'Existing Account is Deleted in your data');
    }

    public function getFrdCode(Request $request)
    {
        $frd_code = DB::table('accounts_frd')->where('acc_id',$request->acc_id)->pluck('frd_coa','id');
        return response()->json($frd_code);
    }

}