<?php

namespace App\Http\Controllers\Crawl;

use App\Http\Controllers\Controller;
use App\Models\Crawl\Crawl;
use App\Models\Crawl\Crawl_Center;
use App\Models\Crawl\Crawl_Group;
use App\Models\Crawl\Crawl_Guarantors;
use App\Models\Crawl\Crawl_Staff;
use App\Models\Crawl\CrawlCounter;
use App\Models\Crawl\Staff;
use App\Models\MainJoin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class DataEntryCrawlController extends Controller
{
    //staff start -------------------------------
    public function generateStaffUniquekey($branchid)
    {
        if (DB::table('tbl_branches')->where('id', $branchid)->first()) {
            $branch = DB::table('tbl_branches')
                ->where('id', $branchid)
                ->first()->id;
        } else {
            $branch = 1;
        }
        $idnum = DB::table('tbl_staff')
            ->where('branch_id', $branch)
            ->get()
            ->count();
        $idnum = $idnum + 1;
        $idnum = str_pad($idnum, 4, 0, STR_PAD_LEFT);

        if (DB::connection('portal')->table('tbl_branches')->where('id', $branchid)->first()) {
            $branchcode = DB::connection('portal')
                ->table('tbl_branches')
                ->where('id', $branchid)
                ->first()->branch_code;
        } else {
            $branchcode = DB::connection('portal')
                ->table('tbl_branches')
                ->where('id', 1)
                ->first()->branch_code;
        }

        $uniqueid = $branchcode;
        $uniqueid .= '-';
        $uniqueid .= $idnum;

        return $uniqueid;
    }

    public function staffIndex()
    {
        $crawl_staffs = Crawl::where('module_name', 'staff')->paginate(10);
        return view('crawling.crawl.staff_crawl.index', compact('crawl_staffs'));
    }

    public function crawlStaffs()
    {
        $branchcount = DB::table('tbl_branches')->count();

        // return response()->json($staffs = DB::connection('main')
        // ->table('user_branches')
        // ->leftJoin('users', 'user_branches.user_id', '=', 'users.id')
        // // ->join('user_branches', function ($join) {
        // //     $join->on('users.id', '=', 'user_branches.user_id')->orWhereNull('user_branches.user_id');
        // // })
        // ->select('users.*', 'users.id as userid', 'user_branches.branch_id as branchid')
        // ->where('user_branches.branch_id', '!=', NULL)
        // ->get());
        $sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;

        $z = 0;
        for ($y = $sessionBranch; $y <= $sessionBranch; $y++) {
            $staffs = DB::connection('main')
                ->table('users')
                ->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
                ->leftJoin('user_branches', 'user_branches.user_id', '=', 'users.id')
                // ->leftJoin('model_has_permissions', 'model_has_permissions.model_id', '=', 'users.id')
                ->select('users.*', 'model_has_roles.*', 'user_branches.user_id as userid', 'user_branches.branch_id as branchid')
                ->get();

            // return response()->json($staffs);

            $userBranches = DB::connection('main')->table('user_branches')->get();
            DB::table('user_branches')->truncate();
            foreach ($userBranches as $userBranch) {
                if (DB::table('tbl_main_join_staff')->where('main_staff_id', $userBranch->user_id)->first()) {
                    $mainJoinInfo = DB::table('tbl_main_join_staff')->where('main_staff_id', $userBranch->user_id)->first();

                    // if(DB::table('user_branches')->where('user_id', $mainJoinInfo->portal_staff_id)->first()) {
                    //     continue;
                    // }

                    DB::table('user_branches')->insert([
                        'user_id' => $mainJoinInfo->portal_staff_id,
                        'branch_id' => $userBranch->branch_id
                    ]);
                }
            }

            $modelHasPermissions = DB::connection('main')->table('model_has_permissions')->get();
            DB::table('model_has_permissions')->truncate();
            foreach ($modelHasPermissions as $modelHasPermission) {
                if (DB::table('tbl_main_join_staff')->where('main_staff_id', $modelHasPermission->model_id)->first()) {
                    $mainJoinInfo = DB::table('tbl_main_join_staff')->where('main_staff_id', $modelHasPermission->model_id)->first();

                    // if(DB::table('model_has_permissions')->where('model_id', $mainJoinInfo->portal_staff_id)->first()) {
                    //     continue;
                    // }

                    DB::table('model_has_permissions')->insert([
                        'model_id' => $mainJoinInfo->portal_staff_id,
                        'model_type' => $modelHasPermission->model_type,
                        'permission_id' => $modelHasPermission->permission_id
                    ]);
                }
            }

            foreach ($staffs as $staff) {
                if (DB::table('tbl_main_join_staff')->where('main_staff_id', $staff->id)->first()) {
                    $mainJoinInfo = DB::table('tbl_main_join_staff')->where('main_staff_id', $staff->id)->first();
                    if ((DB::table('tbl_staff')->where('staff_code', $mainJoinInfo->portal_staff_id)->first()->updated_at != $staff->updated_at) || (DB::table('tbl_staff')->where('staff_code', $mainJoinInfo->portal_staff_id)->first()->role_id != $staff->role_id)) {
                        if ($staff->role_id == null) {
                            DB::table('model_has_roles')->where('model_id', $mainJoinInfo->portal_staff_id)->delete();
                        } else {
                            DB::table('model_has_roles')->where('model_id', $mainJoinInfo->portal_staff_id)->update([
                                'role_id' => $staff->role_id,
                                'model_type' => $staff->model_type,
                            ]);
                        }

                        DB::table('tbl_staff')->where('staff_code', $mainJoinInfo->portal_staff_id)->update([
                            'name' => $staff->name,
                            'phone' => $staff->phone,
                            'email' => $staff->email,
                            'email_verified_at' => $staff->email_verified_at == null ? now() : $staff->email_verified_at,
                            'user_acc' => $staff->name,
                            'user_password' => $staff->password,
                            'photo' => $staff->photo,
                            'branch_id' => $staff->branchid == null ? 1 : $staff->branchid,
                            'center_leader_id' => $staff->center_leader_id,
                            'role_id' => $staff->role_id,
                            'user_token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI1IiwianRpIjoiODU4OWQ0MzFmYmRiZjliYzJjOGNkMzVlY2Q1Yzc5ZTdlNmIyMzQ3NDEzODA2OTQ1OGYyZWI3ZjUxY2M3ZDRhOGU0Y2EzNTViYTJjY2I5MWEiLCJpYXQiOjE2NjcyMDgwNjAsIm5iZiI6MTY2NzIwODA2MCwiZXhwIjoxNjk4NzQ0MDYwLCJzdWIiOiIzMyIsInNjb3BlcyI6W119.PhK5uxfM_xfUw1F5pDgtdwj41OrIqudhhE6JJkKWrBcZlZGVnZV1KYC7IsK2StlllrnCZZciG9Uom0UDfuJ58LmbzmZ3rRcr0Yk0cOZO7CF-MoJ96WGeMsg3z46MYYLIcGDXf483P5xjzgm4uJsIkhlXSOWpTQO7ajT3dVwWm5aqR1tx3meyJXr7tdW2QWvaYzfxVIPOzTTSDgxy5AcUHPJdkmAT3hnNGR8Lb77ziVxcCXlnO35L3vhsn_sP7ijobz9V2I4ektLgQlSeTNGx-P-8B5UEi-TIjVa8-rNXDMGLr1mDWQQsTcBoLZQNGWS-kzmRNcdgGzu4WSPooU0rTbFp0d4hKQL9bvTQm0DyyKyVU97n3fJIrL-VUBNRwTcrQc9DbpO74StQhJFTsfieUjM8aGkN-H-iyy6yIREQrxmqY3xvukDdQ2Z4Y79F24n8i26vWJsLnLC8hiB2C1vOezogaivOAQ7I-jqk0j358RxsB627jLwG6o6cnjJHIu55IIlj3BMWiQzoGlSDSqriYXifg9JtSKbDhg6smjPiQ_Io9kRAA9s8H9tFUsF5RGJQ7RFXBNkUCoQ84STDS0Tem2og-hK2aq8iCISSVZ3WgWHbPB8Sb_Io2q5_zSiO90WhbDcLS134im5hIF_RCFGjV7rnwTN-iaUzHOUzLTgZl1c',
                            'del_status' => 'none',
                            'created_at' => $staff->created_at,
                            'updated_at' => $staff->updated_at,
                        ]);

                        $z = ++$z;
                    }



                    continue;
                }

                $crawlstaff = new Crawl_Staff();
                $crawlstaff->portal_staff_id = $this->generateStaffUniquekey($staff->branchid);
                $crawlstaff->main_staff_id = $staff->id;
                $crawlstaff->main_staff_code = $staff->user_code;
                $crawlstaff->main_staff_email = $staff->email;
                $crawlstaff->save();

                DB::table('model_has_roles')->insert([
                    'role_id' => $staff->role_id,
                    'model_type' => $staff->model_type,
                    'model_id' => $crawlstaff->portal_staff_id
                ]);

                DB::table('tbl_staff')->insert([
                    'staff_code' => $crawlstaff->portal_staff_id,
                    'name' => $staff->name,
                    'phone' => $staff->phone,
                    'email' => $staff->email,
                    'email_verified_at' => $staff->email_verified_at == null ? now() : $staff->email_verified_at,
                    'user_acc' => $staff->name,
                    'user_password' => $staff->password,
                    'photo' => $staff->photo,
                    'branch_id' => $staff->branchid == null ? 1 : $staff->branchid,
                    'center_leader_id' => $staff->center_leader_id,
                    'role_id' => $staff->role_id,
                    'user_token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI1IiwianRpIjoiODU4OWQ0MzFmYmRiZjliYzJjOGNkMzVlY2Q1Yzc5ZTdlNmIyMzQ3NDEzODA2OTQ1OGYyZWI3ZjUxY2M3ZDRhOGU0Y2EzNTViYTJjY2I5MWEiLCJpYXQiOjE2NjcyMDgwNjAsIm5iZiI6MTY2NzIwODA2MCwiZXhwIjoxNjk4NzQ0MDYwLCJzdWIiOiIzMyIsInNjb3BlcyI6W119.PhK5uxfM_xfUw1F5pDgtdwj41OrIqudhhE6JJkKWrBcZlZGVnZV1KYC7IsK2StlllrnCZZciG9Uom0UDfuJ58LmbzmZ3rRcr0Yk0cOZO7CF-MoJ96WGeMsg3z46MYYLIcGDXf483P5xjzgm4uJsIkhlXSOWpTQO7ajT3dVwWm5aqR1tx3meyJXr7tdW2QWvaYzfxVIPOzTTSDgxy5AcUHPJdkmAT3hnNGR8Lb77ziVxcCXlnO35L3vhsn_sP7ijobz9V2I4ektLgQlSeTNGx-P-8B5UEi-TIjVa8-rNXDMGLr1mDWQQsTcBoLZQNGWS-kzmRNcdgGzu4WSPooU0rTbFp0d4hKQL9bvTQm0DyyKyVU97n3fJIrL-VUBNRwTcrQc9DbpO74StQhJFTsfieUjM8aGkN-H-iyy6yIREQrxmqY3xvukDdQ2Z4Y79F24n8i26vWJsLnLC8hiB2C1vOezogaivOAQ7I-jqk0j358RxsB627jLwG6o6cnjJHIu55IIlj3BMWiQzoGlSDSqriYXifg9JtSKbDhg6smjPiQ_Io9kRAA9s8H9tFUsF5RGJQ7RFXBNkUCoQ84STDS0Tem2og-hK2aq8iCISSVZ3WgWHbPB8Sb_Io2q5_zSiO90WhbDcLS134im5hIF_RCFGjV7rnwTN-iaUzHOUzLTgZl1c',
                    'del_status' => 'none',
                    'created_at' => $staff->created_at,
                    'updated_at' => $staff->updated_at,
                ]);

                $z = ++$z;
            }

            $userBranches = DB::connection('main')->table('user_branches')->get();
            foreach ($userBranches as $userBranch) {
                if (DB::table('tbl_main_join_staff')->where('main_staff_id', $userBranch->user_id)->first()) {
                    $mainJoinInfo = DB::table('tbl_main_join_staff')->where('main_staff_id', $userBranch->user_id)->first();

                    // if(DB::table('user_branches')->where('user_id', $mainJoinInfo->portal_staff_id)->first()) {
                    //     continue;
                    // }

                    DB::table('user_branches')->insert([
                        'user_id' => $mainJoinInfo->portal_staff_id,
                        'branch_id' => $userBranch->branch_id
                    ]);
                }
            }

            $modelHasPermissions = DB::connection('main')->table('model_has_permissions')->get();
            foreach ($modelHasPermissions as $modelHasPermission) {
                if (DB::table('tbl_main_join_staff')->where('main_staff_id', $modelHasPermission->model_id)->first()) {
                    $mainJoinInfo = DB::table('tbl_main_join_staff')->where('main_staff_id', $modelHasPermission->model_id)->first();

                    // if(DB::table('model_has_permissions')->where('model_id', $mainJoinInfo->portal_staff_id)->first()) {
                    //     continue;
                    // }

                    DB::table('model_has_permissions')->insert([
                        'model_id' => $mainJoinInfo->portal_staff_id,
                        'model_type' => $modelHasPermission->model_type,
                        'permission_id' => $modelHasPermission->permission_id
                    ]);
                }
            }
        }

        // $entrey = DB::connection('portal')
        //     ->table('tbl_staff')
        //     ->get();

        DB::connection('portal')
            ->table('tbl_crawling')
            ->where('module_name', 'staff')
            ->update([
                'data_count' => $z,
                'crawl_date' => date('Y-m-d H:i:s'),
                //'crawl_date' => date('d-M-Y', Carbon::now())
            ]);

        if ($z) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $z]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //staff end -------------------------------

    //center start -------------------------------
    public function getCenterUniquekey($branchid)
    {
        $branch = DB::table('tbl_branches')
            ->where('id', $branchid)
            ->first()->id;
        $idnum = DB::table('tbl_center')
            ->where('branch_id', $branch)
            ->get()
            ->count();
        $idnum = $idnum + 1;
        $idnum = str_pad($idnum, 4, 0, STR_PAD_LEFT);

        $branchcode = DB::connection('portal')
            ->table('tbl_branches')
            ->where('id', $branchid)
            ->first()->branch_code;
        $uniqueid = $branchcode;
        $uniqueid .= '-05-';
        $uniqueid .= $idnum;

        return $uniqueid;
    }

    public function centerIndex()
    {
        $crawl_centers = Crawl::where('module_name', 'center')->Paginate(10);
        return view('crawling.crawl.center_crawl.index', compact('crawl_centers'));
    }

    public function getPortalStaffId($mainid)
    {
        $centerid = DB::table('tbl_main_join_staff')
            ->where('main_staff_id', $mainid)
            ->first();
        if ($centerid) {
            $centerid = $centerid->portal_staff_id;
        } else {
            $centerid = '';
        }
        return $centerid;
    }

    public function crawlCenters()
    {
        $branchcount = DB::table('tbl_branches')->count();
        $sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;

        $z = 0;
        for ($y = $sessionBranch; $y <= $sessionBranch; $y++) {
            //$centers = DB::connection('main')->table('center_leaders')->get();
            $centers = DB::connection('main')
                ->table('center_leaders')
                ->join('users', function ($join) {
                    $join->on('center_leaders.user_id', '=', 'users.id');
                    // ->orWhereNull('center_leaders.user_id')
                    // ->orWhereNull('center_leaders.branch_id');
                })
                ->join('user_centers', function ($join) {
                    $join->on('user_centers.center_id', '=', 'center_leaders.id');
                })
                ->where('center_leaders.branch_id', $y)
                ->select('center_leaders.*', 'user_centers.user_id as staff_id')
                ->get();
            //dd($this->getPortalStaffId('897'));
            // return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $centers]);
            $filterCenters = [];

            for ($i = 0; $i < count($centers); $i++) {
                if (
                    DB::table('tbl_main_join_center')
                    ->where('main_center_code', $centers[$i]->code)
                    ->first()
                ) {
                    continue;
                }
                $staffs = DB::table('tbl_main_join_staff')->get();

                foreach ($staffs as $s) {
                    if ($s->main_staff_id == $centers[$i]->staff_id) {
                        array_push($filterCenters, $centers[$i]);
                    }
                }
            }
            // return response()->json($filterCenters);
            foreach ($filterCenters as $fc) {
                if ($fc->branch_id != null or $fc->staff_id != null) {
                    $portalStaffID = $this->getPortalStaffId($fc->staff_id);

                    if ($portalStaffID == '') {
                        continue;
                    }

                    $mainJoin = new Crawl_Center();
                    $mainJoin->portal_center_id = $this->getCenterUniquekey($fc->branch_id);
                    $mainJoin->main_center_id = $fc->id;
                    $mainJoin->main_center_code = $fc->code;
                    // $mainJoin->main_center_leader_id = $fc->user_id;

                    $mainJoin->save();

                    DB::connection('portal')
                        ->table('tbl_center')
                        ->insert([
                            // 'staff_client_id' => $this->getPortalStaffId($fc->user_id),
                            'center_uniquekey' => $mainJoin->portal_center_id,
                            'staff_id' => $portalStaffID,
                            'branch_id' => $fc->branch_id,
                            // 'type_status' => 'client'
                            'village_id' => $fc->village_id,
                            'township_id' => $fc->commune_id,
                            'quarter_id' => NULL,
                            'district_id' => $fc->district_id,
                            'division_id' => $fc->province_id,
                            'address' => $fc->address,
                            'phone_number' => $fc->phone,
                        ]);


                    $z = ++$z;
                }
            }
        }

        $verify = DB::connection('portal')
            ->table('tbl_center')
            ->get();
        DB::connection('portal')
            ->table('tbl_crawling')
            ->where('module_name', 'center')
            ->update([
                'data_count' => $z,
                'crawl_date' => date('Y-m-d H:i:s'),
            ]);

        if ($z) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $z]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //center end -------------------------------

    //group start -------------------------------
    public function generateGroupUniquekey($branchid)
    {
        $branch = DB::table('tbl_branches')
            ->where('id', $branchid)
            ->first()->id;
        $idnum = DB::table('tbl_group')
            ->where('branch_id', $branch)
            ->get()
            ->count();
        $idnum = $idnum + 1;
        $idnum = str_pad($idnum, 5, 0, STR_PAD_LEFT);

        $branchcode = DB::connection('portal')
            ->table('tbl_branches')
            ->where('id', $branchid)
            ->first()->branch_code;
        $uniqueid = $branchcode;
        $uniqueid .= '-06-';
        $uniqueid .= $idnum;

        return $uniqueid;
    }

    public function groupIndex()
    {
        $crawl_groups = Crawl::where('module_name', 'group')->paginate(10);
        return view('crawling.crawl.group_crawl.index', compact('crawl_groups'));
    }

    public function crawlGroups()
    {
        $branchcount = DB::table('tbl_branches')->count();
        $sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;

        $z = 0;
        for ($y = $sessionBranch; $y <= $sessionBranch; $y++) {
            $new_groups = DB::connection('main')
                ->table('group_loans')
                ->where('branch_id', $y)
                ->get();

            // return response()->json($new_groups);

            for ($i = 0; $i < count($new_groups); $i++) {
                $check = DB::table('tbl_main_join_group')
                    ->where('main_group_id', $new_groups[$i]->id)
                    ->first();
                // return response()->json($check);
                if ($check) {
                    continue;
                }

                $centerid = DB::table('tbl_main_join_center')
                    ->where('main_center_id', $new_groups[$i]->center_id)
                    ->first();
                if ($centerid) {
                    $centerid = $centerid->portal_center_id;
                } else {
                    continue;
                }

                $mainJoin = new Crawl_Group();
                $mainJoin->portal_group_id = $this->generateGroupUniquekey($new_groups[$i]->branch_id);
                $mainJoin->main_group_id = $new_groups[$i]->id;
                $mainJoin->main_group_code = $new_groups[$i]->group_code;
                $mainJoin->save();

                // $portal_group_id=DB::table('tbl_main_join_group')->where('main_group_id',$new_groups[$i]->id)->first()->portal_group_id;
                // return response()->json($portal_group_id);

                // $check2 = DB::table('tbl_group')->where('group_uniquekey', $mainJoin->portal_group_id)->first();
                // // return response()->json($check);
                // if ($check2) {
                // continue;
                // }
                DB::connection('portal')
                    ->table('tbl_group')
                    ->insert([
                        'group_uniquekey' => $mainJoin->portal_group_id,
                        // 'staff_id' => $staffid,
                        'client_id' => null,
                        'branch_id' => $new_groups[$i]->branch_id,
                        'level_status' => null,
                        'del_status' => 'none',
                        'center_id' => $centerid,
                    ]);

                $z = ++$z;
            }
        }

        // Save crawl counter
        $entrey = DB::connection('portal')
            ->table('tbl_group')
            ->get();
        DB::connection('portal')
            ->table('tbl_crawling')
            ->where('module_name', 'group')
            ->update([
                'data_count' => $z,
                'crawl_date' => date('Y-m-d H:i:s'),
            ]);
        if ($z) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $z]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //group end -------------------------------

    //guarantor start -------------------------------
    public function generateGuarantorUniquekey($branchid)
    {
        $branch = DB::table('tbl_branches')
            ->where('id', $branchid)
            ->first()->id;
        $idnum = DB::table('tbl_guarantors')
            ->where('branch_id', $branch)
            ->get()
            ->count();
        $idnum = $idnum + 1;
        $idnum = str_pad($idnum, 7, 0, STR_PAD_LEFT);

        $branchcode = DB::connection('portal')
            ->table('tbl_branches')
            ->where('id', $branchid)
            ->first()->branch_code;
        $uniqueid = $branchcode;
        $uniqueid .= '-02-';
        $uniqueid .= $idnum;

        return $uniqueid;
    }

    public function guarantorIndex()
    {
        $crawl_guarantors = Crawl::where('module_name', 'guarantor')->paginate(10);
        return view('crawling.crawl.guarantor_crawl.index', compact('crawl_guarantors'));
    }

    public function crawlGuarantors()
    {
        $guarantors = DB::connection('main')
            ->table('guarantors')
            ->where('branch_id', (DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id))
            ->get();
        if (count($guarantors) == 0) {
            return response()->json(['status_code' => 200, 'message' => 'guarantors not found in core system', 'data' => 0]);
        }

        $z = 0;
        //insert data to portal's table
        for ($i = 0; $i < count($guarantors); $i++) {
            if (
                DB::connection('portal')
                ->table('tbl_main_join_guarantor')
                ->where('main_guarantor_id', $guarantors[$i]->id)
                ->first()
            ) {
                continue;
            }

            $mainJoin = new Crawl_Guarantors();
            $mainJoin->portal_guarantor_id = $this->generateGuarantorUniquekey($guarantors[$i]->branch_id == null ? 1 : $guarantors[$i]->branch_id);
            $mainJoin->main_guarantor_id = $guarantors[$i]->id;
            $mainJoin->main_guarantor_code = '';
            $mainJoin->save();

            DB::connection('portal')
                ->table('tbl_guarantors')
                ->insert([
                    'branch_id' => $guarantors[$i]->branch_id,
                    'guarantor_uniquekey' => $mainJoin->portal_guarantor_id,
                    'name' => $guarantors[$i]->full_name_en,
                    'dob' => $guarantors[$i]->dob,
                    'nrc' => $guarantors[$i]->nrc_number,
                    'gender' => NULL,
                    'phone_primary' => $guarantors[$i]->mobile,
                    'email' => $guarantors[$i]->email == null ? 'N/A' : $guarantors[$i]->email,
                    //'blood_type' => $guarantors[$i]->blood_type,
                    'religion' => '',
                    'nationality' => 'Myanmar', //$guarantors[$i]->country_id,
                    //'other_education' => '',
                    'income' => $guarantors[$i]->income,
                    'village_id' => $guarantors[$i]->village_id == null ? '' : $guarantors[$i]->village_id,
                    'quarter_id' => $guarantors[$i]->ward_id == null ? '' : $guarantors[$i]->ward_id,
                    'township_id' => $guarantors[$i]->commune_id == null ? '' : $guarantors[$i]->commune_id,
                    'district_id' => $guarantors[$i]->district_id == null ? '' : $guarantors[$i]->district_id,
                    'division_id' => $guarantors[$i]->province_id == null ? '' : $guarantors[$i]->province_id,
                    'address_primary' => $guarantors[$i]->address,
                    //'address_secondary',
                    'guarantor_nrc_front_photo' => '',
                    'guarantor_nrc_back_photo' => '',
                    'guarantor_photo' => '',
                    'kyc_photo' => '',
                    //'current_job' => ,
                    //'nrc_card_id',
                    //'old_nrc',
                    'name_mm' => $guarantors[$i]->full_name_mm,
                    //'education_id'
                ]);

            $z = ++$z;
        }

        // Save crawl counter
        $entrey = DB::connection('portal')
            ->table('tbl_guarantors')
            ->get();
        DB::connection('portal')
            ->table('tbl_crawling')
            ->where('module_name', 'guarantor')
            ->update([
                'data_count' => $z,
                'crawl_date' => date('Y-m-d H:i:s'),
            ]);
        if ($z) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $z]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //guarantor end -------------------------------

    public function crawlBranches()
    {
        $branches = DB::connection('main')
            ->table('branches')
            ->get();

        foreach ($branches as $branch) {
            DB::table('tbl_branches')->insert([
                'branch_name' => $branch->title,
                'branch_code' => $branch->code,
                'branch_photo' => '123.jpg',
                'client_prefix' => $branch->client_prefix,
                'title' => $branch->title,
                'phone_primary' => $branch->phone,
                // 'village_id' => $branch->village_id == NULL ? DB::connection('main')->table('addresses')->where('code', $branch->village_id)->first()->id : 0,
                'village_id' => $branch->village_id == null ? '' : $branch->village_id,
                'division_id' => $branch->province_id == null ? '' : $branch->province_id,
                'township_id' => $branch->commune_id == null ? '' : $branch->commune_id,
                'district_id' => $branch->district_id == null ? '' : $branch->district_id,
                'division_id' => $branch->province_id == null ? '' : $branch->province_id,
                'full_address' => $branch->address1,
            ]);
            return response()->json('success');
        }
    }
}
