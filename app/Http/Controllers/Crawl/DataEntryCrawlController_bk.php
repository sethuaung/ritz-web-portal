<?php

namespace App\Http\Controllers\Crawl;

use App\Http\Controllers\Controller;
use App\Models\Crawl\Crawl;
use App\Models\Crawl\Crawl_Center;
use App\Models\Crawl\Crawl_Group;
use App\Models\Crawl\Crawl_Guarantors;
use App\Models\Crawl\Crawl_Staff;
use App\Models\Crawl\CrawlCounter;
use App\Models\Crawl\Staff;
use App\Models\MainJoin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class DataEntryCrawlController extends Controller
{

    //staff start -------------------------------
    public function generateStaffUniquekey($branchid)
    {
        $branch = DB::table('tbl_branches')->where('id', $branchid)->first()->id;
        $idnum = DB::table('tbl_staff')->where('branch_id', $branch)->get()->count();
        $idnum = $idnum + 1;
        $idnum = str_pad($idnum, 4, 0, STR_PAD_LEFT);

        $branchcode = DB::connection('portal')->table('tbl_branches')->where('id', $branchid)->first()->branch_code;
        $uniqueid = $branchcode;
        $uniqueid .= "-";
        $uniqueid .= $idnum;

        return $uniqueid;
    }

    public function staffIndex()
    {
        $crawl_staffs =  Crawl::where('module_name', 'staff')->paginate(10);
        return view('crawling.crawl.staff_crawl.index', compact('crawl_staffs'));
    }

    public function crawlStaffs()
    {
        $staffs =DB::connection('main')->table('users')
            ->join('user_branches', function($join){
                $join->on('users.id', '=', 'user_branches.user_id')
                ->orWhereNull('user_branches.user_id');
            })
        ->select('users.*', 'users.id as user_id', 'user_branches.branch_id as branchid') 
        ->get();

        for($i = 0; $i < count($staffs); $i++) {

            //dd($staffs);
            if($staffs[$i]->branchid != null) {
                if(DB::connection('portal')->table('tbl_main_join_staff')->where('main_staff_id', $staffs[$i]->user_id)->first()) {
                    continue;
                }

                $crawlstaff = new Crawl_Staff();
                $crawlstaff->portal_staff_id = $this->generateStaffUniquekey($staffs[$i]->branchid);
                $crawlstaff->main_staff_id = $staffs[$i]->user_id;
                $crawlstaff->save();

                $data_staffs = DB::connection('portal')->table('tbl_staff')->insert([
                    'name' => $staffs[$i]->name,
                    'staff_code' => $crawlstaff->portal_staff_id,
                    'phone' => $staffs[$i]->phone,
                    'father_name' => '',
                    'nrc' => '',
                    'full_address' => '',
                    'email' => $staffs[$i]->email,
                    'user_acc' => $staffs[$i]->name,
                    'user_password' => $staffs[$i]->password,
                    'photo' => $staffs[$i]->photo,
                    'branch_id' => $staffs[$i]->branchid,
                    'center_leader_id' => $staffs[$i]->id,
                    'user_token' => 'smth'
                ]);
            }
        }

        $entrey = DB::connection('portal')->table('tbl_staff')->get();

        DB::connection('portal')->table('tbl_crawling')->where('module_name', 'staff')->update([
            'data_count' => count($entrey),
            'crawl_date' => date('Y-m-d H:i:s')
            //'crawl_date' => date('d-M-Y', Carbon::now())
        ]);

        if ($entrey) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => count($entrey)]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //staff end -------------------------------

    //center start -------------------------------
    public function getCenterUniquekey($branchid)
    {
        $branch = DB::table('tbl_branches')->where('id', $branchid)->first()->id;
        $idnum = DB::table('tbl_center')->where('branch_id', $branch)->get()->count();
        $idnum = $idnum + 1;
        $idnum = str_pad($idnum, 4, 0, STR_PAD_LEFT);

        $branchcode = DB::connection('portal')->table('tbl_branches')->where('id', $branchid)->first()->branch_code;
        $uniqueid = $branchcode;
        $uniqueid .= "-05-";
        $uniqueid .= $idnum;

        return $uniqueid;
    }

    public function centerIndex()
    {
        $crawl_centers= Crawl::where('module_name', 'center')->Paginate(10);
        return view('crawling.crawl.center_crawl.index', compact('crawl_centers'));
    }

    public function getPortalStaffId($mainid)
    {
        $centerid = DB::table('tbl_main_join_staff')->where('main_staff_id', $mainid)->first();
        if($centerid){
            $centerid =  $centerid->portal_staff_id;
        }else{
            $centerid =  '';
        }
        return $centerid;
    }

    public function crawlCenters()
    {
        //$centers = DB::connection('main')->table('center_leaders')->get();
        $centers =DB::connection('main')->table('center_leaders')
            ->join('users', function($join){
                $join->on('center_leaders.user_id', '=', 'users.id')
                    ->orWhereNull('center_leaders.user_id')
                    ->orWhereNull('center_leaders.branch_id');
                })
            ->where('center_leaders.branch_id','1')
            ->select('center_leaders.*') 
            ->get();
        //dd($this->getPortalStaffId('897'));    
        //return response()->json(['status_code' => 200, 'message' => 'success', 'data' => count($centers)]);

        for($i = 1; $i < count($centers); $i++) {
            if(($centers[$i]->branch_id != null) or ($centers[$i]->user_id != null)){

                $mainJoin = new Crawl_Center();
                $mainJoin->portal_center_id = $this->getCenterUniquekey($centers[$i]->branch_id);
                $mainJoin->main_center_id = $centers[$i]->id;
                $mainJoin->main_center_code = $centers[$i]->code;
                
                $mainJoin->save();

                DB::connection('portal')->table('tbl_center')->insert([
                    'staff_client_id' => $this->getPortalStaffId($centers[$i]->user_id),
                    'center_uniquekey' => $mainJoin->portal_center_id,
                    'staff_id' => $this->getPortalStaffId($centers[$i]->user_id),
                    'branch_id' => $centers[$i]->branch_id,
                    'type_status' => 'staff',
                    'village_id'=>$centers[$i]->village_id,
                    'township_id'=>$centers[$i]->commune_id,
                    'quarter_id'=>NULL,
                    'district_id'=>$centers[$i]->district_id,
                    'division_id'=>$centers[$i]->province_id,
                    'address'=>$centers[$i]->address,
                    'phone_number'=>$centers[$i]->phone,
                ]);

            }
        }

        $verify = DB::connection('portal')->table('tbl_center')->get();
        DB::connection('portal')->table('tbl_crawling')->where('module_name', 'center')->update([
            'data_count' => count($verify),
            'crawl_date' => date('Y-m-d H:i:s')
        ]);

        if ($verify) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => count($verify)]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //center end -------------------------------

    //group start -------------------------------
    public function generateGroupUniquekey($branchid)
    {
        $branch = DB::table('tbl_branches')->where('id', $branchid)->first()->id;
        $idnum = DB::table('tbl_group')->where('branch_id', $branch)->get()->count();
        $idnum = $idnum + 1;
        $idnum = str_pad($idnum, 5, 0, STR_PAD_LEFT);

        $branchcode = DB::connection('portal')->table('tbl_branches')->where('id', $branchid)->first()->branch_code;
        $uniqueid = $branchcode;
        $uniqueid .= "-06-";
        $uniqueid .= $idnum;

        return $uniqueid;
    }

    public function groupIndex()
    {
        $crawl_groups =  Crawl::where('module_name', 'group')->paginate(10);
        return view('crawling.crawl.group_crawl.index', compact('crawl_groups'));
    }

    public function crawlGroups()
    {
        $groups =DB::connection('main')->table('group_loans')
            ->join('center_leaders', function($join){
                $join->on('group_loans.center_id', '=', 'center_leaders.id')
                    ->orWhereNull('center_leaders.id');
                })
            ->select('group_loans.*') 
            ->where('group_loans.center_id','1')
            ->get();
        //return response()->json(['status_code' => 200, 'message' => 'success', 'data' => count($groups)]);
        //insert data to portal's table
        for($i = 0; $i < count($groups); $i++) {

                $mainJoin = new Crawl_Group();
                $mainJoin->portal_group_id = $this->generateGroupUniquekey($groups[$i]->branch_id);
                $mainJoin->main_group_id = $groups[$i]->id;
                $mainJoin->main_group_code = $groups[$i]->group_code;
                $mainJoin->save();

                // Staff Check
                $staffid = DB::table('tbl_staff')->where('branch_id', $groups[$i]->branch_id)->first();
                if( $staffid){
                    $staffid = $staffid->staff_code;
                }else{
                    $staffid = '';
                }
                // Center Check
                $centerid = DB::table('tbl_main_join_center')->where('main_center_id', $groups[$i]->center_id)->first();
                if( $centerid){
                    $centerid = $centerid->portal_center_id;
                }else{
                    $centerid = '';
                }
                
                DB::connection('portal')->table('tbl_group')->insert([
                    'group_uniquekey' => $mainJoin->portal_group_id,
                    'staff_id' => $staffid,
                    'branch_id' => $groups[$i]->branch_id,
                    'level_status' => 'leader',
                    'del_status' => 'none',
                    'center_id' => $centerid,
                ]);
        }

        // Save crawl counter
        $entrey = DB::connection('portal')->table('tbl_group')->get();
        DB::connection('portal')->table('tbl_crawling')->where('module_name', 'group')->update([
            'data_count' => count($entrey),
            'crawl_date' => date('Y-m-d H:i:s')
        ]);
        if ($entrey) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => count($entrey)]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //group end -------------------------------

    //guarantor start -------------------------------
    public function generateGuarantorUniquekey($branchid)
    {
        $branch = DB::table('tbl_branches')->where('id', $branchid)->first()->id;
        $idnum = DB::table('tbl_guarantors')->where('branch_id', $branch)->get()->count();
        $idnum = $idnum + 1;
        $idnum = str_pad($idnum, 7, 0, STR_PAD_LEFT);

        $branchcode = DB::connection('portal')->table('tbl_branches')->where('id', $branchid)->first()->branch_code;
        $uniqueid = $branchcode;
        $uniqueid .= "-02-";
        $uniqueid .= $idnum;

        return $uniqueid;
    }

    public function guarantorIndex()
    {
        $crawl_guarantors =  Crawl::where('module_name', 'guarantor')->paginate(10);
        return view('crawling.crawl.guarantor_crawl.index', compact('crawl_guarantors'));
    }

    public function crawlGuarantors()
    {
        $guarantors =DB::connection('main')->table('guarantors')->get();
        if (count($guarantors) == 0) {
            return response()->json(['status_code' => 400, 'message' => 'guarantors not found in core system', 'data' => 0]);
        }

        //insert data to portal's table
        for($i = 0; $i < count($guarantors); $i++) {
            if(DB::connection('portal')->table('tbl_main_join_guarantor')->where('main_guarantor_id', $guarantors[$i]->id)->first()) {
                continue;
            }

            $mainJoin = new Crawl_Guarantors();
            $mainJoin->portal_guarantor_id = $this->generateGuarantorUniquekey($guarantors[$i]->branch_id == null ? 1 : $guarantors[$i]->branch_id);
            $mainJoin->main_guarantor_id = $guarantors[$i]->id;
            $mainJoin->main_guarantor_code = '';
            $mainJoin->save();

            DB::connection('portal')->table('tbl_guarantors')->insert([
                'branch_id' => $guarantors[$i]->branch_id == null ? 1 : $guarantors[$i]->branch_id,
                'guarantor_uniquekey' => $mainJoin->portal_guarantor_id,
                'name' => $guarantors[$i]->full_name_en,
                'dob' => $guarantors[$i]->dob,
                'nrc' => $guarantors[$i]->nrc_number,
                //'gender' => '',
                'phone_primary' => $guarantors[$i]->mobile,
                'email' => $guarantors[$i]->email == null ? 'N/A' : $guarantors[$i]->email,
                //'blood_type' => $guarantors[$i]->blood_type,
                'religion' => '',
                'nationality' => 'Myanmar',//$guarantors[$i]->country_id,
                //'other_education' => '',
                'income' => $guarantors[$i]->income,
                'quarter_id' => '',
                'township_id' => '',
                'district_id' => $guarantors[$i]->district_id,
                'division_id' => '',
                'city_id' => '',
                'address_primary' => $guarantors[$i]->address,
                //'address_secondary',
                'guarantor_nrc_front_photo' => '',
                'guarantor_nrc_back_photo' => '',
                'guarantor_photo' => '',
                'kyc_photo' => '',
                //'current_job' => ,
                //'nrc_card_id',
                //'old_nrc',
                'name_mm' => $guarantors[$i]->full_name_mm,
                //'education_id'
            ]);
        }

        // Save crawl counter
        $entrey = DB::connection('portal')->table('tbl_guarantors')->get();
        DB::connection('portal')->table('tbl_crawling')->where('module_name', 'guarantor')->update([
            'data_count' => count($entrey),
            'crawl_date' => date('Y-m-d H:i:s')
        ]);
        if ($entrey) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => count($entrey)]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //guarantor end -------------------------------

    //reverse staff start ----------------------

    public function staffRecrawlIndex()
    {
        $reverse_staffs = DB::table('tbl_reverse_crawling')->where('module_name', 'staff')->get();
        return view('crawling.reverse_crawl.staff_reverse_crawl.index', compact('reverse_staffs'));
    }

    public function reverseCrawlStaffs()
    {
        $staffs = DB::connection('portal')->table('tbl_staff')
            ->leftjoin('tbl_branches','tbl_staff.branch_id','=','tbl_branches.id')
            ->select("tbl_staff.*","tbl_branches.*")
            // ->where('tbl_staff.id', 936)
            ->get();
//        return response()->json($staffs);

        for($i = 0; $i < count($staffs); $i++){
            if(DB::table('main_join')->where('portal_staff_id', $staffs[$i]->staff_code)->first()){
                continue;
            }

//            $staffid = DB::table('main_join')->where('portal_staff_id', $staffs[$i]->staff_code)->first()->main_staff_id;

            DB::connection('main')->table('users')->insert([
                //'id' => $staffs[$i]->id,
                'finger_print_no' => $staffs[$i]->finger_biometric,
                'name' => $staffs[$i]->name,
                'phone' => $staffs[$i]->phone,
                'email' => $staffs[$i]->email,
                'photo' => $staffs[$i]->photo,
                'branch_id' => $staffs[$i]->branch_id,
                'center_leader_id' =>  null,//$staffs[$i]->center_leader_id,
                'email_verified_at' => $staffs[$i]->email_verified_at,
                'password' => $staffs[$i]->user_password,
                //'remember_token' => $staffs[$i]->user_token,
                'created_at' => $staffs[$i]->created_at,
                'updated_at' => $staffs[$i]->updated_at,
                //'created_by' =>
                //'updated_by' =>
                'user_code'=> $staffs[$i]->staff_code,
                'br_code' => $staffs[$i]->branch_code,
                //'cen_code' => $staffs[$i]->center_uniquekey
            ]);

            $mainStaffid = DB::connection('main')->table('users')->where('email', $staffs[$i]->email)->first()->id;
            DB::table('main_join')->insert([
                'main_staff_id' => $mainStaffid,
                'portal_staff_id' => $staffs[$i]->staff_code
            ]);
        }

        $verify = DB::connection('main')->table('users')->get();
        if ($verify) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => count($verify)]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //reverse staff end ----------------------

    //reverse center start -------------------

    public function centerRecrawlIndex()
    {
        $reverse_centers = DB::table('tbl_reverse_crawling')->where('module_name', 'center')->get();
        return view('crawling.reverse_crawl.center_reverse_crawl.index', compact('reverse_centers'));
    }

    public function reverseCrawlCenters()
    {
        $centers = DB::connection('portal')->table('tbl_center')
            ->leftJoin('tbl_branches','tbl_center.branch_id','=','tbl_branches.id')
            ->select("tbl_center.*","tbl_branches.*")
            ->get();
       // return response()->json($centers);
        //DB::connection('mkt_backup')->statement('ALTER TABLE center_leaders AUTO_INCREMENT = 1');

        for($i = 0; $i < count($centers); $i++){
            if(DB::table('main_join')->where('portal_center_id', $centers[$i]->center_uniquekey)->first()){
                continue;
            }

            $division_id = DB::connection('main')->table('addresses')->where('id', $centers[$i]->division_id)->first()->code;
            $district_id = DB::connection('main')->table('addresses')->where('id', $centers[$i]->district_id)->first()->code;
            $village_id = DB::connection('main')->table('addresses')->where('id', $centers[$i]->village_id)->first()->code;
            $commue_id = DB::connection('main')->table('addresses')->where('id', $centers[$i]->township_id)->first()->code;

            DB::connection('main')->table('center_leaders')->insert([
                //'id' => $centers[$i]->id,
                'branch_id' => $centers[$i]->branch_id,
                'code' => $centers[$i]->center_uniquekey,
                'title' => $centers[$i]->center_uniquekey,
                //'phone' => $centers[$i]->phone_primary,
                //'location'
                //'description'
                'created_at' => $centers[$i]->created_at,
                'updated_at' => $centers[$i]->updated_at,
                'address' => $centers[$i]->full_address,
                'province_id' => $division_id,
                'district_id' => $district_id,
                'commune_id' => $commue_id,
                'village_id' => $village_id,
                //'seq'
                //'created_by'
                //'updated_by'
                'br_code' => $centers[$i]->branch_code,
                'user_id' => 1290 //change later
            ]);

            DB::table('main_join')->insert([
               'main_center_id' => DB::connection('main')->table('center_leaders')->where('code', $centers[$i]->center_uniquekey)->first()->id,
               'portal_center_id' => $centers[$i]->center_uniquekey
            ]);
        }

        $verify = DB::connection('main')->table('center_leaders')->get();

        DB::table('tbl_reverse_crawling')->where('module_name', 'center')->update([
            'data_count' => count($verify)
        ]);

        if ($verify) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => count($verify)]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //reverse center end -------------------

    //reverse group start ------------------
    public function groupRecrawlIndex()
    {
        $reverse_groups = DB::table('tbl_reverse_crawling')->where('module_name', 'group')->get();
        return view('crawling.reverse_crawl.group_reverse_crawl.index', compact('reverse_groups'));
    }

    public function reverseCrawlGroups()
    {
        $groups = DB::connection('portal')->table('tbl_group')
            ->leftJoin('tbl_branches','tbl_group.branch_id','=','tbl_branches.id')
            ->get();
//        return response()->json($groups);

        for($i = 0; $i < count($groups); $i++){
            if(DB::table('main_join')->where('portal_group_id', $groups[$i]->group_uniquekey)->first()){
                continue;
            }

            DB::connection('main')->table('group_loans')->insert([
                //'id' => $groups[$i]->id,
                'group_code' => $groups[$i]->group_uniquekey,
                'group_name' => $groups[$i]->group_uniquekey,
                'center_id' => 10,//$groups[$i]->center_id,    change later
                'created_at' => $groups[$i]->created_at,
                'updated_at' => $groups[$i]->updated_at,
                //'created_by' => $groups[$i]->staff_id,
                //'updated_by' => $groups[$i]->staff_id,
                'branch_id' => $groups[$i]->branch_id
            ]);

            DB::table('main_join')->insert([
               'main_group_id' => DB::connection('main')->table('group_loans')->where('group_code', $groups[$i]->group_uniquekey)->first()->group_code,
               'portal_group_id' => $groups[$i]->group_uniquekey
            ]);

        }

        $verify = DB::connection('main')->table('group_loans')->get();

        DB::table('tbl_reverse_crawling')->where('module_name', 'group')->update([
           'data_count'=> count($verify)
        ]);

        if ($verify) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => count($verify)]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //reverse group end ------------------

    //reverse guarantor start ------------------
    public function guarantorRecrawlIndex()
    {
        $reverse_guarantors = DB::table('tbl_reverse_crawling')->where('module_name', 'guarantor')->get();
        return view('crawling.reverse_crawl.guarantor_reverse_crawl.index', compact('reverse_guarantors'));
    }

    public function reverseCrawlGuarantors()
    {
        $guarantors = DB::connection('portal')->table('tbl_guarantors')
            ->leftJoin('tbl_center', 'tbl_center.branch_id', '=', 'tbl_guarantors.branch_id')
            ->leftJoin('tbl_branches','tbl_guarantors.branch_id','=','tbl_branches.id')
            ->select('tbl_center.*', 'tbl_guarantors.*', 'tbl_branches.id as branch_id')
            ->get();
//        return response()->json($guarantors);

        for($i = 10; $i < count($guarantors); $i++){
            if(DB::table('main_join')->where('portal_guarantor_id', $guarantors[$i]->guarantor_uniquekey)->first()){
                continue;
            }

            $division_id = DB::connection('main')->table('addresses')->where('id', $guarantors[$i]->division_id)->first()->code;
            $district_id = DB::connection('main')->table('addresses')->where('id', $guarantors[$i]->district_id)->first()->code;
            $village_id = DB::connection('main')->table('addresses')->where('id', $guarantors[$i]->village_id)->first()->code;
            $commue_id = DB::connection('main')->table('addresses')->where('id', $guarantors[$i]->township_id)->first()->code;

            DB::connection('main')->table('guarantors')->insert([
                //'id' => $guarantors[$i}->id,
                'nrc_number' => $guarantors[$i]->nrc == null ? $guarantors[$i]->old_nrc : $guarantors[$i]->nrc,
//                'title' => $guarantors->gender == 'male' ? 'Mr' : 'Ms',
                'full_name_en' => $guarantors[$i]->name,
                'full_name_mm' => $guarantors[$i]->name_mm,
                'mobile' => $guarantors[$i]->phone_primary,
                'phone' => $guarantors[$i]->phone_secondary,
                'email' => $guarantors[$i]->email,
                'dob' => $guarantors[$i]->dob,
                //'working_status_id',
                //'place_of_birth',
                'photo' => $guarantors[$i]->guarantor_photo,
                //'attach_file',
                //'country_id',
                'address' => $guarantors[$i]->address_primary,
                'province_id' => $division_id,
                'district_id' => $district_id,
                'commune_id' => $commue_id,
                'village_id' => $village_id,
                //'ward_id',
                //'street_number',
                //'house_number',
                //'description',
                //'marital_status',
//                'spouse_gender',
                //'spouse_name'
                //'spouse_date_of_birth'
                //'number_of_child'
                //'spouse_mobile_phone'
                //'user_id',
                'branch_id' => $guarantors[$i]->branch_id,
                'center_leader_id' => $guarantors[$i]->staff_client_id,
                //'business_info',
                'income' => $guarantors[$i]->income,
                //'nrc_type'
                //'seq',
                'created_at' => $guarantors[$i]->created_at,
                'updated_at' => $guarantors[$i]->updated_at,
                //'created_by' => $guarantors[$i]->staff_client_id,
                //a bit unsure about this
                //'updated_by' => $guarantors[$i]->staff_client_id,
                //'annual_income'
            ]);

            DB::table('main_join')->insert([
               'main_guarantor_id' => DB::connection('main')->table('guarantors')->where('nrc_number', $guarantors[$i]->old_nrc)->orWhere('nrc_number', $guarantors[$i]->nrc)->first()->id,
                'portal_guarantor_id' => $guarantors[$i]->guarantor_uniquekey
            ]);
        }

        $verify = DB::connection('main')->table('guarantors')->get();

        DB::table('tbl_reverse_crawling')->where('module_name', 'guarantor')->update([
           'data_count' => count($verify)
        ]);

        if ($verify) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => count($verify)]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //reverse guarantor end ------------------
}
