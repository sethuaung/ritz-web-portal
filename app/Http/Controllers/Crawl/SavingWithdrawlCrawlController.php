<?php

namespace App\Http\Controllers\Crawl;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SavingWithdrawlCrawlController extends Controller
{
    public function index()
    {
        $crawl_savings = DB::table('tbl_crawling')->where('module_name', 'saving_withdrawl')->get();
        return view('crawling.crawl.savingwithdrawl_crawl.index', compact('crawl_savings'));
    }

    public function getBranchCode($branchid)
    {
        $bcode = DB::table('tbl_branches')->where('id', $branchid)->first()->branch_code;
        return $bcode;
    }

    public function generateSavingAccID($branchid)
    {
        $bcode = $this->getBranchCode($branchid);
        $bcode = strtolower($bcode);


        $savingId = DB::table($bcode . '_saving_withdrawl')->get()->last();
        if ($savingId == null) {
            $bcode = strtoupper($bcode);
            $savingId = $bcode;
            $savingId .= '-';
            $savingId .= 'S';
            $savingId .= '-';
            $savingId .= '0000001';
        } else {
            $savingId = $savingId->saving_unique_id;

            $bstr = substr($savingId, 3);
            $id = substr($bstr, -7, 7);
            $bstr_cusm = $id + 1;
            $bstr_cusm = (string)str_pad($bstr_cusm, 7, '0', STR_PAD_LEFT);

            // get loan_uniquekey in tbl_loans
            $savingId = strtoupper($bcode); // Branch Code
            $savingId .= '-';
            $savingId .= 'S';
            $savingId .= '-';
            $savingId .= $bstr_cusm;
        }

        return $savingId;
    }

    public function crawlSavingWithdrawls()
    {
        $branchCount = DB::table('tbl_branches')->count();

    	$z = 0;
        //$y = branchid with loop
        for ($y = 1; $y <= 2; $y++) {
            $branch_code = $this->getBranchCode($y);
            $branch_code = strtolower($branch_code);
            $table = $branch_code . '_saving_withdrawl';

            $data = DB::connection('main')->table('loan_compulsory_' . $y)
                ->leftJoin('loans_' . $y, 'loans_' . $y . '.id', '=', 'loan_compulsory_' . $y . '.loan_id')
                ->where('compulsory_status', '=', 'Active')
                ->select('loan_id')
                ->get();

            for ($i = 0; $i < count($data); $i++) {
                //update existing records
                if (DB::connection('portal')->table('tbl_main_join_saving')->where('main_loan_id', $data[$i]->loan_id)->first()) {
                    $saving_id=DB::connection('portal')->table('tbl_main_join_saving')->where('main_loan_id', $data[$i]->loan_id)->first()->portal_saving_withdrawl_id;
                    // $portal_info = DB::connection('portal')->table('tbl_main_join_loan')->where('main_loan_id', $data[$i]->loan_id)->first();
                    // $client_info = DB::table($branch_code . '_loans')
                    //     ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', $branch_code . '_loans.client_id')
                    //     ->where($branch_code . '_loans.loan_unique_id', $portal_info->portal_loan_id)
                    //     ->first();
                    // $main_client_id = DB::connection('portal')->table('tbl_main_join_client')->where('portal_client_id', $client_info->client_uniquekey)->first()->main_client_code;

                    $principle = DB::connection('main')->table('compulsory_saving_transaction')
                        ->where('loan_id', $data[$i]->loan_id)
                        ->where('train_type_ref', '=', 'deposit')
                        ->first()->amount;

                    $interest = DB::connection('main')->table('compulsory_saving_transaction')
                        ->where('loan_id', $data[$i]->loan_id)
                        ->where('train_type_ref', '=', 'accrue-interest')
                        ->sum('amount');


                    if (DB::connection('main')->table('compulsory_saving_transaction')->where('loan_id', $data[$i]->loan_id)->where('train_type_ref', '=', 'withdraw')->first()) {
                        $withdraw_saving_info = DB::connection('main')->table('compulsory_saving_transaction')
                            ->where('loan_id', $data[$i]->loan_id)
                            ->where('train_type_ref', '=', 'withdraw')
                            ->orderBy('id', 'desc')
                            ->first();

                        $principle_balance = $withdraw_saving_info->total_principle;
                        $interest_balance = $withdraw_saving_info->total_interest;
                    } else {
                        $principle_balance = $principle;
                        $interest_balance = $interest;
                    }

                    DB::connection('portal')->table($table)->where('saving_unique_id',$saving_id)->update([
                        // 'loan_unique_id' => $data[$i]->loan_id,
                        // 'client_idkey' => $client_info->client_uniquekey,
                        // 'client_name' => $client_info->name,
                        // 'total_saving'=> $savings[$i]->principles,
                        'total_saving' => $principle,
                        'total_interest' => $interest,
                        'saving_withdrawl_amount' => 0,
                        'interest_withdrawl_amount' => 0,
                        'saving_left' => $principle_balance,
                        'interest_left' => $interest_balance,
                        'withdrawl_type' => 'none',
                        'status' => 'none',
                        'date' => NULL,
                        // 'created_at' => $data[$i]->created_at,
                        // 'updated_at' => $data[$i]->updated_at
                    ]);
                	continue;
                }
                //insert new saving infos
                $portal_info = DB::connection('portal')->table('tbl_main_join_loan')->where('main_loan_id', $data[$i]->loan_id)->first();
            	if($portal_info){
                	
                }else{
                	continue;
                }
                $client_info = DB::table($branch_code . '_loans')
                    ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', $branch_code . '_loans.client_id')
                    ->where($branch_code . '_loans.loan_unique_id', $portal_info->portal_loan_id)
                    ->first();

                $saving_id = $this->generateSavingAccID($y);
                $main_client_id = DB::connection('portal')->table('tbl_main_join_client')->where('portal_client_id', $client_info->client_uniquekey)->first()->main_client_code;

                DB::connection('portal')->table('tbl_main_join_saving')->insert([
                    'portal_saving_withdrawl_id' => $saving_id,
                    'main_loan_id' => $data[$i]->loan_id,
                    'portal_client_id' => $client_info->client_uniquekey,
                    'main_client_id' => $main_client_id,
                    // 'created_at' => $data[$i]->created_at,
                    // 'updated_at' => $data[$i]->updated_at,
                ]);


                $principle = DB::connection('main')->table('compulsory_saving_transaction')
                    ->where('loan_id', $data[$i]->loan_id)
                    ->where('train_type_ref', '=', 'deposit')
                    ->first()->amount;

                $interest = DB::connection('main')->table('compulsory_saving_transaction')
                    ->where('loan_id', $data[$i]->loan_id)
                    ->where('train_type_ref', '=', 'accrue-interest')
                    ->sum('amount');


                if (DB::connection('main')->table('compulsory_saving_transaction')->where('loan_id', $data[$i]->loan_id)->where('train_type_ref', '=', 'withdraw')->first()) {
                    $withdraw_saving_info = DB::connection('main')->table('compulsory_saving_transaction')
                        ->where('loan_id', $data[$i]->loan_id)
                        ->where('train_type_ref', '=', 'withdraw')
                        ->orderBy('id', 'desc')
                        ->first();

                    $principle_balance = $withdraw_saving_info->total_principle;
                    $interest_balance = $withdraw_saving_info->total_interest;
                } else {
                    $principle_balance = $principle;
                    $interest_balance = $interest;
                }

                DB::connection('portal')->table($table)->insert([
                    'saving_unique_id' => $saving_id,
                    'loan_unique_id' => $data[$i]->loan_id,
                    'client_idkey' => $client_info->client_uniquekey,
                    'client_name' => $client_info->name,
                    // 'total_saving'=> $savings[$i]->principles,
                    'total_saving' => $principle,
                    'total_interest' => $interest,
                    'saving_withdrawl_amount' => 0,
                    'interest_withdrawl_amount' => 0,
                    'saving_left' => $principle_balance,
                    'interest_left' => $interest_balance,
                    'withdrawl_type' => 'none',
                    'status' => 'none',
                    'date' => NULL,
                    // 'created_at' => $data[$i]->created_at,
                    // 'updated_at' => $data[$i]->updated_at
                ]);
            
            	$z = ++$z;
            }
        }



        $verify = DB::connection('portal')->table($table)->get();
        if ($z) {
            DB::connection('portal')->table('tbl_crawling')->where('module_name', 'saving_withdrawl')->update([
                'data_count' => $z,
                'crawl_date' => date('Y-m-d H:i:s')
            ]);

            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $z]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
}
