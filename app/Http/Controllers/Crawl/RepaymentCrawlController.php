<?php

namespace App\Http\Controllers\Crawl;

use App\Http\Controllers\Controller;
use App\Models\Crawl\Crawl_Loan_Repayment_Schedule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class RepaymentCrawlController extends Controller
{
    public function index()
    {
        $crawl_repayments = DB::table('tbl_crawling')->where('module_name', 'repayment')->get();
        return view('crawling.crawl.repayment_crawl.index', compact('crawl_repayments'));
    }

    public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }

    public function crawRepaymentListwithAuthorization($branch_id)
    {

        $route = $this->getMainUrl37();

        $route_name = 'get-repayments';
        $routeurl = $route . $route_name . "?branch_id=" . $branch_id;
        $authorization = $this->loginToken();
        $headers = [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $authorization,
        ];
        $response = Http::withHeaders($headers)->get($routeurl);
        //dd(json_decode($response));

        return json_decode($response)->data;
    }

    public function getBranchCode($branchid)
    {
        $bcode = DB::table('tbl_branches')->where('id', $branchid)->first()->branch_code;
        return $bcode;
    }

    public function getRepaymentDetail()
    {
        //$RpDetail =DB::connection('main')->table('loan_disbursement_calculate_1')
        //->where('disbursement_id', '16679663738365')->get();
        $RpDetail = DB::connection('portal')->table('ab_loans_schedule')->where('loan_unique_id', 'AB-03-000000002')->get();
        if ($RpDetail) {
            return response()->json(['status_code' => 200, 'message' => 'success success', 'data' => $RpDetail]);
        } else {
            return response()->json(['status_code' => 400, 'message' => 'success', 'data' => null]);
        }
    }

    public function crawlRepayments()
    {
        $branchCount = DB::table('tbl_branches')->count();
    	$sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;

        //$k == branchcode with loop
        $z = 0;
        for ($k = $sessionBranch; $k <= $sessionBranch; $k++) {
            // $repayments = $this->crawRepaymentListwithAuthorization($k);
        	$repayments = DB::connection('main')->table('loan_payments')->leftjoin('loans_'.$k, 'loans_'.$k.'.id', '=', 'loan_payments.disbursement_id')->where('loan_payments.branch_id', $k)->get();
            $branch_code = $this->getBranchCode($k);
            $branch_code = strtolower($branch_code);

            // return response()->json($repayments);

            for ($i = 0; $i < count($repayments); $i++) {
                $loan_id = DB::table('tbl_main_join_loan')->where('main_loan_id', '=', $repayments[$i]->disbursement_id)->where('branch_id', $k)->first();
                if ($loan_id) {
                    $loan_id = $loan_id->portal_loan_id;
                } else {
                    continue;
                }


                // $LoansSchedule = DB::connection('portal')->table($branch_code . '_loans_schedule')->where('loan_unique_id', $loan_id)->get();
                // $RpDetail = DB::connection('main')->table('loan_disbursement_calculate_' . $k)->where('disbursement_id', $repayments[$i]->disbursement_id)->get();

                // for ($y = 0; $y < count($LoansSchedule); $y++) {
                //     if (DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$y]->id)->where('branch_id', $k)->first()) {
                //         break;
                //     } else {
                //         // if ($RpDetail[$y]->no == ($y + 1)) {
                //         //     $mainJoin = new Crawl_Loan_Repayment_Schedule();
                //         //     $mainJoin->portal_disbursement_id = $LoansSchedule[$y]->loan_unique_id;
                //         //     $mainJoin->portal_disbursement_schedule_id = $LoansSchedule[$y]->id;
                //         //     $mainJoin->main_disbursement_schedule_id = $RpDetail[$y]->id;
                //         //     $mainJoin->branch_id = $k;
                //         //     $mainJoin->save();
                //         // }
                //     }
                // }


                $table = $branch_code . '_repayment';
            	// return response()->json(['status_code' => 200, 'message' => 'success loan', 'data' => $table.'_late']);

                $rp_table_type = '';
                $repayment_status = '';
                if ($repayments[$i]->pre_repayment == 1) {
                    $rp_table_type = '_pre';
                } elseif ($repayments[$i]->late_repayment == 1) {
                    $rp_table_type = '_late';
                } else {
                    $rp_table_type = '_due';
                }

                if ($repayments[$i]->owed_balance == 0) {
                    $repayment_status = 'paid';
                } else {
                    $repayment_status = 'pending';
                }

                $schedules = explode('x', $repayments[$i]->disbursement_detail_id);

                for ($u = 0; $u < count($schedules); $u++) {

                	if(DB::table('tbl_main_join_repayment_schedule')->where('main_disbursement_schedule_id', str_replace('"', '', $schedules[$u]))->where('branch_id', $k)->first()) {
                		
                	} else {
                    	continue;
                    }
                    $scheduleid = DB::table('tbl_main_join_repayment_schedule')->where('main_disbursement_schedule_id', str_replace('"', '', $schedules[$u]))->where('branch_id', $k)->first()->portal_disbursement_schedule_id;

                    if (DB::table($branch_code . '_loans_schedule')->where('id', $scheduleid)->first()->status == 'paid') {
                        continue;
                    }

                    $scheduleDetail = DB::table($branch_code . '_loans_schedule')->where('id', $scheduleid)->first();


                    if ($repayments[$i]->late_repayment == 1) {
                        // return response()->json(str_replace('"', '', $schedules[$u]));
                        if (DB::table('tbl_main_join_repayment')->where('main_repayment_disbursement_detail_id', str_replace('"', '', $schedules[$u]))->first()) {
                            if (DB::table('tbl_main_join_repayment')->where('main_repayment_disbursement_detail_id', str_replace('"', '', $schedules[$u]))->orderBy('id', 'desc')->first()->main_owed_balance == $repayments[$i]->owed_balance) {
                                continue;
                            } else {
                                if (DB::table($branch_code . '_repayment_late')->where('schedule_id', $scheduleDetail->id)->first()) {
                                    $lastPaid = DB::table($branch_code . '_repayment_late')->where('schedule_id', $scheduleDetail->id)->latest($branch_code . '_repayment_late.schedule_id')->first();
                                    // return response()->json($lastPaid);
                                    $principlebalance = $lastPaid->principal_balance - $repayments[$i]->principle;
                                    $interestbalance = $lastPaid->interest_balance - $repayments[$i]->interest;
                                } else {
                                    $principlebalance = $repayments[$i]->principle;
                                    $interestbalance = $repayments[$i]->interest;
                                }
                                // return response()->json($table . $rp_table_type);
                                DB::connection('portal')->table($table . '_late')
                                    ->insert([
                                        'schedule_id' => $scheduleid,
                                        //'repayment_id'=>$repayments[$i]->id,
                                        'disbursement_id' => $loan_id,
                                        'cash_acc_id' => $repayments[$i]->cash_acc_id,
                                        'payment_number' => $repayments[$i]->payment_number,
                                        'receipt_no' => $repayments[$i]->receipt_no,
                                        //'repayment_process_id'=>$repayments[$i]->branch_id,
                                        'principal' => $scheduleDetail->capital,
                                        'principal_balance' => $principlebalance,
                                        'interest' => $scheduleDetail->interest,
                                        'interest_balance' => $interestbalance,
                                        'principal_paid' => $repayments[$i]->principle,
                                        'interest_paid' => $repayments[$i]->interest,
                                        'total_amount_paid' => $repayments[$i]->payment,
                                        'penalty' => $repayments[$i]->penalty_amount,
                                        'compulsory_saving' => $repayments[$i]->compulsory_saving,
                                        'total_amount' => $scheduleDetail->interest + $scheduleDetail->capital,
                                        // 'final_paid_balance' => $repayments[$i]->principle_balance,
                                        'own_balance' => $repayments[$i]->owed_balance,
                                        'over_days' => $repayments[$i]->over_days,
                                        'documents' => $repayments[$i]->document,
                                        'payment_method' => $repayments[$i]->payment_method,
                                        'repayment_status' => $repayment_status,
                                        'payment_date' => $repayments[$i]->payment_date,
                                        'created_at' => $repayments[$i]->created_at,
                                        'updated_at' => $repayments[$i]->updated_at,
                                        'counter_name' => DB::table('tbl_main_join_staff')->where('main_staff_id', $repayments[$i]->updated_by)->first()->portal_staff_id
                                    ]);

                                DB::table('tbl_main_join_repayment')->insert([
                                    'portal_repayment_id' => DB::table($branch_code . '_repayment_late')->where('schedule_id', $scheduleid)->orderby('repayment_id', 'desc')->first()->repayment_id,
                                    'main_owed_balance' => $repayments[$i]->owed_balance,
                                    'main_repayment_disbursement_detail_id' => str_replace('"', '', $schedules[$u])
                                ]);

                                DB::table($branch_code . '_loans_schedule')->where('id', $scheduleid)->update([
                                    'repayment_type' => 'late',
                                    'repayment_date' => $repayments[$i]->payment_date,
                                ]);

                                if ($repayment_status == 'paid') {
                                    DB::table($branch_code . '_loans_schedule')->where('id', $scheduleid)->update([
                                        'status' => 'paid',
                                        'repayment_date' => $repayments[$i]->payment_date,
                                    ]);
                                }

                                $portalLoanID = DB::table('tbl_main_join_loan')->where('main_loan_id', $repayments[$i]->disbursement_id)->where('branch_id', $k)->first()->portal_loan_id;

                                if ($repayments[$i]->disbursement_status == 'Closed') {
                                    DB::table($branch_code . '_loans')->where('loan_unique_id', $portalLoanID)->update([
                                        'disbursement_status' => 'Closed'
                                    ]);
                                }
                            }
                        } else {
                            DB::connection('portal')->table($table . '_late')
                                ->insert([
                                    'schedule_id' => $scheduleid,
                                    //'repayment_id'=>$repayments[$i]->id,
                                    'disbursement_id' => $loan_id,
                                    'cash_acc_id' => $repayments[$i]->cash_acc_id,
                                    'payment_number' => $repayments[$i]->payment_number,
                                    'receipt_no' => $repayments[$i]->receipt_no,
                                    //'repayment_process_id'=>$repayments[$i]->branch_id,
                                    'principal' => $scheduleDetail->capital,
                                    'principal_balance' => $scheduleDetail->capital - $repayments[$i]->principle,
                                    'interest' => $scheduleDetail->interest,
                                    'interest_balance' => $scheduleDetail->interest - $repayments[$i]->interest,
                                    'principal_paid' => $repayments[$i]->principle,
                                    'interest_paid' => $repayments[$i]->interest,
                                    'total_amount_paid' => $repayments[$i]->payment,
                                    'penalty' => $repayments[$i]->penalty_amount,
                                    'compulsory_saving' => $repayments[$i]->compulsory_saving,
                                    'total_amount' => $scheduleDetail->interest + $scheduleDetail->capital,
                                    // 'final_paid_balance' => $repayments[$i]->principle_balance,
                                    'own_balance' => $repayments[$i]->owed_balance,
                                    'over_days' => $repayments[$i]->over_days,
                                    'documents' => $repayments[$i]->document,
                                    'payment_method' => $repayments[$i]->payment_method,
                                    'repayment_status' => $repayment_status,
                                    'payment_date' => $repayments[$i]->payment_date,
                                    'created_at' => $repayments[$i]->created_at,
                                    'updated_at' => $repayments[$i]->updated_at,
                                    'counter_name' => DB::table('tbl_main_join_staff')->where('main_staff_id', $repayments[$i]->updated_by)->first()->portal_staff_id
                                ]);

                            DB::table('tbl_main_join_repayment')->insert([
                                'portal_repayment_id' => DB::table($branch_code . '_repayment_late')->where('schedule_id', $scheduleid)->orderby('repayment_id', 'desc')->first()->repayment_id,
                                'portal_schedule_id' => $scheduleid,
                                'main_owed_balance' => $repayments[$i]->owed_balance,
                                'main_repayment_disbursement_detail_id' => str_replace('"', '', $schedules[$u])
                            ]);

                            DB::table($branch_code . '_loans_schedule')->where('id', $scheduleid)->update([
                                'repayment_type' => 'late',
                                'repayment_date' => $repayments[$i]->payment_date,
                            ]);

                            if ($repayment_status == 'paid') {
                                DB::table($branch_code . '_loans_schedule')->where('id', $scheduleid)->update([
                                    'status' => 'paid',
                                    'repayment_date' => $repayments[$i]->payment_date,
                                ]);
                            }

                            $portalLoanID = DB::table('tbl_main_join_loan')->where('main_loan_id', $repayments[$i]->disbursement_id)->where('branch_id', $k)->first()->portal_loan_id;

                            if ($repayments[$i]->disbursement_status == 'Closed') {
                                DB::table($branch_code . '_loans')->where('loan_unique_id', $portalLoanID)->update([
                                    'disbursement_status' => 'Closed'
                                ]);
                            }
                        }

                        $z = ++$z;
                    } elseif ($repayments[$i]->pre_repayment == 1) {
                        DB::connection('portal')->table($table . '_pre')
                            ->insert([
                                'schedule_id' => $scheduleid,
                                //'repayment_id'=>$repayments[$i]->id,
                                'disbursement_id' => $loan_id,
                                'cash_acc_id' => $repayments[$i]->cash_acc_id,
                                'payment_number' => $repayments[$i]->payment_number,
                                'receipt_no' => $repayments[$i]->receipt_no,
                                //'repayment_process_id'=>$repayments[$i]->branch_id,
                                'principal' => $repayments[$i]->principle,
                                'interest' => $repayments[$i]->interest,
                                'penalty' => $repayments[$i]->penalty_amount,
                                'compulsory_saving' => $repayments[$i]->compulsory_saving,
                                'total_amount' => $repayments[$i]->total_payment,
                                // 'final_paid_balance' => $repayments[$i]->principle_balance,
                                'own_balance' => $repayments[$i]->owed_balance,
                                'over_days' => $repayments[$i]->over_days,
                                'documents' => $repayments[$i]->document,
                                'payment_method' => $repayments[$i]->payment_method,
                                'repayment_status' => $repayment_status,
                                'payment_date' => $repayments[$i]->payment_date,
                                'created_at' => $repayments[$i]->created_at,
                                'updated_at' => $repayments[$i]->updated_at,
                                'counter_name' => DB::table('tbl_main_join_staff')->where('main_staff_id', $repayments[$i]->updated_by)->first()->portal_staff_id
                            ]);

                        // DB::table('tbl_main_join_repayment')->insert([
                        //     'portal_repayment_id' => DB::table($branch_code . '_repayment_late')->orderby('repayment_id', 'desc')->first()->repayment_id,
                        //     'main_owed_balance' => $repayments[$i]->owed_balance,
                        //     'main_repayment_disbursement_detail_id' => str_replace('"', '', $schedules[$u])
                        // ]);

                        DB::table($branch_code . '_loans_schedule')->where('id', $scheduleid)->update([
                            'status' => 'paid',
                            'repayment_date' => $repayments[$i]->payment_date,
                        ]);

                        DB::table($branch_code . '_loans_schedule')->where('id', $scheduleid)->update([
                            'repayment_type' => 'pre',
                            'interest' => $repayments[$i]->interest,
                            'repayment_date' => $repayments[$i]->payment_date,
                        ]);


                        $portalLoanID = DB::table('tbl_main_join_loan')->where('main_loan_id', $repayments[$i]->disbursement_id)->where('branch_id', $k)->first()->portal_loan_id;

                        if ($repayments[$i]->disbursement_status == 'Closed') {
                            DB::table($branch_code . '_loans')->where('loan_unique_id', $portalLoanID)->update([
                                'disbursement_status' => 'Closed'
                            ]);
                        }
                        $z = ++$z;
                    } else {
                        DB::connection('portal')->table($table . '_due')
                            ->insert([
                                'schedule_id' => $scheduleid,
                                //'repayment_id'=>$repayments[$i]->id,
                                'disbursement_id' => $loan_id,
                                'cash_acc_id' => $repayments[$i]->cash_acc_id,
                                'payment_number' => $repayments[$i]->payment_number,
                                'receipt_no' => $repayments[$i]->receipt_no,
                                //'repayment_process_id'=>$repayments[$i]->branch_id,
                                'principal' => $scheduleDetail->capital,
                                'interest' => $scheduleDetail->interest,
                                'penalty' => $repayments[$i]->penalty_amount,
                                'compulsory_saving' => $repayments[$i]->compulsory_saving,
                                'total_balance' => $scheduleDetail->capital + $scheduleDetail->interest,
                                'final_paid_balance' => $repayments[$i]->principle_balance,
                                'own_balance' => $repayments[$i]->owed_balance,
                                'over_days' => $repayments[$i]->over_days,
                                'documents' => $repayments[$i]->document,
                                'payment_method' => $repayments[$i]->payment_method,
                                'repayment_status' => $repayment_status,
                                'payment_date' => $repayments[$i]->payment_date,
                                'created_at' => $repayments[$i]->created_at,
                                'updated_at' => $repayments[$i]->updated_at,
                                'counter_name' => DB::table('tbl_main_join_staff')->where('main_staff_id', $repayments[$i]->updated_by)->first()->portal_staff_id
                            ]);

                        DB::table($branch_code . '_loans_schedule')->where('id', $scheduleid)->update([
                            'status' => 'paid',
                            'repayment_date' => $repayments[$i]->payment_date,
                        ]);

                        DB::table($branch_code . '_loans_schedule')->where('id', $scheduleid)->update([
                            'repayment_type' => 'due',
                            'repayment_date' => $repayments[$i]->payment_date,
                        ]);

                        $portalLoanID = DB::table('tbl_main_join_loan')->where('main_loan_id', $repayments[$i]->disbursement_id)->where('branch_id', $k)->first()->portal_loan_id;

                        if ($repayments[$i]->disbursement_status == 'Closed') {
                            DB::table($branch_code . '_loans')->where('loan_unique_id', $portalLoanID)->update([
                                'disbursement_status' => 'Closed'
                            ]);
                        }

                        $z = ++$z;
                    }

                    // }
                }
            }
        }

        // $verify_pre = DB::connection('portal')->table($branch_code . '_repayment_pre')->get();
        // $verify_late = DB::connection('portal')->table($branch_code . '_repayment_late')->get();
        // $verify_due = DB::connection('portal')->table($branch_code . '_repayment_due')->get();
        // $total_data = count($verify_pre) + count($verify_late) + count($verify_due);
        DB::connection('portal')->table('tbl_crawling')->where('module_name', 'repayment')->update([
            'data_count' => $z,
            'crawl_date' => date('Y-m-d H:i:s')
        ]);
        // if ($verify_pre == true && $verify_late == true && $verify_due == true) {
		if($z) {
            return response()->json(['status_code' => 200, 'message' => 'success loan', 'data' => $z]);
        } else {
            return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
        }
    }
}
