<?php

namespace App\Http\Controllers\Crawl;

use App\Http\Controllers\Controller;
use App\Models\Crawl\Crawl_Loan_Deposit;
use App\Models\Loandataenteries\SavingWithdrawl;
use App\Models\MainJoin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class DepositCrawlController extends Controller
{
    public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json',
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456',
        ]);
        return json_decode($response)->data[0]->token;
    }

    public function crawDepositListwithAuthorization()
    {
        $route = $this->getMainUrl37();
        $route_name = 'get-deposits';
        $routeurl = $route . $route_name;
        $authorization = $this->loginToken();
        $headers = [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $authorization,
        ];

        $response = Http::withHeaders($headers)->get($routeurl);
        //dd($response);
        return json_decode($response)->data;
    }

    public function getBranchCode($branchid)
    {
        $bcode = DB::table('tbl_branches')
            ->where('id', $branchid)
            ->first();
        if ($bcode) {
            $bcode = $bcode->branch_code;
        } else {
            $bcode = '';
        }
        return $bcode;
    }

    public function index()
    {
        $crawl_deposits = DB::table('tbl_crawling')
            ->where('module_name', 'deposit')
            ->get();
        return view('crawling.crawl.deposit_crawl.index', compact('crawl_deposits'));
    }

    public function crawlDeposits()
    {
        $branchCount = DB::table('tbl_branches')->count();
        $sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;

        //$y == branchcode with loop
        // for ($x = 1; $x <= $branchcount; $x++) {
        $z = 0;
        for ($y = $sessionBranch; $y <= $sessionBranch; $y++) {
            $deposits = $this->crawDepositListwithAuthorization();
            // return response()->json($deposits);

            $branch_code = $this->getBranchCode($y);
            $branch_code = strtolower($branch_code);
            $table = $branch_code . '_deposit';
            //return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => ($deposits)]);
            for ($i = 0; $i < count($deposits); $i++) {
                if (
                    DB::connection('portal')
                    ->table('tbl_main_join_loan_deposit')
                    ->where('main_loan_deposit_id', $deposits[$i]->applicant_number_id)
                    ->where('branch_id', $y)
                    ->first()
                ) {
                    continue;
                }

                // Loan id null or not found
                $loan_id = DB::table('tbl_main_join_loan')
                    ->where('main_loan_id', $deposits[$i]->applicant_number_id)
                    ->where('branch_id', $y)
                    ->first();
                if ($loan_id) {
                    $loan_id = $loan_id->portal_loan_id;
                } else {
                    // return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => $deposits[$i]->applicant_number_id]);

                    continue;
                }

                // Client null or not found
                $client_id = DB::table('tbl_main_join_client')
                    ->where('main_client_id', $deposits[$i]->client_id)
                    ->first();
                if ($client_id) {
                    $client_id = $client_id->portal_client_id;
                } else {
                    continue;
                }


                $loan = DB::table($branch_code . '_loans')
                    ->where('loan_unique_id', $loan_id)
                    ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $branch_code . '_loans.client_id')
                    ->first();

                if ($loan) {
                } else {
                    continue;
                }

                $mainJoin = new Crawl_Loan_Deposit();
                $mainJoin->portal_loan_deposit_id = $loan->loan_unique_id;
                $mainJoin->main_loan_deposit_id = $deposits[$i]->applicant_number_id;
                $mainJoin->main_loan_deposit_client_id = $deposits[$i]->client_id;
                $mainJoin->branch_id = $y;
                $mainJoin->save();

                $data_deposit = DB::connection('portal')
                    ->table($table)
                    ->insert([
                        //'deposit_id' => $deposits[$i]->id,
                        // 'deposit_id' => $portal_loan_id,
                        'loan_id' => $loan_id,
                        'client_id' => $client_id,
                        'ref_no' => $deposits[$i]->referent_no,
                        'invoice_no' => $deposits[$i]->invoice_no,
                        // 'compulsory_saving' => $deposits[$i]->compulsory_saving_amount,
                        'acc_join_id' => $deposits[$i]->acc_code,
                        'total_deposit_balance' => $deposits[$i]->total_deposit,
                        'paid_deposit_balance' => $deposits[$i]->client_pay,
                        'note' => $deposits[$i]->note,
                        'deposit_pay_date' => $deposits[$i]->loan_deposit_date,
                        'created_at' => $deposits[$i]->created_at,
                        'updated_at' => $deposits[$i]->updated_at,
                    ]);

                if ($data_deposit == false) {
                    continue;
                }


                $charges_compulsory = DB::table('loan_charges_compulsory_join')
                    ->leftJoin($branch_code . '_loans', 'loan_charges_compulsory_join.loan_unique_id', '=', $branch_code . '_loans.loan_unique_id')
                    ->where($branch_code . '_loans.loan_unique_id', $loan_id)
                    ->select('loan_charges_compulsory_join.*')
                    ->get();

                foreach ($charges_compulsory as $c) {
                    DB::table('loan_charges_compulsory_join')
                        ->where('compulsory_code', $c->id)
                        ->update([
                            'status' => 'paid',
                        ]);
                }

                DB::table($branch_code . '_loans')
                    ->where('loan_unique_id', $loan_id)
                    ->update([
                        'disbursement_status' => 'Deposited',
                    ]);


                $z = ++$z;
            }

            // $verify = DB::connection('portal')
            //     ->table($branch_code . '_deposit')
            //     ->get();
        }
        DB::connection('portal')
            ->table('tbl_crawling')
            ->where('module_name', 'deposit')
            ->update([
                'data_count' => $z,
                'crawl_date' => date('Y-m-d H:i:s'),
            ]);
        if ($z) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $z]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
}
