<?php

namespace App\Http\Controllers\Crawl;

use App\Http\Controllers\Controller;
use App\Models\Crawl\Crawl;
use App\Models\Crawl\Crawl_Client;
use Illuminate\Support\Facades\DB;
use App\Models\Crawl\Crawl_Center;

class ClientCrawlController extends Controller
{
    public function index()
    {
        $crawl_clients = Crawl::where('module_name', 'client')->get();
        return view('crawling.crawl.client_crawl.index', compact('crawl_clients'));
    }

    //auto generate client uniquekey
    public function generateClientID($branchid)
    {
        $obj_branchcode = DB::table('tbl_branches')
            ->where('id', $branchid)
            ->first()->branch_code;
        if ($obj_branchcode) {
            $obj_fund = DB::table('tbl_client_basic_info')
                ->where('client_uniquekey', 'LIKE', '%' . $obj_branchcode . '%')
                ->get()
                ->last();

            if ($obj_fund != null) {
                // $subnum = substr($obj_fund->client_uniquekey, 6); // sub-string to 01-000001
                // $addnum = $subnum + 1; // add id in +1
                // $cusmclientidnum = (string) str_pad($addnum, 7, '0', STR_PAD_LEFT); // add generate in zero
                // $clientid = $obj_branchcode; // Branch Code
                // $clientid .= '-';
                // $clientid .= '01';
                // $clientid .= '-';
                // $clientid .= $cusmclientidnum; // Client ID in branch
                $clientid = ++$obj_fund->client_uniquekey;
                return $clientid;
            } else {
                $cusmclientidnum = '0000001'; // add generate in zero
                $clientid = $obj_branchcode;
                $clientid .= '-';
                $clientid .= '01';
                $clientid .= '-';
                $clientid .= $cusmclientidnum; // Client ID in branch

                return $clientid;
            }
        } else {
            return response()->json(['status_code' => 500, 'message' => 'client create fail', 'data' => null]);
        }
    }

    public function getCenterUniqueID($branchid)
    {
        $branchcode = DB::table('tbl_branches')
            ->where('id', $branchid)
            ->first()->branch_code;
        $branch = DB::table('tbl_branches')
            ->where('id', $branchid)
            ->first()->id;
        $idnum = DB::table('tbl_center')
            ->where('branch_id', $branch)
            ->get()
            ->count();
        $idnum = $idnum + 1;
        $idnum = str_pad($idnum, 4, 0, STR_PAD_LEFT);

        $uniquekey = $branchcode;
        $uniquekey .= '-05-';
        $uniquekey .= $idnum;

        return $uniquekey;
    }

    public function crawlClients()
    {
        // $this_branch_id = DB::table('tbl_staff')->where('staff_code', '=', Session::get('staff_code'))->select('branch_id')->first();

        $branchcount = DB::table('tbl_branches')->count();
        $sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;

        $z = 0;
        for ($y = $sessionBranch; $y <= $sessionBranch; $y++) {
            $clients = DB::connection('main')
                ->table('clients')
                ->where('branch_id', '=', $y)
                // ->where('customer_group_id','!=','0')
                // ->limit(2500)
                ->get();

            // return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $clients]);

            $mainJoinClients = DB::table('tbl_main_join_client')->get();
            //delete records
            for ($i = 0; $i < count($mainJoinClients); $i++) {
                if (
                    DB::connection('main')
                    ->table('clients')
                    ->where('client_number', $mainJoinClients[$i]->main_client_code)
                    ->first()
                ) {
                    continue;
                } else {
                    $clientPortalID = DB::table('tbl_main_join_client')
                        ->where('main_client_code', $mainJoinClients[$i]->main_client_code)
                        ->first()->portal_client_id;

                    DB::table('tbl_client_basic_info')
                        ->join('tbl_client_family_info', 'tbl_client_family_info.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                        ->join('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                        ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                        ->join('tbl_client_survey_ownership', 'tbl_client_survey_ownership.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                        ->join('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                        ->where('tbl_client_basic_info.client_uniquekey', '=', $clientPortalID)
                        ->delete();
                    DB::table('tbl_client_family_info')
                        ->where('tbl_client_family_info.client_uniquekey', '=', $clientPortalID)
                        ->delete();
                    DB::table('tbl_client_job_main')
                        ->where('tbl_client_job_main.client_uniquekey', '=', $clientPortalID)
                        ->delete();
                    DB::table('tbl_client_photo')
                        ->where('tbl_client_photo.client_uniquekey', '=', $clientPortalID)
                        ->delete();
                    DB::table('tbl_client_survey_ownership')
                        ->where('tbl_client_survey_ownership.client_uniquekey', '=', $clientPortalID)
                        ->delete();
                    DB::table('tbl_client_join')
                        ->where('tbl_client_join.client_id', '=', $clientPortalID)
                        ->delete();

                    //delete at main join
                    DB::table('tbl_main_join_client')
                        ->where('portal_client_id', $clientPortalID)
                        ->delete();
                }
            }

            //insert data to portal's table
            for ($i = 0; $i < count($clients); $i++) {
                if (
                    DB::table('tbl_main_join_client')
                    ->where('main_client_code', $clients[$i]->client_number)
                    ->first()
                ) {
                    $pClientID = DB::table('tbl_main_join_client')->where('main_client_code', $clients[$i]->client_number)->first()->portal_client_id;
                    if ($clients[$i]->updated_at != DB::table('tbl_client_basic_info')->where('client_uniquekey', $pClientID)->first()->updated_at) {
                        //update data
                        $clientPortalKey = DB::table('tbl_main_join_client')
                            ->where('main_client_code', $clients[$i]->client_number)
                            ->first()->portal_client_id;

                        $clientType = DB::connection('main')
                            ->table('customer_groups')
                            ->where('id', $clients[$i]->customer_group_id)
                            ->first();
                        if ($clientType) {
                            $clientType = $clientType->name;
                            if ($clientType == 'Active Member') {
                                $clientType = 'active';
                            } elseif ($clientType == 'Dead Member') {
                                $clientType = 'dead';
                            } elseif ($clientType == 'Drop Out Member') {
                                $clientType == 'dropout';
                            } elseif ($clientType == 'Rejoin Member') {
                                $clientType = 'rejoin';
                            } elseif ($clientType == 'Substitute Member') {
                                $clientType = 'substitute';
                            } elseif ($clientType == 'Dormant Member') {
                                $clientType = 'dormant';
                            } else {
                                $clientType = 'dormant';
                            }
                        } else {
                            $clientType = 'new';
                        }

                        if ($clients[$i]->nrc_type == 'Old Format') {
                            $nrc_old = $clients[$i]->nrc_number == null ? '' : $clients[$i]->nrc_number;
                            $nrc_new = '';
                        } else {
                            $nrc_old = '';
                            $nrc_new = $clients[$i]->nrc_number == null ? '' : $clients[$i]->nrc_number;
                        }
                        
                        // $main_province_name = DB::connection('main')->table('addresses')->where('code', $clients[$i]->province_id)->first();
                        // if ($main_province_name) {
                        //     $main_province_name = $main_province_name->name;
                        //     $portal_province_id = DB::connection('portal')->table('tbl_provinces')->where('province_name', $main_province_name)->first()->id;
                        // } else {
                        //     $portal_province_id = null;
                        // }

                        // $main_district_name = DB::connection('main')->table('addresses')->where('code', $clients[$i]->district_id)->first();
                        // if ($main_district_name) {
                        //     $main_district_name = $main_district_name->name;
                        //     $portal_district_id = DB::connection('portal')->table('tbl_districts')->where('district_name', $main_district_name)->first()->id;
                        // } else {
                        //     $portal_district_id = null;
                        // }

                        // $main_commune_name = DB::connection('main')->table('addresses')->where('code', $clients[$i]->commune_id)->first();
                        // if ($main_commune_name) {
                        //     $main_commune_name = $main_commune_name->name;
                        //     $portal_township_id = DB::connection('portal')->table('tbl_townships')->where('township_name', $main_commune_name)->first()->id;
                        // } else {
                        //     $portal_township_id = null;
                        // }

                        // $main_village_name = DB::connection('main')->table('addresses')->where('code', $clients[$i]->village_id)->first();
                        // if ($main_village_name) {
                        //     $main_village_name = $main_village_name->name;
                        //     $portal_village_id = DB::connection('portal')->table('tbl_villages')->where('village_name', $main_village_name)->first()->id;
                        // } else {
                        //     $portal_village_id = null;
                        // }

                        DB::connection('portal')
                            ->table('tbl_client_basic_info')
                            ->where('client_uniquekey', $clientPortalKey)
                            ->update([
                                'name' => $clients[$i]->name == null ? '' : $clients[$i]->name,
                                'name_mm' => $clients[$i]->name_other == null ? '' : $clients[$i]->name_other,
                                'dob' => $clients[$i]->dob == null ? '' : $clients[$i]->dob,
                                'nrc' => $nrc_new,
                                'old_nrc' => $nrc_old,
                                'gender' => $clients[$i]->gender == null ? NULL : $clients[$i]->gender,
                                'phone_primary' => $clients[$i]->primary_phone_number == null ? '' : $clients[$i]->primary_phone_number,
                                'phone_secondary' => $clients[$i]->alternate_phone_number == null ? '' : $clients[$i]->alternate_phone_number,
                                'education_id' => $clients[$i]->education == null ? '' : $clients[$i]->education,

                                'division_id' => $clients[$i]->province_id == null ? '' : $clients[$i]->province_id,
                                'district_id' => $clients[$i]->district_id == null ? '' : $clients[$i]->district_id,
                                'township_id' => $clients[$i]->commune_id == null ? '' : $clients[$i]->commune_id,
                                'village_id' => $clients[$i]->village_id == null ? '' : $clients[$i]->village_id,
                                'quarter_id' => $clients[$i]->ward_id == null ? '' : $clients[$i]->ward_id,

                                'address_primary' => $clients[$i]->address1 == null ? '' : $clients[$i]->address1,
                                'address_secondary' => $clients[$i]->address2 == null ? '' : $clients[$i]->address2,
                                'status' => 'none', //default
                                'client_type' => $clientType,
                                'created_at' => $clients[$i]->created_at,
                                'updated_at' => $clients[$i]->updated_at,
                            ]);

                        if (str_starts_with($clients[$i]->marital_status, 'S')) {
                            $marital_status = "Single";
                        } elseif (str_starts_with($clients[$i]->marital_status, 'M')) {
                            $marital_status = "Married";
                        } elseif (str_starts_with($clients[$i]->marital_status, 'D')) {
                            $marital_status = "Divorced";
                        } else {
                            $marital_status = "None";
                        }

                        DB::connection('portal')
                            ->table('tbl_client_family_info')
                            ->where('client_uniquekey', $clientPortalKey)
                            ->update([
                                'client_uniquekey' => $clients[$i]->id == null ? '' : $clientPortalKey,
                                'father_name' => $clients[$i]->father_name == null ? '' : $clients[$i]->father_name,
                                'marital_status' => $marital_status,
                                'spouse_name' => $clients[$i]->husband_name == null ? '' : $clients[$i]->husband_name,
                                'occupation_of_spouse' => $clients[$i]->occupation_of_husband == null ? '' : $clients[$i]->occupation_of_husband,
                                'no_of_family' => $clients[$i]->no_of_person_in_family == null ? '' : $clients[$i]->no_of_person_in_family,
                                'no_of_working_family' => $clients[$i]->no_of_working_people == null ? '' : $clients[$i]->no_of_working_people,
                                'no_of_children' => $clients[$i]->no_children_in_family == null ? '' : $clients[$i]->no_children_in_family,
                            ]);

                        //officer
                        $loan_officer_id = DB::table('tbl_main_join_staff')
                            ->where('main_staff_id', $clients[$i]->loan_officer_id)
                            ->first();
                        if ($loan_officer_id) {
                            $loan_officer_id = $loan_officer_id->portal_staff_id;
                        } else if ($loan_officer_id == 0) {
                            $loan_officer_id = 0;
                        } else {
                            $loan_officer_id = '';
                        }

                        // update center leader in client_join if client is center leader
                        $leader_center = DB::connection('main')
                            ->table('clients')
                            ->leftJoin('center_leaders', 'center_leaders.id', '=', 'clients.center_leader_id')
                            ->where('clients.client_number', $clients[$i]->client_number)
                            ->first()->code;
                        $portalClientID = DB::table('tbl_main_join_client')
                            ->where('main_client_code', $clients[$i]->client_number)
                            ->first()->portal_client_id;
                        $portalCenterID = DB::table('tbl_main_join_center')
                            ->where('main_center_code', $leader_center)
                            ->first()->portal_center_id;

                        if ($clients[$i]->you_are_a_center_leader == 'Yes') {
                            DB::table('tbl_client_join')
                                ->where('client_id', $portalClientID)
                                ->update([
                                    'you_are_a_center_leader' => 'yes',
                                ]);

                            DB::table('tbl_main_join_center')
                                ->where('portal_center_id', $portalCenterID)
                                ->update([
                                    'main_center_leader_id' => $clients[$i]->center_leader_id,
                                ]);
                        } else {
                            DB::table('tbl_client_join')
                                ->where('client_id', $portalClientID)
                                ->update([
                                    'you_are_a_center_leader' => 'no',
                                ]);
                        }

                        if ($clients[$i]->you_are_a_group_leader == 'Yes') {
                            // $group_leader_check = DB::connection('main')
                            //     ->table('group_loan_details')
                            //     ->where('client_id', '=', $clients[$i]->id)
                            //     ->first();
                            // if ($group_leader_check) {
                            //     $groups = DB::connection('main')
                            //         ->table('group_loan_details')
                            //         ->join('group_loans', function ($join) {
                            //             $join->on('group_loans.id', '=', 'group_loan_details.group_loan_id');
                            //         })
                            //         ->select('group_loans.*', 'group_loan_details.*')
                            //         ->where('group_loan_details.client_id', $clients[$i]->id)
                            //         ->first()->group_code;

                                // $portalGroupID = DB::table('tbl_main_join_group')->where('main_group_code', $groups)->first()->portal_group_id;
                                DB::table('tbl_client_join')
                                    ->where('client_id', $portalClientID)
                                    ->update([
                                        'you_are_a_group_leader' => 'yes',
                                    ]);
                            // }
                        } else {
                            DB::table('tbl_client_join')
                                ->where('client_id', $portalClientID)
                                ->update([
                                    'you_are_a_group_leader' => 'no',
                                ]);
                        }

                        //center
                        $center_id = DB::table('tbl_main_join_center')
                            ->where('main_center_id', $clients[$i]->center_leader_id)
                            ->first();
                        if ($center_id) {
                            $center_id = $center_id->portal_center_id;
                        } else if ($center_id == 0) {
                            $center_id = 0;
                        } else {
                            $center_id = '';
                        }

                        // group
                        $group_id = DB::table('tbl_main_join_group')
                            ->where('main_group_id', $clients[$i]->customer_group_id)
                            ->first();
                        if ($group_id) {
                            $group_id = $group_id->portal_group_id;
                        } else if ($group_id == 0) {
                            $group_id = 0;
                        } else {
                            $group_id = '';
                        }

                        DB::connection('portal')
                            ->table('tbl_client_join')
                            ->where('client_id', $clientPortalKey)
                            ->update([
                                'client_id' => $clients[$i]->id == null ? '' : $clientPortalKey,
                                'branch_id' => $clients[$i]->branch_id == null ? '' : $clients[$i]->branch_id,
                                'staff_id' => $clients[$i]->loan_officer_id == null ? '' : $loan_officer_id,
                                'group_id' => $clients[$i]->customer_group_id == null ? '' : $group_id,
                                'center_id' => $clients[$i]->center_leader_id == null ? '' : $center_id,
                                'client_status' => $clientType,
                                'registration_date' => $clients[$i]->register_date == null ? '' : $clients[$i]->register_date,
                            ]);

                        DB::connection('portal')
                            ->table('tbl_client_photo')
                            ->where('client_uniquekey', $clientPortalKey)
                            ->update([
                                'client_uniquekey' => $clients[$i]->id == null ? '' : $clientPortalKey,
                                'client_photo' => $clients[$i]->photo_of_client,
                                'registration_family_form_front' => $clients[$i]->family_registration_copy == null ? '' : $clients[$i]->family_registration_copy,
                                'finger_print_bio' => $clients[$i]->scan_finger_print == null ? '' : $clients[$i]->scan_finger_print,
                            ]);

                        DB::connection('portal')
                            ->table('tbl_client_survey_ownership')
                            ->where('client_uniquekey', $clientPortalKey)
                            ->update([
                                'client_uniquekey' => $clients[$i]->id == null ? '' : $clientPortalKey,
                            ]);

                        DB::connection('portal')
                            ->table('tbl_client_job_main')
                            ->where('client_uniquekey', $clientPortalKey)
                            ->update([
                                'client_uniquekey' => $clients[$i]->id == null ? '' : $clientPortalKey,
                                'industry_id' => 2,
                                'job_status' => 'Has Job',
                                'job_position_id' => 4,
                                'department_id' => 4,
                                'experience' => 1,
                                'experience2' => 1,
                                'salary' => null,
                                'company_name' => '',
                            ]);

                        //update end
                    }

                    continue;
                }
                if ($clients[$i]->branch_id != 0 && $clients[$i]->branch_id != null && $clients[$i]->id != null) {
                    //officer
                    $loan_officer_id = DB::table('tbl_main_join_staff')
                        ->where('main_staff_id', $clients[$i]->loan_officer_id)
                        ->first();
                    if ($loan_officer_id) {
                        $loan_officer_id = $loan_officer_id->portal_staff_id;
                    } else if ($clients[$i]->loan_officer_id == 0) {
                        $loan_officer_id = 0;
                    } else {
                        $loan_officer_id = '';
                        // continue;
                    }

                    //center
                    $center_id = DB::table('tbl_main_join_center')
                        ->where('main_center_id', $clients[$i]->center_leader_id)
                        ->first();
                    if ($center_id) {
                        $center_id = $center_id->portal_center_id;
                    } else if ($clients[$i]->center_leader_id = 0) {
                        $center_id = 0;
                    } else {
                        $center_id = '';
                        // continue;
                    }

                    // group
                    $group_id = DB::table('tbl_main_join_group')
                        ->where('main_group_id', $clients[$i]->customer_group_id)
                        ->first();
                    if ($group_id) {
                        $group_id = $group_id->portal_group_id;
                    } else if ($clients[$i]->customer_group_id == 0) {
                        $group_id = 0;
                    } else {
                        $group_id = '';
                        // continue;
                    }

                    $mainJoin = new Crawl_Client();
                    $mainJoin->portal_client_id = $clients[$i]->id == null ? null : $this->generateClientID($clients[$i]->branch_id);
                    $mainJoin->portal_branch_id = $clients[$i]->branch_id;
                    $mainJoin->main_client_id = $clients[$i]->id;
                    $mainJoin->main_client_code = $clients[$i]->client_number;
                    $mainJoin->save();

                    $clientType = DB::connection('main')
                        ->table('customer_groups')
                        ->where('id', $clients[$i]->customer_group_id)
                        ->first();
                    if ($clientType) {
                        $clientType = $clientType->name;
                        if ($clientType == 'Active Member') {
                            $clientType = 'active';
                        } elseif ($clientType == 'Dead Member') {
                            $clientType = 'dead';
                        } elseif ($clientType == 'Drop Out Member') {
                            $clientType == 'dropout';
                        } elseif ($clientType == 'Rejoin Member') {
                            $clientType = 'rejoin';
                        } elseif ($clientType == 'Substitute Member') {
                            $clientType = 'substitute';
                        } elseif ($clientType == 'Dormant Member') {
                            $clientType = 'dormant';
                        } else {
                            $clientType = 'dormant';
                        }
                    } else {
                        $clientType = 'new';
                    }


                    // $main_province_name = DB::connection('main')->table('addresses')->where('code', $clients[$i]->province_id)->first();
                    // if ($main_province_name) {
                    //     $main_province_name = $main_province_name->name;
                    //     $portal_province_id = DB::connection('portal')->table('tbl_provinces')->where('province_name', $main_province_name)->first()->id;
                    // } else {
                    //     $portal_province_id = null;
                    // }

                    // $main_district_name = DB::connection('main')->table('addresses')->where('code', $clients[$i]->district_id)->first();
                    // if ($main_district_name) {
                    //     $main_district_name = $main_district_name->name;
                    //     $portal_district_id = DB::connection('portal')->table('tbl_districts')->where('district_name', $main_district_name)->first()->id;
                    // } else {
                    //     $portal_district_id = null;
                    // }

                    // $main_commune_name = DB::connection('main')->table('addresses')->where('code', $clients[$i]->commune_id)->first();
                    // if ($main_commune_name) {
                    //     $main_commune_name = $main_commune_name->name;
                    //     $portal_township_id = DB::connection('portal')->table('tbl_townships')->where('township_name', $main_commune_name)->first()->id;
                    // } else {
                    //     $portal_township_id = null;
                    // }

                    // $main_village_name = DB::connection('main')->table('addresses')->where('code', $clients[$i]->village_id)->first();
                    // if ($main_village_name) {
                    //     $main_village_name = $main_village_name->name;
                    //     $portal_village_id = DB::connection('portal')->table('tbl_villages')->where('village_name', $main_village_name)->first()->id;
                    // } else {
                    //     $portal_village_id = null;
                    // }

                    if ($clients[$i]->nrc_type == 'Old Format') {
                        DB::connection('portal')
                            ->table('tbl_client_basic_info')
                            ->insert([
                                'client_uniquekey' => $clients[$i]->id == null ? '' : $mainJoin->portal_client_id,
                                'name' => $clients[$i]->name == null ? '' : $clients[$i]->name,
                                'name_mm' => $clients[$i]->name_other == null ? '' : $clients[$i]->name_other,
                                'dob' => $clients[$i]->dob == null ? '' : $clients[$i]->dob,
                                'old_nrc' => $clients[$i]->nrc_number == null ? '' : $clients[$i]->nrc_number,
                                'gender' => $clients[$i]->gender == null ? NULL : $clients[$i]->gender,
                                'phone_primary' => $clients[$i]->primary_phone_number == null ? '' : $clients[$i]->primary_phone_number,
                                'phone_secondary' => $clients[$i]->alternate_phone_number == null ? '' : $clients[$i]->alternate_phone_number,
                                'education_id' => $clients[$i]->education == null ? '' : $clients[$i]->education,
                                'division_id' => $clients[$i]->province_id == null ? '' : $clients[$i]->province_id,
                                'district_id' => $clients[$i]->district_id == null ? '' : $clients[$i]->district_id,
                                'township_id' => $clients[$i]->commune_id == null ? '' : $clients[$i]->commune_id,
                                'village_id' => $clients[$i]->village_id == null ? '' : $clients[$i]->village_id,
                                'quarter_id' => $clients[$i]->ward_id == null ? '' : $clients[$i]->ward_id,
                                'address_primary' => $clients[$i]->address1 == null ? '' : $clients[$i]->address1,
                                'address_secondary' => $clients[$i]->address2 == null ? '' : $clients[$i]->address2,
                                'status' => 'none', //default
                                'client_type' => $clientType,
                                'created_at' => $clients[$i]->created_at,
                                'updated_at' => $clients[$i]->updated_at,
                            ]);
                    } elseif ($clients[$i]->nrc_type == 'New Format') {
                        DB::connection('portal')
                            ->table('tbl_client_basic_info')
                            ->insert([
                                'client_uniquekey' => $clients[$i]->id == null ? '' : $mainJoin->portal_client_id,
                                'name' => $clients[$i]->name == null ? '' : $clients[$i]->name,
                                'name_mm' => $clients[$i]->name_other == null ? '' : $clients[$i]->name_other,
                                'dob' => $clients[$i]->dob == null ? '' : $clients[$i]->dob,
                                'nrc' => $clients[$i]->nrc_number == null ? '' : $clients[$i]->nrc_number,
                                'gender' => $clients[$i]->gender == null ? NULL : $clients[$i]->gender,
                                'phone_primary' => $clients[$i]->primary_phone_number == null ? '' : $clients[$i]->primary_phone_number,
                                'phone_secondary' => $clients[$i]->alternate_phone_number == null ? '' : $clients[$i]->alternate_phone_number,
                                'education_id' => $clients[$i]->education == null ? '' : $clients[$i]->education,
                                'division_id' => $clients[$i]->province_id == null ? '' : $clients[$i]->province_id,
                                'district_id' => $clients[$i]->district_id == null ? '' : $clients[$i]->district_id,
                                'township_id' => $clients[$i]->commune_id == null ? '' : $clients[$i]->commune_id,
                                'village_id' => $clients[$i]->village_id == null ? '' : $clients[$i]->village_id,
                                'quarter_id' => $clients[$i]->ward_id == null ? '' : $clients[$i]->ward_id,
                                'address_primary' => $clients[$i]->address1 == null ? '' : $clients[$i]->address1,
                                'address_secondary' => $clients[$i]->address2 == null ? '' : $clients[$i]->address2,
                                'status' => 'none',
                                'client_type' => $clientType,
                                'created_at' => $clients[$i]->created_at,
                                'updated_at' => $clients[$i]->updated_at,
                            ]);
                    }
                    if (str_starts_with($clients[$i]->marital_status, 'S')) {
                        $marital_status = "Single";
                    } elseif (str_starts_with($clients[$i]->marital_status, 'M')) {
                        $marital_status = "Married";
                    } elseif (str_starts_with($clients[$i]->marital_status, 'D')) {
                        $marital_status = "Divorced";
                    } else {
                        $marital_status = "None";
                    }

                    DB::connection('portal')
                        ->table('tbl_client_family_info')
                        ->insert([
                            'client_uniquekey' => $clients[$i]->id == null ? '' : $mainJoin->portal_client_id,
                            'father_name' => $clients[$i]->father_name == null ? '' : $clients[$i]->father_name,
                            'marital_status' => $marital_status,
                            'spouse_name' => $clients[$i]->husband_name == null ? '' : $clients[$i]->husband_name,
                            'occupation_of_spouse' => $clients[$i]->occupation_of_husband == null ? '' : $clients[$i]->occupation_of_husband,
                            'no_of_family' => $clients[$i]->no_of_person_in_family == null ? '' : $clients[$i]->no_of_person_in_family,
                            'no_of_working_family' => $clients[$i]->no_of_working_people == null ? '' : $clients[$i]->no_of_working_people,
                            'no_of_children' => $clients[$i]->no_children_in_family == null ? '' : $clients[$i]->no_children_in_family,
                        ]);

                    DB::connection('portal')
                        ->table('tbl_client_join')
                        ->insert([
                            'client_id' => $clients[$i]->id == null ? '' : $mainJoin->portal_client_id,
                            'branch_id' => $clients[$i]->branch_id == null ? '' : $clients[$i]->branch_id,
                            'staff_id' => $clients[$i]->loan_officer_id == null ? '' : $loan_officer_id,
                            'group_id' => $clients[$i]->customer_group_id == null ? '' : $group_id,
                            'center_id' => $clients[$i]->center_leader_id == null ? '' : $center_id,
                            'you_are_a_center_leader' => $clients[$i]->you_are_a_center_leader,
                            'you_are_a_group_leader' => $clients[$i]->you_are_a_group_leader,
                            'client_status' => $clientType,
                            'registration_date' => $clients[$i]->register_date == null ? '' : $clients[$i]->register_date,
                        ]);

                    DB::connection('portal')
                        ->table('tbl_client_photo')
                        ->insert([
                            'client_uniquekey' => $clients[$i]->id == null ? '' : $mainJoin->portal_client_id,
                            'client_photo' => $clients[$i]->photo_of_client,
                            'registration_family_form_front' => $clients[$i]->family_registration_copy == null ? '' : $clients[$i]->family_registration_copy,
                            'finger_print_bio' => $clients[$i]->scan_finger_print == null ? '' : $clients[$i]->scan_finger_print,
                        ]);

                    DB::connection('portal')
                        ->table('tbl_client_survey_ownership')
                        ->insert([
                            'client_uniquekey' => $clients[$i]->id == null ? '' : $mainJoin->portal_client_id,
                        ]);

                    DB::connection('portal')
                        ->table('tbl_client_job_main')
                        ->insert([
                            'client_uniquekey' => $clients[$i]->id == null ? '' : $mainJoin->portal_client_id,
                            'industry_id' => 2,
                            'job_status' => 'Has Job',
                            'job_position_id' => 4,
                            'department_id' => 4,
                            'experience' => 1,
                            'experience2' => 1,
                            'salary' => null,
                            'company_name' => '',
                        ]);
                }
                $z = ++$z;
            }
        }

        $entrey = DB::connection('portal')
            ->table('tbl_client_basic_info')
            ->get();
        DB::connection('portal')
            ->table('tbl_crawling')
            ->where('module_name', 'client')
            ->update([
                'data_count' => $z,
                'crawl_date' => date('Y-m-d H:i:s'),
            ]);

        if ($z) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $z]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
}
