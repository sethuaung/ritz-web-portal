<?php

namespace App\Http\Controllers\Crawl;

use App\Http\Controllers\Controller;
use App\Models\Crawl\Crawl_Loan_Disbursement;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class DisbursementCrawlController
{
    public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }

    public function crawDisbursementListwithAuthorization($branchid)
    {
        $route = $this->getMainUrl37();
        $route_name = 'get-disbursements';
        $routeurl = $route . $route_name . '?branch_id=' . $branchid;
        $authorization = $this->loginToken();
        $headers = [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $authorization,
        ];

        $response = Http::withHeaders($headers)->get($routeurl);
        //dd($response);
        return json_decode($response)->data;
    }
    public function getBranchCode($branchid)
    {
        $bcode = DB::table('tbl_branches')->where('id', $branchid)->first();
        if ($bcode) {
            $bcode = $bcode->branch_code;
        } else {
            $bcode = '';
        }
        return $bcode;
    }

    public function index()
    {
        $crawl_disbursements = DB::table('tbl_crawling')->where('module_name', 'disbursement')->get();
        return view('crawling.crawl.disbursement_crawl.index', compact('crawl_disbursements'));
    }

    public function crawlDisbursements()
    {
        $branchCount = DB::table('tbl_branches')->count();
        $sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;
        $branch_id = $sessionBranch;

        //$y == branchcode with loop
        // for ($x = 1; $x <= $branchcount; $x++) {
        $z = 0;
        for ($y = $sessionBranch; $y <= $sessionBranch; $y++) {
            $disbursements = $this->crawDisbursementListwithAuthorization($y);
            //             return response()->json($disbursements);

            $branch_code = $this->getBranchCode($y);
            $branch_code = strtolower($branch_code);
            $table = $branch_code . '_disbursement';
            // return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $disbursements]);
            // return response()->json(explode(" ", DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('id', 1583)->first()->date_s)[0]);
            // return response()->json(date('Y-m-d', strtotime('08-Feb-2023')));
            for ($i = 0; $i < count($disbursements); $i++) {
                if (DB::connection('portal')->table('tbl_main_join_loan_disbursement')->where('main_loan_disbursement_id', $disbursements[$i]->contract_id)->where('branch_id', $y)->first()) {
                    continue;
                }

                // Loan id null or not found
                $loan_id = DB::table('tbl_main_join_loan')->where('main_loan_id', $disbursements[$i]->contract_id)->where('branch_id', $y)->first();
                if ($loan_id) {
                    $loan_id = $loan_id->portal_loan_id;
                } else {
                    // $loan_id = '';
                    continue;
                }

                // Client null or not found
                $client_id = DB::table('tbl_main_join_client')->where('main_client_id', $disbursements[$i]->client_id)->first();
                if ($client_id) {
                    $client_id = $client_id->portal_client_id;
                } else {
                    $client_id = '';
                    //continue;
                }

                // center
                $centerid = DB::table('tbl_main_join_center')->where('main_center_id', $disbursements[$i]->center_code_id)->first();
                if ($centerid) {
                    $centerid = $centerid->portal_center_id;
                } else {
                    $centerid = '';
                }
                // group
                $groupid = DB::table('tbl_main_join_group')->where('main_group_id', $disbursements[$i]->center_code_id)->first();
                if ($groupid) {
                    $groupid = $groupid->portal_group_id;
                } else {
                    $groupid = '';
                }

                // staff
                $staffid = DB::table('tbl_main_join_staff')->where('main_staff_id', $disbursements[$i]->loan_officer_id)->first();
                if ($staffid) {
                    $staffid = $staffid->portal_staff_id;
                } else {
                    $staffid = '';
                }
                //return response()->json(['status_code' => 200, 'message' => 'success', 'data' => count($client_id)]);
                $mainJoin = new Crawl_Loan_Disbursement();
                $mainJoin->portal_loan_disbursement_id = $loan_id;
                $mainJoin->main_loan_disbursement_id = $disbursements[$i]->contract_id;
                $mainJoin->main_loan_disbursement_client_id = $disbursements[$i]->client_id;
                $mainJoin->branch_id = $y;
                $mainJoin->save();

                DB::connection('portal')->table($table)
                    ->insert([
                        //'disbursement_id'=>$disbursements[$i]->id,
                        'loan_id' => $loan_id,
                        'client_id' => $client_id,
                        'center_id' => $centerid,
                        'group_id' => $groupid,
                        'branch_id' => $disbursements[$i]->branch_id,
                        'loan_officer_id' => $staffid,
                        'loan_product_id' => $disbursements[$i]->loan_production_id,
                        'cash_acc_id' => $disbursements[$i]->acc_code,
                        'disbursed_amount' => $disbursements[$i]->disburse_amount,
                        'principal' => $disbursements[$i]->principle_receivable,
                        'interest' => $disbursements[$i]->interest_rate,
                        //'penalty'=>$disbursements[$i]->updated_at,
                        //'total_balance'=>$disbursements[$i]->updated_at,
                        'paid_deposit_balance' => $disbursements[$i]->deposit,
                        'paid_by' => 'cash',
                        'cash_by' => $disbursements[$i]->cash_pay,
                        'remark' => $disbursements[$i]->remark,
                        'disbursement_status' => $disbursements[$i]->disbursement_status,
                        'first_installment_date' => $disbursements[$i]->first_installment_date,
                        //'processing_date'=>$disbursements[$i]->updated_at,
                        'created_at' => $disbursements[$i]->updated_at,
                        'updated_at' => $disbursements[$i]->created_at,
                    ]);

                DB::table($branch_code . '_loans')->where('loan_unique_id', $loan_id)->update(['disbursement_status' => 'Activated']);
                // }

                $portal_disbursement_id = DB::connection('portal')->table('tbl_main_join_loan')->where('main_loan_id', $disbursements[$i]->contract_id)->first()->portal_loan_id;
                $LoansSchedule = DB::connection('portal')->table($branch_code . '_loans_schedule')->where('loan_unique_id', $portal_disbursement_id)->get();
                $RpDetail = DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $disbursements[$i]->contract_id)->get();
                $firstMainScheduleID = DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_id', $portal_disbursement_id)->first()->main_disbursement_schedule_id;

                for ($g = 0; $g < count($LoansSchedule); $g++) {
                    $exploded = explode(" ", $RpDetail[0]->date_s);
                    if (($exploded[0] != date('Y-m-d', strtotime($LoansSchedule[0]->month))) || (($RpDetail[0]->balance_s + $RpDetail[0]->principal_p) != ($LoansSchedule[0]->capital + $LoansSchedule[0]->final_amount))) {
                        DB::table($branch_code . '_loans_schedule')->where('id', $LoansSchedule[$g]->id)->update([
                            'month' => date_format(date_create($RpDetail[$g]->date_s), "d-M-Y"),
                            'capital' => $RpDetail[$g]->principal_s,
                            'interest' => $RpDetail[$g]->interest_s,
                            'final_amount' => $RpDetail[$g]->balance_s,
                        ]);
                    }

                    if ((count($RpDetail) == count($LoansSchedule)) && ($firstMainScheduleID != $RpDetail[0]->id)) {
                        DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$g]->id)->update([
                            'portal_disbursement_id' => $LoansSchedule[$g]->loan_unique_id,
                            'portal_disbursement_schedule_id' => $LoansSchedule[$g]->id,
                            'main_disbursement_schedule_id' => $RpDetail[$g]->id,
                            'branch_id' => $branch_id
                        ]);
                    }
                }

                $z = ++$z;
            }

            $verify = DB::connection('portal')->table($branch_code . '_disbursement')->get();
            DB::connection('portal')->table('tbl_crawling')->where('module_name', 'disbursement')->update([
                'data_count' => $z,
                'crawl_date' => date('Y-m-d H:i:s')
            ]);
        }
        if ($z) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $z]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
}
