<?php

namespace App\Http\Controllers\Crawl;

use App\Http\Controllers\Controller;
use App\Models\MainJoin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CrawlingController extends Controller
{
   
    public function crawlStaffs()
    {
        //required fields: id, staff_client_id, branch_id, type_status
        $staffs = DB::connection('main')->table('users')
        ->leftjoin('user_branches','users.id','=','user_branches.user_id')
        ->select('users.*', 'users.id as user_id', 'user_branches.branch_id as branchid') 
        ->get();
        // return response()->json($staffs);

        //insert data to portal's table
        for($i = 0; $i < count($staffs); $i++) {
            if($staffs[$i]->branchid != null) {
                if(DB::connection('portal')->table('main_join')->where('main_staff_id', $staffs[$i]->user_id)->first()) {
                    continue;
                }

                $mainJoin = new MainJoin();
                $mainJoin->portal_staff_id = $this->generateStaffUniquekey($staffs[$i]->branchid);
                $mainJoin->main_staff_id = $staffs[$i]->user_id;
                $mainJoin->save();
            
                $role_id = DB::connection('main')->table('model_has_roles')->where('model_id', $staffs[$i]->user_id)->first()->role_id;
                // return response()->json($role_id);

                DB::connection('portal')->table('tbl_staff')->insert([
                    'name' => $staffs[$i]->name,
                    'staff_code' => $mainJoin->portal_staff_id,
                    'phone' => $staffs[$i]->phone,
                    'father_name' => '',
                    'nrc' => '',
                    'full_address' => '',
                    'email' => $staffs[$i]->email,
                    'user_acc' => $staffs[$i]->name,
                    'user_password' => $staffs[$i]->password,
                    'photo' => $staffs[$i]->photo,
                    'branch_id' => $staffs[$i]->branchid,
                    'center_leader_id' => $staffs[$i]->id,
                	'role_id' => $role_id,
                    'user_token' => 'smth'
                ]);
            }
        }

        $entrey = DB::connection('portal')->table('tbl_staff')->get();

        DB::connection('portal')->table('tbl_crawling')->where('module_name', 'staff')->update([
            'data_count' => count($entrey),
            // 'crawl_date' => date('d-M-Y', Carbon::now())
        ]);

        if ($entrey) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => count($entrey)]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
}
