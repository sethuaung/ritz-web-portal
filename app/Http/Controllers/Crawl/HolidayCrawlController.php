<?php

namespace App\Http\Controllers\Crawl;

use App\Http\Controllers\Controller;
use App\Models\Crawl\Crawl;
use App\Models\Crawl\Crawl_Client;
use Illuminate\Support\Facades\DB;
use App\Models\Crawl\Crawl_Center;
use DateInterval;
use DatePeriod;
use DateTime;

class HolidayCrawlController extends Controller
{
    public function crawlHolidays()
    {
        $holidays = DB::connection('main')->table('holiday_schedules')->get();

        // return response()->json($holidays);
        $z = 0;
        foreach($holidays as $holiday) {

            $start = new DateTime($holiday->start_date);
            $end = new DateTime($holiday->end_date);

            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($start, $interval, $end);

            $result = array();
            array_push($result, date("Y-m-d", strtotime($holiday->start_date)));
            foreach ($period as $dt) {
                array_push($result, $dt->format("Y-m-d"));
            }
            // return response()->json($result);

            $bcode = strtolower(DB::table('tbl_branches')->where('id', $holiday->branch_id)->first()->branch_code);

            foreach ($result as $r) {
                $formatedMonth = date('d-M-Y', strtotime($r));
                DB::table($bcode.'_loans_schedule')->where('month', $formatedMonth)->update([
                    'month' => date('d-M-Y', strtotime($holiday->change_date)),
                    'updated_at' => now()
                ]);

                ++$z;
            }

        } 

        if($z) {
            DB::table('tbl_crawling')->where('module_name', 'crawl_holiday')->update([
                'data_count' => $z,
                'crawl_date' => now()
            ]);

            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $z]);
        } else {
            return response()->json(['status_code' => 404, 'message' => 'not found', 'data' => 'Encounter error']);
        }

    }
}
