<?php

namespace App\Http\Controllers\Crawl;

use App\Http\Controllers\Controller;
use App\Models\Crawl\Crawl;
use App\Models\Loans\Loan;
use App\Models\Loans\LoanCycle;
use App\Models\Loans\LoanJoin;
use App\Models\Loans\LoanSchedule;
use App\Models\MainJoin;
use Illuminate\Http\Request;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Loan\LoanController;
use App\Models\Crawl\Crawl_Loan;
use App\Models\Crawl\Crawl_Loan_Repayment_Schedule;

class LoanCrawlController extends Controller
{
    public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json',
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456',
        ]);
        // return response()->json(json_decode($response));
        return json_decode($response)->data[0]->token;
    }

    public function crawLoanListwithAuthorization($branchid)
    {
        $route = $this->getMainUrl37();

        $route_name = 'get-loans';
        $routeurl = $route . $route_name . '?branch_id=' . $branchid;
        $authorization = $this->loginToken();
        $headers = [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . $authorization,
        ];
        $response = Http::withHeaders($headers)->get($routeurl);

        return json_decode($response)->data;
    }

    public function getBranchCode($branchid)
    {
        $bcode = DB::table('tbl_branches')
            ->where('id', $branchid)
            ->first()->branch_code;
        return $bcode;
    }

    public function generateLoanUniquekey($branchid)
    {
        $bcode = DB::table('tbl_branches')
            ->where('id', $branchid)
            ->first()->branch_code;
        $bcode = strtolower($bcode);
        $idnum = DB::table($bcode . '_loans')
            ->get()
            ->count();
        $idnum = $idnum + 1;
        $idnum = str_pad($idnum, 9, 0, STR_PAD_LEFT);

        $idkey = strtoupper($bcode);
        $idkey .= '-03-';
        $idkey .= $idnum;

        return $idkey;
    }

    public function index()
    {
        $crawl_loans = Crawl::where('module_name', 'loan')->Paginate(10);
        return view('crawling.crawl.loan_crawl.index', compact('crawl_loans'));
    }

    public function crawlLoans()
    {
        $branchCount = DB::table('tbl_branches')->count();
        $sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;

        //loop with branch count
        // for ($x = 1; $x <= $branchcount; $x++) {
        $z = 0;
        for ($x = $sessionBranch; $x <= $sessionBranch; $x++) {
            $loans = DB::connection('main')->table('loans_' . $sessionBranch)->get();
            // $loans = $this->crawLoanListwithAuthorization($x);
            // return response()->json($loans);

            $bcode = $this->getBranchCode($x);
            $bcode = strtolower($bcode);

            for ($i = 0; $i < count($loans); $i++) {
                if (
                    DB::connection('portal')
                    ->table('tbl_main_join_loan')
                    ->where('main_loan_id', $loans[$i]->id)
                    ->where('branch_id', $x)
                    ->first()
                ) {
                    //update here

                    if ($loans[$i]->disbursement_status == 'Pending') {
                        $portalLID = DB::table('tbl_main_join_loan')->where('main_loan_id', $loans[$i]->id)->where('branch_id', $x)->first()->portal_loan_id;
                        $portalCheckUpdate = DB::table($bcode . '_loans')->where('loan_unique_id', $portalLID)->first()->updated_at;
                        $mainCheckUpdate = DB::connection('main')->table('loans_' . $x)->where('id', $loans[$i]->id)->first()->updated_at;
                        if ($portalCheckUpdate != $mainCheckUpdate) {
                            $loanPortalKey = DB::table('tbl_main_join_loan')
                                ->where('main_loan_id', $loans[$i]->id)
                                ->where('branch_id', $x)
                                ->first()->portal_loan_id;
                            // group
                            $groupid = DB::table('tbl_main_join_group')
                                ->where('main_group_id', $loans[$i]->group_loan_id)
                                ->first();
                            if ($groupid) {
                                $groupid = $groupid->portal_group_id;
                            } else if ($loans[$i]->group_loan_id == 0) {
                                $groupid = 0;
                            } else {
                                $groupid = '';
                            }
                            // client
                            $clientid = DB::table('tbl_main_join_client')
                                ->where('main_client_id', $loans[$i]->client_id)
                                ->first();
                            if ($clientid) {
                                $clientid = $clientid->portal_client_id;
                            } else {
                                $clientid = '';
                            }
                            // center
                            $centerid = DB::table('tbl_main_join_center')
                                ->where('main_center_id', $loans[$i]->center_code_id)
                                ->first();
                            if ($centerid) {
                                $centerid = $centerid->portal_center_id;
                            } else if ($loans[$i]->center_code_id == 0) {
                                $centerid = 0;
                            } else {
                                $centerid = '';
                            }
                            // staff
                            $staffid = DB::table('tbl_main_join_staff')
                                ->where('main_staff_id', $loans[$i]->loan_officer_id)
                                ->first();
                            if ($staffid) {
                                $staffid = $staffid->portal_staff_id;
                            } else if ($loans[$i]->loan_officer_id == 0) {
                                $staffid = 0;
                            } else {
                                $staffid = '';
                            }
                            // Guarantor_a
                            $guarantor_a = DB::table('tbl_main_join_guarantor')
                                ->where('main_guarantor_id', $loans[$i]->guarantor_id)
                                ->first();
                            if ($guarantor_a) {
                                $guarantor_a = $guarantor_a->portal_guarantor_id;
                            } else {
                                $guarantor_a = '';
                            }
                            // Guarantor_b
                            $guarantor_b = DB::table('tbl_main_join_guarantor')
                                ->where('main_guarantor_id', $loans[$i]->guarantor2_id)
                                ->first();
                            if ($guarantor_b) {
                                $guarantor_b = $guarantor_b->portal_guarantor_id;
                            } else {
                                $guarantor_b = NULL;
                            }

                            // Guarantor_c
                            $guarantor_c = DB::table('tbl_main_join_guarantor')
                                ->where('main_guarantor_id', $loans[$i]->guarantor3_id)
                                ->first();
                            if ($guarantor_c) {
                                $guarantor_c = $guarantor_c->portal_guarantor_id;
                            } else {
                                $guarantor_c = NULL;
                            }

                            $data_loan = DB::connection('portal')
                                ->table($bcode . '_loans')
                                ->where('loan_unique_id', $loanPortalKey)
                                ->update([
                                    //'process_join_id'
                                    'branch_id' => $loans[$i]->branch_id,
                                    'group_id' => $groupid, // change later
                                    'center_id' => $centerid,
                                    'loan_officer_id' => $staffid,
                                    'client_id' => $clientid,
                                    'guarantor_a' => $guarantor_a,
                                    'guarantor_b' => $guarantor_b,
                                    'guarantor_c' => $guarantor_c,
                                    'loan_type_id' => $loans[$i]->loan_production_id,
                                    'loan_amount' => $loans[$i]->loan_amount,
                                    // 'estimate_receivable_amount'=> $loans[$i]->principle_receivable,
                                    'loan_term_value' => $loans[$i]->loan_term_value,
                                    'loan_term' => $loans[$i]->loan_term,
                                    'interest_rate_period' => $loans[$i]->interest_rate_period,
                                    'disbursement_status' => 'Pending',
                                    'interest_method' => $loans[$i]->interest_method,
                                    'remark' => $loans[$i]->remark,
                                    //'status_del'
                                    'loan_application_date' => $loans[$i]->loan_application_date,
                                    'first_installment_date' => $loans[$i]->first_installment_date,
                                    // 'approve_date'=> $loans[$i]->status_note_date_approve,
                                    'disbursement_date' => $loans[$i]->loan_application_date,
                                    'cancel_date' => $loans[$i]->cancel_date,
                                    'active_at' => $loans[$i]->status_note_date_activated == NULL ? date('Y-m-d') : $loans[$i]->status_note_date_activated,
                                    'created_at' => $loans[$i]->created_at,
                                    'updated_at' => $loans[$i]->updated_at,
                                    //'business_category_id'
                                ]);

                            //return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $data_loan ]);
                            $principal_formula = DB::table('loan_type')
                                ->where('id', $loans[$i]->loan_production_id)
                                ->first()->principal_formula;

                            // Repayment date
                            if ($loans[$i]->loan_term == 'Day') {
                                $loan_term = 1;
                            } elseif ($loans[$i]->loan_term == 'Week') {
                                $loan_term = 7;
                            } elseif ($loans[$i]->loan_term == 'Two-Weeks') {
                                $loan_term = 14;
                            } elseif ($loans[$i]->loan_term == 'Four-Weeks') {
                                $loan_term = 28;
                            } elseif ($loans[$i]->loan_term == 'Month') {
                                $loan_term = 30;
                            } elseif ($loans[$i]->loan_term == 'Year') {
                                $loan_term = 30;
                            }

                            if ($loans[$i]->loan_production_id == 1 or $loans[$i]->loan_production_id == 2) {
                                // GeneralWeekly14D
                                $loanschedule = LoanController::fixedLoanMethod(
                                    $loans[$i]->loan_amount,
                                    $loan_term,
                                    $loans[$i]->loan_term_value,
                                    $loans[$i]->interest_rate,
                                    $principal_formula,
                                    // $start_date);
                                    $loans[$i]->loan_application_date,
                                );
                            } elseif ($loans[$i]->loan_production_id == 3 or $loans[$i]->loan_production_id == 4) {
                                // ExtraLoanWeekly
                                $loanschedule = LoanController::principalFinalMethod30D(
                                    $loans[$i]->loan_amount,
                                    $loan_term,
                                    $loans[$i]->loan_term_value,
                                    $loans[$i]->interest_rate,
                                    $principal_formula,
                                    // $start_date);
                                    $loans[$i]->loan_application_date,
                                );
                            } elseif ($loans[$i]->loan_production_id > 4 && $loans[$i]->loan_production_id < 20) {
                                // 5 to 19
                                // ExtraLoanWeekly
                                $loanschedule = LoanController::fixedPrincipalMethod30D(
                                    $loans[$i]->loan_amount,
                                    $loan_term,
                                    $loans[$i]->loan_term_value,
                                    $loans[$i]->interest_rate,
                                    $principal_formula,
                                    // $start_date);
                                    $loans[$i]->loan_application_date,
                                );
                            } elseif ($loans[$i]->loan_production_id > 19 && $loans[$i]->loan_production_id < 23) {
                                // ExtraLoanWeekly
                                $loanschedule = LoanController::principalFinalMethod28D(
                                    $loans[$i]->loan_amount,
                                    $loan_term,
                                    $loans[$i]->loan_term_value,
                                    $loans[$i]->interest_rate,
                                    $principal_formula,
                                    // $start_date);
                                    $loans[$i]->loan_application_date,
                                );
                            } elseif ($loans[$i]->loan_production_id > 22 && $loans[$i]->loan_production_id < 26) {
                                // GeneralMonthly28D
                                $loanschedule = LoanController::fixedPrincipalMethod28D(
                                    $loans[$i]->loan_amount,
                                    $loan_term,
                                    $loans[$i]->loan_term_value,
                                    $loans[$i]->interest_rate,
                                    $principal_formula,
                                    // $start_date);
                                    $loans[$i]->loan_application_date,
                                );
                            } elseif ($loans[$i]->loan_production_id == 26 or $loans[$i]->loan_production_id == 27) {
                                // ExtraLoanWeekly
                                $loanschedule = LoanController::fixedPrincipalMethod30D(
                                    $loans[$i]->loan_amount,
                                    $loan_term,
                                    $loans[$i]->loan_term_value,
                                    $loans[$i]->interest_rate,
                                    $principal_formula,
                                    // $start_date);
                                    $loans[$i]->loan_application_date,
                                );
                            }

                            $loan_amount = $loans[$i]->loan_amount;

                            DB::table($bcode . '_loans_schedule')
                                ->where('loan_unique_id', $loanPortalKey)
                                ->delete();

                            foreach ($loanschedule as $schedule) {
                                $entrydate = $schedule['date'];
                                // $total = $schedule->repay_interest + $schedule->repay_principal;
                                $loan_amount -= $schedule['repay_principal'];

                                $loan_schedule = new LoanSchedule();
                                $loan_schedule->setTable($bcode . '_loans_schedule');

                                $loan_schedule->loan_unique_id = $loanPortalKey;
                                $loan_schedule->loan_type_id = $loans[$i]->loan_production_id;
                                $loan_schedule->month = $entrydate;
                                $loan_schedule->capital = $schedule['repay_principal'];
                                $loan_schedule->interest = $schedule['repay_interest'];
                                $loan_schedule->discount = 0;
                                $loan_schedule->refund_interest = 1;
                                $loan_schedule->refund_amount = 1;
                                $loan_schedule->final_amount = $loan_amount;
                                $loan_schedule->status = 'none';
                                $loan_schedule->save();
                            }

                            //add to schedule main join
                            $LoansSchedule = DB::connection('portal')->table($bcode . '_loans_schedule')->where('loan_unique_id', $portalLID)->get();
                            $RpDetail = DB::connection('main')->table('loan_disbursement_calculate_' . $x)->where('disbursement_id', $loans[$i]->id)->get();

                            for ($t = 0; $t < count($LoansSchedule); $t++) {
                                if (DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$t]->id)->where('branch_id', $x)->first()) {
                                    DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$t]->id)->where('branch_id', $x)->delete();
                                } else {
                                    if ($RpDetail[$t]->no == ($t + 1)) {
                                        $mainJoinSchedule = new Crawl_Loan_Repayment_Schedule();
                                        $mainJoinSchedule->portal_disbursement_id = $LoansSchedule[$t]->loan_unique_id;
                                        $mainJoinSchedule->portal_disbursement_schedule_id = $LoansSchedule[$t]->id;
                                        $mainJoinSchedule->main_disbursement_schedule_id = $RpDetail[$t]->id;
                                        $mainJoinSchedule->branch_id = $x;
                                        $mainJoinSchedule->save();
                                    }
                                }
                            }
                            //add to schedule main join

                            // loan cycle

                            DB::table($bcode . '_loan_cycle')
                                ->where('loan_unique_id', $loanPortalKey)
                                ->delete();

                            $loan_cycle = new LoanCycle();
                            $loan_cycle->setTable($bcode . '_loan_cycle');

                            $loan_cycle->loan_unique_id = $loanPortalKey;
                            $loan_cycle->client_unique_id = $clientid;
                            $loan_cycle->loan_amount = $loans[$i]->loan_amount;
                            $loan_cycle->loan_final_amount = $loans[$i]->loan_amount;
                            $loan_cycle->description = '2' . ',' . '2' . ',' . '2';
                            $loan_cycle->cycle = $loans[$i]->loan_cycle == 0 ? 1 : $loans[$i]->loan_cycle;
                            $loan_cycle->now_status = 1;
                            $loan_cycle->save();

                            // loan charge and compulsory join
                            $charges = DB::table('loan_charges_type')
                                ->leftJoin('loan_charges_and_type', 'loan_charges_and_type.charges_id', '=', 'loan_charges_type.id')
                                ->where('loan_charges_and_type.loan_type_id', $loans[$i]->loan_production_id)
                                ->get();

                            DB::table($bcode . '_loan_charges_compulsory_join')
                                ->where('loan_unique_id', $loanPortalKey)
                                ->delete();

                            foreach ($charges as $charge) {
                                $loan_join = new LoanJoin();
                                $loan_join->setTable($bcode . '_loan_charges_compulsory_join');
                                $loan_join->loan_unique_id = $loanPortalKey;
                                $loan_join->loan_charge_id = $charge->charge_code;
                                $loan_join->loan_type_id = $loans[$i]->loan_production_id;
                                $loan_join->branch_id = $loans[$i]->branch_id;
                                $loan_join->save();
                            }

                            $compulsories = DB::table('loan_compulsory_type')
                                ->leftJoin('loan_compulsory_product', 'loan_compulsory_product.compulsory_product_id', '=', 'loan_compulsory_type.id')
                                ->where('loan_compulsory_product.branch_id', $loans[$i]->branch_id)
                                ->get();
                            foreach ($compulsories as $compulsory) {
                                $text = 'loan_compulsory' . ($i + 1) . '_id';
                                $loan_join = new LoanJoin();
                                $loan_join->setTable($bcode . '_loan_charges_compulsory_join');
                                $loan_join->loan_unique_id = $loanPortalKey;
                                $loan_join->loan_compulsory_id = $compulsory->compulsory_code;
                                $loan_join->loan_type_id = $loans[$i]->loan_production_id;
                                $loan_join->branch_id = $loans[$i]->branch_id;
                                $loan_join->save();
                            }
                        }
                    }


                    //update end

                    //declined
                    $getID = DB::table('tbl_main_join_loan')->where('main_loan_id', $loans[$i]->id)->where('branch_id', $x)->first()->portal_loan_id;
                    if ($loans[$i]->disbursement_status == 'Declined') {
                        DB::table($bcode . '_loans')->where('loan_unique_id', $getID)->update(['disbursement_status' => 'Declined']);
                    }

                    //cancel
                    if ($loans[$i]->disbursement_status == 'Canceled') {
                        DB::table($bcode . '_loans')->where('loan_unique_id', $getID)->update(['disbursement_status' => 'Canceled']);
                    }

                    if (($loans[$i]->disbursement_status == 'Approved') && (DB::table($bcode . '_loans')->where('loan_unique_id', $getID)->first()->disbursement_status == 'Pending')) {
                        DB::table($bcode . '_loans')->where('loan_unique_id', $getID)->update([
                            'disbursement_status' => 'Approved',
                            'approved_date' => $loans[$i]->status_note_date_approve
                        ]);
                    }

                    continue;
                }

                if ($loans[$i]->loan_production_id == 0) {
                    continue;
                }

                // staff
                $staffid = DB::table('tbl_main_join_staff')
                    ->where('main_staff_id', $loans[$i]->loan_officer_id)
                    ->first();
                if ($staffid) {
                    $staffid = $staffid->portal_staff_id;
                } else if ($loans[$i]->loan_officer_id == 0) {
                    $staffid = 0;
                } else {
                    // $staffid = '';
                    continue;
                }
                // center
                $centerid = DB::table('tbl_main_join_center')
                    ->where('main_center_id', $loans[$i]->center_leader_id)
                    ->first();
                if ($centerid) {
                    $centerid = $centerid->portal_center_id;
                } else if ($loans[$i]->center_leader_id == 0) {
                    $centerid = 0;
                } else {
                    // $centerid = '';
                    continue;
                }
                // group
                $groupid = DB::table('tbl_main_join_group')
                    ->where('main_group_id', $loans[$i]->group_loan_id)
                    ->first();
                if ($groupid) {
                    $groupid = $groupid->portal_group_id;
                } else if ($loans[$i]->group_loan_id == 0) {
                    $groupid = 0;
                } else {
                    $groupid = '';
                    // continue;
                }

                // Guarantor_a
                $guarantor_a = DB::table('tbl_main_join_guarantor')
                    ->where('main_guarantor_id', $loans[$i]->guarantor_id)
                    ->first();
                if ($guarantor_a) {
                    $guarantor_a = $guarantor_a->portal_guarantor_id;
                } else {
                    $guarantor_a = '';
                }
                // Guarantor_b
                $guarantor_b = DB::table('tbl_main_join_guarantor')
                    ->where('main_guarantor_id', $loans[$i]->guarantor_id)
                    ->first();
                if ($guarantor_b) {
                    $guarantor_b = $guarantor_b->portal_guarantor_id;
                } else {
                    $guarantor_b = '';
                }

                // Guarantor_c
                $guarantor_c = DB::table('tbl_main_join_guarantor')
                    ->where('main_guarantor_id', $loans[$i]->guarantor_id)
                    ->first();
                if ($guarantor_c) {
                    $guarantor_c = $guarantor_c->portal_guarantor_id;
                } else {
                    $guarantor_c = '';
                }

                // client
                $clientid = DB::table('tbl_main_join_client')
                    ->where('main_client_id', $loans[$i]->client_id)
                    ->first();
                if ($clientid) {
                    $clientid = $clientid->portal_client_id;
                } else {
                    // $clientid = '';
                    continue;
                }



                $mainJoin = new Crawl_Loan();
                $mainJoin->portal_loan_id = $this->generateLoanUniquekey($x);
                $mainJoin->main_loan_id = $loans[$i]->id;
                $mainJoin->main_loan_code = $loans[$i]->disbursement_number;
                $mainJoin->branch_id = $x;
                $mainJoin->save();


                //return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $bcode ]);
                $data_loan = DB::connection('portal')
                    ->table($bcode . '_loans')
                    ->insert([
                        'loan_unique_id' => $mainJoin->portal_loan_id,
                        //'process_join_id'
                        'branch_id' => $loans[$i]->branch_id,
                        'group_id' => $groupid, // change later
                        'center_id' => $centerid,
                        'loan_officer_id' => $staffid,
                        'client_id' => $clientid,
                        'guarantor_a' => $guarantor_a,
                        'guarantor_b' => $guarantor_b,
                        'guarantor_c' => $guarantor_c,
                        'loan_type_id' => $loans[$i]->loan_production_id,
                        'loan_amount' => $loans[$i]->loan_amount,
                        // 'estimate_receivable_amount'=> $loans[$i]->principle_receivable,
                        'loan_term_value' => $loans[$i]->loan_term_value,
                        'loan_term' => $loans[$i]->loan_term,
                        'interest_rate_period' => $loans[$i]->interest_rate_period,
                        'disbursement_status' => $loans[$i]->disbursement_status == 'Approved' ? 'Approved' : 'Pending',
                        'interest_method' => $loans[$i]->interest_method,
                        'remark' => $loans[$i]->remark,
                        //'status_del'
                        'loan_application_date' => $loans[$i]->loan_application_date,
                        'first_installment_date' => $loans[$i]->first_installment_date,
                        'approveD_date' => $loans[$i]->disbursement_status == 'Approved' ? $loans[$i]->status_note_date_approve : NULL,
                        'disbursement_date' => $loans[$i]->loan_application_date,
                        'cancel_date' => $loans[$i]->cancel_date,
                        'active_at' => $loans[$i]->status_note_date_activated == NULL ? date('Y-m-d') : $loans[$i]->status_note_date_activated,
                        'created_at' => $loans[$i]->created_at,
                        'updated_at' => $loans[$i]->updated_at,
                        //'business_category_id'
                    ]);

                //return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $data_loan ]);
                $principal_formula = DB::table('loan_type')
                    ->where('id', $loans[$i]->loan_production_id)
                    ->first()->principal_formula;

                // Repayment date
                if ($loans[$i]->loan_term == 'Day') {
                    $loan_term = 1;
                } elseif ($loans[$i]->loan_term == 'Week') {
                    $loan_term = 7;
                } elseif ($loans[$i]->loan_term == 'Two-Weeks') {
                    $loan_term = 14;
                } elseif ($loans[$i]->loan_term == 'Four-Weeks') {
                    $loan_term = 28;
                } elseif ($loans[$i]->loan_term == 'Month') {
                    $loan_term = 30;
                } elseif ($loans[$i]->loan_term == 'Year') {
                    $loan_term = 30;
                }

                if ($loans[$i]->loan_production_id == 1 or $loans[$i]->loan_production_id == 2) {
                    // GeneralWeekly14D
                    $loanschedule = LoanController::fixedLoanMethod(
                        $loans[$i]->loan_amount,
                        $loan_term,
                        $loans[$i]->loan_term_value,
                        $loans[$i]->interest_rate,
                        $principal_formula,
                        // $start_date);
                        $loans[$i]->loan_application_date,
                    );
                } elseif ($loans[$i]->loan_production_id == 3 or $loans[$i]->loan_production_id == 4) {
                    // ExtraLoanWeekly
                    $loanschedule = LoanController::principalFinalMethod30D(
                        $loans[$i]->loan_amount,
                        $loan_term,
                        $loans[$i]->loan_term_value,
                        $loans[$i]->interest_rate,
                        $principal_formula,
                        // $start_date);
                        $loans[$i]->loan_application_date,
                    );
                } elseif ($loans[$i]->loan_production_id > 4 && $loans[$i]->loan_production_id < 20) {
                    // 5 to 19
                    // ExtraLoanWeekly
                    $loanschedule = LoanController::fixedPrincipalMethod30D(
                        $loans[$i]->loan_amount,
                        $loan_term,
                        $loans[$i]->loan_term_value,
                        $loans[$i]->interest_rate,
                        $principal_formula,
                        // $start_date);
                        $loans[$i]->loan_application_date,
                    );
                } elseif ($loans[$i]->loan_production_id > 19 && $loans[$i]->loan_production_id < 23) {
                    // ExtraLoanWeekly
                    $loanschedule = LoanController::principalFinalMethod28D(
                        $loans[$i]->loan_amount,
                        $loan_term,
                        $loans[$i]->loan_term_value,
                        $loans[$i]->interest_rate,
                        $principal_formula,
                        // $start_date);
                        $loans[$i]->loan_application_date,
                    );
                } elseif ($loans[$i]->loan_production_id > 22 && $loans[$i]->loan_production_id < 26) {
                    // GeneralMonthly28D
                    $loanschedule = LoanController::fixedPrincipalMethod28D(
                        $loans[$i]->loan_amount,
                        $loan_term,
                        $loans[$i]->loan_term_value,
                        $loans[$i]->interest_rate,
                        $principal_formula,
                        // $start_date);
                        $loans[$i]->loan_application_date,
                    );
                } elseif ($loans[$i]->loan_production_id == 26 or $loans[$i]->loan_production_id == 27) {
                    // ExtraLoanWeekly
                    $loanschedule = LoanController::fixedPrincipalMethod30D(
                        $loans[$i]->loan_amount,
                        $loan_term,
                        $loans[$i]->loan_term_value,
                        $loans[$i]->interest_rate,
                        $principal_formula,
                        // $start_date);
                        $loans[$i]->loan_application_date,
                    );
                }

                $loan_amount = $loans[$i]->loan_amount;

                foreach ($loanschedule as $schedule) {
                    $entrydate = $schedule['date'];
                    // $total = $schedule->repay_interest + $schedule->repay_principal;
                    $loan_amount -= $schedule['repay_principal'];

                    $loan_schedule = new LoanSchedule();
                    $loan_schedule->setTable($bcode . '_loans_schedule');

                    $loan_schedule->loan_unique_id = $mainJoin->portal_loan_id;
                    $loan_schedule->loan_type_id = $loans[$i]->loan_production_id;
                    $loan_schedule->month = $entrydate;
                    $loan_schedule->capital = $schedule['repay_principal'];
                    $loan_schedule->interest = $schedule['repay_interest'];
                    $loan_schedule->discount = 0;
                    $loan_schedule->refund_interest = 1;
                    $loan_schedule->refund_amount = 1;
                    $loan_schedule->final_amount = $loan_amount;
                    $loan_schedule->status = 'none';
                    $loan_schedule->save();
                }

                //add to schedule main join
                $LoansSchedule = DB::connection('portal')->table($bcode . '_loans_schedule')->where('loan_unique_id', $mainJoin->portal_loan_id)->get();
                $RpDetail = DB::connection('main')->table('loan_disbursement_calculate_' . $x)->where('disbursement_id', $loans[$i]->id)->get();

                for ($t = 0; $t < count($LoansSchedule); $t++) {
                    if (DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$t]->id)->where('branch_id', $x)->first()) {
                        break;
                    } else {
                        if ($RpDetail[$t]->no == ($t + 1)) {
                            $mainJoinSchedule = new Crawl_Loan_Repayment_Schedule();
                            $mainJoinSchedule->portal_disbursement_id = $LoansSchedule[$t]->loan_unique_id;
                            $mainJoinSchedule->portal_disbursement_schedule_id = $LoansSchedule[$t]->id;
                            $mainJoinSchedule->main_disbursement_schedule_id = $RpDetail[$t]->id;
                            $mainJoinSchedule->branch_id = $x;
                            $mainJoinSchedule->save();
                        }
                    }
                }
                //add to schedule main join

                // loan cycle

                $loan_cycle = new LoanCycle();
                $loan_cycle->setTable($bcode . '_loan_cycle');

                $loan_cycle->loan_unique_id = $mainJoin->portal_loan_id;
                $loan_cycle->client_unique_id = $clientid;
                $loan_cycle->loan_amount = $loans[$i]->loan_amount;
                $loan_cycle->loan_final_amount = $loans[$i]->loan_amount;
                $loan_cycle->description = '2' . ',' . '2' . ',' . '2';
                $loan_cycle->cycle = $loans[$i]->loan_cycle == 0 ? 1 : $loans[$i]->loan_cycle;
                $loan_cycle->now_status = 1;
                $loan_cycle->save();

                // loan charge and compulsory join
                $charges = DB::table('loan_charges_type')
                    ->leftJoin('loan_charges_and_type', 'loan_charges_and_type.charges_id', '=', 'loan_charges_type.id')
                    ->where('loan_charges_and_type.loan_type_id', $loans[$i]->loan_production_id)
                    ->get();
                foreach ($charges as $charge) {
                    $loan_join = new LoanJoin();
                    $loan_join->setTable($bcode . '_loan_charges_compulsory_join');
                    $loan_join->loan_unique_id = $mainJoin->portal_loan_id;
                    $loan_join->loan_charge_id = $charge->charge_code;
                    $loan_join->loan_type_id = $loans[$i]->loan_production_id;
                    $loan_join->branch_id = $loans[$i]->branch_id;
                    $loan_join->save();
                }

                $compulsories = DB::table('loan_compulsory_type')
                    ->leftJoin('loan_compulsory_product', 'loan_compulsory_product.compulsory_product_id', '=', 'loan_compulsory_type.id')
                    ->where('loan_compulsory_product.branch_id', $loans[$i]->branch_id)
                    ->get();
                foreach ($compulsories as $compulsory) {
                    $text = 'loan_compulsory' . ($i + 1) . '_id';
                    $loan_join = new LoanJoin();
                    $loan_join->setTable($bcode . '_loan_charges_compulsory_join');
                    $loan_join->loan_unique_id = $mainJoin->portal_loan_id;
                    $loan_join->loan_compulsory_id = $compulsory->compulsory_code;
                    $loan_join->loan_type_id = $loans[$i]->loan_production_id;
                    $loan_join->branch_id = $loans[$i]->branch_id;
                    $loan_join->save();
                }
                $z = ++$z;
            }
        }

        $entrey = DB::connection('portal')
            ->table($bcode . '_loans')
            ->get();

        DB::connection('portal')
            ->table('tbl_crawling')
            ->where('module_name', 'loan')
            ->update([
                'data_count' => $z,
                'crawl_date' => date('Y-m-d H:i:s'),
            ]);

        if ($z) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $z]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
}
