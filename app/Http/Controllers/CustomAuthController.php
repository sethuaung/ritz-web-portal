<?php

namespace App\Http\Controllers;

use App\Models\ClientDataEnteries\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class CustomAuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }  
      

    public function customLogin(Request $request)
    {
       
   
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6|max:15',
        ]);
    
       	$user =Staff::where('email', $request->email)->first();
    // dd($user);
        // dd(Hash::check($request->password, $user->user_password));
        if($user){
           if (Hash::check($request->password, $user->user_password)) {
            // if($request->password == $user->user_password){
                session()->put('staff_code', $user->staff_code);
                session()->put('role_id',$user->role_id);

                return redirect('/dashboard')
                        ->withSuccess('Login accept permitted!');
            }else{
                return back()->with('fail','Invalid password');
            }
        }else{
            return back()->with('fail','No account Found for this Email');
        }
    }   
    
    public function dashboard()
    {
        if(Auth::check()){
            return view('dashboard.index');
        }
  
        return redirect("login")->withSuccess('You are not allowed to access');
    }
   
    

    public function signOut() {
        Session::flush();
        Auth::logout();
  
        return Redirect('login');
    }
}
