<?php

namespace App\Http\Controllers;

use App\Models\Loans\Loan;
use App\Models\ClientDataEnteries\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Loandataenteries\SavingWithdrawl;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;


class LoanDepositController extends Controller
{
    public function getBranchCode()
    {
        $staff_code = Session::get('staff_code');
        // dd($staff_code);
        $B_code = DB::table('tbl_staff')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->select('tbl_branches.branch_code')
            ->where('tbl_staff.staff_code', $staff_code)
            ->first();
        $b_code = strtolower($B_code->branch_code);
        return $b_code;
    }

    public function groupindex()
    {
        $bcode = $this->getBranchCode();

        $search_loan_product = request()->type;
        $search_center = request()->center;
        $search_group = request()->group;

        $data_loanstitle = 'View All Loan by group';

        if (request()->has('search_loanslist')) {
            $searchvalue = request()->get('search_loanslist');
            $query = DB::table($bcode . '_loans')
                ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
                ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
                ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_a')
                ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode . '_loans.loan_unique_id')
                ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->orWhere(function ($query) {
                    $bcode = $this->getBranchCode();
                    $searchvalue = request()->get('search_loanslist');
                    $query->where($bcode . '_loans.loan_unique_id', 'LIKE', $searchvalue . '%')
                        ->orWhere('tbl_staff.name', 'LIKE', $searchvalue . '%')
                        ->orWhere('tbl_client_basic_info.name', 'LIKE', $searchvalue . '%')
                        ->orWhere('tbl_guarantors.name', 'LIKE', $searchvalue . '%')
                        ->orWhere('loan_type.name', 'LIKE', $searchvalue . '%')
                        ->orWhere('tbl_main_join_loan.main_loan_code', 'LIKE', $searchvalue . '%')
                        ->orWhere('tbl_main_join_client.main_client_code', 'LIKE', $searchvalue . '%')
                        ->orWhere('tbl_client_basic_info.client_uniquekey', 'LIKE', $searchvalue . '%');
                })
                ->where($bcode . '_loans.disbursement_status', 'Approved');
            // ->where('tbl_main_join_loan.branch_id', DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id);
            $query->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name', 'tbl_main_join_loan.main_loan_code');
            $loans = $query->paginate(10);
        } else {
            $query = DB::table($bcode . '_loans')
                ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
                ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
                ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_a')
                ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode . '_loans.loan_unique_id')
                ->leftjoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where($bcode . '_loans.disbursement_status', 'Approved');
            // ->where('tbl_main_join_loan.branch_id', DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id);
            if ($search_loan_product) {
                $query->where($bcode . '_loans.loan_type_id', $search_loan_product);
            }
            if ($search_center) {
                $query->where($bcode . '_loans.center_id', $search_center);
            }
            if ($search_group) {
                $query->where($bcode . '_loans.group_id', $search_group);
            }

            $query->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name', 'tbl_main_join_loan.main_loan_code');
            $loans = $query->orderBy('id', 'desc')->paginate(10);
        }


        $centers = DB::table('tbl_center')
            ->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', 'tbl_center.center_uniquekey')
            ->where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
            ->get();
        $groups = DB::table('tbl_group')
            ->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', 'tbl_group.group_uniquekey')
            ->where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
            ->get();
        $types = DB::table('loan_type')->get();

        // dd($loans);

        return view('loan.dataentery.loandeposit.groupindex', compact('data_loanstitle', 'loans', 'centers', 'groups', 'types'));
    }

    public function groupCreate(Request $request)
    {
        $this->permissionFilter("group-deposit");
        $bcode = $this->getBranchCode();

        $charge_deposit = 0;
        $compulsory_deposit = 0;
        $total_deposit = 0;

        $total_loans = array();
        $checkCount = array_count_values($request->all());

        if (array_key_exists('checked', $checkCount)) {
            $count = $checkCount['checked'];
            for ($x = 1; $x <= $request->count; $x++) {
                $name = "check_" . $x;
                $value = "check_" . $x . "_value";
                if ($request->$name === "checked") {
                    array_push($total_loans, $request->$value);
                    $loan = DB::table($bcode . '_loans')
                        ->leftJoin('tbl_client_basic_info', $bcode . '_loans.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                        ->where($bcode . '_loans.loan_unique_id', $request->$value)
                        ->select($bcode . '_loans.*', 'tbl_client_basic_info.id AS client_id', 'tbl_client_basic_info.client_uniquekey', 'tbl_client_basic_info.name', 'tbl_client_basic_info.nrc')
                        ->first();

                    $charge = DB::table($bcode . '_loan_charges_compulsory_join')
                        ->leftJoin('loan_charges_type', $bcode . '_loan_charges_compulsory_join.loan_charge_id', '=', 'loan_charges_type.charge_code')
                        ->where($bcode . '_loan_charges_compulsory_join.loan_unique_id', $loan->loan_unique_id)
                        ->where($bcode . '_loan_charges_compulsory_join.loan_charge_id', '!=', null)
                        ->get();

                    $compulsory = DB::table($bcode . '_loan_charges_compulsory_join')
                        ->leftJoin('loan_compulsory_type', $bcode . '_loan_charges_compulsory_join.loan_compulsory_id', '=', 'loan_compulsory_type.compulsory_code')
                        ->where($bcode . '_loan_charges_compulsory_join.loan_unique_id', $loan->loan_unique_id)
                        ->where($bcode . '_loan_charges_compulsory_join.loan_compulsory_id', '!=', null)
                        ->get();

                    foreach ($charge as $c) {
                        if ($c->charge_option == 2) {
                            $charge_deposit += $loan->loan_amount * ($c->amount / 100);
                        } else {
                            $charge_deposit += $c->amount;
                        }
                    }

                    foreach ($compulsory as $c) {
                        if ($c->charge_option == 2) {
                            $compulsory_deposit += $loan->loan_amount * ($c->saving_amount / 100);
                        } else {
                            $compulsory_deposit += $c->saving_amount;
                        }
                    }

                    $total_deposit = $charge_deposit + $compulsory_deposit;
                    if (!isset($loan->client_uniquekey)) {
                        return redirect()->back()->with('check', 'Client Unique Key Not Found In a Loan! Please check again!');
                    } else {
                        continue;
                    }
                }
            }

            return view('loan.dataentery.loandeposit.groupcreate', compact('count', 'total_loans', 'total_deposit', 'charge_deposit', 'compulsory_deposit'));
        } else {
            return redirect()->back()->with('check', 'Please check at least one loan');
        }
    }

    public function create($id)
    {
        $this->permissionFilter("deposit-payment");
        $bcode = $this->getBranchCode();

        $loan = DB::table($bcode . '_loans')
            ->leftJoin('tbl_client_basic_info', $bcode . '_loans.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
            ->where($bcode . '_loans.loan_unique_id', $id)
            ->select($bcode . '_loans.*', 'tbl_client_basic_info.id AS client_id', 'tbl_client_basic_info.client_uniquekey', 'tbl_client_basic_info.name', 'tbl_client_basic_info.nrc')
            ->first();

        $charge = DB::table($bcode . '_loan_charges_compulsory_join')
            ->leftJoin('loan_charges_type', $bcode . '_loan_charges_compulsory_join.loan_charge_id', '=', 'loan_charges_type.charge_code')
            ->where($bcode . '_loan_charges_compulsory_join.loan_unique_id', $loan->loan_unique_id)
            ->where($bcode . '_loan_charges_compulsory_join.loan_charge_id', '!=', null)
            ->get();

        // dd($charge);

        $compulsory = DB::table($bcode . '_loan_charges_compulsory_join')
            ->leftJoin('loan_compulsory_type', $bcode . '_loan_charges_compulsory_join.loan_compulsory_id', '=', 'loan_compulsory_type.compulsory_code')
            ->where($bcode . '_loan_charges_compulsory_join.loan_unique_id', $loan->loan_unique_id)
            ->where($bcode . '_loan_charges_compulsory_join.loan_compulsory_id', '!=', null)
            ->get();

        $this_branch_id = DB::table('tbl_staff')->where('staff_code', '=', Session::get('staff_code'))->select('branch_id')->first();
        $accounts = DB::table('account_chart_externals')->where('branch_id', '=', $this_branch_id->branch_id)->where('external_acc_code', 'LIKE', '1-11' . '%')->get();
        $charge_deposit = 0;
        foreach ($charge as $c) {
            if ($c->charge_option == 2) {
                $charge_deposit += $loan->loan_amount * ($c->amount / 100);
            } else {
                $charge_deposit += $c->amount;
            }
        }

        $compulsory_deposit = 0;
        foreach ($compulsory as $c) {
            if ($c->charge_option == 2) {
                $compulsory_deposit += $loan->loan_amount * ($c->saving_amount / 100);
            } else {
                $compulsory_deposit += $c->saving_amount;
            }
        }


        $total_deposit = $charge_deposit + $compulsory_deposit;

        if (!isset($loan->client_uniquekey)) {
            return redirect()->back()->withErrors("Client Unique Key Not Found! Cannot Deposit");
        } else {
            return view('loan.dataentery.loandeposit.create', compact('accounts', 'loan', 'charge', 'compulsory', 'total_deposit', 'charge_deposit', 'compulsory_deposit'));
        }
    }

    public function groupStore(Request $request)
    {
        $this->permissionFilter("group-deposit");
        $validator = $request->validate([
            'deposit_pay_date' => 'required',
            'ref_no' => 'required',
            'invoice_no' => 'required'
        ]);
        if ($validator) {
            $bcode = $this->getBranchCode();
            // dd($request);
            for ($i = 1; $i <= $request->count; $i++) {

                $name = 'loan_id_' . $i;
                $loan_id = $request->$name;
                $loan = DB::table($bcode . '_loans')->where('loan_unique_id', $loan_id)
                    ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
                    ->first();


                $charge = DB::table($bcode . '_loan_charges_compulsory_join')
                    ->leftJoin('loan_charges_type', 'loan_charges_type.charge_code', '=', $bcode . '_loan_charges_compulsory_join.loan_charge_id')
                    ->where($bcode . '_loan_charges_compulsory_join.loan_unique_id', $loan_id)
                    ->where($bcode . '_loan_charges_compulsory_join.loan_charge_id', '!=', null)
                    ->get();

                $ttl_deposit = 0;

                if (count($charge) > 0) {
                    for ($x = 0; $x < count($charge); $x++) {
                        if ($charge[$x]->charge_option == "1") {
                            $ttl_deposit += $charge[$x]->charge;
                        } else {
                            $charge2 = $loan->loan_amount * ($charge[$x]->charge / 100);
                            $ttl_deposit += $charge2;
                        }
                    }

                    $saving_in_charges = 0;
                    for ($y = 0; $y < count($charge); $y++) {
                        if (($charge[$y]->charge_option == "1") && ($charge[$y]->product_type == 'savings')) {
                            $saving_in_charges += $charge[$y]->charge;
                        } else {
                            $charge2 = $loan->loan_amount * ($charge[$y]->charge / 100);
                            $saving_in_charges += $charge2;
                        }
                    }
                } else {
                    $saving_in_charges = 0;
                }

                $compulsory = DB::table($bcode . '_loan_charges_compulsory_join')
                    ->leftJoin('loan_compulsory_type', 'loan_compulsory_type.compulsory_code', '=', $bcode . '_loan_charges_compulsory_join.loan_compulsory_id')
                    ->where($bcode . '_loan_charges_compulsory_join.loan_unique_id', $loan_id)
                    ->where($bcode . '_loan_charges_compulsory_join.loan_compulsory_id', '!=', null)
                    ->get();


                if (count($compulsory) > 0) {
                    for ($z = 0; $z < count($compulsory); $z++) {
                        if ($compulsory[$z]->charge_option == "1") {
                            $ttl_deposit += $compulsory[$z]->saving_amount;
                        } else {
                            $charge2 = $loan->loan_amount * ($compulsory[$z]->saving_amount / 100);
                            $ttl_deposit += $charge2;
                        }
                    }

                    $compulsory_saving = 0;
                    for ($j = 0; $j < count($compulsory); $j++) {
                        if ($compulsory[$j]->charge_option == "1") {
                            $compulsory_saving += $compulsory[$j]->saving_amount;
                        } else {
                            $charge2 = $loan->loan_amount * ($compulsory[$j]->saving_amount / 100);
                            $compulsory_saving += $charge2;
                        }
                    }
                } else {
                    $compulsory_saving = 0;
                }

                //                 $this_branch_id = DB::table('tbl_staff')->where('staff_code', '=', Session::get('staff_code'))->select('branch_id')->first();
                //                 $accounts = DB::table('account_chart_externals')->where('branch_id', '=', $this_branch_id->branch_id)->where('external_acc_code', 'LIKE', '1-11' . '%')->first();
                //                 $cash_acc_id = $accounts->id;
                //                 $total_saving = $saving_in_charges + $compulsory_saving;

                //                 $acc_code = [$cash_acc_id, "182", "183"];
                //                 for ($a = 0; $a < count($acc_code); $a++) {
                //                     DB::table($bcode . '_deposit')->insert([
                //                         'loan_id' => $loan_id,
                //                         'client_id' => $loan->client_id,
                //                         'ref_no' => $request->ref_no,
                //                         'invoice_no' => $request->invoice_no,
                //                         'acc_join_id' => $acc_code[$a],
                //                         'total_deposit_balance' => $ttl_deposit,
                //                         'paid_deposit_balance' => $ttl_deposit,
                //                         'note' => $request->note,
                //                         'deposit_pay_date' => $request->deposit_pay_date
                //                     ]);
                //                 }
                $this_branch_id = DB::table('tbl_staff')->where('staff_code', '=', Session::get('staff_code'))->select('branch_id')->first();
                $accounts = DB::table('account_chart_externals')->where('branch_id', '=', $this_branch_id->branch_id)->where('external_acc_code', 'LIKE', '1-11' . '%')->first();
                $cash_acc_id = $accounts->id;
                $total_saving = $saving_in_charges + $compulsory_saving;
                $acc_code = DB::table('account_chart_external_details')->where('account_chart_external_details.external_acc_id', $cash_acc_id)->first()->main_acc_code;
                $main_acc_code = [$acc_code];
                // dd($main_acc_code);
                for ($a = 0; $a < count($main_acc_code); $a++) {
                    DB::table($bcode . '_deposit')->insert([
                        'loan_id' => $loan_id,
                        'client_id' => $loan->client_id,
                        'ref_no' => $request->ref_no,
                        'invoice_no' => $request->invoice_no,
                        'acc_join_id' => $main_acc_code[$a],
                        'total_deposit_balance' => $ttl_deposit,
                        'paid_deposit_balance' => $ttl_deposit,
                        'note' => $request->note,
                        'deposit_pay_date' => $request->deposit_pay_date
                    ]);
                }

                $charges_compulsory = DB::table($bcode . '_loan_charges_compulsory_join')
                    ->leftJoin($bcode . '_loans', $bcode . '_loan_charges_compulsory_join.loan_unique_id', '=', $bcode . '_loans.loan_unique_id')
                    ->where($bcode . '_loans.loan_unique_id', $loan_id)
                    ->select($bcode . '_loan_charges_compulsory_join.*')
                    ->get();



                foreach ($charges_compulsory as $c) {
                    DB::table($bcode . '_loan_charges_compulsory_join')
                        ->where('loan_unique_id', $c->loan_unique_id)
                        ->update([
                            "status" => "paid"
                        ]);
                }

                DB::table($bcode . '_loans')->where('loan_unique_id', $loan_id)->update([
                    'disbursement_status' => 'Deposited'
                ]);

                $this->liveDeposit($loan_id);
                //live deposit
                //                 $route = $this->getMainUrl37();
                //                 $route_name = 'create-deposit';
                //                 $routeurl = $route . $route_name;
                //                 $authorization = $this->loginToken();

                //                 $main_loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $loan_id)->first();
                //                 if ($main_loan_id) {
                //                     $clientPID = DB::table($bcode.'_loans')->where('loan_unique_id', $loan_id)->first()->client_id;
                //                     $clientDetail = DB::table('tbl_client_basic_info')->where('client_uniquekey', $clientPID)->first();
                //                     $main_loan_id = $main_loan_id->main_loan_id;

                //                     $client = new \GuzzleHttp\Client(['verify' => false]);
                //                     $result = $client->post($routeurl, [
                //                         'headers' => [
                //                             'Content-Type' => 'application/x-www-form-urlencoded',
                //                             'Authorization' => 'Bearer ' . $authorization,
                //                         ],
                //                         'form_params' => [
                //                             'applicant_number_id' => $main_loan_id,
                //                             'loan_deposit_date' =>  $request->deposit_pay_date,
                //                             'referent_no' => $request->ref_no,
                //                             'customer_name' => $clientDetail->name,
                //                             'nrc' => $clientDetail->nrc == NULL ? $clientDetail->old_nrc : $clientDetail->nrc,
                //                             'client_id' => DB::table('tbl_main_join_client')->where('portal_client_id', $clientDetail->client_uniquekey)->first()->main_client_id,
                //                             'invoice_no' => $request->invoice_no,
                //                             'compulsory_saving_amount' => $compulsory_saving,
                //                             'cash_acc_id' => $main_acc_code[0], //138, //153620,
                //                             'total_deposit' => $ttl_deposit,
                //                             'client_pay' => $request->paid_deposit_balance,
                //                             'note' => $request->note,
                //                             'branch_id' => DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id,
                //                         ]

                //                     ]);

                //                     $response = (string) $result->getBody();
                //                     $response = json_decode($response);
                //                     if ($response->status_code == 200) {
                //                         DB::table('tbl_main_join_loan_deposit')->insert([
                //                             'main_loan_deposit_id' => $main_loan_id,
                //                             'main_loan_deposit_client_id' => DB::table('tbl_main_join_client')->where('portal_client_id', $clientDetail->client_uniquekey)->first()->main_client_id,
                //                             'portal_loan_deposit_id' => $loan_id
                //                          ]);
                //                     }
                //                 }

                //end live deposit


                //Add to saving withdrawl
                //                 $current_saving = DB::table($bcode . '_saving_withdrawl')->where('client_idkey', $loan->client_uniquekey)->get()->first();
                //                 $saving_id = $this->generateSavingAccID();

                //                 if (empty($current_saving)) {

                //                     $saving_withdrawl = new SavingWithdrawl;
                //                     $saving_withdrawl->setTable($bcode . '_saving_withdrawl');
                //                     $saving_withdrawl->saving_unique_id = $saving_id;
                //                     $saving_withdrawl->loan_unique_id = $loan_id;
                //                     $saving_withdrawl->client_idkey = $loan->client_uniquekey;
                //                     $saving_withdrawl->client_name = $loan->name;
                //                     $saving_withdrawl->total_saving = $total_saving;
                //                     $saving_withdrawl->total_interest = NULL;
                //                     $saving_withdrawl->saving_left = $total_saving;
                //                     $saving_withdrawl->interest_left = NULL;
                //                     $saving_withdrawl->withdrawl_type = 'none';
                //                     $saving_withdrawl->status = 'none';
                //                     $saving_withdrawl->save();
                //                 } else {

                //                     $saving_withdrawl = new SavingWithdrawl;
                //                     $saving_withdrawl->setTable($bcode . '_saving_withdrawl');
                //                     $saving_withdrawl->saving_unique_id = $current_saving->saving_unique_id;
                //                     $saving_withdrawl->loan_unique_id = $request->loan_id;
                //                     $saving_withdrawl->client_idkey = $loan->client_uniquekey;
                //                     $saving_withdrawl->client_name = $loan->name;
                //                     $saving_withdrawl->total_saving = $current_saving->saving_left + $total_saving;
                //                     $saving_withdrawl->total_interest = $current_saving->interest_left;
                //                     $saving_withdrawl->saving_left = $current_saving->saving_left + $total_saving;
                //                     $saving_withdrawl->interest_left = $current_saving->interest_left;
                //                     $saving_withdrawl->withdrawl_type = 'none';
                //                     $saving_withdrawl->status = 'none';
                //                     $saving_withdrawl->save();
                //                 }
            }
            return redirect()->route('depositgroup.index')->with("successMsg", 'Deposit success ');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    public function store(Request $request)
    {
        $this->permissionFilter("deposit-payment");
        $bcode = $this->getBranchCode();

        $loan = DB::table($bcode . '_loans')->where('loan_unique_id', $request->loan_id)
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
            ->first();

        $charge = DB::table($bcode . '_loan_charges_compulsory_join')
            ->leftJoin('loan_charges_type', 'loan_charges_type.charge_code', '=', $bcode . '_loan_charges_compulsory_join.loan_charge_id')
            ->where($bcode . '_loan_charges_compulsory_join.loan_unique_id', $request->loan_id)
            ->where($bcode . '_loan_charges_compulsory_join.loan_charge_id', '!=', null)
            ->where('loan_charges_type.product_type', '=', 'savings')
            ->get();

        if ($charge) {
            $saving_in_charges = 0;
            for ($i = 0; $i < count($charge); $i++) {
                if ($charge[$i]->charge_option == "1") {
                    $saving_in_charges += $charge[$i]->charge;
                } else {
                    $charge2 = $loan->loan_amount * ($charge[$i]->charge / 100);
                    $saving_in_charges += $charge2;
                }
            }
        } else {
            $saving_in_charges = 0;
        }

        $compulsory = DB::table($bcode . '_loan_charges_compulsory_join')
            ->leftJoin('loan_compulsory_type', 'loan_compulsory_type.compulsory_code', '=', $bcode . '_loan_charges_compulsory_join.loan_compulsory_id')
            ->where($bcode . '_loan_charges_compulsory_join.loan_unique_id', $request->loan_id)
            ->where($bcode . '_loan_charges_compulsory_join.loan_compulsory_id', '!=', null)
            ->get();

        if ($compulsory) {
            $compulsory_saving = 0;
            for ($i = 0; $i < count($compulsory); $i++) {
                if ($compulsory[$i]->charge_option == "1") {
                    $compulsory_saving += $compulsory[$i]->saving_amount;
                } else {
                    $charge2 = $loan->loan_amount * ($compulsory[$i]->saving_amount / 100);
                    $compulsory_saving += $charge2;
                }
            }
        } else {
            $compulsory_saving = 0;
        }
        $total_saving = $saving_in_charges + $compulsory_saving;

        $validator = $request->validate([
            'loan_id' => ['required', 'string', 'max:255'],
            'client_id' => ['required', 'string', 'max:255'],
            'ref_no' => ['required', 'string', 'max:255'],
            'invoice_no' => ['required', 'string', 'max:255'],

            'total_deposit_balance' => ['required', 'string', 'max:255'],
            'paid_deposit_balance' => ['required', 'string', 'max:255'],
            'note' => ['nullable', 'string'],
            'deposit_pay_date' => ['required', 'date']
        ]);
        if ($validator) {
            // $acc_code = [$request->cash_acc_id, "182", "183"];
            // for ($i = 0; $i < count($acc_code); $i++) {
            //     DB::table($bcode . '_deposit')->insert([
            //         'loan_id' => $request->loan_id,
            //         'client_id' => $request->client_id,
            //         'ref_no' => $request->ref_no,
            //         'invoice_no' => $request->invoice_no,
            //         'acc_join_id' => $acc_code[$i],
            //         'total_deposit_balance' => $request->total_deposit_balance,
            //         'paid_deposit_balance' => $request->paid_deposit_balance,
            //         'note' => $request->note,
            //         'deposit_pay_date' => $request->deposit_pay_date
            //     ]);
            // }
            $acc_code = DB::table('account_chart_external_details')->where('account_chart_external_details.external_acc_id', $request->cash_acc_id)->first()->main_acc_code;
            $main_acc_code = [$acc_code];
            // dd($main_acc_code);
            for ($i = 0; $i < count($main_acc_code); $i++) {
                DB::table($bcode . '_deposit')->insert([
                    'loan_id' => $request->loan_id,
                    'client_id' => $request->client_id,
                    'ref_no' => $request->ref_no,
                    'invoice_no' => $request->invoice_no,
                    'acc_join_id' => $main_acc_code[$i],
                    'total_deposit_balance' => $request->total_deposit_balance,
                    'paid_deposit_balance' => $request->paid_deposit_balance,
                    'note' => $request->note,
                    'deposit_pay_date' => $request->deposit_pay_date
                ]);
            }

            $charges_compulsory = DB::table('loan_charges_compulsory_join')
                ->leftJoin($bcode . '_loans', 'loan_charges_compulsory_join.loan_unique_id', '=', $bcode . '_loans.loan_unique_id')
                ->where($bcode . '_loans.loan_unique_id', $request->loan_id)
                ->select('loan_charges_compulsory_join.*')
                ->get();

            foreach ($charges_compulsory as $c) {
                DB::table('loan_charges_compulsory_join')
                    ->where('compulsory_code', $c->id)
                    ->update([
                        "status" => "paid"
                    ]);
            }


            DB::table($bcode . '_loans')->where('loan_unique_id', $request->loan_id)->update([
                'disbursement_status' => 'Deposited'
            ]);

            //live deposit
            $this->liveDeposit($request->loan_id);

            //Add to saving withdrawl
            //             $current_saving = DB::table($bcode . '_saving_withdrawl')->where('client_idkey', $loan->client_uniquekey)->get()->first();
            //             $saving_id = $this->generateSavingAccID();

            //             if (empty($current_saving)) {

            //                 $saving_withdrawl = new SavingWithdrawl;
            //                 $saving_withdrawl->setTable($bcode . '_saving_withdrawl');
            //                 $saving_withdrawl->saving_unique_id = $saving_id;
            //                 $saving_withdrawl->loan_unique_id = $request->loan_id;
            //                 $saving_withdrawl->client_idkey = $loan->client_uniquekey;
            //                 $saving_withdrawl->client_name = $loan->name;
            //                 $saving_withdrawl->total_saving = $total_saving;
            //                 $saving_withdrawl->total_interest = NULL;
            //                 $saving_withdrawl->saving_left = $total_saving;
            //                 $saving_withdrawl->interest_left = NULL;
            //                 $saving_withdrawl->withdrawl_type = 'none';
            //                 $saving_withdrawl->status = 'none';
            //                 $saving_withdrawl->save();

            //                 return redirect('loan?disbursement_status=Deposited')->with("successMsg", 'Deposit success & your saving account ID is ' . $saving_id);
            //             } else {
            //                 $saving_withdrawl = new SavingWithdrawl;
            //                 $saving_withdrawl->setTable($bcode . '_saving_withdrawl');
            //                 $saving_withdrawl->saving_unique_id = $current_saving->saving_unique_id;
            //                 $saving_withdrawl->loan_unique_id = $request->loan_id;
            //                 $saving_withdrawl->client_idkey = $loan->client_uniquekey;
            //                 $saving_withdrawl->client_name = $loan->name;
            //                 $saving_withdrawl->total_saving = $current_saving->saving_left + $total_saving;
            //                 $saving_withdrawl->total_interest = $current_saving->interest_left;
            //                 $saving_withdrawl->saving_left = $current_saving->saving_left + $total_saving;
            //                 $saving_withdrawl->interest_left = $current_saving->interest_left;
            //                 $saving_withdrawl->withdrawl_type = 'none';
            //                 $saving_withdrawl->status = 'none';
            //                 $saving_withdrawl->save();

            return redirect('loan?disbursement_status=Deposited')->with("successMsg", 'Deposit success');
            //             }
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    //auto generate saving uniquekey
    public function generateSavingAccID()
    {
        $bcode = $this->getBranchCode();

        $savingId = DB::table($bcode . '_saving_withdrawl')->get()->last();
        if ($savingId == null) {
            $bcode = strtoupper($this->getBranchCode());
            $savingId = $bcode;
            $savingId .= '-';
            $savingId .= 'S';
            $savingId .= '-';
            $savingId .= '0000001';
        } else {
            $savingId = $savingId->saving_unique_id;

            $bstr = substr($savingId, 3);
            $id = substr($bstr, -7, 7);
            $bstr_cusm = $id + 1;
            $bstr_cusm = (string)str_pad($bstr_cusm, 7, '0', STR_PAD_LEFT);

            // get loan_uniquekey in tbl_loans
            $savingId = strtoupper($this->getBranchCode()); // Branch Code
            $savingId .= '-';
            $savingId .= 'S';
            $savingId .= '-';
            $savingId .= $bstr_cusm;
        }

        return $savingId;
    }

    public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }

    // public function liveDeposit($loan_id)
    // {
        // $bcode = $this->getBranchCode();

        // $depositDetail=DB::table($bcode.'_deposit')->where('loan_id',$loan_id)->first();

        // $route = $this->getMainUrl37();
        // $route_name = 'create-deposit';
        // $routeurl = $route . $route_name;
        // $authorization = $this->loginToken();
        // $branch_id = DB::table('tbl_staff')->where('staff_code', Session::get('staff_code'))->first()->branch_id;

        // $loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $depositDetail->loan_id)->first();
        // if ($loan_id) {
        //     $loan_id = $loan_id->main_loan_id;
        // 	$acc_code = DB::table('account_chart_external_details')->where('account_chart_external_details.external_acc_id', $depositDetail->cash_acc_id)->first()->main_acc_code;
        //     $main_acc_code=DB::table('account_chart_external_details')->where('main_acc_code',$depositDetail->acc_join_id)->first()->main_acc_id;
        // 	// dd($main_acc_code);


        //     $client = new \GuzzleHttp\Client(['verify' => false]);
        //     $result = $client->post($routeurl, [
        //         'headers' => [
        //             'Content-Type' => 'application/x-www-form-urlencoded',
        //             'Authorization' => 'Bearer ' . $authorization,
        //         ],
        //         'form_params' => [
        //             'applicant_number_id' => $loan_id,
        //             'loan_deposit_date' =>  $depositDetail->deposit_pay_date,
        //             'referent_no' => $depositDetail->ref_no,
        //             'customer_name' => $depositDetail->client_name,
        //             'nrc' => $depositDetail->client_nrc,
        //             'client_id' => DB::table('tbl_main_join_client')->where('portal_client_id', $depositDetail->client_id)->first()->main_client_id,
        //             'invoice_no' => $depositDetail->invoice_no,
        //             'compulsory_saving_amount' => $depositDetail->compulsory_saving,
        //             'cash_acc_id' => $main_acc_code,  //153620,
        //             'total_deposit' => $depositDetail->total_deposit_balance,
        //             'client_pay' => $depositDetail->paid_deposit_balance,
        //             'note' => $depositDetail->note,
        //             'branch_id' => $branch_id,
        //         ]

        //     ]);

        //     $response = (string) $result->getBody();
        //     $response = json_decode($response);
        //     if ($response->status_code == 200) {
        //         DB::table('tbl_main_join_loan_deposit')->insert([
        //             'main_loan_deposit_id' => $loan_id,
        //             'main_loan_deposit_client_id' => DB::table('tbl_main_join_client')->where('portal_client_id', $depositDetail->client_id)->first()->main_client_id,
        //             'portal_loan_deposit_id' => $depositDetail->loan_id,
        //             'branch_id' => $branch_id,

        //          ]);
        //     }
        // }
    // }
    
    public function liveDeposit($loan_id){
        // loginToken
        $route = $this->getMainUrl37();
        $route_name = 'create-deposit';
        $routeurl = $route . $route_name;
        $authorization = $this->loginToken();

        $branch_id = DB::table('tbl_staff')->where('staff_code', Session::get('staff_code'))->first()->branch_id;
        $branch_code = $this->getBranchCode();
        $deposits = DB::connection('portal')->table($branch_code . '_deposit')->where('loan_id',$loan_id)->first();

        // return response()->json($deposits);



        // Loan id null or not found
        $loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $deposits->loan_id)->where('branch_id', $branch_id)->first();
        if ($loan_id) {
            $loan_id = $loan_id->main_loan_id;
        }


        // client
        $clientid = DB::table('tbl_main_join_client')->where('main_client_id', $deposits->client_id)->first();
        if ($clientid) {
            $clientid = $clientid->main_client_id;
        } else {
            $clientid = '';
        }

        $depclient_info = DB::connection('portal')->table('tbl_client_basic_info')->where('client_uniquekey', $deposits->client_id)->first();

        //get loan compulsory amount
        $compulsory = DB::table($branch_code . '_loan_charges_compulsory_join')
            ->leftJoin('loan_compulsory_type', $branch_code . '_loan_charges_compulsory_join.loan_compulsory_id', '=', 'loan_compulsory_type.compulsory_code')
            ->where($branch_code . '_loan_charges_compulsory_join.loan_unique_id', $deposits->loan_id)
            ->where($branch_code . '_loan_charges_compulsory_join.loan_compulsory_id', '!=', null)
            ->get();

        $loan_amount = DB::table($branch_code . '_loans')->where('loan_unique_id', $deposits->loan_id)->first()->loan_amount;
        $compulsory_deposit = 0;

        foreach ($compulsory as $c) {
            if ($c->charge_option == 2) {
                $compulsory_deposit += $loan_amount * ($c->saving_amount / 100);
            } else {
                $compulsory_deposit += $c->saving_amount;
            }
        }

        $main_acc_code = DB::table('account_chart_external_details')->where('main_acc_code', $deposits->acc_join_id)->first()->main_acc_id;
        // Create deposit
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $result = $client->post($routeurl, [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Bearer ' . $authorization,
            ],
            'form_params' => [
                'applicant_number_id' => $loan_id,
                'loan_deposit_date' =>  $deposits->deposit_pay_date,
                'referent_no' => $deposits->ref_no,
                'customer_name' => $depclient_info->name,
                'nrc' => $depclient_info->nrc,
                'client_id' => $clientid,
                'invoice_no' => $deposits->invoice_no,
                'compulsory_saving_amount' => $compulsory_deposit,
                'cash_acc_id' => $main_acc_code, //153620,
                'total_deposit' => $deposits->total_deposit_balance,
                'client_pay' => $deposits->paid_deposit_balance,
                'note' => $deposits->note,
                'branch_id' => $branch_id,
            ]

        ]);

        //return $result;
        $response = (string) $result->getBody();
        $response = json_decode($response); // Using this you can access any key like below
        if ($response->status_code == 200) {
            DB::table('tbl_main_join_loan_deposit')->insert([
                'main_loan_deposit_id' => DB::connection('main')->table('loans_' . $branch_id)->orderBy('id', 'desc')->first()->id,
                'main_loan_deposit_client_id' => DB::connection('main')->table('loans_' . $branch_id)->orderBy('id', 'desc')->first()->client_id,
                'portal_loan_deposit_id' => $deposits->loan_id,
                'branch_id' => $branch_id,
            ]);
        } 
    }

}
