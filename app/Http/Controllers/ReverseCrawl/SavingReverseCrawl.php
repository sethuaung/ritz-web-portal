<?php

namespace App\Http\Controllers\ReverseCrawl;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class SavingReverseCrawl extends Controller
{

	public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }

    public function index()
    {
        $reverse_crawl_savings = DB::table('tbl_crawling')->where('module_name', 'rev_saving_withdrawl')->get();
        return view('crawling.reverse_crawl.saving_reverse_crawl.index', compact('reverse_crawl_savings'));
    }

	public function getBranchCode($branchid)
    {
        $bcode = DB::table('tbl_branches')->where('id', $branchid)->first()->branch_code;
        return $bcode;
    }

    public function reverseCrawlSaving()
    {
    	$route = $this->getMainUrl37();

        $route_name = 'create-compulsory-savings';
        $routeurl = $route . $route_name;
        $authorization = $this->loginToken();
    	//$y = branchid with loop
        for($y = 1; $y <= 1; $y++) {
            $branch_code = $this->getBranchCode($y);
            $bcode = strtolower($branch_code);
            $savings = DB::connection('portal')->table($bcode.'_saving_withdrawl')->leftJoin($bcode.'_loans',$bcode.'_loans.loan_unique_id',$bcode.'_saving_withdrawl.saving_unique_id')->select($bcode.'_saving_withdrawl.*',$bcode.'_loans.loan_officer_id')->get();
        	for($i = 0; $i < count($savings); $i++) {
            
            	$saving_id = DB::table('tbl_main_join_saving')->where('portal_saving_withdrawl_id',$savings[$i]->saving_unique_id)->first();
                if($saving_id){
                    continue;
                }
            	// latest clients id generate
//             	$datasaving = DB::connection('main')->table('loan_compulsory_'.$y)->orderBy('compulsory_number', 'desc')->first();
//             	$compulsory_num = $datasaving->compulsory_number;
//             	$compulsory_num = ++$compulsory_num;
            
//             	$main_loan_id=$datasaving->loan_id;
//             	$main_loan_id= ++$main_loan_id;
            
//             	$seq= $datasaving->seq;
//             	$seq= ++$seq;
            
//             	$portal_client_id=DB::connection('portal')->table('tbl_main_join_client')->where('portal_client_id',$savings[$i]->client_idkey)->first()->main_client_id;
//             	$portal_staff_id=DB::connection('portal')->table($bcode.'_loans')->where('loan_unique_id',$savings[$i]->loan_unique_id)->first()->loan_officer_id;
// 				$main_staff_id=DB::table('tbl_main_join_staff')->where('portal_staff_id',$portal_staff_id)->first()->main_staff_id;
            
            	$clientDetail=DB::table('tbl_client_basic_inf')->where('client_uniquekey', $savings[$i]->client_idkey)->first();
            
            	$nrc = $clientDetail->nrc == NULL ? $clientDetail->old_nrc : $clientDetail->nrc;
            
            	$result = $client->post($routeurl, [
                        'headers' => [
                            'Content-Type' => 'application/x-www-form-urlencoded',
                            'Authorization' => 'Bearer ' . $authorization,
                        ],
                        'form_params' => [
                            "http_referrer" => "http://172.16.50.101/mis-core/public/admin/compulsorysavingactive"
      						"save_reference_id" => "1",
      						"compulsory_number" => DB::table('loan_compulsory_type')->first()->compulsory_code,
      						"withdrawal_date" => $savings[$i]->date,
      						"reference" => "00001",
      						"name" => $savings[$]->client_name,
      						"invoice" => 'inv-0001',
      						"nrc" => $nrc,
      						"product_name" => DB::table('loan_compulsory_type')->first()->compulsory_name,
      						"available_balance" => $savings[$i]->total_saving + $savings[$i]->total_interest, 
      						"principle" => $savings[$i]->total_saving,
      						"interest" => $savings[$i]->total_interest,
      						"cash_from" => "Available balance",
      						"cash_balance" => $savings[$i]->total_saving + $savings[$i]->total_interest,
      						"cash_withdrawal" => $savings[$i]->saving_withdrawl_amount,
      						"cash_remaining" => $savings[$i]->interest_withdrawl_amount,
      						"cash_out_id" => "138",
      						"paid_by_tran_id" => "2",
                        	"branch_id" => $y,
                        	"client_id" => DB::connection('main')->table('clients')->where('nrc_number', $nrc)->first()->id
                        ]
                    ]);
                
                	$response = (string) $result->getBody();
                	// return response()->json($result);
                	$response = json_decode($response); // Using this you can access any key like below
                	// return response()->json(['status_code' => 200, 'message' => 're-crawling loan success', 'data' => $response]);
                	if ($response->status_code == 200) {
                    	// return response()->json($response);
                    	$dataloan = DB::connection('main')->table('loans_' . $y)->orderBy('id', 'desc')->first();
                    		DB::table('tbl_main_join_saving')->insert([
               					'portal_saving_withdrawl_id' => $savings[$i]->saving_unique_id,
               					'main_loan_id' => $main_loan_id,
               					'portal_client_id'=> $savings[$i]->client_idkey,
               					'main_client_id' => $portal_client_id,
               					'created_at' => $savings[$i]->created_at,
               					'updated_at' => $savings[$i]->updated_at,
            			]);
                   	 	$z = $z + 1;
                	} else {
                    	// return response()->json(['status_code' => 400, 'message' => 're-crawling loan fail', 'data' => 'error']);
                    	continue;
                	}

            
            // 	DB::connection('main')->table('loan_compulsory_'.$y)->insert([
            //     	'loan_id'=> $main_loan_id ,
            //     	'compulsory_id'=> 5,
            //     	'product_name'=> "Saving 5%",
            //     	'saving_amount'=> 5,
            //     	'charge_option'=> 2,
            //     	'interest_rate'=> 1.25,
            //     	'compound_interest'=> 1,
            //     	'compulsory_product_type_id'=> 1,
            //     	'compulsory_number'=> $compulsory_num,
            //     	'seq'=> $seq,
            //     	'status' => "Yes",
            //     	'compulsory_status' =>"Active",
            //     	'balance' => $savings[$i]->saving_left,
            //     	'calculate_interest' => $savings[$i]->interest_left,
            //     	'created_at' => $savings[$i]->created_at,
            //     	'updated_at' => $savings[$i]->updated_at,
            //     	'created_by' => $main_staff_id,		//fix later
            //     	'updated_by' =>$main_staff_id,		//fix later
            //     	'principles' =>$savings[$i]->total_saving,
            //     	'interests' =>$savings[$i]->total_interest,
            //     	'principle_withdraw' => $savings[$i]->saving_withdrawl_amount,
            //     	'interest_withdraw' => $savings[$i]->interest_withdrawl_amount,
            //     	'cash_withdrawal' => 0,
            //     	'available_balance'=>$savings[$i]->saving_left,
            //     	'override_cycle'=> "no",
            //     	'client_id' => $portal_client_id,
            //     	'branch_id' => $y
            //     ]);
            // 	DB::table('tbl_main_join_saving')->insert([
            //    'portal_saving_withdrawl_id' => $savings[$i]->saving_unique_id,
            //    'main_loan_id' => $main_loan_id,
            //    'portal_client_id'=> $savings[$i]->client_idkey,
            //    'main_client_id' => $portal_client_id,
            //    'created_at' => $savings[$i]->created_at,
            //    'updated_at' => $savings[$i]->updated_at,
            // ]);

            }
        }
    
        $verify = DB::connection('main')->table('loan_compulsory_'.$y)->get();
        DB::connection('portal')->table('tbl_crawling')->where('module_name','rev_saving_withdrawl')->update([
            'data_count' => $y,
            'crawl_date' => date('Y-m-d H:i:s')
        ]);

        if ($verify) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => count($verify)]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
}
