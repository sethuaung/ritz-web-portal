<?php

namespace App\Http\Controllers\ReverseCrawl;

use App\Http\Controllers\Controller;
use App\Models\Crawl\Crawl;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use App\Models\Crawl\Crawl_Loan_Repayment_Schedule;


class RepaymentReverseCrawl extends Controller
{
    public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }

    public function getBranchCode($branchid)
    {
        $bcode = DB::table('tbl_branches')->where('id', $branchid)->first()->branch_code;
        return $bcode;
    }

    public function index()
    {
        $reverse_crawl_repayments = DB::table('tbl_crawling')->where('module_name', 'rev_repayment')->get();
        return view('crawling.reverse_crawl.loan_repayment_reverse_crawl.index', compact('reverse_crawl_repayments'));
    }


    public function reverseCrawlRepayment()
    {
        $branchcount = DB::table('tbl_branches')->count();
        $sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;

        $route = $this->getMainUrl37();

        $route_name = 'create-repayment';
        $routeurl = $route . $route_name;
        $authorization = $this->loginToken();

        $z = 0;
        for ($y = $sessionBranch; $y <= $sessionBranch; $y++) {
            $branch_id = $y;
            $branch_code = strtolower($this->getBranchCode($branch_id));
            $repayment_loans = DB::connection('portal')->table($branch_code . '_loans')->where('disbursement_status', 'Activated')->orWhere('disbursement_status', 'Closed')->get();

            // return response()->json(['status_code' => 200, 'message' => 'success', 'data' =>  $y]);

            for ($i = 0; $i < count($repayment_loans); $i++) {
                if (DB::table('tbl_main_join_loan_disbursement')->where('portal_loan_disbursement_id', $repayment_loans[$i]->loan_unique_id)->first()) {
                } else {
                    continue;
                }

                // Loan id null or not found
                $loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $repayment_loans[$i]->loan_unique_id)->where('branch_id', $y)->first();
                if ($loan_id) {
                    $loan_id = $loan_id->main_loan_id;
                } else {
                    continue;
                }
                // client
                $main_join_clientid = DB::table('tbl_main_join_client')->where('portal_client_id', $repayment_loans[$i]->client_id)->first();
                if ($main_join_clientid) {
                    $clientid = $main_join_clientid->main_client_id;
                    $clientcode = $main_join_clientid->main_client_code;
                } else {
                    continue;
                }
                // Client Info
                $client_info = DB::connection('portal')->table('tbl_client_basic_info')->where('client_uniquekey', $repayment_loans[$i]->client_id)->first();

                //add scheduels join
                //                 $LoansSchedule = DB::connection('portal')->table($branch_code . '_loans_schedule')->where('loan_unique_id', $repayment_loans[$i]->loan_unique_id)->get();
                //                 $mainSchedule = DB::connection('main')->table('loan_disbursement_calculate_' . $y)->where('disbursement_id', $loan_id)->get();

                //                 for ($h = 0; $h < count($LoansSchedule); $h++) {
                //                     if (DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$h]->id)->where('branch_id', $y)->first()) {
                //                         break;
                //                     } else {
                // if ($mainSchedule[$h]->no == ($h + 1)) {
                //     $mainJoin = new Crawl_Loan_Repayment_Schedule();
                //     $mainJoin->portal_disbursement_id = $LoansSchedule[$h]->loan_unique_id;
                //     $mainJoin->portal_disbursement_schedule_id = $LoansSchedule[$h]->id;
                //     $mainJoin->main_disbursement_schedule_id = $mainSchedule[$h]->id;
                //     $mainJoin->branch_id = $y;
                //     $mainJoin->save();
                // }
                // }
                // }

                // Main join in repayment schedule list
                // $main_join_repayment_schedule = DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_id', $repayment_loans[$i]->loan_unique_id)->first();

                // if ($main_join_repayment_schedule) {
                // start repayment

                $schedules = DB::table($branch_code . '_loans_schedule')->where('loan_unique_id', $repayment_loans[$i]->loan_unique_id)->get();
                // $main_acc_code=DB::table('account_chart_external_details')->where('main_acc_code',$repayment_loans[$i]->cash_acc_id)->first->main_acc_id;

                //-------------
                $due_schedules = DB::table($branch_code . '_repayment_due')->where('disbursement_id', $repayment_loans[$i]->loan_unique_id)->get();
                foreach ($due_schedules as $ds) {
                    $main_schedule_id = DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $ds->schedule_id)->where('branch_id', $y)->first()->main_disbursement_schedule_id;
                    $s = DB::table($branch_code.'_loans_schedule')->where('id', $ds->schedule_id)->first();
                    if (DB::connection('main')->table('loan_disbursement_calculate_' . $y)->where('id', $main_schedule_id)->first()->payment_status == 'paid') {
                        continue;
                    } else {
                        $client = new \GuzzleHttp\Client(['verify' => false]);
                        $result = $client->post($routeurl, [
                            'headers' => [
                                'Content-Type' => 'application/x-www-form-urlencoded',
                                'Authorization' => 'Bearer ' . $authorization,
                            ],
                            'form_params' => [
                                'disbursement_id' => $loan_id,
                                'payment_number' => $ds->payment_number,
                                'client_number' => $clientcode,
                                'client_name' => $client_info->name,
                                'client_id' => $clientid,
                                'receipt_no' => $ds->payment_number,
                                'over_days' => $ds->over_days == NULL ? 0 : $ds->over_days,
                                'penalty_amount' => $ds->penalty == NULL ? 0 : $ds->penalty,
                                'principle' => $ds->principal,
                                'interest' =>  $ds->interest,
                                'compulsory_saving' => $ds->compulsory_saving == NULL ? 0 : $ds->compulsory_saving,
                                'other_payment' => 0,
                                'total_payment' => $ds->total_balance,
                                'payment' => $ds->total_balance,
                                'payment_date' => $ds->payment_date,
                                'owed_balance' => $ds->own_balance == NULL ? 0 : $ds->own_balance,
                                'principle_balance' => $s->final_amount,
                                'payment_method' => $ds->payment_method,
                                'cash_acc_id' => $ds->cash_acc_id, //138
                                'disbursement_detail_id' =>  $main_schedule_id,
                                'pre_repayment' => 0, // due - 0
                                'late_repayment' => 0, // due - 0
                                'branch_id' => $branch_id,
                                'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $ds->counter_name)->first()->main_staff_id,
                            ]
                        ]);
                        $response = (string) $result->getBody();
                        $response = json_decode($response);
                        if ($response->status_code == 200) {
                            // return response()->json($response);
                            // DB::table('tbl_main_join_repayment')->insert([
                            //     'portal_repayment_id' => $portal_repayment_schedule_id,
                            //     'portal_loan_id' => $portal_repayment_id,
                            //     'main_repayment_id' => $loan_id,
                            //     'main_repayment_disbursement_detail_id' => $main_repayment_schedule_id,
                            // ]);

                            $z = ++$z;
                        } else {
                            //return response()->json(['status_code' => 200, 'message' => 'success', 'data' =>  null]);
                            continue;
                        }
                    }
                }

                $pre_schedules = DB::table($branch_code . '_repayment_pre')->where('disbursement_id', $repayment_loans[$i]->loan_unique_id)->get();
                foreach ($pre_schedules as $ps) {
                    $main_schedule_id = DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $ps->schedule_id)->where('branch_id', $y)->first()->main_disbursement_schedule_id;
                    $s = DB::table($branch_code.'_loans_schedule')->where('id', $ds->schedule_id)->first();
                    if (DB::connection('main')->table('loan_disbursement_calculate_' . $y)->where('id', $main_schedule_id)->first()->payment_status == 'paid') {
                        continue;
                    } else {
                        $client = new \GuzzleHttp\Client(['verify' => false]);
                        $result = $client->post($routeurl, [
                            'headers' => [
                                'Content-Type' => 'application/x-www-form-urlencoded',
                                'Authorization' => 'Bearer ' . $authorization,
                            ],
                            'form_params' => [
                                'disbursement_id' => $loan_id,
                                'payment_number' => $ps->payment_number,
                                'disbursement_detail_id' =>  $main_schedule_id,
                                'receipt_no' => $ps->payment_number,
                                'client_id' => $clientid,
                                'client_number' => $clientcode,
                                'client_name' => $client_info->name,
                                'over_days' => $ps->over_days == NULL ? 0 : $ps->over_days,
                                'penalty_amount' => $ps->penalty == NULL ? 0 : $ps->penalty,
                                'principle' => $ps->principal,
                                'interest' =>  $ps->interest,
                                'compulsory_saving' => $ps->compulsory_saving == NULL ? 0 : $ps->compulsory_saving,
                                'owed_balance' => $ps->own_balance == NULL ? 0 : $ps->own_balance,
                                'principle_balance' => $s->final_amount,
                                'other_payment' => 0,
                                'total_payment' => $ps->total_amount_paid,
                                'cash_acc_id' => $ps->cash_acc_id,
                                'payment' => $ps->total_amount_paid,
                                'payment_date' =>  $ps->payment_date,
                                'payment_method' => $ps->payment_method,
                                'pre_repayment' => 1, //pre
                                'late_repayment' => 0,
                                'branch_id' => $branch_id,
                                'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $ps->counter_name)->first()->main_staff_id,
                            ]
                        ]);
                        $response = (string) $result->getBody();
                        $response = json_decode($response);
                        if ($response->status_code == 200) {
                            // DB::table('tbl_main_join_repayment')->insert([
                            //     'portal_repayment_id' => $portal_repayment_schedule_id,
                            //     'portal_loan_id' => $portal_repayment_id,
                            //     'main_repayment_id' => $loan_id,
                            //     'main_repayment_disbursement_detail_id' => $main_repayment_schedule_id,
                            // ]);

                            $z = ++$z;
                        } else {
                            // return response()->json(['status_code' => 200, 'message' => 'success', 'data' =>  null]);
                            continue;
                        }
                    }
                }

                $late_schedules = DB::table($branch_code . '_repayment_late')->where('disbursement_id', $repayment_loans[$i]->loan_unique_id)->get();
                foreach ($late_schedules as $ls) {
                    $main_schedule_id = DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $ls->schedule_id)->where('branch_id', $y)->first()->main_disbursement_schedule_id;
                    $s = DB::table($branch_code.'_loans_schedule')->where('id', $ds->schedule_id)->first();
                    if (DB::connection('main')->table('loan_disbursement_calculate_' . $y)->where('id', $main_schedule_id)->first()->payment_status == 'paid') {
                        continue;
                    } else {
                        if (DB::table('tbl_main_join_repayment')->where('portal_schedule_id', $ls->schedule_id)->orderBy('id', 'desc')->first()) {
                            if (DB::table('tbl_main_join_repayment')->where('portal_repayment_id', $ls->repayment_id)->first()) {
                                continue;
                            }
                            if (DB::table('tbl_main_join_repayment')->where('portal_schedule_id', $ls->schedule_id)->orderBy('id', 'desc')->first()->main_owed_balance == $ls->own_balance) {
                                continue;
                            }
                        }

                        $status = '';

                        $client = new \GuzzleHttp\Client(['verify' => false]);
                        $result = $client->post($routeurl, [
                            'headers' => [
                                'Content-Type' => 'application/x-www-form-urlencoded',
                                'Authorization' => 'Bearer ' . $authorization,
                            ],
                            'form_params' => [
                                'disbursement_id' => $loan_id,
                                'payment_number' => $ls->payment_number,
                                'disbursement_detail_id' =>  $main_schedule_id,
                                'receipt_no' => $ls->payment_number,
                                'client_id' => $clientid,
                                'client_number' => $clientcode,
                                'client_name' => $client_info->name,
                                'over_days' => $ls->over_days == NULL ? 0 : $ls->over_days,
                                'penalty_amount' => $ls->penalty == NULL ? 0 : $ls->penalty,
                                'principle' => $ls->principal_paid,
                                'interest' =>  $ls->interest_paid,
                                'compulsory_saving' => $ls->compulsory_saving == NULL ? 0 : $ls->compulsory_saving,
                                'owed_balance' => $ls->own_balance,
                                'principle_balance' => $s->final_amount,
                                'other_payment' => 0,
                                'total_payment' => $ls->total_amount_paid,
                                'cash_acc_id' => $ls->cash_acc_id,
                                'payment' => $ls->total_amount_paid,
                                'payment_date' =>  $ls->payment_date,
                                'payment_method' => $ls->payment_method,
                                'pre_repayment' => 0,
                                'late_repayment' => 1, //late
                                'branch_id' => $branch_id,
                                'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $ls->counter_name)->first()->main_staff_id,
                            ]
                        ]);

                        $status = 'success';

                        //make paid schedule that is paid with late and owed balance = 0
                        if ($ls->own_balance == 0) {
                            $client = new \GuzzleHttp\Client(['verify' => false]);
                            $result = $client->post($routeurl, [
                                'headers' => [
                                    'Content-Type' => 'application/x-www-form-urlencoded',
                                    'Authorization' => 'Bearer ' . $authorization,
                                ],
                                'form_params' => [
                                    'disbursement_id' => $loan_id,
                                    'payment_number' => $ls->payment_number,
                                    'client_number' => $clientcode,
                                    'client_name' => $client_info->name,
                                    'client_id' => $clientid,
                                    'receipt_no' => $ls->payment_number,
                                    'over_days' => $ls->over_days == NULL ? 0 : $ls->over_days,
                                    'penalty_amount' => $ls->penalty == NULL ? 0 : $ls->penalty,
                                    'principle' => 0,
                                    'interest' =>  0,
                                    'compulsory_saving' => $ls->compulsory_saving == NULL ? 0 : $ls->compulsory_saving,
                                    'other_payment' => 0,
                                    'total_payment' => 0,
                                    'payment' => 0,
                                    'payment_date' =>  $ls->payment_date,
                                    'owed_balance' => 0,
                                    'principle_balance' => 0,
                                    'payment_method' => $ls->payment_method,
                                    'cash_acc_id' => $ls->cash_acc_id,
                                    'disbursement_detail_id' =>  $main_schedule_id,
                                    'pre_repayment' => 0, // due - 0
                                    'late_repayment' => 0, // due - 0
                                    'branch_id' => $branch_id,
                                    'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $ls->counter_name)->first()->main_staff_id,
                                ]
                            ]);

                            $status = 'success';
                        }

                        // $response = (string) $result->getBody();
                        // $response = json_decode($response);
                        // if ($response->status_code == 200) {
                        if ($status == 'success') {
                            DB::table('tbl_main_join_repayment')->insert([
                                'portal_repayment_id' => $ls->repayment_id,
                                'portal_schedule_id' => $s->id,
                                'main_owed_balance' => $ls->own_balance,
                                'main_repayment_disbursement_detail_id' => $main_schedule_id,
                            ]);

                            $z = ++$z;
                        } else {
                            //return response()->json(['status_code' => 200, 'message' => 'success', 'data' =>  null]);
                            continue;
                        }
                    }
                }
                //-------------

                // foreach ($schedules as $s) {
                //     $main_schedule_id = DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $s->id)->where('branch_id', $y)->first()->main_disbursement_schedule_id;
                //     if (DB::connection('main')->table('loan_disbursement_calculate_' . $y)->where('id', $main_schedule_id)->first()->payment_status == 'paid') {
                //         continue;
                //     } else {
                //         //due repayment
                //         if (DB::table($branch_code . '_repayment_due')->where('schedule_id', $s->id)->first()) {
                //             $repayDetail = DB::table($branch_code . '_repayment_due')->where('schedule_id', $s->id)->first();

                //             $client = new \GuzzleHttp\Client(['verify' => false]);
                //             $result = $client->post($routeurl, [
                //                 'headers' => [
                //                     'Content-Type' => 'application/x-www-form-urlencoded',
                //                     'Authorization' => 'Bearer ' . $authorization,
                //                 ],
                //                 'form_params' => [
                //                     'disbursement_id' => $loan_id,
                //                     'payment_number' => $repayDetail->payment_number,
                //                     'client_number' => $clientcode,
                //                     'client_name' => $client_info->name,
                //                     'client_id' => $clientid,
                //                     'receipt_no' => $repayDetail->payment_number,
                //                     'over_days' => $repayDetail->over_days == NULL ? 0 : $repayDetail->over_days,
                //                     'penalty_amount' => $repayDetail->penalty == NULL ? 0 : $repayDetail->penalty,
                //                     'principle' => $repayDetail->principal,
                //                     'interest' =>  $repayDetail->interest,
                //                     'compulsory_saving' => $repayDetail->compulsory_saving == NULL ? 0 : $repayDetail->compulsory_saving,
                //                     'other_payment' => 0,
                //                     'total_payment' => $repayDetail->total_balance,
                //                     'payment' => $repayDetail->total_balance,
                //                     'payment_date' => $repayDetail->repayment_date,
                //                     'owed_balance' => $repayDetail->own_balance == NULL ? 0 : $repayDetail->own_balance,
                //                     'principle_balance' => $s->final_amount,
                //                     'payment_method' => $repayDetail->payment_method,
                //                     'cash_acc_id' => $repayDetail->cash_acc_id, //138
                //                     'disbursement_detail_id' =>  $main_schedule_id,
                //                     'pre_repayment' => 0, // due - 0
                //                     'late_repayment' => 0, // due - 0
                //                     'branch_id' => $branch_id,
                //                 ]
                //             ]);
                //             $response = (string) $result->getBody();
                //             $response = json_decode($response);
                //             if ($response->status_code == 200) {
                //                 // return response()->json($response);
                //                 // DB::table('tbl_main_join_repayment')->insert([
                //                 //     'portal_repayment_id' => $portal_repayment_schedule_id,
                //                 //     'portal_loan_id' => $portal_repayment_id,
                //                 //     'main_repayment_id' => $loan_id,
                //                 //     'main_repayment_disbursement_detail_id' => $main_repayment_schedule_id,
                //                 // ]);

                //                 $z = ++$z;
                //             } else {
                //                 //return response()->json(['status_code' => 200, 'message' => 'success', 'data' =>  null]);
                //                 continue;
                //             }
                //         } else if (DB::table($branch_code . '_repayment_pre')->where('schedule_id', $s->id)->first()) {
                //             $repayDetail = DB::table($branch_code . '_repayment_pre')->where('schedule_id', $s->id)->first();

                //             $client = new \GuzzleHttp\Client(['verify' => false]);
                //             $result = $client->post($routeurl, [
                //                 'headers' => [
                //                     'Content-Type' => 'application/x-www-form-urlencoded',
                //                     'Authorization' => 'Bearer ' . $authorization,
                //                 ],
                //                 'form_params' => [
                //                     'disbursement_id' => $loan_id,
                //                     'payment_number' => $repayDetail->payment_number,
                //                     'disbursement_detail_id' =>  $main_schedule_id,
                //                     'receipt_no' => $repayDetail->payment_number,
                //                     'client_id' => $clientid,
                //                     'client_number' => $clientcode,
                //                     'client_name' => $client_info->name,
                //                     'over_days' => $repayDetail->over_days == NULL ? 0 : $repayDetail->over_days,
                //                     'penalty_amount' => $repayDetail->penalty == NULL ? 0 : $repayDetail->penalty,
                //                     'principle' => $repayDetail->principal,
                //                     'interest' =>  $repayDetail->interest,
                //                     'compulsory_saving' => $repayDetail->compulsory_saving == NULL ? 0 : $repayDetail->compulsory_saving,
                //                     'owed_balance' => $repayDetail->own_balance == NULL ? 0 : $repayDetail->own_balance,
                //                     'principle_balance' => $s->final_amount,
                //                     'other_payment' => 0,
                //                     'total_payment' => $repayDetail->total_amount_paid,
                //                     'cash_acc_id' => $repayDetail->cash_acc_id,
                //                     'payment' => $repayDetail->total_amount_paid,
                //                     'payment_date' =>  $repayDetail->repayment_date,
                //                     'payment_method' => $repayDetail->payment_method,
                //                     'pre_repayment' => 1, //pre
                //                     'late_repayment' => 0,
                //                     'branch_id' => $branch_id,
                //                 ]
                //             ]);
                //             $response = (string) $result->getBody();
                //             $response = json_decode($response);
                //             if ($response->status_code == 200) {
                //                 // DB::table('tbl_main_join_repayment')->insert([
                //                 //     'portal_repayment_id' => $portal_repayment_schedule_id,
                //                 //     'portal_loan_id' => $portal_repayment_id,
                //                 //     'main_repayment_id' => $loan_id,
                //                 //     'main_repayment_disbursement_detail_id' => $main_repayment_schedule_id,
                //                 // ]);

                //                 $z = ++$z;
                //             } else {
                //                 // return response()->json(['status_code' => 200, 'message' => 'success', 'data' =>  null]);
                //                 continue;
                //             }
                //         } else if (DB::table($branch_code . '_repayment_late')->where('schedule_id', $s->id)->first()) {
                //             $repayDetails = DB::table($branch_code . '_repayment_late')->where('schedule_id', $s->id)->get();
                //             // return response()->json($repayDetails);

                //             foreach ($repayDetails as $repayDetail) {
                //                 // return response()->json(DB::table('tbl_main_join_repayment')->where('portal_repayment_id', 12)->orderBy('id', 'desc')->first());
                //                 if (DB::table('tbl_main_join_repayment')->where('portal_schedule_id', $repayDetail->schedule_id)->orderBy('id', 'desc')->first()) {
                //                     if (DB::table('tbl_main_join_repayment')->where('portal_repayment_id', $repayDetail->repayment_id)->first()) {
                //                         continue;
                //                     }
                //                     if (DB::table('tbl_main_join_repayment')->where('portal_schedule_id', $repayDetail->schedule_id)->orderBy('id', 'desc')->first()->main_owed_balance == $repayDetail->own_balance) {
                //                         continue;
                //                     }
                //                 }

                //                 $status = '';

                //                 $client = new \GuzzleHttp\Client(['verify' => false]);
                //                 $result = $client->post($routeurl, [
                //                     'headers' => [
                //                         'Content-Type' => 'application/x-www-form-urlencoded',
                //                         'Authorization' => 'Bearer ' . $authorization,
                //                     ],
                //                     'form_params' => [
                //                         'disbursement_id' => $loan_id,
                //                         'payment_number' => $repayDetail->payment_number,
                //                         'disbursement_detail_id' =>  $main_schedule_id,
                //                         'receipt_no' => $repayDetail->payment_number,
                //                         'client_id' => $clientid,
                //                         'client_number' => $clientcode,
                //                         'client_name' => $client_info->name,
                //                         'over_days' => $repayDetail->over_days == NULL ? 0 : $repayDetail->over_days,
                //                         'penalty_amount' => $repayDetail->penalty == NULL ? 0 : $repayDetail->penalty,
                //                         'principle' => $repayDetail->principal_paid,
                //                         'interest' =>  $repayDetail->interest_paid,
                //                         'compulsory_saving' => $repayDetail->compulsory_saving == NULL ? 0 : $repayDetail->compulsory_saving,
                //                         'owed_balance' => $repayDetail->own_balance,
                //                         'principle_balance' => $s->final_amount,
                //                         'other_payment' => 0,
                //                         'total_payment' => $repayDetail->total_amount_paid,
                //                         'cash_acc_id' => $repayDetail->cash_acc_id,
                //                         'payment' => $repayDetail->total_amount_paid,
                //                         'payment_date' =>  $repayDetail->repayment_date,
                //                         'payment_method' => $repayDetail->payment_method,
                //                         'pre_repayment' => 0,
                //                         'late_repayment' => 1, //late
                //                         'branch_id' => $branch_id,
                //                     ]
                //                 ]);

                //                 $status = 'success';

                //                 //make paid schedule that is paid with late and owed balance = 0
                //                 if ($repayDetail->own_balance == 0) {
                //                     $client = new \GuzzleHttp\Client(['verify' => false]);
                //                     $result = $client->post($routeurl, [
                //                         'headers' => [
                //                             'Content-Type' => 'application/x-www-form-urlencoded',
                //                             'Authorization' => 'Bearer ' . $authorization,
                //                         ],
                //                         'form_params' => [
                //                             'disbursement_id' => $loan_id,
                //                             'payment_number' => $repayDetail->payment_number,
                //                             'client_number' => $clientcode,
                //                             'client_name' => $client_info->name,
                //                             'client_id' => $clientid,
                //                             'receipt_no' => $repayDetail->payment_number,
                //                             'over_days' => $repayDetail->over_days == NULL ? 0 : $repayDetail->over_days,
                //                             'penalty_amount' => $repayDetail->penalty == NULL ? 0 : $repayDetail->penalty,
                //                             'principle' => 0,
                //                             'interest' =>  0,
                //                             'compulsory_saving' => $repayDetail->compulsory_saving == NULL ? 0 : $repayDetail->compulsory_saving,
                //                             'other_payment' => 0,
                //                             'total_payment' => 0,
                //                             'payment' => 0,
                //                             'payment_date' =>  $repayDetail->repayment_date,
                //                             'owed_balance' => 0,
                //                             'principle_balance' => 0,
                //                             'payment_method' => $repayDetail->payment_method,
                //                             'cash_acc_id' => $repayDetail->cash_acc_id,
                //                             'disbursement_detail_id' =>  $main_schedule_id,
                //                             'pre_repayment' => 0, // due - 0
                //                             'late_repayment' => 0, // due - 0
                //                             'branch_id' => $branch_id,
                //                         ]
                //                     ]);

                //                     $status = 'success';
                //                 }

                //                 // $response = (string) $result->getBody();
                //                 // $response = json_decode($response);
                //                 // if ($response->status_code == 200) {
                //                 if ($status == 'success') {
                //                     DB::table('tbl_main_join_repayment')->insert([
                //                         'portal_repayment_id' => $repayDetail->repayment_id,
                //                         'portal_schedule_id' => $s->id,
                //                         'main_owed_balance' => $repayDetail->own_balance,
                //                         'main_repayment_disbursement_detail_id' => $main_schedule_id,
                //                     ]);

                //                     $z = ++$z;
                //                 } else {
                //                     //return response()->json(['status_code' => 200, 'message' => 'success', 'data' =>  null]);
                //                     continue;
                //                 }
                //             }
                //         }
                //     }
                // }

                //end repayment
                // }
            }

            //$verify = DB::connection('main')->table($table)->get();
            DB::connection('portal')->table('tbl_crawling')->where('module_name', 'rev_repayment')->update([
                'data_count' => $z,
                'crawl_date' => date('Y-m-d H:i:s')
            ]);
            if ($z) {
                return response()->json(['status_code' => 200, 'message' => 'success', 'data' =>  $z]);
            } else {
                return response()->json(['status_code' => 400, 'message' => 're-crawling data not found', 'data' => null]);
            }
        }
    }
}
