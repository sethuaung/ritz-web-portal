<?php

namespace App\Http\Controllers\ReverseCrawl;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Http;


class RevCrawlingController extends Controller
{
    //reverse staff start ----------------------

    public function staffRecrawlIndex()
    {
        $reverse_staffs = DB::table('tbl_crawling')
            ->where('module_name', 'rev_staff')
            ->get();
        return view('crawling.reverse_crawl.staff_reverse_crawl.index', compact('reverse_staffs'));
    }

    public function reverseCrawlStaffs()
    {
        // $sessionBranch = DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id;
        $sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;
        // return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $sessionBranch]);

        $staffs = DB::connection('portal')
            ->table('tbl_staff')
            ->leftjoin('tbl_branches', 'tbl_staff.branch_id', '=', 'tbl_branches.id')
            ->select('tbl_staff.*', 'tbl_branches.*')
            ->where('tbl_staff.branch_id', $sessionBranch)
            ->get();

        $branches = DB::table('tbl_branches')->get();
        $z = 0;

        for ($y = $sessionBranch; $y <= $sessionBranch; $y++) {
            for ($i = 0; $i < count($staffs); $i++) {
                $staff_check = DB::table('tbl_main_join_staff')
                    ->where('portal_staff_id', $staffs[$i]->staff_code)
                    ->first();
                if ($staff_check) {
                    continue;
                }

                $branchCode = DB::connection('main')
                    ->table('branches')
                    ->where('id', $y)
                    ->first()->code;
                $last_user_code = DB::connection('main')
                    ->table('user_branches')
                    ->leftJoin('users', 'users.id', '=', 'user_branches.user_id')
                    ->orderBy('users.id', 'desc')
                    ->where('user_branches.branch_id', $y)
                    ->select('users.user_code')
                    ->get()->first();
                if ($last_user_code) {
                    $new_user_code = $last_user_code->user_code;
                    $user_code = ++$new_user_code;
                } else {
                    $user_code = 'MKT-' . $branchCode . '-0001';
                }
                // $count = DB::connection('main')
                //     ->table('user_branches')
                //     ->count();
                // $count = $count + 1;
                // $count = str_pad($count, 4, 0, STR_PAD_LEFT);
                // $user_code = 'MKT-' . $branchCode . '-' . $count;
                // return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $staffs[9]->email]);
                DB::connection('main')
                    ->table('users')
                    ->insert([
                        //'id' => $staffs[$i]->id,
                        'finger_print_no' => $staffs[$i]->finger_biometric,
                        'name' => $staffs[$i]->name,
                        'phone' => $staffs[$i]->phone,
                        'email' => $staffs[$i]->email,
                        'photo' => $staffs[$i]->photo,
                        'branch_id' => $staffs[$i]->branch_id,
                        'center_leader_id' => null, //$staffs[$i]->center_leader_id,
                        'email_verified_at' => $staffs[$i]->email_verified_at,
                        'password' => $staffs[$i]->user_password,
                        //'remember_token' => $staffs[$i]->user_token,
                        'created_at' => $staffs[$i]->created_at,
                        'updated_at' => $staffs[$i]->updated_at,
                        //'created_by' =>
                        //'updated_by' =>
                        'user_code' => $user_code,
                        'br_code' => $staffs[$i]->branch_code,
                        //'cen_code' => $staffs[$i]->center_uniquekey
                    ]);
                DB::connection('main')
                    ->table('model_has_roles')
                    ->insert([
                        'role_id' => $staffs[$i]->role_id,
                        'model_type' => 'App\Models\BackpackUser',
                        'model_id' => DB::connection('main')
                            ->table('users')
                            ->orderBy('id', 'desc')
                            ->first()->id,
                        'created_by' => 0,
                        'updated_by' => 0,
                    ]);

                $z = ++$z;

                $mainStaffid = DB::connection('main')
                    ->table('users')
                    ->orderBy('id', 'desc')
                    ->first();
                DB::table('tbl_main_join_staff')->insert([
                    'main_staff_id' => $mainStaffid->id,
                    'portal_staff_id' => $staffs[$i]->staff_code,
                    'main_staff_code' => $mainStaffid->user_code,
                    'main_staff_email' => $mainStaffid->email
                ]);

                DB::connection('main')
                    ->table('user_branches')
                    ->insert([
                        'user_id' => $mainStaffid->id,
                        'branch_id' => $y,
                    ]);
            }
        }

        //$verify = DB::connection('main')->table('users')->get();
        DB::connection('portal')
            ->table('tbl_crawling')
            ->where('module_name', 'rev_staff')
            ->update([
                'data_count' => $z,
                'crawl_date' => date('Y-m-d H:i:s'),
                'status' => 'finish',
            ]);

        if ($z) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $z]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //reverse staff end ----------------------

    //reverse center start -------------------

    public function centerRecrawlIndex()
    {
        $reverse_centers = DB::table('tbl_crawling')
            ->where('module_name', 'rev_center')
            ->get();
        return view('crawling.reverse_crawl.center_reverse_crawl.index', compact('reverse_centers'));
    }

    public function reverseCrawlCenters()
    {
        $sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;

        $centers = DB::connection('portal')
            ->table('tbl_center')
            ->leftJoin('tbl_branches', 'tbl_center.branch_id', '=', 'tbl_branches.id')
            ->where('tbl_center.branch_id', $sessionBranch)
            ->select('tbl_center.*', 'tbl_branches.branch_code AS branchcode')
            ->get();

        $branches = DB::table('tbl_branches')->get();

        // return response()->json($centers);
        //DB::connection('mkt_backup')->statement('ALTER TABLE center_leaders AUTO_INCREMENT = 1');
        $z = 0;
        for ($y = $sessionBranch; $y <= $sessionBranch; $y++) {
            for ($i = 0; $i < count($centers); $i++) {
                if (
                    DB::table('tbl_main_join_center')
                    ->where('portal_center_id', $centers[$i]->center_uniquekey)
                    ->first()
                ) {
                    continue;
                }

                // $division_id = DB::connection('main')
                //     ->table('addresses')
                //     ->where('id', $centers[$i]->division_id)
                //     ->first()->code;
                // $district_id = DB::connection('main')
                //     ->table('addresses')
                //     ->where('id', $centers[$i]->district_id)
                //     ->first()->code;
                // $village_id = DB::connection('main')
                //     ->table('addresses')
                //     ->where('id', $centers[$i]->village_id)
                //     ->first()->code;
                // $commue_id = DB::connection('main')
                //     ->table('addresses')
                //     ->where('id', $centers[$i]->township_id)
                //     ->first()->code;

                // $branchCode = DB::connection('main')->table('branches')->where('id', 1)->first()->code;
                // $count =  DB::connection('main')->table('center_leaders')->where('branch_id', 1)->count();
                // $count = $count + 1;
                // $count = str_pad($count, 4, 0, STR_PAD_LEFT);
                // $center_code = 'ct-'.$branchCode.'-'.$count;

                // $branchCode = DB::connection('main')->table('branches')->where('id', 1)->first()->code;
                $lastID = DB::connection('main')
                    ->table('center_leaders')
                    ->where('branch_id', $y)
                    ->orderBy('title', 'desc')
                    ->first()->code;
                // $centerCount = DB::connection('main')
                //     ->table('center_leaders')
                //     ->where('branch_id', $y)
                //     ->count();
                // $mainBCode = DB::connection('main')
                //     ->table('branches')
                //     ->where('id', $y)
                //     ->first()
                //     ->code;
                // $newID = 'CT-' . $mainBCode . '-' . str_pad($centerCount + 1, 4, '0', STR_PAD_LEFT);
                $newID = ++$lastID;

                $user_id = '';
                // if($centers[$i]->type_status == 'staff') {
                // if(DB::table('tbl_main_join_staff')->where('portal_staff_id', $centers[$i]->staff_client_id)->first()){
                // 		$user_id = DB::table('tbl_main_join_staff')->where('portal_staff_id', $centers[$i]->staff_client_id)->first()->main_staff_id;
                // }else{
                // continue;
                // }
                // } else {
                if (
                    DB::table('tbl_main_join_staff')
                    ->where('portal_staff_id', $centers[$i]->staff_id)
                    ->first()
                ) {
                    $user_id = DB::table('tbl_main_join_staff')
                        ->where('portal_staff_id', $centers[$i]->staff_id)
                        ->first()->main_staff_id;
                } else {
                    continue;
                }
                // }

                DB::connection('main')
                    ->table('center_leaders')
                    ->insert([
                        //'id' => $centers[$i]->id,
                        'branch_id' => $centers[$i]->branch_id,
                        'code' => $newID,
                        'title' => $newID,
                        //'phone' => $centers[$i]->phone_primary,
                        //'location'
                        //'description'
                        'created_at' => $centers[$i]->created_at,
                        'updated_at' => $centers[$i]->updated_at,
                        'address' => $centers[$i]->address,
                        'province_id' => $centers[$i]->division_id,
                        'district_id' => $centers[$i]->district_id,
                        'commune_id' => $centers[$i]->township_id,
                        'village_id' => $centers[$i]->village_id,
                        'phone'=>$centers[$i]->phone_number,
                        //'seq'
                        //'created_by'
                        //'updated_by'
                        'br_code' => $centers[$i]->branchcode,
                        'user_id' => $user_id,
                    ]);

                // DB::connection('main')->table('clients')->where('id', $user_id)->update([
                // 'you_are_a_center_leader' => 'Yes',
                // 'center_leader_id' => DB::connection('main')->table('center_leaders')->orderby('id', 'desc')->first()->id //center id
                // ]);

                DB::table('tbl_main_join_center')->insert([
                    'main_center_id' => DB::connection('main')
                        ->table('center_leaders')
                        ->where('code', $newID)
                        ->first()->id,
                    'portal_center_id' => $centers[$i]->center_uniquekey,
                    'main_center_code' => $newID,
                    //'main_center_leader_id' => $user_id, //client id(center leader)
                ]);

                DB::connection('main')
                    ->table('user_centers')
                    ->insert([
                        'user_id' => DB::table('tbl_main_join_staff')
                            ->where('portal_staff_id', $centers[$i]->staff_id)
                            ->first()->main_staff_id,
                        'center_id' => DB::connection('main')
                            ->table('center_leaders')
                            ->where('code', $newID)
                            ->first()->id,
                    ]);

                $z = ++$z;
            }
        }

        //$verify = DB::connection('main')->table('center_leaders')->get();

        // $count = DB::connection('main')
        //     ->table('center_leaders')
        //     ->where('branch_id', $y)
        //     ->get()
        //     ->count();

        DB::connection('portal')
            ->table('tbl_crawling')
            ->where('module_name', 'rev_center')
            ->update([
                'data_count' => $z,
                'crawl_date' => date('Y-m-d H:i:s'),
            ]);

        if ($z) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $z]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //reverse center end -------------------

    //reverse group start ------------------
    public function groupRecrawlIndex()
    {
        $reverse_groups = DB::table('tbl_crawling')
            ->where('module_name', 'group')
            ->get();
        return view('crawling.reverse_crawl.group_reverse_crawl.index', compact('reverse_groups'));
    }

    public function reverseCrawlGroups()
    {
        $sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;

        $groups = DB::connection('portal')
            ->table('tbl_group')
            ->leftJoin('tbl_branches', 'tbl_group.branch_id', '=', 'tbl_branches.id')
            ->where('tbl_group.branch_id', $sessionBranch)
            // ->leftJoin('tbl_main_join_center','tbl_group.center_id','=','tbl_main_join_center.portal_center_id')
            // ->leftJoin('tbl_main_join_group','tbl_group.group_uniquekey','=','tbl_main_join_group.portal_group_id')
            ->get();
        // return response()->json($groups);

        $branches = DB::table('tbl_branches')->get();

        $z = 0;
        for ($y = $sessionBranch; $y <= $sessionBranch; $y++) {
            for ($i = 0; $i < count($groups); $i++) {
                if (
                    DB::table('tbl_main_join_group')
                    ->where('portal_group_id', $groups[$i]->group_uniquekey)
                    ->first()
                ) {
                    continue;
                }
                $group_code_generate = DB::connection('main')
                    ->table('group_loans')
                    ->where('branch_id', $y)
                    ->orderBy('group_code', 'desc')
                    ->first()->group_code;
                $group_code_new = ++$group_code_generate;
                // return response()->json($group_code_new);

                if (
                    DB::table('tbl_main_join_center')
                    ->where('portal_center_id', $groups[$i]->center_id)
                    ->first()
                ) {
                    $mainCenterID = DB::table('tbl_main_join_center')
                        ->where('portal_center_id', $groups[$i]->center_id)
                        ->first()->main_center_id;
                } else {
                    continue;
                }

                DB::connection('main')
                    ->table('group_loans')
                    ->insert([
                        //'id' => $groups[$i]->id,
                        'group_code' => $group_code_new,
                        'group_name' => $group_code_new,
                        'center_id' => $mainCenterID,
                        'created_at' => $groups[$i]->created_at,
                        'updated_at' => $groups[$i]->updated_at,
                        //'created_by' => $groups[$i]->staff_id,
                        //'updated_by' => $groups[$i]->staff_id,
                        'branch_id' => $groups[$i]->branch_id,
                    ]);

                DB::table('tbl_main_join_group')->insert([
                    'main_group_id' => DB::connection('main')
                        ->table('group_loans')
                        ->where('group_code', $group_code_new)
                        ->first()->id,
                    'main_group_code' => DB::connection('main')
                        ->table('group_loans')
                        ->where('group_code', $group_code_new)
                        ->first()->group_code,
                    'portal_group_id' => $groups[$i]->group_uniquekey,
                ]);
                $z = ++$z;
            }
        }

        //$verify = DB::connection('main')->table('group_loans')->get();
        DB::connection('portal')
            ->table('tbl_crawling')
            ->where('module_name', 'rev_group')
            ->update([
                'data_count' => $z,
                'crawl_date' => date('Y-m-d H:i:s'),
            ]);

        if ($z) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $z]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //reverse group end ------------------

    //reverse guarantor start ------------------
    public function guarantorRecrawlIndex()
    {
        $reverse_guarantors = DB::table('tbl_crawling')
            ->where('module_name', 'rev_guarantor')
            ->get();
        return view('crawling.reverse_crawl.guarantor_reverse_crawl.index', compact('reverse_guarantors'));
    }

    public function reverseCrawlGuarantors()
    {

        $sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;

        $guarantors = DB::connection('portal')
            ->table('tbl_guarantors')
            ->leftJoin('tbl_center', 'tbl_center.branch_id', '=', 'tbl_guarantors.branch_id')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_guarantors.branch_id')
            ->where('tbl_guarantors.branch_id', $sessionBranch)
            ->select('tbl_center.*', 'tbl_guarantors.*', 'tbl_branches.id as branch_id')
            ->get();
        // return response()->json($guarantors);

        $branches = DB::table('tbl_branches')->get();

        $z = 0;
        for ($y = $sessionBranch; $y <= $sessionBranch; $y++) {
            for ($i = 0; $i < count($guarantors); $i++) {
                if ($guarantors[$i]->staff_id) {
                    if (
                        DB::table('tbl_main_join_guarantor')
                        ->where('portal_guarantor_id', $guarantors[$i]->guarantor_uniquekey)
                        ->first()
                    ) {
                        continue;
                    }

                    // $division_id = DB::connection('main')->table('addresses')->where('id', $guarantors[$i]->division_id)->first()->code;
                    // $district_id = DB::connection('main')->table('addresses')->where('id', $guarantors[$i]->district_id)->first()->code;
                    // $village_id = DB::connection('main')->table('addresses')->where('id', $guarantors[$i]->village_id)->first()->code;
                    // $commue_id = DB::connection('main')->table('addresses')->where('id', $guarantors[$i]->township_id)->first()->code;

                    DB::connection('main')
                        ->table('guarantors')
                        ->insert([
                            //'id' => $guarantors[$i}->id,
                            'nrc_number' => $guarantors[$i]->nrc == null ? $guarantors[$i]->old_nrc : $guarantors[$i]->nrc,
                            //                'title' => $guarantors->gender == 'male' ? 'Mr' : 'Ms',
                            'full_name_en' => $guarantors[$i]->name,
                            'full_name_mm' => $guarantors[$i]->name_mm,
                            'mobile' => $guarantors[$i]->phone_primary,
                            'phone' => $guarantors[$i]->phone_secondary,
                            'email' => $guarantors[$i]->email,
                            'dob' => $guarantors[$i]->dob,
                            //'working_status_id',
                            //'place_of_birth',
                            'photo' => $guarantors[$i]->guarantor_photo,
                            //'attach_file',
                            //'country_id',
                            'address' => $guarantors[$i]->address_primary,
                            'province_id' => $guarantors[$i]->division_id == null ? '' : $guarantors[$i]->division_id,
                            'district_id' => $guarantors[$i]->district_id == null ? '' : $guarantors[$i]->district_id,
                            'commune_id' => $guarantors[$i]->township_id == null ? '' : $guarantors[$i]->township_id,
                            'village_id' => $guarantors[$i]->village_id == null ? '' : $guarantors[$i]->village_id,
                            'ward_id' => $guarantors[$i]->quarter_id == null ? '' : $guarantors[$i]->quarter_id,
                            //'street_number',
                            //'house_number',
                            //'description',
                            //'marital_status',
                            //                'spouse_gender',
                            //'spouse_name'
                            //'spouse_date_of_birth'
                            //'number_of_child'
                            //'spouse_mobile_phone'
                            //'user_id',
                            'branch_id' => $guarantors[$i]->branch_id,
                            'center_leader_id' => $guarantors[$i]->staff_client_id,
                            //'business_info',
                            'income' => $guarantors[$i]->income,
                            //'nrc_type'
                            //'seq',
                            'created_at' => $guarantors[$i]->created_at,
                            'updated_at' => $guarantors[$i]->updated_at,
                            //'created_by' => $guarantors[$i]->staff_client_id,
                            //a bit unsure about this
                            //'updated_by' => $guarantors[$i]->staff_client_id,
                            //'annual_income'
                        ]);

                    $z = ++$z;

                    DB::table('tbl_main_join_guarantor')->insert([
                        'main_guarantor_id' => DB::connection('main')
                            ->table('guarantors')
                            ->where('nrc_number', $guarantors[$i]->old_nrc)
                            ->orWhere('nrc_number', $guarantors[$i]->nrc)
                            ->first()->id,
                        'portal_guarantor_id' => $guarantors[$i]->guarantor_uniquekey,
                    ]);
                }
            }
        }

        //$verify = DB::connection('main')->table('guarantors')->get();

        DB::connection('portal')
            ->table('tbl_crawling')
            ->where('module_name', 'rev_guarantor')
            ->update([
                'data_count' => $z,
                'crawl_date' => date('Y-m-d H:i:s'),
            ]);

        if ($z) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $z]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
    //reverse guarantor end ------------------
}
