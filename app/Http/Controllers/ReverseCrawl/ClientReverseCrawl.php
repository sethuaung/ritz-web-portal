<?php

namespace App\Http\Controllers\ReverseCrawl;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientReverseCrawl extends Controller
{
    public function index()
    {
        $reverse_crawl_clients = DB::table('tbl_crawling')->where('module_name', 'rev_client')->get();
        return view('crawling.reverse_crawl.client_reverse_crawl.index', compact('reverse_crawl_clients'));
    }

    public function reverseCrawlClients()
    {
        $sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;

        for ($y = $sessionBranch; $y <= $sessionBranch; $y++) {


            $clients = DB::connection('portal')->table('tbl_client_basic_info')
                ->leftjoin('tbl_client_family_info', 'tbl_client_family_info.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftjoin('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftjoin('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftjoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftjoin('tbl_client_survey_ownership', 'tbl_client_join.client_id', '=', 'tbl_client_survey_ownership.client_uniquekey')
                // ->leftJoin('tbl_group', function ($advancedLeftJoin) {
                //     $advancedLeftJoin->on('tbl_client_join.group_id', '=', 'tbl_group.group_uniquekey');
                // })
                // ->leftJoin('tbl_center', function ($advancedLeftJoin) {
                //     $advancedLeftJoin->on('tbl_client_join.center_id', '=', 'tbl_center.center_uniquekey');
                // })
                ->leftJoin('tbl_educations', function ($advancedLeftJoin) {
                    $advancedLeftJoin->on('tbl_client_basic_info.education_id', '=', 'tbl_educations.id');
                })
                ->leftJoin('tbl_branches', function ($advancedLeftJoin) {
                    $advancedLeftJoin->on('tbl_branches.id', '=', 'tbl_client_join.branch_id'); //edited
                })
                ->leftJoin('tbl_center', 'tbl_center.staff_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftJoin('tbl_group', 'tbl_group.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_join.branch_id', $y)
                ->select('tbl_group.client_id AS group_leader_id', 'tbl_client_basic_info.*', 'tbl_client_basic_info.created_at AS Ccreated_at','tbl_client_basic_info.updated_at AS Cupdated_at','tbl_client_basic_info.phone_primary AS client_phone', 'tbl_client_family_info.*', 'tbl_client_job_main.*', 'tbl_client_photo.*', 'tbl_client_join.*', 'tbl_client_survey_ownership.*', 'tbl_educations.*', 'tbl_branches.branch_name AS branch_name', 'tbl_center.staff_client_id')
                ->get();
            // dd($clients);
            $check = DB::connection('main')->table('clients')->select('clients.name', 'clients.dob', 'clients.nrc_number')->get();
            $z = 0;
            // return response()->json($clients);
            $mainJoins = DB::table('tbl_main_join_client')->get();
            for ($i = 0; $i < count($mainJoins); $i++) {

                // $mainClientId = DB::table('tbl_main_join_client')->where('portal_client_id', $clients[$i]->client_uniquekey)->first()->main_client_code;
                // $portalClientId = DB::table('tbl_main_join_client')->where('portal_client_id', $clients[$i]->client_uniquekey)->first()->portal_client_id;
                // return response()->json(DB::table('tbl_client_basic_info')->where('client_uniquekey', 'AB-01-0002436')->first());
                if (DB::table('tbl_client_basic_info')->where('client_uniquekey', $mainJoins[$i]->portal_client_id)->first()) {
                    continue;
                } else {
                    // return response()->json(DB::table('tbl_client_basic_info')->where('client_uniquekey', 'AB-01-0002436')->first());
                    DB::connection('main')->table('clients')->where('client_number', '=', $mainJoins[$i]->main_client_code)->delete();
                    //delete at main join
                    DB::table('tbl_main_join_client')->where('main_client_code', $mainJoins[$i]->main_client_code)->delete();
                }
            }

            for ($i = 0; $i < count($clients); $i++) {


                if (DB::table('tbl_main_join_client')->where('portal_client_id', $clients[$i]->client_uniquekey)->first()) {

                    //update start
                    //                 $mjMainClientID = DB::table('tbl_main_join_client')->where('portal_client_id', $clients[$i]->client_uniquekey)->first()->main_client_id;

                    //                 // group
                    //                 // $portal_group_id=DB::table('tbl_group')->where('group_uniquekey',$clients[$i]->group_id)->first()
                    //                 $groupid = DB::table('tbl_main_join_group')->where('portal_group_id', $clients[$i]->group_id)->first();
                    //                 if ($groupid) {
                    //                     $groupid = $groupid->main_group_id;
                    //                 } else {
                    //                     $groupid = '';
                    //                 }

                    //                 // center
                    //                 $centerid = DB::table('tbl_main_join_center')->where('portal_center_id', $clients[$i]->center_id)->first();
                    //                 if ($centerid) {
                    //                     $centerid = $centerid->main_center_id;
                    //                 } else {
                    //                     $centerid = '';
                    //                 }

                    //                 //client_type
                    //                 if ($clients[$i]->client_status == 'new') {
                    //                     $customer_group_id = "1";
                    //                 } elseif ($clients[$i]->client_status == 'active') {
                    //                     $customer_group_id = "1";
                    //                 } elseif ($clients[$i]->client_status == 'dropout') {
                    //                     $customer_group_id = "3";
                    //                 } elseif ($clients[$i]->client_status == 'dead') {
                    //                     $customer_group_id = "2";
                    //                 } elseif ($clients[$i]->client_status == 'rejoin') {
                    //                     $customer_group_id = "4";
                    //                 } elseif ($clients[$i]->client_status == 'substitute') {
                    //                     $customer_group_id = "5";
                    //                 } elseif ($clients[$i]->client_status == 'dormant') {
                    //                     $customer_group_id = "13";
                    //                 } elseif ($clients[$i]->client_status == 'reject') {
                    //                     $customer_group_id = "13";
                    //                 }

                    //                 // latest clients id generate
                    //                 $dataclients = DB::connection('main')->table('clients')->where('branch_id', $clients[$i]->branch_id)->orderBy('client_number', 'desc')->first();
                    //                 $main_client_generate_id = $dataclients->client_number;
                    //                 $main_client_generate_id = ++$main_client_generate_id;

                    //                 // $you_are_center_leader = 'No';
                    //                 // if ($clients[$i]->staff_client_id != NULL) {
                    //                 // $you_are_center_leader = 'Yes';
                    //                 // }
                    //                 // $you_are_group_leader='No';
                    //                 // if($clients[$i]->group_leader_id != NULL){
                    //                 // $you_are_group_leader='Yes';
                    //                 // }

                    //                 DB::connection('main')->table('clients')->where([
                    //                 	['branch_id','=',$clients[$i]->branch_id],
                    //                 	['id','=', $mjMainClientID],
                    //                 	])->update([
                    //                     'branch_id' => $clients[$i]->branch_id,
                    //                     'name' => $clients[$i]->name,
                    //                     'name_other' => $clients[$i]->name_mm,
                    //                     // 'center_code' => $centerid, // change center
                    //                     'center_name' => $clients[$i]->branch_name, // change later
                    //                     'register_date' => $clients[$i]->registration_date,
                    //                     'customer_group_id' => $customer_group_id, //change group
                    //                     'gender' => $clients[$i]->gender,
                    //                     'dob' => $clients[$i]->dob,
                    //                     'education' => $clients[$i]->education_name,
                    //                     'primary_phone_number' => $clients[$i]->client_phone,
                    //                     'alternate_phone_number' => $clients[$i]->phone_secondary,
                    //                     'nrc_number' => $clients[$i]->nrc == null ? $clients[$i]->old_nrc : $clients[$i]->nrc,
                    //                     'nrc_type' => $clients[$i]->nrc == null ? 'Old Format' : 'New Format',
                    //                     'marital_status' => $clients[$i]->marital_status,
                    //                     'status' => 'Active',
                    //                     'father_name' => $clients[$i]->father_name,
                    //                     'husband_name' => $clients[$i]->spouse_name,
                    //                     'occupation_of_husband' => $clients[$i]->occupation_of_spouse,
                    //                     'no_children_in_family' => $clients[$i]->no_of_children,
                    //                     'no_of_working_people' => $clients[$i]->no_of_working_family,
                    //                     'no_of_person_in_family' => $clients[$i]->no_of_family,
                    //                     'address1' => $clients[$i]->address_primary,
                    //                     'address2' => $clients[$i]->address_secondary,
                    //                     'family_registration_copy' => $clients[$i]->registration_family_form_front,
                    //                     'photo_of_client' => $clients[$i]->client_photo,
                    //                     'nrc_photo' => $clients[$i]->recognition_front,
                    //                     'scan_finger_print' => $clients[$i]->finger_print_bio,
                    //                     'survey_id' => $clients[$i]->id,
                    //                     'remark' => 'active',
                    //                     'center_leader_id' => $centerid,
                    //                     // 'loan_officer_id'=>,
                    //                     'you_are_a_group_leader' => $clients[$i]->you_are_a_group_leader,
                    //                     'you_are_a_center_leader' => $clients[$i]->you_are_a_center_leader,
                    //                     //'ownership_of_farmland'=>$clients[$i]->land,
                    //                     //'ownership'=>$clients[$i]->ownership,
                    //                     //'province_id' => $division_id,
                    //                     //'district_id' => $district_id,
                    //                     //'commune_id' => $commue_id,
                    //                     //'village_id' => $village_id,
                    //                     'loan_officer_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $clients[$i]->staff_id)->first()->main_staff_id,
                    //                     'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $clients[$i]->staff_id)->first()->main_staff_id,
                    //                     'updated_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $clients[$i]->staff_id)->first()->main_staff_id,
                    //                     'created_at' => $clients[$i]->created_at,
                    //                     'updated_at' => $clients[$i]->updated_at,
                    //                     'company_address1' => $clients[$i]->company_address,
                    //                 ]);
                    //update end

                    continue;
                }

                // center
                $centerid = DB::table('tbl_main_join_center')->where('portal_center_id', $clients[$i]->center_id)->first();
                if ($centerid) {
                    $centerid = $centerid->main_center_id;
                } else {
                    continue;
                }

                // group
                // $portal_group_id=DB::table('tbl_group')->where('group_uniquekey',$clients[$i]->group_id)->first()
                $groupid = DB::table('tbl_main_join_group')->where('portal_group_id', $clients[$i]->group_id)->first();
                if ($groupid) {
                    $groupid = $groupid->main_group_id;
                } else {
                    continue;
                }


                //client_type
                if ($clients[$i]->client_status == 'new') {
                    $customer_group_id = "1";
                } elseif ($clients[$i]->client_status == 'active') {
                    $customer_group_id = "1";
                } elseif ($clients[$i]->client_status == 'dropout') {
                    $customer_group_id = "3";
                } elseif ($clients[$i]->client_status == 'dead') {
                    $customer_group_id = "2";
                } elseif ($clients[$i]->client_status == 'rejoin') {
                    $customer_group_id = "4";
                } elseif ($clients[$i]->client_status == 'substitute') {
                    $customer_group_id = "5";
                } elseif ($clients[$i]->client_status == 'dormant') {
                    $customer_group_id = "13";
                } elseif ($clients[$i]->client_status == 'reject') {
                    $customer_group_id = "13";
                }

                // latest clients id generate
                $dataclients = DB::connection('main')->table('clients')->where('branch_id', $clients[$i]->branch_id)->orderBy('client_number', 'desc')->first();
                $main_client_generate_id = $dataclients->client_number;
                $main_client_generate_id = ++$main_client_generate_id;

                // $you_are_center_leader = 'No';
                // if ($clients[$i]->staff_client_id != NULL) {
                // $you_are_center_leader = 'Yes';
                // }
                // $you_are_group_leader='No';
                // if($clients[$i]->group_leader_id != NULL){
                // $you_are_group_leader='Yes';
                // }
                // $clients[$i]->village_id;
                // $clients[$i]->province_id;
                // $clients[$i]->quarter_id;
                // $clients[$i]->township_id;
                // $clients[$i]->district_id;
                // $clients[$i]->division_id;
                // $clients[$i]->city_id;


                // $portal_division=DB::connection('portal')->table('tbl_divisions')->where('id',$clients[$i]->division_id)->first();
                // if($portal_division){
                //     $portal_division_name=$portal_division->division_name;
                //     $main_division_id=DB::connection('main')->table('addresses')->where('name',$portal_division_name)->where('type','=','state')->first()->code;
                // }else{
                //     $main_division_id=null;
                // }

                // $portal_district=DB::connection('portal')->table('tbl_districts')->where('id',$clients[$i]->district_id)->first();
                // if($portal_district){
                //     $portal_district_name=$portal_district->district_name;
                //     $main_district_id=DB::connection('main')->table('addresses')->where('name',$portal_district_name)->where('type','=','districts')->first()->code;
                // }else{
                //     $main_district_id=null;
                // }

                // $portal_township=DB::connection('portal')->table('tbl_townships')->where('id',$clients[$i]->township_id)->first();
                // if($portal_township){
                //     $portal_township_name=$portal_township->township_name;
                //     $main_commune_id=DB::connection('main')->table('addresses')->where('name',$portal_township_name)->where('type','=','township')->first()->code;
                // }else{
                //     $main_commune_id=null;
                // }

                // $portal_village=DB::connection('portal')->table('tbl_villages')->where('id',$clients[$i]->village_id)->first();
                // if($portal_village){
                //     $portal_village_name=$portal_village->village_name;
                //     $main_village_id=DB::connection('main')->table('addresses')->where('name',$portal_village_name)->first()->code;
                // }else{
                //     $main_village_id=null;
                // }


                DB::connection('main')->table('clients')->insert([
                    'branch_id' => $clients[$i]->branch_id,
                    'name' => $clients[$i]->name,
                    'name_other' => $clients[$i]->name_mm,
                    // 'center_code' => $centerid, // change center
                    // 'center_name' => $clients[$i]->branch_name, // change later
                    'register_date' => date('Y-m-d', strtotime($clients[$i]->registration_date)),
                    'client_number' => $main_client_generate_id,
                    'customer_group_id' => $customer_group_id, //change group
                    'gender' => $clients[$i]->gender,
                    'dob' => date('Y-m-d', strtotime($clients[$i]->dob)),
                    'education' => $clients[$i]->education_name,
                    'primary_phone_number' => $clients[$i]->client_phone,
                    'alternate_phone_number' => $clients[$i]->phone_secondary,
                    'nrc_number' => $clients[$i]->nrc == null ? $clients[$i]->old_nrc : $clients[$i]->nrc,
                    'nrc_type' => $clients[$i]->nrc == null ? 'Old Format' : 'New Format',
                    'marital_status' => $clients[$i]->marital_status,
                    'status' => 'Active',
                    'father_name' => $clients[$i]->father_name,
                    'husband_name' => $clients[$i]->spouse_name,
                    'occupation_of_husband' => $clients[$i]->occupation_of_spouse,
                    'no_children_in_family' => $clients[$i]->no_of_children,
                    'no_of_working_people' => $clients[$i]->no_of_working_family,
                    'no_of_person_in_family' => $clients[$i]->no_of_family,
                    'address1' => $clients[$i]->address_primary,
                    'address2' => $clients[$i]->address_secondary,
                    'family_registration_copy' => $clients[$i]->registration_family_form_front,
                    'photo_of_client' => $clients[$i]->client_photo,
                    'nrc_photo' => $clients[$i]->recognition_front,
                    'scan_finger_print' => $clients[$i]->finger_print_bio,
                    'survey_id' => $clients[$i]->id,
                    'remark' => 'active',
                    'center_leader_id' => $centerid,
                    // 'loan_officer_id'=>,
                    'you_are_a_group_leader' => $clients[$i]->you_are_a_group_leader,
                    'you_are_a_center_leader' => $clients[$i]->you_are_a_center_leader,
                    //'ownership_of_farmland'=>$clients[$i]->land,
                    //'ownership'=>$clients[$i]->ownership,
                    'province_id' => $clients[$i]->division_id,
                    'district_id' => $clients[$i]->district_id,
                    'commune_id' => $clients[$i]->township_id,
                    'village_id' => $clients[$i]->village_id,
                    'ward_id'=>$clients[$i]->quarter_id,
                    'loan_officer_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $clients[$i]->staff_id)->first()->main_staff_id,
                    'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $clients[$i]->staff_id)->first()->main_staff_id,
                    'updated_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $clients[$i]->staff_id)->first()->main_staff_id,
                    'created_at' => date('Y-m-d H:i:s', strtotime($clients[$i]->Ccreated_at)),
                    'updated_at' => date('Y-m-d H:i:s', strtotime($clients[$i]->Cupdated_at)),
                    'company_address1' => $clients[$i]->company_address,
                ]);
                $main_client_id = DB::connection('main')->table('clients')->where('client_number', $main_client_generate_id)->first()->id;
                $job = DB::table('tbl_client_job_main')->where('client_uniquekey', $clients[$i]->client_uniquekey)
                    ->leftJoin('tbl_industry_type', 'tbl_industry_type.id', 'tbl_client_job_main.industry_id')
                    ->leftJoin('tbl_job_position', 'tbl_job_position.id', 'tbl_client_job_main.job_position_id')
                    ->leftJoin('tbl_job_department', 'tbl_job_department.id', 'tbl_client_job_main.department_id')
                    ->select(
                        'tbl_client_job_main.*',
                        'tbl_industry_type.industry_name AS employment_industry',
                        'tbl_job_position.job_position_name AS position',
                        'tbl_job_department.department_name AS department'

                    )->first();
                if ($job->job_status == 'Has Job') {
                    $employment_status = 'Active';
                } else {
                    $employment_status = 'Inactive';
                }
                DB::connection('main')->table('employee_status')->insert([
                    'client_id' => $main_client_id,
                    'position' => $job->position,
                    'employment_status' => $employment_status,
                    'employment_industry' => strtolower($job->employment_industry),
                    'senior_level' => null,
                    'branch' => $clients[$i]->branch_name,
                    'company_name' => $job->company_name,
                    'department' => $job->department,
                    'work_phone' => $job->company_phone,
                    'work_phone2' => $job->company_phone,
                    'working_experience' => $job->experience,
                    'basic_salary' => $job->salary,
                    'company_address' => $job->company_address

                ]);

                DB::table('tbl_main_join_client')->insert([
                    'main_client_id' => $main_client_id,
                    'portal_client_id' => $clients[$i]->client_uniquekey,
                    'portal_branch_id' => $clients[$i]->branch_id,
                    'main_client_code' => $main_client_generate_id
                ]);

                // update main join center leader
                // DB::table('tbl_main_join_center')->where('portal_center_id', $clients[$i]->center_id)->update([
                //     'main_center_leader_id' => DB::connection('main')->table('clients')->where('client_number', $main_client_generate_id)->first()->id, //client id(center leader)
                // ]);

                $z = ++$z;
            }
        }

        // $verify = DB::connection('main')->table('clients')->where('branch_id', 1)->get();
        DB::connection('portal')->table('tbl_crawling')->where('module_name', '=', 'rev_client')->update([
            'module_name' => 'rev_client',
            'data_count' => $z,
            'crawl_date' => date('Y-m-d H:i:s')
        ]);

        if ($z) {
            return response()->json(['status_code' => 200, 'message' => 'success', 'data' => $z]);
        }
        return response()->json(['status_code' => 400, 'message' => 'not found', 'data' => null]);
    }
}
