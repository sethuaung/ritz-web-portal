<?php

namespace App\Http\Controllers\ReverseCrawl;

use App\Http\Controllers\Controller;
use App\Models\Crawl\Crawl;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class DisbursementReverseCrawl extends Controller
{
    public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }

    public function getBranchCode($branchid)
    {
        $bcode = DB::table('tbl_branches')->where('id', $branchid)->first()->branch_code;
        return $bcode;
    }

    public function index()
    {
        $reverse_crawl_disbursements = DB::table('tbl_crawling')->where('module_name','rev_disbursement')->get();
        return view('crawling.reverse_crawl.loan_disbursement_crawl.index', compact('reverse_crawl_disbursements'));
    }

    public function reverseCrawlDisbursement()
    {
        $branchcount = DB::table('tbl_branches')->count();
    	$sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;
    	$bcode = DB::table('tbl_branches')->where('id', $sessionBranch)->first()->branch_code;
    	$bcode = strtolower($bcode);

        $route = $this->getMainUrl37();

        $route_name = 'create-disbursement';
        $routeurl = $route.$route_name;
        $authorization = $this->loginToken();

        $x = 0;
        for($y = $sessionBranch; $y <= $sessionBranch; $y++) {
            $branch_id = $y;
            $branch_code = strtolower($this->getBranchCode($branch_id));
            $disbursements = DB::connection('portal')->table($branch_code.'_disbursement')->get();
            $table = 'loans_'.$branch_id;
        
        	// return response()->json($disbursements[8]->first_installment_date);

            for($i = 0; $i < count($disbursements); $i++) {
                if(DB::table('tbl_main_join_loan_disbursement')->where('portal_loan_disbursement_id', $disbursements[$i]->loan_id)->where('branch_id', $y)->first()) {
                    continue;
                }
            
                // Loan id null or not found
                $loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $disbursements[$i]->loan_id)->where('branch_id', $y)->first();
                if($loan_id){
                    $loan_id = $loan_id->main_loan_id;
                }else{
                    continue;
                }
                // client
                $clientid = DB::table('tbl_main_join_client')->where('portal_client_id', $disbursements[$i]->client_id)->first();
                if($clientid){
                    $clientid = $clientid->main_client_id;
                }else{
                    $clientid = '';
                }
            
            
                // staff
                $staffid = DB::table('tbl_main_join_staff')->where('portal_staff_id', $disbursements[$i]->loan_officer_id)->first();
                if($staffid){
                    $staffid = $staffid->main_staff_id;
                }else{
                    $staffid = '';
                }
            
                // group
                $groupid = DB::table('tbl_main_join_group')->where('portal_group_id', $disbursements[$i]->group_id)->first();
                if($groupid){
                    $groupid = $groupid->main_group_id;
                }else{
                    $groupid = '';
                }
            
            	DB::connection('main')->table('loans_'.$branch_id)->where('id', $loan_id)->update([
                	'disbursement_status' => 'Activated'
                ]);

                $depclient_info = DB::connection('portal')->table('tbl_client_basic_info')->where('client_uniquekey', $disbursements[$i]->client_id)->first();
            	//get loan compulsory amount
            	$compulsory = DB::table($branch_code . '_loan_charges_compulsory_join')
                        ->leftJoin('loan_compulsory_type', $branch_code . '_loan_charges_compulsory_join.loan_compulsory_id', '=', 'loan_compulsory_type.compulsory_code')
                        ->where($branch_code . '_loan_charges_compulsory_join.loan_unique_id', $disbursements[$i]->loan_id)
                        ->where($branch_code . '_loan_charges_compulsory_join.loan_compulsory_id', '!=', null)
                        ->get();
            
            	$loan_amount = DB::table($branch_code.'_loans')->where('loan_unique_id', $disbursements[$i]->loan_id)->first()->loan_amount;
            	$compulsory_deposit = 0;
            
            	foreach ($compulsory as $c) {
                        if ($c->charge_option == 2) {
                            $compulsory_deposit += $loan_amount * ($c->saving_amount / 100);
                        } else {
                            $compulsory_deposit += $c->saving_amount;
                        }
                }
            	$main_acc_code=DB::table('account_chart_external_details')->where('main_acc_code',$disbursements[$i]->cash_acc_id)->first()->main_acc_id;

            	$client = new \GuzzleHttp\Client(['verify' => false]);
                $result = $client->post($routeurl, [
            		'headers' => [
                		'Content-Type' => 'application/x-www-form-urlencoded',
                		'Authorization' => 'Bearer '.$authorization,
            		],
            		'form_params' => [
                        'contract_id' => $loan_id,
                        'paid_disbursement_date' => $disbursements->processing_date,
                        'first_payment_date' =>  $disbursements->first_installment_date,
                        'client_id' => $clientid,
                        'reference' => 002250, //$disbursements[$i]->remark,
                        'client_name' => $depclient_info->name,
                        'client_nrc' => $depclient_info->nrc == NULL ? $depclient_info->old_nrc : $depclient_info->nrc,
                        'invoice_no' => 'inv-02345678',
                        'compulsory_saving' => 0,
                        'loan_amount' => $disbursements->disbursed_amount,
                        'total_money_disburse' => $disbursements->disbursed_amount,
                        'cash_out_id' => $main_acc_code,
                        'paid_by_tran_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', session()->get('staff_code'))->first()->main_staff_id,
                        'cash_pay' =>  $disbursements->disbursed_amount,
                        'disburse_by' => 'loan-officer',
                        'branch_id' => $branch_id,
        
                        // 'loan_id'=> $loan_id,
                        // 'welfare_fund'=> 0,
                        // 'loan_process_fee'=> 0,
                        'disburse_amount'=> $disbursements->disbursed_amount,
                        // 'seq'=> 0,
                        'user_id'=> DB::table('tbl_main_join_staff')->where('portal_staff_id', session()->get('staff_code'))->first()->main_staff_id,
                        // 'contract_no'=> '',
                        // 'seq_contract'=> 0,
                        'created_by'=>  DB::table('tbl_main_join_staff')->where('portal_staff_id', session()->get('staff_code'))->first()->main_staff_id,
                        'updated_by'=>  DB::table('tbl_main_join_staff')->where('portal_staff_id', session()->get('staff_code'))->first()->main_staff_id,
                        // 'total_service_charge'=> 0,
                        // 'acc_code'=> '153620',
                        // 'group_tran_id'=> $groupid,
                        // 'product_id'=> 0,
                        // 'deposit'=> 0,
                        // 'service_charge'=> null,
                        // 'service_charge_id'=> null,
                        // 'charge_id'=> null,
                    ]
                ]);

            // return $result;
           		$response = (string) $result->getBody();
				$response =json_decode($response); // Using this you can access any key like below
    			if($response->status_code == 200){
                // return response()->json($response);
                    DB::table('tbl_main_join_loan_disbursement')->insert([
                        'main_loan_disbursement_id' => DB::connection('main')->table('loans_'.$branch_id)->orderBy('id', 'desc')->first()->id,
                    	'main_loan_disbursement_client_id' => DB::connection('main')->table('loans_'.$branch_id)->orderBy('id', 'desc')->first()->client_id,
                        'portal_loan_disbursement_id' => $disbursements[$i]->loan_id,
                        'branch_id' => $y,
                     ]);
                
                	$LoansSchedule = DB::connection('portal')->table($bcode . '_loans_schedule')->where('loan_unique_id', $disbursements[$i]->loan_id)->get();
                	$main_disbursement_id = DB::connection('portal')->table('tbl_main_join_loan')->where('portal_loan_id', $disbursements[$i]->loan_id)->where('branch_id', $y)->first()->main_loan_id;
                	$RpDetail = DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $main_disbursement_id)->get();

                	for ($g = 0; $g < count($LoansSchedule); $g++) {
                    	if ($RpDetail[$g]->no == ($g + 1)) {
                        	DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$g]->id)->update([
                            	'portal_disbursement_id' => $LoansSchedule[$g]->loan_unique_id,
                            	'portal_disbursement_schedule_id' => $LoansSchedule[$g]->id,
                            	'main_disbursement_schedule_id' => $RpDetail[$g]->id,
                            	'branch_id' => $branch_id
                        	]);
                    	}
                	}
     
                     $x = ++$x;
                }else{
                	// return response()->json(['status_code' => 400, 'message' => 're-crawling data not found', 'data' => null]);
                    continue;
                }
            }

            //$verify = DB::connection('main')->table($table)->get();
            DB::connection('portal')->table('tbl_crawling')->where('module_name', 'rev_disbursement')->update([
                'data_count' => $x,
                'crawl_date' => date('Y-m-d H:i:s')
            ]);
            if ($x) {
                return response()->json(['status_code' => 200, 'message' => 'success', 'data' =>  $x]);
            }else{
                return response()->json(['status_code' => 400, 'message' => 're-crawling data not found', 'data' => null]);
            }
            
        }


    }

}
