<?php

namespace App\Http\Controllers\ReverseCrawl;

use App\Http\Controllers\Controller;
use App\Models\Crawl\Crawl;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class DepositReverseCrawl extends Controller
{
    public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }

    public function getBranchCode($branchid)
    {
        $bcode = DB::table('tbl_branches')->where('id', $branchid)->first()->branch_code;
        return $bcode;
    }

    public function index()
    {
        $reverse_crawl_deposits = DB::table('tbl_crawling')->where('module_name','rev_deposit')->get();
        return view('crawling.reverse_crawl.loan_deposit_reverse_crawl.index', compact('reverse_crawl_deposits'));
    }

    public function reverseCrawlDeposit()
    {
        $branchcount = DB::table('tbl_branches')->count();
    	$sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;

    	// loginToken
        $route = $this->getMainUrl37();
        $route_name = 'create-deposit';
        $routeurl = $route.$route_name;
        $authorization = $this->loginToken();
    
        $x = 0;
        for($y = $sessionBranch; $y <= $sessionBranch; $y++) {
            $branch_id = $y;
            $branch_code = strtolower($this->getBranchCode($branch_id));
            $deposits = DB::connection('portal')->table($branch_code.'_deposit')->get();
            $table = 'loans_'.$branch_id;
        
        	// return response()->json($deposits);

            for($i = 0; $i < count($deposits); $i++) {
                if(DB::table('tbl_main_join_loan_deposit')->where('portal_loan_deposit_id', $deposits[$i]->loan_id)->where('branch_id', $y)->first()) {
                    continue;
                }
            
                // Loan id null or not found
                $loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $deposits[$i]->loan_id)->where('branch_id', $y)->first();
                if($loan_id){
                    $loan_id = $loan_id->main_loan_id;
                }else{
                    continue;
                }
            
            	//approve loan
            	DB::connection('main')->table('loans_'.$branch_id)->where('id', $loan_id)->update([
                	'disbursement_status' => 'Approved'
                ]);	
            
                // client
                $clientid = DB::table('tbl_main_join_client')->where('main_client_id', $deposits[$i]->client_id)->first();
                if($clientid){
                    $clientid = $clientid->main_client_id;
                }else{
                    $clientid = '';
                }

                $depclient_info = DB::connection('portal')->table('tbl_client_basic_info')->where('client_uniquekey', $deposits[$i]->client_id)->first();
            
            	//get loan compulsory amount
            	$compulsory = DB::table($branch_code . '_loan_charges_compulsory_join')
                        ->leftJoin('loan_compulsory_type', $branch_code . '_loan_charges_compulsory_join.loan_compulsory_id', '=', 'loan_compulsory_type.compulsory_code')
                        ->where($branch_code . '_loan_charges_compulsory_join.loan_unique_id', $deposits[$i]->loan_id)
                        ->where($branch_code . '_loan_charges_compulsory_join.loan_compulsory_id', '!=', null)
                        ->get();
            
            	$loan_amount = DB::table($branch_code.'_loans')->where('loan_unique_id', $deposits[$i]->loan_id)->first()->loan_amount;
            	$compulsory_deposit = 0;
            
            	foreach ($compulsory as $c) {
                        if ($c->charge_option == 2) {
                            $compulsory_deposit += $loan_amount * ($c->saving_amount / 100);
                        } else {
                            $compulsory_deposit += $c->saving_amount;
                        }
                }
            
            	// return response()->json($compulsory_deposit); //output = 5000 (if loan_amount = 100000)
				$main_acc_code=DB::table('account_chart_external_details')->where('main_acc_code',$deposits[$i]->acc_join_id)->first()->main_acc_id;
            	// Create deposit
            	$client = new \GuzzleHttp\Client(['verify' => false]);
                $result = $client->post($routeurl, [
            		'headers' => [
                		'Content-Type' => 'application/x-www-form-urlencoded',
                		'Authorization' => 'Bearer '.$authorization,
            		],
                    'form_params' => [
                    	'applicant_number_id'=> $loan_id,
                    	'loan_deposit_date'=>  $deposits[$i]->deposit_pay_date,
                    	'referent_no'=> $deposits[$i]->ref_no,
                    	'customer_name'=> $depclient_info->name,
                    	'nrc'=> $depclient_info->nrc,
                    	'client_id'=> $clientid,
                   	 	'invoice_no'=> $deposits[$i]->invoice_no,
                    	'compulsory_saving_amount'=> $compulsory_deposit,
                    	'cash_acc_id'=> $main_acc_code, //153620,
                    	'total_deposit'=> $deposits[$i]->total_deposit_balance,
                    	'client_pay'=>$deposits[$i]->paid_deposit_balance,
                    	'note'=> $deposits[$i]->note,
                    	'branch_id'=> $branch_id,
                	]

        		]);

            	//return $result;
                $response = (string) $result->getBody();
				$response =json_decode($response); // Using this you can access any key like below
    			if($response->status_code == 200){
                	// return response()->json(['status_code' => 200, 'message' => 'failed', 'data', 'kk']);
                    DB::table('tbl_main_join_loan_deposit')->insert([
                        'main_loan_deposit_id' => DB::connection('main')->table('loans_'.$y)->orderBy('id', 'desc')->first()->id,
                    	'main_loan_deposit_client_id' => DB::connection('main')->table('loans_'.$y)->orderBy('id', 'desc')->first()->client_id,
                        'portal_loan_deposit_id' => $deposits[$i]->loan_id,
                        'branch_id' => $y,
                     ]);
     
                     $x = ++$x;
                }else{
                    //return response()->json(['status_code' => 400, 'message' => 're-crawling data not found', 'data' => null]);
                	 continue;
                }
            }

            //$verify = DB::connection('main')->table($table)->get();
            DB::connection('portal')->table('tbl_crawling')->where('module_name', 'rev_deposit')->update([
                'data_count' => $x,
                'crawl_date' => date('Y-m-d H:i:s')
            ]);

            if ($x) {
                return response()->json(['status_code' => 200, 'message' => 'success to crawling data', 'data' =>  $x]);
            }else{
                return response()->json(['status_code' => 400, 'message' => 're-crawling data not found', 'data' => null]);
            }
            
        }


    }

}
