<?php

namespace App\Http\Controllers\ReverseCrawl;

use App\Http\Controllers\Controller;
use App\Models\Crawl\Crawl;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use App\Models\Crawl\Crawl_Loan_Repayment_Schedule;



class LoanReverseCrawl extends Controller
{
    public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }

    public function getBranchCode($branchid)
    {
        $bcode = DB::table('tbl_branches')->where('id', $branchid)->first()->branch_code;
        return $bcode;
    }

    public function index()
    {
        $reverse_crawl_loans = DB::table('tbl_crawling')->where('module_name', 'rev_loan')->get();
        return view('crawling.reverse_crawl.loan_reverse_crawl.index', compact('reverse_crawl_loans'));
    }

    public function reverseCrawlLoans_main()
    {
        $branchcount = DB::table('tbl_branches')->count();

        $route = $this->getMainUrl37();

        $route_name = 'create-loan';
        $routeurl = $route . $route_name;
        $authorization = $this->loginToken();

        // $headers = [
        // 'Accept' => 'application/json',
        // 'Authorization' => 'Bearer ' . $authorization,
        // ];
        // $response = Http::withHeaders($headers)->get($routeurl);

        // $headers = [
        //     'Accept' => 'application/json',
        //     'Authorization' => 'Bearer '.$authorization,
        // ];


        $client = new \GuzzleHttp\Client(['verify' => false]);
        // $routeurl = "http://172.16.50.101/mis-core/public/api/create-loan";
        $routeurl = env('MAIN_URL');

        $res = $client->post($routeurl, [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Bearer ' . $authorization,
            ],
            'form_params' => [
                'username' => 'nay kaung Lar',
                'password' => 'passwordaaaa',
            ]
        ]);

        //return $response->getBody()->getContents();
        $response = (string) $res->getBody();
        $response = json_decode($response); // Using this you can access any key like below
        $key_value = $response->status_code; //access key  
        if ($response->status_code == 200) {
            $key_e = '11111';
        } else {
            $key_e = '00000';
        }
        return $key_e;
    }

    public function reverseCrawlLoans()
    {

        $branchcount = DB::table('tbl_branches')->count();
        $sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;

        $route = $this->getMainUrl37();

        $route_name = 'create-loan';
        $routeurl = $route . $route_name;
        $authorization = $this->loginToken();

        for ($y = $sessionBranch; $y <= $sessionBranch; $y++) {
            $branch_id = $y;
            $branch_code = strtolower($this->getBranchCode($branch_id));
            $loans = DB::connection('portal')->table($branch_code . '_loans')
                ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $branch_code . '_loans.client_id')
                // ->leftJoin($branch_code . '_loan_charges_compulsory_join', $branch_code . '_loan_charges_compulsory_join.loan_unique_id', '=', $branch_code . '_loans.loan_unique_id')
                // ->where($branch_code.'_loans.disbursement_status', 'Pending')
                // ->orwhere($branch_code.'_loans.disbursement_status', 'Approved')
                ->get();
            $table = 'loans_' . $branch_id;

            // return response()->json(['status_code' => 200, 'message' => 're-crawling loan success  ', 'data' => $loans]);
            $z = 0;
            for ($i = 0; $i < count($loans); $i++) {

                if (DB::table('tbl_main_join_loan')->where('portal_loan_id', $loans[$i]->loan_unique_id)->where('branch_id', $y)->first()) {
                    $getID = DB::table('tbl_main_join_loan')->where('portal_loan_id', $loans[$i]->loan_unique_id)->where('branch_id', $y)->first()->main_loan_id;
                    if ($loans[$i]->disbursement_status == 'Declined') {
                        DB::connection('main')->table('loans_' . $y)->where('id', $getID)->update(['disbursement_status' => 'Declined']);
                    }

                    if ($loans[$i]->disbursement_status == 'Canceled') {
                        DB::connection('main')->table('loans_' . $y)->where('id', $getID)->update(['disbursement_status' => 'Canceled']);
                    }
                    //Approval Condition + Loan Amount & Schedule Change
                    if ($loans[$i]->disbursement_status == 'Approved' && DB::connection('main')->table('loans_' . $y)->where('disbursement_number', $getID)->first()->disbursement_status == 'Pending') {
                        //check loan amount and first installment date change or not

                        $loan_amount_in_main = DB::connection('main')->table('loans_' . $y)->where('disbursement_number', $getID)->first()->loan_amount;
                        $first_ins_date = DB::connection('main')->table('loans_' . $y)->where('disbursement_number', $getID)->first()->first_installment_date;
                        if ($loan_amount_in_main == $loans[$i]->loan_amount || $first_ins_date == $loans[$i]->first_installment_date) {

                            DB::connection('main')->table('loans_' . $y)->where('disbursement_number', $getID)->update([
                                'disbursement_status' => 'Approved',
                                'status_note_date_approve' => $loans[$i]->approved_date,
                                'loan_amount' => $loans[$i]->loan_amount,
                                'first_installment_date' => $loans[$i]->first_installment_date,
                                'status_note_approve_by_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans[$i]->loan_officer_id)->first()->main_staff_id
                            ]);
                        } else {
                            DB::connection('main')->table('loans_' . $y)->where('disbursement_number', $getID)->update([
                                'disbursement_status' => 'Approved',
                                'status_note_date_approve' => $loans[$i]->approved_date,
                                'loan_amount' => $loans[$i]->loan_amount,
                                'first_installment_date' => $loans[$i]->first_installment_date,
                                'status_note_approve_by_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans[$i]->loan_officer_id)->first()->main_staff_id
                            ]);
                            $portal_schedule = DB::connection('portal')->table($branch_code . '_loans_schedule')->where('loan_unique_id', $loans[$i]->loan_unique_id)->get();
                            $main_schedule = DB::connection('main')->table('loan_disbursement_calculate_' . $y)->where('disbursement_id', $getID)->get();
                            for ($i = 0; $i < count($main_schedule); $i++) {
                                DB::connection('main')->table('loan_disbursement_calculate_' . $y)->where('disbursement_id', $getID)->where('no',$i+1)->update([
                                    'date_s' => date('Y-m-d h:m:s', strtotime($portal_schedule[$i]->month)),
                                    'principal_s' => $portal_schedule[$i]->capital,
                                    'interest_s' => $portal_schedule[$i]->interest,
                                    'total_s' => $portal_schedule[$i]->capital + $portal_schedule[$i]->interest,
                                    'balance_s' => $portal_schedule[$i]->final_amount,
                                ]);
                            }

                        }
                    }
                    continue;
                }

                $interest_rate = DB::connection('portal')->table('loan_type')->where('id', $loans[$i]->loan_type_id)->first()->interest_rate;

                // staff
                $staffid = DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans[$i]->loan_officer_id)->first();
                if ($staffid) {
                    $staffid = $staffid->main_staff_id;
                } else {
                    continue;
                }
                // center
                $centerid = DB::table('tbl_main_join_center')->where('portal_center_id', $loans[$i]->center_id)->first();
                if ($centerid) {
                    $centerid = $centerid->main_center_id;
                } else {
                    continue;
                }
                // group
                $groupid = DB::table('tbl_main_join_group')->where('portal_group_id', $loans[$i]->group_id)->first();
                if ($groupid) {
                    $groupid = $groupid->main_group_id;
                } else {
                    $groupid = '';
                    //continue;
                }
                // client
                $clientid = DB::table('tbl_main_join_client')->where('portal_client_id', $loans[$i]->client_id)->first();
                if ($clientid) {
                    $clientid = $clientid->main_client_id;
                } else {
                    continue;
                }


                // Guarantor_a
                $guarantor_a = DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id', $loans[$i]->guarantor_a)->first();
                if ($guarantor_a) {
                    $guarantor_a = $guarantor_a->main_guarantor_id;
                } else {
                    $guarantor_a = '';
                }
                // Guarantor_b
                $guarantor_b = DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id', $loans[$i]->guarantor_b)->first();
                if ($guarantor_b) {
                    $guarantor_b = $guarantor_b->main_guarantor_id;
                } else {
                    $guarantor_b = '';
                }

                // Guarantor_c
                $guarantor_c = DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id', $loans[$i]->guarantor_c)->first();
                if ($guarantor_c) {
                    $guarantor_c = $guarantor_c->main_guarantor_id;
                } else {
                    $guarantor_c = '';
                }


                // latest loan id generate
                $dataloan = DB::connection('main')->table('loans_' . $branch_id)->orderBy('id', 'desc')->first();
                $loanCount = DB::connection('main')->table('loans_' . $branch_id)->count();

                // $main_loan_generate_id = $dataloan->disbursement_number;
                // $bMainCode = DB::connection('main')->table('branches')->where('id', $y)
                $mainbcode = DB::connection('main')->table('branches')->where('id', $y)->first()->code;
                $main_loan_generate_id = 'LID-' . $mainbcode . '-2023-';
                $num = $loanCount + 1;
                $main_loan_generate_id .= $num;

                // return response()->json($main_loan_generate_id);

                // save new loan
                $client = new \GuzzleHttp\Client(['verify' => false]);
                $routeurl = env('MAIN_URL') . "create-loan";


                $chargeDetail = DB::table($branch_code . '_loan_charges_compulsory_join')
                    ->leftJoin('loan_charges_type', 'loan_charges_type.charge_code', '=', $branch_code . '_loan_charges_compulsory_join.loan_charge_id')
                    ->where($branch_code . '_loan_charges_compulsory_join.loan_unique_id', $loans[$i]->loan_unique_id)
                    ->where($branch_code . '_loan_charges_compulsory_join.loan_charge_id', '!=', NULL)
                    ->select('loan_charges_type.*', 'loan_charges_type.id as charge_id')
                    ->get();
                // return response()->json($loans[$i]->loan_term);

                $repayment_term = '';
                if ($loans[$i]->loan_term == 'Month') {
                    $repayment_term = 'Monthly';
                } else if ($loans[$i]->loan_term == 'Day') {
                    $repayment_term = 'Daily';
                } else if ($loans[$i]->loan_term == 'Week') {
                    $repayment_term = 'Weekly';
                } else if ($loans[$i]->loan_term == 'Two-Weeks') {
                    $repayment_term = 'Two-Weeks';
                } else if ($loans[$i]->loan_term == 'Four-Weeks') {
                    $repayment_term = 'Four-Weeks';
                } else {
                    $repayment_term = 'Yearly';
                }

                if (count($chargeDetail) > 2) {
                    $result = $client->post($routeurl, [
                        'headers' => [
                            'Content-Type' => 'application/x-www-form-urlencoded',
                            'Authorization' => 'Bearer ' . $authorization,
                        ],
                        'form_params' => [
                            'client_id' => $clientid,
                            'client_nrc_number' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans[$i]->client_uniquekey)->first()->nrc == NULL ? DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans[$i]->client_uniquekey)->first()->old_nrc : DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans[$i]->client_uniquekey)->first()->nrc,
                            'client_name' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans[$i]->client_uniquekey)->first()->name,
                            'client_phone' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans[$i]->client_uniquekey)->first()->phone_primary,
                            'you_are_a_group_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_group_leader,
                            'you_are_a_center_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_center_leader,
                            'business_proposal' => 'business_proposal',
                            'guarantor_id' => $guarantor_a,
                            'guarantor2_id' => $guarantor_b,
                            'guarantor3_id' => $guarantor_c,
                            'disbursement_number' => $main_loan_generate_id,
                            'branch_id' => $branch_id,
                            'loan_officer_id' => $staffid,
                            'loan_production_id' => $loans[$i]->loan_type_id,
                            'loan_application_date' => $loans[$i]->loan_application_date,
                            'first_installment_date' => $loans[$i]->first_installment_date,
                            'loan_amount' => $loans[$i]->loan_amount,
                            'interest_rate' => $interest_rate,
                            'interest_rate_period' => $loans[$i]->interest_rate_period,
                            'loan_term' => $loans[$i]->loan_term,
                            'loan_term_value' => $loans[$i]->loan_term_value,
                            'repayment_term' => $repayment_term,
                            'currency_id' => 1,
                            'transaction_type_id' => 2,
                            'group_loan_id' => $groupid,
                            'center_leader_id' => $centerid,
                            'charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
                            'charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
                            'charge_id[6981667280779875]' => $chargeDetail[2]->charge_id,
                            'name[3061667280779394]' => $chargeDetail[0]->charge_name,
                            'name[6471667280779921]' => $chargeDetail[1]->charge_name,
                            'name[6981667280779875]' => $chargeDetail[2]->charge_name,
                            'amount[3061667280779394]' => $chargeDetail[0]->amount,
                            'amount[6471667280779921]' => $chargeDetail[1]->amount,
                            'amount[6981667280779875]' => $chargeDetail[2]->amount,
                            'charge_option[3061667280779394]' => $chargeDetail[0]->charge_option,
                            'charge_option[6471667280779921]' => $chargeDetail[1]->charge_option,
                            'charge_option[6981667280779875]' => $chargeDetail[2]->charge_option,
                            'charge_type[3061667280779394]' => $chargeDetail[0]->charge_type,
                            'charge_type[6471667280779921]' => $chargeDetail[1]->charge_type,
                            'charge_type[6981667280779875]' => $chargeDetail[2]->charge_type,
                            'status[3061667280779394]' => 'Yes',
                            'status[6471667280779921]' => 'Yes',
                            'status[6981667280779875]' => 'Yes',
                            // 'charge_id'=> $charge_detail->id,
                            // 'name'=> $charge_detail->charge_name, //'Sinking Fun(0.5%)'
                            // 'amount'=> $charge_detail->amount,//'0.5',
                            // 'charge_option'=> $charge_detail->charge_option,//'2',
                            // 'charge_type'=>$charge_detail->charge_type, //'1'
                            // 'status'=>$charge_detail->status,//'Yes',
                            'compulsory_id' => 5,
                            'compound_interest' => 1,
                            'override_cycle' => 'no',
                            'product_name' => 'Saving 5%',
                            'saving_amount' => 5,
                            'c_charge_option' => 2,
                            'c_interest_rate' => 1.25,
                            'compulsory_product_type_id' => 1,
                            'c_status' => 'Yes',
                            'loan_charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
                            'loan_charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
                            'loan_charge_id[6981667280779875]' => $chargeDetail[2]->charge_id,
                            // 'loan_charge_id'=> 'Null',
                            'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans[$i]->loan_officer_id)->first()->main_staff_id,
                        ]
                    ]);

                    $response = (string) $result->getBody();
                    // return response()->json($result);
                    $response = json_decode($response); // Using this you can access any key like below
                    // return response()->json(['status_code' => 200, 'message' => 're-crawling loan success', 'data' => $response]);
                    if ($response->status_code == 200) {
                        // return response()->json($response);
                        $dataloan = DB::connection('main')->table('loans_' . $y)->orderBy('id', 'desc')->first();
                        DB::table('tbl_main_join_loan')->insert([
                            'main_loan_id' => $dataloan->id,
                            'main_loan_code' => $dataloan->disbursement_number,
                            'portal_loan_id' => $loans[$i]->loan_unique_id,
                            'branch_id' => $y
                        ]);
                        $getID = DB::table('tbl_main_join_loan')->where('portal_loan_id', $loans[$i]->loan_unique_id)->where('branch_id', $y)->first()->main_loan_id;

                        //add scheduels join
                        $LoansSchedule = DB::connection('portal')->table($branch_code . '_loans_schedule')->where('loan_unique_id', $loans[$i]->loan_unique_id)->get();
                        $mainSchedule = DB::connection('main')->table('loan_disbursement_calculate_' . $y)->where('disbursement_id', $getID)->get();

                        for ($t = 0; $t < count($LoansSchedule); $t++) {
                            if (DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$t]->id)->where('branch_id', $y)->first()) {
                                break;
                            } else {
                                if ($mainSchedule[$t]->no == ($t + 1)) {
                                    $mainJoinSchedule = new Crawl_Loan_Repayment_Schedule();
                                    $mainJoinSchedule->portal_disbursement_id = $LoansSchedule[$t]->loan_unique_id;
                                    $mainJoinSchedule->portal_disbursement_schedule_id = $LoansSchedule[$t]->id;
                                    $mainJoinSchedule->main_disbursement_schedule_id = $mainSchedule[$t]->id;
                                    $mainJoinSchedule->branch_id = $y;
                                    $mainJoinSchedule->save();
                                }
                            }
                        }

                        // activate
                        if ($loans[$i]->disbursement_status == 'Approved') {
                            DB::connection('main')->table('loans_' . $y)->where('id', $getID)->update([
                                'disbursement_status' => 'Approved',
                                'loan_amount' => $loans[$i]->loan_amount,
                                'first_installment_date' => $loans[$i]->first_installment_date,
                                'status_note_date_approve' => $loans[$i]->approved_date,
                                'status_note_approve_by_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans[$i]->loan_officer_id)->first()->main_staff_id
                            ]);
                        }
                        // $dataloan = DB::connection('main')->table('loans_' . $y)->orderBy('id', 'desc')->first();
                        // DB::table('tbl_main_join_loan')->insert([
                        // 'main_loan_id' => $dataloan->id,
                        // 'main_loan_code' => $dataloan->disbursement_number,
                        // 'portal_loan_id' => $loans[$i]->loan_unique_id,
                        // 'branch_id' => $y
                        // ]);
                        $z = ++$z;
                    } else {
                        // return response()->json(['status_code' => 400, 'message' => 're-crawling loan fail', 'data' => 'error']);
                        continue;
                    }
                } else {
                    $result = $client->post($routeurl, [
                        'headers' => [
                            'Content-Type' => 'application/x-www-form-urlencoded',
                            'Authorization' => 'Bearer ' . $authorization,
                        ],
                        'form_params' => [
                            'client_id' => $clientid,
                            'client_nrc_number' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans[$i]->client_uniquekey)->first()->nrc == NULL ? DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans[$i]->client_uniquekey)->first()->old_nrc : DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans[$i]->client_uniquekey)->first()->nrc,
                            'client_name' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans[$i]->client_uniquekey)->first()->name,
                            'client_phone' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans[$i]->client_uniquekey)->first()->phone_primary,
                            'you_are_a_group_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_group_leader,
                            'you_are_a_center_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_center_leader,
                            'business_proposal' => 'business_proposal',
                            'guarantor_id' => $guarantor_a,
                            'guarantor2_id' => $guarantor_b,
                            'guarantor3_id' => $guarantor_c,
                            'disbursement_number' => $main_loan_generate_id,
                            'branch_id' => $branch_id,
                            'loan_officer_id' => $staffid,
                            'loan_production_id' => $loans[$i]->loan_type_id,
                            'loan_application_date' => $loans[$i]->loan_application_date,
                            'first_installment_date' => $loans[$i]->first_installment_date,
                            'loan_amount' => $loans[$i]->loan_amount,
                            'interest_rate' => $interest_rate,
                            'interest_rate_period' => $loans[$i]->interest_rate_period,
                            'loan_term' => $loans[$i]->loan_term,
                            'loan_term_value' => $loans[$i]->loan_term_value,
                            'repayment_term' => $repayment_term,
                            'currency_id' => 1,
                            'transaction_type_id' => 2,
                            'group_loan_id' => $groupid,
                            'center_leader_id' => $centerid,
                            'charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
                            'charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
                            'name[3061667280779394]' => $chargeDetail[0]->charge_name,
                            'name[6471667280779921]' => $chargeDetail[1]->charge_name,
                            'amount[3061667280779394]' => $chargeDetail[0]->amount,
                            'amount[6471667280779921]' => $chargeDetail[1]->amount,
                            'charge_option[3061667280779394]' => $chargeDetail[0]->charge_option,
                            'charge_option[6471667280779921]' => $chargeDetail[1]->charge_option,
                            'charge_type[3061667280779394]' => $chargeDetail[0]->charge_type,
                            'charge_type[6471667280779921]' => $chargeDetail[1]->charge_type,
                            'status[3061667280779394]' => 'Yes',
                            'status[6471667280779921]' => 'Yes',
                            // 'charge_id'=> $charge_detail->id,
                            // 'name'=> $charge_detail->charge_name, //'Sinking Fun(0.5%)'
                            // 'amount'=> $charge_detail->amount,//'0.5',
                            // 'charge_option'=> $charge_detail->charge_option,//'2',
                            // 'charge_type'=>$charge_detail->charge_type, //'1'
                            // 'status'=>$charge_detail->status,//'Yes',
                            'compulsory_id' => 5,
                            'compound_interest' => 1,
                            'override_cycle' => 'no',
                            'product_name' => 'Saving 5%',
                            'saving_amount' => 5,
                            'c_charge_option' => 2,
                            'c_interest_rate' => 1.25,
                            'compulsory_product_type_id' => 1,
                            'c_status' => 'Yes',
                            'loan_charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
                            'loan_charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
                            // 'loan_charge_id'=> 'Null',
                            'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans[$i]->loan_officer_id)->first()->main_staff_id,
                        ]
                    ]);

                    $response = (string) $result->getBody();
                    // return response()->json($result);
                    $response = json_decode($response); // Using this you can access any key like below
                    // return response()->json(['status_code' => 200, 'message' => 're-crawling loan success', 'data' => $response]);
                    if ($response->status_code == 200) {
                        // return response()->json($response);\
                        $dataloan = DB::connection('main')->table('loans_' . $y)->orderBy('id', 'desc')->first();
                        DB::table('tbl_main_join_loan')->insert([
                            'main_loan_id' => $dataloan->id,
                            'main_loan_code' => $dataloan->disbursement_number,
                            'portal_loan_id' => $loans[$i]->loan_unique_id,
                            'branch_id' => $y
                        ]);
                        $getID = DB::table('tbl_main_join_loan')->where('portal_loan_id', $loans[$i]->loan_unique_id)->where('branch_id', $y)->first();
                        if ($getID) {
                            //add scheduels join
                            $LoansSchedule = DB::connection('portal')->table($branch_code . '_loans_schedule')->where('loan_unique_id', $loans[$i]->loan_unique_id)->get();
                            $mainSchedule = DB::connection('main')->table('loan_disbursement_calculate_' . $y)->where('disbursement_id', $getID->main_loan_id)->get();

                            for ($t = 0; $t < count($LoansSchedule); $t++) {
                                if (DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$t]->id)->where('branch_id', $y)->first()) {
                                    break;
                                } else {
                                    if ($mainSchedule[$t]->no == ($t + 1)) {
                                        $mainJoinSchedule = new Crawl_Loan_Repayment_Schedule();
                                        $mainJoinSchedule->portal_disbursement_id = $LoansSchedule[$t]->loan_unique_id;
                                        $mainJoinSchedule->portal_disbursement_schedule_id = $LoansSchedule[$t]->id;
                                        $mainJoinSchedule->main_disbursement_schedule_id = $mainSchedule[$t]->id;
                                        $mainJoinSchedule->branch_id = $y;
                                        $mainJoinSchedule->save();
                                    }
                                }
                            }


                            // activate
                            if ($loans[$i]->disbursement_status == 'Approved') {
                                DB::connection('main')->table('loans_' . $y)->where('id', $getID->main_loan_id)->update([
                                    'disbursement_status' => 'Approved',
                                    'status_note_date_approve' => $loans[$i]->approved_date,
                                    'status_note_approve_by_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans[$i]->loan_officer_id)->first()->main_staff_id
                                ]);
                            }

                            // $dataloan = DB::connection('main')->table('loans_' . $y)->orderBy('id', 'desc')->first();
                            // DB::table('tbl_main_join_loan')->insert([
                            //     'main_loan_id' => $dataloan->id,
                            //     'main_loan_code' => $dataloan->disbursement_number,
                            //     'portal_loan_id' => $loans[$i]->loan_unique_id,
                            //     'branch_id' => $y
                            // ]);
                            $z = ++$z;
                        } else {
                            return response()->json(['status_code' => 400, 'message' => 're-crawling loan fail', 'data' => $loans[$i]->loan_unique_id]);
                        }
                    } else {
                        continue;
                    }
                }
            }

            //$verify = DB::connection('main')->table($table)->get();
            DB::connection('portal')->table('tbl_crawling')->where('module_name', 'rev_loan')->update([
                'data_count' => $z,
                'crawl_date' => date('Y-m-d H:i:s'),
            ]);
            if ($z) {
                return response()->json(['status_code' => 200, 'message' => 're-crawling loan success', 'data' =>  $z]);
            } else {
                return response()->json(['status_code' => 400, 'message' => 're-crawling loan fail', 'data' => null]);
            }
        }
    }
}
