<?php

namespace App\Http\Controllers\ReverseCrawl;

use App\Http\Controllers\Controller;
use App\Models\Crawl\Crawl;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;


class LoanReverseCrawl extends Controller
{
    public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }

    public function getBranchCode($branchid)
    {
        $bcode = DB::table('tbl_branches')->where('id', $branchid)->first()->branch_code;
        return $bcode;
    }

    public function index()
    {
        $reverse_crawl_loans = DB::table('tbl_crawling')->where('module_name','rev_loan')->get();
        return view('crawling.reverse_crawl.loan_reverse_crawl.index', compact('reverse_crawl_loans'));
    }

    public function reverseCrawlLoans_main()
    {
        $branchcount = DB::table('tbl_branches')->count();
    
        $route = $this->getMainUrl37();

        $route_name = 'create-loan';
        $routeurl = $route.$route_name;
        $authorization = $this->loginToken();
    
     	// $headers = [
     	// 'Accept' => 'application/json',
     	// 'Authorization' => 'Bearer ' . $authorization,
     	// ];
     	// $response = Http::withHeaders($headers)->get($routeurl);
    
        // $headers = [
        //     'Accept' => 'application/json',
        //     'Authorization' => 'Bearer '.$authorization,
        // ];

    
        $client = new \GuzzleHttp\Client(['verify' => false]);
        // $routeurl = "http://172.16.50.101/mis-core/public/api/create-loan";
    	$routeurl = env('MAIN_URL');

        $res = $client->post($routeurl, [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Bearer '.$authorization,
            ],
            'form_params' => [
                'username' => 'nay kaung Lar',
                'password' => 'passwordaaaa',
            ]
        ]);

    	//return $response->getBody()->getContents();
        $response = (string) $res->getBody();
		$response =json_decode($response); // Using this you can access any key like below
		$key_value = $response->status_code; //access key  
    	if($response->status_code == 200){
          $key_e = '11111';
        }else{
         $key_e = '00000';
        }
        return $key_e;

    }

    public function reverseCrawlLoans()
    {
        $branchcount = DB::table('tbl_branches')->count();

        $route = $this->getMainUrl37();

        $route_name = 'create-loan';
        $routeurl = $route.$route_name;
        $authorization = $this->loginToken();

        for($y = 1; $y <= $branchcount; $y++) {
            $branch_id = $y;
            $branch_code = strtolower($this->getBranchCode($branch_id));
            $loans = DB::connection('portal')->table($branch_code.'_loans')
            			->join('tbl_client_basic_info','tbl_client_basic_info.client_uniquekey','=',$branch_code.'_loans.client_id')
            			->join($branch_code.'_loan_charges_compulsory_join', $branch_code.'_loan_charges_compulsory_join.loan_unique_id', '=', $branch_code.'_loans.loan_unique_id')
            			->get();
            $table = 'loans_'.$branch_id;

            // return response()->json(['status_code' => 200, 'message' => 're-crawling loan success', 'data' =>  count($loans)]);
            $z = 0;
            for($i = 0; $i < count($loans); $i++) {
                if(DB::table('tbl_main_join_loan')->where('portal_loan_id', $loans[$i]->loan_unique_id)->first()) {
                	$getID = DB::table('tbl_main_join_loan')->where('portal_loan_id', $loans[$i]->loan_unique_id)->first()->main_loan_code;
                	if($loans[$i]->disbursement_status == 'Declined') {
                    	DB::connection('main')->table('loans_'.$y)->where('disbursement_number', $getID)->update(['disbursement_status' => 'Declined']);
                    }
                    continue;
                }

                $interest_rate = DB::connection('portal')->table('loan_type')->where('id', $loans[$i]->loan_type_id)->first()->interest_rate;

                // group
                $groupid = DB::table('tbl_main_join_group')->where('portal_group_id', $loans[$i]->group_id)->first();
                if($groupid){
                    $groupid = $groupid->main_group_id;
                }else{
                    $groupid = '';
                }
                // client
                $clientid = DB::table('tbl_main_join_client')->where('portal_client_id', $loans[$i]->client_id)->first();
                if($clientid){
                    $clientid = $clientid->main_client_id;
                }else{
                    $clientid = '';
                }
                // center
                $centerid = DB::table('tbl_main_join_center')->where('portal_center_id', $loans[$i]->center_id)->first();
                if($centerid){
                    $centerid = $centerid->main_center_id;
                }else{
                    $centerid = '';
                }
                // staff
                $staffid = DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans[$i]->loan_officer_id)->first();
                if($staffid){
                    $staffid = $staffid->main_staff_id;
                }else{
                    $staffid = '';
                }
                // Guarantor_a
                $guarantor_a = DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id', $loans[$i]->guarantor_a)->first();
                if($guarantor_a){
                    $guarantor_a = $guarantor_a->main_guarantor_id;
                }else{
                    $guarantor_a = '';
                }
                // Guarantor_b
                $guarantor_b = DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id', $loans[$i]->guarantor_b)->first();
                if($guarantor_b){
                    $guarantor_b = $guarantor_b->main_guarantor_id;
                }else{
                    $guarantor_b = '';
                }

                // Guarantor_c
                $guarantor_c = DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id', $loans[$i]->guarantor_c)->first();
                if($guarantor_c){
                    $guarantor_c = $guarantor_c->main_guarantor_id;
                }else{
                    $guarantor_c = '';
                }
            	
            	$charge_detail = DB::table('loan_charges_type')->where('charge_code', $loans[$i]->loan_charge_id)->first();
            	$compulsory_detail = DB::table('loan_compulsory_type')->first();
            
                // latest loan id generate
                $dataloan = DB::connection('main')->table('loans_'.$branch_id)->orderBy('disbursement_number', 'desc')->first();
            	$loanCount = DB::connection('main')->table('loans_'.$branch_id)->count();

                // $main_loan_generate_id = $dataloan->disbursement_number;
            	$main_loan_generate_id = 'LID-22-2022-';
                $num = $loanCount + 1;
            	$main_loan_generate_id .= $num;
            
            	// return response()->json($main_loan_generate_id);
            
             	// save new loan
                $client = new \GuzzleHttp\Client(['verify' => false]);
        		$routeurl = env('MAIN_URL')."create-loan";

        		$result = $client->post($routeurl, [
            		'headers' => [
                		'Content-Type' => 'application/x-www-form-urlencoded',
                		'Authorization' => 'Bearer '.$authorization,
           			 ],
            		'form_params' => [
                   		'client_id'=> $clientid,
                    	'client_nrc_number' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans[$i]->client_uniquekey)->first()->nrc == NULL ? DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans[$i]->client_uniquekey)->first()->old_nrc : DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans[$i]->client_uniquekey)->first()->nrc,
                        'client_name' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans[$i]->client_uniquekey)->first()->name,
                    	'client_phone' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans[$i]->client_uniquekey)->first()->phone_primary,
                   		'you_are_a_group_leader'=> DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_center_leader,
                   		'you_are_a_center_leader'=>DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_group_leader,
                   		'business_proposal'=>NULL,
                   		'guarantor_id'=> $guarantor_a,
                   		'guarantor2_id'=> $guarantor_b,
                   		'guarantor3_id'=> $guarantor_c,
                   		'disbursement_number'=> $main_loan_generate_id,
                   		'branch_id'=> $branch_id,
                   		'loan_officer_id'=> $staffid,
                   		'loan_production_id'=> $loans[$i]->loan_type_id,
                   		'loan_application_date'=> $loans[$i]->loan_application_date,
                   		'first_installment_date'=> $loans[$i]->first_installment_date,
                   		'loan_amount'=> $loans[$i]->loan_amount,
                   		'interest_rate'=> $interest_rate,
                   		'interest_rate_period'=> $loans[$i]->interest_rate_period,
                   		'loan_term'=> $loans[$i]->loan_term,
                   		'loan_term_value'=> 25, //$loans[$i]->loan_term_value **not working
                   		'repayment_term'=> $loans[$i]->loan_term,
                   		'currency_id'=>1,
                   		'transaction_type_id'=>2,
                   		'group_loan_id'=> $groupid,
                    	'center_leader_id'=> $centerid,
						// 'charge_id[3061667280779394]':Null
						// 'name[3061667280779394]':Sinking Fun(0.5%)
						// 'name[6471667280779921]':Application Service Fees(1%)
						// 'name[6981667280779875]':Document Fees(0.5%)
						// 'amount[3061667280779394]':0.5
						// 'amount[6471667280779921]':1
						// 'amount[6981667280779875]':0.5
						// 'charge_option[3061667280779394]':2
						// 'charge_option[6471667280779921]':2
						// charge_option[6981667280779875]:2
						// charge_type[3061667280779394]:1
						// charge_type[6471667280779921]:1
						// charge_type[6981667280779875]:1
						// status[3061667280779394]:Yes
						// status[6471667280779921]:Yes
						// status[6981667280779875]:Yes
                   		'charge_id'=> $charge_detail->id,
                   		'name'=> $charge_detail->charge_name, //'Sinking Fun(0.5%)'
                   		'amount'=> $charge_detail->amount,//'0.5',
                   		'charge_option'=> $charge_detail->charge_option,//'2',
                   		'charge_type'=>$charge_detail->charge_type, //'1'
                   		'status'=>$charge_detail->status,//'Yes',
                   		'compulsory_id'=> 5,
                   		'compound_interest'=>1,
                   		'override_cycle'=>'no',
                   		'product_name'=>'Saving 5%',
                   		'saving_amount'=>5,
                   		'c_charge_option'=>2,
                   		'c_interest_rate'=>1.25,
                   		'compulsory_product_type_id'=>1,
                   		'c_status'=> 'Yes',
                   		'loan_charge_id'=> 'Null',
                   		'created_by'=>$loans[$i]->loan_officer_id,
            		]
        		]);
            
            	$response = (string) $result->getBody();
                // return response()->json($result);
        		$response =json_decode($response); // Using this you can access any key like below
            	// return response()->json(['status_code' => 200, 'message' => 're-crawling loan success', 'data' => $response]);
        		if($response->status_code == 200){
                	// return response()->json($response);
          			$dataloan = DB::connection('main')->table('loans_'.$y)->orderBy('id', 'desc')->first();
                    DB::table('tbl_main_join_loan')->insert([
                        'main_loan_id' => $dataloan->id,
                        'main_loan_code' => $dataloan->disbursement_number,
                        'portal_loan_id' => $loans[$i]->loan_unique_id
                     ]);
                 	$z = $z+1;
        		}else{
         			return response()->json(['status_code' => 400, 'message' => 're-crawling loan fail', 'data' => 'error']);
                	continue;
        		}
               
            }
        
           //$verify = DB::connection('main')->table($table)->get();
           DB::connection('portal')->table('tbl_crawling')->where('module_name', 'rev_loan')->update([
                		'data_count' => $z,
                		'crawl_date' => date('Y-m-d H:i:s'),
           ]);
           if ($z) {
              return response()->json(['status_code' => 200, 'message' => 're-crawling loan success', 'data' =>  $z]);
           }else{
              return response()->json(['status_code' => 400, 'message' => 're-crawling loan fail', 'data' => null]);
           }


        }


    }

}
