<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\City;
use App\Models\ClientDataEnteries\District;
use App\Models\ClientDataEnteries\Division;
use App\Models\ClientDataEnteries\Education;
use App\Models\ClientDataEnteries\Guarantor;
use App\Models\ClientDataEnteries\Province;
use App\Models\ClientDataEnteries\Quarter;
use App\Models\ClientDataEnteries\Staff;
use App\Models\ClientDataEnteries\Township;
use App\Models\ClientDataEnteries\Village;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class GuarantorController extends Controller
{
    public function getBranchCode()
    {
        $staff_code = Session::get('staff_code');
        // dd($staff_code);
        $B_code = DB::table('tbl_staff')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->select('tbl_branches.branch_code')
            ->where('tbl_staff.staff_code', $staff_code)
            ->first();
        $b_code = strtolower($B_code->branch_code);
        return $b_code;
    }
    //Index Page
    public function index()
    {
        $this->permissionFilter("guarantor-list");

        $guarantors = Guarantor::where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
            ->Paginate(10);
        return view('client.dataentery.guarantors.index', compact('guarantors'));
    }

    //Create Page
    public function create()
    {
        $this->permissionFilter("add-guarantor");
        $divisions = DB::table('addresses')->where('type', '=', 'state')->get();
        $districts = District::select('district_name', 'id')->get();
        $cities = City::select('city_name', 'id')->get();
        $townships = Township::select('township_name', 'id')->get();
        $villages = Village::select('village_name', 'id')->get();
        $quarters = Quarter::select('quarter_name', 'id')->get();
        $educations = Education::select('id', 'education_name')->get();

        return view('client.dataentery.guarantors.create', compact('divisions', 'districts', 'cities', 'townships', 'villages', 'quarters', 'educations'));
    }


    public function store(Request $request)
    {
        $this->permissionFilter("add-guarantor");
        $validator = $request->validate([
            'guarantor_uniquekey' => ['string', 'max:255', 'unique:tbl_guarantors'],
            'name' => ['required', 'string', 'max:255'],
            'dob' => ['required', 'string', 'max:255'],
            'gender' => ['required', 'string', 'max:255'],
            'guarantor_photo' => ['required', 'mimes:jpeg,png,jpg|max:2048'],
            'kyc_photo' => ['required', 'mimes:jpeg,png,jpg|max:2048'],
            'income' => ['nullable', 'string', 'max:255'],
            'phone_primary' => ['required', 'string', 'max:255'],
            'phone_secondary' => ['nullable', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255'],
            'blood_type' => ['required', 'string', 'max:255'],
            'religion' => ['required', 'string', 'max:255'],
            'nationality' => ['required', 'string', 'max:255'],
            'education_id' => ['required', 'string', 'max:255'],
            'other_education' => ['nullable', 'string', 'max:255'],
            'village_id' => ['required', 'string', 'max:255'],
            'ward_id' => ['nullable', 'string', 'max:255'],
            'township_id' => ['required', 'string', 'max:255'],
            'district_id' => ['required', 'string', 'max:255'],
            'division_id' => ['required', 'string', 'max:255'],
            // 'city_id' => ['required', 'string', 'max:255'],
            'address_primary' => ['required', 'string', 'max:255'],
            'address_secondary' => ['nullable', 'string', 'max:255'],
            'status' => ['required', 'string', 'max:255']
        ]);

        if ($validator) {

            $guarantor = new Guarantor;

            if ($request->has_nrc == "Yes") {

                $nrc_card_id = Guarantor::where('nrc_card_id', '=', $request->input('nrc_card_id'))->first();
                if ($nrc_card_id === null) {
                    $nrc_card = $request->nrc_card_id;
                    if (!empty($request->old_nrc)) {
                        $guarantor->old_nrc = $request->old_nrc;
                    } else if (!empty($request->nrc_1 && $request->nrc_2 && $request->nrc_3 && $request->nrc_4)) {
                        $guarantor->nrc = $request->nrc_1 . '/' . $request->nrc_2 . $request->nrc_3 . $request->nrc_4;
                    } else {
                        $guarantor->nrc = null;
                        $guarantor->old_nrc = null;
                    }
                    $guarantor->nrc_card_id = $nrc_card;
                } else {

                    return redirect()->back()->withErrors("NRC is Already Exist")->withInput();
                }
            }

            $guarantorkey = Guarantor::where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)->get()->last();
            // dd($guarantorkey->guarantor_uniquekey);
            if ($guarantorkey == null) {
                $bcode = strtoupper($this->getBranchCode());
                $guarantorkey = $bcode;
                $guarantorkey .= '-';
                $guarantorkey .= '02';
                $guarantorkey .= '-';
                $guarantorkey .= '0000001';
            } else {
                $guarantorkey = $guarantorkey->guarantor_uniquekey;

                $bstr = substr($guarantorkey, 3);
                // dd($bstr);
                $id = substr($bstr, -7, 7);
                // dd($id);
                $bstr_cusm = $id + 1;
                $bstr_cusm = (string)str_pad($bstr_cusm, 7, '0', STR_PAD_LEFT);

                // get loan_uniquekey in tbl_loans
                $guarantorkey = strtoupper($this->getBranchCode()); // Branch Code
                $guarantorkey .= '-';
                $guarantorkey .= '02';
                $guarantorkey .= '-';
                $guarantorkey .= $bstr_cusm;
            }

            $guarantor->guarantor_uniquekey = $guarantorkey;
            $guarantor->name = $request->name;
            $guarantor->name_mm = $request->name_mm;
            $guarantor->dob = $request->dob;
            $guarantor->gender = $request->gender;

            $guarantor->current_job = $request->current_job;
            $guarantor->income = $request->income;
            $guarantor->phone_primary = $request->phone_primary;
            $guarantor->phone_secondary = $request->phone_secondary;
            $guarantor->email = $request->email;
            $guarantor->blood_type = $request->blood_type;
            $guarantor->religion = $request->religion;
            $guarantor->nationality = $request->nationality;
            $guarantor->education_id = $request->education_id;
            $guarantor->other_education = $request->other_education;
            $guarantor->village_id = $request->village_id;
            $guarantor->township_id = $request->township_id;
            $guarantor->district_id = $request->district_id;
            $guarantor->division_id = $request->division_id;
            $guarantor->quarter_id = $request->ward_id;
            $guarantor->city_id = $request->city_id;
            $guarantor->address_primary = $request->address_primary;
            $guarantor->address_secondary = $request->address_secondary;
            $guarantor->status = $request->status;

            $guarantor->branch_id = Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id;


            $destinationPath = 'public/guarantor_photos/';
            if ($request->guarantor_nrc_front_photo) {
                $validator = $request->validate([
                    'guarantor_nrc_front_photo' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                    'guarantor_nrc_back_photo' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                if ($validator) {
                    $guarantor_nrc_front_photo_name = time() . '.' . $request->guarantor_nrc_front_photo->getClientOriginalName();
                    $guarantor_nrc_back_photo_name = time() . '.' . $request->guarantor_nrc_back_photo->getClientOriginalName();
                    $request->guarantor_nrc_front_photo->storeAs($destinationPath, $guarantor_nrc_front_photo_name);
                    $request->guarantor_nrc_back_photo->storeAs($destinationPath, $guarantor_nrc_back_photo_name);
                    $guarantor->guarantor_nrc_front_photo = $guarantor_nrc_front_photo_name;
                    $guarantor->guarantor_nrc_back_photo = $guarantor_nrc_back_photo_name;
                } else {
                    return redirect()->back()->withErrors('Please insert photos in jpg,png,jpeg,gif,svg format only');
                }
            }
            // else {
            // $validator = $request->validate([
            // 'nrc_recommendation' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            // ]);
            // if ($validator) {
            // $nrc_recommendation_name = time() . '.' . $request->nrc_recommendation->getClientOriginalName();
            // $request->nrc_recommendation->storeAs($destinationPath, $nrc_recommendation_name);
            // $guarantor->nrc_recommendation = $nrc_recommendation_name;
            // } else {
            // return redirect()->back()->withErrors('Please insert photos in jpg,png,jpeg,gif,svg format only');
            // }
            // }
            $guarantor_photo_name = time() . '.' . $request->guarantor_photo->getClientOriginalName();
            $kyc_photo_name = time() . '.' . $request->kyc_photo->getClientOriginalName();
            $request->guarantor_photo->storeAs($destinationPath, $guarantor_photo_name);
            $request->kyc_photo->storeAs($destinationPath, $kyc_photo_name);
            $guarantor->guarantor_photo = $guarantor_photo_name;
            $guarantor->kyc_photo = $kyc_photo_name;

            $guarantor->save();
            if (!$guarantor) {

                return redirect()->back()->withErrors('Something Wrong Guarantor Info');
            }

            $this->liveReverseGuarantors($guarantorkey);


            return redirect('guarantors/')->with("successMsg", 'New Guarantor is Added in your data');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    //Edit Page
    public function edit(Guarantor $guarantor, $id)
    {
        $this->permissionFilter("update-guarantor");
        $guarantor = Guarantor::where('guarantor_uniquekey', $id)->first();
        function explodeNRC($delimiters, $string)
        {
            return explode(chr(1), str_replace($delimiters, chr(1), $string));
        }

        $list = $guarantor->nrc;

        $exploded_nrc = explodeNRC(array('/', '(', ')'), $list);
        // $divisions = Division::select('division_name', 'id')->get();
        // $districts = District::select('district_name', 'id')->get();
        // $cities = City::select('city_name', 'id')->get();
        // $quarters = Quarter::select('quarter_name', 'id')->get();
        // $townships = Township::select('township_name', 'id')->get();
        // $villages = Village::select('village_name', 'id')->get();
        $divisions = DB::table('addresses')->where('type', '=','state')->get();
        $districts = DB::table('addresses')->where('parent_code',$guarantor->division_id)->get();
        $townships = DB::table('addresses')->where('parent_code',$guarantor->district_id)->get();
        $villages = DB::table('addresses')->where('parent_code',$guarantor->township_id)->get();
        $wards = DB::table('addresses')->where('parent_code',$guarantor->village_id)->get();

        $educations = Education::select('id', 'education_name')->get();
        return view('client.dataentery.guarantors.edit', compact('guarantor', 'educations', 'divisions', 'wards', 'districts', 'townships', 'villages', 'exploded_nrc'));
    }



    public function update(Request $request, Guarantor $guarantor, $id)
    {
        $this->permissionFilter("update-guarantor");
        $validator = $request->validate([
            'guarantor_uniquekey' => ['string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'dob' => ['required', 'string', 'max:255'],
            'gender' => ['required', 'string', 'max:255'],
            'phone_primary' => ['nullable', 'string', 'max:255'],
            'phone_secondary' => ['nullable', 'string', 'max:255'],
            'email' => ['nullable', 'string', 'max:255'],
            'blood_type' => ['nullable', 'string', 'max:255'],
            'religion' => ['nullable', 'string', 'max:255'],
            'nationality' => ['nullable', 'string', 'max:255'],
            'education_id' => ['nullable', 'string', 'max:255'],
            'other_education' => ['nullable', 'string', 'max:255'],
            'village_id' => ['required', 'string', 'max:255'],
            'quarter_id' => ['nullable', 'string', 'max:255'],
            'township_id' => ['required', 'string', 'max:255'],
            'district_id' => ['required', 'string', 'max:255'],
            'division_id' => ['required', 'string', 'max:255'],
            'city_id' => ['required', 'string', 'max:255'],
            'address_primary' => ['required', 'string', 'max:255'],
            'address_secondary' => ['nullable', 'string', 'max:255'],
            'status' => ['required', 'string', 'max:255']
        ]);

        if ($validator) {
            //$guarantor->update($request->all());
            if ($request->nrc_1) {
                $nrc = $request->nrc_1 . '/' . $request->nrc_2 . $request->nrc_3 . $request->nrc_4;
            } else {
                $nrc = NULL;
            }
            $guarantor = Guarantor::where('guarantor_uniquekey', $id)->first();
            $guarantor->name = $request->name;
            $guarantor->name_mm = $request->name_mm;
            $guarantor->dob = $request->dob;
            $guarantor->gender = $request->gender;
            $guarantor->nrc = $nrc;
            $guarantor->old_nrc = $request->old_nrc;
            $guarantor->nrc_card_id = $request->nrc_card_id;
            $guarantor->phone_primary = $request->phone_primary;
            $guarantor->phone_secondary = $request->phone_secondary;
            $guarantor->email = $request->email;
            $guarantor->blood_type = $request->blood_type;
            $guarantor->religion = $request->religion;
            $guarantor->nationality = $request->nationality;
            $guarantor->education_id = $request->education_id;
            $guarantor->other_education = $request->other_education;
            $guarantor->village_id = $request->village_id;
            $guarantor->township_id = $request->township_id;
            $guarantor->district_id = $request->district_id;
            $guarantor->division_id = $request->division_id;
            $guarantor->city_id = $request->city_id;
            $guarantor->address_primary = $request->address_primary;
            $guarantor->address_secondary = $request->address_secondary;
            $guarantor->status = $request->status;
            $guarantor->current_job = $request->current_job;
            $guarantor->income = $request->income;

            $destinationPath = 'public/guarantor_photos/';

            if ($request->hasFile('guarantor_nrc_front_photo')) {
                $request->validate([
                    'guarantor_nrc_front_photo' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $guarantor_nrc_front_photo_name = time() . '.' . $request->guarantor_nrc_front_photo->getClientOriginalName();
                $request->guarantor_nrc_front_photo->storeAs($destinationPath, $guarantor_nrc_front_photo_name);
                $guarantor->guarantor_nrc_front_photo = $guarantor_nrc_front_photo_name;
            }

            if ($request->hasFile('guarantor_nrc_back_photo')) {
                $request->validate([
                    'guarantor_nrc_back_photo' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $guarantor_nrc_back_photo_name = time() . '.' . $request->guarantor_nrc_back_photo->getClientOriginalName();
                $request->guarantor_nrc_back_photo->storeAs($destinationPath, $guarantor_nrc_back_photo_name);
                $guarantor->guarantor_nrc_back_photo = $guarantor_nrc_back_photo_name;
            }

            if ($request->hasFile('nrc_recommendation')) {
                $request->validate([
                    'nrc_recommendation' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $nrc_recommendation_name = time() . '.' . $request->nrc_recommendation->getClientOriginalName();
                $request->nrc_recommendation->storeAs($destinationPath, $nrc_recommendation_name);
                $guarantor->nrc_recommendation = $nrc_recommendation_name;
            }

            if ($request->hasFile('guarantor_photo')) {
                $request->validate([
                    'guarantor_photo' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $guarantor_photo_name = time() . '.' . $request->guarantor_photo->getClientOriginalName();
                $request->guarantor_photo->storeAs($destinationPath, $guarantor_photo_name);
                $guarantor->guarantor_photo = $guarantor_photo_name;
            }

            if ($request->hasFile('kyc_photo')) {
                $request->validate([
                    'kyc_photo' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $kyc_photo_name = time() . '.' . $request->kyc_photo->getClientOriginalName();
                $request->kyc_photo->storeAs($destinationPath, $kyc_photo_name);
                $guarantor->kyc_photo = $kyc_photo_name;
            }

            $guarantor->save();

            return redirect('guarantors/')->with("successMsg", 'Existing Guarantor is Updated in your data');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    // View Page
    public function view($id)
    {

        $guarantor  = DB::table('tbl_guarantors')
            // ->leftjoin('tbl_divisions', 'tbl_guarantors.division_id', '=', 'tbl_divisions.id')
            // ->leftjoin('tbl_districts', 'tbl_guarantors.district_id', '=', 'tbl_districts.id')
            // ->leftjoin('tbl_cities', 'tbl_guarantors.city_id', '=', 'tbl_cities.id')
            // ->leftjoin('tbl_townships', 'tbl_guarantors.township_id', '=', 'tbl_townships.id')
            // ->leftjoin('tbl_provinces','tbl_guarantors.province_id','=','tbl_provinces.id')
            // ->leftjoin('tbl_quarters', 'tbl_guarantors.quarter_id', '=', 'tbl_quarters.id')
            // ->leftjoin('tbl_villages', 'tbl_guarantors.village_id', '=', 'tbl_villages.id')
            ->leftjoin('tbl_educations', 'tbl_guarantors.education_id', '=', 'tbl_educations.id')
            ->where('tbl_guarantors.guarantor_uniquekey', '=', $id)
            ->first();

        $division = DB::table('addresses')->where('code', $guarantor->division_id)->first();
        $district = DB::table('addresses')->where('code', $guarantor->district_id)->first();
        $township = DB::table('addresses')->where('code', $guarantor->township_id)->first();
        $village = DB::table('addresses')->where('code', $guarantor->village_id)->first();
        $quarter = DB::table('addresses')->where('code', $guarantor->quarter_id)->first();
        // dd($guarantor);
        return view('client.dataentery.guarantors.view', compact('guarantor', 'division', 'district', 'township', 'village', 'quarter'));
    }

    //Delete
    public function destroy(Guarantor $guarantor, $id)
    {
        $this->permissionFilter("delete-guarantor");
        $guarantor = Guarantor::where('guarantor_uniquekey', $id)->first();
        $guarantor->delete();

        $main_guarantor_id=DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id',$id)->first()->main_guarantor_id;
        DB::connection('main')->table('guarantors')->where('id',$main_guarantor_id)->delete();
        DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id',$id)->delete();

        return redirect('guarantors/')->with("successMsg", 'Existing Guarantor is Deleted in your data');
    }

    public function getUniqueID(Request $request)
    {
        // $branchid = $request->branchid;
        $branch = DB::table('tbl_staff')->where('staff_code', Session::get('staff_code'))->select('branch_id')->first();
        $branch_code = DB::table('tbl_branches')->where('id', $branch->branch_id)->first()->branch_code;
        $idnum = DB::table('tbl_guarantors')->where('branch_id', $branch->branch_id)->get()->count();
        $idnum = $idnum + 1;
        $idnum = str_pad($idnum, 7, 0, STR_PAD_LEFT);

        $unique_key = $branch_code;
        $unique_key .= '-02-';
        $unique_key .= $idnum;


        return response()->json($unique_key);
    }

    public function liveReverseGuarantors($guarantorkey)
    {

        $guarantors = DB::connection('portal')
            ->table('tbl_guarantors')
            ->leftJoin('tbl_center', 'tbl_center.branch_id', '=', 'tbl_guarantors.branch_id')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_guarantors.branch_id')
            ->where('guarantor_uniquekey', $guarantorkey)
            ->select('tbl_center.*', 'tbl_guarantors.*', 'tbl_branches.id as branch_id')
            ->first();


        if ($guarantors->staff_id) {
            if (
                DB::table('tbl_main_join_guarantor')
                ->where('portal_guarantor_id', $guarantors->guarantor_uniquekey)
                ->first()
            ) {
            } else {
                DB::connection('main')
                    ->table('guarantors')
                    ->insert([
                        'nrc_number' => $guarantors->nrc == null ? $guarantors->old_nrc : $guarantors->nrc,
                        'full_name_en' => $guarantors->name,
                        'full_name_mm' => $guarantors->name_mm,
                        'mobile' => $guarantors->phone_primary,
                        'phone' => $guarantors->phone_secondary,
                        'email' => $guarantors->email,
                        'dob' => $guarantors->dob,
                        'photo' => $guarantors->guarantor_photo,
                        'address' => $guarantors->address_primary,
                        'province_id'=>$guarantors->division_id,
                        'village_id'=>$guarantors->village_id,
                        'commune_id'=>$guarantors->township_id,
                        'district_id'=>$guarantors->district_id,
                        'branch_id' => $guarantors->branch_id,
                        'center_leader_id' => $guarantors->staff_client_id,
                        'income' => $guarantors->income,
                        'created_at' => $guarantors->created_at,
                        'updated_at' => $guarantors->updated_at,

                    ]);

                DB::table('tbl_main_join_guarantor')->insert([
                    'main_guarantor_id' => DB::connection('main')
                        ->table('guarantors')
                        ->where('nrc_number', $guarantors->old_nrc)
                        ->orWhere('nrc_number', $guarantors->nrc)
                        ->first()->id,
                    'portal_guarantor_id' => $guarantors->guarantor_uniquekey,
                ]);
            }
        }
    }
}
