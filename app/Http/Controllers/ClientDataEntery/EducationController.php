<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Education;
use Illuminate\Http\Request;

class EducationController extends Controller
{
    //Index Page
    public function index()
    {
        $educations = Education::Paginate(10);
        return view('client.dataentery.educations.index', compact('educations'));
    }

    //Create Page
    public function create()
    {
        return view('client.dataentery.educations.create');
    }

    
    public function store(Request $request)
    {
        //validation
        $validator=$request->validate([
            'education_name' => ['required', 'string', 'max:255', 'unique:tbl_educations'],
            'education_name_mm' => ['required','string','max:255']
        ]);
        
        if($validator){

            //data insert
        $tbl_education = new Education();
        $tbl_education->education_name =$request->education_name;
        $tbl_education->education_name_mm = $request->education_name_mm;
        $tbl_education->save();

        //return
        return redirect('educations/')->with("successMsg",'New Education Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

   

   //Edit Page
    public function edit($id)
    {
        $education=Education::find($id);
        return view('client.dataentery.educations.edit', compact('education'));
    }

    public function update(Request $request, $id)
    {

        //data insert
        $tbl_education = Education::find($id);
        $tbl_education->education_name =$request->education_name;
        $tbl_education->education_name_mm = $request->education_name_mm;
        $tbl_education->save();

        //return
        return redirect('educations/')->with("successMsg",'Existing Education is Updated in your data');
    }

     //View Page
     public function view($id)
     {
         $education = Education::find($id);
         return view('client.dataentery.educations.view', compact('education'));
     }

    //Delete
    public function destroy($id)
    {
        $education = Education::find($id);
        $education ->delete();

        return redirect('educations/')->with("successMsg",'Existing Education is Delete in your data');
    }
}
