<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Branch;
use App\Models\ClientDataEnteries\Center;
use App\Models\ClientDataEnteries\Group;
use App\Models\ClientDataEnteries\Staff;
use App\Models\Clients\ClientBasicInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{
    //Index Page
    public function index()
    {
        if (request()->has('search_grouplist')) {
            $searchterm = request()->get('search_grouplist');
            $results = DB::table('tbl_group')
                ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_group.branch_id')
                ->leftJoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', 'tbl_group.group_uniquekey')
                ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', 'tbl_group.client_id')
                ->where('tbl_group.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->where('tbl_group.level_status', '=', 'leader')
                ->where('tbl_group.group_uniquekey', 'LIKE', $searchterm . '%')
                ->orWhere('tbl_main_join_group.main_group_code', 'LIKE', $searchterm . '%')
                ->orWhere('tbl_client_basic_info.name', 'LIKE', $searchterm . '%')
                ->orWhere('tbl_branches.branch_name', 'LIKE', $searchterm . '%')
                ->Paginate(10);
        } else {
            $results = DB::table('tbl_group')
                ->where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                // ->where('level_status','=','leader')
                // ->orWhere('level_status', null)
                ->Paginate(10);
        }

        // dd($results);
        $groups = array();
        foreach ($results as $result) {
            if ($result->level_status == 'leader') {
                //             	if($result->staff_id){
                //                 	$staffs = DB::table('tbl_staff')->where('staff_code',$result->staff_id)->select('name')->first();

                //                 	if($staffs){
                //             			$name=$staffs->name;
                //             		}else{
                //             			$name=NULL;
                //             		}

                //                 }elseif($result->client_id){
                if ($result->client_id) {
                    $name = DB::table('tbl_client_basic_info')->where('client_uniquekey', $result->client_id)->first()->name;
                } else {
                    $name = '';
                }
                //                 }


                $branches = DB::table('tbl_branches')->where('id', $result->branch_id)->select('branch_name')->first();
                $centers = DB::table('tbl_center')->where('center_uniquekey', $result->center_id)->select('center_uniquekey')->first();

                // dd($staffs->name);
                $groups[] = array(
                    'id' => $result->id,
                    'group_uniquekey' => $result->group_uniquekey,
                    'staff_id' => $name,
                    'branch_id' => $branches->branch_name,
                    // 'center_id' =>$centers->center_uniquekey,
                    'level_status' => $result->level_status,
                    "del_status" =>  $result->del_status,
                    "created_at" =>  $result->created_at,
                    "updated_at" =>  $result->updated_at
                );
            } else {
                $clients = DB::table('tbl_client_basic_info')->where('client_uniquekey', $result->client_id)->select('name')->first();
                $branches = DB::table('tbl_branches')->where('id', $result->branch_id)->select('branch_name')->first();
                $centers = DB::table('tbl_center')->where('center_uniquekey', $result->center_id)->select('center_uniquekey')->first();
                $groups[] = array(
                    'id' => $result->id,
                    'group_uniquekey' => $result->group_uniquekey,
                    'staff_id' => 'N/A',
                    'branch_id' => $branches->branch_name,
                    // 'center_id' =>$centers->center_uniquekey,
                    'level_status' => $result->level_status,
                    "del_status" =>  $result->del_status,
                    "created_at" =>  $result->created_at,
                    "updated_at" =>  $result->updated_at
                );
            }
        }
        // 	$a=DB::table('tbl_group')->where('level_status','=','leader')->get();
        // dd($a);

        return view('client.dataentery.groups.index', compact('groups', 'results'));
    }

    //Create Page
    public function create()
    {


        $branches = Branch::select('branch_code', 'branch_name', 'id')->get();
        // dd($branches);
        // $centers2 = Center::select('center_uniquekey')->where('branch_id', DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id)->get();
        $centers = DB::table('tbl_center')
            ->leftJoin('tbl_branches', 'tbl_branches.id', 'tbl_center.branch_id')
            ->leftJoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', 'tbl_center.center_uniquekey')
            ->where('tbl_center.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
            ->select('tbl_center.*', 'tbl_main_join_center.*', 'tbl_branches.branch_code AS branch_code')
            ->get();
        // dd($centers);
        $staffs = Staff::select('staff_code', 'name')->get();
        $clients = DB::table('tbl_client_basic_info')
            ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', 'tbl_client_basic_info.client_uniquekey')
            ->where('tbl_client_join.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('tbl_group')
                    ->whereRaw('tbl_client_basic_info.client_uniquekey = tbl_group.client_id');
            })
            ->select('tbl_client_basic_info.client_uniquekey', 'tbl_client_basic_info.name', 'tbl_client_basic_info.id')
            ->orderby('tbl_client_basic_info.client_uniquekey', 'asc')
            ->get();
        // dd($staffs);
        return view('client.dataentery.groups.create', compact('branches', 'centers', 'staffs', 'clients'));
    }


    public function store(Request $request)
    {
        $validator = $request->validate([
            'group_uniquekey' => ['required', 'string', 'max:255'],
            'staff_id' => ['string', 'max:255', 'nullable'],
            'client_id' => ['string', 'max:255', 'nullable'],
            'branch_id' => ['required', 'string', 'max:255'],
            'center_id' => ['required', 'string', 'max:255'],
            'level_status' => ['required', 'string', 'max:255']
        ]);

        if ($validator) {
            $group = new Group;
            $group->group_uniquekey = $request->group_uniquekey;
            $group->staff_id = $request->staff_id;
            $group->client_id = $request->client_id;
            $group->branch_id = $request->branch_id;
            $group->center_id = $request->center_id;
            $group->level_status = NULL;
            $group->save();

            $this->liveGroupReverse($request->group_uniquekey);

            return redirect('groups/')->with("successMsg", 'New Group is Added in your data');

            // Group::create($request->all());
            // return redirect('groups/')->with("successMsg",'New Group is Added in your data');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }


    //Edit Page
    public function edit(Group $group, $id)
    {
        $group = Group::where('group_uniquekey', $id)->first();
        $staff = DB::table('tbl_group')
            ->leftJoin('tbl_center', 'tbl_center.center_uniquekey', '=', 'tbl_group.center_id')
            ->leftJoin('tbl_staff', 'tbl_staff.staff_code', '=', 'tbl_center.staff_id')
            ->where('tbl_group.group_uniquekey', '=', $id)
            ->select('tbl_staff.staff_code', 'tbl_staff.name')
            ->first();
        //        dd($staff);
        $branches = Branch::select('branch_code', 'branch_name', 'id')->get();
        $centers = Center::select('center_uniquekey')->get();
        $staffs = Staff::select('staff_code', 'name')->get();
        $clients = ClientBasicInfo::select('client_uniquekey', 'name')
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('tbl_group')
                    ->whereRaw('tbl_client_basic_info.client_uniquekey = tbl_group.client_id');
            })
            ->get();
        return view('client.dataentery.groups.edit', compact('group', 'branches', 'centers', 'staffs', 'clients', 'staff'));
    }

    public function update(Request $request, Group $group, $id)
    {

        // dd($request);
        $validator = $request->validate([
            'group_uniquekey' => ['required', 'string', 'max:255'],
            'staff_id' => ['max:255'],
            'client_id' => ['max:255'],
            'branch_id' => ['required', 'string', 'max:255'],
            'center_id' => ['required', 'string', 'max:255'],
            'level_status' => ['required', 'string', 'max:255']
        ]);
        if ($validator) {
            // $group->update($request->all());
            $group = Group::where('group_uniquekey', $id)->first();
            $group->group_uniquekey = $request->group_uniquekey;
            $group->staff_id = $request->staff_id;
            $group->client_id = $request->client_id;
            $group->branch_id = $request->branch_id;
            $group->center_id = $request->center_id;
            $group->level_status = $request->level_status;
            $group->save();
            return redirect('groups/')->with("successMsg", 'Existing Group is Updated in your data');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    //View Page
    public function view(Group $group, $id)
    {
        $group = DB::table('tbl_group')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', 'tbl_group.client_id')
            ->leftJoin('tbl_staff', 'tbl_staff.staff_code', '=', 'tbl_group.staff_id')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_group.branch_id')
            ->leftJoin('tbl_center', 'tbl_center.branch_id', '=', 'tbl_group.branch_id')
            ->where('tbl_group.group_uniquekey', '=', $id)
            ->select(
                'tbl_group.group_uniquekey',
                'tbl_client_basic_info.name AS client_name',
                'tbl_client_basic_info.client_uniquekey AS c_id',
                'tbl_staff.name AS staff_name',
                'tbl_branches.branch_name',
                'tbl_center.center_uniquekey',
                'tbl_group.level_status'
            )
            ->first();
        // dd($group);

        return view('client.dataentery.groups.view', compact('group'));
    }

    //Delete
    public function destroy(Group $group, $id)
    {
        $group = Group::where('group_uniquekey', $id)->first();
        $group->delete();

        $main_group = DB::table('tbl_main_join_group')->where('portal_group_id', $id)->first();
        DB::connection('main')->table('group_loans')->where('group_code', $main_group->main_group_code)->delete();
        DB::table('tbl_main_join_group')->where('portal_group_id', $id)->delete();
        return redirect('groups/')->with("successMsg", 'Existing Group is Deleted in your data');
    }

    public function getLatestId(Request $request)
    {
        if ($request->get('bcode')) {
            $data = $request->get('bcode');
            $branchid = DB::table('tbl_branches')->where('branch_code', $data)->select('id')->first();
            $uniquekey = DB::table('tbl_group')->where('branch_id', $branchid->id)->select('group_uniquekey')->get()->last();
            $group_uniquekey = $uniquekey->group_uniquekey;
            if ($group_uniquekey) {
                $bstr = substr($group_uniquekey, 6);
                // $id = substr($bstr, -5, 5);
                $bstr_cusm = $bstr + 1;
                $bstr_cusm = (string)str_pad($bstr_cusm, 5, '0', STR_PAD_LEFT);
            } else {
                $group_uniquekey = $data . '-06-00001';
            }

            return response()->json($bstr_cusm);
        } else {
            return response()->json(['status_code' => 500, 'message' => 'loan id generate fail', 'data' => null]);
        }
    }

    public function getUniqueID(Request $request)
    {
        $branchid = $request->branchid;
        $branch = DB::table('tbl_branches')->where('id', $branchid)->first()->id;
        $idnum = DB::table('tbl_group')
            ->where('branch_id', $branch)
            ->where('level_status', '=', 'leader')
            ->orWhere('level_status', NULL)
            ->whereNull('level_status')
            ->get()
            ->count();


        $idnum = $idnum + 1;
        $idnum = str_pad($idnum, 5, 0, STR_PAD_LEFT);

        return response()->json($idnum);
    }

    public function liveGroupReverse($group_id)
    {
        $branch_id = Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id;

        // dd($branch_id);
        $groups = DB::connection('portal')
            ->table('tbl_group')
            ->leftJoin('tbl_branches', 'tbl_group.branch_id', '=', 'tbl_branches.id')
            ->where('tbl_group.branch_id', $branch_id)
            ->where('tbl_group.group_uniquekey', $group_id)
            // ->leftJoin('tbl_main_join_center','tbl_group.center_id','=','tbl_main_join_center.portal_center_id')
            // ->leftJoin('tbl_main_join_group','tbl_group.group_uniquekey','=','tbl_main_join_group.portal_group_id')
            ->first();
        // dd($groups);
        $main_center_id = DB::table('tbl_main_join_center')->where('portal_center_id', $groups->center_id)->first()->main_center_id;
        $main_center_code = DB::connection('main')->table('center_leaders')->where('id', $main_center_id)->first()->code;
        $main_center_code = explode('-', $main_center_code);
        $main_center_code = $main_center_code[2];
        $main_branch_id = DB::connection('main')->table('branches')->where('id', $branch_id)->first()->code;
        // dd($main_center_id);
        // $group_code_generate = DB::connection('main')
        //     ->table('group_loans')
        //     ->where('branch_id', $branch_id)
        //     ->where('center_id',$main_center_id)
        //     ->orderBy('group_code', 'desc')
        //     ->first()->group_code;
        if (DB::connection('main')->table('group_loans')->where('branch_id', $branch_id)->where('center_id', $main_center_id)->orderBy('group_code', 'desc')->first()) {
            $group_code_generate = DB::connection('main')
                ->table('group_loans')
                ->where('branch_id', $branch_id)
                ->where('center_id', $main_center_id)
                ->orderBy('group_code', 'desc')
                ->first()->group_code;
            $group_code_new = ++$group_code_generate;
        } else {
            $group_code_new = 'gp-' . $main_branch_id . '-' . $main_center_code . '-0001';
            // dd($group_code_new);
        }
        // dd($group_code_new);
        // return response()->json($group_code_new);

        if (
            DB::table('tbl_main_join_center')
            ->where('portal_center_id', $groups->center_id)
            ->first()
        ) {
            $mainCenterID = DB::table('tbl_main_join_center')
                ->where('portal_center_id', $groups->center_id)
                ->first()->main_center_id;
        }

        DB::connection('main')
            ->table('group_loans')
            ->insert([
                //'id' => $groups[$i]->id,
                'group_code' => $group_code_new,
                'group_name' => $group_code_new,
                'center_id' => $mainCenterID,
                'created_at' => $groups->created_at,
                'updated_at' => $groups->updated_at,
                // 'created_by' => $groups->staff_id,
                // 'updated_by' => $groups->staff_id,
                'branch_id' => $groups->branch_id,
            ]);

        DB::table('tbl_main_join_group')->insert([
            'main_group_id' => DB::connection('main')
                ->table('group_loans')
                ->where('group_code', $group_code_new)
                ->first()->id,
            'main_group_code' => DB::connection('main')
                ->table('group_loans')
                ->where('group_code', $group_code_new)
                ->first()->group_code,
            'portal_group_id' => $groups->group_uniquekey,
        ]);
    }
}
