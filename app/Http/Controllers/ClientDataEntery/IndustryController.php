<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Industry;
use Illuminate\Http\Request;

class IndustryController extends Controller
{
    //Index Page
    public function index()
    {
        $industries = Industry::Paginate(10);
        return view('client.dataentery.industries.index', compact('industries'));
    }

   //Create Page
    public function create()
    {
      return view('client.dataentery.industries.create');
    }

    public function store(Request $request)
    {
        $validator=$request->validate([
            'industry_name' => ['required', 'string', 'max:255', 'unique:tbl_industry_type'],
            'industry_name_mm' => ['required','string','max:255']
        ]);

        if($validator){
           
        $industry = new Industry();
        $industry->industry_name =$request->industry_name;
        $industry->industry_name_mm = $request->industry_name_mm;
        $industry->save();

        //return
        return redirect('industries/')->with("successMsg",'New Industry is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    //Edit Page
    public function edit($id)
    {
        $industry= Industry::find($id);
        return view('client.dataentery.industries.edit', compact('industry'));
    }

    public function update(Request $request, $id)
    {
        $validator=$request->validate([
            'industry_name' => ['required', 'string', 'max:255'],
            'industry_name_mm' => ['required','string','max:255']
        ]);

        if($validator){
            $industry = Industry::find($id);
            $industry->industry_name =$request->industry_name;
            $industry->industry_name_mm = $request->industry_name_mm;
            $industry->save(); 
            return redirect('industries/')->with("successMsg",'Existing Industry is Updated in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    //View Page
    public function view($id)
    {
        $industry = Industry::find($id);
        return view('client.dataentery.industries.view', compact('industry'));
    }

    //Delete
    public function destroy($id)
    {
        $industry = Industry::find($id);
        $industry -> delete();

        return redirect('industries/')->with("successMsg",'Existing Industry is Deleted in your data');

    }
}
