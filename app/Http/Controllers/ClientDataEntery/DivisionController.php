<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Division;
use Illuminate\Http\Request;

class DivisionController extends Controller
{
    //Index Page
    public function index()
    {
        $divisions = Division::Paginate(10);
        return view('client.dataentery.divisions.index', compact('divisions'));
    }

    //Create Page
    public function create()
    {
        //
        return view('client.dataentery.divisions.create');
    }

    public function store(Request $request)
    {
        //validation
        $validator=$request->validate([
            'division_name' => ['required', 'string', 'max:255', 'unique:tbl_divisions'],
            'division_name_mm' => ['required','string','max:255']
        ]);
        
        if($validator){
            //data insert
        $division = new Division();
        $division->division_name =$request->division_name;
        $division->division_name_mm = $request->division_name_mm;
        $division->save();

        //return
        return redirect('divisions/')->with("successMsg",'New Division Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }

        

    }


    //Edit Page
    public function edit($id)
    {
        //
        $division=Division::find($id);
        return view('client.dataentery.divisions.edit', compact('division'));
    }

    
    public function update(Request $request, $id)
    {
        //
        $division_name = $request->division_name;
        $division_name_mm = $request->division_name_mm;

        //data insert
        $division = Division::find($id);
        $division->division_name =$request->division_name;
        $division->division_name_mm = $request->division_name_mm;
        $division->save();

        //return
        return redirect('divisions/')->with("successMsg",'Existing Division is Updated in your data');

    }

    //View Page
    public function view($id)
    {
        //
        $division = Division::find($id);
        return view('client.dataentery.divisions.view', compact('division'));
    }
    //Delete
    public function destroy($id)
    {
        //
        $division = Division::find($id);
        $division ->delete();

        return redirect('divisions/')->with("successMsg",'Existing Division is Delete in your data');

    }
}
