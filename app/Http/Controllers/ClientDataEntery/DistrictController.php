<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\District;
use App\Models\ClientDataEnteries\Division;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DistrictController extends Controller
{
    //Index Page
    public function index()
    {
       
        $results =DB::table('tbl_districts')->Paginate(10);
        $districts =array();
        foreach($results as $result){
            $division = DB::table('tbl_divisions')->where('id',$result->division_id)->select('division_name')->first();
            $districts[] =array(
                'id' =>$result->id,
                'division_id' =>$division->division_name,
                'district_name' =>$result->district_name,
                'district_name_mm' =>$result->district_name_mm,
                "del_status" =>  $result->del_status,
                "created_at" =>  $result->created_at,
                "updated_at" =>  $result->updated_at
            );
        }
        return view('client.dataentery.districts.index',compact('districts','results'));
    }

    //Create Page
    public function create()
    {
        //
        
        $divisions = Division::select('division_name','id')->get();
        return view('client.dataentery.districts.create', compact('divisions'));
    }

    
    public function store(Request $request)
    {
        //validation
        $validator=$request->validate([
            // 'division_id' => 'rquired',
            'district_name' => ['required', 'string', 'max:255', 'unique:tbl_districts'],
            'district_name_mm' => ['required','string','max:255']
        ]);
    
        if($validator){

            //data insert
        $district = new District();
        $district->division_id = $request->division_id;
        $district->district_name =$request->district_name;
        $district->district_name_mm = $request->district_name_mm;
        $district->save();

        //return
        return redirect('districts/')->with("successMsg",'New District Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }

    }

    //Edit Page
    public function edit($id)
    {
        $divisions = Division::all();
        $district = District::find($id);
        return view('client.dataentery.districts.edit', compact('district','divisions'));
    }

    
    public function update(Request $request, $id)
    {
        //data insert
        $district =District::find($id);
        $district->division_id = $request->division_id;
        $district->district_name =$request->district_name;
        $district->district_name_mm = $request->district_name_mm;
        $district->save();

        return redirect('districts/')->with("successMsg",'Existing District is Updated in your data');

    }

    //View Page
    public function view($id)
    {
        $district =  DB::table('tbl_districts')
                        ->leftJoin('tbl_divisions','tbl_divisions.id','=','tbl_districts.division_id')
                        ->where('tbl_districts.id','=',$id)
                        ->first();

        return view('client.dataentery.districts.view',compact('district'));
    }

    //Delete
    public function destroy($id)
    {
        $district = District::find($id);
        $district->delete();

        return redirect('districts/')->with("successMsg",'Existing District Delete in your data');
    }
}
