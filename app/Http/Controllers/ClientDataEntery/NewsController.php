<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ClientDataEnteries\Company;
use App\Models\ClientDataEnteries\Department;
use App\Models\ClientDataEnteries\News;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::Paginate(10);
        return view('client.dataentery.news.index',compact('news'));
    }

    public function create()
    {
        $departments=Department::orderby('department_name')->select('department_name','id')->get();
        $companies = Company::orderby('company_name')->select('company_name','id')->get();
        return view('client.dataentery.news.create',compact('departments','companies'));
    }

    public function store(Request $request)
    {
        //dd($request);
        $validator = $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'author' => ['required','string','max:255'],
            'department' => ['required','string','max:255'],
            'company' => ['required','string','max:255'],
            'description' => ['required','string','max:255'],
            //'photo' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048']
        ]);

        if($validator){
            
            if($request->hasFile('photo')){
                foreach($request->file('photo') as $file){
                    $photo_name = time().'.'.$file->getClientOriginalName();
                    $destinationPath = 'public/clientphotos/';
                    $file->storeAs($destinationPath,$photo_name);
                    $photo_data[] = $photo_name;
                }
            }

            // $photo_name = time().'.'.$request->photo->getClientOriginalName();
            // $destinationPath = 'public/clientphotos/'; 
            // $request->photo->storeAs($destinationPath, $photo_name);

            $news = new News;
            $news->title = $request->title;
            $news->author = $request->author;
            $news->department = $request->department;
            $news->company = $request->company;
            $news->description = $request->description;
            $news->photo = json_encode($photo_data);
            $news->save();

            return redirect('news/')->with("successMsg",'News is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    public function edit($id)
    {
        
        $news = News::find($id);
        $departments=Department::orderby('department_name')->select('department_name','id')->get();
        $companies = Company::orderby('company_name')->select('company_name','id')->get();
        return view('client.dataentery.news.edit', compact('news','departments','companies'));
    }

    public function update(Request $request, $id)
    {
        $validator = $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'author' => ['required','string','max:255'],
            'department' => ['required','string','max:255'],
            'company' => ['required','string','max:255'],
            'description' => ['required','string','max:255'],
            //'photo' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048']
        ]);

        if($validator){
            $news = News::find($id);
            if($request->hasFile('photo')){
                foreach($request->file('photo') as $file){
                    $photo_name = time().'.'.$file->getClientOriginalName();
                    $destinationPath = 'public/clientphotos/';
                    $file->storeAs($destinationPath,$photo_name);
                    $photo_data[] = $photo_name;
                }
            }else{
                $photo_name = $news->photo;
            }

            $news->title = $request->title;
            $news->author = $request->author;
            $news->department = $request->department;
            $news->company = $request->company;
            $news->description = $request->description;
            $news->photo = json_encode($photo_data);
            $news->save();

            return redirect('news/')->with("successMsg",'News is Updated in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    public function view($id)
    {
        $news = News::find($id);
        return view('client.dataentery.news.view',compact('news'));
    }

    public function destroy($id)
    {
        $news = News::find($id);
        $news->delete();
        return redirect('news/')->with("successMsg",'Existing News is Deleted in your data');
    }
}
