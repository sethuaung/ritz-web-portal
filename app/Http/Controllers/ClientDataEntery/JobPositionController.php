<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Department;
use App\Models\ClientDataEnteries\Industry;
use App\Models\ClientDataEnteries\JobPosition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JobPositionController extends Controller
{
    public function index()
    {
        $results =DB::table('tbl_job_position')->Paginate(10);
        $jobpositions =array();
        foreach($results as $result){
            $departments = DB::table('tbl_job_department')->where('id',$result->department_id)->select('department_name')->first();
            $jobpositions[] =array(
                'id' =>$result->id,
                'department_id' =>$departments->department_name,
                'job_position_name' =>$result->job_position_name,
                'job_position_name_mm' =>$result->job_position_name_mm,
                "del_status" =>  $result->del_status,
                "created_at" =>  $result->created_at,
                "updated_at" =>  $result->updated_at
            );
        }
        return view('client.dataentery.jobpositions.index', compact('jobpositions','results'));
    }

    //Create Page
    public function create()
    {
        $industries=Industry::select()->get();
        $departments=Department::select()->get();
        return view('client.dataentery.jobpositions.create',compact('departments','industries'));
    }

    public function store(Request $request)
    {
        $validator=$request->validate([
            'industry_id' => 'required',
            'department_id' => 'required',
            'job_position_name' => ['required', 'string', 'max:255', 'unique:tbl_job_position'],
            'job_position_name_mm' => ['required','string','max:255']
        ]);

        if($validator){
           
        $jobposition = new JobPosition;
        $jobposition->department_id =$request->department_id;
        $jobposition->job_position_name =$request->job_position_name;
        $jobposition->job_position_name_mm = $request->job_position_name_mm;
        $jobposition->save();

        //return
        return redirect('jobpositions/')->with("successMsg",'New Job Position is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    //Edit Page
    public function edit($id)
    {
        $industries=Industry::select('industry_name','id')->get();
        $departments=Department::select()->get();
        $jobposition= JobPosition::find($id);
        return view('client.dataentery.jobpositions.edit', compact('jobposition','departments','industries'));
    }

    
    public function update(Request $request, $id)
    {
        $validator=$request->validate([
            'industry_id' => 'required',
            'department_id' => 'required',
            'job_position_name' => ['required', 'string', 'max:255'],
            'job_position_name_mm' => ['required','string','max:255']
        ]);

        if($validator){
           
        $jobposition = JobPosition::find($id);
        $jobposition->department_id =$request->department_id;
        $jobposition->job_position_name =$request->job_position_name;
        $jobposition->job_position_name_mm = $request->job_position_name_mm;
        $jobposition->save();

        //return
        return redirect('jobpositions/')->with("successMsg",'Existing Job Position is Updated in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    //View Page
    public function view($id)
    {
        
        $jobposition = DB::table('tbl_job_position')
                        ->leftJoin('tbl_job_department','tbl_job_department.id','=','tbl_job_position.department_id')
                        ->where('tbl_job_position.id','=',$id)
                        ->first();
        
        $departments = DB::table('tbl_job_department')
                        ->leftJoin('tbl_industry_type','tbl_industry_type.id','=','tbl_job_department.industry_id')
                        ->where('tbl_job_department.id','=',$jobposition->department_id)
                        ->first();
        return view('client.dataentery.jobpositions.view', compact('jobposition','departments'));
    }


    //Delete
    public function destroy($id)
    {
        $jobposition = JobPosition::find($id);
        $jobposition -> delete();

        return redirect('jobpositions/')->with("successMsg",'Existing Job Position is Deleted in your data');

    }

    public function getDept(Request $request)
 {
     
            $depts = Department::where("industry_id",$request->industry_id)
                        ->pluck("department_name","id");
            return response()->json($depts);
 }

 public function getJob(Request $request)
 {
     
            $jobs = JobPosition::where("department_id",$request->department_id)
                        ->pluck("job_position_name","id");
            return response()->json($jobs);
 }
}
