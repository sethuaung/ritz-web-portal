<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\District;
use App\Models\ClientDataEnteries\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProvinceController extends Controller
{
    //Index Page
    public function index()
    {
        //
        $results =DB::table('tbl_provinces')->Paginate(10);
        $provinces =array();
        foreach($results as $result){
            $district = DB::table('tbl_districts')->where('id',$result->district_id)->select('district_name')->first();
            $provinces[] =array(
                'id' =>$result->id,
                'district_id' =>$district->district_name,
                'province_name' =>$result->province_name,
                'province_name_mm' =>$result->province_name_mm,
                "del_status" =>  $result->del_status,
                "created_at" =>  $result->created_at,
                "updated_at" =>  $result->updated_at
            );
        }

        return view('client.dataentery.provinces.index',compact('provinces','results'));
    }

    //Create Page
    public function create()
    {
        
        $districts = District::select('district_name','id')->get();

        return view('client.dataentery.provinces.create', compact('districts'));
    }

    public function store(Request $request)
    {
         //validation
       $validator=$request->validate([
        // 'district_id' => 'rquired',
        'province_name' => ['required', 'string', 'max:255', 'unique:tbl_provinces'],
        'province_name_mm' => ['required','string','max:255']
    ]);
    
    if($validator){

        //data insert
    $tbl_province = new Province;
    $tbl_province->district_id = $request->district_id;
    $tbl_province->province_name =$request->province_name;
    $tbl_province->province_name_mm = $request->province_name_mm;
    $tbl_province->save();

    //return
    return redirect('provinces/')->with("successMsg",'New Province Added in your data');
    }else{
        return redirect()->back()->withErrors($validator);
    }
    }

    //Edit Page
    public function edit($id)
    {
        $province = Province::find($id);
        $districts = District::all();

        return view('client.dataentery.provinces.edit',compact('province','districts'));
    }

    public function update(Request $request, $id)
    {
        //data insert
    $tbl_province = Province::find($id);
    $tbl_province->district_id = $request->district_id;
    $tbl_province->province_name =$request->province_name;
    $tbl_province->province_name_mm = $request->province_name_mm;
    $tbl_province->save();

    //return
    return redirect('provinces/')->with("successMsg",'Existing Province Update in your data');
    }

    //View Page
    public function view($id)   
    {
        $province = DB::table('tbl_provinces')
                    ->leftJoin('tbl_districts','tbl_districts.id','=','tbl_provinces.district_id')
                    ->where('tbl_provinces.id','=',$id)
                    ->first();

        return view('client.dataentery.provinces.view',compact('province'));
    }

    //Delete
    public function destroy($id)
    {
        $province = Province::find($id);
        $province->delete();

    return redirect('provinces/')->with("successMsg",'Existing Province Delete in your data');
    }
}
