<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\City;
use App\Models\ClientDataEnteries\Division;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CityController extends Controller
{
    // Index Page
    public function index()
    {
        //
        $results =DB::table('tbl_cities')->Paginate(10);
        $cities =array();
        foreach($results as $result){
            $division = DB::table('tbl_divisions')->where('id',$result->division_id)->select('division_name')->first();
            $cities[] =array(
                'id' =>$result->id,
                'division_id' =>$division->division_name,
                'city_name' =>$result->city_name,
                'city_name_mm' =>$result->city_name_mm,
                "del_status" =>  $result->del_status,
                "created_at" =>  $result->created_at,
                "updated_at" =>  $result->updated_at
            );
        }
        return view('client.dataentery.cities.index',compact('cities','results'));
    }

    // Create Page
    public function create()
    {
        //
        
        $divisions = Division::select('division_name','id')->get();
        return view('client.dataentery.cities.create', compact('divisions'));
    }

    public function store(Request $request)
    {
        //validation
       $validator=$request->validate([
        // 'division_id' => 'rquired',
        'city_name' => ['required', 'string', 'max:255', 'unique:tbl_cities'],
        'city_name_mm' => ['required','string','max:255']
        ]);
    
        if($validator){
            

            //data insert
        $tbl_city = new City;
        $tbl_city->division_id = $request->division_id;
        $tbl_city->city_name =$request->city_name;
        $tbl_city->city_name_mm = $request->city_name_mm;
        $tbl_city->save();

        //return
        return redirect('cities/')->with("successMsg",'New City Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    // Edit Page
    public function edit($id)
    {
        // dd($id);
        $divisions = Division::all();
        $city = City::find($id);
        return view('client.dataentery.cities.edit', compact('city','divisions'));
    }

    public function update(Request $request, $id)
    {
        //data insert
        $tbl_city =City::find($id);
        $tbl_city->division_id = $request->division_id;
        $tbl_city->city_name =$request->city_name;
        $tbl_city->city_name_mm = $request->city_name_mm;
        $tbl_city->save();

        return redirect('cities/')->with("successMsg",'Existing City is Updated in your data');
    }
    //View Page
    public function view($id)
    {
        $city = DB::table('tbl_cities')
                    ->leftJoin('tbl_divisions','tbl_divisions.id','=','tbl_cities.division_id')
                    ->where('tbl_cities.id','=',$id)
                    ->first();

        return view('client.dataentery.cities.view',compact('city'));
    }

    //Delete
    public function destroy($id)
    {
        $city = City::find($id);
        $city->delete();

        return redirect('cities/')->with("successMsg",'Existing City Delete in your data');
    }
}
