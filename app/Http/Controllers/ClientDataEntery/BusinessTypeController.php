<?php

namespace App\Http\Controllers\ClientDataEntery;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\BusinessType;

class BusinessTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businesstypes = BusinessType::Paginate(10);
        return view('client.dataentery.business_types.index', compact('businesstypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.dataentery.business_types.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=$request->validate([
            'business_type_name' => ['required', 'string', 'max:255', 'unique:tbl_business_type'],
            'business_type_name_mm' => ['required','string','max:255']
        ]);

        if($validator){
           
        $business_type = new BusinessType();
        $business_type->business_type_name =$request->business_type_name;
        $business_type->business_type_name_mm = $request->business_type_name_mm;
        $business_type->save();

        //return
        return redirect('business_types/')->with("successMsg",'New Business Type is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    
    public function edit($id)
    {
        $business_type= BusinessType::find($id);
        return view('client.dataentery.business_types.edit', compact('business_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator=$request->validate([
            'business_type_name' => ['required', 'string', 'max:255',Rule::unique('tbl_business_type')->ignore($id)],
            'business_type_name_mm' => ['required','string','max:255',Rule::unique('tbl_business_type')->ignore($id)]
        ]);

        if($validator){
            $business_type = BusinessType::find($id);
            $business_type->business_type_name =$request->business_type_name;
            $business_type->business_type_name_mm = $request->business_type_name_mm;
            $business_type->save(); 
            return redirect('business_types/')->with("successMsg",'Existing Business Type is Updated in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    //View Page
    public function view($id)
    {
        $business_type = BusinessType::find($id);
        return view('client.dataentery.business_types.view', compact('business_type'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $business_type = BusinessType::find($id);
        $business_type -> delete();

        return redirect('business_types/')->with("successMsg",'Existing Business Type is Deleted in your data');
    }
}
