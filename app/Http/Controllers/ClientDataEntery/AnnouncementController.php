<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\AnnounceDepartment;
use App\Models\ClientDataEnteries\Announcement;
use App\Models\ClientDataEnteries\Branch;
use App\Models\ClientDataEnteries\Company;

use Illuminate\Http\Request;

class AnnouncementController extends Controller
{
    public function index()
    {
        $announcements = Announcement::Paginate(10);
        return view('client.dataentery.announcements.index',compact('announcements'));
    }

    public function create()
    {
        $departments=AnnounceDepartment::orderby('department_name')->select('department_name','id')->get();
        $companies = Company::orderby('company_name')->select('company_name','id')->get();
        $branches = Branch::orderby('branch_name')->select('branch_name','id')->get();
        return view('client.dataentery.announcements.create',compact('departments','companies','branches'));
    }

    public function store(Request $request)
    {
        //dd($request);
        $validator = $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'author' => ['required','string','max:255'],
            'department' => ['required','string','max:255'],
            'company' => ['required','string','max:255'],
            'description' => ['required','string','max:255']
        ]);
        if($validator){
            $announcements = new Announcement;
            $announcements->title = $request->title;
            $announcements->author = $request->author;
            $announcements->company = $request->company;
            $announcements->branch = $request->branch;
            $announcements->department = $request->department;
            $announcements->description = $request->description;
            $announcements->save();

            return redirect('announcements/')->with("successMsg",'Announcement is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    public function edit($id)
    {
        
        $announcements = Announcement::find($id);
        $departments=AnnounceDepartment::orderby('department_name')->select('department_name','id')->get();
        $companies = Company::orderby('company_name')->select('company_name','id')->get();
        $branches = Branch::orderby('branch_name')->select('branch_name','id')->get();
        return view('client.dataentery.announcements.edit', compact('announcements','departments','companies','branches'));
    }

    public function update(Request $request, $id)
    {
        $validator = $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'author' => ['required','string','max:255'],
            'department' => ['required','string','max:255'],
            'company' => ['required','string','max:255'],
            'description' => ['required','string','max:255']
        ]);

        if($validator){
            $announcements = Announcement::find($id);
            
            $announcements->title = $request->title;
            $announcements->author = $request->author;
            $announcements->company = $request->company;
            $announcements->branch = $request->branch;
            $announcements->department = $request->department;
            $announcements->description = $request->description;
            $announcements->save();
            return redirect('announcements/')->with("successMsg",'Announcement is Updated in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    public function view($id)
    {
        $announcements = Announcement::find($id);
        return view('client.dataentery.announcements.view',compact('announcements'));
    }

    public function destroy($id)
    {
        $announcements = Announcement::find($id);
        $announcements->delete();
        return redirect('announcements/')->with("successMsg",'Existing Announcement is Deleted in your data');
    }

}