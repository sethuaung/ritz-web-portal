<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\AnnounceDepartment;
use Illuminate\Http\Request;

class AnnounceDepartmentController extends Controller
{
    public function index()
    {
        $departments = AnnounceDepartment::Paginate(10);
        return view('client.dataentery.announce_departments.index',compact('departments'));
    }

    public function create()
    {
        return view('client.dataentery.announce_departments.create');
    }

    public function store(Request $request)
    {
        $validator = $request->validate([
            'department_name' => ['required', 'string', 'max:255']
        ]);

        if($validator){
            $department = new AnnounceDepartment;
            $department->department_name = $request->department_name;
            $department->save();
            return redirect('announce_departments/')->with("successMsg",'Department Name is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    public function edit($id)
    {
        $department = AnnounceDepartment::find($id);
        return view('client.dataentery.announce_departments.edit', compact('department'));
    }

    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'department_name' => ['required', 'string', 'max:255']
        ]);
        if($validator){
            $department = AnnounceDepartment::find($id);
            $department->department_name = $request->department_name;
            $department->save();
            return redirect('/announce_departments')->with("successMsg",'Department Name is Updated in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
        
    }

    public function view($id)
    {
        $department = AnnounceDepartment::find($id);
        return view('client.dataentery.announce_departments.view',compact('department'));
    }

    public function destroy($id)
    {
        $department = AnnounceDepartment::find($id);
        $department->delete();
        return redirect('announce_departments/')->with("successMsg",'Existing Department is Deleted in your data');
    }
}
