<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Branch;
use App\Models\ClientDataEnteries\Center;
use App\Models\ClientDataEnteries\Staff;
use App\Models\Clients\ClientBasicInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CenterController extends Controller
{
    public function index()
    {
        //Index Page
        if (request()->has('search_centerlist')) {
            $searchterm = request()->get('search_centerlist');
            $results = DB::table('tbl_center')
                ->leftJoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', 'tbl_center.center_uniquekey')
                ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_center.branch_id')
                ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', 'tbl_center.staff_client_id')
                ->leftJoin('tbl_staff', 'tbl_staff.staff_code', '=', 'tbl_center.staff_id')
                ->where('tbl_center.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->where('tbl_main_join_center.main_center_code', 'LIKE', $searchterm . '%')
                ->orWhere('tbl_branches.branch_name', 'LIKE', $searchterm . '%')
                ->orWhere('tbl_center.center_uniquekey', 'LIKE', $searchterm . '%')
                ->orWhere('tbl_client_basic_info.name', 'LIKE', $searchterm . '%')
                ->orWhere('tbl_staff.name', 'LIKE', $searchterm . '%')
                ->Paginate(10);
        } else {
            $results = DB::table('tbl_center')
                ->where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->Paginate(10);
        }

        $centers = array();
        foreach ($results as $result) {
            if ($result->type_status == "staff") {
                $typeofname = DB::table('tbl_staff')->where('staff_code', $result->staff_client_id)->select('name')->first();
                $result_name = $typeofname->name;
            } else  if ($result->type_status == "client") {
                $typeofname = DB::table('tbl_client_basic_info')->where('client_uniquekey', $result->staff_client_id)->select('name')->first();
                if ($typeofname) {
                    $result_name = $typeofname->name;
                } else {
                    $result_name = NULL;
                }
            } else {
                $result_name = "Name not found";
            }
            $branchname = DB::table('tbl_branches')->where('id', $result->branch_id)->select('branch_name')->first();
            $centers[] = array(
                "id" => $result->id,
                "center_uniquekey" =>  $result->center_uniquekey,
                "staff_client_id" =>  $result->staff_client_id,
                "staff_client_name" => $result_name,
                "staff_id" => $result->staff_id,
                "branch_id" => $branchname->branch_name,
                "type_status" =>  $result->type_status,
                "del_status" =>  $result->del_status,
                "created_at" =>  $result->created_at,
                "updated_at" =>  $result->updated_at
            );
        }
        return view('client.dataentery.centers.index', compact('centers', 'results'));
    }

    //Create Page
    public function create()
    {
        $this->permissionFilter("centers");
        $branches = Branch::select('branch_code', 'branch_name', 'id')->get();
        $divisions = DB::table('addresses')->where('type', '=', 'state')->get();

        return view('client.dataentery.centers.create', compact('branches', 'divisions'));
    }

    public function store(Request $request)
    {
        $this->permissionFilter("centers");
        $check = $this->mainDuplicateCheck($request->main_center_id);
        if ($check == 'true') {
            return redirect()->back()->withErrors("Main Center ID already exists!");
        } else {
            $validator = $request->validate([
                'center_uniquekey' => ['required', 'string', 'max:255', 'unique:tbl_center'],
                // 'staff_client_id' => ['string','max:255'],
                'branch_id' => ['required', 'string', 'max:255'],
                'staff_id' => ['required', 'string', 'max:255'],
                'type_status' => ['string', 'max:255']
            ]);
            // dd($request);
            if ($validator) {
                $center =  new Center;
                $center->center_uniquekey = $request->center_uniquekey;
                $center->staff_client_id = $request->staff_client_id;
                $center->branch_id = $request->branch_id;
                $center->staff_id = $request->staff_id;
                $center->type_status = $request->type_status;
                $center->district_id = $request->district_id;
                $center->township_id = $request->township_id;
                $center->village_id = $request->village_id;
                $center->quarter_id = $request->ward_id;
                $center->division_id = $request->division_id;
                $center->address = $request->address_primary;
                $center->phone_number = $request->phoneno;
                $center->save();

                $this->liveCenterReverse($request->center_uniquekey, $request->main_center_id);
                return redirect('centers/')->with("successMsg", 'New Center is Added in your data');
                // Center::create($request->all());
                // return redirect('centers/')->with("successMsg",'New Center is Added in your data');
            } else {
                return redirect()->back()->withErrors($validator);
            }
        }
    }

    //Edit Page
    public function edit($id)
    {
        $this->permissionFilter("centers");
        $branches = Branch::select('branch_code', 'branch_name', 'id')->get();
        $staff = DB::table('tbl_staff')->get();
        $center = DB::table('tbl_center')
            ->leftjoin('tbl_branches', 'tbl_center.branch_id', '=', 'tbl_branches.id')
            ->leftjoin('tbl_staff', 'tbl_center.staff_id', '=', 'tbl_staff.staff_code')
            ->where('tbl_center.center_uniquekey', '=', $id)
            ->select('tbl_center.*', 'tbl_staff.name as staff_name')
            ->first();
        // dd($center);
        if ($center->type_status == "staff") {
            $typeofname = DB::table('tbl_staff')->where('staff_code', $center->staff_client_id)->select('name', 'staff_code')->first();
            if ($typeofname) {
                $centerleadername = $typeofname->name;
                $centerleaderid = $typeofname->staff_code;
            } else {
                $centerleadername = NULL;
                $centerleaderid = NULL;
            }
        } else if ($center->type_status == "client") {
            $typeofname = DB::table('tbl_client_basic_info')->where('client_uniquekey', $center->staff_client_id)->select('name', 'client_uniquekey')->first();
            if ($typeofname) {
                $centerleadername = $typeofname->name;
                $centerleaderid = $typeofname->client_uniquekey;
            } else {
                $centerleadername = NULL;
                $centerleaderid = NULL;
            }
        } else {
            $centerleadername = NULL;
            $centerleaderid = NULL;
        }
        $divisions = DB::table('addresses')->where('type', '=', 'state')->get();
        $districts = DB::table('addresses')->where('parent_code', $center->division_id)->get();
        $townships = DB::table('addresses')->where('parent_code', $center->district_id)->get();
        $villages = DB::table('addresses')->where('parent_code', $center->township_id)->get();
        $quarters = DB::table('addresses')->where('parent_code', $center->village_id)->get();
        $core_center_id = DB::table('tbl_main_join_center')->where('portal_center_id', $id)->first()->main_center_code;

        return view('client.dataentery.centers.edit', compact('core_center_id', 'staff', 'center', 'branches', 'centerleadername', 'centerleaderid', 'divisions', 'districts', 'villages', 'townships', 'quarters'));
    }

    public function update(Request $request, Center $center, $id)
    {
        // dd($request);
        $this->permissionFilter("centers");
        // dd($request);
        $validator = $request->validate([
            'center_uniquekey' => ['required', 'string', 'max:255'],
            'main_center_id' => ['required'],
            // 'staff_client_id' => ['string','max:255'],
            'branch_id' => ['required', 'string', 'max:255'],
            'staff_id' => ['required', 'string', 'max:255'],
            'type_status' => ['string', 'max:255'],
            'division_id' => ['required', 'string', 'max:255'],
            'district_id' => ['required', 'string', 'max:255'],
            'township_id' => ['required', 'string', 'max:255'],
            'village_id' => ['required', 'string', 'max:255'],
            'phoneno' => ['required', 'string', 'max:255'],
        ]);
        if ($validator) {
            // $center->update($request->all());
            $center = Center::where('center_uniquekey', $id)->first();
            //         dd($center);
            $center->center_uniquekey = $request->center_uniquekey;
            $center->staff_client_id = $request->staff_client_id;
            $center->branch_id = $request->branch_id;
            $center->staff_id = $request->staff_id;
            $center->type_status = $request->type_status;
            $center->division_id = $request->division_id;
            $center->district_id = $request->district_id;
            $center->township_id = $request->township_id;
            $center->village_id = $request->village_id;
            $center->phone_number = $request->phoneno;

            $center->update();

            //update to core system
            $main_staff_id = DB::table('tbl_main_join_staff')->where('portal_staff_id', $request->staff_id)->first()->main_staff_id;
            $main_center_id = DB::table('tbl_main_join_center')->where('portal_center_id', $request->center_uniquekey)->first();
            DB::connection('main')->table('center_leaders')->where('code', $main_center_id->main_center_code)->update([
                'code' => $request->main_center_id,
                'title' => $request->main_center_id,
                'phone' => $request->phoneno,
                'province_id' => $request->division_id,
                'district_id' => $request->district_id,
                'commune_id' => $request->township_id,
                'village_id' => $request->village_id,
                'user_id' => $main_staff_id
            ]);
            DB::connection('main')->table('user_centers')->where('center_id',$main_center_id->main_center_id)->update([
                'user_id'=>$main_staff_id
            ]);
            DB::table('tbl_main_join_center')->where('portal_center_id', $request->center_uniquekey)->update([
                'main_center_code' => $request->main_center_id
            ]);

            return redirect('centers/')->with("successMsg", 'Existing Center is Updated in your data');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    //View Page
    public function view($id)
    {

        $center = DB::table('tbl_center')
            // ->leftjoin('tbl_branches','tbl_center.branch_id','=','tbl_branches.id')
            ->where('tbl_center.center_uniquekey', '=', $id)
            ->first();
        $branchname = DB::table('tbl_branches')->where('id', $center->branch_id)
            ->select('tbl_branches.branch_name')->first()->branch_name;

        // dd($branchname);
        if ($center->type_status == "staff") {
            $typeofname = DB::table('tbl_staff')->where('staff_code', $center->staff_client_id)->select('name')->first();

            if ($typeofname) {
                $centerleadername = $typeofname->name;
            } else {
                $centerleadername = NULL;
            }
        } else if ($center->type_status == "client") {
            $typeofname = DB::table('tbl_client_basic_info')->where('client_uniquekey', $center->staff_client_id)->select('name')->first();
            if ($typeofname) {
                $centerleadername = $typeofname->name;
            } else {
                $centerleadername = NULL;
            }
        } else {
            $centerleadername = "-";
        }

        $staffName = DB::table('tbl_center')
            ->leftJoin('tbl_staff', 'tbl_staff.staff_code', '=', 'tbl_center.staff_id')
            ->where('tbl_center.center_uniquekey', '=', $id)
            ->select('name')
            ->get();
        $staffName = $staffName[0]->name;
        $division = DB::table('addresses')->where('code', $center->division_id)->first();
        $district = DB::table('addresses')->where('code', $center->district_id)->first();
        $township = DB::table('addresses')->where('code', $center->township_id)->first();
        $village = DB::table('addresses')->where('code', $center->village_id)->first();
        $quarter = DB::table('addresses')->where('code', $center->quarter_id)->first();
        // dd($typeofname);
        return view('client.dataentery.centers.view', compact('branchname', 'center', 'centerleadername', 'staffName', 'division', 'district', 'township', 'village', 'quarter'));
    }

    //Delete
    public function destroy(Center $center, $id)
    {
        $center = Center::where('center_uniquekey', $id)->first();
        $center->delete();

        $main_center_id = DB::table('tbl_main_join_center')->where('portal_center_id', $id)->first()->main_center_code;

        DB::connection('main')->table('center_leaders')->where('code', $main_center_id)->delete();

        DB::table('tbl_main_join_center')->where('portal_center_id', $id)->delete();

        return redirect('centers/')->with("successMsg", 'Existing Center is Deleted in your data');
    }

    //response  json
    public function getStaffClient(Request $request)
    {
        if ($request->type_status == "staff") {
            $centerleader = DB::table('tbl_staff')
                ->where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->select()
                ->pluck("name", "staff_code");
        } elseif ($request->type_status == "client") {
            $centerleader = DB::table('tbl_client_basic_info')
                ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                // ->join('tbl_center', 'tbl_center.staff_client_id', '=', 'tbl_clent_basic_info.client_uniquekey')
                ->where('tbl_client_join.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                // ->whereNull('tbl_center.staff_client_id')
                ->whereNotExists(function ($query) {
                    $query->select(DB::raw(1))
                        ->from('tbl_center')
                        ->whereRaw('tbl_client_basic_info.client_uniquekey = tbl_center.staff_client_id');
                })
                ->select()
                ->orderby('tbl_client_basic_info.client_uniquekey', 'asc')
                ->pluck("tbl_client_basic_info.name", "tbl_client_basic_info.client_uniquekey");
        }
        return response()->json($centerleader);
    }

    public function getUniqueID(Request $request)
    {
        $branchid = $request->branchid;
        $branch = DB::table('tbl_branches')->where('id', $branchid)->first()->id;
        $idnum = DB::table('tbl_center')->where('branch_id', $branch)->get()->count();
        $idnum = $idnum + 1;
        $idnum = str_pad($idnum, 4, 0, STR_PAD_LEFT);

        return response()->json($idnum);
    }

    public function liveCenterReverse($centerid, $main_center_id)
    {
        $branch_id = Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id;
        $centers = DB::connection('portal')
            ->table('tbl_center')
            ->leftJoin('tbl_branches', 'tbl_center.branch_id', '=', 'tbl_branches.id')
            ->where('tbl_center.branch_id', $branch_id)
            ->where('tbl_center.center_uniquekey', $centerid)
            ->select('tbl_center.*', 'tbl_branches.branch_code AS branchcode')
            ->first();
        // dd($centers);
        $lastID = DB::connection('main')
            ->table('center_leaders')
            ->where('branch_id', $branch_id)
            ->orderBy('title', 'desc')
            ->first()->code;

        $newID = ++$lastID;

        $user_id = '';

        if (
            DB::table('tbl_main_join_staff')
            ->where('portal_staff_id', $centers->staff_id)
            ->first()
        ) {
            $user_id = DB::table('tbl_main_join_staff')
                ->where('portal_staff_id', $centers->staff_id)
                ->first()->main_staff_id;
        }

        DB::connection('main')
            ->table('center_leaders')
            ->insert([
                //'id' => $centers[$i]->id,
                'branch_id' => $centers->branch_id,
                'code' => $main_center_id,
                'title' => $main_center_id,
                //'phone' => $centers[$i]->phone_primary,
                //'location'
                //'description'
                'created_at' => $centers->created_at,
                'updated_at' => $centers->updated_at,
                'address' => $centers->address,
                'province_id' => $centers->division_id,
                'district_id' => $centers->district_id,
                'commune_id' => $centers->township_id,
                'village_id' => $centers->village_id,
                'phone' => $centers->phone_number,
                //'seq'
                //'created_by'
                //'updated_by'
                // 'br_code' => $centers->branchcode,
                'user_id' => $user_id,
            ]);

        DB::table('tbl_main_join_center')->insert([
            'main_center_id' => DB::connection('main')
                ->table('center_leaders')
                ->where('code', $main_center_id) //newID
                ->first()->id,
            'portal_center_id' => $centers->center_uniquekey,
            'main_center_code' => $main_center_id, //newID
            //'main_center_leader_id' => $user_id, //client id(center leader)
        ]);

        DB::connection('main')
            ->table('user_centers')
            ->insert([
                'user_id' => DB::table('tbl_main_join_staff')
                    ->where('portal_staff_id', $centers->staff_id)
                    ->first()->main_staff_id,
                'center_id' => DB::connection('main')
                    ->table('center_leaders')
                    ->where('code', $main_center_id) //newID
                    ->first()->id,

            ]);
    }
    public function mainDuplicateCheck($main_center_id_new)
    {
        $check = DB::connection('main')->table('center_leaders')->where('code', $main_center_id_new)->first();
        if ($check) {
            return "true";
        } else {
            return "false";
        }
    }
}
