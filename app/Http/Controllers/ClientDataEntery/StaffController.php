<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Branch;
use App\Models\ClientDataEnteries\Center;
use App\Models\ClientDataEnteries\Staff;
use App\Models\ClientDataEnteries\Permission;
use App\Models\ClientDataEnteries\Role;
use App\Models\ClientDataEnteries\Model_Has_Role;
use App\Models\ClientDataEnteries\Model_Has_Permission;
use App\Models\ClientDataEnteries\Role_Has_Permission;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class StaffController extends Controller
{
    //Index Page
    public function index(Request $request)
    {
        $this->permissionFilter('list-users');

        if ($request->search_value) {
            $staffs = DB::table('tbl_staff')
                ->leftJoin('roles', 'roles.id', '=', 'tbl_staff.role_id')
                ->leftJoin('tbl_main_join_staff', 'tbl_main_join_staff.portal_staff_id', '=', 'tbl_staff.staff_code')
                ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
                ->where('tbl_staff.staff_code', 'LIKE', $request->search_value . '%')
                ->orWhere('tbl_main_join_staff.main_staff_id', 'LIKE', $request->search_value . '%')
                ->orWhere('tbl_staff.name', 'LIKE', $request->search_value . '%')
                ->orWhere('tbl_staff.email', 'LIKE', $request->search_value . '%')
                ->orWhere('tbl_staff.phone', 'LIKE', $request->search_value . '%')
                ->orWhere('roles.name', 'LIKE', $request->search_value . '%')
                ->orWhere('tbl_branches.branch_name', 'LIKE', $request->search_value . '%')
                ->select('tbl_staff.*', 'roles.name as role')->paginate(10);
        } else {
            $staffs = DB::table('tbl_staff')->leftJoin('roles', 'roles.id', '=', 'tbl_staff.role_id')->select('tbl_staff.*', 'roles.name as role')->paginate(10);
        }
        // dd($staffs);

        return view('client.dataentery.staffs.index', compact('staffs'));
    }

    //Create Page
    public function create()
    {
        $this->permissionFilter("role-permission");
        $branches = Branch::select('branch_code', 'branch_name', 'id')->get();
        $centers = Center::select('center_uniquekey', 'branch_id', 'id')->get();
        $permissions = Permission::get();
        $roles = Role::get();
        return view('client.dataentery.staffs.create', compact('branches', 'centers', 'permissions', 'roles'));
    }

    public function store(Request $request)
    {
        // dd($request);
        $validator = $request->validate([
            'staff_code' => ['required', 'string', 'max:255', 'unique:tbl_staff,staff_code'],
            'finger_biometric' => ['image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'father_name' => ['required', 'string', 'max:255'],
            'nrc_1' => ['required', 'string', 'max:255'],
            'nrc_2' => ['required', 'string', 'max:255'],
            'nrc_3' => ['required', 'string', 'max:255'],
            'nrc_4' => ['required', 'string', 'max:255'],
            // 'full_address' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255'],
            // 'user_acc' => ['required', 'string', 'max:255'],
            'user_password' => ['required', 'confirmed', 'string', 'max:255'],
            'photo' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            // 'branch_id' => ['required', 'string', 'max:255'],
            'center_leader_id' => ['string', 'max:255', 'nullable'],
            // 'user_role' => ['required'],
            'user_token' => ['string', 'max:255']
        ]);

        if ($validator) {

            $photo_name = time() . '.' . $request->photo->getClientOriginalName();

            $destinationPath = 'public/clientphotos/';
            $request->photo->storeAs($destinationPath, $photo_name);

            if ($request->finger_biometric) {
                $finger_biometric_name = time() . '.' . $request->finger_biometric->getClientOriginalName();
                $request->finger_biometric->storeAs($destinationPath, $finger_biometric_name);
            } else {
                $finger_biometric_name = NULL;
            }

            $nrc = $request->nrc_1 . '/' . $request->nrc_2 . $request->nrc_3 . $request->nrc_4;

            // Staff::create($request->all());
            if ($request->branch_id && count($request->branch_id) == 1) {
                $branch_id = $request->branch_id;
            } else {
                $branch_id = 1;
            }

            $staff = new Staff;
            $staff->staff_code = $request->staff_code;
            $staff->finger_biometric = $finger_biometric_name;
            $staff->name = $request->name;
            $staff->phone = $request->phone;
            $staff->father_name = $request->father_name;
            $staff->nrc = $nrc;
            $staff->full_address = $request->full_address;
            $staff->email = $request->email;
            $staff->user_acc = $request->name;
            $staff->user_password = Hash::make($request->user_password);
            $staff->photo = $photo_name;
            $staff->branch_id = $branch_id;
            $staff->center_leader_id = $request->center_leader_id;
            $staff->role_id = $request->role;
            // $staff->user_token = $request->user_token;
            $staff->save();

            if ($request->branch_id) {
                foreach ($request->branch_id as $bid) {
                    DB::table('user_branches')->insert([
                        'user_id' => $staff->staff_code,
                        'branch_id' => $bid,
                    ]);
                }
            }

            if ($staff) {
                $usercreate = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => Hash::make($request->user_password),
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }

            if ($request->center_leader_id) {
                $staff_client_id = Staff::where("staff_code", "=", $request->staff_code)->first()->id;

                $center = new Center;
                $center->center_uniquekey = $request->center_leader_id;
                $center->staff_client_id = $staff_client_id;
                $center->branch_id = $request->branch_id;
                $center->type_status = "staff";
                $center->staff_id = $staff->id;
                $center->save();
            }


            if($request->role) {
                DB::table('model_has_roles')->insert([
                    'role_id' => $request->role,
                    'model_type' => 'App\Models\BackpackUser',
                    'model_id' => $request->staff_code
                ]);

            }


            if ($request->permissions) {
                foreach ($request->permissions as $permission) {
                    DB::table('model_has_permissions')->insert([
                        'permission_id' => $permission,
                        'model_type' => 'App\Models\BackpackUser',
                        'model_id' => $request->staff_code
                    ]);
                }
            }


            $this->liveReverseStaffs($request);


            return redirect('staffs/')->with("successMsg", 'New Staff is Added in your data');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    public function liveReverseStaffs($request)
    {
        // $sessionBranch = DB::table('tbl_staff')->where('staff_code', request()->staff_code)->first()->branch_id;
        $last_portal_staff = DB::table('tbl_staff')->where('email', '=', $request->email)->first();

        $staff_check = DB::table('tbl_main_join_staff')
            ->where('portal_staff_id', $last_portal_staff->staff_code)
            ->first();
        if ($staff_check) {
            //do something
        } else {
            // if($request->branch_id) {
            //     $branchCode = DB::connection('main')
            //         ->table('branches')
            //         ->where('id', $request->branch_id[0])
            //         ->first()->code;

            // } else {
            //     $branchCode = 'HO';
            // }

            // $last_user_code = DB::connection('main')
            //     ->table('user_branches')
            //     ->leftJoin('users', 'users.id', '=', 'user_branches.user_id')
            //     ->orderBy('users.id', 'desc')
            //     ->where('user_branches.branch_id', $last_portal_staff->branch_id)
            //     ->select('users.user_code')
            //     ->get()->first();
            // if ($last_user_code) {
            //     $new_user_code = $last_user_code->user_code;
            //     $user_code = ++$new_user_code;
            // } else {
            //     $user_code = 'MIS-' . $branchCode . '-0001';
            // }
            DB::connection('main')
                ->table('users')
                ->insert([
                    'name' => $request->name,
                    'phone' => $request->phone,
                    'email' => $request->email,
                    'photo' => $request->photo,
                    'center_leader_id' => null, //$staffs[$i]->center_leader_id,
                    'email_verified_at' => $request->email_verified_at,
                    'password' => Hash::make($request->user_password),
                    'user_code' => $request->user_code,
                    'created_at' => now(),
                    'updated_at' => now()
                    // 'br_code' => $last_portal_staff->branch_code,
                ]);

            if($request->role) {
                DB::connection('main')
                ->table('model_has_roles')
                ->insert([
                    'role_id' => $request->role,
                    'model_type' => 'App\Models\BackpackUser',
                    'model_id' => DB::connection('main')
                        ->table('users')
                        ->orderBy('id', 'desc')
                        ->first()->id,
                    'created_by' => 0,
                    'updated_by' => 0,
                ]);
            }

            $mainStaffid = DB::connection('main')
                ->table('users')
                ->orderBy('id', 'desc')
                ->first();
            
            if($request->branch_id) {
                foreach($request->branch_id as $bid) {
                    DB::connection('main')
                    ->table('user_branches')
                    ->insert([
                        'user_id' => $mainStaffid->id,
                        'branch_id' => $bid,
                    ]);
                }
            }

            DB::table('tbl_main_join_staff')->insert([
                'main_staff_id' => $mainStaffid->id,
                'portal_staff_id' => $last_portal_staff->staff_code,
                'main_staff_code' => $mainStaffid->user_code,
                'main_staff_email' => $mainStaffid->email
            ]);

            if ($request->permissions) {
                foreach ($request->permissions as $permission) {
                    DB::connection('main')->table('model_has_permissions')->insert([
                        'permission_id' => $permission,
                        'model_type' => 'App\Models\BackpackUser',
                        'model_id' => $mainStaffid->id
                    ]);
                }
            }

            
        }
    }

    //Edit Page
    public function edit(Staff $staff, $id)
    {
        $this->permissionFilter("role-permission");
        $staff = Staff::where('staff_code', $id)->first();

        $permissions = Permission::get();
        $roles = Role::get();
        $user_branches = DB::table('user_branches')->where('user_id', $id)->select('branch_id')->pluck('branch_id')->toArray();
        $extra_permissions = DB::table('model_has_permissions')->where('model_id', $id)->select('permission_id')->pluck('permission_id')->toArray();
        //    dd($user_branches);
        function explodeNRC($delimiters, $string)
        {
            return explode(chr(1), str_replace($delimiters, chr(1), $string));
        }
        $list = $staff->nrc;

        $exploded_nrc = explodeNRC(array('/', '(', ')'), $list);


        // dd($exploded);
        $branches = Branch::select('branch_code', 'branch_name', 'id')->get();
        $centers = Center::select('center_uniquekey', 'branch_id', 'id')->get();
        return view('client.dataentery.staffs.edit', compact('staff', 'branches', 'centers', 'exploded_nrc', 'roles', 'permissions', 'user_branches', 'extra_permissions'));
    }

    public function update(Request $request, $id)
    {
        // dd($request);
        $this->permissionFilter("role-permission");
        $validator = $request->validate([
            'staff_code' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['nullable', 'string', 'max:255'],
            'father_name' => ['nullable', 'string', 'max:255'],
            'nrc_1' => ['string', 'max:255'],
            'nrc_2' => ['string', 'max:255'],
            'nrc_3' => ['string', 'max:255'],
            'nrc_4' => ['string', 'max:255'],
            'full_address' => ['nullable', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255'],
            // 'user_acc' => ['required', 'string', 'max:255'],
            // 'user_password' => ['required','string','max:255'],
            // 'branch_id' => ['required', 'string', 'max:255'],
            'center_leader_id' => ['nullable', 'string', 'max:255'],
            // 'user_role' => ['required'],
            // 'user_token' => ['required','string','max:255']
        ]);

        if ($validator) {

            $staff = Staff::where('staff_code', $id)->first();
            $mainStaffID = DB::table('tbl_main_join_staff')->where('portal_staff_id', $id)->first()->main_staff_id;
            $destinationPath = 'public/clientphotos/';


            if ($request->hasFile('finger_biometric')) {
                $request->validate([
                    'finger_biometric' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $finger_biometric_name = time() . '.' . $request->finger_biometric->getClientOriginalName();
                $request->finger_biometric->storeAs($destinationPath, $finger_biometric_name);
            } else {
                $finger_biometric_name = $staff->finger_biometric;
            }

            if ($request->hasFile('photo')) {
                $request->validate([
                    'photo' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $photo_name = time() . '.' . $request->photo->getClientOriginalName();
                $request->photo->storeAs($destinationPath, $photo_name);
            } else {
                $photo_name = $staff->photo;
            }

            if ($request->branch_id) {
                DB::table('user_branches')->where('user_id', $request->staff_code)->delete();
                foreach ($request->branch_id as $bid) {
                    DB::table('user_branches')->insert([
                        'user_id' => $request->staff_code,
                        'branch_id' => $bid,
                    ]);
                }

                //core 
                DB::connection('main')->table('user_branches')->where('user_id', $mainStaffID)->delete();
                foreach ($request->branch_id as $bid) {
                    DB::connection('main')->table('user_branches')->insert([
                        'user_id' => $mainStaffID,
                        'branch_id' => $bid,
                    ]);
                }
            }

            $staff->staff_code = $request->staff_code;
            $staff->finger_biometric = $finger_biometric_name;
            $staff->name = $request->name;
            $staff->phone = $request->phone;
            $staff->father_name = $request->father_name;
            $staff->nrc = $request->nrc;
            $staff->full_address = $request->full_address;
            $staff->email = $request->email;
            $staff->user_acc = $request->user_acc;
            if ($request->user_password) {
                $staff->user_password = Hash::make($request->user_password);
            }
            $staff->photo = $photo_name;
            $staff->branch_id = $request->branch_id[0];
            $staff->center_leader_id = $request->center_leader_id;
            $staff->role_id = $request->role;
            $staff->user_token = $request->user_token;
            $staff->update();

            //core
            DB::connection('main')->table('users')->where('id', $mainStaffID)->update([
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'updated_at' => now(),
            ]);

            if ($request->user_password) {
                DB::connection('main')->table('users')->where('id', $mainStaffID)->update([
                    'password' => Hash::make($request->user_password),
                ]);
            }

            
            if ($staff) {
                $usercreate = User::where('email', $request->email)->update([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => $staff->user_password,
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }

            DB::table('model_has_roles')->where('model_id', $request->staff_code)->delete();
            DB::connection('main')->table('model_has_roles')->where('model_id', $mainStaffID)->delete();
            
            if($request->role) {

                DB::table('model_has_roles')->insert([
                    'role_id' => $request->role,
                    'model_type' => 'App\Models\BackpackUser',
                    'model_id' => $request->staff_code
                ]);

                //core
                DB::connection('main')->table('model_has_roles')->insert([
                    'role_id' => $request->role,
                    'model_type' => 'App\Models\BackpackUser',
                    'model_id' => $mainStaffID
                ]);
            }


            if ($request->permissions) {
                DB::table('model_has_permissions')->where('model_id', $request->staff_code)->delete();
                foreach ($request->permissions as $permission) {
                    DB::table('model_has_permissions')->insert([
                        'permission_id' => $permission,
                        'model_type' => 'App\Models\BackpackUser',
                        'model_id' => $request->staff_code
                    ]);
                }

                //core
                DB::connection('main')->table('model_has_permissions')->where('model_id', $mainStaffID)->delete();
                foreach ($request->permissions as $permission) {
                    DB::connection('main')->table('model_has_permissions')->insert([
                        'permission_id' => $permission,
                        'model_type' => 'App\Models\BackpackUser',
                        'model_id' => $mainStaffID
                    ]);
                }
            }

            $new_staff_client_id = $staff->id;
            //dd($request);
            if ($request->center_leader_id) {
                $center = Center::where("center_uniquekey", "=", $request->center_leader_id)->first();

                if ($request->staff_client_id != $center->staff_client_id) {
                    $center->delete();

                    $center_new = new Center;
                    $center_new->center_uniquekey = $request->center_leader_id;
                    $center_new->staff_client_id = $new_staff_client_id;
                    $center_new->branch_id = $request->branch_id;
                    $center_new->type_status = "staff";
                    $center_new->save();
                }
            }

            // $staff->update($request->all());
            return redirect('staffs/')->with("successMsg", 'Existing Staff is Updated in your data');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    //View Page
    public function view(Staff $staff, $id)
    {
        $staff = DB::table('tbl_staff')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->leftJoin('roles', 'roles.id', '=', 'tbl_staff.role_id')
            ->where('tbl_staff.staff_code', '=', $id)
            ->select('tbl_staff.*', 'tbl_branches.*', 'tbl_staff.full_address as address', 'roles.name as role')
            ->first();
        // dd($staff);
        return view('client.dataentery.staffs.view', compact('staff'));
    }

    // Delete
    public function destroy(Staff $staff, $id)
    {
        $this->permissionFilter("role-permission");
        $staff = Staff::where('staff_code', $id)->first();
        $staff->delete();

        DB::table('user_branches')->where('user_id', $id)->delete();
        DB::table('model_has_roles')->where('model_id', $id)->delete();
        DB::table('model_has_permissions')->where('model_id', $id)->delete();

        $mainStaffID = DB::table('tbl_main_join_staff')->where('portal_staff_id', $id)->first()->main_staff_id;

        DB::connection('main')->table('users')->where('id', $mainStaffID)->delete();
        DB::connection('main')->table('user_branches')->where('user_id', $mainStaffID)->delete();
        DB::connection('main')->table('model_has_roles')->where('model_id', $mainStaffID)->delete();
        DB::connection('main')->table('model_has_permissions')->where('model_id', $mainStaffID)->delete();

        return redirect('staffs/')->with("successMsg", 'Existing Staff is Deleted in your data');
    }

    public function getNRC(Request $request)
    {
        $nrc_raw = Storage::disk('local')->get('json/myanmar-nrc.json');
        // $nrc = str_replace('\\', '\\\\', $nrc);
        $decoded_json = json_decode($nrc_raw, true);
        $nrcs = $decoded_json['data'];
        //  dd($nrcs);
        foreach ($nrcs as $nrc) {
            $nrc_code = $nrc['nrc_code'];
            $name_en = $nrc['name_en'];
            $name_mm = $nrc['name_mm'];
            if ($nrc_code == $request->nrc_1) {
                $test[] = array(
                    "name_en" => $name_en,
                    "nrc_code" => $nrc_code
                );
            }
        }
        return response()->json($test);
    }

    public function getUniqueID(Request $request)
    {
        $branchid = $request->branchid;
        $branch = DB::table('tbl_branches')->where('id', $branchid)->first()->id;
        $idnum = DB::table('tbl_staff')->where('branch_id', $branch)->get()->count();
        $idnum = $idnum + 1;
        $idnum = str_pad($idnum, 4, 0, STR_PAD_LEFT);

        return response()->json($idnum);
    }

    public function getPermissions(Request $request)
    {
        $permissions  = DB::table('role_has_permissions')->where('role_id', $request->role)->get();

        return response()->json(['status' => 200, 'message' => 'success', 'data' => $permissions]);
    }
}
