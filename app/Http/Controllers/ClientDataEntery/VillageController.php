<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Province;
use App\Models\ClientDataEnteries\Township;
use App\Models\ClientDataEnteries\Village;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VillageController extends Controller
{
    //Index Page
    public function index()
    {
        $results = DB::table('tbl_villages')->Paginate(10);
        // dd($results);
        $villages = array();
        foreach ($results as $result) {
            if($result->sub_status == "township"){
                $typeofname =DB::table('tbl_townships')->where('id',$result->township_province_id)->select('township_name')->first();
                $result_name = $typeofname->township_name;
            }else  if($result->sub_status == "province"){
                $typeofname =DB::table('tbl_provinces')->where('id',$result->township_province_id)->select('province_name')->first();
                $result_name = $typeofname->province_name;
              
            }else{
                $result_name = "name not found";
            }
            $villages[] = array(
                "id" => $result->id,
                "township_province_id" =>  $result->township_province_id,
                "twonship_province_name" => $result_name,
                "village_name" =>  $result->village_name,
                "village_name_mm" =>  $result->village_name_mm,
                "sub_status" =>  $result->sub_status,
                "del_status" =>  $result->del_status,
                "created_at" =>  $result->created_at,
                "updated_at" =>  $result->updated_at
            );
        }
        //dd($centers);
        return view('client.dataentery.villages.index', compact('villages','results'));
        
    }

    //Create Page
    public function create()
    {
        
        $townships = Township::select()->get();
        $provinces = Province::select()->get();

        return view('client.dataentery.villages.create', compact('townships','provinces'));
    }

    public function store(Request $request)
    {
        $validator=$request->validate([           
            'village_name' => ['required', 'string', 'max:255', 'unique:tbl_villages'],
            'village_name_mm' => ['required','string','max:255'],
            
        ]);
        
        if($validator){
            
        //data insert
            $tbl_village = new Village;
            $tbl_village->township_province_id = $request->township_province_id;
            $tbl_village->sub_status = $request->sub_status;
            $tbl_village->village_name =$request->village_name;
            $tbl_village->village_name_mm = $request->village_name_mm;
            $tbl_village->save();
    
        //return
        return redirect('villages/')->with("successMsg",'New Village Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
        
    }

    

    //Edit Page
    public function edit($id)
    {
        $village = Village::find($id);
        $townships = Township::all();
        $provinces = Province::all();


        return view('client.dataentery.villages.edit',compact('village','townships','provinces'));
    }

    public function update(Request $request, $id)
    {
        
        //data insert
        $tbl_village = Village::find($id);
        $tbl_village->township_province_id = $request->township_province_id;
        $tbl_village->sub_status = $request->sub_status;
        $tbl_village->village_name =$request->village_name;
        $tbl_village->village_name_mm = $request->village_name_mm;
        $tbl_village->save();

    //return
    return redirect('villages/')->with("successMsg",'Existing Village Update in your data');
    }

    //View Page
    public function view($id)
    {
        $village =DB::table('tbl_villages')
            ->where('tbl_villages.id','=',$id)
            ->first();
        if($village->sub_status == "township"){
            $typeofname =DB::table('tbl_townships')->where('id',$village->township_province_id)->select('township_name')->first();
            $townshipprovince = $typeofname->township_name;
        }else if($village->sub_status == "province"){
            $typeofname =DB::table('tbl_provinces')->where('id',$village->township_province_id)->select('province_name')->first();
            $townshipprovince = $typeofname->province_name;
        }else{
            $townshipprovince = "-";
        }

        return view('client.dataentery.villages.view',compact('village','townshipprovince'));
    }

    //Delete
    public function destroy($id)
    {
        $village = Village::find($id);
        $village->delete();

    return redirect('villages/')->with("successMsg",'Existing Village Delete in your data');
    }
}
