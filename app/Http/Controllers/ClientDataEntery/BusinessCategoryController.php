<?php

namespace App\Http\Controllers\ClientDataEntery;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\BusinessType;
use App\Models\ClientDataEnteries\BusinessCategory;

class BusinessCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results =DB::table('tbl_business_category')->Paginate(10);
        $business_categories =array();
        foreach($results as $result){
            $business_type = DB::table('tbl_business_type')->where('id',$result->business_type_id)->select('business_type_name')->first();
            $business_categories[] =array(
                'id' =>$result->id,
                'business_type_name' =>$business_type->business_type_name,
                'business_category_name' =>$result->business_category_name,
                'business_category_name_mm' =>$result->business_category_name_mm,
                "created_at" =>  $result->created_at,
                "updated_at" =>  $result->updated_at
            );
        }
        return view('client.dataentery.business_categories.index', compact('business_categories','results'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $business_types=BusinessType::select()->get();
        return view('client.dataentery.business_categories.create',compact('business_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator=$request->validate([
            'business_type_id' => 'required',
            'business_category_name' => ['required', 'string', 'max:255', 'unique:tbl_business_category'],
            'business_category_name_mm' => ['required','string','max:255']
        ]);

        if($validator){
           
        $business_category = new BusinessCategory();
        $business_category->business_type_id =$request->business_type_id;
        $business_category->business_category_name =$request->business_category_name;
        $business_category->business_category_name_mm = $request->business_category_name_mm;
        $business_category->save();

        //return
        return redirect('business_categories/')->with("successMsg",'New Business Category is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $business_types=BusinessType::all();
        $business_categories= BusinessCategory::find($id);
        return view('client.dataentery.business_categories.edit', compact('business_types','business_categories'));
    }

    
    public function update(Request $request, $id)
    {
        // dd($request);
        $validator=$request->validate([
            'business_type_id' => 'required',
            'business_category_name' => ['required', 'string', 'max:255',Rule::unique('tbl_business_category')->ignore($id)],
            'business_category_name_mm' => ['required','string','max:255',Rule::unique('tbl_business_category')->ignore($id)]
        ]);

        if($validator){
           
        $business_category = BusinessCategory::find($id);
        $business_category->business_type_id =$request->business_type_id;
        $business_category->business_category_name =$request->business_category_name;
        $business_category->business_category_name_mm = $request->business_category_name_mm;
        $business_category->save();

        //return
        return redirect('business_categories/')->with("successMsg",'Existing Business Category is Updated in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    //View Page
    public function view($id)
    {
        $business_category = DB::table('tbl_business_category')
                        ->leftJoin('tbl_business_type','tbl_business_type.id','=','tbl_business_category.business_type_id')
                        ->where('tbl_business_category.id','=',$id)
                        ->first();
        return view('client.dataentery.business_categories.view', compact('business_category'));
    }

    //Delete
    public function destroy($id)
    {
        $business_category = BusinessCategory::find($id);
        $business_category -> delete();

        return redirect('business_categories/')->with("successMsg",'Existing Business Category is Deleted in your data');

    }
}
