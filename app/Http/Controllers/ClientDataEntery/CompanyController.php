<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ClientDataEnteries\Company;

class CompanyController extends Controller
{
    public function index()
    {
        $companies = Company::Paginate(10);
        return view('client.dataentery.company.index',compact('companies'));
    }

    public function create()
    {
        return view('client.dataentery.company.create');
    }

    public function store(Request $request)
    {
        $validator = $request->validate([
            'company_name' => ['required', 'string', 'max:255']
        ]);

        if($validator){
            $company = new Company;
            $company->company_name = $request->company_name;
            $company->save();
            return redirect('companies/')->with("successMsg",'Company Name is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    public function edit($id)
    {
        $company = Company::find($id);
        return view('client.dataentery.company.edit', compact('company'));
    }

    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'company_name' => ['required', 'string', 'max:255']
        ]);
        if($validator){
            $company = Company::find($id);
            $company->company_name = $request->company_name;
            $company->save();
            return redirect('/companies')->with("successMsg",'Company Name is Updated in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
        
    }
    
    public function view($id)
    {
        $company = Company::find($id);
        return view('client.dataentery.company.view',compact('company'));
    }

    public function destroy($id)
    {
        $company = Company::find($id);
        $company->delete();
        return redirect('companies/')->with("successMsg",'Existing Company is Deleted in your data');
    }
}
