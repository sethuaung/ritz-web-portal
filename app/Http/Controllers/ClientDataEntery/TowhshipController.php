<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\District;
use App\Models\ClientDataEnteries\Township;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TowhshipController extends Controller
{
    //Index
    public function index()
    {
        //
        $results =DB::table('tbl_townships')->Paginate(10);
        $townships =array();
        foreach($results as $result){
            $district = DB::table('tbl_districts')->where('id',$result->district_id)->select('district_name')->first();
            $townships[] =array(
                'id' =>$result->id,
                'district_id' =>$district->district_name,
                'township_name' =>$result->township_name,
                'township_name_mm' =>$result->township_name_mm,
                "del_status" =>  $result->del_status,
                "created_at" =>  $result->created_at,
                "updated_at" =>  $result->updated_at
            );
        }

        return view('client.dataentery.townships.index',compact('townships','results'));
    }

   //Create Page
    public function create()
    {
        //
       
        $districts = District::all();

        return view('client.dataentery.townships.create', compact('districts'));
    }

    public function store(Request $request)
    {
        //
        //validation
        $validator=$request->validate([
            // 'district_id' => 'rquired',
            'township_name' => ['required', 'string', 'max:255', 'unique:tbl_townships'],
            'township_name_mm' => ['required','string','max:255']
        ]);
        
        if($validator){

            //data insert
        $tbl_township = new Township;
        $tbl_township->district_id = $request->district_id;
        $tbl_township->township_name =$request->township_name;
        $tbl_township->township_name_mm = $request->township_name_mm;
        $tbl_township->save();

        //return
        return redirect('townships/')->with("successMsg",'New Township Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

   //Edit Page
    public function edit($id)
    {
        //
        $township = Township::find($id);
        $districts = District::all();

        return view('client.dataentery.townships.edit',compact('township','districts'));
    }

    public function update(Request $request, $id)
    {
        
            //data insert
        $tbl_township = Township::find($id);
        $tbl_township->district_id = $request->district_id;
        $tbl_township->township_name =$request->township_name;
        $tbl_township->township_name_mm = $request->township_name_mm;
        $tbl_township->save();

        //return
        return redirect('townships/')->with("successMsg",'Existing Township Update in your data');
    }

    //View Page
    public function view($id)   
    {
        $township = DB::table('tbl_townships')
                    ->leftJoin('tbl_districts','tbl_districts.id','=','tbl_townships.district_id')
                    ->where('tbl_townships.id','=',$id)
                    ->first();

        return view('client.dataentery.townships.view',compact('township'));
    }


    //Delete
    public function destroy($id)
    {
        //
        $township = Township::find($id);
        $township->delete();

        return redirect('townships/')->with("successMsg",'Existing Township Delete in your data');

    }
}
