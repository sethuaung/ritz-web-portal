<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Quarter;
use App\Models\ClientDataEnteries\Village;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuarterController extends Controller
{
    //Index Page
    public function index()
    {
        $results =DB::table('tbl_quarters')->Paginate(10);
    	// dd($results);
        $quarters =array();
        foreach($results as $result){
            $village = DB::table('tbl_villages')->where('id',$result->town_id)->select('village_name')->first();
        	// dd($township);
            $quarters[] =array(
                'id' =>$result->id,
                'town_id' =>$village->village_name,
                'quarter_name' =>$result->quarter_name,
                'quarter_name_mm' =>$result->quarter_name_mm,
                "del_status" =>  $result->del_status,
                "created_at" =>  $result->created_at,
                "updated_at" =>  $result->updated_at
            );
        }

        return view('client.dataentery.quarters.index',compact('quarters','results'));
    }

    //Create Page
    public function create()
    {
        
        $townships = Township::select('township_name','id')->get();

        return view('client.dataentery.quarters.create', compact('townships'));
    }

    public function store(Request $request)
    {
         //validation
       $validator=$request->validate([
        'township_id' => ['required'],
        'quarter_name' => ['required', 'string', 'max:255', 'unique:tbl_quarters'],
        'quarter_name_mm' => ['required','string','max:255']
    ]);
    
    if($validator){

        //data insert
    $tbl_quarter = new Quarter;
    $tbl_quarter->township_id = $request->township_id;
    $tbl_quarter->quarter_name =$request->quarter_name;
    $tbl_quarter->quarter_name_mm = $request->quarter_name_mm;
    $tbl_quarter->save();

    //return
    return redirect('quarters/')->with("successMsg",'New Quarter Added in your data');
    }else{
        return redirect()->back()->withErrors($validator);
    }
    }

   

    // Edit Page
    public function edit($id)
    {
        $quarter = Quarter::find($id);
        $village = Village::all();

        return view('client.dataentery.quarters.edit',compact('quarter','village'));
    }

    public function update(Request $request, $id)
    {
        // dd($township_id);
        //data insert
        $tbl_quarter = Quarter::find($id);
        $tbl_quarter->township_id = $request->township_id;        
        $tbl_quarter->quarter_name =$request->quarter_name;
        $tbl_quarter->quarter_name_mm = $request->quarter_name_mm;
        $tbl_quarter->save();

    //return
    return redirect('quarters/')->with("successMsg",'Existing Quarter Update in your data');
    }

     // View Page
     public function view($id)
     {
         $quarter = DB::table('tbl_quarters')
                    ->leftJoin('tbl_villages','tbl_villages.id','=','tbl_quarters.town_id')
                    ->where('tbl_quarters.id','=',$id)
                    ->first();
 
         return view('client.dataentery.quarters.view',compact('quarter'));
     }


    //Delete
    public function destroy($id)
    {
        $quarter = Quarter::find($id);
        $quarter->delete();

    return redirect('quarters/')->with("successMsg",'Existing Quarter Delete in your data');
    }
}
