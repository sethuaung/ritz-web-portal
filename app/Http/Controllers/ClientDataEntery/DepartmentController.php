<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Department;
use App\Models\ClientDataEnteries\Industry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepartmentController extends Controller
{
    //Index Page
    public function index()
    {
        $results =DB::table('tbl_job_department')->Paginate(10);
        $departments =array();
        foreach($results as $result){
            $industry = DB::table('tbl_industry_type')->where('id',$result->industry_id)->select('industry_name')->first();
            $departments[] =array(
                'id' =>$result->id,
                'industry_id' =>$industry->industry_name,
                'department_name' =>$result->department_name,
                'department_name_mm' =>$result->department_name_mm,
                "del_status" =>  $result->del_status,
                "created_at" =>  $result->created_at,
                "updated_at" =>  $result->updated_at
            );
        }
        return view('client.dataentery.departments.index', compact('departments','results'));
    }

    //Create Page
    public function create()
    {
        $industries=Industry::select()->get();
        return view('client.dataentery.departments.create',compact('industries'));
    }

    public function store(Request $request)
    {
        $validator=$request->validate([
            'industry_id' => 'required',
            'department_name' => ['required', 'string', 'max:255', 'unique:tbl_job_department'],
            'department_name_mm' => ['required','string','max:255']
        ]);

        if($validator){
           
        $department = new Department;
        $department->industry_id =$request->industry_id;
        $department->department_name =$request->department_name;
        $department->department_name_mm = $request->department_name_mm;
        $department->save();

        //return
        return redirect('departments/')->with("successMsg",'New Department is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    //Edit Page
    public function edit($id)
    {
        // dd($id);
        $industries=Industry::all();
        $department= Department::find($id);
        return view('client.dataentery.departments.edit', compact('department','industries'));
    }

    
    public function update(Request $request, $id)
    {
        $validator=$request->validate([
            'industry_id' => 'required',
            'department_name' => ['required', 'string', 'max:255'],
            'department_name_mm' => ['required','string','max:255']
        ]);

        if($validator){
           
        $department = Department::find($id);
        $department->industry_id =$request->industry_id;
        $department->department_name =$request->department_name;
        $department->department_name_mm = $request->department_name_mm;
        $department->save();

        //return
        return redirect('departments/')->with("successMsg",'Existing Department is Updated in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    //View Page
    public function view($id)
    {
        $department = DB::table('tbl_job_department')
                        ->leftJoin('tbl_industry_type','tbl_industry_type.id','=','tbl_job_department.industry_id')
                        ->where('tbl_job_department.id','=',$id)
                        ->first();
        return view('client.dataentery.departments.view', compact('department'));
    }

    //Delete
    public function destroy($id)
    {
        $department = Department::find($id);
        $department -> delete();

        return redirect('departments/')->with("successMsg",'Existing Department is Deleted in your data');

    }
}
