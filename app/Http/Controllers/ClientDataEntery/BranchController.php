<?php

namespace App\Http\Controllers\ClientDataEntery;

use App\Models\Loans\Loan;
use App\Models\Loans\LoanJoin;
use Illuminate\Http\Request;
use App\Models\Loans\LoanDeposit;
use App\Models\Loans\LoanSchedule;
use App\Models\Loans\RepaymentDue;
use App\Models\Loans\RepaymentPre;
use App\Models\Loans\LoanCycle;
use App\Models\Loans\LoanDocument;
use Illuminate\Support\Facades\DB;
use App\Models\Loans\RepaymentLate;
use App\Http\Controllers\Controller;
use App\Models\Loans\LoanDisbursement;
use App\Models\ClientDataEnteries\City;
use App\Models\ClientDataEnteries\Branch;
use App\Models\ClientDataEnteries\Quarter;
use App\Models\ClientDataEnteries\Village;
use App\Models\ClientDataEnteries\District;
use App\Models\ClientDataEnteries\Division;
use App\Models\ClientDataEnteries\Province;
use App\Models\ClientDataEnteries\Township;
use App\Models\Loandataenteries\SavingWithdrawl;



class BranchController extends Controller
{
    //Index Page
    public function index()
    {
        $branches = Branch::Paginate(10);

        return view('client.dataentery.branches.index', compact('branches'));
    }

    //Create Page
    public function create()
    {
      	$this->permissionFilter("branches");

          $divisions = DB::table('addresses')->where('type','=','state')->get();
          $districts = District::select('district_name','id')->get();
        $cities = City::select('city_name','id')->get();
        $quarters = Quarter::select('quarter_name','id')->get();
        $townships = Township::select('township_name','id')->get();
        // $provinces = Province::select('province_name','id')->get();
        $villages = Village::select('village_name','id')->get();
        return view('client.dataentery.branches.create',compact('divisions','quarters','districts','cities','townships','villages'));
    }

    public function store(Request $request)
    {
      	$this->permissionFilter("branches");
        $validator=$request->validate([
            'branch_name' => ['required','string','max:255', 'unique:tbl_branches'],
            'branch_code' => ['required', 'string', 'max:255', 'unique:tbl_branches'],
            'phone_primary' => ['required','string','max:255'],
            'phone_secondary' => ['string','max:255','nullable'],
            'phone_tertiary' => ['string','max:255','nullable'],
            'description' => ['string','max:255','nullable'],
            'village_id' => ['string','max:255'],
            // 'province_id' => ['required','string','max:255'],
            'quarter_id' => ['nullable', 'string','max:255'],
            'township_id' => ['required','string','max:255'],
            'district_id' => ['required','string','max:255'],
            'division_id' => ['required','string','max:255'],
            'city_id' => ['string','max:255'],
            'location' => ['string','max:255','nullable'],
            'address_primary' => ['required','string','max:255'],
            'branch_photo' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048']
        ]);

        $branch_photo_name = time().'.'.$request->branch_photo->getClientOriginalName();
        $destinationPath = 'public/clientphotos/';
        $request->branch_photo->storeAs($destinationPath, $branch_photo_name);

        if($validator){
        //  $request->request->add(['branch_photo' => $branch_photo_name]);
        //  Branch::create($request->all());
        // why no working ? need to ask KO ZLN

        $branch = new Branch;
        $branch->branch_name = $request->branch_name;
        $branch->branch_code = $request->branch_code;
        $branch->phone_primary = $request->phone_primary;
        $branch->phone_secondary = $request->phone_secondary;
        $branch->phone_tertiary = $request->phone_tertiary;
        $branch->description = $request->description;
        $branch->village_id = $request->village_id;
        // $branch->province_id = $request->province_id;
        $branch->quarter_id = $request->quarter_id;
        $branch->township_id = $request->township_id;
        $branch->district_id = $request->district_id;
        $branch->division_id = $request->division_id;
        $branch->city_id = $request->city_id;
        $branch->location = $request->location;
        $branch->full_address = $request->full_address;
        $branch->branch_photo = $branch_photo_name;
        $branch->save();
        $b_code=strtolower($request->branch_code);


        $loan = new Loan;
        $columns = $loan->getTableColumns();
        $table_name=$b_code.'_loans';
        $this->createMultipleTables($columns,$table_name);

        $loanDoc = new LoanJoin;
        $columns = $loanDoc->getTableColumns();
        $table_name=$b_code.'_loan_charges_compulsory_join';
        $this->createMultipleTables($columns,$table_name);

        $loan_cycle = new LoanCycle;
        $columns = $loan_cycle->getTableColumns();
        $table_name=$b_code.'_loan_cycle';
        $this->createMultipleTables($columns,$table_name);

        $loan_docu = new LoanDocument;
        $columns = $loan_docu->getTableColumns();
        $table_name=$b_code.'_loan_document';
        $this->createMultipleTables($columns,$table_name);

        $loan_schedule = new LoanSchedule;
        $columns = $loan_schedule->getTableColumns();
        $table_name=$b_code.'_loans_schedule';
        $this->createMultipleTables($columns,$table_name);

        $loan_deposit = new LoanDeposit;
        $columns = $loan_deposit->getTableColumns();
        $table_name=$b_code.'_deposit';
        $this->createMultipleTables($columns,$table_name);

        $loan_disbursement = new LoanDisbursement();
        $columns = $loan_disbursement->getTableColumns();
        $table_name=$b_code.'_disbursement';
        $this->createMultipleTables($columns,$table_name);

        $due = new RepaymentDue();
        $columns = $due->getTableColumns();
        $table_name=$b_code.'_repayment_due';
        $this->createMultipleTables($columns,$table_name);

        $late = new RepaymentLate();
        $columns = $late->getTableColumns();
        $table_name=$b_code.'_repayment_late';
        $this->createMultipleTables($columns,$table_name);

        $pre = new RepaymentPre();
        $columns = $pre->getTableColumns();
        $table_name=$b_code.'_repayment_pre';
        $this->createMultipleTables($columns,$table_name);

        $pre = new SavingWithdrawl();
        $columns = $pre->getTableColumns();
        $table_name=$b_code.'_saving_withdrawl';
        $this->createMultipleTables($columns,$table_name);

        return redirect('branches/')->with("successMsg",'New Branch is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    //Edit Page
    public function edit(Branch $branch,$id)
    {
      	$this->permissionFilter("branches");

        $branch= Branch::find($id);
        // return view('branches.edit', compact('branch'));
        // $divisions = Division::select('division_name','id')->get();
        // $districts = District::select('district_name','id')->get();
        // $cities = City::select('city_name','id')->get();
        // $quarters = Quarter::select('quarter_name','id')->get();
        // $townships = Township::select('township_name','id')->get();
        // $provinces = Province::select('province_name','id')->get();
        // $villages = Village::select('village_name','id')->get();
        $divisions = DB::table('addresses')->where('type', '=','state')->get();
        $districts = DB::table('addresses')->where('parent_code',$branch->division_id)->get();
        $townships = DB::table('addresses')->where('parent_code',$branch->district_id)->get();
        $villages = DB::table('addresses')->where('parent_code',$branch->township_id)->get();
        $wards = DB::table('addresses')->where('parent_code',$branch->village_id)->get();

        return view('client.dataentery.branches.edit',compact('branch','divisions','wards','districts','townships','villages'));

    }

    public function update(Request $request, $id)
    {
      	$this->permissionFilter("branches");
            $branch = Branch::find($id);
            $destinationPath = 'public/clientphotos/';


            if($request->hasFile('branch_photo')){
                $request->validate([
                  'branch_photo' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                  ]);
                $branch_photo_name = time().'.'.$request->branch_photo->getClientOriginalName();
                $request->branch_photo->storeAs($destinationPath, $branch_photo_name);
                 }else {
                     $branch_photo_name = $branch->branch_photo;
                 }

            $branch->branch_name =$request->branch_name;
            $branch->branch_code =$request->branch_code;

            $branch->title =$request->title;
            $branch->phone_primary =$request->phone_primary;
            $branch->phone_secondary =$request->phone_secondary;
            $branch->phone_tertiary =$request->phone_tertiary;
            $branch->description =$request->description;
            $branch->village_id =$request->village_id;
            // $branch->province_id =$request->province_id;
            $branch->quarter_id =$request->ward_id;
            $branch->township_id =$request->township_id;
            $branch->district_id =$request->district_id;
            $branch->division_id =$request->division_id;
            $branch->city_id =$request->city_id;
            $branch->location =$request->location;
            $branch->full_address =$request->full_address;
            $branch->branch_photo = $branch_photo_name;
            $branch->save();
            // $branch->update($request->all());
            return redirect('branches/')->with("successMsg",'Existing Branch is Updated in your data');

    }

    //View Page
    public function view($id)
    {
        $branch = DB::table('tbl_branches')
                    // ->leftjoin('tbl_divisions','tbl_branches.division_id','=','tbl_divisions.id')
                    // ->leftjoin('tbl_districts','tbl_branches.district_id','=','tbl_districts.id')
                    // ->leftjoin('tbl_cities','tbl_branches.city_id','=','tbl_cities.id')
                    // ->leftjoin('tbl_townships','tbl_branches.township_id','=','tbl_townships.id')
                    // ->leftjoin('tbl_provinces','tbl_branches.province_id','=','tbl_provinces.id')
                    // ->leftjoin('tbl_quarters','tbl_branches.quarter_id','=','tbl_quarters.id')
                    // ->leftjoin('tbl_villages','tbl_branches.village_id','=','tbl_villages.id')
                    ->select('tbl_branches.*')
                    ->where('tbl_branches.id','=',$id)
                    ->first();
                    // dd($branch);
        $division=DB::table('addresses')->where('code',$branch->division_id)->first();
        $district=DB::table('addresses')->where('code',$branch->district_id)->first();
        $township=DB::table('addresses')->where('code',$branch->township_id)->first();
        $village=DB::table('addresses')->where('code',$branch->village_id)->first();
        $ward=DB::table('addresses')->where('code',$branch->quarter_id)->first();

        return view('client.dataentery.branches.view',compact('branch','division','district','township','village','ward'));
    }

    //Delete
    public function destroy(Branch $branch,$id)
    {
        $branch = Branch::find($id);
        $branch -> delete();

        return redirect('branches/')->with("successMsg",'Existing Branch is Deleted in your data');

    }

    public function createMultipleTables($columns,$table_name){
        $column_count=count($columns);
        $primary_column_name=$columns[0]->Field;
        $primary_data_type=$columns[0]->Type;

        $createTableSqlString = "CREATE TABLE $table_name (
            $primary_column_name $primary_data_type AUTO_INCREMENT PRIMARY KEY NOT NULL

        ) COLLATE='utf8_unicode_ci';";


        DB::statement($createTableSqlString);

        for ($i=1; $i < $column_count; $i++) {
            $column_name=$columns[$i]->Field;
            $data_type=$columns[$i]->Type;

            if($columns[$i]->Null == 'YES'){
                $null='NULL';
            }else{
                $null='NOT NULL';
            }

            if(isset($columns[$i]->Default)){
                $default='DEFAULT '.$columns[$i]->Default;
            }else{
                $default='DEFAULT NULL';
            }

            if($columns[$i]->Default=='none'){
                $insertColumns="ALTER TABLE `$table_name` ADD `$column_name` $data_type DEFAULT 'none' NOT NULL;";
            }elseif($columns[$i]->Default=='unpaid') {
                $insertColumns="ALTER TABLE `$table_name` ADD `$column_name` $data_type DEFAULT 'unpaid' NOT NULL;";
            }else{
                $insertColumns="ALTER TABLE `$table_name` ADD `$column_name` $data_type $default $null ;";
            }
            DB::statement($insertColumns);

        }
    }
}
