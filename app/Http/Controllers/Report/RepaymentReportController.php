<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class RepaymentReportController extends Controller
{
    public function getBranchCode(){
        $staff_code=Session::get('staff_code');
        //dd($staff_code);
        $B_code=DB::table('tbl_staff')
                ->leftJoin('tbl_branches','tbl_branches.id','=','tbl_staff.branch_id')
                ->select('tbl_branches.branch_code')
                ->where('tbl_staff.staff_code',$staff_code)
                ->first();
        $b_code=strtolower($B_code->branch_code);
        return $b_code;
    }
    public function view($id)
    {
        $bcode = $this->getBranchCode();
        
        $repayment = DB::table($bcode.'_loans_schedule')
                    ->select('loan_unique_id','month','capital','interest','final_amount','status','repayment_type')
                    ->where($bcode.'_loans_schedule.loan_unique_id','=',$id)
                    ->get();
        //dd($repayment);
        return view('repayment_reports.view', compact('repayment'));
    }
} 