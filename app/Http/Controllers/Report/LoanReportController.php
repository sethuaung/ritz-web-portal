<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class LoanReportController extends Controller
{
	public function getBranchCode(){
        $staff_code=Session::get('staff_code');
        // dd($staff_code);
        $B_code=DB::table('tbl_staff')
                ->leftJoin('tbl_branches','tbl_branches.id','=','tbl_staff.branch_id')
                ->select('tbl_branches.branch_code')
                ->where('tbl_staff.staff_code',$staff_code)
                ->first();
        $b_code=strtolower($B_code->branch_code);
        return $b_code;
    }

    public function index(Request $request){
      	$this->permissionFilter("loan-reports");
    	$bcode=$this->getBranchCode();

        if ($request->filled('search_loanslist')) {
            // Search method
            $searchvalue = $request->search_loanslist;
            $data_loanstitle = 'loan list';
            $data_loans = DB::table($bcode.'_loans')
                ->leftjoin('tbl_client_basic_info','tbl_client_basic_info.client_uniquekey','=',$bcode.'_loans.client_id')
                ->leftjoin('loan_type','loan_type.id','=',$bcode.'_loans.loan_type_id')
                ->leftJoin('tbl_branches','tbl_branches.id','=',$bcode.'_loans.branch_id')
                ->leftJoin('tbl_center','tbl_center.center_uniquekey','=',$bcode.'_loans.center_id')
                ->where($bcode.'_loans.loan_unique_id', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_client_basic_info.name', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_client_basic_info.nrc', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_center.center_uniquekey', 'LIKE', $searchvalue . '%')
                ->orWhere('loan_type.name', 'LIKE', $searchvalue . '%')
                ->orWhere($bcode.'_loans.loan_amount', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_branches.branch_name', 'LIKE', $searchvalue . '%')
                ->orWhere($bcode.'_loans.disbursement_status', 'LIKE', $searchvalue . '%')
                ->orWhere($bcode.'_loans.disbursement_date', 'LIKE', $searchvalue . '%')
                ->select($bcode.'_loans.*', 'tbl_client_basic_info.name AS client_name', 'loan_type.name AS loan_type_name','tbl_branches.branch_name','tbl_client_basic_info.nrc', 'tbl_client_basic_info.old_nrc', 'tbl_center.center_uniquekey')
                ->Paginate(10);
        } elseif($request->filled('disbursement_status')) {
            $searchvalue = $request->disbursement_status;
            $data_loanstitle = 'loan list';
            $data_loans = DB::table($bcode.'_loans')
                ->leftjoin('tbl_client_basic_info','tbl_client_basic_info.client_uniquekey','=',$bcode.'_loans.client_id')
                ->leftjoin('loan_type','loan_type.id','=',$bcode.'_loans.loan_type_id')
                ->leftJoin('tbl_branches','tbl_branches.id','=',$bcode.'_loans.branch_id')
                ->leftJoin('tbl_center','tbl_center.center_uniquekey','=',$bcode.'_loans.center_id')
                ->where($bcode.'_loans.disbursement_status', 'LIKE', $searchvalue . '%')
                ->select($bcode.'_loans.*', 'tbl_client_basic_info.name AS client_name', 'loan_type.name AS loan_type_name','tbl_branches.branch_name','tbl_client_basic_info.nrc', 'tbl_client_basic_info.old_nrc','tbl_center.center_uniquekey')
                ->Paginate(10);
        } elseif($request->filled('date')) {
            $searchvalue = $request->date;
            $data_loanstitle = 'loan list';
            if($searchvalue == 'date') {
                $data_loans = DB::table($bcode.'_loans')
                    ->leftjoin('tbl_client_basic_info','tbl_client_basic_info.client_uniquekey','=',$bcode.'_loans.client_id')
                    ->leftjoin('loan_type','loan_type.id','=',$bcode.'_loans.loan_type_id')
                    ->leftJoin('tbl_branches','tbl_branches.id','=',$bcode.'_loans.branch_id')
                    ->leftJoin('tbl_center','tbl_center.center_uniquekey','=',$bcode.'_loans.center_id')
                    ->whereDate($bcode.'_loans.created_at', 'LIKE', Carbon::now())
                    ->select($bcode.'_loans.*', 'tbl_client_basic_info.name AS client_name', 'loan_type.name AS loan_type_name','tbl_branches.branch_name','tbl_client_basic_info.nrc', 'tbl_client_basic_info.old_nrc','tbl_center.center_uniquekey')
                    ->Paginate(10);
            } else if($searchvalue == 'month') {
                $data_loans = DB::table($bcode.'_loans')
                    ->leftjoin('tbl_client_basic_info','tbl_client_basic_info.client_uniquekey','=',$bcode.'_loans.client_id')
                    ->leftjoin('loan_type','loan_type.id','=',$bcode.'_loans.loan_type_id')
                    ->leftJoin('tbl_branches','tbl_branches.id','=',$bcode.'_loans.branch_id')
                    ->leftJoin('tbl_center','tbl_center.center_uniquekey','=',$bcode.'_loans.center_id')
                    ->whereMonth($bcode.'_loans.created_at', 'LIKE', Carbon::now()->month)
                    ->select($bcode.'_loans.*', 'tbl_client_basic_info.name AS client_name', 'loan_type.name AS loan_type_name','tbl_branches.branch_name','tbl_client_basic_info.nrc', 'tbl_client_basic_info.old_nrc','tbl_center.center_uniquekey')
                    ->Paginate(10);
            } else if($searchvalue == 'year') {
                $data_loans = DB::table($bcode.'_loans')
                    ->leftjoin('tbl_client_basic_info','tbl_client_basic_info.client_uniquekey','=',$bcode.'_loans.client_id')
                    ->leftjoin('loan_type','loan_type.id','=',$bcode.'_loans.loan_type_id')
                    ->leftJoin('tbl_branches','tbl_branches.id','=',$bcode.'_loans.branch_id')
                    ->leftJoin('tbl_center','tbl_center.center_uniquekey','=',$bcode.'_loans.center_id')
                    ->whereYear($bcode.'_loans.created_at', 'LIKE', Carbon::now()->year)
                    ->select($bcode.'_loans.*', 'tbl_client_basic_info.name AS client_name', 'loan_type.name AS loan_type_name','tbl_branches.branch_name','tbl_client_basic_info.nrc', 'tbl_client_basic_info.old_nrc','tbl_center.center_uniquekey')
                    ->Paginate(10);
            } else {
                $data_loans = DB::table($bcode.'_loans')
                    ->leftjoin('tbl_client_basic_info','tbl_client_basic_info.client_uniquekey','=',$bcode.'_loans.client_id')
                    ->leftjoin('loan_type','loan_type.id','=',$bcode.'_loans.loan_type_id')
                    ->leftJoin('tbl_branches','tbl_branches.id','=',$bcode.'_loans.branch_id')
                    ->leftJoin('tbl_center','tbl_center.center_uniquekey','=',$bcode.'_loans.center_id')
                    ->whereBetween($bcode.'_loans.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                    ->select($bcode.'_loans.*', 'tbl_client_basic_info.name AS client_name', 'loan_type.name AS loan_type_name','tbl_branches.branch_name','tbl_client_basic_info.nrc', 'tbl_client_basic_info.old_nrc','tbl_center.center_uniquekey')
                    ->Paginate(10);
            }
        } elseif ($request->filled('loan_report_length')) {
            $searchvalue = $request->loan_report_length;
            $data_loans =DB::table($bcode.'_loans')
                ->leftjoin('tbl_client_basic_info','tbl_client_basic_info.client_uniquekey','=',$bcode.'_loans.client_id')
                ->leftjoin('loan_type','loan_type.id','=',$bcode.'_loans.loan_type_id')
                ->leftJoin('tbl_branches','tbl_branches.id','=',$bcode.'_loans.branch_id')
                ->leftJoin('tbl_center','tbl_center.center_uniquekey','=',$bcode.'_loans.center_id')
                ->select($bcode.'_loans.*','tbl_client_basic_info.name AS client_name','loan_type.name AS loan_type_name','tbl_branches.branch_name','tbl_client_basic_info.nrc', 'tbl_client_basic_info.old_nrc','tbl_center.center_uniquekey')
                ->Paginate($searchvalue);
        } else{
            $data_loans =DB::table($bcode.'_loans')
                ->leftjoin('tbl_client_basic_info','tbl_client_basic_info.client_uniquekey','=',$bcode.'_loans.client_id')
                ->leftjoin('loan_type','loan_type.id','=',$bcode.'_loans.loan_type_id')
                ->leftJoin('tbl_branches','tbl_branches.id','=',$bcode.'_loans.branch_id')
                ->leftJoin('tbl_center','tbl_center.center_uniquekey','=',$bcode.'_loans.center_id')
                ->select($bcode.'_loans.*','tbl_client_basic_info.name AS client_name','loan_type.name AS loan_type_name','tbl_branches.branch_name','tbl_client_basic_info.nrc', 'tbl_client_basic_info.old_nrc','tbl_center.center_uniquekey')
                ->Paginate(10);
        }
        $data_branch = Branch::orderby('id')->get();
        return view('loan_reports.index',compact('data_branch','data_loans'));
    }
}
