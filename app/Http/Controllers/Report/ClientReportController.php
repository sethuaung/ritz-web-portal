<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\ClientDataEnteries\Staff;


class ClientReportController extends Controller
{
	

public function index(Request $request)
    {
        if ($request->filled('search_clientlist')) {
            // Search method
            $searchvalue = $request->search_clientlist;
            $data_clienttitle = 'client list';
            $data_clients = DB::table('tbl_client_basic_info')
                ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftjoin('tbl_educations', 'tbl_educations.id', '=', 'tbl_client_basic_info.education_id')
                ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_basic_info.client_uniquekey', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_client_basic_info.name', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_client_basic_info.dob', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_client_basic_info.gender', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_client_basic_info.nrc', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_client_basic_info.phone_primary', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_client_basic_info.client_type', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_client_join.branch_id', 'LIKE', $searchvalue . '%')
                ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_educations.education_name')
                ->Paginate(10);
        } elseif ($request->filled('searchbybranch')) {
            $searchvalue = $request->searchbybranch;
            $data_clienttitle = 'client list';
            $data_clients = DB::table('tbl_client_basic_info')
                ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftjoin('tbl_educations', 'tbl_educations.id', '=', 'tbl_client_basic_info.education_id')
                ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_join.branch_id', 'LIKE', $searchvalue . '%')
                ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_educations.education_name')
                ->Paginate(10);
        } elseif ($request->filled('searchbytype')) {
            $searchvalue = $request->searchbytype;
            $data_clienttitle = 'client list';
            $data_clients = DB::table('tbl_client_basic_info')
                ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftjoin('tbl_educations', 'tbl_educations.id', '=', 'tbl_client_basic_info.education_id')
                ->where('tbl_client_basic_info.client_type', 'LIKE', $searchvalue . '%')
                ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_educations.education_name')
                ->Paginate(10);
        } elseif ($request->filled('date')) {
            $searchvalue = $request->date;
            $data_clienttitle = 'client list';
            if($searchvalue == 'date') {
                $data_clients = DB::table('tbl_client_basic_info')
                    ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->leftjoin('tbl_educations', 'tbl_educations.id', '=', 'tbl_client_basic_info.education_id')
                    ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->whereDate('tbl_client_basic_info.created_at', Carbon::now())
                    ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_educations.education_name')
                    ->Paginate(10);
            } else if($searchvalue == 'month') {
                $data_clients = DB::table('tbl_client_basic_info')
                    ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->leftjoin('tbl_educations', 'tbl_educations.id', '=', 'tbl_client_basic_info.education_id')
                    ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->whereMonth('tbl_client_basic_info.created_at', Carbon::now()->month)
                    ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_educations.education_name')
                    ->Paginate(10);
            } else if($searchvalue == 'year') {
                $data_clients = DB::table('tbl_client_basic_info')
                    ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->leftjoin('tbl_educations', 'tbl_educations.id', '=', 'tbl_client_basic_info.education_id')
                    ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->whereYear('tbl_client_basic_info.created_at', Carbon::now()->year)
                    ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_educations.education_name')
                    ->Paginate(10);
            } else {
                $data_clients = DB::table('tbl_client_basic_info')
                    ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->leftjoin('tbl_educations', 'tbl_educations.id', '=', 'tbl_client_basic_info.education_id')
                    ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->whereBetween('tbl_client_basic_info.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                    ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_educations.education_name')
                    ->Paginate(10);
            }
        } elseif ($request->filled('client_report_length')) {
            $searchvalue = $request->client_report_length;
            $data_clients = DB::table('tbl_client_basic_info')
                ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftjoin('tbl_educations', 'tbl_educations.id', '=', 'tbl_client_basic_info.education_id')
                ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_educations.education_name')
                ->Paginate($searchvalue);
        } else {
            $data_clients = DB::table('tbl_client_basic_info')
                ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftjoin('tbl_educations', 'tbl_educations.id', '=', 'tbl_client_basic_info.education_id')
                ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_educations.education_name')
                ->Paginate(10);
        }

        $data_branch = Branch::orderby('id')->get();
        return view('client_reports.index', compact('data_clients', 'data_branch'));
    }

    // public function getClientType(Request $request){
    //     $client_report = DB::table('tbl_client_basic_info')
    //                     ->join('tbl_client_photo','tbl_client_photo.client_uniquekey','=','tbl_client_basic_info.client_uniquekey')
    //                     ->leftjoin('tbl_educations','tbl_educations.id','=','tbl_client_basic_info.education_id')
    //                     ->where('tbl_client_basic_info.client_type','=',$request->client_type)
    //                     ->select('tbl_client_basic_info.*','tbl_client_photo.client_photo','tbl_educations.education_name')
    //                     ->get();
    //     return response()->json($client_report);
    // }
}
