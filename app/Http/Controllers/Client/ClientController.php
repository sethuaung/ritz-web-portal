<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Branch;
use App\Models\ClientDataEnteries\Center;
use App\Models\ClientDataEnteries\City;
use App\Models\ClientDataEnteries\Department;
use App\Models\ClientDataEnteries\District;
use App\Models\ClientDataEnteries\Division;
use App\Models\ClientDataEnteries\Education;
use App\Models\ClientDataEnteries\Group;
use App\Models\ClientDataEnteries\Staff;
use App\Models\ClientDataEnteries\Guarantor;
use App\Models\ClientDataEnteries\Industry;
use App\Models\ClientDataEnteries\JobPosition;
use App\Models\ClientDataEnteries\Province;
use App\Models\ClientDataEnteries\Quarter;
use App\Models\ClientDataEnteries\Township;
use App\Models\ClientDataEnteries\Village;
use App\Models\Clients\ClientBasicInfo;
use App\Models\Clients\ClientFamilyInfo;
use App\Models\Clients\ClientJob;
use App\Models\Clients\ClientJoin;
use App\Models\Clients\ClientPhoto;
use App\Models\Clients\ClientSurveyOwnership;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ClientController extends Controller
{
    //Index Page
    public function index(Request $request)
    {
        $this->permissionFilter("client-list");

        if ($request->filled('search_clientlist')) {
            // Search method
            $searchvalue = $request->search_clientlist;
            $data_clienttitle =  'client list';
            $data_clients = DB::table('tbl_client_basic_info')
                ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->join('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftJoin('tbl_job_position', 'tbl_job_position.id', '=', 'tbl_client_job_main.job_position_id')
                ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftJoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_basic_info.client_uniquekey', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_client_basic_info.name', 'LIKE',  $searchvalue . '%')
                ->orWhere('tbl_client_basic_info.dob', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_client_basic_info.gender', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_client_basic_info.nrc', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_client_basic_info.phone_primary', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_client_basic_info.client_type', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_main_join_client.main_client_code', 'LIKE', $searchvalue . '%')
                ->where('tbl_client_join.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_job_position.job_position_name')
                ->orderby('id', 'desc')
                ->Paginate(10);
        } else if ($request->filled('clienttype')) {
            // Client type method
            // 'active','dropout','dead','rejoin','substitute','dormant','reject'
            $clienttypevalue = $request->clienttype;
            $data_clienttitle =  $clienttypevalue . ' member list';
            $data_clients = DB::table('tbl_client_basic_info')
                ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->join('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftJoin('tbl_job_position', 'tbl_job_position.id', '=', 'tbl_client_job_main.job_position_id')
                ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_basic_info.client_type', '=', $clienttypevalue)
                ->where('tbl_client_join.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_job_position.job_position_name')
                ->orderby('id', 'desc')
                ->Paginate(10);
        } else if ($request->filled('date')) {
            $searchvalue = $request->date;
            $data_clienttitle =  'Client List';
            if ($request->date == "date") {
                $data_clients = DB::table('tbl_client_basic_info')
                    ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->join('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->leftJoin('tbl_job_position', 'tbl_job_position.id', '=', 'tbl_client_job_main.job_position_id')
                    ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->whereDate('tbl_client_basic_info.created_at', '=', Carbon::now()->format('Y-m-d'))
                    ->where('tbl_client_join.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                    ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_job_position.job_position_name')
                    ->orderby('id', 'desc')
                    ->Paginate(10);
            } else if ($request->date == "month") {
                $data_clients = DB::table('tbl_client_basic_info')
                    ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->join('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->leftJoin('tbl_job_position', 'tbl_job_position.id', '=', 'tbl_client_job_main.job_position_id')
                    ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->whereMonth('tbl_client_basic_info.created_at', '=', Carbon::now()->month)
                    ->where('tbl_client_join.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                    ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_job_position.job_position_name')
                    ->orderby('id', 'desc')
                    ->Paginate(10);
            } else if ($request->date == "year") {
                $data_clients = DB::table('tbl_client_basic_info')
                    ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->join('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->leftJoin('tbl_job_position', 'tbl_job_position.id', '=', 'tbl_client_job_main.job_position_id')
                    ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->whereYear('tbl_client_basic_info.created_at', '=', Carbon::now()->year)
                    ->where('tbl_client_join.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                    ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_job_position.job_position_name')
                    ->orderby('id', 'desc')
                    ->Paginate(10);
            } else {
                $data_clients = DB::table('tbl_client_basic_info')
                    ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->join('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->leftJoin('tbl_job_position', 'tbl_job_position.id', '=', 'tbl_client_job_main.job_position_id')
                    ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                    ->whereBetween('tbl_client_basic_info.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                    ->where('tbl_client_join.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                    ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_job_position.job_position_name')
                    ->orderby('id', 'desc')
                    ->Paginate(10);
            }
        } else if ($request->filled('client_length')) {
            $searchvalue = $request->client_length;
            $data_clienttitle =  'View All Client';
            $data_clients = DB::table('tbl_client_basic_info')
                ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->join('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftJoin('tbl_job_position', 'tbl_job_position.id', '=', 'tbl_client_job_main.job_position_id')
                ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_join.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_job_position.job_position_name')
                ->orderby('id', 'desc')
                ->Paginate($searchvalue);
        } else {
            // All data get
            $data_clienttitle =  'View All Client';
            $data_clients = DB::table('tbl_client_basic_info')
                ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->join('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftJoin('tbl_job_position', 'tbl_job_position.id', '=', 'tbl_client_job_main.job_position_id')
                ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_join.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->select('tbl_client_basic_info.*', 'tbl_client_photo.client_photo', 'tbl_job_position.job_position_name')
                ->orderby('id', 'desc')
                ->Paginate(10);
            // dd($data_clients);
        }


        $data_branch = Branch::orderby('id')->get();

        $data_clients = $data_clients->appends(['clienttype' => $request->clienttype]);

        //dd($data_clients);
        return view('client.index', compact('data_clients', 'data_clienttitle', 'data_branch'));
    }

    //Create Page
    public function create()
    {
        $this->permissionFilter("add-client");

        $divisions = DB::table('addresses')->where('type', '=', 'state')->get();
        $districts = District::orderby('district_name')->select('district_name', 'id')->get();
        $cities = City::orderby('city_name')->select('city_name', 'id')->get();
        $townships = Township::orderby('township_name')->select('township_name', 'id')->get();
        $provinces = Province::orderby('province_name')->select('province_name', 'id')->get();
        $quarters = Quarter::orderby('quarter_name')->select('quarter_name', 'id')->get();
        $villages = Village::orderby('village_name')->select('village_name', 'id')->get();
        $industries = Industry::orderby('industry_name')->select('industry_name', 'id')->get();
        $departments = Department::orderby('department_name')->select('department_name', 'id')->get();
        $jobpositions = JobPosition::orderby('job_position_name')->select('job_position_name', 'id')->get();
        $educations = Education::select()->get();
        $clients = ClientBasicInfo::select('nrc_card_id')->get();
        // dd($clients);
        // $data_group = Group::where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)->orderby('id')->select('id','group_uniquekey')->distinct()->get();
        $data_group = Group::where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)->orderby('id')->select('id', 'group_uniquekey')->get()->unique('group_uniquekey');
        $data_center = DB::table('tbl_center')
            ->leftjoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', 'tbl_center.center_uniquekey')
            ->where('tbl_center.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
            ->orderby('tbl_main_join_center.main_center_code', 'asc')
            ->get();
        $data_guarantor = Guarantor::where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)->orderby('id')->get();
        $data_loan_officers = DB::table('tbl_staff')->where('role_id', 1)->get();
        // dd($data_group);
        return view('/client/create', compact('clients', 'divisions', 'districts', 'cities', 'provinces', 'townships', 'quarters', 'villages', 'industries', 'educations', 'data_group', 'data_center', 'data_guarantor', 'data_loan_officers'));
    }
    //auto generate client uniquekey
    public function generateClientID()
    {
        $obj_branchcode = DB::table('tbl_branches')->where('id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)->select('branch_code')->get();
        // $obj_branchcode = DB::table('tbl_staff')
        //                     ->join('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
        //                     ->where('tbl_staff.staff_code',"01-1322")
        //                     ->select('tbl_branches.branch_code')
        //                     ->get();
        if (count(array($obj_branchcode)) > 0) {
            $subcode = substr($obj_branchcode[0]->branch_code, 0, 2); //split branch code
            // get client_uniquekey in tbl_client_basic_info
            $obj_fund = DB::table('tbl_client_basic_info')->where('client_uniquekey', 'LIKE', '%' . $subcode . '%')->get()->last();
            // dd($obj_fund);

            if ($obj_fund != null) {
                // $subnum = substr($obj_fund->client_uniquekey, 6); // sub-string to 01-000001
                // // dd($subnum);
                // $addnum = $subnum + 1; // add id in +1
                // $cusmclientidnum =  (string)str_pad($addnum, 7, '0', STR_PAD_LEFT); // add generate in zero
                // $clientid = $subcode; // Branch Code
                // $clientid .= '-';
                // $clientid .= '01';
                // $clientid .= '-';
                // $clientid .= $cusmclientidnum; // Client ID in branch
                $clientid = ++$obj_fund->client_uniquekey;
                return $clientid;
            } else {
                $cusmclientidnum =  '0000001'; // add generate in zero
                $clientid = $subcode;
                $clientid .= '-';
                $clientid .= '01';
                $clientid .= '-';
                $clientid .= $cusmclientidnum; // Client ID in branch

                return $clientid;
                // return response()->json(['status_code'=>500,'message'=>'client create fail','data'=>null]);
            }
        } else {
            return response()->json(['status_code' => 500, 'message' => 'client create fail', 'data' => null]);
        }
    }

    public function store(Request $request)
    {
        // dd($request);
        
        $this->permissionFilter("add-client");

        $validator = $request->validate([
            'name' => 'required',
            'dob' => 'required|date|before:-18 years',
            'phone_primary' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:8|max:11',
            // 'gender' => 'required',
            'phone_secondary' => 'regex:/^([0-9\s\-\+\(\)]*)$/|min:8|max:11|nullable',
            'nrc_card_id' => 'unique:tbl_client_basic_info,nrc_card_id',
            // 'blood_type' => 'required|max:2',
            // 'religion' => 'required',
            // 'nationality' => 'required',
            // 'education_id' => 'required',
            'address_primary' => 'required',
            // 'father_name' => 'required|min:3',
            // 'marital_status' => 'required',
            'client_photo' => 'required|mimes:jpeg,png,jpg,gif,svg,web|max:2048',
            'recognition_front_photo' => 'mimes:jpeg,png,jpg,gif,svg,web|max:2048',
            'recognition_back_photo' => 'mimes:jpeg,png,jpg,gif,svg,web|max:2048'


        ]);

        if ($validator) {

            $client_uniquekey = $this->generateClientID();
            // dd($client_uniquekey);
            // $nrc_card_id = ClientBasicInfo::where('nrc_card_id','=', $request->input('nrc_card_id'))->first();
            // if($nrc_card_id === null){
            //     $nrc_card = $request->nrc_card_id;
            // }elseif($request->nrc_card_id==NULL){
            // 	return redirect()->back()->withErrors("Please Fill NRC")->withInput();
            // }else{
            //     return redirect()->back()->withErrors("NRC is Already Exist")->withInput();
            // }


            $client = new ClientBasicInfo;
            $client->client_uniquekey = $client_uniquekey;
            // $client->client_uniquekey = "AM-01-0000001";
            $client->name = $request->name;
            $client->name_mm = $request->name_mm;
            $client->dob = date_format(date_create($request->dob), 'd/m/Y');
            $client->phone_primary = $request->phone_primary;
            if (!empty($request->old_nrc)) {
                $client->old_nrc = $request->old_nrc;
            } else if (!empty($request->nrc_1 && $request->nrc_2 && $request->nrc_3 && $request->nrc_4)) {
                $client->nrc = $request->nrc_1 . '/' . $request->nrc_2 . $request->nrc_3 . $request->nrc_4;
            } else {
                $client->nrc = null;
                $client->old_nrc = null;
            }
            if ($request->nrc_card_id) {
                $client->nrc_card_id = $request->nrc_card_id;
            }
            $client->gender = $request->gender;
            $client->email = $request->email;
            $client->phone_secondary = $request->phone_secondary;
            $client->blood_type = $request->blood_type;
            $client->religion = $request->religion;
            $client->nationality = $request->nationality;
            $client->education_id = $request->education_id;
            $client->other_education = $request->other_education;
            $client->quarter_id = $request->ward_id;
            $client->village_id = $request->village_id;
            $client->township_id = $request->township_id;
            $client->district_id = $request->district_id;
            // $client->city_id = $request->city_id;
            $client->division_id = $request->division_id;
            $client->address_primary = $request->address_primary;
            $client->address_secondary = $request->address_secondary;
            $client->client_type = $request->client_type;
            $client->save();


            if (!$client) {

                return redirect()->back()->withErrors('Something Wrong ClientBasicInfo');
            }

            //family
            $clientfamily = new ClientFamilyInfo;
            $clientfamily->client_uniquekey = $client_uniquekey;
            $clientfamily->father_name = $request->father_name;
            $clientfamily->marital_status = $request->marital_status;
            $clientfamily->spouse_name = $request->spouse_name;
            $clientfamily->occupation_of_spouse = $request->occupation_of_spouse;
            $clientfamily->no_of_family = $request->no_of_family;
            $clientfamily->no_of_working_family = $request->no_of_working_family;
            $clientfamily->no_of_children = $request->no_of_children;
            $clientfamily->no_of_working_progeny = $request->no_of_working_progeny;
            $clientfamily->save();

            if (!$clientfamily) {

                return redirect()->back()->withErrors('Something Wrong ClientFamilyInfo');
            }

            //poto
            $destinationPath = 'public/clientphotos/';

            if ($request->nrc_recommendation) {
                $client_photo_name = time() . '.' . $request->client_photo->getClientOriginalName();
                $nrc_recommendation = time() . '.' . $request->nrc_recommendation->getClientOriginalName();
                $registration_family_front_photo_name = time() . '.' . $request->registration_family_front_photo->getClientOriginalName();
                $registration_family_back_photo_name = time() . '.' . $request->registration_family_back_photo->getClientOriginalName();

                $request->client_photo->storeAs($destinationPath, $client_photo_name);
                $request->nrc_recommendation->storeAs($destinationPath, $nrc_recommendation);
                $request->registration_family_front_photo->storeAs($destinationPath, $registration_family_front_photo_name);
                $request->registration_family_back_photo->storeAs($destinationPath, $registration_family_back_photo_name);

                $clientphoto = new ClientPhoto();
                $clientphoto->client_uniquekey = $client_uniquekey;
                $clientphoto->client_photo = $client_photo_name;
                $clientphoto->nrc_recommendation = $nrc_recommendation;
                $clientphoto->registration_family_form_front = $registration_family_front_photo_name;
                $clientphoto->registration_family_form_back = $registration_family_back_photo_name;

                if ($request->finger_print_bio) {
                    $finger_print_bio_name = time() . '.' . $request->finger_print_bio->getClientOriginalName();
                    $request->finger_print_bio->storeAs($destinationPath, $finger_print_bio_name);
                    $clientphoto->finger_print_bio = $finger_print_bio_name;
                }
                if ($request->government_recommendations_photo) {
                    $government_recommendations_photo_name = time() . '.' . $request->government_recommendations_photo->getClientOriginalName();
                    $request->government_recommendations_photo->storeAs($destinationPath, $government_recommendations_photo_name);
                    $clientphoto->government_recommendations_photo = $government_recommendations_photo_name;
                }


                $clientphoto->save();
            } else {
                $client_photo_name = time() . '.' . $request->client_photo->getClientOriginalName();
                $recognition_front_photo_name = time() . '.' . $request->recognition_front_photo->getClientOriginalName();
                $recognition_back_photo_name = time() . '.' . $request->recognition_back_photo->getClientOriginalName();
                $registration_family_front_photo_name = time() . '.' . $request->registration_family_front_photo->getClientOriginalName();
                $registration_family_back_photo_name = time() . '.' . $request->registration_family_back_photo->getClientOriginalName();

                $request->client_photo->storeAs($destinationPath, $client_photo_name);
                $request->recognition_front_photo->storeAs($destinationPath, $recognition_front_photo_name);
                $request->recognition_back_photo->storeAs($destinationPath, $recognition_back_photo_name);
                $request->registration_family_front_photo->storeAs($destinationPath, $registration_family_front_photo_name);
                $request->registration_family_back_photo->storeAs($destinationPath, $registration_family_back_photo_name);

                $clientphoto = new ClientPhoto();
                $clientphoto->client_uniquekey = $client_uniquekey;
                $clientphoto->client_photo = $client_photo_name;
                $clientphoto->recognition_front = $recognition_front_photo_name;
                $clientphoto->recognition_back = $recognition_back_photo_name;
                $clientphoto->registration_family_form_front = $registration_family_front_photo_name;
                $clientphoto->registration_family_form_back = $registration_family_back_photo_name;

                if ($request->finger_print_bio) {
                    $finger_print_bio_name = time() . '.' . $request->finger_print_bio->getClientOriginalName();
                    $request->finger_print_bio->storeAs($destinationPath, $finger_print_bio_name);
                    $clientphoto->finger_print_bio = $finger_print_bio_name;
                }
                if ($request->government_recommendations_photo) {
                    $government_recommendations_photo_name = time() . '.' . $request->government_recommendations_photo->getClientOriginalName();
                    $request->government_recommendations_photo->storeAs($destinationPath, $government_recommendations_photo_name);
                    $clientphoto->government_recommendations_photo = $government_recommendations_photo_name;
                }

                $clientphoto->save();
            }

            if (!$clientphoto) {

                return redirect()->back()->withErrors('Something Wrong Clientphoto');
            }

            //survey&ownerships
            $surveyownership = new ClientSurveyOwnership();
            $surveyownership->client_uniquekey = $client_uniquekey;

            if ($request->unremovable_checkbox) {
                $unremovableCheckboxes = implode(',', $request->get('unremovable_checkbox'));
                $surveyownership->assetstype_unremovable = $unremovableCheckboxes;
            }
            if ($request->removable_checkbox) {
                $removableCheckboxes = implode(',', $request->get('removable_checkbox'));
                $surveyownership->assetstype_removable = $removableCheckboxes;
            }
            // dd($removableCheckboxes);


            $surveyownership->purchase_price = $request->purchase_price;
            $surveyownership->current_value = $request->current_value;
            $surveyownership->quantity = $request->quantity;


            $destinationPath2 = 'public/client_ownership_documents/';

            if ($request->ownership_docu_photo1) {
                $ownership_docu_photo1_name = time() . '.' . $request->ownership_docu_photo1->getClientOriginalName();
                $request->ownership_docu_photo1->storeAs($destinationPath2, $ownership_docu_photo1_name);
                $surveyownership->attach_1 = $ownership_docu_photo1_name;
            }
            if ($request->ownership_docu_photo2) {
                $ownership_docu_photo2_name = time() . '.' . $request->ownership_docu_photo2->getClientOriginalName();
                $request->ownership_docu_photo2->storeAs($destinationPath2, $ownership_docu_photo2_name);
                $surveyownership->attach_2 = $ownership_docu_photo2_name;
            }
            if ($request->ownership_docu_photo3) {
                $ownership_docu_photo3_name = time() . '.' . $request->ownership_docu_photo3->getClientOriginalName();
                $request->ownership_docu_photo3->storeAs($destinationPath2, $ownership_docu_photo3_name);
                $surveyownership->attach_3 = $ownership_docu_photo3_name;
            }
            if ($request->ownership_docu_photo4) {
                $ownership_docu_photo4_name = time() . '.' . $request->ownership_docu_photo4->getClientOriginalName();
                $request->ownership_docu_photo4->storeAs($destinationPath2, $ownership_docu_photo4_name);
                $surveyownership->attach_4 = $ownership_docu_photo4_name;
            }

            $surveyownership->save();

            if (!$surveyownership) {
                return redirect()->back()->withErrors('Something Wrong ClientSurvey&Ownership');
            }
            if ($request->job_status == "Choose Job Status") {
                $job = 'none';
            } else {
                $job = $request->job_status;
            }

            //employee status
            $clientjob = new ClientJob();
            $clientjob->client_uniquekey = $client_uniquekey;
            $clientjob->industry_id = $request->industry_id;
            $clientjob->job_status = $job;
            $clientjob->department_id = $request->department_id;
            $clientjob->job_position_id = $request->job_position_id;
            $clientjob->company_name = $request->company_name;
            $clientjob->company_phone = $request->company_phone;
            $clientjob->experience = $request->experience;
            $clientjob->experience2 = $request->experience2;
            $clientjob->salary = $request->salary;
            $clientjob->company_address = $request->company_address;
            $clientjob->current_business = $request->current_business;
            $clientjob->business_income = $request->business_income;
            $clientjob->save();
            if (!$clientjob) {
                return redirect()->back()->withErrors('Something Wrong ClientJob');
            }

            //join to other status


            $clientjoin = new ClientJoin();
            $clientjoin->client_id = $client_uniquekey;
            $clientjoin->branch_id = Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id;
            // $clientjoin->staff_id = Staff::where('staff_code', session()->get('staff_code'))->first()->staff_code;
            $clientjoin->staff_id = $request->loan_officer;
            $clientjoin->guarantors_id = $request->client_guarantor;
            $clientjoin->center_id = $request->client_type_center;
            $clientjoin->group_id = $request->client_type_group;
            $clientjoin->loan_id = '';
            $clientjoin->deposit_id = '';
            $clientjoin->disbursement_id = '';
            $clientjoin->repayment_id = '';
            $clientjoin->saving_withdrawal_id = '';
            $clientjoin->registration_date = $request->reg_date;
            $clientjoin->client_status = $request->client_type;
            $clientjoin->you_are_a_center_leader = $request->you_are_a_center_leader;
            $clientjoin->you_are_a_group_leader = $request->you_are_a_group_leader;
            $clientjoin->save();

            $staff_id = DB::table('tbl_group')->where('group_uniquekey', $request->client_type_group)->select('staff_id')->first();

            //add client as a group member
            // if($request->client_type_group) {
            //    $group = new Group();
            //    $group->group_uniquekey = $request->client_type_group;
            //     $group->staff_id = $staff_id->staff_id;
            //     $group->client_id = $client_uniquekey;
            //     $group->branch_id = Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id;
            //     $group->center_id = $request->client_type_center;
            //     $group->level_status = 'member';
            //     $group->del_status = 'none';
            //     $group->save();
            // }

            //add client in center
            // if($request->client_type_center) {
            //     $center = new Center();
            //     $center->center_uniquekey = $request->client_type_center;
            //     $center->staff_client_id = $client_uniquekey;
            //     $center->branch_id = Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id;
            //     $center->staff_id = session()->get('staff_code');
            //     $center->type_status = 'client';
            //     $center->del_status = 'none';
            // }

            //live crawl start

            // // main clients id generate
            // $dataclients = DB::connection('main')->table('clients')->where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)->orderBy('client_number', 'desc')->first();
            // $main_client_generate_id = $dataclients->client_number;
            // $main_client_generate_id = ++$main_client_generate_id;

            // //client_type
            // if ($request->client_type == 'new') {
            //     $customer_group_id = "1";
            // } elseif ($request->client_type == 'active') {
            //     $customer_group_id = "1";
            // } elseif ($request->client_type == 'dropout') {
            //     $customer_group_id = "3";
            // } elseif ($request->client_type == 'dead') {
            //     $customer_group_id = "2";
            // } elseif ($request->client_type == 'rejoin') {
            //     $customer_group_id = "4";
            // } elseif ($request->client_type == 'substitute') {
            //     $customer_group_id = "5";
            // } elseif ($request->client_type == 'dormant') {
            //     $customer_group_id = "13";
            // } elseif ($request->client_type == 'reject') {
            //     $customer_group_id = "13";
            // }

            // //main centerid
            // $main_center_id = DB::table('tbl_main_join_center')->where('portal_center_id', $clientjoin->center_id)->first()->main_center_id;

            // DB::connection('main')->table('clients')->insert([
            //     'branch_id' => Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id,
            //     'name' => $request->name,
            //     'name_other' => $request->name_mm,
            //     // 'center_code' => $centerid, // change center
            //     'center_name' => '', // change later
            //     'register_date' => date('Y-m-d', strtotime($request->reg_date)),
            //     'client_number' => $main_client_generate_id,
            //     'customer_group_id' => $customer_group_id, //change group
            //     'gender' => $request->gender,
            //     'dob' => date('Y-m-d', strtotime($request->dob)),
            //     'education' => DB::table('tbl_educations')->where('id', $request->education_id)->first()->education_name,
            //     'primary_phone_number' => $request->phone_primary,
            //     'alternate_phone_number' => $request->phone_secondary,
            //     'nrc_number' => $client->nrc == null ? $client->old_nrc : $client->nrc,
            //     'nrc_type' => $client->nrc == null ? 'Old Format' : 'New Format',
            //     'marital_status' => $clientfamily->marital_status,
            //     'status' => 'Active',
            //     'father_name' => $clientfamily->father_name,
            //     'husband_name' => $clientfamily->spouse_name,
            //     'occupation_of_husband' => $clientfamily->occupation_of_spouse,
            //     'no_children_in_family' => $clientfamily->no_of_children,
            //     'no_of_working_people' => $clientfamily->no_of_working_family,
            //     'no_of_person_in_family' => $clientfamily->no_of_family,
            //     'address1' => $client->address_primary,
            //     'address2' => $client->address_secondary,
            //     'family_registration_copy' => $clientphoto->registration_family_form_front,
            //     'photo_of_client' => $clientphoto->client_photo,
            //     'nrc_photo' => $clientphoto->recognition_front,
            //     'scan_finger_print' => $clientphoto->finger_print_bio,
            //     'survey_id' => NULL,
            //     'remark' => 'active',
            //     'center_leader_id' => $main_center_id,
            //     // 'loan_officer_id'=>,
            //     'you_are_a_group_leader' => $request->you_are_a_group_leader,
            //     'you_are_a_center_leader' => $request->you_are_a_center_leader,
            //     //'ownership_of_farmland'=>$clients[$i]->land,
            //     //'ownership'=>$clients[$i]->ownership,
            //     //'province_id' => $division_id,
            //     //'district_id' => $district_id,
            //     //'commune_id' => $commue_id,
            //     //'village_id' => $village_id,
            //     'loan_officer_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $clientjoin->staff_id)->first()->main_staff_id,
            //     'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $clientjoin->staff_id)->first()->main_staff_id,
            //     'updated_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $clientjoin->staff_id)->first()->main_staff_id,
            //     'created_at' => $request->created_at,
            //     'updated_at' => $request->updated_at,
            //     'company_address1' => $request->company_address,
            // ]);

            // DB::table('tbl_main_join_client')->insert([
            //     'main_client_id' => DB::connection('main')->table('clients')->where('client_number', $main_client_generate_id)->first()->id,
            //     'portal_client_id' => $client_uniquekey,
            //     'portal_branch_id' => Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id,
            //     'main_client_code' => $main_client_generate_id
            // ]);

            //live crawl end

            if (!$clientjoin) {
                return redirect()->back()->withErrors('Something Wrong ClientJoin');
            }
            //live reverse
            $this->liveReverseClient($client_uniquekey);

            return redirect('client/')->with("successMsg", 'New Client is Added in your data');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    //Detail Page
    public function view($id)
    {
        // dd($id);
        $client_uniquekey = ClientBasicInfo::where("client_uniquekey", "=", $id)->first()->client_uniquekey;

        $clients  = DB::table('tbl_client_basic_info')
            ->join('tbl_client_family_info', 'tbl_client_family_info.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->join('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->join('tbl_client_survey_ownership', 'tbl_client_survey_ownership.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            // ->leftjoin('tbl_divisions', 'tbl_client_basic_info.division_id', '=', 'tbl_divisions.id')
            // ->leftjoin('tbl_districts', 'tbl_client_basic_info.district_id', '=', 'tbl_districts.id')
            // ->leftjoin('tbl_cities', 'tbl_client_basic_info.city_id', '=', 'tbl_cities.id')
            // ->leftjoin('tbl_townships', 'tbl_client_basic_info.township_id', '=', 'tbl_townships.id')
            // ->leftjoin('tbl_provinces', 'tbl_client_basic_info.province_id', '=', 'tbl_provinces.id')
            // ->leftjoin('tbl_quarters', 'tbl_client_basic_info.quarter_id', '=', 'tbl_quarters.id')
            // ->leftjoin('tbl_villages', 'tbl_client_basic_info.village_id', '=', 'tbl_villages.id')
            ->leftjoin('tbl_educations', 'tbl_client_basic_info.education_id', '=', 'tbl_educations.id')
            ->where('tbl_client_basic_info.client_uniquekey', '=', $client_uniquekey)
            ->first();
        // dd($clients);
        // dd(explode(',',$clients->land));
        $job = DB::table('tbl_client_job_main')
            ->leftjoin('tbl_industry_type', 'tbl_client_job_main.industry_id', '=', 'tbl_industry_type.id')
            ->leftjoin('tbl_job_department', 'tbl_client_job_main.department_id', '=', 'tbl_job_department.id')
            ->leftjoin('tbl_job_position', 'tbl_client_job_main.job_position_id', '=', 'tbl_job_position.id')
            ->where('tbl_client_job_main.client_uniquekey', '=', $client_uniquekey)
            ->first();

        $client_type = DB::table('tbl_client_join')
            ->leftjoin('tbl_group', 'tbl_client_join.group_id', '=', 'tbl_group.group_uniquekey')
            ->leftjoin('tbl_center', 'tbl_client_join.center_id', '=', 'tbl_center.center_uniquekey')
            ->leftjoin('tbl_guarantors', 'tbl_client_join.guarantors_id', '=', 'tbl_guarantors.guarantor_uniquekey')
            ->where('tbl_client_join.client_id', '=', $client_uniquekey)
            ->select('tbl_group.group_uniquekey', 'tbl_center.center_uniquekey', 'tbl_guarantors.name', 'tbl_client_join.*')
            ->first();

        $division = DB::table('addresses')->where('code', $clients->division_id)->first();
        $district = DB::table('addresses')->where('code', $clients->district_id)->first();
        $township = DB::table('addresses')->where('code', $clients->township_id)->first();
        $village = DB::table('addresses')->where('code', $clients->village_id)->first();
        $ward = DB::table('addresses')->where('code', $clients->quarter_id)->first();
        // dd($clients);
        // dd($client_type);
        return view('client/view', compact('clients', 'job', 'client_type', 'division', 'district', 'township', 'village', 'ward'));
    }

    //Edit Page
    public function edit($id)
    {
        $this->permissionFilter("update-client");

        $client_uniquekey = ClientBasicInfo::where("client_uniquekey", "=", $id)->first()->client_uniquekey;
        $clients  = DB::table('tbl_client_basic_info')
            ->join('tbl_client_family_info', 'tbl_client_family_info.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->join('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->join('tbl_client_survey_ownership', 'tbl_client_survey_ownership.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->join('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftjoin('tbl_divisions', 'tbl_client_basic_info.division_id', '=', 'tbl_divisions.id')
            ->leftjoin('tbl_districts', 'tbl_client_basic_info.district_id', '=', 'tbl_districts.id')
            ->leftjoin('tbl_cities', 'tbl_client_basic_info.city_id', '=', 'tbl_cities.id')
            ->leftjoin('tbl_townships', 'tbl_client_basic_info.township_id', '=', 'tbl_townships.id')
            ->leftjoin('tbl_provinces', 'tbl_client_basic_info.province_id', '=', 'tbl_provinces.id')
            ->leftjoin('tbl_quarters', 'tbl_client_basic_info.quarter_id', '=', 'tbl_quarters.id')
            ->leftjoin('tbl_villages', 'tbl_client_basic_info.village_id', '=', 'tbl_villages.id')
            ->leftjoin('tbl_educations', 'tbl_client_basic_info.education_id', '=', 'tbl_educations.id')
            ->where('tbl_client_basic_info.client_uniquekey', '=', $client_uniquekey)
            ->select('tbl_client_basic_info.*', 'tbl_client_family_info.*', 'tbl_client_job_main.*', 'tbl_client_photo.*', 'tbl_client_survey_ownership.*', 'tbl_client_join.*')
            ->first();
        // dd($clients);
        function explodeNRC($delimiters, $string)
        {
            return explode(chr(1), str_replace($delimiters, chr(1), $string));
        }
        $assetstype_removable = str_replace(array('[', ']'), '', $clients->assetstype_removable);
        // dd(in_array('Stock in Hand', explode(',', $clients->assetstype_removable)));
        $assetstype_unremovable = str_replace(array('[', ']'), '', $clients->assetstype_unremovable);

        $list = $clients->nrc;

        $exploded_nrc = explodeNRC(array('/', '(', ')'), $list);

        $divisions = Division::select('division_name', 'id')->get();
        $districts = District::where('division_id', '=', $clients->division_id)->select('district_name', 'id')->get();
        $cities = City::select('city_name', 'id')->get();
        $townships = Township::where('district_id', '=', $clients->district_id)->select('township_name', 'id')->get();
        $provinces = Province::where('district_id', '=', $clients->district_id)->select('province_name', 'id')->get();
        $villages = Village::where('township_province_id', '=', $clients->township_id)->select('village_name', 'id')->get();
        $quarters = Quarter::where('town_id', '=', $clients->village_id)->select('quarter_name', 'id')->get();
        $industries = Industry::select('industry_name', 'id')->get();
        $departments = Department::select('department_name', 'id')->get();
        $jobpositions = JobPosition::select('job_position_name', 'id')->get();
        $educations = Education::select()->get();
        // $data_group = Group::where('branch_id',Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)->orderby('id')->select('id', 'group_uniquekey')->distinct()->get();
        $data_group = Group::where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)->where('client_id', $client_uniquekey)->orderby('id')->select('id', 'group_uniquekey')->get()->unique('group_uniquekey');
        $data_center = Center::where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)->orderby('id')->get();
        $data_guarantor = Guarantor::where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)->orderby('id')->get();
        // dd($data_group);

        return view('client/edit', compact('clients', 'divisions', 'districts', 'cities', 'townships', 'provinces', 'villages', 'quarters', 'industries', 'departments', 'jobpositions', 'educations', 'exploded_nrc', 'data_group', 'data_center', 'data_guarantor', 'assetstype_unremovable', 'assetstype_removable'));
    }

    //Update
    public function update(Request $request, $id)
    {
        // dd($request);
        $this->permissionFilter("update-client");

        if ($request->nrc_1) {
            $nrc = $request->nrc_1 . '/' . $request->nrc_2 . $request->nrc_3 . $request->nrc_4;
        } else {
            $nrc = null;
        }

        $client_uniquekey = $request->client_uniquekey;
        // dd($client_uniquekey);
        DB::table('tbl_client_basic_info')
            ->join('tbl_client_family_info', 'tbl_client_family_info.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->join('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->join('tbl_client_survey_ownership', 'tbl_client_survey_ownership.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftjoin('tbl_divisions', 'tbl_client_basic_info.division_id', '=', 'tbl_divisions.id')
            ->leftjoin('tbl_districts', 'tbl_client_basic_info.district_id', '=', 'tbl_districts.id')
            ->leftjoin('tbl_cities', 'tbl_client_basic_info.city_id', '=', 'tbl_cities.id')
            ->leftjoin('tbl_townships', 'tbl_client_basic_info.township_id', '=', 'tbl_townships.id')
            ->leftjoin('tbl_provinces', 'tbl_client_basic_info.province_id', '=', 'tbl_provinces.id')
            ->leftjoin('tbl_quarters', 'tbl_client_basic_info.quarter_id', '=', 'tbl_quarters.id')
            ->leftjoin('tbl_villages', 'tbl_client_basic_info.village_id', '=', 'tbl_villages.id')
            ->leftjoin('tbl_educations', 'tbl_client_basic_info.education_id', '=', 'tbl_educations.id')
            ->where('tbl_client_basic_info.client_uniquekey', $client_uniquekey)
            ->update([
                'tbl_client_basic_info.client_uniquekey' => $client_uniquekey,
                'tbl_client_basic_info.name' => $request->name,
                'tbl_client_basic_info.name_mm' => $request->name_mm,
                'tbl_client_basic_info.dob' => $request->dob,
                'tbl_client_basic_info.nrc' => $nrc,
                'tbl_client_basic_info.old_nrc' => $request->old_nrc,
                'tbl_client_basic_info.nrc_card_id' => $request->nrc_card_id,
                'tbl_client_basic_info.phone_primary' => $request->phone_primary,
                'tbl_client_basic_info.gender' => $request->gender,
                'tbl_client_basic_info.email' => $request->email,
                'tbl_client_basic_info.phone_secondary' => $request->phone_secondary,
                'tbl_client_basic_info.blood_type' => $request->blood_type,
                'tbl_client_basic_info.religion' => $request->religion,
                'tbl_client_basic_info.nationality' => $request->nationality,
                'tbl_client_basic_info.education_id' => $request->education_id,
                'tbl_client_basic_info.other_education' => $request->other_education,
                'tbl_client_basic_info.client_type' => $request->client_type,
                'tbl_client_basic_info.quarter_id' => $request->quarter_id,
                'tbl_client_basic_info.village_id' => $request->village_id,
                'tbl_client_basic_info.township_id' => $request->township_id,
                'tbl_client_basic_info.district_id' => $request->district_id,
                'tbl_client_basic_info.city_id' => $request->city_id,
                'tbl_client_basic_info.division_id' => $request->division_id,
                'tbl_client_basic_info.address_primary' => $request->address_primary,
                'tbl_client_basic_info.address_secondary' => $request->address_secondary
            ]);

        DB::table('tbl_client_family_info')
            ->where('tbl_client_family_info.client_uniquekey', $client_uniquekey)
            ->update([
                'tbl_client_family_info.client_uniquekey' => $client_uniquekey,
                'tbl_client_family_info.father_name' => $request->father_name,
                'tbl_client_family_info.marital_status' => $request->marital_status,
                'tbl_client_family_info.spouse_name' => $request->spouse_name,
                'tbl_client_family_info.occupation_of_spouse' => $request->occupation_of_spouse,
                'tbl_client_family_info.no_of_family' => $request->no_of_family,
                'tbl_client_family_info.no_of_working_family' => $request->no_of_working_family,
                'tbl_client_family_info.no_of_children' => $request->no_of_children,
                'tbl_client_family_info.no_of_working_progeny' => $request->no_of_working_progeny
            ]);

        DB::table('tbl_client_job_main')
            ->where('tbl_client_job_main.client_uniquekey', $client_uniquekey)
            ->update([
                'tbl_client_job_main.client_uniquekey' => $client_uniquekey,
                'tbl_client_job_main.industry_id' => $request->industry_id,
                'tbl_client_job_main.job_status' => $request->job_status,
                'tbl_client_job_main.department_id' => $request->department_id,
                'tbl_client_job_main.job_position_id' => $request->job_position_id,
                'tbl_client_job_main.company_name' => $request->company_name,
                'tbl_client_job_main.company_phone' => $request->company_phone,
                'tbl_client_job_main.experience' => $request->experience,
                'tbl_client_job_main.experience2' => $request->experience2,
                'tbl_client_job_main.salary' => $request->salary,
                'tbl_client_job_main.company_address' => $request->company_address,
                'tbl_client_job_main.current_business' => $request->current_business,
                'tbl_client_job_main.business_income' => $request->business_income
            ]);

        $surveyownership = ClientSurveyOwnership::where('client_uniquekey', $request->client_uniquekey)->first();
        $destinationPath2 = 'public/client_ownership_documents/';

        if ($request->hasFile('ownership_docu_photo1')) {
            $request->validate([
                'ownership_docu_photo1' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $ownership_docu_photo1_name = time() . '.' . $request->ownership_docu_photo1->getClientOriginalName();
            $request->ownership_docu_photo1->storeAs($destinationPath2, $ownership_docu_photo1_name);
        } else {
            $ownership_docu_photo1_name = $surveyownership->attach_1;
        }

        if ($request->hasFile('ownership_docu_photo2')) {
            $request->validate([
                'ownership_docu_photo2' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $ownership_docu_photo2_name = time() . '.' . $request->ownership_docu_photo2->getClientOriginalName();
            $request->ownership_docu_photo2->storeAs($destinationPath2, $ownership_docu_photo2_name);
        } else {
            $ownership_docu_photo2_name = $surveyownership->attach_2;
        }

        if ($request->hasFile('ownership_docu_photo3')) {
            $request->validate([
                'ownership_docu_photo3' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $ownership_docu_photo3_name = time() . '.' . $request->ownership_docu_photo3->getClientOriginalName();
            $request->ownership_docu_photo3->storeAs($destinationPath2, $ownership_docu_photo3_name);
        } else {
            $ownership_docu_photo3_name = $surveyownership->attach_3;
        }

        if ($request->hasFile('ownership_docu_photo4')) {
            $request->validate([
                'ownership_docu_photo4' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $ownership_docu_photo4_name = time() . '.' . $request->ownership_docu_photo4->getClientOriginalName();
            $request->ownership_docu_photo4->storeAs($destinationPath2, $ownership_docu_photo4_name);
        } else {
            $ownership_docu_photo4_name = $surveyownership->attach_4;
        }
        if ($request->removable_checkbox) {
            $removable = implode(',', $request->get('removable_checkbox'));
            $unremovable = $surveyownership->assetstype_unremovable;
        }
        if ($request->unremovable_checkbox) {
            $removable = $surveyownership->assetstype_removable;
            $unremovable = implode(',', $request->get('unremovable_checkbox'));
        }

        DB::table('tbl_client_survey_ownership')
            ->where('tbl_client_survey_ownership.client_uniquekey', $client_uniquekey)
            ->update([
                'tbl_client_survey_ownership.assetstype_removable' => $removable,
                'tbl_client_survey_ownership.assetstype_unremovable' => $unremovable,
                'tbl_client_survey_ownership.purchase_price' => $request->purchase_price,
                'tbl_client_survey_ownership.current_value' => $request->current_value,
                'tbl_client_survey_ownership.quantity' => $request->quantity,
                'tbl_client_survey_ownership.attach_1' => $ownership_docu_photo1_name,
                'tbl_client_survey_ownership.attach_2' => $ownership_docu_photo2_name,
                'tbl_client_survey_ownership.attach_3' => $ownership_docu_photo3_name,
                'tbl_client_survey_ownership.attach_4' => $ownership_docu_photo4_name,

            ]);

        DB::table('tbl_client_join')
            ->where('tbl_client_join.client_id', $client_uniquekey)
            ->update([
                // 'tbl_client_join.guarantors_id'=>implode(',', $request->get('client_guarantor')),
                // 'tbl_client_join.center_id'=>implode(',', $request->get('client_type_center')),
                // 'tbl_client_join.group_id'=>implode(',', $request->get('client_type_group'))
                'tbl_client_join.guarantors_id' => $request->client_guarantor,
                'tbl_client_join.center_id' => $request->client_type_center,
                'tbl_client_join.group_id' => $request->client_type_group,
                'tbl_client_join.you_are_a_center_leader' => $request->you_are_a_center_leader,
                'tbl_client_join.you_are_a_group_leader' => $request->you_are_a_group_leader,

            ]);

        $clientphoto = ClientPhoto::where('client_uniquekey', $request->client_uniquekey)->first();
        $destinationPath = 'public/clientphotos/';



        // dd($clientphoto);
        if ($request->hasFile('client_photo')) {
            $request->validate([
                'client_photo' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $client_photo_name = time() . '.' . $request->client_photo->getClientOriginalName();
            $request->client_photo->storeAs($destinationPath, $client_photo_name);
        } else {
            $client_photo_name = $clientphoto->client_photo;
        }

        if ($request->hasFile('recognition_front_photo')) {
            $request->validate([
                'recognition_front_photo' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $recognition_front_photo_name = time() . '.' . $request->recognition_front_photo->getClientOriginalName();
            $request->recognition_front_photo->storeAs($destinationPath, $recognition_front_photo_name);
        } else {
            $recognition_front_photo_name = $clientphoto->recognition_front;
        }

        if ($request->hasFile('recognition_back_photo')) {
            $request->validate([
                'recognition_back_photo' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $recognition_back_photo_name = time() . '.' . $request->recognition_back_photo->getClientOriginalName();
            $request->recognition_back_photo->storeAs($destinationPath, $recognition_back_photo_name);
        } else {
            $recognition_back_photo_name = $clientphoto->recognition_back;
        }

        if ($request->hasFile('nrc_recommendation')) {
            $request->validate([
                'nrc_recommendation' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $nrc_recommendation_name = time() . '.' . $request->nrc_recommendation->getClientOriginalName();
            $request->nrc_recommendation->storeAs($destinationPath, $nrc_recommendation_name);
        } else {
            $nrc_recommendation_name = $clientphoto->recognition_back;
        }

        if ($request->hasFile('registration_family_front_photo')) {
            $request->validate([
                'registration_family_front_photo' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $registration_family_front_photo_name = time() . '.' . $request->registration_family_front_photo->getClientOriginalName();
            $request->registration_family_front_photo->storeAs($destinationPath, $registration_family_front_photo_name);
        } else {
            $registration_family_front_photo_name = $clientphoto->registration_family_form_front;
        }

        if ($request->hasFile('registration_family_back_photo')) {
            $request->validate([
                'registration_family_back_photo' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $registration_family_back_photo_name = time() . '.' . $request->registration_family_back_photo->getClientOriginalName();
            $request->registration_family_back_photo->storeAs($destinationPath, $registration_family_back_photo_name);
        } else {
            $registration_family_back_photo_name = $clientphoto->registration_family_form_back;
        }

        if ($request->hasFile('finger_print_bio')) {
            $request->validate([
                'finger_print_bio' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $finger_print_bio_name = time() . '.' . $request->finger_print_bio->getClientOriginalName();
            $request->finger_print_bio->storeAs($destinationPath, $finger_print_bio_name);
        } else {
            $finger_print_bio_name = $clientphoto->finger_print_bio;
        }

        if ($request->hasFile('government_recommendations_photo')) {
            $request->validate([
                'government_recommendations_photo' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            ]);
            $government_recommendations_photo_name = time() . '.' . $request->government_recommendations_photo->getClientOriginalName();
            $request->government_recommendations_photo->storeAs($destinationPath, $government_recommendations_photo_name);
        } else {
            $government_recommendations_photo_name = $clientphoto->government_recommendations_photo;
        }
        //  dd($request);
        DB::table('tbl_client_photo')
            ->where('tbl_client_photo.client_uniquekey', $request->client_uniquekey)
            ->update([
                'tbl_client_photo.client_uniquekey' => $client_uniquekey,
                'tbl_client_photo.client_photo' => $client_photo_name,
                'tbl_client_photo.recognition_front' => $recognition_front_photo_name,
                'tbl_client_photo.recognition_back' => $recognition_back_photo_name,
                'tbl_client_photo.nrc_recommendation' => $nrc_recommendation_name,
                'tbl_client_photo.registration_family_form_front' => $registration_family_front_photo_name,
                'tbl_client_photo.registration_family_form_back' => $registration_family_back_photo_name,
                'tbl_client_photo.finger_print_bio' => $finger_print_bio_name,
                'tbl_client_photo.government_recommendations_photo' => $government_recommendations_photo_name,


            ]);


        return redirect('client/')->with("successMsg", 'Existing Client Update in your data');
    }

    //Delete
    public function destroy($id)
    {
        $this->permissionFilter("delete-client");

        $client_uniquekey = ClientBasicInfo::where("client_uniquekey", "=", $id)->first()->client_uniquekey;
        // dd($client_uniquekey);
        DB::table('tbl_client_basic_info')
            ->join('tbl_client_family_info', 'tbl_client_family_info.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->join('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->join('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->join('tbl_client_survey_ownership', 'tbl_client_survey_ownership.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->join('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
            ->where('tbl_client_basic_info.client_uniquekey', '=', $client_uniquekey)->delete();
        DB::table('tbl_client_family_info')->where('tbl_client_family_info.client_uniquekey', '=', $client_uniquekey)->delete();
        DB::table('tbl_client_job_main')->where('tbl_client_job_main.client_uniquekey', '=', $client_uniquekey)->delete();
        DB::table('tbl_client_photo')->where('tbl_client_photo.client_uniquekey', '=', $client_uniquekey)->delete();
        DB::table('tbl_client_survey_ownership')->where('tbl_client_survey_ownership.client_uniquekey', '=', $client_uniquekey)->delete();
        DB::table('tbl_client_join')->where('tbl_client_join.client_id', '=', $client_uniquekey)->delete();


        // dd($clients);
        return redirect('client/')->with('successMsg', 'Existing Client Delete in your Data');
    }



    //json response
    // public function getYearlySalary(Request $request){
    //     $salary = $request->salary;
    //     $salary_yearly = $salary  * 12;

    //     return response()->json($salary_yearly);
    // }

    public function getDept(Request $request)
    {

        $depts = Department::where("industry_id", $request->industry_id)
            ->pluck("department_name", "id");
        return response()->json($depts);
    }

    public function getJob(Request $request)
    {

        $jobs = JobPosition::where("department_id", $request->department_id)
            ->pluck("job_position_name", "id");
        return response()->json($jobs);
    }

    public function getDistrict(Request $request)
    {

        // $dists = District::where("division_id", $request->division_id)
        //     ->pluck("district_name", "id");
        $dists = DB::table('addresses')->where('parent_code', $request->division_id)->get();

        return response()->json($dists);
    }
    public function getCity(Request $request)
    {

        $cits = City::where("division_id", $request->division_id)
            ->pluck("city_name", "id");
        return response()->json($cits);
    }

    public function getTownship(Request $request)
    {

        // $towns = Township::where("district_id", $request->district_id)
        //     ->pluck("township_name", "id");
        $towns = DB::table('addresses')->where('parent_code', $request->district_id)->get();
        return response()->json($towns);
    }
    public function getProvince(Request $request)
    {

        $provs = Province::where("district_id", $request->district_id)
            ->pluck("province_name", "id");
        return response()->json($provs);
    }

    public function getQuarter(Request $request)
    {

        // $quats = Quarter::where("town_id", $request->village_id)
        //     ->pluck("quarter_name", "id");
        $quats = DB::table('addresses')->where('parent_code', $request->village_id)->get();
        return response()->json($quats);
    }
    public function getVillage(Request $request)
    {

        // $vills = Village::where("township_province_id", $request->township_id)
        //     ->pluck("village_name", "id");
        $vills = DB::table('addresses')->where('parent_code', $request->township_id)->get();
        return response()->json($vills);
    }

    public function getCenter(Request $request)
    {
        $centers = Center::where("branch_id", $request->branch_id)
            ->pluck("center_uniquekey");
        return response()->json($centers);
    }

    public function getGroupp(Request $request)
    {
        $groups = Group::where("center_id", $request->get('center'))
            ->distinct()
            ->pluck("group_uniquekey");
        $main_group = DB::table('tbl_group')
            ->leftjoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', 'tbl_group.group_uniquekey')
            ->whereIn('tbl_group.group_uniquekey', $groups)
            ->orderby('tbl_main_join_group.main_group_code', 'asc')
            ->get();
        return response()->json($main_group);
    }
    public function getLO(Request $request)
    {
        $staff_id = DB::table('tbl_center')->where('center_uniquekey', $request->get('center'))->first()->staff_id;
        // ->distinct()
        // ->pluck("staff_id");
        $staff = DB::table('tbl_staff')->where('staff_code', $staff_id)->first();
        return response()->json($staff);
    }


    public function getStaff(Request $request)
    {
        if ($request->branch_id) {
            $staffs = Staff::where("branch_id", $request->branch_id)
                ->pluck("name", "staff_code");
            return response()->json($staffs);
        }
        $staffs = DB::table('tbl_center')
            // ->leftJoin('tbl_staff', 'tbl_staff.staff_code', '=', 'tbl_center.staff_id')
            // ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', 'tbl_center.staff_client_id')
            ->leftJoin('tbl_staff', 'tbl_staff.staff_code', '=', 'tbl_center.staff_id')
            ->where("tbl_center.center_uniquekey", $request->center_id)
            ->select("tbl_staff.name", "tbl_staff.staff_code")
            ->get();
        return response()->json($staffs);
    }

    public function getCenterLeaders(Request $request)
    {
        if ($request->filled('search_leader_list')) {
            // Search method
            $searchvalue = $request->search_leader_list;
            $data = DB::table('tbl_client_join')
                ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', 'tbl_client_join.client_id')
                ->leftJoin('tbl_center', 'tbl_center.center_uniquekey', 'tbl_client_join.center_id')
                ->leftJoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', 'tbl_client_join.center_id')
                ->leftJoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', 'tbl_client_join.client_id')
                ->where('tbl_client_join.you_are_a_center_leader', '=', 'yes')
                ->where('tbl_client_join.branch_id', '=', DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->where('tbl_client_join.client_id', 'LIKE', $searchvalue)
                ->orWhere('tbl_client_basic_info.name', 'LIKE', $searchvalue)
                ->orWhere('tbl_center.center_uniquekey', 'LIKE', $searchvalue)
                ->orWhere('tbl_main_join_client.main_client_code', 'LIKE', $searchvalue)
                ->orWhere('tbl_main_join_center.main_center_code', 'LIKE', $searchvalue)
                ->paginate(10);

            //         	if($search->you_are_a_center_leader =='yes'){
            //             	$data=$search;
            //             }else{

            //             }
        } else {
            $data = DB::table('tbl_client_join')
                ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', 'tbl_client_join.client_id')
                ->leftJoin('tbl_center', 'tbl_center.center_uniquekey', 'tbl_client_join.center_id')
                ->leftJoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', 'tbl_client_join.center_id')
                ->leftJoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', 'tbl_client_join.client_id')
                ->where('tbl_client_join.you_are_a_center_leader', '=', 'yes')
                ->where('tbl_client_join.branch_id', '=', DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->paginate(10);
        }

        // dd($data);
        return view('client.dataentery.center_leaders.index', compact('data'));
    }

    public function liveReverseClient($portal_client_id)
    {
        //live crawl start
        $clients = DB::table('tbl_client_basic_info')
            ->leftjoin('tbl_client_family_info', 'tbl_client_family_info.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftjoin('tbl_client_job_main', 'tbl_client_job_main.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftjoin('tbl_client_photo', 'tbl_client_photo.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftjoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftjoin('tbl_client_survey_ownership', 'tbl_client_join.client_id', '=', 'tbl_client_survey_ownership.client_uniquekey')
            // ->leftJoin('tbl_group', function ($advancedLeftJoin) {
            //     $advancedLeftJoin->on('tbl_client_join.group_id', '=', 'tbl_group.group_uniquekey');
            // })
            // ->leftJoin('tbl_center', function ($advancedLeftJoin) {
            //     $advancedLeftJoin->on('tbl_client_join.center_id', '=', 'tbl_center.center_uniquekey');
            // })
            ->leftJoin('tbl_educations', function ($advancedLeftJoin) {
                $advancedLeftJoin->on('tbl_client_basic_info.education_id', '=', 'tbl_educations.id');
            })
            ->leftJoin('tbl_branches', function ($advancedLeftJoin) {
                $advancedLeftJoin->on('tbl_branches.id', '=', 'tbl_client_join.branch_id'); //edited
            })
            ->leftJoin('tbl_center', 'tbl_center.staff_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftJoin('tbl_group', 'tbl_group.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
            ->where('tbl_client_basic_info.client_uniquekey', $portal_client_id)
            ->select('tbl_group.client_id AS group_leader_id', 'tbl_client_basic_info.*', 'tbl_client_basic_info.created_at AS Ccreated_at', 'tbl_client_basic_info.updated_at AS Cupdated_at', 'tbl_client_basic_info.phone_primary AS client_phone', 'tbl_client_family_info.*', 'tbl_client_job_main.*', 'tbl_client_photo.*', 'tbl_client_join.*', 'tbl_client_survey_ownership.*', 'tbl_educations.*', 'tbl_branches.branch_name AS branch_name', 'tbl_center.staff_client_id')
            ->first();
        // dd($clients);
        //client_type
        if ($clients->client_status == 'new') {
            $customer_group_id = "1";
        } elseif ($clients->client_status == 'active') {
            $customer_group_id = "1";
        } elseif ($clients->client_status == 'dropout') {
            $customer_group_id = "3";
        } elseif ($clients->client_status == 'dead') {
            $customer_group_id = "2";
        } elseif ($clients->client_status == 'rejoin') {
            $customer_group_id = "4";
        } elseif ($clients->client_status == 'substitute') {
            $customer_group_id = "5";
        } elseif ($clients->client_status == 'dormant') {
            $customer_group_id = "13";
        } elseif ($clients->client_status == 'reject') {
            $customer_group_id = "13";
        }
        // center
        $centerid = DB::table('tbl_main_join_center')->where('portal_center_id', $clients->center_id)->first();
        if ($centerid) {
            $centerid = $centerid->main_center_id;
        }
        $groupid = DB::table('tbl_main_join_group')->where('portal_group_id', $clients->group_id)->first();
        if ($groupid) {
            $groupid = $groupid->main_group_id;
        }
        $house_number=explode(",",$clients->address_primary);
        $house_number=$house_number[0];


        // latest clients id generate
        $dataclients = DB::connection('main')->table('clients')->where('branch_id', $clients->branch_id)->orderBy('client_number', 'desc')->first();
        $main_client_generate_id = $dataclients->client_number;
        $main_client_generate_id = ++$main_client_generate_id;

        DB::connection('main')->table('clients')->insert([
            'branch_id' => $clients->branch_id,
            'name' => $clients->name,
            'name_other' => $clients->name_mm == null ? $clients->name : $clients->name_mm,
            // 'center_code' => $centerid, // change center
            // 'center_name' => $clients[$i]->branch_name, // change later
            'register_date' => date('Y-m-d', strtotime($clients->registration_date)),
            'client_number' => $main_client_generate_id,
            'customer_group_id' => $customer_group_id, //change group
            'gender' => $clients->gender,
            'dob' => date('Y-m-d', strtotime($clients->dob)),
            'education' => $clients->education_name,
            'primary_phone_number' => $clients->client_phone,
            'alternate_phone_number' => $clients->phone_secondary,
            'nrc_number' => $clients->nrc == null ? $clients->old_nrc : $clients->nrc,
            'nrc_type' => $clients->nrc == null ? 'Old Format' : 'New Format',
            'marital_status' => $clients->marital_status,
            'status' => 'Active',
            'father_name' => $clients->father_name,
            'husband_name' => $clients->spouse_name,
            'occupation_of_husband' => $clients->occupation_of_spouse,
            'no_children_in_family' => $clients->no_of_children == null ? 0 : $clients->no_of_children,
            'no_of_working_people' => $clients->no_of_working_family == null ? 0 : $clients->no_of_working_family,
            'no_of_dependent' => $clients->no_of_working_progeny == null ? 0 : $clients->no_of_working_progeny,
            'no_of_person_in_family' => $clients->no_of_family == null ? 0 : $clients->no_of_family,
            'address1' => $clients->address_primary,
            'address2' => $clients->address_secondary,
            'family_registration_copy' => $clients->registration_family_form_front,
            'photo_of_client' => $clients->client_photo,
            'nrc_photo' => $clients->recognition_front,
            'scan_finger_print' => $clients->finger_print_bio,
            'survey_id' => $clients->id,
            'remark' => 'active',
            'center_leader_id' => $centerid,
            // 'loan_officer_id'=>,
            'you_are_a_group_leader' => $clients->you_are_a_group_leader,
            'you_are_a_center_leader' => $clients->you_are_a_center_leader,
            //'ownership_of_farmland'=>$clients[$i]->land,
            //'ownership'=>$clients[$i]->ownership,
            'province_id' => $clients->division_id,
            'district_id' => $clients->district_id,
            'commune_id' => $clients->township_id,
            'village_id' => $clients->village_id,
            'ward_id' => $clients->quarter_id,
            'house_number'=> $house_number,
            'loan_officer_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $clients->staff_id)->first()->main_staff_id,
            'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $clients->staff_id)->first()->main_staff_id,
            'updated_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $clients->staff_id)->first()->main_staff_id,
            'created_at' => date('Y-m-d H:i:s', strtotime($clients->Ccreated_at)),
            'updated_at' => date('Y-m-d H:i:s', strtotime($clients->Cupdated_at)),
            'company_address1' => $clients->company_address,
        ]);
        $main_client_id = DB::connection('main')->table('clients')->where('client_number', $main_client_generate_id)->first()->id;
        $job = DB::table('tbl_client_job_main')->where('client_uniquekey', $clients->client_uniquekey)
            ->leftJoin('tbl_industry_type', 'tbl_industry_type.id', 'tbl_client_job_main.industry_id')
            ->leftJoin('tbl_job_position', 'tbl_job_position.id', 'tbl_client_job_main.job_position_id')
            ->leftJoin('tbl_job_department', 'tbl_job_department.id', 'tbl_client_job_main.department_id')
            ->select(
                'tbl_client_job_main.*',
                'tbl_industry_type.industry_name AS employment_industry',
                'tbl_job_position.job_position_name AS position',
                'tbl_job_department.department_name AS department'

            )->first();
        if ($job->job_status == 'Has Job') {
            $employment_status = 'Active';
        } else {
            $employment_status = 'Inactive';
        }
        if(DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id',$clients->guarantors_id)->first()){
            $main_guarantor_id=DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id',$clients->guarantors_id)->first()->main_guarantor_id;
            //insert client_guarantor into main
            DB::connection('main')->table('client_guarantor')->insert([
                'client_id'=>$main_client_id,
                'guarantor_id'=>$main_guarantor_id,
                'created_at' => date('Y-m-d H:i:s', strtotime($clients->Ccreated_at)),
                'updated_at' => date('Y-m-d H:i:s', strtotime($clients->Cupdated_at)),
            ]);
        }
        DB::connection('main')->table('employee_status')->insert([
            'client_id' => $main_client_id,
            'position' => $job->position,
            'employment_status' => $employment_status,
            'employment_industry' => strtolower($job->employment_industry),
            'senior_level' => null,
            'branch' => $clients->branch_name,
            'company_name' => $job->company_name,
            'department' => $job->department,
            'work_phone' => $job->company_phone,
            'work_phone2' => $job->company_phone,
            'working_experience' => $job->experience,
            'basic_salary' => $job->salary,
            'company_address' => $job->company_address

        ]);

        DB::table('tbl_main_join_client')->insert([
            'main_client_id' => $main_client_id,
            'portal_client_id' => $clients->client_uniquekey,
            'portal_branch_id' => $clients->branch_id,
            'main_client_code' => $main_client_generate_id
        ]);

        //live crawl end
    }
}
