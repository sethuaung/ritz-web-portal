<?php

namespace App\Http\Controllers\LoanDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Staff;
use App\Models\Loandataenteries\LoanCalendar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LoanCalendarController extends Controller
{
    //Index Page
    public function index(Request $request)
    {
        
            $loancalendars = DB::table('loan_custom_calendar')
                            ->leftjoin('tbl_staff','tbl_staff.id','=','loan_custom_calendar.staff_id')
                            ->select('loan_custom_calendar.*','tbl_staff.name AS staff_name')
                            ->take(31)->get();

           
        
        return view('loan.dataentery.loancalendars.index',compact('loancalendars'));
    }

    //Create Page
    public function create(Request $request)
    {
        //
    }

    public function store(Request $request)
    {
        $validator = $request->validate([
            'day' => ['required','integer','max:255'],
            'day_name' => ['required','string','max:255'],
            'month' => ['required','integer','max:255'],
            'year' => ['required','integer'],
            'status' => ['required','string','max:255'],
        ]);

        if($validator){
            $loan_calender = new LoanCalendar;
            $loan_calender->day = $request->day;
            $loan_calender->day_name = $request->day_name;
            $loan_calender->month = $request->month;
            $loan_calender->year = $request->year;
            $loan_calender->status = $request->status;
            $loan_calender->save();
            
            return redirect()->back()->with("successMsg",'New Loan Calender Date is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    //view Page
    public function show(LoanCalendar $loanCalender)
    {
        //
    }

    //Edit Page
    public function edit(Request $request, $id)
    {
        $input = $request->only(['id', 'title', 'start', 'end']);

        $request_data = [
            'id' => 'required',
            'title' => 'required',
            'start' => 'required',
            'end' => 'required'
        ];

        $validator = Validator::make($input, $request_data);

        

        $loancalendar= LoanCalendar::find($id);
        $staff = Staff::all();
        return view('loan.dataentery.loancalendars.edit',compact('loancalendar','staff'));
    }

    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'staff_id'=>['required','string','max:255'],
            'status' => ['required','string','max:255'],
        ]);

        if($validator){
            $loan_calender =LoanCalendar::find($id);
            $loan_calender->staff_id =$request->staff_id;
            $loan_calender->status = $request->status;
            $loan_calender->save();
            
            return redirect('loancalendars/')->with("successMsg",'Existin Loan Calender is Updated in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

   //Delete
    public function destroy(Request $request)
    {
        //
    }
}
