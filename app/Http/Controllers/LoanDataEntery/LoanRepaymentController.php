<?php

namespace App\Http\Controllers\LoanDataEntery;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ReverseCrawl\RepaymentReverseCrawl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;


class LoanRepaymentController extends Controller
{
    public function getBranchCode()
    {
        $staff_code = Session::get('staff_code');
        // dd($staff_code);
        $B_code = DB::table('tbl_staff')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->select('tbl_branches.branch_code')
            ->where('tbl_staff.staff_code', $staff_code)
            ->first();
        $b_code = strtolower($B_code->branch_code);
        return $b_code;
    }

    public function getLoanSchedule(Request $request)
    {
        $bcode = $this->getBranchCode();

        $loan_data = DB::table($bcode . '_loans')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
            ->leftJoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', $bcode . '_loans.branch_id')
            ->where($bcode . '_loans.loan_unique_id', '=', $request->loan_id)
            ->select($bcode . '_loans.*', 'tbl_client_basic_info.*', 'tbl_staff.name as loan_officer_name', 'tbl_branches.branch_name as branch_name')
            ->first();

        $loan_schedule = DB::table($bcode . '_loans')
            ->leftJoin($bcode . '_loans_schedule', $bcode . '_loans_schedule.loan_unique_id', '=', $bcode . '_loans.loan_unique_id')
            ->leftJoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
            ->where($bcode . '_loans.loan_unique_id', $request->loan_id)
            ->select($bcode . '_loans.*', $bcode . '_loans_schedule.*', $bcode . '_loans_schedule.id as schedule_id', 'loan_type.loan_term_value')
            ->orderBy($bcode . '_loans_schedule.id', 'asc')
            ->distinct()
            ->get();

        $count = DB::table($bcode . '_loans')
            ->leftJoin($bcode . '_loans_schedule', $bcode . '_loans_schedule.loan_unique_id', '=', $bcode . '_loans.loan_unique_id')
            ->where($bcode . '_loans.loan_unique_id', $request->loan_id)
            ->count();
        return response()->json([$loan_data, $loan_schedule, $count]);
    }

    public function genPaymentNum()
    {
        $bcode = $this->getBranchCode();
        $bcode = strtoupper($bcode);
        $randomNum = rand(1, 9999);;
        $num = $bcode;
        $num .= '-P-';
        $num .= str_pad($randomNum, 5, 0, STR_PAD_LEFT);
        $num .= date('d');
        return $num;
    }


    public function create(Request $request)
    {
        $this->permissionFilter("add-loan-repayment");

        $bcode = $this->getBranchCode();

        $i = $request->count;
        $arr = array();
        for ($x = 1; $x <= $i; $x++) {
            $name = "repayment_r" . $x;
            $value = "repayment_r" . $x . "_value";
            if ($request->$name === "checked") {
                array_push($arr, $request->$value);
            }
        }

        $data = DB::table($bcode . '_loans_schedule')
            ->leftJoin($bcode . '_loans', $bcode . '_loans.loan_unique_id', '=', $bcode . '_loans_schedule.loan_unique_id')
            ->whereIn($bcode . '_loans_schedule.id', $arr)
            ->where($bcode . '_loans_schedule.status', 'none')
            ->select($bcode . '_loans_schedule.id AS schedule_id', $bcode . '_loans_schedule.*', $bcode . '_loans.*')
            ->get();

        $is_late = false;
        if (count($data) == 1) {
            $is_late = true;
        }

        if (count($data) == 0) {
            return redirect()->back()->withErrors('Please select at least one schedule');
        }

        $repayment_type = $data[0]->repayment_type;

        $disbursement_id = DB::table($bcode . '_loans')
            ->leftJoin($bcode . '_disbursement', $bcode . '_disbursement.loan_id', '=', $bcode . '_loans.loan_unique_id')
            ->where($bcode . '_loans.loan_unique_id', $data[0]->loan_unique_id)
            ->select($bcode . '_disbursement.disbursement_id')
            ->get();


        $c = count($data);
        $latest = $c - 1;
        $latest_repayment_date = $data[$latest]->month;


        $data_info = DB::table($bcode . '_loans')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
            ->where($bcode . '_loans.loan_unique_id', '=', $request->loan_unique_id)
            ->select($bcode . '_loans.loan_unique_id', 'tbl_client_basic_info.name', 'tbl_client_basic_info.client_uniquekey')
            ->get();

        $this_branch_id = DB::table('tbl_staff')->where('staff_code', '=', Session::get('staff_code'))->select('branch_id')->first();
        $accounts = DB::table('account_chart_externals')->where('branch_id', '=', $this_branch_id->branch_id)->where('external_acc_code', 'LIKE', '1-11' . '%')->get();

        $loanschedule_id = array();
        for ($x = $c - 1; $x >= 0; $x--) {
            array_push($loanschedule_id, $data[$x]->schedule_id);
        }

        $final_amount = DB::table($bcode . '_loans_schedule')
            ->select($bcode . '_loans_schedule.final_amount')
            ->where($bcode . '_loans_schedule.id', '=', $loanschedule_id)
            ->latest()
            ->get();

        $month = DB::table($bcode . '_loans_schedule')
            ->select($bcode . '_loans_schedule.month')
            ->where($bcode . '_loans_schedule.id', '=', $loanschedule_id[count($loanschedule_id) - 1])
            ->first();

        // dd($month->month);

        $late_balance = DB::table($bcode . '_repayment_late')
            ->select($bcode . '_repayment_late.principal_balance', $bcode . '_repayment_late.interest_balance', $bcode . '_repayment_late.own_balance')
            ->where($bcode . '_repayment_late.schedule_id', '=', $loanschedule_id)
            // ->latest()
            ->get();
        // dd($late_balance);
        $latestnum = count($late_balance) - 1;
        // dd($late_balance[$latestnum]);

        if (isset($late_balance[$latestnum]->principal_balance, $late_balance[$latestnum]->interest_balance, $late_balance[$latestnum]->own_balance)) {
            $total_interest_to_pay = $late_balance[$latestnum]->interest_balance;
            $total_principal = $late_balance[$latestnum]->principal_balance;
            $total_amount_to_pay = $late_balance[$latestnum]->own_balance;
        } else {
            $total_amount_to_pay = 0;
            $total_interest_to_pay = 0;
            $total_principal = 0;
            for ($x = $c - 1; $x >= 0; $x--) {

                $value1 = $data[$x]->capital + $data[$x]->interest;
                $total_amount_to_pay += $value1;

                $value2 = $data[$x]->interest;
                // dd($data);
                $total_interest_to_pay += $value2;

                $principal = $data[$x]->capital;
                $total_principal += $principal;
            }
        }
        $principal_balance = $final_amount[0]->final_amount;

        $payment_num = $this->genPaymentNum();
        $schedule_pay_date = DB::table($bcode . '_loans_schedule')->where('id', $loanschedule_id)->first()->month;

        return view('loan.dataentery.loanrepayment.create', compact('schedule_pay_date', 'loanschedule_id', 'month', 'is_late', 'total_amount_to_pay', 'total_interest_to_pay', 'total_principal', 'principal_balance', 'data_info', 'disbursement_id', 'latest_repayment_date', 'accounts', 'repayment_type', 'payment_num'));
    }


    public function store(Request $request)
    {
        // dd($request);
        $this->permissionFilter("add-loan-repayment");

        $bcode = $this->getBranchCode();

        $initial_amount = DB::table($bcode . '_loans_schedule')
            ->where('id', $request->schedule_id)
            ->select('capital', 'interest')
            ->get();
        // dd($request);

        $initial_principal = $initial_amount[0]->capital;
        $initial_interest = $initial_amount[0]->interest;
        $initial_total_amount = $initial_principal + $initial_interest;

        $validator = $request->validate([
            'payment_date' => ['required', 'date'],
            'repayment_type' => ['required', 'string', 'max:255'],
            'document' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            // 'receipt_no' => ['required'],
        ]);
        if ($validator) {
            // Due Repayment
            if ($request->repayment_type == "due") {
                $destinationPath = 'public/loan_repayments_documents/';
                if ($request->document) {
                    $document_name = time() . '.' . $request->document->getClientOriginalName();
                    $request->document->storeAs($destinationPath, $document_name);
                } else {
                    $document_name = NULL;
                }

                foreach ($request->schedule_id as $scheduleid) {
                    $acc_code = [$request->cash_acc_id];

                    for ($i = 0; $i < count($acc_code); $i++) {
                        DB::table($bcode . '_repayment_due')->insert([
                            'schedule_id' =>  $scheduleid,
                            'disbursement_id' => $request->loan_id,
                            'cash_acc_id' => DB::table('account_chart_external_details')->where('external_acc_id', $acc_code[$i])->first()->id,
                            'payment_number' => $request->payment_num,
                            'receipt_no' => $request->receipt_no,
                            'principal' => $request->principal,
                            'interest' => $request->interest,
                            'penalty' => $request->penalty_amount,
                            'total_balance' => $request->total_balance,
                            'final_paid_balance' => $request->paid_amount,
                            'own_balance' => $request->owed_balance,
                            'over_days' => $request->over_days,
                            'documents' => $document_name,
                            'payment_method' => $request->payment_method,
                            'repayment_status' => "paid",
                            'payment_date' => $request->payment_date,
                            'counter_name' => session()->get('staff_code')
                        ]);
                    }

                    $this->liveReverseRepayment($request, $scheduleid);

                }

                DB::table($bcode . '_loans_schedule')
                    ->whereIn($bcode . '_loans_schedule.id', $request->schedule_id)
                    ->update([
                        $bcode . '_loans_schedule.status' => "paid",
                        $bcode . '_loans_schedule.repayment_type' => "due",
                        $bcode . '_loans_schedule.repayment_date' => $request->payment_date,
                    ]);

                if ($request->principal_balance == 0) {
                    DB::table($bcode . '_loans')
                        ->where('loan_unique_id', $request->loan_id)
                        ->update([
                            'disbursement_status' => "Closed"
                        ]);
                }
            }
            // Late Repayment
            if ($request->repayment_type == "late") {

                $late_balance = DB::table($bcode . '_repayment_late')
                    ->select($bcode . '_repayment_late.principal_balance', $bcode . '_repayment_late.interest_balance', $bcode . '_repayment_late.own_balance')
                    ->where($bcode . '_repayment_late.schedule_id', '=', $request->schedule_id[0])
                    ->latest($bcode . '_repayment_late.schedule_id')
                    ->get();

                if (count($late_balance) != 0) {
                    $latestnum = count($late_balance) - 1;
                    $late_balance[$latestnum]->principal_balance;
                    $princ_bal = $late_balance[$latestnum]->principal_balance - $request->principal;
                    if ($princ_bal < 0) {
                        $princ_bal = 0;
                    }

                    $int_bal = $late_balance[$latestnum]->interest_balance - $request->interest;
                    if ($int_bal < 0) {
                        $int_bal = 0;
                    }

                    $total_bal = $late_balance[$latestnum]->own_balance - $request->payment;
                    if ($total_bal < 0) {
                        $total_bal = 0;
                    }
                } else {
                    $princ_bal = $initial_principal - $request->principal;
                    $int_bal = $initial_interest - $request->interest;
                    $total_bal = $initial_total_amount - $request->payment;
                }

                $destinationPath = 'public/loan_repayments_documents/';
                if ($request->document) {
                    $document_name = time() . '.' . $request->document->getClientOriginalName();
                    $request->document->storeAs($destinationPath, $document_name);
                } else {
                    $document_name = NULL;
                }


                foreach ($request->schedule_id as $scheduleid) {
                    DB::table($bcode . '_loans_schedule')
                        ->where($bcode . '_loans_schedule.id', $scheduleid)
                        ->update([
                            $bcode . '_loans_schedule.repayment_type' => "late",
                            $bcode . '_loans_schedule.repayment_date' => $request->payment_date,
                        ]);
                }
                $acc_code = [$request->cash_acc_id];

                for ($i = 0; $i < count($acc_code); $i++) {
                    DB::table($bcode . '_repayment_late')->insert([
                        'disbursement_id' => $request->loan_id,
                        'schedule_id' => $request->schedule_id[0],
                        'cash_acc_id' => DB::table('account_chart_external_details')->where('external_acc_id', $acc_code[$i])->first()->id,
                        'payment_number' => $request->payment_num,
                        'receipt_no' => $request->receipt_no,
                        'principal' => $initial_principal,
                        'interest' => $initial_interest,
                        'penalty' => $request->penalty_amount,
                        'total_amount' => $initial_total_amount,
                        'principal_paid' => $request->principal,
                        'interest_paid' => $request->interest,
                        'total_amount_paid' => $request->payment,
                        'principal_balance' => $princ_bal,
                        'interest_balance' => $int_bal,
                        'own_balance' => $total_bal,
                        'over_days' => $request->over_days,
                        'documents' => $document_name,
                        'payment_method' => $request->payment_method,
                        'payment_date' => $request->payment_date,
                        'counter_name' => session()->get('staff_code')
                    ]);
                    
                    $this->liveReverseRepayment($request, $scheduleid);
                }

                // Late repayment complete with no outstanding
                if ($request->outstanding == 0) {
                    DB::table($bcode . '_loans_schedule')
                        ->whereIn($bcode . '_loans_schedule.id', $request->schedule_id)
                        ->update([
                            $bcode . '_loans_schedule.status' => "paid",
                            $bcode . '_loans_schedule.repayment_type' => "late",
                            $bcode . '_loans_schedule.repayment_date' => $request->payment_date,
                        ]);
                    DB::table($bcode . '_repayment_late')
                        ->where($bcode . '_repayment_late.disbursement_id', $request->loan_id)
                        ->update([
                            $bcode . '_repayment_late.repayment_status' => "paid",
                        ]);
                    if ($request->principal_balance == 0 && $request->outstanding == 0) {
                        DB::table($bcode . '_loans')
                            ->where('loan_unique_id', $request->loan_id)
                            ->update([
                                'disbursement_status' => "Closed"
                            ]);
                    }
                }
                // Late repayment not complete yet with loan outstanding
                else {

                    // DB::table('loans_schedule')
                    // ->whereIn('loans_schedule.id',$request->schedule_id)
                    // ->update([
                    //     'loans_schedule.principal_balance'=>$initial_principal - $request->principal,
                    //     'loans_schedule.interest_balance'=>$initial_interest - $request->interest,
                    //     'loans_schedule.total_balance'=>$initial_total_amount - $request->payment
                    // ]);
                }
                if ($request->principal_balance == 0 && $request->outstanding == 0) {
                    DB::table($bcode . '_loans')
                        ->where('loan_unique_id', $request->loan_id)
                        ->update([
                            'disbursement_status' => "Closed"
                        ]);
                }
            }

            // Pre Repayment
            if ($request->repayment_type == "pre") {
                if ($request->payment == ($request->principal + $request->interest)) {
                    $destinationPath = 'public/loan_repayments_documents/';
                    if ($request->document) {
                        $document_name = time() . '.' . $request->document->getClientOriginalName();
                        $request->document->storeAs($destinationPath, $document_name);
                    } else {
                        $document_name = NULL;
                    }

                    foreach ($request->schedule_id as $scheduleid) {
                        $acc_code = [$request->cash_acc_id];

                        for ($i = 0; $i < count($acc_code); $i++) {
                            DB::table($bcode . '_repayment_pre')->insert([

                                'schedule_id' => $scheduleid,
                                'disbursement_id' => $request->loan_id,
                                'cash_acc_id' => DB::table('account_chart_external_details')->where('external_acc_id', $acc_code[$i])->first()->id,
                                'payment_number' => $request->payment_num,
                                'receipt_no' => $request->receipt_no,
                                'principal' => $request->principal / count($request->schedule_id),
                                'interest' => round($request->interest / count($request->schedule_id)),
                                'penalty' => $request->penalty_amount,
                                'total_amount' => round(($request->principal / count($request->schedule_id)) + ($request->interest / count($request->schedule_id))),
                                'principal_paid' => $request->principal / count($request->schedule_id),
                                'interest_paid' => round($request->interest / count($request->schedule_id)),
                                'total_amount_paid' => round(($request->principal / count($request->schedule_id)) + ($request->interest / count($request->schedule_id))),
                                'principal_balance' => DB::table($bcode . '_loans_schedule')->where('id', $scheduleid)->first()->final_amount,
                                'own_balance' => $request->outstanding,
                                'over_days' => $request->over_days,
                                'documents' => $document_name,
                                'payment_method' => $request->payment_method,
                                'repayment_status' => "paid",
                                'payment_date' => $request->payment_date,
                                'counter_name' => session()->get('staff_code')
                            ]);
                            
                            $this->liveReverseRepayment($request, $scheduleid);
                        }

                        DB::table($bcode . '_loans_schedule')
                            ->where($bcode . '_loans_schedule.id', $scheduleid)
                            ->update([
                                $bcode . '_loans_schedule.interest' => round($request->interest / count($request->schedule_id)),
                                $bcode . '_loans_schedule.status' => "paid",
                                $bcode . '_loans_schedule.repayment_type' => "pre",
                                $bcode . '_loans_schedule.repayment_date' => $request->payment_date,
                            ]);

                    }


                    if ($request->principal_balance == 0) {
                        DB::table($bcode . '_loans')
                            ->where('loan_unique_id', $request->loan_id)
                            ->update([
                                'disbursement_status' => "Closed"
                            ]);
                    }
                } else {
                    DB::table($bcode . '_loans_schedule')
                        ->whereIn($bcode . '_loans_schedule.id', $request->schedule_id)
                        ->update([
                            $bcode . '_loans_schedule.status' => "none",
                            $bcode . '_loans_schedule.repayment_date' => $request->payment_date,
                        ]);
                }
            }




            return redirect('loan?disbursement_status=Activated')->with("successMsg", 'Repayment success');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }

    public function liveReverseRepayment($repayDetail, $sid)
    {
        $bcode = $this->getBranchCode();
        $route = $this->getMainUrl37();
        $route_name = 'create-repayment';
        $routeurl = $route . $route_name;
        $authorization = $this->loginToken();

        $branch_id = DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id;
        $loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $repayDetail->loan_id)->first()->main_loan_id;
        $clientDetail = DB::table('tbl_main_join_client')->where('portal_client_id', $repayDetail->client_id)->first();
// dd($sid);
        // foreach ($repayDetail->schedule_id as $sid) {
        if (DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $sid)->first()) {
            $main_schedule_id = DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $sid)->where('branch_id', $branch_id)->first()->main_disbursement_schedule_id;
            // dd($main_schedule_id);

            $client = new \GuzzleHttp\Client(['verify' => false]);
            $result = $client->post($routeurl, [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Authorization' => 'Bearer ' . $authorization,
                ],
                'form_params' => [
                    'disbursement_id' => $loan_id,
                    'payment_number' => $repayDetail->payment_num,
                    'disbursement_detail_id' =>  $main_schedule_id,
                    'receipt_no' => $repayDetail->payment_num,
                    'client_id' => $clientDetail->main_client_id,
                    'client_number' => $clientDetail->main_client_code,
                    'client_name' => $repayDetail->client_name,
                    'over_days' => $repayDetail->over_days == NULL ? 0 : $repayDetail->over_days,
                    'penalty_amount' => $repayDetail->penalty_amount == NULL ? 0 : $repayDetail->penalty_amount,
                    'principle' => $repayDetail->principal / count($repayDetail->schedule_id),
                    'interest' =>  $repayDetail->interest / count($repayDetail->schedule_id),
                    'compulsory_saving' => 0,
                    'owed_balance' => $repayDetail->outstanding,
                    'principle_balance' => $repayDetail->principal_balance,
                    'other_payment' => 0,
                    'total_payment' => $repayDetail->payment,
                    'cash_acc_id' => DB::table('account_chart_external_details')->where('external_acc_id', $repayDetail->cash_acc_id)->first()->main_acc_id,
                    'payment' => $repayDetail->payment,
                    'payment_date' => $repayDetail->payment_date,
                    'payment_method' => $repayDetail->payment_method,
                    'pre_repayment' => $repayDetail->repayment_type == 'pre' ? 1 : 0,
                    'late_repayment' => $repayDetail->repayment_type == 'late' ? 1 : 0,
                    'branch_id' => $branch_id,
                    'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', session()->get('staff_code'))->first()->main_staff_id,
                    'updated_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', session()->get('staff_code'))->first()->main_staff_id,
                ]
            ]);

            if ($repayDetail->repayment_type == 'late' && $repayDetail->outstanding == 0) {
                $client = new \GuzzleHttp\Client(['verify' => false]);
                $result = $client->post($routeurl, [
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded',
                        'Authorization' => 'Bearer ' . $authorization,
                    ],
                    'form_params' => [
                        'disbursement_id' => $loan_id,
                        'payment_number' => $repayDetail->payment_num,
                        'disbursement_detail_id' =>  $main_schedule_id,
                        'receipt_no' => $repayDetail->payment_num,
                        'client_id' => $clientDetail->main_client_id,
                        'client_number' => $clientDetail->main_client_code,
                        'client_name' => $repayDetail->client_name,
                        'over_days' => $repayDetail->over_days == NULL ? 0 : $repayDetail->over_days,
                        'penalty_amount' => $repayDetail->penalty_amount == NULL ? 0 : $repayDetail->penalty_amount,
                        'principle' => 0,
                        'interest' =>  0,
                        'compulsory_saving' => 0,
                        'owed_balance' => 0,
                        'principle_balance' => 0,
                        'other_payment' => 0,
                        'total_payment' => 0,
                        'cash_acc_id' => DB::table('account_chart_external_details')->where('external_acc_id', $repayDetail->cash_acc_id)->first()->main_acc_id,
                        'payment' => 0,
                        'payment_date' => $repayDetail->payment_date,
                        'payment_method' => $repayDetail->payment_method,
                        'pre_repayment' => 0,
                        'late_repayment' => 0,
                        'branch_id' => $branch_id,
                        'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', session()->get('staff_code'))->first()->main_staff_id,
                        'updated_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', session()->get('staff_code'))->first()->main_staff_id,
                    ]
                ]);

                DB::table('tbl_main_join_repayment')->insert([
                    'portal_repayment_id' => DB::table($bcode . '_repayment_late')->orderBy('repayment_id', 'desc')->first()->repayment_id,
                    'portal_schedule_id' => $sid,
                    'main_owed_balance' => $repayDetail->outstanding,
                    'main_repayment_disbursement_detail_id' => $main_schedule_id,
                ]);
            } else if ($repayDetail->repayment_type == 'late') {
                DB::table('tbl_main_join_repayment')->insert([
                    'portal_repayment_id' => DB::table($bcode . '_repayment_late')->orderBy('repayment_id', 'desc')->first()->repayment_id,
                    'portal_schedule_id' => $sid,
                    'main_owed_balance' => $repayDetail->outstanding,
                    'main_repayment_disbursement_detail_id' => $main_schedule_id,
                ]);
            }
        }
    }


    public function getRepaymentDate(Request $request)
    {
        $bcode = $this->getBranchCode();
        $dueDate = DB::table($bcode . '_repayment_due')->where('schedule_id', $request->schedule_id)->orderBy('repayment_id', 'desc')->first();
        $lateDate = DB::table($bcode . '_repayment_late')->where('schedule_id', $request->schedule_id)->orderBy('repayment_id', 'desc')->first();
        $preDate = DB::table($bcode . '_repayment_pre')->where('schedule_id', $request->schedule_id)->orderBy('repayment_id', 'desc')->first();

        $paymentDate = '';
        if ($dueDate) {
            $paymentDate = date('d-m-Y', strtotime($dueDate->payment_date));
        } else if ($lateDate) {
            $paymentDate = date('d-m-Y', strtotime($lateDate->payment_date));
        } else if ($preDate) {
            $paymentDate = date('d-m-Y', strtotime($preDate->payment_date));
        } else {
            $paymentDate = 'no data';
        }

        return response()->json($paymentDate);
    }
}
