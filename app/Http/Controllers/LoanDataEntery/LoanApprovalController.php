<?php

namespace App\Http\Controllers\LoanDataEntery;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Loan\LoanController;
use App\Models\Loans\LoanSchedule;



class LoanApprovalController extends Controller
{
    public function getBranchCode()
    {
        $staff_code = Session::get('staff_code');
        $B_code = DB::table('tbl_staff')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->select('tbl_branches.branch_code')
            ->where('tbl_staff.staff_code', $staff_code)
            ->first();
        $b_code = strtolower($B_code->branch_code);
        return $b_code;
    }


    public function store(Request $request)
    {
        // dd($request);
        // dd(session()->get('staff_code'));
        $branch_id = DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id;
        $this->permissionFilter("loan-approved");
        $validator = $request->validate([
            'approve_date' => ['required', 'date']
        ]);

        $checkCount = array_count_values($request->all());
        if (array_key_exists('checked', $checkCount)) {
            if ($validator) {
                $bcode = $this->getBranchCode();

                for ($x = 1; $x <= $request->count; $x++) {
                    $name = "check_" . $x;
                    $value = "check_" . $x . "_value";
                    if ($request->$name === "checked") {
                        // dd($request->$value);
                        // dd($request->$value);
                        $charges_check = DB::table($bcode . '_loan_charges_compulsory_join')->where('loan_unique_id', $request->$value)->get();
                        if (count($charges_check) <= 0) {
                            DB::table($bcode . '_loans')
                                ->where($bcode . '_loans.loan_unique_id', '=', $request->$value)
                                ->update([
                                    'approved_date' => $request->approve_date,
                                    'disbursement_status' => "Deposited"

                                ]);
                        } else {
                            DB::table($bcode . '_loans')
                                ->where($bcode . '_loans.loan_unique_id', '=', $request->$value)
                                ->update([
                                    'approved_date' => $request->approve_date,
                                    'disbursement_status' => "Approved"

                                ]);
                        }

                        $this->NormalliveReverseLoanApproval($request->approve_date,$request->$value);

                        //update main join schedule
                        $LoansSchedule = DB::connection('portal')->table($bcode . '_loans_schedule')->where('loan_unique_id', $request->$value)->get();
                        $main_disbursement_id = DB::connection('portal')->table('tbl_main_join_loan')->where('portal_loan_id', $request->$value)->where('branch_id', $branch_id)->first()->main_loan_id;
                        $RpDetail = DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $main_disbursement_id)->get();

                        //delete old main join schedule
                        DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_id', $request->$value)->delete();

                        for ($g = 0; $g < count($LoansSchedule); $g++) {
                            if ($RpDetail[$g]->no == ($g + 1)) {
                                DB::table('tbl_main_join_repayment_schedule')->insert([
                                    'portal_disbursement_id' => $LoansSchedule[$g]->loan_unique_id,
                                    'portal_disbursement_schedule_id' => $LoansSchedule[$g]->id,
                                    'main_disbursement_schedule_id' => $RpDetail[$g]->id,
                                    'branch_id' => $branch_id
                                ]);
                            }
                        }
                    }
                }

                return redirect('/loan?disbursement_status=Approved')->with("successMsg", 'Loan Approval Success!!');
            } else {
                return redirect()->back()->withErrors($validator);
            }
        }
        return redirect()->back()->with('check', 'please check at least one loan');
    }

    public function approveSingle(Request $request)
    {
        // dd($request);
        $bcode = $this->getBranchCode();
        $validator = $request->validate([
            'approvedate' => ['required', 'date']
        ]);
        if ($validator) {
            $charges_check = DB::table($bcode . '_loan_charges_compulsory_join')->where('loan_unique_id', $request->lid)->get();
            if (count($charges_check) <= 0) {
                // DB::table($bcode . '_loans')
                //     ->where($bcode . '_loans.loan_unique_id', '=', $request->$value)
                //     ->update([
                //         'approved_date' => $request->approve_date,
                //         'disbursement_status' => "Deposited"

                //     ]);
                DB::table($bcode . '_loans')->where('loan_unique_id', $request->lid)->update([
                    'first_installment_date' => $request->fdate,
                    'loan_amount' => $request->approveloanamount,
                    'disbursement_status' => "Deposited",
                    'approved_date' => $request->approvedate,
                ]);
            } else {
                // DB::table($bcode . '_loans')
                //     ->where($bcode . '_loans.loan_unique_id', '=', $request->$value)
                //     ->update([
                //         'approved_date' => $request->approve_date,
                //         'disbursement_status' => "Approved"

                //     ]);
                DB::table($bcode . '_loans')->where('loan_unique_id', $request->lid)->update([
                    'first_installment_date' => $request->fdate,
                    'loan_amount' => $request->approveloanamount,
                    'disbursement_status' => "Approved",
                    'approved_date' => $request->approvedate,
                ]);
            }
            // DB::table($bcode . '_loans')->where('loan_unique_id', $request->lid)->update([
            //     'first_installment_date' => $request->fdate,
            //     'loan_amount' => $request->approveloanamount,
            //     'disbursement_status' => "Approved",
            //     'approved_date' => $request->approvedate,
            // ]);
            $disbursement_loan = DB::table($bcode . '_loans')
                ->leftJoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                ->leftJoin('tbl_branches', 'tbl_branches.id', '=', $bcode . '_loans.branch_id')
                ->leftJoin('tbl_center', 'tbl_center.center_uniquekey', '=', $bcode . '_loans.center_id')
                ->leftJoin('tbl_group', 'tbl_group.group_uniquekey', '=', $bcode . '_loans.group_id')
                ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
                ->leftJoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
                ->leftJoin($bcode . '_deposit', $bcode . '_deposit.loan_id', '=', $bcode . '_loans.loan_unique_id')
                ->where($bcode . '_loans.loan_unique_id', '=', $request->lid)
                ->select($bcode . '_loans.*', $bcode . '_deposit.*', 'loan_type.*', 'tbl_client_basic_info.*', 'tbl_client_basic_info.client_uniquekey AS client_id', 'tbl_staff.*')
                ->first();

            //re schedule start
            DB::table($bcode . '_loans_schedule')
                ->where($bcode . '_loans_schedule.loan_unique_id', $request->lid)
                ->delete();

            // Repayment date
            if ($disbursement_loan->loan_term == "Day") {
                $loan_term = 1;
            } else if ($disbursement_loan->loan_term == "Week") {
                $loan_term = 7;
            } else if ($disbursement_loan->loan_term == "Two-Weeks") {
                $loan_term = 14;
            } else if ($disbursement_loan->loan_term == "Four-Weeks") {
                $loan_term = 28;
            } else if ($disbursement_loan->loan_term == "Month") {
                $loan_term = 30;
            } else if ($disbursement_loan->loan_term == "Year") {
                $loan_term = 30;
            }

            if ($disbursement_loan->loan_type_id == 1 or $disbursement_loan->loan_type_id == 2) {
                // GeneralWeekly14D
                $loanschedule = LoanController::fixedLoanMethod(
                    $disbursement_loan->loan_amount,
                    $loan_term,
                    $disbursement_loan->loan_term_value,
                    $disbursement_loan->interest_rate,
                    $disbursement_loan->principal_formula,
                    // $start_date);
                    $request->fdate,
                );
            } elseif ($disbursement_loan->loan_type_id == 3 or $disbursement_loan->loan_type_id == 4) {
                // ExtraLoanWeekly
                $loanschedule = LoanController::principalFinalMethod30D(
                    $disbursement_loan->loan_amount,
                    $loan_term,
                    $disbursement_loan->loan_term_value,
                    $disbursement_loan->interest_rate,
                    $disbursement_loan->principal_formula,
                    // $start_date);
                    $request->fdate,
                );
            } elseif ($disbursement_loan->loan_type_id > 4 && $disbursement_loan->loan_type_id < 20) {
                // 5 to 19
                // ExtraLoanWeekly
                $loanschedule = LoanController::fixedPrincipalMethod30D(
                    $disbursement_loan->loan_amount,
                    $loan_term,
                    $disbursement_loan->loan_term_value,
                    $disbursement_loan->interest_rate,
                    $disbursement_loan->principal_formula,
                    // $start_date);
                    $request->fdate,
                );
            } elseif ($disbursement_loan->loan_type_id > 19 && $disbursement_loan->loan_type_id < 23) {
                // ExtraLoanWeekly
                $loanschedule = LoanController::principalFinalMethod28D(
                    $disbursement_loan->loan_amount,
                    $loan_term,
                    $disbursement_loan->loan_term_value,
                    $disbursement_loan->interest_rate,
                    $disbursement_loan->principal_formula,
                    // $start_date);
                    $request->fdate,
                );
            } elseif ($disbursement_loan->loan_type_id > 22 && $disbursement_loan->loan_type_id < 26) {
                // GeneralMonthly28D
                $loanschedule = LoanController::fixedPrincipalMethod28D(
                    $disbursement_loan->loan_amount,
                    $loan_term,
                    $disbursement_loan->loan_term_value,
                    $disbursement_loan->interest_rate,
                    $disbursement_loan->principal_formula,
                    // $start_date);
                    $request->fdate,
                );
            } elseif ($disbursement_loan->loan_type_id == 26 or $disbursement_loan->loan_type_id == 27) {
                // ExtraLoanWeekly
                $loanschedule = LoanController::fixedPrincipalMethod30D(
                    $disbursement_loan->loan_amount,
                    $loan_term,
                    $disbursement_loan->loan_term_value,
                    $disbursement_loan->interest_rate,
                    $disbursement_loan->principal_formula,
                    // $start_date);
                    $request->fdate,
                );
            }

            $loan_amount = $disbursement_loan->loan_amount;

            foreach ($loanschedule as $schedule) {
                $entrydate = $schedule['date'];
                // $total = $schedule->repay_interest + $schedule->repay_principal;
                $loan_amount -=  $schedule['repay_principal'];

                $loan_schedule = new LoanSchedule;
                $loan_schedule->setTable($bcode . '_loans_schedule');

                $loan_schedule->loan_unique_id = $request->lid;
                $loan_schedule->loan_type_id = $disbursement_loan->loan_type_id;
                $loan_schedule->month = $entrydate;
                $loan_schedule->capital = $schedule['repay_principal'];
                $loan_schedule->interest = $schedule['repay_interest'];
                $loan_schedule->discount = 0;
                $loan_schedule->refund_interest = 1;
                $loan_schedule->refund_amount = 1;
                $loan_schedule->final_amount = $loan_amount;
                $loan_schedule->status = "none";
                $loan_schedule->save();
            }
            //re schedule end

            $this->RescheduleliveReverseLoanApproval($request,$request->lid);

             //update main join schedule
             $LoansSchedule = DB::connection('portal')->table($bcode . '_loans_schedule')->where('loan_unique_id', $request->lid)->get();
             $main_disbursement_id = DB::connection('portal')->table('tbl_main_join_loan')->where('portal_loan_id', $request->lid)->where('branch_id', $disbursement_loan->branch_id)->first()->main_loan_id;
             $RpDetail = DB::connection('main')->table('loan_disbursement_calculate_' . $disbursement_loan->branch_id)->where('disbursement_id', $main_disbursement_id)->get();

             //delete old main join schedule
            DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_id', $request->lid)->delete();
 
             for ($g = 0; $g < count($LoansSchedule); $g++) {
                 if ($RpDetail[$g]->no == ($g + 1)) {
                     DB::table('tbl_main_join_repayment_schedule')->insert([
                         'portal_disbursement_id' => $LoansSchedule[$g]->loan_unique_id,
                         'portal_disbursement_schedule_id' => $LoansSchedule[$g]->id,
                         'main_disbursement_schedule_id' => $RpDetail[$g]->id,
                         'branch_id' => $disbursement_loan->branch_id
                     ]);
                 }
             }

            return redirect('/loan?disbursement_status=Approved')->with("successMsg", 'Loan Approval Success!!');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }


    public function cancel($id)
    {
        // dd($id);
        $bcode = $this->getBranchCode();
        $branch_id = DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id;

        DB::table($bcode . '_loans')
            ->where($bcode . '_loans.id', '=', $id)
            ->update([
                'disbursement_status' => "Canceled"
            ]);

        $main_loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $id)->first()->main_loan_id;

        DB::connection('main')->table('loans_' . $barnch_id)->where('id', $main_loan_id)->update([
            'disbursement_status' => 'Canceled'
        ]);

        return redirect('/loan')->with("successMsg", 'Loan Canceled!!');
    }


    public function NormalliveReverseLoanApproval($approve_date, $portal_loan_id)
    {
        $main_loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $portal_loan_id)->first()->main_loan_id;
        $branch_id = DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id;

        $result = DB::connection('main')->table('loans_' . $branch_id)->where('disbursement_number', $main_loan_id)->update([
            'disbursement_status' => 'Approved',
            'status_note_date_approve' => $approve_date,
            'status_note_approve_by_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', Session::get('staff_code'))->first()->main_staff_id
        ]);

        // $response = (string) $result->getBody();
        // $response = json_decode($response);
        // if ($response->status_code == 200) {
        //     return true;
        // } else {
        //     return false;
        // }
    }

    public function RescheduleliveReverseLoanApproval(Request $request, $loan_id)
    {
        $bcode = $this->getBranchCode();
        $main_loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $loan_id)->first()->main_loan_id;
        $branch_id = DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id;

        $loans=DB::table($bcode.'_loans')->where('loan_unique_id',$loan_id)->first();

        //Approval Condition + Loan Amount & Schedule Change
        if ($loans->disbursement_status == 'Approved' && DB::connection('main')->table('loans_' . $branch_id)->where('disbursement_number', $main_loan_id)->first()->disbursement_status == 'Pending') {
            //check loan amount and first installment date change or not

            $loan_amount_in_main = DB::connection('main')->table('loans_' . $branch_id)->where('disbursement_number', $main_loan_id)->first()->loan_amount;
            $first_ins_date = DB::connection('main')->table('loans_' . $branch_id)->where('disbursement_number', $main_loan_id)->first()->first_installment_date;
            if ($loan_amount_in_main == $loans->loan_amount || $first_ins_date == $loans->first_installment_date) {

                DB::connection('main')->table('loans_' . $branch_id)->where('disbursement_number', $main_loan_id)->update([
                    'disbursement_status' => 'Approved',
                    'status_note_date_approve' => $loans->approved_date,
                    'loan_amount' => $loans->loan_amount,
                    'first_installment_date' => $loans->first_installment_date,
                    'status_note_approve_by_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans->loan_officer_id)->first()->main_staff_id
                ]);
            } else {
                DB::connection('main')->table('loans_' . $branch_id)->where('disbursement_number', $main_loan_id)->update([
                    'disbursement_status' => 'Approved',
                    'status_note_date_approve' => $loans->approved_date,
                    'loan_amount' => $loans->loan_amount,
                    'first_installment_date' => $loans->first_installment_date,
                    'status_note_approve_by_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans->loan_officer_id)->first()->main_staff_id
                ]);
                $portal_schedule = DB::connection('portal')->table($bcode . '_loans_schedule')->where('loan_unique_id', $loans->loan_unique_id)->get();
                $main_schedule = DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $main_loan_id)->get();
                for ($i = 0; $i < count($main_schedule); $i++) {
                    DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $main_loan_id)->where('no',$i+1)->update([
                        'date_s' => date('Y-m-d h:m:s', strtotime($portal_schedule[$i]->month)),
                        'principal_s' => $portal_schedule[$i]->capital,
                        'interest_s' => $portal_schedule[$i]->interest,
                        'total_s' => $portal_schedule[$i]->capital + $portal_schedule[$i]->interest,
                        'balance_s' => $portal_schedule[$i]->final_amount,
                    ]);
                }

            }
        }
    }
}
