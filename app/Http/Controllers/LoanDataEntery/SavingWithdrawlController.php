<?php

namespace App\Http\Controllers\LoanDataEntery;

use DateTime;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\Loandataenteries\SavingWithdrawl;


class SavingWithdrawlController extends Controller
{
    public function getBranchCode()
    {
        $staff_code = Session::get('staff_code');
        $B_code = DB::table('tbl_staff')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->select('tbl_branches.branch_code')
            ->where('tbl_staff.staff_code', $staff_code)
            ->first();
        $b_code = strtolower($B_code->branch_code);
        return $b_code;
    }

   public function create()
    {
        $bcode = $this->getBranchCode();

        $this->permissionFilter("add-withdrawal");
        $staff_code = Session::get('staff_code');
        $B_code = DB::table('tbl_staff')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->select('tbl_branches.id')
            ->where('tbl_staff.staff_code', $staff_code)
            ->first()->id;

    	
   		$all_loan_info=DB::connection('main')->table('loan_compulsory_'.$B_code)->where('compulsory_status','=','Active')->select('loan_id')->get();
   		$saving_active_loans=array();
    	foreach($all_loan_info as $d){
        
        	$portal_info=DB::connection('portal')->table('tbl_main_join_loan')->where('main_loan_id',$d->loan_id)->first();
			if($portal_info){
        	array_push($saving_active_loans,[
            	'portal_loan_id'=>$portal_info->portal_loan_id,
				'main_id'=>$portal_info->main_loan_id,
            	'main_loan_code'=>$portal_info->main_loan_code,
                ]);
            }
    	
        }
   		// dd($saving_active_loans);
   		// $saving_info=array();
   		// for ($i=0; $i < count($saving_active_loans) ; $i++) { 
   		// $all_loan_info=DB::table('tbl_main_join_loan')
   		// ->leftJoin($bcode.'_loans',$bcode.'_loans.loan_unique_id','tbl_main_join_loan.portal_loan_id')
   		// ->where('tbl_main_join_loan.main_loan_id',$saving_active_loans[$i]->loan_id)->get();
   		// }
    	

        // $ids = array_unique($ids);
		// dd($ids);
        // $withdrawl_date = now()->toDateString();
        // $this_branch_id = DB::table('tbl_staff')->where('staff_code', '=', Session::get('staff_code'))->select('branch_id')->first();
        // $accounts = DB::table('account_chart_externals')->where('branch_id', '=', $this_branch_id->branch_id)->where('external_acc_code', 'LIKE', '1-11' . '%')->get();


        return view('loan.dataentery.savingwithdrawl.create', compact('saving_active_loans'));
    }

    public function update(Request $request)
    {
        $this->permissionFilter("add-withdrawal");

        $bcode = $this->getBranchCode();

        $withdrawl_info = DB::table($bcode . '_saving_withdrawl')->where('saving_unique_id', '=', $request->saving_unique_id)->orderBy('id', 'DESC')->first();

        $validator = $request->validate([
            'withdrawl_amount' => ['required'],
            'withdrawl_type' => ['required'],

        ]);

        if ($validator) {

            if ($request->withdrawl_type == "Normal") {
                DB::table($bcode . '_saving_withdrawl')
                    ->where('loan_unique_id', $withdrawl_info->loan_unique_id)
                    ->update([
                        'saving_withdrawl_amount' => $request->total_saving_amount,
                        'interest_withdrawl_amount' => $request->total_interest,
                        'saving_left' => 0,
                        'interest_left' => 0,
                        'withdrawl_type' => $request->withdrawl_type,
                        'status' => "inactive",
                        'date' => $request->withdrawl_date,
                    ]);

                DB::table($bcode . '_saving_withdrawl')
                    ->where('saving_unique_id', $withdrawl_info->saving_unique_id)
                    ->update([
                        'status' => "inactive"
                    ]);
            } else {
                if ($request->withdrawl_amount <= $request->total_saving_amount) {

                    $saving_left = $withdrawl_info->saving_left;

                    if ($saving_left != 0) {
                        $status = 'active';
                    } else {
                        $status = 'inactive';
                        $saving_left = 0;
                    }
                    DB::table($bcode . '_saving_withdrawl')
                        ->where('loan_unique_id', $withdrawl_info->loan_unique_id)
                        ->update([

                            'saving_withdrawl_amount' => $request->withdrawl_amount,
                            'interest_withdrawl_amount' => 0,
                            'saving_left' => $saving_left - $request->withdrawl_amount,
                            'interest_left' => $request->total_interest,
                            'withdrawl_type' => $request->withdrawl_type,
                            'status' => $status,
                            'date' => $request->withdrawl_date,
                        ]);
                } else {
                    return redirect()->back()->withErrors("Withdrawl Amount can't be more than Available Amount");
                }
            }
            return redirect('loan/')->with("successMsg", 'Saving Withdrawl Successful!');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }


    public function getSavingWithdrawlInfo(Request $request)
    {
		$B_code = DB::table('tbl_staff')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->select('tbl_branches.id')
            ->where('tbl_staff.staff_code','=', Session::get('staff_code'))
            ->first()->id;
        
    	
    if ($request->get('loan_id')) {
            $main_loan_id = $request->get('loan_id');
        	// $single_saving_info=DB::connection('main')->table('loan_compulsory_'.$B_code)
        	// ->leftJoin('compulsory_saving_transaction','compulsory_saving_transaction.loan_compulsory_id','loan_compulsory_'.$B_code.'.id')
        	// ->where('loan_compulsory_'.$B_code.'.loan_id',$main_loan_id)
        	// ->get();
    
    		$principle=DB::connection('main')->table('compulsory_saving_transaction')
        	->where('loan_id',$main_loan_id)
            ->where('train_type_ref','=','deposit')
        	->first()->amount;
    		
    
    		$interest=DB::connection('main')->table('compulsory_saving_transaction')
        		->where('loan_id',$main_loan_id)
            	->where('train_type_ref','=','accrue-interest')
        		->get();  
        	$total_interest=0;
        	foreach($interest as $i){
            	$total_interest += $i->amount;
            }
    		$total_saving=$principle + $total_interest;
    
    		if(DB::connection('main')->table('compulsory_saving_transaction')->where('loan_id',$main_loan_id)->where('train_type_ref','=','withdraw')->first()){
    			$withdraw_saving_info=DB::connection('main')->table('compulsory_saving_transaction')
        			->where('loan_id',$main_loan_id)
            		->where('train_type_ref','=','withdraw')
            		->orderBy('id','desc')
        			->first();
            
    			$principle_balance=$withdraw_saving_info->total_principle;
                $interest_balance=$withdraw_saving_info->total_interest;
    			$total_balance=$withdraw_saving_info->available_balance;
				
            	// $principle_withdrawl= $principle - $principle_balance;
            	// $interest_withdrawl=$interest - $interest_balance;
            	// $total_withdrawl= $principle_withdrawl + $interest_withdrawl;
            	$withdraw_total=DB::connection('main')->table('compulsory_saving_transaction')
        			->where('loan_id',$main_loan_id)
            		->where('train_type_ref','=','withdraw')
        			->get();
            	$total_withdrew=0;
            	foreach($withdraw_total as $wt){
                	$total_withdrew += abs($wt->amount);
                }
            	// $principle_withdrawl= $withdraw_saving_info->total_principle;
            	// $interest_withdrawl=$withdraw_saving_info->total_interest;
            	// $total_withdrawl=$withdraw_saving_info->available_balance;
            	
            }else{
    			$principle_balance=$principle;
    			$interest_balance=$total_interest;
    			$total_balance=$principle + $total_interest;
            	
            	$total_withdrew=0;
            	// $principle_withdrawl= 0;
            	// $interest_withdrawl=0;
            	// $total_withdrawl= 0;

            }  
    		// $last_tran=DB::$withdraw_saving_info=DB::connection('main')->table('compulsory_saving_transaction')
    		// ->where('loan_id',$main_loan_id)
    		// ->orderBy('id','desc')
    		// ->first()->tran_date;

            // return response()->json($data);
    		return response()->json(['principle'=>$principle,
                                     'interest'=>$total_interest,
                                     'total_saving'=>$total_saving,
                                     
                                     'total_withdrew'=>$total_withdrew,
                                     // 'principle_withdrawl'=>$principle_withdrawl,
                                     // 'interest_withdrawl'=>$interest_withdrawl,
                                     // 'total_withdrawl'=>$total_withdrawl,

                                     'principle_balance'=>$principle_balance,
                                     'interest_balance'=>$interest_balance,
                                     'total_balance'=>$total_balance,
                                     
                                     // 'tran_date'=>$last_tran
                                    ]);
        }
    }

    public function getSavingClientInfo(Request $request)
    {
        $bcode = $this->getBranchCode();

        if ($request->get('loan_id')) {
        	$main_loan_id = $request->get('loan_id');

			$portal_loan_id=DB::table('tbl_main_join_loan')->where('main_loan_id',$main_loan_id)->first()->portal_loan_id;
        	$client_info=DB::table($bcode.'_loans')
        	->leftJoin('tbl_client_basic_info','tbl_client_basic_info.client_uniquekey',$bcode.'_loans.client_id')
        	->where($bcode.'_loans.loan_unique_id',$portal_loan_id)
        	->first();

            return response()->json($client_info);
        }
    }

    public function getSavingWithdrawlInterest(Request $request)
    {
        $bcode = $this->getBranchCode();
		$B_code = DB::table('tbl_staff')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->select('tbl_branches.id')
            ->where('tbl_staff.staff_code','=', Session::get('staff_code'))
            ->first()->id;
        if ($request->get('loan_id')) {
        	$main_loan_id = $request->get('loan_id');
			
			$interest=DB::connection('main')->table('compulsory_saving_transaction')
        		->where('loan_id',$main_loan_id)
            	->where('train_type_ref','=','accrue-interest')
        		->get();  
        	$total_interest=0;
        	foreach($interest as $i){
            	$total_interest += $i->amount;
            }
        	// $client_id = $request->get('id');
            // $single_saving_info = DB::table($bcode . '_saving_withdrawl')
            //     ->leftJoin($bcode . '_deposit', $bcode . '_deposit.loan_id', '=', $bcode . '_saving_withdrawl.loan_unique_id')
            //     ->where($bcode . '_saving_withdrawl.client_idkey', '=', $client_id)->orderBy($bcode . '_saving_withdrawl.id', 'DESC')->first();
            // $principal_saving = $single_saving_info->saving_left;

            // $record_count = DB::table($bcode . '_saving_withdrawl')->where($bcode . '_saving_withdrawl.saving_unique_id', '=', $client_id)->count();

            // if ($record_count > 1) {
            //     $second_last_record = DB::table($bcode . '_saving_withdrawl')
            //         ->leftJoin($bcode . '_deposit', $bcode . '_deposit.loan_id', '=', $bcode . '_saving_withdrawl.loan_unique_id')
            //         ->where($bcode . '_saving_withdrawl.client_idkey', '=', $client_id)->orderBy('id', 'desc')->skip(1)->take(1)->first();

            //     //No withdrawl with 2 or more loans yet
            //     //from second Addon saving date until now interest calculateion
            //     $deposit_pay_date = $single_saving_info->deposit_pay_date;
            //     $deposit_day = date('d', strtotime($deposit_pay_date)); //25
            //     $start_interest_date = new Carbon($deposit_pay_date); //26
            //     $withdrawl_date = Carbon::now();
            //     $now_day = date('d', strtotime($withdrawl_date)); //28
            //     if ($deposit_day > $now_day) {
            //         $DiffMonths = $start_interest_date->diffInMonths($withdrawl_date);
            //     } else {
            //         $DiffMonths = $start_interest_date->diffInMonths($withdrawl_date) - 1;
            //     }

            //     $onemonth_interest_for_deposit = $principal_saving * (1.25 / 100);
            //     $new_total_interest_to_withdrawl = round($onemonth_interest_for_deposit) * $DiffMonths;

            //     // from first add saving date until second addon calculation interest
            //     $initial_saving = $second_last_record->saving_left;
            //     $initial_deposit_date = $second_last_record->deposit_pay_date;

            //     $initial_deposit_day = date('d', strtotime($initial_deposit_date)); //25
            //     $initial_interest_date = new Carbon($initial_deposit_date); //26

            //     if ($initial_deposit_day > $deposit_day) {
            //         $DiffMonths2 = $initial_interest_date->diffInMonths($start_interest_date);
            //     } else {
            //         $DiffMonths2 = $initial_interest_date->diffInMonths($start_interest_date) - 1;
            //     }
            //     $onemonth_interest_for_initial_deposit = $initial_saving * (1.25 / 100);
            //     $initial_interest_until_second = (round($onemonth_interest_for_initial_deposit) * $DiffMonths2) + $onemonth_interest_for_initial_deposit;
            //     $total_interest_to_withdrawl = $initial_interest_until_second + $new_total_interest_to_withdrawl;
            // 	if($total_interest_to_withdrawl < 0){
            //     	$total_interest_to_withdrawl = 0;
            //     }
            // } else {
            //     $ans = 2;
            //     $deposit_pay_date = $single_saving_info->deposit_pay_date;
            //     $deposit_day = date('d', strtotime($deposit_pay_date));
            //     $start_interest_date = new Carbon($deposit_pay_date); //26
            //     $withdrawl_date = Carbon::now(); //25
            //     $now_day = date('d', strtotime($withdrawl_date));
            //     if ($deposit_day > $now_day) {
            //         $DiffMonths = $start_interest_date->diffInMonths($withdrawl_date);
            //     } else {
            //         $DiffMonths = $start_interest_date->diffInMonths($withdrawl_date) - 1;
            //     }
            //     $onemonth_interest_for_deposit = $principal_saving * (1.25 / 100);
            //     $total_interest_to_withdrawl = round($onemonth_interest_for_deposit) * $DiffMonths;
            // 	if($total_interest_to_withdrawl < 0){
            //     	$total_interest_to_withdrawl = 0;
            //     }
            // }

            // //Second Time withdrawl after principal withdrew
            // if (($single_saving_info->total_saving - $single_saving_info->saving_left) != 0) {
            //     $ans = 3;

            //     $last_withdrawl_date = $single_saving_info->date;
            //     $deposit_day = date('d', strtotime($last_withdrawl_date));
            //     $start_interest_date = new Carbon($last_withdrawl_date);
            //     $withdrawl_date = Carbon::now();
            //     $now_day = date('d', strtotime($withdrawl_date));

            //     if ($deposit_day > $now_day) {
            //         $DiffMonths = $start_interest_date->diffInMonths($withdrawl_date);
            //     } else {
            //         $DiffMonths = $start_interest_date->diffInMonths($withdrawl_date) - 1;
            //     }

            //     $lastmonth_principal = $single_saving_info->saving_withdrawl_amount + $single_saving_info->saving_left;
            //     $lastmonth_interest = $lastmonth_principal * (1.25 / 100);
            //     $onemonth_interest_after_withdrawl = $single_saving_info->saving_left * (1.25 / 100);
            //     $interest_to_withdrawl = round($onemonth_interest_after_withdrawl) * $DiffMonths;
            //     $total_interest_to_withdrawl = $interest_to_withdrawl + $lastmonth_interest + $single_saving_info->interest_left;
            
            // 	if($total_interest_to_withdrawl < 0){
            //     	$total_interest_to_withdrawl = 0;
            //     }
            // }


            return response()->json($total_interest);
        }
    }
}