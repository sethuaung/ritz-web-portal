<?php

namespace App\Http\Controllers\LoanDataEntery;

use App\Http\Controllers\Controller;
use App\Models\Loandataenteries\LoanType;
use Illuminate\Http\Request;

class LoanTypeController extends Controller
{
    //Index Page
    public function index()
    {
        $loantypes = LoanType::Paginate(10);
        return view('loan.dataentery.loantypes.index', compact('loantypes'));
    }

   //Create Page
    public function create()
    {
      	$this->permissionFilter("loan-products");

        return view('loan.dataentery.loantypes.create');
    }

    public function store(Request $request)
    {
      	$this->permissionFilter("loan-products");


        $validator = $request->validate([
            'name' => ['required','string','max:255', 'unique:loan_type'],
            'description' => ['required','string','max:255'],
            'interest_rate' => ['required','integer','max:255'],
            'interest_rate_period' => ['required','string','max:255'],
            'loan_term' => ['required','string','max:255'],
            'status' => ['required','string','max:255'],
        ]);

        if($validator){
            $loan_type = new LoanType;
            $loan_type->name = $request->name;
            $loan_type->description = $request->description;
            $loan_type->interest_rate = $request->interest_rate;
            $loan_type->interest_rate_period = $request->interest_rate_period;
            $loan_type->loan_term = $request->loan_term;
            $loan_type->status = $request->status;
            $loan_type->save();
            
            return redirect('loantypes/')->with("successMsg",'New Loan Type is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }
       

    //Edit Page
    public function edit(LoanType $loantype,$id)
    {
        $loantype = LoanType::find($id);
        return view('loan.dataentery.loantypes.edit',compact('loantype'));
    }

    public function update(Request $request, LoanType $loantype)
    {
        $validator = $request->validate([
            'name' => ['required','string','max:255', 'unique:loan_type'],
            'description' => ['required','string','max:255'],
            'interest_rate' => ['required','integer','max:255'],
            'interest_rate_period' => ['required','string','max:255'],
            'loan_term' => ['required','string','max:255'],
            'status' => ['required','string','max:255'],
        ]);

        if($validator){
            $loantype->update($request->all());
            
            return redirect('loantypes/')->with("successMsg",'New Loan Type is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    //view Page
    public function view(LoanType $loantype,$id)
    {
        $loantype= LoanType::find($id);
        return view('loan.dataentery.loantypes.view',compact('loantype'));
    }

    //Delete
    public function destroy(LoanType $loantype,$id)
    {
        $loantype =LoanType::find($id);
        $loantype -> delete();

        return redirect('loantypes/')->with("successMsg",'Existing Loan Type is Deleted in your data');

    }
}
