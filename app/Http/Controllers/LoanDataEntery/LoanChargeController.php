<?php

namespace App\Http\Controllers\LoanDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Branch;
use App\Models\Loandataenteries\LoanCharge;
use App\Models\Loandataenteries\LoanChargeType;
use App\Models\Loandataenteries\LoanType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoanChargeController extends Controller
{
    //Index Page
    public function index()
    {
        $loancharges = LoanCharge::Paginate(10);
        return view('loan.dataentery.loancharges.index', compact('loancharges'));
    }

    //Create Page
    public function create()
    {
        $branches = Branch::select('branch_name','id')->get();
        $loantypes = LoanType::select('name','id')->get();
        $accounts = DB::table('account_charts')->get();
        return view('loan.dataentery.loancharges.create', compact('branches','loantypes','accounts'));
    }

    public function store(Request $request)
    {
        $this->permissionFilter("service-charges");
        $validator = $request->validate([
            'charge_code' => ['required','string','max:255'],
            'charge_name' => ['required','string','max:255'],
            'charge_description' => ['required','string','max:255'],
            'product_type' => ['required','string','max:255'],
            'amount' => ['required','string','max:255'],
            'charge_option' => ['required','string','max:255'],
            'charge_type' => ['required','string','max:255'],
            'accounting_id' => ['required','string','max:255'],
            'charge' => ['required','string','max:255'],
            'status' => ['required','string','max:255'],
        ]);

        if($validator){
            $loan_charge = new LoanCharge();
            $loan_charge->charge_code = $request->charge_code;
            $loan_charge->charge_name = $request->charge_name;
            $loan_charge->charge_description = $request->charge_description;
            $loan_charge->product_type = $request->product_type;
            $loan_charge->amount = $request->amount;
            $loan_charge->charge_option = $request->charge_option;
            $loan_charge->charge_type = $request->charge_type;
            $loan_charge->accounting_id = $request->accounting_id;
            $loan_charge->charge = $request->charge;
            //$loan_charge->seq = $request->seq;
            $loan_charge->status = $request->status;
            $loan_charge->save();

            //dd($loan_charge->id);
            if($request->charge_code){
                $loan_charge_type = new LoanChargeType();
                $loan_charge_type->charges_id = $loan_charge->charge_code;
                $loan_charge_type->branch_id = $request->branch_id;
                $loan_charge_type->loan_type_id = $request->loan_type_id;
                $loan_charge_type->status = $loan_charge->status;
                $loan_charge_type->save();
            }

            return redirect('loancharges/')->with("successMsg",'New Loan Charge is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }


    //Edit Page
    public function edit($id)
    {
        $this->permissionFilter("service-charges");
        $loancharge = LoanCharge::where('charge_code', $id)->first();
        //$loan_charge_type = LoanChargeType::all();
        //dd($loan_charge_type);
        $loan_charge_type =DB::table('loan_charges_and_type')
            ->leftJoin('tbl_branches','tbl_branches.id','=','loan_charges_and_type.branch_id')
            ->leftJoin('loan_type','loan_type.id','=','loan_charges_and_type.loan_type_id')
            ->where('loan_charges_and_type.charges_id','=',$loancharge->charge_code)
            ->first();
        $account_type = DB::table('loan_charges_type')
            ->leftJoin('account_charts','account_charts.id','=','loan_charges_type.accounting_id')
            ->where('loan_charges_type.charge_code','=',$id)
            ->first();
        $branches = Branch::select('branch_name','id')->get();
        $loantypes = LoanType::select('name','id')->get();
        $accounts = DB::table('account_charts')->get();
        return view('loan.dataentery.loancharges.edit',compact('loancharge','loan_charge_type','branches','loantypes','accounts','account_type'));
    }

    public function update(Request $request,$id)
    {
        $this->permissionFilter("service-charges");
        $validator = $request->validate([
            'charge_code' => ['required','string','max:255'],
            'charge_name' => ['required','string','max:255'],
            'charge_description' => ['required','string','max:255'],
            'product_type' => ['required','string','max:255'],
            'amount' => ['required','string','max:255'],
            'charge_option' => ['required','string','max:255'],
            'charge_type' => ['required','string','max:255'],
            'accounting_id' => ['required','string','max:255'],
            'charge' => ['required','string','max:255'],
            'status' => ['required','string','max:255'],
        ]);

        if($validator){
            $loan_charge = LoanCharge::where('charge_code', $id)->first();

            $loan_charge->charge_code = $request->charge_code;
            $loan_charge->charge_name = $request->charge_name;
            $loan_charge->charge_description = $request->charge_description;
            $loan_charge->product_type = $request->product_type;
            $loan_charge->amount = $request->amount;
            $loan_charge->charge_option = $request->charge_option;
            $loan_charge->charge_type = $request->charge_type;
            $loan_charge->accounting_id = $request->accounting_id;
            $loan_charge->charge = $request->charge;
            //$loan_charge->seq = $request->seq;
            $loan_charge->status = $request->status;
            $loan_charge->save();

            if($request->charge_code){
                $loan_charge_type = LoanChargeType::where("charges_id","=",$loan_charge->charge_code)->first();
                $loan_charge_type->delete();

                $loan_charge_type_new = new LoanChargeType();
                $loan_charge_type_new->charges_id = $loan_charge->charge_code;
                $loan_charge_type_new->branch_id = $request->branch_id;
                $loan_charge_type_new->loan_type_id = $request->loan_type_id;
                $loan_charge_type_new->status = $loan_charge->status;
                $loan_charge_type_new->save();
            }

            return redirect('loancharges/')->with("successMsg",'Update Loan Charge is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    //view Page
    public function view($id)
    {
        $loancharge= LoanCharge::where('charge_code', $id)->first();
//        dd($loancharge);
        // $loan_charge_type =DB::table('loan_charges_and_type')
        //     // ->leftJoin('tbl_branches','tbl_branches.id','=','loan_charges_and_type.branch_id')
        //     // ->leftJoin('loan_type','loan_type.id','=','loan_charges_and_type.loan_type_id')
        //     ->leftJoin('loan_charges_type', 'loan_charges_type.id', '=', 'loan_charges_and_type.charges_id')
        //     ->where('loan_charges_type.charge_code','=',$loancharge->charge_code)
        //     ->first();

        $loan_charge_type =DB::table('loan_charges_type')
            ->where('charge_code','=',$loancharge->charge_code)
            ->first();

    //    dd($loan_charge_type);
        $account_type = DB::table('loan_charges_type')
            ->leftJoin('account_charts','account_charts.id','=','loan_charges_type.accounting_id')
            ->where('loan_charges_type.charge_code','=',$id)
            ->first();
        return view('loan.dataentery.loancharges.view',compact('loancharge','loan_charge_type','account_type'));
    }

    //Delete
    public function destroy($id)
    {
        $this->permissionFilter("service-charges");
        $loancharge =LoanCharge::where('charge_code', $id)->first();
        $loancharge -> delete();
        $loan_charge_type = DB::table('loan_charges_and_type')
            ->leftJoin('loan_charges_type','loan_charges_type.charge_code','=','loan_charges_and_type.charges_id')
            ->where('loan_charges_and_type.charges_id','=',$id)
            ->delete();
        return redirect('loancharges/')->with("successMsg",'Existing Loan Charge is Deleted in your data');
    }
}
