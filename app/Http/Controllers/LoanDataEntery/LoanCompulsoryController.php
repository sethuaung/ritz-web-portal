<?php

namespace App\Http\Controllers\LoanDataEntery;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Branch;
use App\Models\ClientDataEnteries\Staff;
use App\Models\Loandataenteries\LoanCompulsory;
use App\Models\Loandataenteries\LoanCompulsoryProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoanCompulsoryController extends Controller
{
    //Index Page
    public function index()
    {
      	$this->permissionFilter("compulsory-products");
        $compulsories = DB::table('loan_compulsory_type')
            ->leftJoin('loan_compulsory_product', 'loan_compulsory_product.compulsory_product_id', '=', 'loan_compulsory_type.compulsory_code')
            // ->where('loan_compulsory_product.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
            ->Paginate(10);
    	// dd($compulsories);
        return view('loan.dataentery.loancompulsory.index', compact('compulsories'));
    }

    //Create Page
    public function create()
    {
      	$this->permissionFilter("compulsory-products");

        $branches = Branch::select('branch_name','id')->get();
        $accounts = DB::table('account_charts')->get();
        return view('loan.dataentery.loancompulsory.create',compact('branches','accounts'));
    }

    public function store(Request $request)
    {
      	$this->permissionFilter("compulsory-products");
        $validator = $request->validate([
            'compulsory_code' => ['required','string','max:255'],
            'compulsory_name' => ['required','string','max:255'],
            'compulsory_description' => ['required','string','max:255'],
            'saving_amount' => ['required','string','max:255'],
            'interest_rate' => ['required','string','max:255'],
            'accounting_id' => ['required','string','max:255'],
            'status' => ['required','string','max:255'],
        ]);

        if($validator){
            $loan_compulsory = new LoanCompulsory();
            $loan_compulsory->compulsory_code = $request->compulsory_code;
            $loan_compulsory->compulsory_name = $request->compulsory_name;
            $loan_compulsory->compulsory_description = $request->compulsory_description;
        	$loan_compulsory->charge_option = $request->charge_option;
            $loan_compulsory->saving_amount = $request->saving_amount;
            $loan_compulsory->interest_rate = $request->interest_rate;
            $loan_compulsory->accounting_id = $request->accounting_id;
            $loan_compulsory->status = $request->status;
            $loan_compulsory->save();

            if($request->compulsory_code){
                $loan_compulsory_product = new LoanCompulsoryProduct();
                $loan_compulsory_product->compulsory_product_id = $loan_compulsory->compulsory_code;
                $loan_compulsory_product->name = $loan_compulsory->compulsory_name;
                $loan_compulsory_product->description = $loan_compulsory->compulsory_description;
                $loan_compulsory_product->branch_id = Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id; // edit later
                $loan_compulsory_product->status = $loan_compulsory->status;
                $loan_compulsory_product->save();
            }

            return redirect('loancompulsory/')->with("successMsg",'New Loan Compulsory is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }


    //Edit Page
    public function edit($id)
    {
      	$this->permissionFilter("compulsory-products");
        $compulsory = LoanCompulsory::where('compulsory_code', $id)->first();
    // dd($compulsory);
        $compulsory_product =DB::table('loan_compulsory_product')
            ->leftJoin('tbl_branches','tbl_branches.id','=','loan_compulsory_product.branch_id')
            ->where('loan_compulsory_product.compulsory_product_id','=',$compulsory->compulsory_code)
            ->first();
        $account_type = DB::table('loan_compulsory_type')
            ->leftJoin('account_charts','account_charts.id','=','loan_compulsory_type.accounting_id')
            ->where('loan_compulsory_type.compulsory_code','=',$id)
            ->first();
        $branches = Branch::select('branch_name','id')->get();
        $accounts = DB::table('account_charts')->get();
        return view('loan.dataentery.loancompulsory.edit',compact('compulsory','branches','compulsory_product','account_type','accounts'));
    }

    public function update(Request $request,$id)
    {
      	$this->permissionFilter("compulsory-products");
        $validator = $request->validate([
            'compulsory_code' => ['required','string','max:255'],
            'compulsory_name' => ['required','string','max:255'],
            'compulsory_description' => ['required','string','max:255'],
            'saving_amount' => ['required','string','max:255'],
            'interest_rate' => ['required','string','max:255'],
            'accounting_id' => ['required','string','max:255'],
            'status' => ['required','string','max:255'],
        ]);

        if($validator){
            $loan_compulsory = LoanCompulsory::where('compulsory_code', $id)->first();
            $loan_compulsory->compulsory_code = $request->compulsory_code;
            $loan_compulsory->compulsory_name = $request->compulsory_name;
            $loan_compulsory->compulsory_description = $request->compulsory_description;
        	$loan_compulsory->charge_option = $request->charge_option;
            $loan_compulsory->saving_amount = $request->saving_amount;
            $loan_compulsory->interest_rate = $request->interest_rate;
            $loan_compulsory->accounting_id = $request->accounting_id;
            $loan_compulsory->status = $request->status;
            $loan_compulsory->save();

            if($request->compulsory_code){
                $loan_compulsory_product = LoanCompulsoryProduct::where("compulsory_product_id","=",$loan_compulsory->compulsory_code)->first();
                $loan_compulsory_product->delete();

                $loan_compulsory_product_new = new LoanCompulsoryProduct();
                $loan_compulsory_product_new->compulsory_product_id = $loan_compulsory->compulsory_code;
                $loan_compulsory_product_new->name = $loan_compulsory->compulsory_name;
                $loan_compulsory_product_new->description = $loan_compulsory->compulsory_description;
                $loan_compulsory_product_new->branch_id = Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id; // edit later
                $loan_compulsory_product_new->status = $loan_compulsory->status;
                $loan_compulsory_product_new->save();
            }

            return redirect('loancompulsory/')->with("successMsg",'Update Loan Compulsory is Added in your data');
        }else{
            return redirect()->back()->withErrors($validator);
        }
    }

    //view Page
    public function view($id)
    {
        $compulsory= LoanCompulsory::where('compulsory_code', $id)->first();
        $compulsory_product =DB::table('loan_compulsory_product')
            ->leftJoin('tbl_branches','tbl_branches.id','=','loan_compulsory_product.branch_id')
            ->where('loan_compulsory_product.compulsory_product_id','=',$compulsory->compulsory_code)
            ->first();
        $account_type = DB::table('loan_compulsory_type')
            ->leftJoin('account_charts','account_charts.id','=','loan_compulsory_type.accounting_id')
            ->where('loan_compulsory_type.compulsory_code','=',$id)
            ->first();
        return view('loan.dataentery.loancompulsory.view',compact('compulsory','compulsory_product','account_type'));
    }

    //Delete
    public function destroy($id)
    {
      	$this->permissionFilter("compulsory-products");
        $compulsory =LoanCompulsory::where('compulsory_code', $id)->first();
        $compulsory -> delete();
        $compulsory_product = DB::table('loan_compulsory_product')
            ->leftJoin('loan_compulsory_type','loan_compulsory_type.compulsory_code','=','loan_compulsory_product.compulsory_product_id')
            ->where('loan_compulsory_product.compulsory_product_id','=',$id)
            ->delete();
        return redirect('loancompulsory/')->with("successMsg",'Existing Loan Compulsory is Deleted in your data');
    }
}
