<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function permissionFilter($data)
    {
        // dd($data);
        $permissions=DB::table('role_has_permissions')
        ->leftJoin('permissions','permissions.id','=','role_has_permissions.permission_id')
        ->leftJoin('model_has_permissions', 'model_has_permissions.permission_id', '=', 'permissions.id')
        ->where('role_has_permissions.role_id','=',Session::get('role_id'))
        ->orWhere('model_has_permissions.model_id', Session::get('staff_code'))
        ->select('permissions.name')->pluck('name')->toArray();
        if(in_array($data,$permissions)){
            return true;
        }else{
            abort(403,"User don't have permission to access!");
        }
    }
}
