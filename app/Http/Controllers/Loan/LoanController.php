<?php

namespace App\Http\Controllers\Loan;

use App\Http\Controllers\Controller;
use App\Models\ClientDataEnteries\Branch;
use App\Models\ClientDataEnteries\Center;
use App\Models\ClientDataEnteries\Group;
use App\Models\ClientDataEnteries\Guarantor;
use App\Models\ClientDataEnteries\Staff;
use App\Models\ClientDataEnteries\BusinessType;
use App\Models\ClientDataEnteries\BusinessCategory;
use App\Models\Clients\ClientBasicInfo;
use App\Models\Loandataenteries\LoanType;
use App\Models\Loans\Loan;
use App\Models\Loans\LoanCycle;
use App\Models\Loans\LoanDocument;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use App\Models\Loans\LoanJoin;
use App\Models\Loans\LoanSchedule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use App\Models\Crawl\Crawl_Loan_Repayment_Schedule;



class LoanController extends Controller
{
    public function getBranchCode()
    {
        $staff_code = Session::get('staff_code');
        // dd($staff_code);
        $B_code = DB::table('tbl_staff')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', 'tbl_staff.branch_id')
            ->select('tbl_branches.branch_code')
            ->where('tbl_staff.staff_code', $staff_code)
            ->first();
        $b_code = strtolower($B_code->branch_code);
        return $b_code;
    }
    //Index Page
    public function index(Request $request)
    {
        $this->permissionFilter("view-all-loan");

        $bcode = $this->getBranchCode();
        if ($request->filled('search_loanslist')) {
            // Search method
            $searchvalue = $request->search_loanslist;
            $data_loanstitle = 'loan list';
            $data_loans = DB::table($bcode . '_loans')
                ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
                ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
                ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_a')
                ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode . '_loans.loan_unique_id')
                ->where($bcode . '_loans.loan_unique_id', 'LIKE', $searchvalue . '%')
                ->orWhere($bcode . '_loans.loan_officer_id', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_staff.name', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_client_basic_info.name', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_guarantors.name', 'LIKE', $searchvalue . '%')
                ->orWhere('loan_type.name', 'LIKE', $searchvalue . '%')
                ->orWhere($bcode . '_loans.disbursement_status', 'LIKE', $searchvalue . '%')
                ->orWhere('tbl_main_join_loan.main_loan_code', 'LIKE', $searchvalue . '%')
                ->where($bcode . '_loans.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                // ->where('tbl_main_join_loan.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name')
                ->orderby($bcode . '_loans.id', 'DESC')
                ->Paginate(10);
        } else if ($request->filled('disbursement_status')) {
            // loans type method
            $loanstypevalue = $request->disbursement_status;
            $data_loanstitle = $loanstypevalue . ' loan list';
            $data_loans = DB::table($bcode . '_loans')
                ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
                ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
                ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_a')
                ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode . '_loans.loan_unique_id')
                ->where($bcode . '_loans.disbursement_status', '=', $loanstypevalue)
                ->where($bcode . '_loans.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                // ->where('tbl_main_join_loan.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name')
                ->orderBy($bcode . '_loans.id', 'DESC')
                ->Paginate(10);
        } else if ($request->filled('date')) {
            $searchvalue = $request->date;
            //            dd($searchvalue);
            $data_loanstitle = ' loan list';
            if ($searchvalue == 'date') {
                $data_loans = DB::table($bcode . '_loans')
                    ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
                    ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
                    ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_a')
                    ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                    ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode . '_loans.loan_unique_id')
                    ->whereDate($bcode . '_loans.created_at', '=', Carbon::now())
                    ->where($bcode . '_loans.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                    // ->where('tbl_main_join_loan.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                    ->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name')
                    ->orderBy($bcode . '_loans.id', 'DESC')
                    ->Paginate(10);
            } else if ($searchvalue == 'month') {
                $data_loans = DB::table($bcode . '_loans')
                    ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
                    ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
                    ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_a')
                    ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                    ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode . '_loans.loan_unique_id')
                    ->whereMonth($bcode . '_loans.created_at', '=', Carbon::now()->month)
                    ->where($bcode . '_loans.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                    // ->where('tbl_main_join_loan.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                    ->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name')
                    ->orderBy($bcode . '_loans.id', 'DESC')
                    ->Paginate(10);
            } else if ($searchvalue == "year") {
                $data_loans = DB::table($bcode . '_loans')
                    ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
                    ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
                    ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_a')
                    ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                    ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode . '_loans.loan_unique_id')
                    ->whereYear($bcode . '_loans.created_at', '=', Carbon::now()->year)
                    ->where($bcode . '_loans.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                    // ->where('tbl_main_join_loan.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                    ->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name')
                    ->orderBy($bcode . '_loans.id', 'DESC')
                    ->Paginate(10);
            } else {
                $data_loans = DB::table($bcode . '_loans')
                    ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
                    ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
                    ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_a')
                    ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                    ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode . '_loans.loan_unique_id')
                    ->whereBetween($bcode . '_loans.created_at', [Carbon::now()->startOfWeek()->format('Y-m-d'), Carbon::now()->endOfWeek()->format('Y-m-d')])
                    ->where($bcode . '_loans.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                    // ->where('tbl_main_join_loan.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                    ->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name')
                    ->orderBy($bcode . '_loans.id', 'DESC')
                    ->Paginate(10);
            }
        } else if ($request->filled('loan_length')) {
            $searchvalue = $request->loan_length;
            $data_loanstitle = 'View All Loan';
            $data_loans = DB::table($bcode . '_loans')
                ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
                ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
                ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_a')
                ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode . '_loans.loan_unique_id')
                ->where($bcode . '_loans.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                // ->where('tbl_main_join_loan.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name')
                ->orderBy($bcode . '_loans.id', 'DESC')
                ->Paginate($searchvalue);
        } else {
            // All data get
            $data_loanstitle = 'View All Loan';
            $data_loans = DB::table($bcode . '_loans')
                ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
                ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
                ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_a')
                ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode . '_loans.loan_unique_id')
                ->where($bcode . '_loans.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                // ->where('tbl_main_join_loan.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name')
                ->orderBy($bcode . '_loans.id', 'DESC')
                ->Paginate(10);
        }

        $data_branch = Branch::orderby('id')->get();

        $loans = DB::table($bcode . '_loans')
            ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
            ->leftjoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
            ->leftjoin('tbl_guarantors', 'tbl_guarantors.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_a')
            ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
            ->leftjoin('tbl_main_join_loan', 'tbl_main_join_loan.portal_loan_id', '=', $bcode . '_loans.loan_unique_id')
            ->where($bcode . '_loans.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
            // ->where('tbl_main_join_loan.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
            ->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name')
            ->orderBy($bcode . '_loans.id', 'DESC')
            ->Paginate(10);

        $data_loans = $data_loans->appends(['disbursement_status' => $request->disbursement_status]);
        // dd($loans);

        return view('loan.index', compact('data_loans', 'data_loanstitle', 'data_branch', 'loans'));
    }

    //Create Page
    public function create()
    {
        $this->permissionFilter("create-loan");

        $loantypes = LoanType::select('name', 'id')->get();
        if (DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->role_id == 1) {
            $branches = Branch::where('id', DB::table('tbl_staff')->where('staff_code', session()->get('staff_code'))->first()->branch_id)->get();
            $staffs = Staff::where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)->get();
        } else {
            $branches = Branch::select()->get();
            $staffs = Staff::select()->get();
        }
        // $groups = DB::table('tbl_group')
        // ->leftJoin('tbl_main_join_group', 'tbl_main_join_group.portal_group_id', '=', 'tbl_group.group_uniquekey')
        // ->where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
        // ->select()
        // ->get()
        // ->unique('group_uniquekey', 'main_group_code');
        $centers = DB::table('tbl_center')->leftJoin('tbl_main_join_center', 'tbl_main_join_center.portal_center_id', '=', 'tbl_center.center_uniquekey')
            ->where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
            ->select()
            ->get();
        $clients = DB::table('tbl_client_basic_info')
            ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftJoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
            ->where('tbl_client_join.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
            ->select()
            ->get();
        $guarantors = DB::table('tbl_guarantors')
            ->where('branch_id', Staff::where('staff_code', session()
                ->get('staff_code'))
                ->first()->branch_id)
            ->select()
            ->get();
        $business_types = BusinessType::orderby('business_type_name')->select('business_type_name', 'id')->get();
        $business_categories = BusinessCategory::orderby('business_category_name')->select('business_category_name', 'id', 'business_type_id')->get();
        $loanuniquekey = $this->generateLoanID();
        date_default_timezone_set('Asia/Yangon');

        $loan_application_date = date('Y-m-d');
        $first_installment_date = date('Y-m-d');

        // dd($first_installment_date);
        //$first_installment_date = date('Y-m-d h:m:s', strtotime($date->month));

        return view('loan.create', compact('loanuniquekey', 'loantypes', 'branches', 'centers', 'clients', 'guarantors', 'staffs', 'business_types', 'business_categories', 'loan_application_date', 'first_installment_date'));
    }

    //auto generate loans uniquekey
    public function generateLoanID()
    {
        $bcode = $this->getBranchCode();

        // $obj_branchcode = DB::table('tbl_branches')->where('id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)->select('branch_code')->get();
        // if (count(array($obj_branchcode)) > 0) {
        $obj_branchcode = DB::table('tbl_branches')->where('id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)->first()->branch_code;
        $obj_fund = DB::table($bcode . '_loans')->get()->last();
        if ($obj_fund) {
            //LG-00001
            $last_inserted_id = $obj_fund->loan_unique_id;
            $loanuniquekey = ++$last_inserted_id;
        } else {
            $loanuniquekey = $obj_branchcode . '-03-000000001';
        }

        return $loanuniquekey;
        // } else {
        //     return response()->json(['status_code' => 500, 'message' => 'loan id generate fail', 'data' => null]);
        // }
    }

    public function storeDocument($request, $name1, $name2, $name3, $name4, $name5, $status, $loan_unique_id)
    {
        $bcode = $this->getBranchCode();

        $loantypename = Loantype::where('id', $request->loan_type_id)->first();

        if ($name1 != null or $name2 != null or $name3 != null or $name4 != null or $name5 != null) {
            $loan_document = new LoanDocument();
            $loan_document->setTable($bcode . '_loan_document');

            $loan_document->loan_unique_id = $loan_unique_id;
            $loan_document->loan_type_id = $request->loan_type_id;
            $loan_document->loan_type_name = $loantypename->name;
            $loan_document->client_id = $request->client_id;
            $loan_document->guarantor_id = $request->guarantor_a;
            $loan_document->status = $status;

            $loan_document->loan_document_1 = $this->convertImage($request->loan_unique_id, $name1);

            $loan_document->loan_document_2 = $this->convertImage($request->loan_unique_id, $name2);

            $loan_document->loan_document_3 = $this->convertImage($request->loan_unique_id, $name3);

            $loan_document->loan_document_4 = $this->convertImage($request->loan_unique_id, $name4);

            $loan_document->loan_document_5 = $this->convertImage($request->loan_unique_id, $name5);

            $loan_document->save();
        }
    }

    public function convertImage($dataloantid, $dataimage)
    {
        $destinationPath = 'public/loan_document_photos/';
        if ($dataimage != '') {
            $image_name = uniqid() . '_' . $dataloantid . '_' . time() . '.' . $dataimage->getClientOriginalName();
            $dataimage->storeAs($destinationPath, $image_name);
        } else {
            $image_name = null;
        }

        return $image_name;
    }

    public function store(Request $request)
    {
        $this->permissionFilter("create-loan");

        $bcode = $this->getBranchCode();
        $loan_id = $this->generateLoanID();
        // dd($loan_id);
        // dd($request);
        $validator = $request->validate([
            // 'loan_unique_id' => ['required', 'string', 'max:255', 'unique:loans'],
            // 'process_join_id' => ['required','string','max:255'],
            'branch_id' => ['required', 'string', 'max:255'],
            'group_id' => ['required', 'string', 'max:255'],
            'center_id' => ['required', 'string', 'max:255'],
            'loan_officer_id' => ['required', 'string', 'max:255'],
            // 'business_category_id' => ['required', 'string', 'max:255'],
            'client_id' => ['required', 'string', 'max:255'],
            'guarantor_a' => ['nullable', 'string', 'max:255'],
            'guarantor_b' => ['nullable', 'string', 'max:255'],
            'guarantor_c' => ['nullable', 'string', 'max:255'],
            'loan_type_id' => ['required', 'integer', 'max:255'],
            'loan_amount' => ['required', 'numeric'],
            'estimate_receivable_amount' => ['nullable', 'numeric'],
            'loan_term_value' => ['required', 'numeric'],
            'loan_term' => ['required', 'string', 'max:255'],
            'interest_rate_period' => ['required', 'string', 'max:255'],
            'disbursement_status' => ['required', 'string', 'max:255'],
            'interest_method' => ['required', 'string', 'max:255'],
            'remark' => ['string', 'max:255', 'nullable'],
            // 'status_del' => ['required','string','max:255'],
            'loan_application_date' => ['required', 'date'],
            'first_installment_date' => ['required', 'date'],
            'cancel_date' => ['date'],
            'loan_document_1' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_2' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_3' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_4' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_5' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_6' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_7' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_8' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_9' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_10' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_11' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_12' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_13' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_14' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_15' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_16' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_17' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_18' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_19' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'loan_document_20' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
        ]);

        //        dd($request);

        // loan document
        if ($validator) {
            $this->storeDocument($request, $request->loan_document_1, $request->loan_document_2, $request->loan_document_3, $request->loan_document_4, $request->loan_document_5, 'client', $loan_id);

            if ($request->guarantor_select >= 1) {
                $this->storeDocument($request, $request->loan_document_6, $request->loan_document_7, $request->loan_document_8, $request->loan_document_9, $request->loan_document_10, 'guarantor_a', $loan_id);
            }

            if ($request->guarantor_select >= 2) {
                $this->storeDocument($request, $request->loan_document_11, $request->loan_document_12, $request->loan_document_13, $request->loan_document_14, $request->loan_document_15, 'guarantor_b', $loan_id);
            }

            if ($request->guarantor_select == 3) {
                $this->storeDocument($request, $request->loan_document_16, $request->loan_document_17, $request->loan_document_18, $request->loan_document_19, $request->loan_document_20, 'guarantor_c', $loan_id);
            }
        }
        // loan cycle

        $loan_cycle = new LoanCycle();
        $loan_cycle->setTable($bcode . '_loan_cycle');

        $loan_cycle->loan_unique_id = $loan_id;
        $loan_cycle->client_unique_id = $request->client_id;
        $loan_cycle->loan_amount = $request->loan_amount;
        $loan_cycle->loan_final_amount = $request->loan_amount;
        $loan_cycle->description = $request->guarantor_a . ',' . $request->guarantor_b . ',' . $request->guarantor_c;
        $loan_cycle->cycle = 1;
        $loan_cycle->now_status = 1;
        $loan_cycle->save();

        // loan charge and compulsory join
        for ($i = 0; $i < $request->loan_charge_count; $i++) {
            $text = "loan_charge" . ($i + 1) . "_id";
            $loan_join = new LoanJoin();
            $loan_join->setTable($bcode . '_loan_charges_compulsory_join');
            $loan_join->loan_unique_id = $loan_id;
            $loan_join->loan_charge_id = $request->$text;
            $loan_join->loan_type_id = $request->loan_type_id;
            $loan_join->branch_id = $request->branch_id;
            $loan_join->save();
        }
        for ($i = 0; $i < $request->loan_compulsory_count; $i++) {
            $text = "loan_compulsory" . ($i + 1) . "_id";
            $loan_join = new LoanJoin();
            $loan_join->setTable($bcode . '_loan_charges_compulsory_join');
            $loan_join->loan_unique_id = $loan_id;
            $loan_join->loan_compulsory_id = $request->$text;
            $loan_join->loan_type_id = $request->loan_type_id;
            $loan_join->branch_id = $request->branch_id;
            $loan_join->save();
        }


        // loan schedule

        //add schedule start-----------------------------------------------------------

        $loantypeid = $request->loan_type_id;
        $loan_amount = $request->loan_amount;
        $principal_formula = $request->principal_formula;
        $loan_term_value = $request->loan_term_value;
        $loan_term = $request->loan_term;
        $interest_rate = $request->interest_rate;
        $disbursement_date = $request->first_installment_date;

        $entrydate = $disbursement_date;

        // Repayment date
        if ($loan_term == "Day") {
            $loan_term = 1;
        } else if ($loan_term == "Week") {
            $loan_term = 7;
        } else if ($loan_term == "Two-Weeks") {
            $loan_term = 14;
        } else if ($loan_term == "Four-Weeks") {
            $loan_term = 28;
        } else if ($loan_term == "Month") {
            $loan_term = 30;
        } else if ($loan_term == "Year") {
            $loan_term = 30;
        }
        if (($loantypeid == 1) or ($loantypeid ==  2)) {
            // GeneralWeekly14D
            $loanschedule = $this->fixedLoanMethod(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        } else if (($loantypeid == 3) or ($loantypeid == 4)) {
            // ExtraLoanWeekly
            $loanschedule = $this->principalFinalMethod30D(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        } else if (($loantypeid > 4) && ($loantypeid < 20)) {
            // 5 to 19
            // ExtraLoanWeekly
            $loanschedule = $this->fixedPrincipalMethod30D(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        } else if (($loantypeid > 19) && ($loantypeid < 23)) {
            // ExtraLoanWeekly
            $loanschedule = $this->principalFinalMethod28D(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        } else if (($loantypeid > 22) && ($loantypeid < 26)) {
            // GeneralMonthly28D
            $loanschedule = $this->fixedPrincipalMethod28D(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        } else if (($loantypeid == 26) or ($loantypeid == 27)) {
            // ExtraLoanWeekly
            $loanschedule = $this->fixedPrincipalMethod30D(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        }
        // dd($loanschedule);

        foreach ($loanschedule as $schedule) {
            $entrydate = $schedule['date'];
            // $total = $schedule->repay_interest + $schedule->repay_principal;
            $loan_amount -=  $schedule['repay_principal'];

            $loan_schedule = new LoanSchedule;
            $loan_schedule->setTable($bcode . '_loans_schedule');

            $loan_schedule->loan_unique_id = $loan_id;
            $loan_schedule->loan_type_id = $request->loan_type_id;
            $loan_schedule->month = $entrydate;
            $loan_schedule->capital = $schedule['repay_principal'];
            $loan_schedule->interest = $schedule['repay_interest'];
            $loan_schedule->discount = 0;
            $loan_schedule->refund_interest = 1;
            $loan_schedule->refund_amount = 1;
            $loan_schedule->final_amount = $loan_amount;
            $loan_schedule->status = "none";
            $loan_schedule->save();
        }

        //add schedule end----------------------------------------------------------------------------------


        if ($validator) {
            $loan = new Loan;
            $loan->setTable($bcode . '_loans');

            $loan->loan_unique_id = $loan_id;
            $loan->process_join_id = 1;
            $loan->branch_id = $request->branch_id;
            $loan->group_id = $request->group_id;
            $loan->center_id = $request->center_id;
            $loan->loan_officer_id = $request->loan_officer_id;
            $loan->business_category_id = $request->business_category_id;
            $loan->client_id = $request->client_id;
            $loan->guarantor_a = $request->guarantor_a;
            $loan->guarantor_b = $request->guarantor_b;
            $loan->guarantor_c = $request->guarantor_c;
            $loan->loan_type_id = $request->loan_type_id;
            $loan->loan_amount = $request->loan_amount;
            $loan->estimate_receivable_amount = $request->estimate_receivable_amount;
            $loan->loan_term_value = $request->loan_term_value;
            $loan->loan_term = $request->loan_term;
            $loan->interest_rate_period = $request->interest_rate_period;
            $loan->disbursement_status = $request->disbursement_status;
            $loan->interest_method = $request->interest_method;
            $loan->remark = $request->remark;
            $loan->status_del = "none";
            // $loan->loan_application_date = date('Y-m-d H:i:s', strtotime($request->loan_application_date));
            $loan->loan_application_date = $request->loan_application_date;
            $loan->first_installment_date = $request->first_installment_date;
            $loan->disbursement_date = $request->disbursement_date;
            $loan->cancel_date = $request->cancel_date;

            $loan->save();


            $this->liveReverseLoans($request, $loan_id);

            return redirect('loan/')->with("successMsg", 'New Loan is Added in your data');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    //Edit Page
    public function edit($id)
    {
        $this->permissionFilter("update-loan");
        $bcode = $this->getBranchCode();

        // $loan = DB::table('loans')->where('id', $id)->get();
        $loan = DB::table($bcode . '_loans')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
            ->leftJoin('tbl_client_family_info', 'tbl_client_family_info.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftJoin('tbl_client_photo', 'tbl_client_basic_info.client_uniquekey', '=', 'tbl_client_photo.client_uniquekey')
            ->leftJoin('tbl_guarantors AS GA', 'GA.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_a')
            ->leftJoin('tbl_guarantors AS GB', 'GB.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_b')
            ->leftJoin('tbl_guarantors AS GC', 'GC.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_c')
            ->leftJoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
            ->leftJoin('tbl_business_category', 'tbl_business_category.id', '=', $bcode . '_loans.business_category_id')
            ->select(
                'tbl_client_basic_info.*',
                $bcode . '_loans.*',
                'tbl_client_basic_info.id as cliend_id',
                'tbl_business_category.business_type_id',
                'tbl_business_category.business_category_name as business_category_name',
                'GA.guarantor_photo as a_photo',
                'GA.name as a_name',
                'GA.nrc as a_nrc',
                'GA.dob as a_dob',
                'loan_type.interest_rate',
                'loan_type.interest_method',
                'loan_type.principal_formula',
                'GA.guarantor_uniquekey as a_key',
                'tbl_client_photo.client_photo as client_photo',
                'tbl_client_family_info.father_name as father_name',
                'GA.phone_primary as a_phone',
                'GB.guarantor_photo as b_photo',
                'GB.name as b_name',
                'GB.nrc as b_nrc',
                'GB.dob as b_dob',
                'GB.nrc as b_nrc',
                'GB.guarantor_uniquekey as b_key',
                'GB.phone_primary as b_phone',
                'GC.nrc as c_nrc',
                'GC.guarantor_photo as c_photo',
                'GC.name as c_name',
                'GC.nrc as c_nrc',
                'GC.dob as c_dob',
                'GC.guarantor_uniquekey as c_key',
                'GC.phone_primary as c_phone'
            )
            ->where($bcode . '_loans.loan_unique_id', '=', $id)->first();
        // dd($loan);
        $loantypes = Loantype::select('name', 'id', 'interest_rate')->get();
        $branches = Branch::select()->get();
        $groups = Group::where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)->select()->get()->unique('group_uniquekey');
        $centers = Center::where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)->select()->get();
        $clients = DB::table('tbl_client_basic_info')
            ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
            ->where('tbl_client_join.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
            ->select()
            ->get();
        $staffs = Staff::select()->get();
        $guarantors = Guarantor::select()->get();
        // dd($loan->cancel_date);

        $business_type = DB::table('tbl_business_category')
            ->leftJoin('tbl_business_type', 'tbl_business_type.id', '=', 'tbl_business_category.business_type_id')
            ->where('tbl_business_category.id', '=', $loan->business_category_id)
            ->select('tbl_business_type.business_type_name', 'tbl_business_type.id')
            ->get();

        $business_types = DB::table('tbl_business_type')->get();

        $business_categories = DB::table('tbl_business_category')
            ->get();

        $loan_application_date = date('Y-m-d\TH:i', strtotime($loan->loan_application_date));
        $first_installment_date = date('Y-m-d\TH:i', strtotime($loan->first_installment_date));
        $disbursement_date = date('Y-m-d\TH:i', strtotime($loan->disbursement_date));
        $cancel_date = $loan->cancel_date;
        if ($cancel_date != NULL) {
            $cancel_date = date('Y-m-d\TH:i', strtotime($loan->cancel_date));
        }
        $dates = array(
            'loan_application_date' => $loan_application_date,
            'first_installment_date' => $first_installment_date,
            'disbursement_date' => $disbursement_date,
            'cancel_date' => $cancel_date
        );

        // dd($dates);


        // dd($loan);
        $client_loan_doc = DB::table($bcode . '_loan_document')->where('loan_unique_id', '=', $loan->loan_unique_id)->where($bcode . '_loan_document.status', '=', "client")->first();
        $guarantor_a_loan_doc = DB::table($bcode . '_loan_document')->where('loan_unique_id', '=', $loan->loan_unique_id)->where($bcode . '_loan_document.status', '=', "guarantor_a")->first();
        $guarantor_b_loan_doc = DB::table($bcode . '_loan_document')->where('loan_unique_id', '=', $loan->loan_unique_id)->where($bcode . '_loan_document.status', '=', "guarantor_b")->first();
        $guarantor_c_loan_doc = DB::table($bcode . '_loan_document')->where('loan_unique_id', '=', $loan->loan_unique_id)->where($bcode . '_loan_document.status', '=', "guarantor_c")->first();
        $schedules = DB::table($bcode . "_loans_schedule")->where('loan_unique_id', '=', $loan->loan_unique_id)->get();
        $total_interest = DB::table($bcode . "_loans_schedule")->where('loan_unique_id', '=', $loan->loan_unique_id)->sum('interest');
        $total_capital = DB::table($bcode . "_loans_schedule")->where('loan_unique_id', '=', $loan->loan_unique_id)->sum('capital');
        $total_final_amount = DB::table($bcode . "_loans_schedule")->where('loan_unique_id', '=', $loan->loan_unique_id)->sum('final_amount');
        // $client_info  = ClientBasicInfo::select()->where('id','=',$loan->client_id)->first();
        // $guarantor_a_info  = Guarantor::select()->where('id','=',$loan->guarantor_a)->first();
        // $guarantor_b_info  = Guarantor::select()->where('id','=',$loan->guarantor_b)->first();

        $loan_charges = DB::table($bcode . '_loan_charges_compulsory_join')
            ->leftJoin('loan_charges_type', $bcode . '_loan_charges_compulsory_join.loan_charge_id', '=', 'loan_charges_type.id')
            ->where('loan_charge_id', '!=', null)
            ->where('loan_unique_id', $loan->loan_unique_id)
            ->get();

        $loan_compulsory = DB::table($bcode . '_loan_charges_compulsory_join')
            ->leftJoin('loan_compulsory_type', $bcode . '_loan_charges_compulsory_join.loan_compulsory_id', '=', 'loan_compulsory_type.compulsory_code')
            ->where('loan_compulsory_id', '!=', null)
            ->where('loan_unique_id', $loan->loan_unique_id)
            ->get();
        //        dd($loan_compulsory);
        //        dd($client_loan_doc);

        // dd($business_categories);
        return view('loan.edit', compact('loan', 'loantypes', 'business_type', 'business_types', 'business_categories', 'branches', 'groups', 'centers', 'clients', 'guarantors', 'staffs', 'client_loan_doc', 'guarantor_a_loan_doc', 'guarantor_b_loan_doc', 'guarantor_c_loan_doc', 'schedules', 'total_interest', 'total_capital', 'total_final_amount', 'dates', 'loan_charges', 'loan_compulsory'));
    }

    public function update(Request $request, Loan $loan)
    {
        $this->permissionFilter("update-loan");
        $bcode = $this->getBranchCode();

        $validator = $request->validate([
            'loan_unique_id' => ['required', 'string', 'max:255'],
            // 'process_join_id' => ['required','string','max:255'],
            'branch_id' => ['required', 'string', 'max:255'],
            'group_id' => ['nullable', 'string', 'max:255'],
            'center_id' => ['nullable', 'string', 'max:255'],
            'loan_officer_id' => ['required', 'string', 'max:255'],
            'business_category_id' => ['required', 'string', 'max:255'],
            'client_id' => ['required', 'string', 'max:255'],
            'guarantor_a' => ['nullable', 'string', 'max:255'],
            'guarantor_b' => ['nullable', 'string', 'max:255'],
            'guarantor_c' => ['nullable', 'string', 'max:255'],
            'loan_type_id' => ['required', 'integer', 'max:255'],
            'loan_amount' => ['required', 'numeric'],
            'estimate_receivable_amount' => ['nullable', 'numeric'],
            'loan_term_value' => ['required', 'numeric'],
            'loan_term' => ['required', 'string', 'max:255'],
            'interest_rate_period' => ['required', 'string', 'max:255'],
            'disbursement_status' => ['required', 'string', 'max:255'],
            'interest_method' => ['required', 'string', 'max:255'],
            'remark' => ['nullable', 'string', 'max:255'],
            // 'status_del' => ['required','string','max:255'],
            'loan_application_date' => ['required', 'date'],
            'first_installment_date' => ['required', 'date'],
            'cancel_date' => ['date'],
        ]);


        $destinationPath = 'public/loan_document_photos/';
        $client_loan_doc = DB::table($bcode . '_loan_document')->where('loan_unique_id', $request->loan_unique_id)->where('status', 'client')->first();
        $guarantor_a_loan_doc = DB::table($bcode . '_loan_document')->where('loan_unique_id', $request->loan_unique_id)->where('status', 'guarantor_a')->first();
        $guarantor_b_loan_doc = DB::table($bcode . '_loan_document')->where('loan_unique_id', $request->loan_unique_id)->where('status', 'guarantor_b')->first();
        $guarantor_c_loan_doc = DB::table($bcode . '_loan_document')->where('loan_unique_id', $request->loan_unique_id)->where('status', 'guarantor_c')->first();

        $loantypename = Loantype::where('id', $request->loan_type_id)->first();

        if ($client_loan_doc) {
            if ($request->hasFile('loan_document_1')) {
                $request->validate([
                    'loan_document_1' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $loan_document_1_name = time() . '.' . $request->loan_document_1->getClientOriginalName();
                $request->loan_document_1->storeAs($destinationPath, $loan_document_1_name);
            } else {
                $loan_document_1_name = $client_loan_doc->loan_document_1;
            }

            if ($request->hasFile('loan_document_2')) {
                $request->validate([
                    'loan_document_2' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $loan_document_2_name = time() . '.' . $request->loan_document_2->getClientOriginalName();
                $request->loan_document_2->storeAs($destinationPath, $loan_document_2_name);
            } else {
                $loan_document_2_name = $client_loan_doc->loan_document_2;
            }


            if ($request->hasFile('loan_document_3')) {
                $request->validate([
                    'loan_document_3' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $loan_document_3_name = time() . '.' . $request->loan_document_3->getClientOriginalName();
                $request->loan_document_3->storeAs($destinationPath, $loan_document_3_name);
            } else {
                $loan_document_3_name = $client_loan_doc->loan_document_3;
            }


            if ($request->hasFile('loan_document_4')) {
                $request->validate([
                    'loan_document_4' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $loan_document_4_name = time() . '.' . $request->loan_document_4->getClientOriginalName();
                $request->loan_document_4->storeAs($destinationPath, $loan_document_4_name);
            } else {
                $loan_document_4_name = $client_loan_doc->loan_document_4;
            }


            if ($request->hasFile('loan_document_5')) {
                $request->validate([
                    'loan_document_5' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $loan_document_5_name = time() . '.' . $request->loan_document_5->getClientOriginalName();
                $request->loan_document_5->storeAs($destinationPath, $loan_document_5_name);
            } else {
                $loan_document_5_name = $client_loan_doc->loan_document_5;
            }


            DB::table($bcode . '_loan_document')
                ->where($bcode . '_loan_document.loan_unique_id', $request->loan_unique_id)
                ->where($bcode . '_loan_document.status', 'client')
                ->update([
                    $bcode . '_loan_document.loan_unique_id' => $request->loan_unique_id,
                    $bcode . '_loan_document.loan_type_id' => $request->loan_type_id,
                    $bcode . '_loan_document.loan_type_name' => $loantypename->name,
                    $bcode . '_loan_document.client_id' => $request->client_id,
                    $bcode . '_loan_document.guarantor_id' => $request->guarantor_a,
                    $bcode . '_loan_document.status' => 'client',
                    $bcode . '_loan_document.loan_document_1' => $loan_document_1_name,
                    $bcode . '_loan_document.loan_document_2' => $loan_document_2_name,
                    $bcode . '_loan_document.loan_document_3' => $loan_document_3_name,
                    $bcode . '_loan_document.loan_document_4' => $loan_document_4_name,
                    $bcode . '_loan_document.loan_document_5' => $loan_document_5_name,
                ]);
        }

        if ($guarantor_a_loan_doc) {
            if ($request->hasFile('loan_document_6')) {
                $request->validate([
                    'loan_document_6' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $loan_document_6_name = time() . '.' . $request->loan_document_6->getClientOriginalName();
                $request->loan_document_6->storeAs($destinationPath, $loan_document_6_name);
            } else {
                $loan_document_6_name = $guarantor_a_loan_doc->loan_document_1;
            }

            if ($request->hasFile('loan_document_7')) {
                $request->validate([
                    'loan_document_7' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $loan_document_7_name = time() . '.' . $request->loan_document_7->getClientOriginalName();
                $request->loan_document_7->storeAs($destinationPath, $loan_document_7_name);
            } else {
                $loan_document_7_name = $guarantor_a_loan_doc->loan_document_2;
            }


            if ($request->hasFile('loan_document_8')) {
                $request->validate([
                    'loan_document_8' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $loan_document_8_name = time() . '.' . $request->loan_document_8->getClientOriginalName();
                $request->loan_document_8->storeAs($destinationPath, $loan_document_8_name);
            } else {
                $loan_document_8_name = $guarantor_a_loan_doc->loan_document_3;
            }


            if ($request->hasFile('loan_document_9')) {
                $request->validate([
                    'loan_document_9' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $loan_document_9_name = time() . '.' . $request->loan_document_9->getClientOriginalName();
                $request->loan_document_9->storeAs($destinationPath, $loan_document_9_name);
            } else {
                $loan_document_9_name = $guarantor_a_loan_doc->loan_document_4;
            }


            if ($request->hasFile('loan_document_10')) {
                $request->validate([
                    'loan_document_10' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                ]);
                $loan_document_10_name = time() . '.' . $request->loan_document_10->getClientOriginalName();
                $request->loan_document_10->storeAs($destinationPath, $loan_document_10_name);
            } else {
                $loan_document_10_name = $guarantor_a_loan_doc->loan_document_5;
            }

            DB::table($bcode . '_loan_document')
                ->where($bcode . '_loan_document.loan_unique_id', $request->loan_unique_id)
                ->where($bcode . '_loan_document.status', 'guarantor_a')
                ->update([
                    $bcode . '_loan_document.loan_unique_id' => $request->loan_unique_id,
                    $bcode . '_loan_document.loan_type_id' => $request->loan_type_id,
                    $bcode . '_loan_document.loan_type_name' => $loantypename->name,
                    $bcode . '_loan_document.client_id' => $request->client_id,
                    $bcode . '_loan_document.guarantor_id' => $request->guarantor_a,
                    $bcode . '_loan_document.status' => 'guarantor_a',
                    $bcode . '_loan_document.loan_document_1' => $loan_document_6_name,
                    $bcode . '_loan_document.loan_document_2' => $loan_document_7_name,
                    $bcode . '_loan_document.loan_document_3' => $loan_document_8_name,
                    $bcode . '_loan_document.loan_document_4' => $loan_document_9_name,
                    $bcode . '_loan_document.loan_document_5' => $loan_document_10_name,
                ]);
        }

        if ($guarantor_b_loan_doc) {
            if ($request->guarantor_b) {
                if ($request->hasFile('loan_document_11')) {
                    $request->validate([
                        'loan_document_11' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                    ]);
                    $loan_document_11_name = time() . '.' . $request->loan_document_11->getClientOriginalName();
                    $request->loan_document_11->storeAs($destinationPath, $loan_document_11_name);
                } else {
                    $loan_document_11_name = $guarantor_b_loan_doc->loan_document_1;
                }

                if ($request->hasFile('loan_document_12')) {
                    $request->validate([
                        'loan_document_12' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                    ]);
                    $loan_document_12_name = time() . '.' . $request->loan_document_12->getClientOriginalName();
                    $request->loan_document_12->storeAs($destinationPath, $loan_document_12_name);
                } else {
                    $loan_document_12_name = $guarantor_b_loan_doc->loan_document_2;
                }

                if ($request->hasFile('loan_document_13')) {
                    $request->validate([
                        'loan_document_13' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                    ]);
                    $loan_document_13_name = time() . '.' . $request->loan_document_13->getClientOriginalName();
                    $request->loan_document_13->storeAs($destinationPath, $loan_document_13_name);
                } else {
                    $loan_document_13_name = $guarantor_b_loan_doc->loan_document_3;
                }

                if ($request->hasFile('loan_document_14')) {
                    $request->validate([
                        'loan_document_14' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                    ]);
                    $loan_document_14_name = time() . '.' . $request->loan_document_14->getClientOriginalName();
                    $request->loan_document_14->storeAs($destinationPath, $loan_document_14_name);
                } else {
                    $loan_document_14_name = $guarantor_b_loan_doc->loan_document_4;
                }

                if ($request->hasFile('loan_document_15')) {
                    $request->validate([
                        'loan_document_15' => 'image|mimes:jpg,png,jpeg,gif,svg|max:1548',
                    ]);
                    $loan_document_15_name = time() . '.' . $request->loan_document_15->getClientOriginalName();
                    $request->loan_document_15->storeAs($destinationPath, $loan_document_15_name);
                } else {
                    $loan_document_15_name = $guarantor_b_loan_doc->loan_document_5;
                }
            }

            if ($guarantor_b_loan_doc != null) {
                DB::table($bcode . '_loan_document')
                    ->where($bcode . '_loan_document.loan_unique_id', $request->loan_unique_id)
                    ->where($bcode . '_loan_document.status', 'guarantor_b')
                    ->update([
                        $bcode . '_loan_document.loan_unique_id' => $request->loan_unique_id,
                        $bcode . '_loan_document.loan_type_id' => $request->loan_type_id,
                        $bcode . '_loan_document.loan_type_name' => $loantypename->name,
                        $bcode . '_loan_document.client_id' => $request->client_id,
                        $bcode . '_loan_document.guarantor_id' => $request->guarantor_b,
                        $bcode . '_loan_document.status' => 'guarantor_b',
                        $bcode . '_loan_document.loan_document_1' => $loan_document_11_name,
                        $bcode . '_loan_document.loan_document_2' => $loan_document_12_name,
                        $bcode . '_loan_document.loan_document_3' => $loan_document_13_name,
                        $bcode . '_loan_document.loan_document_4' => $loan_document_14_name,
                        $bcode . '_loan_document.loan_document_5' => $loan_document_15_name,
                    ]);
            }
        }

        if ($guarantor_c_loan_doc) {
            if ($request->guarantor_c) {
                if ($request->hasFile('loan_document_16')) {
                    $request->validate([
                        'loan_document_16' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                    ]);
                    $loan_document_16_name = time() . '.' . $request->loan_document_16->getClientOriginalName();
                    $request->loan_document_16->storeAs($destinationPath, $loan_document_16_name);
                } else {
                    $loan_document_16_name = $guarantor_c_loan_doc->loan_document_1;
                }

                if ($request->hasFile('loan_document_17')) {
                    $request->validate([
                        'loan_document_17' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                    ]);
                    $loan_document_17_name = time() . '.' . $request->loan_document_17->getClientOriginalName();
                    $request->loan_document_17->storeAs($destinationPath, $loan_document_17_name);
                } else {
                    $loan_document_17_name = $guarantor_c_loan_doc->loan_document_2;
                }

                if ($request->hasFile('loan_document_18')) {
                    $request->validate([
                        'loan_document_18' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                    ]);
                    $loan_document_18_name = time() . '.' . $request->loan_document_18->getClientOriginalName();
                    $request->loan_document_18->storeAs($destinationPath, $loan_document_18_name);
                } else {
                    $loan_document_18_name = $guarantor_c_loan_doc->loan_document_3;
                }

                if ($request->hasFile('loan_document_19')) {
                    $request->validate([
                        'loan_document_19' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                    ]);
                    $loan_document_19_name = time() . '.' . $request->loan_document_19->getClientOriginalName();
                    $request->loan_document_19->storeAs($destinationPath, $loan_document_19_name);
                } else {
                    $loan_document_19_name = $guarantor_c_loan_doc->loan_document_4;
                }

                if ($request->hasFile('loan_document_20')) {
                    $request->validate([
                        'loan_document_20' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048',
                    ]);
                    $loan_document_20_name = time() . '.' . $request->loan_document_20->getClientOriginalName();
                    $request->loan_document_20->storeAs($destinationPath, $loan_document_20_name);
                } else {
                    $loan_document_20_name = $guarantor_c_loan_doc->loan_document_5;
                }
            }

            if ($guarantor_c_loan_doc != null) {

                DB::table($bcode . '_loan_document')
                    ->where($bcode . '_loan_document.loan_unique_id', $request->loan_unique_id)
                    ->where($bcode . '_loan_document.status', 'guarantor_c')
                    ->update([
                        $bcode . '_loan_document.loan_unique_id' => $request->loan_unique_id,
                        $bcode . '_loan_document.loan_type_id' => $request->loan_type_id,
                        $bcode . '_loan_document.loan_type_name' => $loantypename->name,
                        $bcode . '_loan_document.client_id' => $request->client_id,
                        $bcode . '_loan_document.guarantor_id' => $request->guarantor_c,
                        $bcode . '_loan_document.status' => 'guarantor_c',
                        $bcode . '_loan_document.loan_document_1' => $loan_document_16_name,
                        $bcode . '_loan_document.loan_document_2' => $loan_document_17_name,
                        $bcode . '_loan_document.loan_document_3' => $loan_document_18_name,
                        $bcode . '_loan_document.loan_document_4' => $loan_document_19_name,
                        $bcode . '_loan_document.loan_document_5' => $loan_document_20_name,
                    ]);
            }
        }

        //add schedule start-----------------------------------------------------------

        $loantypeid = $request->loan_type_id;
        $loan_amount = $request->loan_amount;
        $principal_formula = $request->principal_formula;
        $loan_term_value = $request->loan_term_value;
        $loan_term = $request->loan_term;
        $interest_rate = $request->interest_rate;

        $entrydate = Carbon::now();

        // Repayment date
        if ($loan_term == "Day") {
            $loan_term = 1;
        } else if ($loan_term == "Week") {
            $loan_term = 7;
        } else if ($loan_term == "Two-Weeks") {
            $loan_term = 14;
        } else if ($loan_term == "Four-Weeks") {
            $loan_term = 28;
        } else if ($loan_term == "Month") {
            $loan_term = 30;
        } else if ($loan_term == "Year") {
            $loan_term = 30;
        }
        if (($loantypeid == 1) or ($loantypeid ==  2)) {
            // GeneralWeekly14D
            $loanschedule = $this->fixedLoanMethod(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        } else if (($loantypeid == 3) or ($loantypeid == 4)) {
            // ExtraLoanWeekly
            $loanschedule = $this->principalFinalMethod30D(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        } else if (($loantypeid > 4) && ($loantypeid < 20)) {
            // 5 to 19
            // ExtraLoanWeekly
            $loanschedule = $this->fixedPrincipalMethod30D(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        } else if (($loantypeid > 19) && ($loantypeid < 23)) {
            // ExtraLoanWeekly
            $loanschedule = $this->principalFinalMethod28D(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        } else if (($loantypeid > 22) && ($loantypeid < 26)) {
            // GeneralMonthly28D
            $loanschedule = $this->fixedPrincipalMethod28D(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        } else if (($loantypeid == 26) or ($loantypeid == 27)) {
            // ExtraLoanWeekly
            $loanschedule = $this->fixedPrincipalMethod30D(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        }
        //  dd($request);

        DB::table($this->getBranchCode() . '_loans_schedule')->where('loan_unique_id', $request->loan_unique_id)->delete();

        foreach ($loanschedule as $schedule) {
            $entrydate = $schedule['date'];
            // $total = $schedule->repay_interest + $schedule->repay_principal;
            $loan_amount -=  $schedule['repay_principal'];

            $loan_schedule = new LoanSchedule;
            $loan_schedule->setTable($bcode . '_loans_schedule');

            $loan_schedule->loan_unique_id = $request->loan_unique_id;
            $loan_schedule->loan_type_id = $request->loan_type_id;
            $loan_schedule->month = $entrydate;
            $loan_schedule->capital = $schedule['repay_principal'];
            $loan_schedule->interest = $schedule['repay_interest'];
            $loan_schedule->discount = 0;
            $loan_schedule->refund_interest = 1;
            $loan_schedule->refund_amount = 1;
            $loan_schedule->final_amount = $loan_amount;
            $loan_schedule->status = "none";
            $loan_schedule->save();
        }

        //add schedule end----------------------------------------------------------------------------------




        // $loan_amount = $request->loan_amount;
        // $loan_term_value = $request->loan_term_value;
        // $loan_term = $request->loan_term;
        // $interest_rate = $request->interest_rate;
        // if ($loan_term == "Day") {
        //     $loan_term = 1;
        // } else if ($loan_term == "Week") {
        //     $loan_term = 7;
        // } else if ($loan_term == "Two-Weeks") {
        //     $loan_term = 14;
        // } else if ($loan_term == "Month") {
        //     $loan_term = 31;
        // } else if ($loan_term == "Year") {
        //     $loan_term = 365;
        // }

        // $entrydate = strtotime($request->date);
        // $principal = $loan_amount / $loan_term_value;
        // $onemonth_interest = (($interest_rate / 100) * $loan_amount) / $loan_term_value;
        // $interest = round($onemonth_interest / 2);

        //         for ($i = 0; $i < $loan_term_value; $i++) {
        //             $entrydate = date("d-M-Y", strtotime($entrydate . '+' . $loan_term . 'days'));
        //             $total = $principal + $interest;
        //             $loan_amount -= $principal;

        //             DB::table($bcode.'_loans_schedule')
        //                 ->where($bcode.'_loans_schedule.loan_unique_id', $request->loan_unique_id)
        //                 ->update([
        //                     $bcode.'_loans_schedule.loan_unique_id' => $request->loan_unique_id,
        //                     $bcode.'_loans_schedule.loan_type_id' => $request->loan_type_id,
        //                     $bcode.'_loans_schedule.month' => $entrydate,
        //                     $bcode.'_loans_schedule.capital' => $principal,
        //                     $bcode.'_loans_schedule.interest' => $interest,
        //                     $bcode.'_loans_schedule.discount' => 0,
        //                     $bcode.'_loans_schedule.refund_interest' => 1,
        //                     $bcode.'_loans_schedule.refund_amount' => 1,
        //                     $bcode.'_loans_schedule.final_amount' => $total,
        //                     $bcode.'_loans_schedule.status' => "none",
        //                 ]);


        //         }

        if ($validator) {
            DB::table($bcode . '_loans')
                ->where($bcode . '_loans.loan_unique_id', $request->loan_unique_id)
                ->update([
                    $bcode . '_loans.loan_unique_id' => $request->loan_unique_id,
                    $bcode . '_loans.process_join_id' => 1,
                    $bcode . '_loans.branch_id' => $request->branch_id,
                    $bcode . '_loans.group_id' => $request->group_id,
                    $bcode . '_loans.center_id' => $request->center_id,
                    $bcode . '_loans.loan_officer_id' => $request->loan_officer_id,
                    $bcode . '_loans.business_category_id' => $request->business_category_id,
                    $bcode . '_loans.client_id' => $request->client_id,
                    $bcode . '_loans.guarantor_a' => $request->guarantor_a,
                    $bcode . '_loans.guarantor_b' => $request->guarantor_b,
                    $bcode . '_loans.guarantor_c' => $request->guarantor_c,
                    $bcode . '_loans.loan_type_id' => $request->loan_type_id,
                    $bcode . '_loans.loan_amount' => $request->loan_amount,
                    $bcode . '_loans.estimate_receivable_amount' => $request->estimate_receivable_amount,
                    $bcode . '_loans.loan_term_value' => $request->loan_term_value,
                    $bcode . '_loans.loan_term' => $request->loan_term,
                    $bcode . '_loans.interest_rate_period' => $request->interest_rate_period,
                    $bcode . '_loans.disbursement_status' => $request->disbursement_status,
                    $bcode . '_loans.interest_method' => $request->interest_method,
                    $bcode . '_loans.remark' => $request->remark,
                    $bcode . '_loans.status_del' => "none",
                    $bcode . '_loans.loan_application_date' => $request->loan_application_date,
                    $bcode . '_loans.first_installment_date' => $request->first_installment_date,
                    $bcode . '_loans.disbursement_date' => $request->disbursement_date,
                    $bcode . '_loans.cancel_date' => $request->cancel_date

                ]);

            // loan charge and compulsory join
            if ($request->loan_charge1_id or $request->loan_compulsory1_id) {
                DB::table($bcode . '_loan_charges_compulsory_join')
                    ->where('loan_unique_id', $request->loan_unique_id)
                    ->delete();

                for ($i = 0; $i < $request->loan_charge_count; $i++) {
                    $text = "loan_charge" . ($i + 1) . "_id";
                    $loan_join = new LoanJoin();
                    $loan_join->setTable($bcode . '_loan_charges_compulsory_join');
                    $loan_join->loan_unique_id = $request->loan_unique_id;
                    $loan_join->loan_charge_id = $request->$text;
                    $loan_join->loan_type_id = $request->loan_type_id;
                    $loan_join->branch_id = Staff::where('staff_code', session()->get('staff_code'))->first()->branch_code;
                    $loan_join->save();
                }
                for ($i = 0; $i < $request->loan_compulsory_count; $i++) {
                    $text = "loan_compulsory" . ($i + 1) . "_id";
                    $loan_join = new LoanJoin();
                    $loan_join->setTable($bcode . '_loan_charges_compulsory_join');
                    $loan_join->loan_unique_id = $request->loan_unique_id;
                    $loan_join->loan_compulsory_id = $request->$text;
                    $loan_join->loan_type_id = $request->loan_type_id;
                    $loan_join->branch_id = Staff::where('staff_code', session()->get('staff_code'))->first()->branch_code;
                    $loan_join->save();
                }
            }


            //   if($validator){
            //         $loan->loan_unique_id = $request->loan_unique_id;
            //         $loan->process_join_id = $request->process_join_id;
            //         $loan->branch_id = $request->branch_id;
            //         $loan->group_id = $request->group_id;
            //         $loan->center_id = $request->center_id;
            //         $loan->loan_officer_id = $request->loan_officer_id;
            //         $loan->client_id = $request->client_id;
            //         $loan->guarantor_a = $request->guarantor_a;
            //         $loan->guarantor_b = $request->guarantor_b;
            //         $loan->loan_type_id = $request->loan_type_id;
            //         $loan->loan_amount = $request->loan_amount;
            //         $loan->estimate_receivable_amount = $request->estimate_receivable_amount;
            //         $loan->loan_term_value = $request->loan_term_value;
            //         $loan->loan_term = $request->loan_term;
            //         $loan->interest_rate_period = $request->interest_rate_period;
            //         $loan->disbursement_status = $request->disbursement_status;
            //         $loan->interest_method = $request->interest_method;
            //         $loan->remark = $request->remark;
            //         $loan->status_del = "none";
            //         $loan->loan_application_date = $request->loan_application_date;
            //         $loan->first_installment_date = $request->first_installment_date;
            //         $loan->disbursement_date = $request->disbursement_date;
            //         $loan->cancel_date = $request->cancel_date;

            //         $loan->save();

            return redirect('loan/')->with("successMsg", 'New Loan is Added in your data');
        } else {
            return redirect()->back()->withErrors($validator);
        }
    }

    //View Page
    public function view($id)
    {

        $bcode = $this->getBranchCode();
        // dd($bcode);

        $loan = DB::table($bcode . '_loans')
            ->leftJoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
            ->leftJoin('tbl_branches', 'tbl_branches.id', '=', $bcode . '_loans.branch_id')
            ->leftJoin('tbl_center', 'tbl_center.center_uniquekey', '=', $bcode . '_loans.center_id')
            ->leftJoin('tbl_group', 'tbl_group.group_uniquekey', '=', $bcode . '_loans.group_id')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $bcode . '_loans.client_id')
            ->leftJoin('tbl_client_family_info', 'tbl_client_family_info.client_uniquekey', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftJoin('tbl_business_category', 'tbl_business_category.id', '=', $bcode . '_loans.business_category_id')
            ->leftJoin('tbl_staff', 'tbl_staff.staff_code', '=', $bcode . '_loans.loan_officer_id')
            ->leftJoin('tbl_guarantors AS GA', 'GA.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_a')
            ->leftJoin('tbl_guarantors AS GB', 'GB.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_b')
            ->leftJoin('tbl_guarantors AS GC', 'GC.guarantor_uniquekey', '=', $bcode . '_loans.guarantor_c')
            ->where($bcode . '_loans.loan_unique_id', '=', $id)
            ->select(
                $bcode . '_loans.*',
                'tbl_business_category.business_category_name AS business_category',
                'tbl_business_category.business_category_name_mm AS business_category_mm',
                'loan_type.name AS loan_type_name',
                'loan_type.interest_rate',
                'tbl_branches.branch_name',
                'tbl_center.center_uniquekey',
                'tbl_group.group_uniquekey',
                'tbl_client_basic_info.client_uniquekey AS client_id',
                'tbl_client_basic_info.name AS client_name',
                'tbl_client_family_info.father_name AS client_father_name',
                'tbl_client_basic_info.nrc AS client_nrc',
                'tbl_client_basic_info.dob AS client_dob',
                'tbl_client_basic_info.phone_primary AS client_phone',
                'tbl_staff.name AS loan_officer_name',
                'GA.guarantor_uniquekey AS guarantor_a_id',
                'GA.name AS guarantor_a',
                'GA.nrc AS guarantor_a_nrc',
                'GA.dob AS guarantor_a_dob',
                'GA.phone_primary AS guarantor_a_phone',
                'GB.name AS guarantor_b',
                'GB.guarantor_uniquekey AS guarantor_b_id',
                'GB.nrc AS guarantor_b_nrc',
                'GB.dob AS guarantor_b_dob',
                'GB.phone_primary AS guarantor_b_phone',
                'GC.guarantor_uniquekey AS guarantor_c_id',
                'GC.name AS guarantor_c',
                'GC.nrc AS guarantor_c_nrc',
                'GC.dob AS guarantor_c_dob',
                'GC.phone_primary AS guarantor_c_phone'
            )
            ->first();

        // dd($loan);

        $business_type = DB::table('tbl_business_category')
            ->leftJoin('tbl_business_type', 'tbl_business_type.id', '=', 'tbl_business_category.business_type_id')
            ->select('tbl_business_type.business_type_name', 'tbl_business_type.business_type_name_mm')
            ->where('tbl_business_category.id', '=', $loan->business_category_id)
            ->first();

        // dd($business_type);

        $schedules = DB::table($bcode . '_loans')
            ->leftjoin($bcode . '_loans_schedule', $bcode . '_loans.loan_unique_id', '=', $bcode . '_loans_schedule.loan_unique_id')
            ->where($bcode . '_loans.loan_unique_id', '=', $id)
            ->orderby($bcode . '_loans_schedule.id', 'asc')
            ->get();

        $total_interest = DB::table($bcode . '_loans_schedule')->where('loan_unique_id', '=', $loan->loan_unique_id)->sum('interest');
        $total_capital = DB::table($bcode . '_loans_schedule')->where('loan_unique_id', '=', $loan->loan_unique_id)->sum('capital');
        $total_final_amount = DB::table($bcode . '_loans_schedule')->where('loan_unique_id', '=', $loan->loan_unique_id)->sum('final_amount');

        $client_photo = DB::table($bcode . '_loans')
            ->leftJoin('tbl_client_basic_info', $bcode . '_loans.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
            ->leftJoin('tbl_client_photo', 'tbl_client_basic_info.client_uniquekey', '=', 'tbl_client_photo.client_uniquekey')
            ->where($bcode . '_loans.loan_unique_id', '=', $id)
            ->first();


        $guarantor_a_photo = DB::table($bcode . '_loans')
            ->leftJoin('tbl_guarantors', $bcode . '_loans.guarantor_a', '=', 'tbl_guarantors.guarantor_uniquekey')
            ->where($bcode . '_loans.loan_unique_id', '=', $id)
            ->first();

        $guarantor_b_photo = DB::table($bcode . '_loans')
            ->leftJoin('tbl_guarantors', $bcode . '_loans.guarantor_b', '=', 'tbl_guarantors.guarantor_uniquekey')
            ->where($bcode . '_loans.loan_unique_id', '=', $id)
            ->first();

        $guarantor_c_photo = DB::table($bcode . '_loans')
            ->leftJoin('tbl_guarantors', $bcode . '_loans.guarantor_c', '=', 'tbl_guarantors.guarantor_uniquekey')
            ->where($bcode . '_loans.loan_unique_id', '=', $id)
            ->first();

        $client_document = DB::table($bcode . '_loans')
            ->leftjoin($bcode . '_loan_document', $bcode . '_loans.loan_unique_id', '=', $bcode . '_loan_document.loan_unique_id')
            ->where($bcode . '_loans.loan_unique_id', '=', $id)
            ->where($bcode . '_loan_document.status', '=', "client")
            ->first();

        $guarantor_a_document = DB::table($bcode . '_loans')
            ->leftjoin($bcode . '_loan_document', $bcode . '_loans.loan_unique_id', '=', $bcode . '_loan_document.loan_unique_id')
            ->where($bcode . '_loans.loan_unique_id', '=', $id)
            ->where($bcode . '_loan_document.status', '=', "guarantor_a")
            ->first();

        $guarantor_b_document = DB::table($bcode . '_loans')
            ->leftjoin($bcode . '_loan_document', $bcode . '_loans.loan_unique_id', '=', $bcode . '_loan_document.loan_unique_id')
            ->where($bcode . '_loans.loan_unique_id', '=', $id)
            ->where($bcode . '_loan_document.status', '=', "guarantor_b")
            ->first();

        $guarantor_c_document = DB::table($bcode . '_loans')
            ->leftjoin($bcode . '_loan_document', $bcode . '_loans.loan_unique_id', '=', $bcode . '_loan_document.loan_unique_id')
            ->where($bcode . '_loans.loan_unique_id', '=', $id)
            ->where($bcode . '_loan_document.status', '=', "guarantor_c")
            ->first();

        $main_loan_id = DB::table('tbl_main_join_loan')->where('portal_loan_id', $loan->loan_unique_id)->first();
        if ($main_loan_id) {
            $main_loan_id = $main_loan_id->main_loan_code;
        } else {
            $main_loan_id = null;
        }

        $main_group_id = DB::table('tbl_main_join_group')->where('portal_group_id', $loan->group_uniquekey)->first();
        if ($main_group_id) {
            $main_group_id = $main_group_id->main_group_code;
        } else {
            $main_group_id = null;
        }

        $main_center_id = DB::table('tbl_main_join_center')->where('portal_center_id', $loan->center_uniquekey)->first();
        if ($main_center_id) {
            $main_center_id = $main_center_id->main_center_code;
        } else {
            $main_center_id = null;
        }

        $main_client_id = DB::table('tbl_main_join_client')->where('portal_client_id', $loan->client_id)->first();
        if ($main_client_id) {
            $main_client_id = $main_client_id->main_client_code;
        } else {
            $main_client_id = null;
        }

        $center_leader_staff_id = $loan->loan_officer_id;
        $center_leader_name = DB::table('tbl_staff')->where('staff_code', $center_leader_staff_id)->first()->name;
        return view('loan.view', compact('main_client_id', 'main_center_id', 'main_loan_id', 'main_group_id', 'center_leader_name', 'loan', 'business_type', 'schedules', 'client_photo', 'client_document', 'guarantor_a_photo', 'guarantor_b_photo', 'guarantor_c_photo', 'guarantor_a_document', 'guarantor_b_document', 'guarantor_c_document', 'total_interest', 'total_capital', 'total_final_amount'));
    }

    //Delete
    public function destroy($id)
    {

        $bcode = $this->getBranchCode();

        DB::table($bcode . '_loans')->where($bcode . '_loans.loan_unique_id', '=', $id)->delete();
        DB::table($bcode . '_loans_schedule')->where($bcode . '_loans_schedule.loan_unique_id', '=', $id)->delete();
        DB::table($bcode . '_loan_cycle')->where($bcode . '_loan_cycle.loan_unique_id', '=', $id)->delete();
        DB::table($bcode . '_loan_document')->where($bcode . '_loan_document.loan_unique_id', '=', $id)->delete();
        DB::table($bcode . '_loan_charges_compulsory_join')->where($bcode . '_loan_charges_compulsory_join.loan_unique_id', '=', $id)->delete();
        // DB::table('acc_saving_join')->where('loan_unique_id', '=', $id)->delete();


        return redirect('loan/')->with("successMsg", 'Existing Loan is Deleted in your data');
    }


    public function getLoanSchedule(Request $request) // For General Loan
    {
        // $loan_amount = $request->loan_amount;
        // $fixed_loan_amount=$request->loan_amount;
        // $interest_rate = $request->interest_rate;
        // $loan_term_value = $request->loan_term_value;
        // $loan_term_dropdown = $request->loan_term_dropdown;

        $entrydate = $request->date;

        //------------------------------------

        $loantypeid = $request->loan_type_id;

        $obj_loantype = DB::table('loan_type')->where('id', $loantypeid)->first();
        $loan_name = $obj_loantype->name;
        if ($request->loan_amount) {
            $loan_amount = $request->loan_amount;
        } else {
            $loan_amount = $obj_loantype->description;
        }
        $loan_term = $obj_loantype->loan_term;
        $loan_term_value = (int)$obj_loantype->loan_term_value;
        $interest_rate = $obj_loantype->interest_rate;
        $interest_rate_period = $obj_loantype->interest_rate_period;
        $interest_method = $obj_loantype->interest_method;
        $principal_formula = $obj_loantype->principal_formula;
        $interest_formula = $obj_loantype->interest_formula;

        // Repayment date
        if ($loan_term == "Day") {
            $loan_term = 1;
        } else if ($loan_term == "Week") {
            $loan_term = 7;
        } else if ($loan_term == "Two-Weeks") {
            $loan_term = 14;
        } else if ($loan_term == "Four-Weeks") {
            $loan_term = 28;
        } else if ($loan_term == "Month") {
            $loan_term = 30;
        } else if ($loan_term == "Year") {
            $loan_term = 30;
        }
        if (($loantypeid == 1) or ($loantypeid ==  2)) {
            // GeneralWeekly14D
            $loanschedule = $this->fixedLoanMethod(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        } else if (($loantypeid == 3) or ($loantypeid == 4)) {
            // ExtraLoanWeekly
            $loanschedule = $this->principalFinalMethod30D(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        } else if (($loantypeid >= 5) && ($loantypeid <= 19)) {
            // 5 to 19
            // ExtraLoanWeekly
            $loanschedule = $this->fixedPrincipalMethod30D(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        } else if (($loantypeid >= 20) && ($loantypeid <= 22)) {
            // ExtraLoanWeekly
            $loanschedule = $this->principalFinalMethod28D(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        } else if (($loantypeid >= 23) && ($loantypeid <= 25)) {
            // GeneralMonthly28D
            $loanschedule = $this->fixedPrincipalMethod28D(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        } else if (($loantypeid == 26) or ($loantypeid == 27)) {
            // ExtraLoanWeekly
            $loanschedule = $this->fixedPrincipalMethod30D(
                $loan_amount,
                $loan_term,
                $loan_term_value,
                $interest_rate,
                $principal_formula,
                // $start_date);
                $entrydate
            );
        }

        $data = array(
            "loan_id" => $loantypeid,
            "loan_name" => $loan_name,
            "loan_schedule" => $loanschedule,
        );
        return response()->json($data);

        //------------------------------------

        // if ($loan_term_dropdown == "Day") {
        //     $loan_term = 1;
        // } else if ($loan_term_dropdown == "Week") {
        //     $loan_term = 7;
        // } else if ($loan_term_dropdown == "Two-Weeks") {
        //     $loan_term = 14;
        // } else if ($loan_term_dropdown == "Month") {
        //     $loan_term = 31;
        // } else if ($loan_term_dropdown == "Year") {
        //     $loan_term = 365;
        // }
        // $principal = $loan_amount / $loan_term_value;

        // for ($i = 0; $i < $loan_term_value; $i++) {
        //     $onemonth_interest = (($interest_rate / 100) * $loan_amount) / $loan_term_value;
        //     $interest[$i] = round($onemonth_interest / 2);
        //     $entrydate = date("d-M-Y", strtotime($entrydate . '+' . $loan_term . 'days'));
        //     $schedule_date[$i] = $entrydate;
        //     $totalgg = $principal + $interest[$i];
        //     $total[$i] = $totalgg;
        //     $fixed_loan_amount -= $principal;
        //     $balance[$i] = $fixed_loan_amount;
        //     $loanschedule[$i] = array(
        //         "balance" => $balance[$i],
        //         "total" => $total[$i],
        //         "principal" => $principal,
        //         "date" => $schedule_date[$i],
        //         "interest" => $interest[$i]
        //     );
        // }




        return response()->json($loanschedule);
        //    return response()->json(['balance : ' => $balance, 'total' => $total, 'interest' => $interest, 'principal' => $principal]);
    }

    public function getClientInfo(Request $request)
    {
        if ($request->get('query')) {
            $client_name = $request->get('query');
            $data = DB::table('tbl_client_basic_info')
                ->leftJoin('tbl_client_join', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftJoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->where('tbl_client_join.branch_id', $request->get('branch_id'))
                ->where('tbl_client_basic_info.name', 'LIKE', "%{$client_name}%")
                ->orWhere('tbl_main_join_client.main_client_code', 'LIKE', $client_name . '%')
                ->orWhere('tbl_client_join.client_id', 'LIKE', $client_name . '%')
                ->orWhere('tbl_client_basic_info.name_mm', 'LIKE', $client_name.'%')
                // ->orWhere(function ($query) use ($client_name) {
                // $query->where('tbl_main_join_client.main_client_code', 'LIKE', $client_name.'%')
                // ->where('tbl_client_join.client_id', 'LIKE', $client_name.'%');
                // })
                ->get();
            $output = '<ul class="list-group" style="display:block;overflow-y: scroll; max-height: 300px">';
            foreach ($data as $row) {
                $mainID = $row->main_client_id != NULL ? $row->main_client_code : $row->client_uniquekey;
                $output .= '
         <li data-val=' . $row->client_uniquekey . ' class="cli_click list-group-item text-center text-dark" style="background:#f5f5f5;"><a class="text-dark" href="#">' . $mainID . '/' . $row->name . '</a></li>
         ';
            }
            $output .= '</ul>';
        }

        return response()->json($output);
    }

    public function getGuarantorAInfo(Request $request)
    {
        if ($request->get('query')) {
            $guarantor_name = $request->get('query');
            $data = DB::table('tbl_guarantors')
                ->where('name', 'LIKE', "%{$guarantor_name}%")
                ->where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
            foreach ($data as $row) {
                $output .= '
     <li data-val=' . $row->guarantor_uniquekey . ' class="gua_click"><a href="#">' . $row->name . '</a></li>
     ';
            }
            $output .= '</ul>';
        }

        return response()->json($output);
    }

    public function getGuarantorBInfo(Request $request)
    {
        if ($request->get('query')) {
            $guarantor_b_name = $request->get('query');
            $dataB = DB::table('tbl_guarantors')
                ->where('name', 'LIKE', "%{$guarantor_b_name}%")
                ->where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->get();
            $outputB = '<ul class="dropdown-menu" style="display:block; position:relative">';
            foreach ($dataB as $rowB) {
                $outputB .= '
     <li data-val=' . $rowB->guarantor_uniquekey . ' class="gua_click2"><a href="#">' . $rowB->name . '</a></li>
     ';
            }
            $outputB .= '</ul>';
        }

        return response()->json($outputB);
    }

    public function getGuarantorCInfo(Request $request)
    {
        if ($request->get('query')) {
            $guarantor_c_name = $request->get('query');
            $dataC = DB::table('tbl_guarantors')
                ->where('name', 'LIKE', "%{$guarantor_c_name}%")
                ->where('branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->get();
            $outputC = '<ul class="dropdown-menu" style="display:block; position:relative">';
            foreach ($dataC as $rowC) {
                $outputC .= '
        <li data-val=' . $rowC->guarantor_uniquekey . ' class="gua_click3"><a href="#">' . $rowC->name . '</a></li>
     ';
            }
            $outputC .= '</ul>';
        }

        return response()->json($outputC);
    }

    public function getSingleClientInfo(Request $request)
    {
        $bcode = $this->getBranchCode();

        if ($request->get('cli')) {
            $single_client_id = $request->get('cli');
            $single_client_info = DB::table('tbl_client_join')
                ->leftJoin('tbl_client_basic_info', 'tbl_client_join.client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftJoin('tbl_client_family_info', 'tbl_client_join.client_id', '=', 'tbl_client_family_info.client_uniquekey')
                ->leftJoin('tbl_client_photo', 'tbl_client_join.client_id', '=', 'tbl_client_photo.client_uniquekey')
                ->leftJoin('tbl_main_join_client', 'tbl_main_join_client.portal_client_id', '=', 'tbl_client_basic_info.client_uniquekey')
                ->leftJoin('tbl_staff', 'tbl_client_join.staff_id', '=', 'tbl_staff.staff_code')
                // ->leftJoin($bcode.'_saving_withdrawl','tbl_client_join.client_id','=',$bcode.'_saving_withdrawl.client_idkey')
                ->where('tbl_client_join.client_id', 'LIKE', "%{$single_client_id}%")
                ->select('tbl_client_basic_info.*', 'tbl_client_family_info.*', 'tbl_client_photo.*', 'tbl_main_join_client.*', 'tbl_client_join.*', 'tbl_staff.name as staff_name')
                ->first();

            return response()->json($single_client_info);
        }
    }
    public function getSavingAmount(Request $request)
    {

        $bcode = $this->getBranchCode();

        if ($request->get('cli')) {
            $single_client_name = $request->get('cli');
            $single_saving_info = DB::table($bcode . '_saving_withdrawl')
                ->where('client_idkey', 'LIKE', "%{$single_client_name}%")
                ->get();
            $saving_amount = 0;
            foreach ($single_saving_info as $amount) {
                $saving_amount += $amount->available_withdrawl;
            }
            return response()->json($saving_amount);
        }
    }

    public function getSingleGuarantorAInfo(Request $request)
    {
        if ($request->get('guarantor_a')) {
            $guarantor_a_id = $request->get('guarantor_a');
            $guarantor_a_info = DB::table('tbl_guarantors')
                ->where('guarantor_uniquekey', 'LIKE', "%{$guarantor_a_id}%")
                ->first();

            return response()->json($guarantor_a_info);
        }
    }

    public function getSingleGuarantorBInfo(Request $request)
    {
        if ($request->get('guarantor_b')) {
            $guarantor_b_id = $request->get('guarantor_b');
            $guarantor_b_info = DB::table('tbl_guarantors')
                ->where('guarantor_uniquekey', 'LIKE', "%{$guarantor_b_id}%")
                ->first();

            return response()->json($guarantor_b_info);
        }
    }

    public function getSingleGuarantorCInfo(Request $request)
    {
        if ($request->get('guarantor_c')) {
            $guarantor_c_id = $request->get('guarantor_c');
            $guarantor_c_info = DB::table('tbl_guarantors')
                ->where('guarantor_uniquekey', 'LIKE', "%{$guarantor_c_id}%")
                ->first();

            return response()->json($guarantor_c_info);
        }
    }


    public function getLoanTypeInfo(Request $request)
    {
        if ($request->get('loan_type_id')) {
            $loan_type_id = $request->get('loan_type_id');
            $loan_type = DB::table('loan_type')
                ->where('id', '=', $loan_type_id)
                ->first();

            return response()->json($loan_type);
        }
    }

    public function getBusinessCategory(Request $request)
    {
        $categories = BusinessCategory::where('business_type_id', $request->business_type_id)
            ->pluck('business_category_name', 'id');
        return response()->json($categories);
    }

    public function loanCancel($id, $staff_code)
    {
        $this->permissionFilter("loan-canceled");
        $branch_id = DB::table('tbl_staff')->where('staff_code', $staff_code)->first()->branch_id;
        $bcode = DB::table('tbl_branches')->where('id', $branch_id)->first()->branch_code;
        $bcode = strtolower($bcode);

        DB::table($bcode . '_loans')->where('loan_unique_id', $id)->update(['disbursement_status' => 'Canceled']);



        return redirect('loan?disbursement_status=Canceled')->with("successMsg", 'Cancel success');
    }


    //Loan Repayment Testing
    public function index_test(Request $request)
    {
        // $loans= Loan::Paginate(10);
        $bcode = $this->getBranchCode();

        // $loans= Loan::Paginate(10);
        if ($request->filled('search_loanslist')) {
            // Search method
            $searchvalue = $request->search_loanslist;
            $data_loanstitle = 'loan list';
            $data_loans = DB::table($bcode . '_loans')
                ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.id', '=', $bcode . '_loans.client_id')
                ->leftjoin('tbl_staff', 'tbl_staff.id', '=', $bcode . '_loans.loan_officer_id')
                ->leftjoin('tbl_guarantors', 'tbl_guarantors.id', '=', $bcode . '_loans.guarantor_a')
                ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                ->where($bcode . '_loans.loan_unique_id', 'LIKE', $searchvalue . '%')
                ->orWhere($bcode . '_loans.loan_officer_id', 'LIKE', $searchvalue . '%')
                ->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name')
                ->Paginate(10);
        } else if ($request->filled('disbursement_status')) {
            // loans type method
            $loanstypevalue = $request->disbursement_status;
            $data_loanstitle = $loanstypevalue . ' member list';
            $data_loans = DB::table($bcode . '_loans')
                ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.id', '=', $bcode . '_loans.client_id')
                ->leftjoin('tbl_staff', 'tbl_staff.id', '=', $bcode . '_loans.loan_officer_id')
                ->leftjoin('tbl_guarantors', 'tbl_guarantors.id', '=', $bcode . '_loans.guarantor_a')
                ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                ->where($bcode . '_loans.disbursement_status', '=', $loanstypevalue)
                ->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name')
                ->Paginate(10);
        } else {
            // All data get
            $data_loanstitle = 'View All Loan';
            $data_loans = DB::table($bcode . '_loans')
                ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.id', '=', $bcode . '_loans.client_id')
                ->leftjoin('tbl_staff', 'tbl_staff.id', '=', $bcode . '_loans.loan_officer_id')
                ->leftjoin('tbl_guarantors', 'tbl_guarantors.id', '=', $bcode . '_loans.guarantor_a')
                ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
                ->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name')
                ->Paginate(10);
        }

        $data_branch = Branch::orderby('id')->get();

        $loans = DB::table($bcode . '_loans')
            ->leftjoin('tbl_client_basic_info', 'tbl_client_basic_info.id', '=', $bcode . '_loans.client_id')
            ->leftjoin('tbl_staff', 'tbl_staff.id', '=', $bcode . '_loans.loan_officer_id')
            ->leftjoin('tbl_guarantors', 'tbl_guarantors.id', '=', $bcode . '_loans.guarantor_a')
            ->leftjoin('loan_type', 'loan_type.id', '=', $bcode . '_loans.loan_type_id')
            ->select($bcode . '_loans.*', 'tbl_client_basic_info.name AS client_name', 'tbl_staff.name AS loan_officer_name', 'tbl_guarantors.name AS guarantor_name', 'loan_type.name AS loan_type_name')
            ->Paginate(10);

        return view('loan.index_test', compact('data_loans', 'data_loanstitle', 'data_branch', 'loans'));
    }

    //Loan Repayment Testing
    //    public function repayment_test(Request $request)
    //    {
    //        $i = $request->count;
    //        dd($request);
    //        $arr = array();
    //        for ($x = 1;$x <= $i; $x++) {
    //            $name = "repayment_r".$x;
    //            $value = "repayment_r".$x."_value";
    //            if($request->$name === "checked") {
    //                array_push($arr, $request->$value);
    //            }
    //        }
    //
    ////        dd($arr[0]);
    //        $data = DB::table('loans_schedule')
    //            ->where('id', $arr[0])
    //            ->first();
    //
    //            dd($data);
    //    }

    public function getChargesCompulsory(Request $request)
    {
        $charge = DB::table('loan_charges_and_type')
            ->leftJoin('loan_charges_type', 'loan_charges_type.id', '=', 'loan_charges_and_type.charges_id')
            ->where('loan_charges_and_type.loan_type_id', '=', $request->loan_type_id)
            ->where('loan_charges_type.status', 'active')
            ->select('loan_charges_type.*', 'loan_charges_type.charge_code as loan_charge_id')
            ->get();
        // dd($charge);

        if (count($charge) <= 0) {
            $compulsory = [];
        } else {
            $compulsory = DB::table('loan_compulsory_product')
                ->leftJoin('loan_compulsory_type', 'loan_compulsory_type.compulsory_code', '=', 'loan_compulsory_product.compulsory_product_id')
                // ->where('loan_compulsory_product.branch_id', Staff::where('staff_code', session()->get('staff_code'))->first()->branch_id)
                ->where('loan_compulsory_type.status', 'active')
                ->select('loan_compulsory_type.*', 'loan_compulsory_type.compulsory_code as loan_compulsory_id')
                ->get();
        }
        return response()->json([$charge, $compulsory]);
    }

    //fixed principal and interest
    public function fixedLoanMethod($loan_amount, $loan_term, $loan_term_value, $interest_rate, $principal_formula, $start_date)
    {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if ($loan_term_value == 1) { // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = $start_date;
        $principal = round($loan_amount / $loan_term_value);
        $interest = round($principal * 14 / 100);

        for ($i = 1; $i <= $loan_term_value; $i++) {

            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            if ($i == 1) {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . '0' . 'days'));
            } else {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            }
            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "date" => $schedule_date
            );
        }
        return $loanschedule;
    }

    //pay principal at final 30days
    public function principalFinalMethod30D($loan_amount, $loan_term, $loan_term_value, $interest_rate, $principal_formula, $start_date)
    {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if ($loan_term_value == 1) { // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = $start_date;
        $principal = 0;
        $next_balance = $loan_amount;
        for ($i = 1; $i <= $loan_term_value; $i++) {
            // $paid_principal = 0;
            if ($i == $loan_term_value) {
                $principal = $loan_amount;
            }
            // $paid_principal += $principal;

            $interest = round($loan_amount * (($interest_rate / 100) / 12));
            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            if ($i == 1) {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . '0' . 'days'));
            } else {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            }

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }

    //pay principal at final 28 days
    public function principalFinalMethod28D($loan_amount, $loan_term, $loan_term_value, $interest_rate, $principal_formula, $start_date)
    {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if ($loan_term_value == 1) { // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = $start_date;
        $principal = 0;
        $next_balance = $loan_amount;
        $interest = round($loan_amount * (($interest_rate / 100) / 365) * 28);

        for ($i = 1; $i <= $loan_term_value; $i++) {
            // $paid_principal = 0;
            if ($i == $loan_term_value) {
                $principal = $loan_amount;
            }
            // $paid_principal += $principal;

            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            if ($i == 1) {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . '0' . 'days'));
            } else {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            }

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }

    //fixed principal and interest change 30 days
    public function fixedPrincipalMethod30D($loan_amount, $loan_term, $loan_term_value, $interest_rate, $principal_formula, $start_date)
    {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if ($loan_term_value == 1) { // 1 mean 12 months
            $loan_term_value = 12;
        }
        // dd($principal_formula);
        $schedule_date = $start_date;
        $principal = round($loan_amount * $principal_formula);
        // dd($principal);
        $next_balance = $loan_amount;
        for ($i = 1; $i <= $loan_term_value; $i++) {

            $paid_principal = 0;
            if ($i == $loan_term_value) {
                $principal = $loan_amount - $paid_principal;
            }
            $paid_principal += $principal;

            $interest = round($next_balance * ($interest_rate / 100) / 12);
            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            if ($i == 1) {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . '0' . 'days'));
            } else {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            }

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }

    //fixed principal and interest change 28 days
    public function fixedPrincipalMethod28D($loan_amount, $loan_term, $loan_term_value, $interest_rate, $principal_formula, $start_date)
    {
        // amount * 0.083 - principal
        // amount *( 28% / 365 ) *28 - interest
        if ($loan_term_value == 1) { // 1 mean 12 months
            $loan_term_value = 12;
        }

        $schedule_date = $start_date;
        $principal = round($loan_amount * $principal_formula);
        $next_balance = $loan_amount;
        for ($i = 1; $i <= $loan_term_value; $i++) {

            $paid_principal = 0;
            if ($i == $loan_term_value) {
                $principal = $loan_amount - $paid_principal;
            }
            $paid_principal += $principal;

            $interest = round($next_balance * (($interest_rate / 100) / 365) * 28);
            $next_balance -= $principal;
            $total = $loan_amount - $principal;
            $loan_amount -=  $principal;
            $balance = $loan_amount;

            if ($i == 1) {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . '0' . 'days'));
            } else {
                $schedule_date = date("d-M-Y", strtotime($schedule_date . '+' . $loan_term . 'days'));
            }

            $loanschedule[$i] = array(
                "repay_count" => $i,
                "repay_principal" => $principal,
                "repay_interest" => $interest,
                "now_balance" => $balance,
                "next_balance" => $total,
                "balance" => $balance,
                "total" => $total,
                "principal" => $principal,
                "date" => $schedule_date,
                "interest" => $interest,
            );
        }
        return $loanschedule;
    }

    public function getLoanInfo(Request $request)
    {
        $loan_id = $request->loan_id;
        $bcode = $this->getBranchCode();

        $loan_info = DB::table($bcode . '_loans')->where('loan_unique_id', $loan_id)->first();
        return $loan_info;
    }

    public function getMainUrl37()
    {
        // $url = 'http://172.16.50.101/mis-core/public/api/';
        $url = env('MAIN_URL');
        return $url;
    }

    public function loginToken()
    {
        $route = $this->getMainUrl37() . 'login';
        $headers = [
            'Accept' => 'application/json'
        ];
        $response = Http::withHeaders($headers)->post($route, [
            'username' => 'ict@mis.com',
            'password' => '123456'
        ]);
        return json_decode($response)->data[0]->token;
    }
    // public function liveReverseLoan(Request $request, $loan_id)
    // {

    //     $route = $this->getMainUrl37();
    //     $route_name = 'create-loan';
    //     $routeurl = $route . $route_name;
    //     $authorization = $this->loginToken();

    //     $branch_id = DB::table('tbl_staff')->where('staff_code', '=', Session::get('staff_code'))->first()->branch_id;
    //     $branch_code = strtolower($this->getBranchCode($branch_id));

    //     $loans = DB::connection('portal')->table($branch_code . '_loans')
    //         ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $branch_code . '_loans.client_id')
    //         // ->leftJoin($branch_code . '_loan_charges_compulsory_join', $branch_code . '_loan_charges_compulsory_join.loan_unique_id', '=', $branch_code . '_loans.loan_unique_id')
    //         ->where($branch_code.'_loans.loan_unique_id',$loan_id)
    //         // ->orwhere($branch_code.'_loans.disbursement_status', 'Approved')
    //         ->get();
    //         dd($loans);
    //     $interest_rate = DB::connection('portal')->table('loan_type')->where('id', $loans->loan_type_id)->first()->interest_rate;

    //     // staff
    //     $staffid = DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans->loan_officer_id)->first()->main_staff_id;

    //     // center
    //     $centerid = DB::table('tbl_main_join_center')->where('portal_center_id', $loans->center_id)->first()->main_center_id;

    //     // group
    //     $groupid = DB::table('tbl_main_join_group')->where('portal_group_id', $loans->group_id)->first()->main_group_id;

    //     // client
    //     $clientid = DB::table('tbl_main_join_client')->where('portal_client_id', $loans->client_id)->first()->main_client_id;

    //     // Guarantor_a
    //     $guarantor_a = DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id', $loans->guarantor_a)->first();
    //     if ($guarantor_a) {
    //         $guarantor_a = $guarantor_a->main_guarantor_id;
    //     } else {
    //         $guarantor_a = '';
    //     }
    //     // Guarantor_b
    //     $guarantor_b = DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id', $loans->guarantor_b)->first();
    //     if ($guarantor_b) {
    //         $guarantor_b = $guarantor_b->main_guarantor_id;
    //     } else {
    //         $guarantor_b = '';
    //     }

    //     // Guarantor_c
    //     $guarantor_c = DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id', $loans->guarantor_c)->first();
    //     if ($guarantor_c) {
    //         $guarantor_c = $guarantor_c->main_guarantor_id;
    //     } else {
    //         $guarantor_c = '';
    //     }


    //     // latest loan id generate
    //     $dataloan = DB::connection('main')->table('loans_' . $branch_id)->orderBy('id', 'desc')->first();
    //     $loanCount = DB::connection('main')->table('loans_' . $branch_id)->count();

    //     // $main_loan_generate_id = $dataloan->disbursement_number;
    //     // $bMainCode = DB::connection('main')->table('branches')->where('id', $y)
    //     $mainbcode = DB::connection('main')->table('branches')->where('id', $branch_id)->first()->code;
    //     $main_loan_generate_id = 'LID-' . $mainbcode . '-2023-';
    //     $num = $loanCount + 1;
    //     $main_loan_generate_id .= $num;

    //     // return response()->json($main_loan_generate_id);

    //     // save new loan
    //     $client = new \GuzzleHttp\Client(['verify' => false]);
    //     $routeurl = env('MAIN_URL') . "create-loan";


    //     $chargeDetail = DB::table($branch_code . '_loan_charges_compulsory_join')
    //         ->leftJoin('loan_charges_type', 'loan_charges_type.charge_code', '=', $branch_code . '_loan_charges_compulsory_join.loan_charge_id')
    //         ->where($branch_code . '_loan_charges_compulsory_join.loan_unique_id', $loan_id)
    //         ->where($branch_code . '_loan_charges_compulsory_join.loan_charge_id', '!=', NULL)
    //         ->select('loan_charges_type.*', 'loan_charges_type.id as charge_id')
    //         ->get();
    //     // return response()->json($loans[$i]->loan_term);

    //     $repayment_term = '';
    //     if ($loans->loan_term == 'Month') {
    //         $repayment_term = 'Monthly';
    //     } else if ($loans->loan_term == 'Day') {
    //         $repayment_term = 'Daily';
    //     } else if ($loans->loan_term == 'Week') {
    //         $repayment_term = 'Weekly';
    //     } else if ($loans->loan_term == 'Two-Weeks') {
    //         $repayment_term = 'Two-Weeks';
    //     } else if ($loans->loan_term == 'Four-Weeks') {
    //         $repayment_term = 'Four-Weeks';
    //     } else {
    //         $repayment_term = 'Yearly';
    //     }

    //     if (count($chargeDetail) > 2) {
    //         $result = $client->post($routeurl, [
    //             'headers' => [
    //                 'Content-Type' => 'application/x-www-form-urlencoded',
    //                 'Authorization' => 'Bearer ' . $authorization,
    //             ],
    //             'form_params' => [
    //                 'client_id' => $clientid,
    //                 'client_nrc_number' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_id)->first()->nrc == NULL ? DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_id)->first()->old_nrc : DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_id)->first()->nrc,
    //                 'client_name' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_id)->first()->name,
    //                 'client_phone' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_id)->first()->phone_primary,
    //                 'you_are_a_group_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_group_leader,
    //                 'you_are_a_center_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_center_leader,
    //                 'business_proposal' => 'business_proposal',
    //                 'guarantor_id' => $guarantor_a,
    //                 'guarantor2_id' => $guarantor_b,
    //                 'guarantor3_id' => $guarantor_c,
    //                 'disbursement_number' => $main_loan_generate_id,
    //                 'branch_id' => $branch_id,
    //                 'loan_officer_id' => $staffid,
    //                 'loan_production_id' => $loans->loan_type_id,
    //                 'loan_application_date' => $loans->loan_application_date,
    //                 'first_installment_date' => $loans->first_installment_date,
    //                 'loan_amount' => $loans->loan_amount,
    //                 'interest_rate' => $interest_rate,
    //                 'interest_rate_period' => $loans->interest_rate_period,
    //                 'loan_term' => $loans->loan_term,
    //                 'loan_term_value' => $loans->loan_term_value,
    //                 'repayment_term' => $repayment_term,
    //                 'currency_id' => 1,
    //                 'transaction_type_id' => 2,
    //                 'group_loan_id' => $groupid,
    //                 'center_leader_id' => $centerid,
    //                 'charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
    //                 'charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
    //                 'charge_id[6981667280779875]' => $chargeDetail[2]->charge_id,
    //                 'name[3061667280779394]' => $chargeDetail[0]->charge_name,
    //                 'name[6471667280779921]' => $chargeDetail[1]->charge_name,
    //                 'name[6981667280779875]' => $chargeDetail[2]->charge_name,
    //                 'amount[3061667280779394]' => $chargeDetail[0]->amount,
    //                 'amount[6471667280779921]' => $chargeDetail[1]->amount,
    //                 'amount[6981667280779875]' => $chargeDetail[2]->amount,
    //                 'charge_option[3061667280779394]' => $chargeDetail[0]->charge_option,
    //                 'charge_option[6471667280779921]' => $chargeDetail[1]->charge_option,
    //                 'charge_option[6981667280779875]' => $chargeDetail[2]->charge_option,
    //                 'charge_type[3061667280779394]' => $chargeDetail[0]->charge_type,
    //                 'charge_type[6471667280779921]' => $chargeDetail[1]->charge_type,
    //                 'charge_type[6981667280779875]' => $chargeDetail[2]->charge_type,
    //                 'status[3061667280779394]' => 'Yes',
    //                 'status[6471667280779921]' => 'Yes',
    //                 'status[6981667280779875]' => 'Yes',
    //                 // 'charge_id'=> $charge_detail->id,
    //                 // 'name'=> $charge_detail->charge_name, //'Sinking Fun(0.5%)'
    //                 // 'amount'=> $charge_detail->amount,//'0.5',
    //                 // 'charge_option'=> $charge_detail->charge_option,//'2',
    //                 // 'charge_type'=>$charge_detail->charge_type, //'1'
    //                 // 'status'=>$charge_detail->status,//'Yes',
    //                 'compulsory_id' => 5,
    //                 'compound_interest' => 1,
    //                 'override_cycle' => 'no',
    //                 'product_name' => 'Saving 5%',
    //                 'saving_amount' => 5,
    //                 'c_charge_option' => 2,
    //                 'c_interest_rate' => 1.25,
    //                 'compulsory_product_type_id' => 1,
    //                 'c_status' => 'Yes',
    //                 'loan_charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
    //                 'loan_charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
    //                 'loan_charge_id[6981667280779875]' => $chargeDetail[2]->charge_id,
    //                 // 'loan_charge_id'=> 'Null',
    //                 'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans->loan_officer_id)->first()->main_staff_id,
    //             ]
    //         ]);


    //         // return response()->json($response);
    //         // $getID = DB::table('tbl_main_join_loan')->where('portal_loan_id', $loan_id)->first();
    //         // if ($getID) {
    //         //     // activate
    //         //     if ($loans->disbursement_status == 'Approved') {
    //         //         DB::connection('main')->table('loans_' . $branch_id)->where('id', $getID->main_loan_id)->update([
    //         //             'disbursement_status' => 'Approved',
    //         //             'status_note_date_approve' => $loans->approved_date,
    //         //             'status_note_approve_by_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans->loan_officer_id)->first()->main_staff_id
    //         //         ]);
    //         //     }
    //         // }
    //         $dataloan = DB::connection('main')->table('loans_' . $branch_id)->orderBy('id', 'desc')->first();
    //         DB::table('tbl_main_join_loan')->insert([
    //             'main_loan_id' => $dataloan->id,
    //             'main_loan_code' => $dataloan->disbursement_number,
    //             'portal_loan_id' => $loan_id
    //         ]);
    //     } else {
    //         $result = $client->post($routeurl, [
    //             'headers' => [
    //                 'Content-Type' => 'application/x-www-form-urlencoded',
    //                 'Authorization' => 'Bearer ' . $authorization,
    //             ],
    //             'form_params' => [
    //                 'client_id' => $clientid,
    //                 'client_nrc_number' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_id)->first()->nrc == NULL ? DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_id)->first()->old_nrc : DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_id)->first()->nrc,
    //                 'client_name' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_id)->first()->name,
    //                 'client_phone' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_id)->first()->phone_primary,
    //                 'you_are_a_group_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_group_leader,
    //                 'you_are_a_center_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_center_leader,
    //                 'business_proposal' => 'business_proposal',
    //                 'guarantor_id' => $guarantor_a,
    //                 'guarantor2_id' => $guarantor_b,
    //                 'guarantor3_id' => $guarantor_c,
    //                 'disbursement_number' => $main_loan_generate_id,
    //                 'branch_id' => $branch_id,
    //                 'loan_officer_id' => $staffid,
    //                 'loan_production_id' => $loans->loan_type_id,
    //                 'loan_application_date' => $loans->loan_application_date,
    //                 'first_installment_date' => $loans->first_installment_date,
    //                 'loan_amount' => $loans->loan_amount,
    //                 'interest_rate' => $interest_rate,
    //                 'interest_rate_period' => $loans->interest_rate_period,
    //                 'loan_term' => $loans->loan_term,
    //                 'loan_term_value' => $loans->loan_term_value,
    //                 'repayment_term' => $repayment_term,
    //                 'currency_id' => 1,
    //                 'transaction_type_id' => 2,
    //                 'group_loan_id' => $groupid,
    //                 'center_leader_id' => $centerid,
    //                 'charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
    //                 'charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
    //                 'name[3061667280779394]' => $chargeDetail[0]->charge_name,
    //                 'name[6471667280779921]' => $chargeDetail[1]->charge_name,
    //                 'amount[3061667280779394]' => $chargeDetail[0]->amount,
    //                 'amount[6471667280779921]' => $chargeDetail[1]->amount,
    //                 'charge_option[3061667280779394]' => $chargeDetail[0]->charge_option,
    //                 'charge_option[6471667280779921]' => $chargeDetail[1]->charge_option,
    //                 'charge_type[3061667280779394]' => $chargeDetail[0]->charge_type,
    //                 'charge_type[6471667280779921]' => $chargeDetail[1]->charge_type,
    //                 'status[3061667280779394]' => 'Yes',
    //                 'status[6471667280779921]' => 'Yes',
    //                 // 'charge_id'=> $charge_detail->id,
    //                 // 'name'=> $charge_detail->charge_name, //'Sinking Fun(0.5%)'
    //                 // 'amount'=> $charge_detail->amount,//'0.5',
    //                 // 'charge_option'=> $charge_detail->charge_option,//'2',
    //                 // 'charge_type'=>$charge_detail->charge_type, //'1'
    //                 // 'status'=>$charge_detail->status,//'Yes',
    //                 'compulsory_id' => 5,
    //                 'compound_interest' => 1,
    //                 'override_cycle' => 'no',
    //                 'product_name' => 'Saving 5%',
    //                 'saving_amount' => 5,
    //                 'c_charge_option' => 2,
    //                 'c_interest_rate' => 1.25,
    //                 'compulsory_product_type_id' => 1,
    //                 'c_status' => 'Yes',
    //                 'loan_charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
    //                 'loan_charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
    //                 // 'loan_charge_id'=> 'Null',
    //                 'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans->loan_officer_id)->first()->main_staff_id,
    //             ]
    //         ]);


    //         // $getID = DB::table('tbl_main_join_loan')->where('portal_loan_id', $loan_id)->first();
    //         // if ($getID) {
    //         //     // activate
    //         //     if ($loans->disbursement_status == 'Approved') {
    //         //         DB::connection('main')->table('loans_' . $branch_id)->where('id', $getID->main_loan_id)->update([
    //         //             'disbursement_status' => 'Approved',
    //         //             'status_note_date_approve' => $loans->approved_date,
    //         //             'status_note_approve_by_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans->loan_officer_id)->first()->main_staff_id
    //         //         ]);
    //         //     }
    //         // }

    //         $dataloan = DB::connection('main')->table('loans_' . $branch_id)->orderBy('id', 'desc')->first();
    //         DB::table('tbl_main_join_loan')->insert([
    //             'main_loan_id' => $dataloan->id,
    //             'main_loan_code' => $dataloan->disbursement_number,
    //             'branch_id' => $branch_id,
    //             'portal_loan_id' => $loan_id
    //         ]);
    //     }

    //     $LoansSchedule = DB::connection('portal')->table($branch_code . '_loans_schedule')->where('loan_unique_id', $loan_id)->get();
    //     $main_disbursement_id = DB::connection('portal')->table('tbl_main_join_loan')->where('portal_loan_id', $loan_id)->first()->main_loan_id;
    //     $RpDetail = DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $main_disbursement_id)->get();

    //     for ($y = 0; $y < count($LoansSchedule); $y++) {
    //         if (DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$y]->id)->where('branch_id', $branch_id)->first()) {
    //             break;
    //         } else {
    //             if ($RpDetail[$y]->no == ($y + 1)) {
    //                 $mainJoin = new Crawl_Loan_Repayment_Schedule();
    //                 $mainJoin->portal_disbursement_id = $LoansSchedule[$y]->loan_unique_id;
    //                 $mainJoin->portal_disbursement_schedule_id = $LoansSchedule[$y]->id;
    //                 $mainJoin->main_disbursement_schedule_id = $RpDetail[$y]->id;
    //                 $mainJoin->branch_id = $branch_id;
    //                 $mainJoin->save();
    //             }
    //         }
    //     }



    //     $response = (string) $result->getBody();
    //     $response = json_decode($response);
    //     if ($response->status_code == 200) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }
    public function liveReverseLoans($request, $loan_id)
    {
        $route = $this->getMainUrl37();
        $route_name = 'create-loan';
        $routeurl = $route . $route_name;
        $authorization = $this->loginToken();

        $branch_id = DB::table('tbl_staff')->where('staff_code', '=', Session::get('staff_code'))->first()->branch_id;
        $branch_code = strtolower($this->getBranchCode($branch_id));
        $loans = DB::connection('portal')->table($branch_code . '_loans')
            ->leftJoin('tbl_client_basic_info', 'tbl_client_basic_info.client_uniquekey', '=', $branch_code . '_loans.client_id')
            ->where($branch_code . '_loans.loan_unique_id', $loan_id)
            // ->leftJoin($branch_code . '_loan_charges_compulsory_join', $branch_code . '_loan_charges_compulsory_join.loan_unique_id', '=', $branch_code . '_loans.loan_unique_id')
            // ->where($branch_code.'_loans.disbursement_status', 'Pending')
            // ->orwhere($branch_code.'_loans.disbursement_status', 'Approved')
            ->first();
        // dd($loans);
        

        $interest_rate = DB::connection('portal')->table('loan_type')->where('id', $loans->loan_type_id)->first()->interest_rate;

        // staff
        $staffid = DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans->loan_officer_id)->first();
        if ($staffid) {
            $staffid = $staffid->main_staff_id;
        } 

        // center
        $centerid = DB::table('tbl_main_join_center')->where('portal_center_id', $loans->center_id)->first();
        if ($centerid) {
            $centerid = $centerid->main_center_id;
        } 

        // group
        $groupid = DB::table('tbl_main_join_group')->where('portal_group_id', $loans->group_id)->first();
        if ($groupid) {
            $groupid = $groupid->main_group_id;
        } 

        // client
        $clientid = DB::table('tbl_main_join_client')->where('portal_client_id', $loans->client_id)->first();
        if ($clientid) {
            $clientid = $clientid->main_client_id;
        } 


        // Guarantor_a
        $guarantor_a = DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id', $loans->guarantor_a)->first();
        if ($guarantor_a) {
            $guarantor_a = $guarantor_a->main_guarantor_id;
        } 

        // Guarantor_b
        $guarantor_b = DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id', $loans->guarantor_b)->first();
        if ($guarantor_b) {
            $guarantor_b = $guarantor_b->main_guarantor_id;
        } 

        // Guarantor_c
        $guarantor_c = DB::table('tbl_main_join_guarantor')->where('portal_guarantor_id', $loans->guarantor_c)->first();
        if ($guarantor_c) {
            $guarantor_c = $guarantor_c->main_guarantor_id;
        } 


        // latest loan id generate
        $dataloan = DB::connection('main')->table('loans_' . $branch_id)->orderBy('id', 'desc')->first();
        $loanCount = DB::connection('main')->table('loans_' . $branch_id)->count();

        // $main_loan_generate_id = $dataloan->disbursement_number;
        // $bMainCode = DB::connection('main')->table('branches')->where('id', $y)
        $mainbcode = DB::connection('main')->table('branches')->where('id',$branch_id)->first()->code;
        $main_loan_generate_id = 'LID-' . $mainbcode . '-2023-';
        $num = $loanCount + 1;
        $main_loan_generate_id .= $num;

        // return response()->json($main_loan_generate_id);

        // save new loan
        $client = new \GuzzleHttp\Client(['verify' => false]);
        $routeurl = env('MAIN_URL') . "create-loan";


        $chargeDetail = DB::table($branch_code . '_loan_charges_compulsory_join')
            ->leftJoin('loan_charges_type', 'loan_charges_type.charge_code', '=', $branch_code . '_loan_charges_compulsory_join.loan_charge_id')
            ->where($branch_code . '_loan_charges_compulsory_join.loan_unique_id', $loans->loan_unique_id)
            ->where($branch_code . '_loan_charges_compulsory_join.loan_charge_id', '!=', NULL)
            ->select('loan_charges_type.*', 'loan_charges_type.id as charge_id')
            ->get();
        // return response()->json($loans[$i]->loan_term);

        $repayment_term = '';
        if ($loans->loan_term == 'Month') {
            $repayment_term = 'Monthly';
        } else if ($loans->loan_term == 'Day') {
            $repayment_term = 'Daily';
        } else if ($loans->loan_term == 'Week') {
            $repayment_term = 'Weekly';
        } else if ($loans->loan_term == 'Two-Weeks') {
            $repayment_term = 'Two-Weeks';
        } else if ($loans->loan_term == 'Four-Weeks') {
            $repayment_term = 'Four-Weeks';
        } else {
            $repayment_term = 'Yearly';
        }

        // if(DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->nrc){
        //     if(DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->old_nrc){
        //         $nrc=DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->old_nrc;
        //     }else{
        //         $nrc=NULL;
        //     }
        // }else{
        //     $nrc=DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->nrc;
        // }
        if($loans->nrc == NULL){
            if($loans->old_nrc == NULL){
                $nrc=NULL;
            }else{
                $nrc=$loans->old_nrc;
            }
        }else{
            $nrc=$loans->nrc;
        }

        if (count($chargeDetail) > 2) {
            $result = $client->post($routeurl, [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Authorization' => 'Bearer ' . $authorization,
                ],
                'form_params' => [
                    'client_id' => $clientid,
                    // 'client_nrc_number' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->nrc == NULL ? DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->old_nrc : DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->nrc,
                    'client_nrc_number'=>$nrc,
                    'client_name' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->name,
                    'client_phone' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->phone_primary,
                    'you_are_a_group_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_group_leader,
                    'you_are_a_center_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_center_leader,
                    'business_proposal' => 'business_proposal',
                    'guarantor_id' => $guarantor_a,
                    'guarantor2_id' => $guarantor_b,
                    'guarantor3_id' => $guarantor_c,
                    'disbursement_number' => $main_loan_generate_id,
                    'branch_id' => $branch_id,
                    'loan_officer_id' => $staffid,
                    'loan_production_id' => $loans->loan_type_id,
                    'loan_application_date' => $loans->loan_application_date,
                    'first_installment_date' => $loans->first_installment_date,
                    'loan_amount' => $loans->loan_amount,
                    'interest_rate' => $interest_rate,
                    'interest_rate_period' => $loans->interest_rate_period,
                    'loan_term' => $loans->loan_term,
                    'loan_term_value' => $loans->loan_term_value,
                    'repayment_term' => $repayment_term,
                    'currency_id' => 1,
                    'transaction_type_id' => 2,
                    'group_loan_id' => $groupid,
                    'center_leader_id' => $centerid,
                    'charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
                    'charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
                    'charge_id[6981667280779875]' => $chargeDetail[2]->charge_id,
                    'name[3061667280779394]' => $chargeDetail[0]->charge_name,
                    'name[6471667280779921]' => $chargeDetail[1]->charge_name,
                    'name[6981667280779875]' => $chargeDetail[2]->charge_name,
                    'amount[3061667280779394]' => $chargeDetail[0]->amount,
                    'amount[6471667280779921]' => $chargeDetail[1]->amount,
                    'amount[6981667280779875]' => $chargeDetail[2]->amount,
                    'charge_option[3061667280779394]' => $chargeDetail[0]->charge_option,
                    'charge_option[6471667280779921]' => $chargeDetail[1]->charge_option,
                    'charge_option[6981667280779875]' => $chargeDetail[2]->charge_option,
                    'charge_type[3061667280779394]' => $chargeDetail[0]->charge_type,
                    'charge_type[6471667280779921]' => $chargeDetail[1]->charge_type,
                    'charge_type[6981667280779875]' => $chargeDetail[2]->charge_type,
                    'status[3061667280779394]' => 'Yes',
                    'status[6471667280779921]' => 'Yes',
                    'status[6981667280779875]' => 'Yes',
                    // 'charge_id'=> $charge_detail->id,
                    // 'name'=> $charge_detail->charge_name, //'Sinking Fun(0.5%)'
                    // 'amount'=> $charge_detail->amount,//'0.5',
                    // 'charge_option'=> $charge_detail->charge_option,//'2',
                    // 'charge_type'=>$charge_detail->charge_type, //'1'
                    // 'status'=>$charge_detail->status,//'Yes',
                    'compulsory_id' => 5,
                    'compound_interest' => 1,
                    'override_cycle' => 'no',
                    'product_name' => 'Saving 5%',
                    'saving_amount' => 5,
                    'c_charge_option' => 2,
                    'c_interest_rate' => 1.25,
                    'compulsory_product_type_id' => 1,
                    'c_status' => 'Yes',
                    'loan_charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
                    'loan_charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
                    'loan_charge_id[6981667280779875]' => $chargeDetail[2]->charge_id,
                    // 'loan_charge_id'=> 'Null',
                    'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans->loan_officer_id)->first()->main_staff_id,
                ]
            ]);

            $response = (string) $result->getBody();
            // return response()->json($result);
            $response = json_decode($response); // Using this you can access any key like below
            // return response()->json(['status_code' => 200, 'message' => 're-crawling loan success', 'data' => $response]);
            if ($response->status_code == 200) {
                // return response()->json($response);
                $dataloan = DB::connection('main')->table('loans_' . $branch_id)->orderBy('id', 'desc')->first();
                DB::table('tbl_main_join_loan')->insert([
                    'main_loan_id' => $dataloan->id,
                    'main_loan_code' => $dataloan->disbursement_number,
                    'portal_loan_id' => $loans->loan_unique_id,
                    'branch_id' => $branch_id
                ]);
                $getID = DB::table('tbl_main_join_loan')->where('portal_loan_id', $loans->loan_unique_id)->where('branch_id', $branch_id)->first()->main_loan_id;

                //add scheduels join
                $LoansSchedule = DB::connection('portal')->table($branch_code . '_loans_schedule')->where('loan_unique_id', $loans->loan_unique_id)->get();
                $mainSchedule = DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $getID)->get();

                for ($t = 0; $t < count($LoansSchedule); $t++) {
                    if (DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$t]->id)->where('branch_id', $branch_id)->first()) {
                        break;
                    } else {
                        if ($mainSchedule[$t]->no == ($t + 1)) {
                            $mainJoinSchedule = new Crawl_Loan_Repayment_Schedule();
                            $mainJoinSchedule->portal_disbursement_id = $LoansSchedule[$t]->loan_unique_id;
                            $mainJoinSchedule->portal_disbursement_schedule_id = $LoansSchedule[$t]->id;
                            $mainJoinSchedule->main_disbursement_schedule_id = $mainSchedule[$t]->id;
                            $mainJoinSchedule->branch_id = $branch_id;
                            $mainJoinSchedule->save();
                        }
                    }
                }

            } else {
                return redirect('loan/')->with('Loan Create Fail');
            }
        } else {
            $result = $client->post($routeurl, [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Authorization' => 'Bearer ' . $authorization,
                ],
                'form_params' => [
                    'client_id' => $clientid,
                    'client_nrc_number' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->nrc == NULL ? DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->old_nrc : DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->nrc,
                    'client_name' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->name,
                    'client_phone' => DB::table('tbl_client_basic_info')->where('client_uniquekey', $loans->client_uniquekey)->first()->phone_primary,
                    'you_are_a_group_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_group_leader,
                    'you_are_a_center_leader' => DB::connection('main')->table('clients')->where('id', $clientid)->first()->you_are_a_center_leader,
                    'business_proposal' => 'business_proposal',
                    'guarantor_id' => $guarantor_a,
                    'guarantor2_id' => $guarantor_b,
                    'guarantor3_id' => $guarantor_c,
                    'disbursement_number' => $main_loan_generate_id,
                    'branch_id' => $branch_id,
                    'loan_officer_id' => $staffid,
                    'loan_production_id' => $loans->loan_type_id,
                    'loan_application_date' => $loans->loan_application_date,
                    'first_installment_date' => $loans->first_installment_date,
                    'loan_amount' => $loans->loan_amount,
                    'interest_rate' => $interest_rate,
                    'interest_rate_period' => $loans->interest_rate_period,
                    'loan_term' => $loans->loan_term,
                    'loan_term_value' => $loans->loan_term_value,
                    'repayment_term' => $repayment_term,
                    'currency_id' => 1,
                    'transaction_type_id' => 2,
                    'group_loan_id' => $groupid,
                    'center_leader_id' => $centerid,
                    'charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
                    'charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
                    'name[3061667280779394]' => $chargeDetail[0]->charge_name,
                    'name[6471667280779921]' => $chargeDetail[1]->charge_name,
                    'amount[3061667280779394]' => $chargeDetail[0]->amount,
                    'amount[6471667280779921]' => $chargeDetail[1]->amount,
                    'charge_option[3061667280779394]' => $chargeDetail[0]->charge_option,
                    'charge_option[6471667280779921]' => $chargeDetail[1]->charge_option,
                    'charge_type[3061667280779394]' => $chargeDetail[0]->charge_type,
                    'charge_type[6471667280779921]' => $chargeDetail[1]->charge_type,
                    'status[3061667280779394]' => 'Yes',
                    'status[6471667280779921]' => 'Yes',
                    // 'charge_id'=> $charge_detail->id,
                    // 'name'=> $charge_detail->charge_name, //'Sinking Fun(0.5%)'
                    // 'amount'=> $charge_detail->amount,//'0.5',
                    // 'charge_option'=> $charge_detail->charge_option,//'2',
                    // 'charge_type'=>$charge_detail->charge_type, //'1'
                    // 'status'=>$charge_detail->status,//'Yes',
                    'compulsory_id' => 5,
                    'compound_interest' => 1,
                    'override_cycle' => 'no',
                    'product_name' => 'Saving 5%',
                    'saving_amount' => 5,
                    'c_charge_option' => 2,
                    'c_interest_rate' => 1.25,
                    'compulsory_product_type_id' => 1,
                    'c_status' => 'Yes',
                    'loan_charge_id[3061667280779394]' => $chargeDetail[0]->charge_id,
                    'loan_charge_id[6471667280779921]' => $chargeDetail[1]->charge_id,
                    // 'loan_charge_id'=> 'Null',
                    'created_by' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans->loan_officer_id)->first()->main_staff_id,
                ]
            ]);

            $response = (string) $result->getBody();
            // return response()->json($result);
            $response = json_decode($response); // Using this you can access any key like below
            // return response()->json(['status_code' => 200, 'message' => 're-crawling loan success', 'data' => $response]);
            if ($response->status_code == 200) {
                // return response()->json($response);\
                $dataloan = DB::connection('main')->table('loans_' . $branch_id)->orderBy('id', 'desc')->first();
                DB::table('tbl_main_join_loan')->insert([
                    'main_loan_id' => $dataloan->id,
                    'main_loan_code' => $dataloan->disbursement_number,
                    'portal_loan_id' => $loans->loan_unique_id,
                    'branch_id' => $branch_id
                ]);
                $getID = DB::table('tbl_main_join_loan')->where('portal_loan_id', $loans->loan_unique_id)->where('branch_id', $branch_id)->first();
                    //add scheduels join
                    $LoansSchedule = DB::connection('portal')->table($branch_code . '_loans_schedule')->where('loan_unique_id', $loans->loan_unique_id)->get();
                    $mainSchedule = DB::connection('main')->table('loan_disbursement_calculate_' . $branch_id)->where('disbursement_id', $getID->main_loan_id)->get();

                    for ($t = 0; $t < count($LoansSchedule); $t++) {
                        if (DB::table('tbl_main_join_repayment_schedule')->where('portal_disbursement_schedule_id', $LoansSchedule[$t]->id)->where('branch_id', $branch_id)->first()) {
                            break;
                        } else {
                            if ($mainSchedule[$t]->no == ($t + 1)) {
                                $mainJoinSchedule = new Crawl_Loan_Repayment_Schedule();
                                $mainJoinSchedule->portal_disbursement_id = $LoansSchedule[$t]->loan_unique_id;
                                $mainJoinSchedule->portal_disbursement_schedule_id = $LoansSchedule[$t]->id;
                                $mainJoinSchedule->main_disbursement_schedule_id = $mainSchedule[$t]->id;
                                $mainJoinSchedule->branch_id = $branch_id;
                                $mainJoinSchedule->save();
                            }
                        }
                    }


                    // // activate
                    // if ($loans->disbursement_status == 'Approved') {
                    //     DB::connection('main')->table('loans_' . $branch_id)->where('id', $getID->main_loan_id)->update([
                    //         'disbursement_status' => 'Approved',
                    //         'status_note_date_approve' => $loans->approved_date,
                    //         'status_note_approve_by_id' => DB::table('tbl_main_join_staff')->where('portal_staff_id', $loans->loan_officer_id)->first()->main_staff_id
                    //     ]);
                    // }

                    // $dataloan = DB::connection('main')->table('loans_' . $y)->orderBy('id', 'desc')->first();
                    // DB::table('tbl_main_join_loan')->insert([
                    //     'main_loan_id' => $dataloan->id,
                    //     'main_loan_code' => $dataloan->disbursement_number,
                    //     'portal_loan_id' => $loans[$i]->loan_unique_id,
                    //     'branch_id' => $y
                    // ]);
                
            } else {
                return redirect('loan/')->with('Loan Create Fail');
            }
        }
    }
}
