<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class DontLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(session()->get('staff_code')) {
            return redirect('/dashboard');
        }
        return $next($request);
    }
}
