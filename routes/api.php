<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ClientController;
use App\Http\Controllers\Api\LoanController;
use App\Http\Controllers\Api\GuarantorController;

use App\Http\Controllers\Api\DataEntry\DataEntryController;
use App\Http\Controllers\Crawl\DataEntryCrawlController as CrawlDataEntryCrawlController;
use App\Http\Controllers\Crawl\LoanCrawlController;
use App\Http\Controllers\Crawl\DepositCrawlController;
use App\Http\Controllers\Crawl\DisbursementCrawlController;
use App\Http\Controllers\Crawl\RepaymentCrawlController;
use App\Http\Controllers\Crawl\SavingWithdrawlCrawlController;
use App\Http\Controllers\Crawl\HolidayCrawlController;
use App\Http\Controllers\Crawl\ClientCrawlController;
use App\Http\Controllers\ReverseCrawl\RevCrawlingController;
use App\Http\Controllers\ReverseCrawl\ClientReverseCrawl;
use App\Http\Controllers\ReverseCrawl\LoanReverseCrawl;
use App\Http\Controllers\ReverseCrawl\DepositReverseCrawl;
use App\Http\Controllers\ReverseCrawl\DisbursementReverseCrawl;

use App\Http\Controllers\ReverseCrawl\RepaymentReverseCrawl;
use App\Http\Controllers\ReverseCrawl\SavingReverseCrawl;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('test/b', [ClientController::class, 'getBranchID']);
Route::post('client/create', [ClientController::class, 'createClient']);
Route::post('loan/create', [LoanController::class, 'createLoan']);
Route::post('guarantors/create', [GuarantorController::class, 'createGuarantor']);

//Route::group(['middleware' => ['ApiSecreteKey']], function () {
    Route::get('staffs',[CrawlDataEntryCrawlController::class,'crawlStaffs'])->name('crawlStaffs');
    Route::get('clients',[ClientCrawlController::class,'crawlClients'])->name('crawlClient');
    Route::get('centers',[CrawlDataEntryCrawlController::class,'crawlCenters'])->name('crawlCenters');
    Route::get('groups',[CrawlDataEntryCrawlController::class,'crawlGroups'])->name('crawlGroups');
    Route::get('guarantors',[CrawlDataEntryCrawlController::class,'crawlGuarantors'])->name('crawlGuarantors');
    Route::get('loans',[LoanCrawlController::class,'crawlLoans'])->name('crawlLoan');
    Route::get('deposits',[DepositCrawlController::class,'crawlDeposits'])->name('crawlDeposits');
    Route::get('disbursements',[DisbursementCrawlController::class,'crawlDisbursements'])->name('crawlDisbursements');
    Route::get('repayments',[RepaymentCrawlController::class,'crawlRepayments'])->name('crawlRepayments');
    Route::get('savings',[SavingWithdrawlCrawlController::class,'crawlSavingWithdrawls'])->name('crawlSavingWithdrawls');
    Route::get('holidays', [HolidayCrawlController::class, 'crawlHolidays'])->name('crawlHolidays');

    Route::get('staffs/reverse',[RevCrawlingController::class,'reverseCrawlStaffs'])->name('reverseCrawlStaffs');
	Route::get('centers/reverse',[RevCrawlingController::class,'reverseCrawlCenters'])->name('reverseCrawlCenters');
    Route::get('groups/reverse',[RevCrawlingController::class,'reverseCrawlGroups'])->name('reverseCrawlGroups');
    Route::get('guarantors/reverse',[RevCrawlingController::class,'reverseCrawlGuarantors'])->name('reverseCrawlGuarantors');
    Route::get('clients/reverse',[ClientReverseCrawl::class,'reverseCrawlClients'])->name('reverseCrawlClients');
    Route::get('loans/reverse',[LoanReverseCrawl::class,'reverseCrawlLoans'])->name('reverseCrawlLoans');
    Route::get('deposit/reverse',[DepositReverseCrawl::class,'reverseCrawlDeposit'])->name('reverseCrawlDeposit');
    Route::get('disbursements/reverse',[DisbursementReverseCrawl::class,'reverseCrawlDisbursement'])->name('reverseCrawlDisbursement');
    Route::get('repayments/reverse',[RepaymentReverseCrawl::class,'reverseCrawlRepayment'])->name('reverseCrawlRepayment');
    Route::get('repayments/detail',[RepaymentCrawlController::class,'getRepaymentDetail'])->name('getRepaymentDetail');
    Route::get('savings/reverse',[SavingReverseCrawl::class,'reverseCrawlSaving'])->name('reverseSaving');

    
//});