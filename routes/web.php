 	<?php

use App\Http\Controllers\Client\ClientController;

use App\Http\Controllers\ClientDataEntery\BranchController;
use App\Http\Controllers\ClientDataEntery\BusinessTypeController;
use App\Http\Controllers\ClientDataEntery\BusinessCategoryController;
use App\Http\Controllers\ClientDataEntery\CenterController;
use App\Http\Controllers\ClientDataEntery\CityController;
use App\Http\Controllers\ClientDataEntery\DepartmentController;
use App\Http\Controllers\ClientDataEntery\DistrictController;
use App\Http\Controllers\ClientDataEntery\DivisionController;
use App\Http\Controllers\ClientDataEntery\EducationController;
use App\Http\Controllers\ClientDataEntery\GroupController;
use App\Http\Controllers\ClientDataEntery\GuarantorController;
use App\Http\Controllers\ClientDataEntery\IndustryController;
use App\Http\Controllers\ClientDataEntery\JobPositionController;
use App\Http\Controllers\ClientDataEntery\ProvinceController;
use App\Http\Controllers\ClientDataEntery\QuarterController;
use App\Http\Controllers\ClientDataEntery\StaffController;
use App\Http\Controllers\ClientDataEntery\TowhshipController;
use App\Http\Controllers\ClientDataEntery\VillageController;
use App\Http\Controllers\ClientDataEntery\AnnouncementController;
use App\Http\Controllers\ClientDataEntery\NewsController;
use App\Http\Controllers\ClientDataEntery\CompanyController;
use App\Http\Controllers\ClientDataEntery\AnnounceDepartmentController;
use App\Http\Controllers\Report\ClientReportController;
use App\Http\Controllers\Report\LoanReportController;
use App\Http\Controllers\Report\RepaymentReportController;
use App\Http\Controllers\LoanDepositController;
use App\Http\Controllers\LoanDataEntery\LoanApprovalController;
use App\Http\Controllers\AccountDataEntery\AccountController;

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CustomAuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Crawl\DataEntryCrawlController;
use App\Http\Controllers\Loan\LoanController;

use App\Http\Controllers\LoanDataEntery\LoanCalendarController;
use App\Http\Controllers\LoanDataEntery\LoanTypeController;
use App\Http\Controllers\LoanDataEntery\SavingWithdrawlController;
use App\Http\Controllers\LoanDisbursementController;
use App\Http\Controllers\LoanDataEntery\LoanRepaymentController;
use App\Http\Controllers\LoanDataEntery\LoanChargeController;
use App\Http\Controllers\LoanDataEntery\LoanCompulsoryController;

use App\Http\Controllers\Crawl\ClientCrawlController;
use App\Http\Controllers\Crawl\LoanCrawlController;
use App\Http\Controllers\Crawl\DepositCrawlController;
use App\Http\Controllers\Crawl\DisbursementCrawlController;
use App\Http\Controllers\Crawl\RepaymentCrawlController;
use App\Http\Controllers\Crawl\SavingWithdrawlCrawlController;

use App\Http\Controllers\ReverseCrawl\ClientReverseCrawl;
use App\Http\Controllers\ReverseCrawl\SavingReverseCrawl;
use Illuminate\Support\Facades\DB;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
})->middleware('dontLogin');

Route::get('/branch_crawl', [DataEntryCrawlController::class, 'crawlBranches']);


//Auth
Route::get('login', [CustomAuthController::class, 'index'])->name('login')->middleware('dontLogin');
Route::post('custom-login', [CustomAuthController::class, 'customLogin'])->name('login.custom');
Route::get('signout', [CustomAuthController::class, 'signOut'])->name('signout');

Route::middleware(['logout'])->group(function() {
//Dashboard
Route::get('/dashboard', [DashboardController::class, 'index']);

//Client
Route::get('/client',[ClientController::class,'index']);
Route::get('/client/create',[ClientController::class,'create']);
Route::post('/client/store',[ClientController::class,'store'])->name('client.store');
Route::get('/client/{id}/edit',[ClientController::class, 'edit']);
Route::put('/client/{id}', [ClientController::class,'update'])->name('client.update');
Route::get('/client/view/{id}',[ClientController::class, 'view']);
Route::delete('/client/destroy/{id}', [ClientController::class,'destroy']);

//centerleaders
Route::get('/center_leaders',[ClientController::class,'getCenterLeaders']);

//Route::get('autocomplete', [ClientController::class,'autocomplete'])->name('autocomplete');// Search
// client ajax
// Route::get('getYearlySalary',[ClientController::class, 'getYearlySalary'])->name('getYearlySalary');


//Client Data Entery
//branches
Route::get('/branches',[BranchController::class,'index']);
Route::get('/branches/create',[BranchController::class,'create']);
Route::post('/branches',[BranchController::class,'store'])->name('branches.store');
Route::get('/branches/{id}/edit',[BranchController::class, 'edit']);
Route::put('/branches/{id}', [BranchController::class,'update'])->name('branches.update');
Route::get('/branches/view/{id}',[BranchController::class, 'view']);
Route::delete('/branches/destroy/{id}', [BranchController::class,'destroy']);
//centers
Route::get('/centers',[CenterController::class,'index']);
Route::get('/centers/create',[CenterController::class,'create']);
Route::post('/centers',[CenterController::class,'store'])->name('centers.store');
Route::get('/centers/{id}/edit',[CenterController::class, 'edit']);
Route::put('/centers/{id}', [CenterController::class,'update'])->name('centers.update');
Route::get('/centers/view/{id}',[CenterController::class, 'view']);
Route::delete('/centers/destroy/{id}', [CenterController::class,'destroy']);
//cities
Route::get('/cities',[CityController::class,'index']);
Route::get('/cities/create',[CityController::class,'create']);
Route::post('/cities',[CityController::class,'store'])->name('cities.store');
Route::get('/cities/{id}/edit',[CityController::class, 'edit']);
Route::put('/cities/{id}', [CityController::class,'update'])->name('cities.update');
Route::get('/cities/view/{id}',[CityController::class, 'view']);
Route::delete('/cities/destroy/{id}', [CityController::class,'destroy']);
//departments
Route::get('/departments',[DepartmentController::class,'index']);
Route::get('/departments/create',[DepartmentController::class,'create']);
Route::post('/departments',[DepartmentController::class,'store'])->name('departments.store');
Route::get('/departments/{id}/edit',[DepartmentController::class, 'edit']);
Route::put('/departments/{id}', [DepartmentController::class,'update'])->name('departments.update');
Route::get('/departments/view/{id}',[DepartmentController::class, 'view']);
Route::delete('/departments/destroy/{id}', [DepartmentController::class,'destroy']);
//districts
Route::get('/districts',[DistrictController::class,'index']);
Route::get('/districts/create',[DistrictController::class,'create']);
Route::post('/districts',[DistrictController::class,'store'])->name('districts.store');
Route::get('/districts/{id}/edit',[DistrictController::class, 'edit']);
Route::put('/districts/{id}', [DistrictController::class,'update'])->name('districts.update');
Route::get('/districts/view/{id}',[DistrictController::class, 'view']);
Route::delete('/districts/destroy/{id}', [DistrictController::class,'destroy']);
//divisions
Route::get('/divisions',[DivisionController::class,'index']);
Route::get('/divisions/create',[DivisionController::class,'create']);
Route::post('/divisions',[DivisionController::class,'store'])->name('divisions.store');
Route::get('/divisions/{id}/edit',[DivisionController::class, 'edit']);
Route::put('/divisions/{id}', [DivisionController::class,'update'])->name('divisions.update');
Route::get('/divisions/view/{id}',[DivisionController::class, 'view']);
Route::delete('/divisions/destroy/{id}', [DivisionController::class,'destroy']);
//educations
Route::get('/educations',[EducationController::class,'index']);
Route::get('/educations/create',[EducationController::class,'create']);
Route::post('/educations',[EducationController::class,'store'])->name('educations.store');
Route::get('/educations/{id}/edit',[EducationController::class, 'edit']);
Route::put('/educations/{id}', [EducationController::class,'update'])->name('educations.update');
Route::get('/educations/view/{id}',[EducationController::class, 'view']);
Route::delete('/educations/destroy/{id}', [EducationController::class,'destroy']);
//groups
Route::get('/groups',[GroupController::class,'index']);
Route::get('/groups/create',[GroupController::class,'create']);
Route::post('/groups',[GroupController::class,'store'])->name('groups.store');
Route::get('/groups/{id}/edit',[GroupController::class, 'edit']);
Route::put('/groups/{id}', [GroupController::class,'update'])->name('groups.update');
Route::get('/groups/view/{id}',[GroupController::class, 'view']);
Route::delete('/groups/destroy/{id}', [GroupController::class,'destroy']);
Route::post('getLatestId',[GroupController::class, 'getLatestId'])->name('getLatestId');

//guarantors
Route::get('/guarantors',[GuarantorController::class,'index']);
Route::get('/guarantors/create',[GuarantorController::class,'create']);
Route::post('/guarantors',[GuarantorController::class,'store'])->name('guarantors.store');
Route::get('/guarantors/{id}/edit',[GuarantorController::class, 'edit']);
Route::put('/guarantors/{id}', [GuarantorController::class,'update'])->name('guarantors.update');
Route::get('/guarantors/view/{id}',[GuarantorController::class, 'view']);
Route::delete('/guarantors/destroy/{id}', [GuarantorController::class,'destroy']);
//industries
Route::get('/industries',[IndustryController::class,'index']);
Route::get('/industries/create',[IndustryController::class,'create']);
Route::post('/industries',[IndustryController::class,'store'])->name('industries.store');
Route::get('/industries/{id}/edit',[IndustryController::class, 'edit']);
Route::put('/industries/{id}', [IndustryController::class,'update'])->name('industries.update');
Route::get('/industries/view/{id}',[IndustryController::class, 'view']);
Route::delete('/industries/destroy/{id}', [IndustryController::class,'destroy']);
//jobPositions
Route::get('/jobpositions',[JobPositionController::class,'index']);
Route::get('/jobpositions/create',[JobPositionController::class,'create']);
Route::post('/jobpositions',[JobPositionController::class,'store'])->name('jobpositions.store');
Route::get('/jobpositions/{id}/edit',[JobPositionController::class, 'edit']);
Route::put('/jobpositions/{id}', [JobPositionController::class,'update'])->name('jobpositions.update');
Route::get('/jobpositions/view/{id}',[JobPositionController::class, 'view']);
Route::delete('/jobpositions/destroy/{id}', [JobPositionController::class,'destroy']);
//provinces
Route::get('/provinces',[ProvinceController::class,'index']);
Route::get('/provinces/create',[ProvinceController::class,'create']);
Route::post('/provinces',[ProvinceController::class,'store'])->name('provinces.store');
Route::get('/provinces/{id}/edit',[ProvinceController::class, 'edit']);
Route::put('/provinces/{id}', [ProvinceController::class,'update'])->name('provinces.update');
Route::get('/provinces/view/{id}',[ProvinceController::class, 'view']);
Route::delete('/provinces/destroy/{id}', [ProvinceController::class,'destroy']);
//quarters
Route::get('/quarters',[QuarterController::class,'index']);
Route::get('/quarters/create',[QuarterController::class,'create']);
Route::post('/quarters',[QuarterController::class,'store'])->name('quarters.store');
Route::get('/quarters/{id}/edit',[QuarterController::class, 'edit']);
Route::put('/quarters/{id}', [QuarterController::class,'update'])->name('quarters.update');
Route::get('/quarters/view/{id}',[QuarterController::class, 'view']);
Route::delete('/quarters/destroy/{id}', [QuarterController::class,'destroy']);
//staffs
Route::get('/staffs',[StaffController::class,'index']);
Route::get('/staffs/create',[StaffController::class,'create']);
Route::post('/staffs',[StaffController::class,'store'])->name('staffs.store');
Route::get('/staffs/{id}/edit',[StaffController::class, 'edit']);
Route::put('/staffs/{id}', [StaffController::class,'update'])->name('staffs.update');
Route::get('/staffs/view/{id}',[StaffController::class, 'view']);
Route::delete('/staffs/destroy/{id}', [StaffController::class,'destroy']);
//townships
Route::get('/townships',[TowhshipController::class,'index']);
Route::get('/townships/create',[TowhshipController::class,'create']);
Route::post('/townships',[TowhshipController::class,'store'])->name('townships.store');
Route::get('/townships/{id}/edit',[TowhshipController::class, 'edit']);
Route::put('/townships/{id}', [TowhshipController::class,'update'])->name('townships.update');
Route::get('/townships/view/{id}',[TowhshipController::class, 'view']);
Route::delete('/townships/destroy/{id}', [TowhshipController::class,'destroy']);
//villages
Route::get('/villages',[VillageController::class,'index']);
Route::get('/villages/create',[VillageController::class,'create']);
Route::post('/villages',[VillageController::class,'store'])->name('villages.store');
Route::get('/villages/{id}/edit',[VillageController::class, 'edit']);
Route::put('/villages/{id}', [VillageController::class,'update'])->name('villages.update');
Route::get('/villages/view/{id}',[VillageController::class, 'view']);
Route::delete('/villages/destroy/{id}', [VillageController::class,'destroy']);


// For Job Json Dropdown
Route::get('getDept',[ClientController::class, 'getDept'])->name('getDept');
Route::get('getJob',[ClientController::class, 'getJob'])->name('getJob');
Route::get('getStaffClient',[CenterController::class, 'getStaffClient'])->name('getStaffClient');
Route::get('getNRC',[StaffController::class, 'getNRC'])->name('getNRC');

//for Address Json
Route::get('getDistrict',[ClientController::class, 'getDistrict'])->name('getDistrict');
Route::get('getCity',[ClientController::class, 'getCity'])->name('getCity');
Route::get('getTownship',[ClientController::class, 'getTownship'])->name('getTownship');
Route::get('getProvince',[ClientController::class, 'getProvince'])->name('getProvince');
Route::get('getVillage',[ClientController::class, 'getVillage'])->name('getVillage');
Route::get('getQuarter',[ClientController::class, 'getQuarter'])->name('getQuarter');

//for group dropdown
Route::get('getCenter',[ClientController::class, 'getCenter'])->name('getCenter');
Route::get('getStaff',[ClientController::class, 'getStaff'])->name('getStaff');
Route::get('getGroupp',[ClientController::class, 'getGroupp'])->name('getGroupp');
Route::get('getLO',[ClientController::class, 'getLO'])->name('getLO');




// For Loan
Route::get('/loan',[LoanController::class,'index']);
Route::get('/loan/create',[LoanController::class,'create']);
Route::post('/loan',[LoanController::class,'store'])->name('loan.store');
// Route::get('/loan/{id}/edit',[LoanController::class, 'edit']);
// Route::put('/loan/{id}', [LoanController::class,'update'])->name('loan.update');
Route::get('/loan/view/{id}',[LoanController::class, 'view']);
// Route::delete('/loan/destroy/{id}', [LoanController::class,'destroy']);
//loan deposit
Route::get('/loandeposit/{id}', [LoanDepositController::class, 'create'])->name('loandeposit.create');
Route::get('/group-deposit' , [LoanDepositController::class, 'groupindex'])->name('depositgroup.index'); 
Route::post('/groupdeposit/create', [LoanDepositController::class, 'groupcreate'])->name('groupdeposit.create');
Route::post('/loandeposit', [LoanDepositController::class, 'store'])->name('loandeposit.store');
Route::post('/group-deposit' , [LoanDepositController::class, 'groupstore'])->name('groupdeposit.store'); 

//loan approval
Route::post('/loan_approval', [LoanApprovalController::class, 'store'])->name('loan_approval.store');
Route::post('/loan_approval_single', [LoanApprovalController::class, 'approveSingle'])->name('loan_approval.approveSingle');

Route::get('/loan_approval/cancel/{id}', [LoanApprovalController::class, 'cancel']);

//loan disbursement
Route::post('/loan_disbursements', [LoanDisbursementController::class, 'store'])->name('loan_disbursements.store');
Route::get('/loan_disbursements', [LoanDisbursementController::class , 'index']);
Route::get('/loan_disbursements/group', [LoanDisbursementController::class , 'groupDisburse']);
Route::post('/loan_disbursements/group/create', [LoanDisbursementController::class, 'groupDisburseCreate'])->name('loan_disbursements.group_disburse_create');
Route::post('/loan_disbursements/group/store', [LoanDisbursementController::class, 'groupStore'])->name('loan_disbursements.group_disburse_store');
Route::get('/loan_disbursements/create/{id}', [LoanDisbursementController::class, 'create'])->name('loandisbursement.create');
Route::get('/loan_disbursements/cancel/{id}', [LoanDisbursementController::class, 'cancel']);

//loan cancel
Route::get('/loancancel/{id}/{staff_code}', [LoanController::class, 'loanCancel'])->name('loancancel');
//loan type
Route::get('/loantypes',[LoanTypeController::class,'index']);
Route::get('/loantypes/create',[LoanTypeController::class,'create']);
Route::post('/loantypes',[LoanTypeController::class,'store'])->name('loantypes.store');
Route::get('/loantypes/{id}/edit',[LoanTypeController::class, 'edit']);
Route::put('/loantypes/{id}', [LoanTypeController::class,'update'])->name('loantypes.update');
Route::get('/loantypes/view/{id}',[LoanTypeController::class, 'view']);
Route::delete('/loantypes/destroy/{id}', [LoanTypeController::class,'destroy']);

//loan Calendar
Route::get('/loancalendars',[LoanCalendarController::class,'index']);
Route::get('/loancalendars/{id}/edit',[LoanCalendarController::class, 'edit']);
Route::patch('/loancalendars/{id}', [LoanCalendarController::class,'update'])->name('loancalendars.update');

//For Business Category json
Route::get('getBusinessCategory',[LoanController::class, 'getBusinessCategory'])->name('getBusinessCategory');



// calculate schedule
Route::get('getajaxLoanSchedule',[LoanController::class, 'getLoanSchedule'])->name('getLoanSchedule');
Route::get('getChargesCompulsory', [LoanController::class, 'getChargesCompulsory'])->name('getChargesCompulsory');
Route::post('getClientInfo',[LoanController::class, 'getClientInfo'])->name('getClientInfo');
Route::post('getGuarantorAInfo',[LoanController::class, 'getGuarantorAInfo'])->name('getGuarantorAInfo');
Route::post('getGuarantorBInfo',[LoanController::class, 'getGuarantorBInfo'])->name('getGuarantorBInfo');
Route::post('getGuarantorCInfo',[LoanController::class, 'getGuarantorCInfo'])->name('getGuarantorCInfo');
Route::post('getSingleClientInfo',[LoanController::class, 'getSingleClientInfo'])->name('getSingleClientInfo');
Route::post('getSavingAmount',[LoanController::class, 'getSavingAmount'])->name('getSavingAmount');
Route::post('getSingleGuarantorAInfo',[LoanController::class, 'getSingleGuarantorAInfo'])->name('getSingleGuarantorAInfo');
Route::post('getSingleGuarantorBInfo',[LoanController::class, 'getSingleGuarantorBInfo'])->name('getSingleGuarantorBInfo');
Route::post('getSingleGuarantorCInfo',[LoanController::class, 'getSingleGuarantorCInfo'])->name('getSingleGuarantorCInfo');
Route::get('getLoanTypeInfo',[LoanController::class, 'getLoanTypeInfo'])->name('getLoanTypeInfo');
Route::get('getLoanInfo',[LoanController::class, 'getLoanInfo'])->name('getLoanInfo');


//company
Route::get('/companies', [CompanyController::class, 'index']);
Route::get('/companies/create', [CompanyController::class, 'create']);
Route::post('/companies', [CompanyController::class, 'store'])->name('companies.store');
Route::get('/companies/{id}/edit', [CompanyController::class, 'edit']);
Route::put('/companies/{id}', [CompanyController::class, 'update'])->name('companies.update');
Route::get('/companies/view/{id}', [CompanyController::class, 'view']);
Route::delete('/companies/destroy/{id}', [CompanyController::class, 'destroy']);

//news
Route::get('/news', [NewsController::class, 'index']);
Route::get('/news/create', [NewsController::class, 'create']);
Route::post('/news', [NewsController::class, 'store'])->name('news.store');
Route::get('/news/{id}/edit', [NewsController::class, 'edit']);
Route::put('/news/{id}', [NewsController::class, 'update'])->name('news.update');
Route::get('/news/view/{id}', [NewsController::class, 'view']);
Route::delete('/news/destroy/{id}', [NewsController::class, 'destroy']);

//announcements
Route::get('/announcements', [AnnouncementController::class, 'index']);
Route::get('/announcements/create', [AnnouncementController::class, 'create']);
Route::post('/announcements', [AnnouncementController::class, 'store'])->name('announcements.store');
Route::get('/announcements/{id}/edit', [AnnouncementController::class, 'edit']);
Route::put('/announcements/{id}', [AnnouncementController::class, 'update'])->name('announcements.update');
Route::get('/announcements/view/{id}', [AnnouncementController::class, 'view']);
Route::delete('/announcements/destroy/{id}', [AnnouncementController::class, 'destroy']);

//announce_departments
Route::get('/announce_departments', [AnnounceDepartmentController::class , 'index']);
Route::get('/announce_departments/create', [AnnounceDepartmentController::class, 'create']);
Route::post('/announce_departments', [AnnounceDepartmentController::class, 'store'])->name('announce_departments.store');
Route::get('/announce_departments/{id}/edit', [AnnounceDepartmentController::class, 'edit']);
Route::put('/announce_departments/{id}', [AnnounceDepartmentController::class, 'update'])->name('announce_departments.update');
Route::get('/announce_departments/view/{id}', [AnnounceDepartmentController::class, 'view']);
Route::delete('/announce_departments/destroy/{id}', [AnnounceDepartmentController::class, 'destroy']);

//business type & category
Route::get('/business_types', [BusinessTypeController::class , 'index']);
Route::get('/business_types/create', [BusinessTypeController::class, 'create']);
Route::post('/business_types', [BusinessTypeController::class, 'store'])->name('business_types.store');
Route::get('/business_types/{id}/edit', [BusinessTypeController::class, 'edit']);
Route::put('/business_types/{id}', [BusinessTypeController::class, 'update'])->name('business_types.update');
Route::get('/business_types/view/{id}', [BusinessTypeController::class, 'view']);
Route::delete('/business_types/destroy/{id}', [BusinessTypeController::class, 'destroy']);

Route::get('/business_categories', [BusinessCategoryController::class , 'index']);
Route::get('/business_categories/create', [BusinessCategoryController::class, 'create']);
Route::post('/business_categories', [BusinessCategoryController::class, 'store'])->name('business_categories.store');
Route::get('/business_categories/{id}/edit', [BusinessCategoryController::class, 'edit']);
Route::put('/business_categories/{id}', [BusinessCategoryController::class, 'update'])->name('business_categories.update');
Route::get('/business_categories/view/{id}', [BusinessCategoryController::class, 'view']);
Route::delete('/business_categories/destroy/{id}', [BusinessCategoryController::class, 'destroy']);

    // Test for Repayment
//    Route::get('/loan/test',[LoanController::class,'index_test']);
    Route::post('/loan_repayment/create', [LoanRepaymentController::class, 'create'])->name("loan_repayment.create");
    Route::get('getLoanSchedule', [LoanRepaymentController::class, 'getLoanSchedule']);

    Route::post('/loan_repayment', [LoanRepaymentController::class, 'store'])->name('loan_repayment.store');

    //loan charges
Route::get('/loancharges',[LoanChargeController::class, 'index']);
Route::get('/loancharges/create',[LoanChargeController::class, 'create']);
Route::post('/loancharges',[LoanChargeController::class, 'store'])->name('loancharges.store');
Route::get('/loancharges/{id}/edit', [LoanChargeController::class, 'edit']);
Route::put('/loancharges/{id}', [LoanChargeController::class, 'update'])->name('loancharges.update');
Route::get('/loancharges/view/{id}', [LoanChargeController::class, 'view']);
Route::delete('/loancharges/destroy/{id}', [LoanChargeController::class, 'destroy']);
//loan compulsory
Route::get('/loancompulsory', [LoanCompulsoryController::class, 'index']);
Route::get('/loancompulsory/create', [LoanCompulsoryController::class, 'create']);
Route::post('/loancompulsory', [LoanCompulsoryController::class, 'store'])->name('loancompulsory.store');
Route::get('/loancompulsory/{id}/edit', [LoanCompulsoryController::class, 'edit']);
Route::put('/loancompulsory/{id}', [LoanCompulsoryController::class, 'update'])->name('loancompulsory.update');
Route::get('/loancompulsory/view/{id}', [LoanCompulsoryController::class, 'view']);
Route::delete('/loancompulsory/destroy/{id}', [LoanCompulsoryController::class, 'destroy']);

//client_reports
Route::get('/client_reports', [ClientReportController::class, 'index'])->name('client_reports');
//loan_reports
Route::get('/loan_reports', [LoanReportController::class, 'index'])->name('loan_reports');
//repayment_reports
Route::get('/repayment_reports/view/{id}',[RepaymentReportController::class, 'view']);

//account
Route::get('/accounts', [AccountController::class, 'index']);
Route::get('/accounts/create', [AccountController::class, 'create']);
Route::post('/accounts', [AccountController::class, 'store'])->name('accounts.store');
Route::get('/accounts/{id}/edit', [AccountController::class, 'edit']);
Route::put('/accounts/{id}', [AccountController::class, 'update'])->name('accounts.update');
Route::get('accounts/view/{id}', [AccountController::class, 'view']);
Route::delete('/accounts/destroy/{id}', [AccountController::class, 'destroy']);

//For Frd Code json
Route::get('getFrdCode',[AccountController::class, 'getFrdCode'])->name('getFrdCode');

//Saving Withdrawl
Route::get('/savingwithdrawl/create',[SavingWithdrawlController::class, 'create']);
Route::post('/getSavingWithdrawlInfo',[SavingWithdrawlController::class, 'getSavingWithdrawlInfo'])->name('getSavingWithdrawlInfo');
Route::put('/updateSavingWithdrawl', [SavingWithdrawlController::class,'update'])->name('savingWithdrawl.update');
Route::post('getSavingClientInfo',[SavingWithdrawlController::class, 'getSavingClientInfo'])->name('getSavingClientInfo');
Route::post('/getSavingWithdrawlInterest',[SavingWithdrawlController::class, 'getSavingWithdrawlInterest'])->name('getSavingWithdrawlInterest');

Route::get('staff/uniqueid', [StaffController::class, 'getUniqueID'])->name('staffUniqueID');
Route::get('center/uniqueid', [CenterController::class, 'getUniqueID'])->name('centerUniqueID');
Route::get('group/uniqueid', [GroupController::class, 'getUniqueID'])->name('groupUniqueID');
Route::get('guarantor/uniqueid', [GuarantorController::class, 'getUniqueID'])->name('guarantorUniqueID');

//change branch
Route::post('/change_branch', [DashboardController::class, 'changeBranch'])->name('changeBranch');

//Crawling
Route::get('/crawl', function() {
    $crawls = DB::table('tbl_crawling')->where('module_name', 'NOT LIKE', 'rev%')->orderBy('order_list', 'asc')->get();
    return view('crawling.crawl.index', compact('crawls'));
});

//Reverse Crawling
Route::get('/reverse_crawl', function() {
    $reverse_crawls = DB::table('tbl_crawling')->where('module_name', 'LIKE', 'rev%')->orderBy('order_list', 'asc')->get();
    return view('crawling.reverse_crawl.index', compact('reverse_crawls'));
});

//Crawling
Route::get('/staff_crawl', [DataEntryCrawlController::class , 'staffIndex']);
Route::get('/center_crawl', [DataEntryCrawlController::class , 'centerIndex']);
Route::get('/group_crawl', [DataEntryCrawlController::class , 'groupIndex']);
Route::get('/guarantor_crawl', [DataEntryCrawlController::class , 'guarantorIndex']);
Route::get('/client_crawl', [ClientCrawlController::class , 'index']);
Route::get('/loan_crawl', [LoanCrawlController::class, 'index']);
Route::get('/repayment_crawl', [RepaymentCrawlController::class, 'index']);
Route::get('/deposit_crawl', [DepositCrawlController::class, 'index']);
Route::get('/disbursement_crawl', [DisbursementCrawlController::class, 'index']);
Route::get('/repayment_crawl', [RepaymentCrawlController::class, 'index']);
Route::get('/savingwithdrawl_crawl', [SavingWithdrawlCrawlController::class, 'index']);

//ReverseCrawling
Route::get('/staff_reverse_crawl', [DataEntryCrawlController::class , 'staffRecrawlIndex']);
Route::get('/center_reverse_crawl', [DataEntryCrawlController::class , 'centerRecrawlIndex']);
Route::get('/group_reverse_crawl', [DataEntryCrawlController::class , 'groupRecrawlIndex']);
Route::get('/guarantor_reverse_crawl', [DataEntryCrawlController::class , 'guarantorRecrawlIndex']);
Route::get('/client_reverse_crawl', [ClientReverseCrawl::class , 'index']);
Route::get('/loan_reverse_crawl', [\App\Http\Controllers\ReverseCrawl\LoanReverseCrawl::class , 'index']);
Route::get('/loan_deposit_reverse_crawl', [\App\Http\Controllers\ReverseCrawl\DepositReverseCrawl::class , 'index']);
Route::get('/loan_disbursement_reverse_crawl', [\App\Http\Controllers\ReverseCrawl\DisbursementReverseCrawl::class , 'index']);
Route::get('/loan_repayment_reverse_crawl', [\App\Http\Controllers\ReverseCrawl\RepaymentReverseCrawl::class , 'index']);
Route::get('/saving_reverse_crawl', [SavingReverseCrawl::class , 'index']);


//get permissions
Route::post('/role_has_permissions', [StaffController::class, 'getPermissions']);
});
